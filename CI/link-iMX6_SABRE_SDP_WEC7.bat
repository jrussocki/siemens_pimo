
mkdir c:\WINCE700\3rdParty

mklink /J c:\WINCE700\3rdParty\USBCam %~dp0..\3rdParty\USBCam
mklink /J c:\WINCE700\3rdParty\AR6K %~dp0..\3rdParty\AR6K

mkdir c:\WINCE700\OSDesigns

mklink /J c:\WINCE700\OSDesigns\iMX6_Siemens_PiMo_WEC7 %~dp0..\OSDesigns\iMX6_Siemens_PiMo_WEC7

mklink /J c:\WINCE700\platform\common\src\soc\COMMON_FSL_V4 %~dp0..\platform\common\src\soc\COMMON_FSL_V4
mklink /J c:\WINCE700\platform\common\src\soc\MX6_FSL_V4 %~dp0..\platform\common\src\soc\MX6_FSL_V4

mklink /J c:\WINCE700\platform\iMX6_Siemens_PiMo %~dp0..\platform\iMX6_Siemens_PiMo

