NOTE: cfimager_debug.exe is only applicable for imx53 configuration

cfimager_debug.exe is same as cfimager.exe but the size limits for Eboot and NK have been increased to accomodate debug Eboot and debug NK

Size Limits:
============
Size Limits for Eboot and NK for default and debug builds are given below

Default (Release):
------------------
enum {
        kMX53EBOOTStart = 0x2,
        kMX53NKStart = 0x402,
        kMX53BootPartitionSize = 0x2FFFE,         // 94 MBs - 1024 bytes (sector 0 for MBR, sector 1 rsvd)
};

Debug:
------
enum {
        kMX53EBOOTStart = 0x2,
        kMX53NKStart = 0xC02,
        kMX53BootPartitionSize = 0x5FFFE,         // 192 MBs - 1024 bytes (sector 0 for MBR, sector 1 rsvd)
};