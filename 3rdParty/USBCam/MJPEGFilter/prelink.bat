@REM -------------------------------------------------------------------------
@REM  <copyright file="prelink.bat" company="Microsoft">
@REM   Copyright (c) Microsoft Corporation.  All rights reserved.
@REM 
@REM     The use and distribution terms for this software are covered by the
@REM     Microsoft Limited Permissive License (Ms-LPL) 
@REM     which can be found in the file MS-LPL.txt at the root of this distribution.
@REM     By using this software in any fashion, you are agreeing to be bound by
@REM     the terms of this license.
@REM 
@REM     THE SOFTWARE IS LICENSED "AS-IS" WITH NO WARRANTIES OR INDEMNITIES. 
@REM 
@REM     You must not remove this notice, or any other, from this software.
@REM  </copyright>
@REM  
@REM  <summary>
@REM     MJPEG DirectShow Filter for use with USB camera driver for Windows Embedded CE 6.0
@REM  </summary>
@REM -------------------------------------------------------------------------
@REM ======================================================================
@REM  USB camera driver for Windows Embedded CE 6.0
@REM ======================================================================

@REM Add pre-link commands below.
