//-----------------------------------------------------------------------------
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//-----------------------------------------------------------------------------
//
//  Copyright (C) 2007-2010, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//-----------------------------------------------------------------------------
//
//  File:  kitl.c
//
//  Support routines for KITL.
//
//  Note: These routines are stubbed out in the kern image.
//
//-----------------------------------------------------------------------------
#include <bsp.h>
#include <kitl_cfg.h>
#include <debugserial.h>
#include <usbkitl.h>
#include "common_fec.h"
#include <usbdbgser.h>
#include <usbdbgrndis.h>

VOID FECPowerOff(VOID);
VOID FECPowerOn(VOID);

extern DWORD g_dwKITLThreadPriority;

static PCSP_CCM_REGS g_pCCM = NULL;

//-----------------------------------------------------------------------------

#define BSP_PREV_ARGS_COOKIE    0x8C020800
#define BSP_PREV_ARGS_MAC       0x8C02080C
#define USE_DHCP_RENEW          1

// #define KITL_USE_POLLING_MODE      // Uncomment to enable polled KITL mode.
#define KITL_THREAD_HIGH_PRIORITY 131 // Default KITL thread priority is 131.

#define MII_REG_CR              0 
#define MII_WRITE_COMMAND(reg, val)     CSP_BITFVAL(FEC_MMFR_ST, FEC_MMFR_ST_VALUE)|\
                                        CSP_BITFVAL(FEC_MMFR_OP, FEC_MMFR_OP_WRITE)|\
                                        CSP_BITFVAL(FEC_MMFR_TA, FEC_MMFR_TA_VALUE)|\
                                        CSP_BITFVAL(FEC_MMFR_RA, reg & 0x1f)|\
                                        CSP_BITFVAL(FEC_MMFR_DATA, val & 0xffff)


BSP_ARGS * g_pBSPArgs;

static OAL_KITL_ETH_DRIVER g_kitlUsbRndis = OAL_ETHDRV_RNDIS;

static OAL_KITL_ETH_DRIVER g_kitlEthLan911x = 
{
    LAN911xInit,
    NULL,
    NULL,
    LAN911xSendFrame,
    LAN911xGetFrame,
    LAN911xEnableInts,
    LAN911xDisableInts,
    NULL,
    NULL,
    LAN911xCurrentPacketFilter,
    LAN911xMulticastList
};

static OAL_KITL_ETH_DRIVER g_kitlEthENET = 
{
    ENETInit,
    ENETInitDMABuffer,
    NULL,
    ENETSendFrame,
    ENETGetFrame,
    ENETEnableInts,
    ENETDisableInts,
    ENETPowerOff,
    ENETPowerOn,
    ENETCurrentPacketFilter,
    ENETMulticastList
};

//static OAL_KITL_ETH_DRIVER g_kitlEthFEC = 
//{
//    FECInit,
//    FECInitDMABuffer,
//    NULL,
//    FECSendFrame,
//    FECGetFrame,
//    FECEnableInts,
//    FECDisableInts,
//    FECPowerOff,
//    FECPowerOn,
//    FECCurrentPacketFilter,
//    FECMulticastList
//};

static OAL_KITL_SERIAL_DRIVER g_kitlCspSerial =
{
    SerialInit,
    SerialDeinit,
    SerialSend,
    SerialSendComplete,
    SerialRecv,
    SerialEnableInts,
    SerialDisableInts,
    NULL,
    NULL,
    NULL
};

static OAL_KITL_SERIAL_DRIVER g_kitlUsbSerial = OAL_KITLDRV_USBSERIAL;//USB_SERIAL_KITL ;
static OAL_KITL_ETH_DRIVER g_kitlEthUsbRndis = OAL_KITLDRV_USBRNDIS;//OAL_ETHDRV_RNDIS;//;


static OAL_KITL_DEVICE g_kitlDevices[] = 
{
    {
        L"LAN911x", Internal, BSP_BASE_REG_PA_LAN911x_IOBASE, 0,
        OAL_KITL_TYPE_ETH, &g_kitlEthLan911x
    },
    {
        L"ENET", Internal, CSP_BASE_REG_PA_ENET, 0,
        OAL_KITL_TYPE_ETH, &g_kitlEthENET
    },
    {
        L"USB_RNDIS", Internal, CSP_BASE_REG_PA_USB, 0,
        OAL_KITL_TYPE_ETH, &g_kitlEthUsbRndis
    },
    {
        L"CSPSERIAL", Internal, BSP_BASE_REG_PA_SERIALKITL, 0,
        OAL_KITL_TYPE_SERIAL, &g_kitlCspSerial
    },
    {
        L"USB_SERIAL",Internal, CSP_BASE_REG_PA_USB+1, 0,
        OAL_KITL_TYPE_SERIAL, &g_kitlUsbSerial
    },
    {
        NULL, 0, 0, 0, 0, NULL
    }
};

extern void OEMEthernetDriverEnable(BOOL bEnable);
extern BOOL OEMKitlIoctl (DWORD code, VOID * pInBuffer, DWORD inSize, VOID * pOutBuffer, DWORD outSize, DWORD * pOutSize);
extern VOID OALKitlInitRegistry();
//------------------------------------------------------------------------------
//
//  Function:  FECPowerOff
//
//  Turns off FEC KITL Ethernet hardware.
//
VOID FECPowerOff(VOID)
{
    UINT32 mmfr;
    PCSP_FEC_REGS pFEC = (PCSP_FEC_REGS) OALPAtoUA(CSP_BASE_REG_PA_ENET);

    if (pFEC)
    {
        // Clear MII data transfer flag
        SETREG32 (&pFEC->EIR, CSP_BITFMASK(FEC_EIR_MII));

        // Enter general power down mode (set bit 11 of basic control register)
        mmfr = MII_WRITE_COMMAND(MII_REG_CR, (1 << 11));
        OUTREG32(&pFEC->MMFR, mmfr);

        // Wait for MII data transfer flag
        while (EXTREG32BF(&pFEC->EIR, FEC_EIR_MII) == 0);
    }
}

//------------------------------------------------------------------------------
//
//  Function:  FECPowerOn
//
//  Turns on FEC KITL Ethernet hardware.
//
VOID FECPowerOn(VOID)
{
    UINT32 mmfr;
    PCSP_FEC_REGS pFEC = (PCSP_FEC_REGS) OALPAtoUA(CSP_BASE_REG_PA_ENET);
    PDDK_CLK_CONFIG pDdkClkConfig = (PDDK_CLK_CONFIG) IMAGE_WINCE_DDKCLK_RAM_UA_START;

    if (pFEC && pDdkClkConfig)
    {
        // Clear MII data transfer flag
        SETREG32 (&pFEC->EIR, CSP_BITFMASK(FEC_EIR_MII));

        // If DVFC driver is active, force 10Mbps transfer speed.  This will
        // keep the transfer rate between the FEC and PHY at 2.5 MHz and
        // allow FEC transfers with the PHY to continue for all DVFC peripheral setpoints. 
        if (pDdkClkConfig->bDvfcActive)
        {
            // Clear bit 13 (0 = 10 Mbps)
            // Clear bit 12 (0 = disable auto-negotiation)
            // Set bit 8 (1 = full duplex)
            mmfr = MII_WRITE_COMMAND(MII_REG_CR, (1 << 8));
        }
        // Else, we can enable auto-negotiation
        else
        {
            // Set bit 12 (1 = enable auto-negotiation)
            // Set bit 9 (1 = restart auto-negotiation)
            mmfr = MII_WRITE_COMMAND(MII_REG_CR, ((1 << 12) | (1 << 9)));
        }
        OUTREG32(&pFEC->MMFR, mmfr);

        // Wait for MII data transfer flag
        while (EXTREG32BF(&pFEC->EIR, FEC_EIR_MII) == 0);
    }
}


//------------------------------------------------------------------------------
//
//  Function:  OALEnableKitlSerial
//
//  This function is called from other OAL to enable corresponding UART clock,
//  For Serial KITL
//
static void OALEnableKitlSerial(void)
{
    UINT32 uiMask = 0, uiIndex = 0;
    _OALKITLSharedDataStruct *pOALKITLSharedData =
        (_OALKITLSharedDataStruct *)(g_pOemGlobal->pKitlInfo);

    // Verify that OEMInit() has properly intialized the g_pCCM pointer
    // to provide access to the UART control registers.
    if (pOALKITLSharedData && pOALKITLSharedData->g_pCCM)
    {
#if 1   // MX6_BRING_UP
        // CCM GPR related, to rewrite
        UNREFERENCED_PARAMETER(uiMask);
        UNREFERENCED_PARAMETER(uiIndex);
#else
        switch (BSP_BASE_REG_PA_SERIALKITL)
        {
            case CSP_BASE_REG_PA_UART1:
                uiMask = CCM_CGR_MASK(DDK_CLOCK_GATE_INDEX_UART1_PERCLK);
                uiIndex = CCM_CGR_INDEX(DDK_CLOCK_GATE_INDEX_UART1_PERCLK);
            break;

            case CSP_BASE_REG_PA_UART2:
                uiMask = CCM_CGR_MASK(DDK_CLOCK_GATE_INDEX_UART2_PERCLK);
                uiIndex = CCM_CGR_INDEX(DDK_CLOCK_GATE_INDEX_UART2_PERCLK);
            break;

            case CSP_BASE_REG_PA_UART3:
                uiMask = CCM_CGR_MASK(DDK_CLOCK_GATE_INDEX_UART3_PERCLK);
                uiIndex = CCM_CGR_INDEX(DDK_CLOCK_GATE_INDEX_UART3_PERCLK);
            break;

        }

        if (uiMask)
        {
            SETREG32(&(pOALKITLSharedData->g_pCCM)->CCGR[uiIndex], uiMask);
        }
#endif

    }
}


//------------------------------------------------------------------------------
// Platform entry point for KITL.
//
// Called when KITLIoctl (IOCTL_KITL_STARTUP, ...) is called.
//
BOOL OEMKitlStartup(void)
{
    BOOL rc;
    OAL_KITL_ARGS *pArgs, args;
    CHAR *szDeviceId, buffer[OAL_KITL_ID_SIZE];
    OAL_KITL_DEVICE *pKitlDevice = NULL;
    UINT32 *kitlFlags;
    UINT32 numKitlDevice = 0;

    g_pBSPArgs = (BSP_ARGS *)IMAGE_SHARE_ARGS_UA_START;

#ifdef DEBUG
    // KITL gets its own debug zone and does not share
    // the OAL one any longer.
    //
    // Turn on all debug zones for KITL debug only by setting the following:
    // dpCurSettings.ulZoneMask = 0xFFFF;
    //
    // For somewhat less verbose debugging output, only enable ZONE_ERROR,
    // and ZONE_WARN messages.
    //
    // Also note that ZONE_KITL_ETHER should not be enabled unless you
    // suspect that there may be a problem with Ethernet KITL driver.
    // Otherwise, you will get an almost continuous stream of Ethernet frame
    // handling messages from the Ethernet KITL driver which will result in very
    // slow performance of the target device.
    //
    dpCurSettings.ulZoneMask =  ZONE_ERROR | ZONE_WARNING;
#else
    // For non-Debug builds, only display error and warning messages.
    dpCurSettings.ulZoneMask = ZONE_ERROR | ZONE_WARNING;
#endif

    KITL_RETAILMSG(ZONE_KITL_OAL, ("+OEMKitlStartup\r\n"));

    // Look for bootargs left by the bootloader or left over from an earlier
    // boot.
    //
    pArgs      = (OAL_KITL_ARGS*)OALArgsQuery(OAL_ARGS_QUERY_KITL);
    szDeviceId = (CHAR *)OALArgsQuery(OAL_ARGS_QUERY_DEVID);
    kitlFlags = (UINT *) OALArgsQuery(BSP_ARGS_QUERY_KITL_FLAGS);   // Always valid

    // If we don't get kitl arguments use default
    if (pArgs == NULL)
    {
        KITL_RETAILMSG(ZONE_WARNING, ("WARN: Boot arguments not found, "
                                      "use defaults\r\n"));
        memset(&args, 0, sizeof(args));
        args.flags = *kitlFlags;

        pKitlDevice = &g_kitlDevices[OAL_KITL_ETH_INDEX];
        args.flags  |= OAL_KITL_FLAGS_ENABLED | OAL_KITL_FLAGS_VMINI | OAL_KITL_FLAGS_DHCP;
        args.mac[0] = (BSP_ARGS_DEFAULT_MAC_BYTE0 | (BSP_ARGS_DEFAULT_MAC_BYTE1 << 8));
        args.mac[1] = (BSP_ARGS_DEFAULT_MAC_BYTE2 | (BSP_ARGS_DEFAULT_MAC_BYTE3 << 8)) ;
        args.mac[2] = (BSP_ARGS_DEFAULT_MAC_BYTE4 | (BSP_ARGS_DEFAULT_MAC_BYTE5 << 8));
        args.devLoc.LogicalLoc = CSP_BASE_REG_PA_ENET;
        args.devLoc.IfcType     = Internal;
        pArgs = &args;
    }

#ifndef USE_DHCP_RENEW
    else if (pArgs->flags & OAL_KITL_FLAGS_DHCP)
    {
        // Reset IP address to force DHCP request instead of renew which
        // is acknowleged much sooner from the DHCP server
        pArgs->ipAddress = 0;
    }
#endif

    // If there isn't a device id from bootloader then create one.
    //
    if (szDeviceId == NULL)
    {
        OALKitlCreateName(BSP_DEVICE_PREFIX, args.mac, buffer);
        szDeviceId = buffer;
    }

    for (numKitlDevice = 0;
         (numKitlDevice < (sizeof(g_kitlDevices)/sizeof(g_kitlDevices[0]))) &&
         (g_kitlDevices[numKitlDevice].name != NULL);
         numKitlDevice++)
    {
        if (((UINT32)pArgs->devLoc.PhysicalLoc) == g_kitlDevices[numKitlDevice].id)
        {
            pKitlDevice = &g_kitlDevices[numKitlDevice];
            break;
        }
    }

    if (pKitlDevice == NULL)
        pKitlDevice = &g_kitlDevices[OAL_KITL_ETH_INDEX];

    // Check to see if we need to increase the KITL thread priority in order
    // to ensure reliable operation.
    if (g_dwKITLThreadPriority != KITL_THREAD_HIGH_PRIORITY)
    {
        KITL_RETAILMSG(ZONE_KITL_OAL, ("KITL: Increasing KITL thread priority "
                                       "from %d to %d\r\n",
                                       g_dwKITLThreadPriority,
                                       KITL_THREAD_HIGH_PRIORITY));
        g_dwKITLThreadPriority = KITL_THREAD_HIGH_PRIORITY;
    }

    // Can now enable KITL.
    //
    rc = OALKitlInit(szDeviceId, pArgs, pKitlDevice);

    KITL_RETAILMSG(ZONE_KITL_OAL, ("-OEMKitlStartup(rc = %d)\r\n", rc));
    return(rc);
}


//-----------------------------------------------------------------------------
//
// FUNCTION:    OALSerialInit
//
// DESCRIPTION:
//      Initializes the internal UART with the specified communication settings.
//
// PARAMETERS:
//      NONE
//
// RETURNS:
//      If this function succeeds, it returns TRUE, otherwise
//      it returns FALSE.
//
//-----------------------------------------------------------------------------
BOOL OALSerialInit(PSERIAL_INFO pSerInfo)
{
    BOOL rc = FALSE;
    _OALKITLSharedDataStruct *pOALKITLSharedData =
        (_OALKITLSharedDataStruct *)(g_pOemGlobal->pKitlInfo);

    // Verify that OEMInit() has provided us with valid pointers
    // for accessing the IOMUX and PBC control registers.
    if (pOALKITLSharedData           &&
        pOALKITLSharedData->g_pIOMUX)
    {
        rc = (OALConfigSerialUART(pSerInfo) &&
              OALConfigSerialIOMUX(pSerInfo->uartBaseAddr,
                                   pOALKITLSharedData->g_pIOMUX));
    }

    return rc;
}


//------------------------------------------------------------------------------
//
//  Function:  OEMKitlSerialInit
//
//  This function is called by OALKitlStart to initialize Serial KITL interface
//
//  Parameters:
//      None
//
//  Returns:
//      None
//------------------------------------------------------------------------------
void OEMKitlSerialInit (void)
{
    SERIAL_INFO serInfo;

    serInfo.baudRate      = BSP_UART_KITL_SERIAL_BAUD;
    serInfo.uartBaseAddr  = BSP_BASE_REG_PA_SERIALKITL;
    serInfo.dataBits      = UART_UCR2_WS_8BIT;
    serInfo.parity        = UART_UCR2_PROE_EVEN;
    serInfo.stopBits      = UART_UCR2_STPB_1STOP;
    serInfo.bParityEnable = FALSE;
    serInfo.flowControl   = FALSE;

    OALSerialInit(&serInfo);
}

VOID OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX index, DDK_CLOCK_GATE_MODE mode)
{
    // Update the clock gating mode
    
    _OALKITLSharedDataStruct *pOALKITLSharedData =
        (_OALKITLSharedDataStruct *)(g_pOemGlobal->pKitlInfo);

    // Verify that OEMInit() has properly intialized the g_pCCM pointer
    // to provide access to the UART control registers.
    if (pOALKITLSharedData && pOALKITLSharedData->g_pCCM)
    {
        INSREG32(&pOALKITLSharedData->g_pCCM->CCGR[CCM_CGR_INDEX(index)], CCM_CGR_MASK(index),
             CCM_CGR_VAL(index, mode));
    }
    else
    {
        if(g_pCCM == NULL)
        {
            g_pCCM = (PCSP_CCM_REGS) OALPAtoUA(CSP_BASE_REG_PA_CCM);
        }
        INSREG32(&g_pCCM->CCGR[CCM_CGR_INDEX(index)], CCM_CGR_MASK(index),
             CCM_CGR_VAL(index, mode));
    }
}

