#include <bsp.h>
#include <devload.h>
#include <menu.h>

extern void OEMEthernetDriverEnable(BOOL bEnable);
extern void OEMUsbDriverEnable(BOOL bEnable);
//------------------------------------------------------------------------------
//
//  Function:  OEMKitlIoctl
//
//  This function handles KITL IOCTL codes.
//
//
BOOL OEMKitlIoctl (DWORD code, VOID * pInBuffer, DWORD inSize, VOID * pOutBuffer, DWORD outSize, DWORD * pOutSize)
{
    BOOL fRet = FALSE;

    switch (code) {
    case IOCTL_HAL_INITREGISTRY:
        OALKitlInitRegistry();
        // Leave return code false and set last error to ERROR_NOT_SUPPORTED
        // This allows code to fall through to OEMIoctl so IOCTL_HAL_INITREGISTRY can be 
        // handled there as well.
        NKSetLastError(ERROR_NOT_SUPPORTED);
        break;
	default:
        fRet = OALIoCtlVBridge (code, pInBuffer, inSize, pOutBuffer, outSize, (DWORD32*)pOutSize);
    }

    return fRet;
}

//------------------------------------------------------------------------------
//
//  Function:  OALKitlInitRegistry
//
VOID
OALKitlInitRegistry(
    )
{
    DEVICE_LOCATION devLoc;

	// Get KITL device location
    if (!OALKitlGetDevLoc(&devLoc)) goto cleanUp;

    // Get KITL device location
    if (!OALKitlGetDevLoc(&devLoc)) goto cleanUp;

    // Depending on device bus
    switch (devLoc.IfcType)
        {
        case Internal:
            switch (devLoc.LogicalLoc)
                {
                case CSP_BASE_REG_PA_ENET:
                    // Disable ethernet, enable USB
                    OEMEthernetDriverEnable(FALSE);
                    OEMUsbDriverEnable(TRUE);
                    break;   
                case CSP_BASE_REG_PA_USB:
                    // Disable USB, enable ethernet
                    OEMEthernetDriverEnable(TRUE);
                    OEMUsbDriverEnable(FALSE);
                    break;   
                default:
                    // Enable both USB and ethernet
                    OEMEthernetDriverEnable(TRUE);
                    OEMUsbDriverEnable(TRUE);
                    break;   
                }
            break;
        }

cleanUp:
    return;
}
