//-----------------------------------------------------------------------------
// 
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES.
//
//-----------------------------------------------------------------------------
//
// i2capp.cpp
// This application demonstrates how to use I2C driver in both master and slave mode.
// See Usage() below for arguments, options, etc.
// 
//-----------------------------------------------------------------------------
#include <windows.h>
#include <ceddk.h>
#include "common_macros.h"
#include "i2cbus.h"
#include "i2capp.h"

//-----------------------------------------------------------------------------
// Defines
//-----------------------------------------------------------------------------
#define dprintf NKDbgPrintfW

#define I2C1_DEVICE_NAME         _T("I2C1:")
#define I2C1_ADDR_SLAVE          0x0A//0x12  //this depends on device which is attached.
#define I2C1_SCLK_FREQ           100000//300000 //this depends on device which is attached.

#define I2C2_DEVICE_NAME         _T("I2C2:")
#define I2C2_ADDR_SLAVE          0x50  //this depends on device which is attached.
#define I2C2_SCLK_FREQ           30000 //this depends on device which is attached.

#define I2C3_DEVICE_NAME         _T("I2C3:")
#define I2C3_ADDR_SLAVE          0x4 //this depends on device which is attached.
#define I2C3_SCLK_FREQ           400000 //this depends on device which is attached.

#define I2C_ADDR_MASTER         0x10

#define NELEMS(x) (sizeof(x)/sizeof((x)[0]))


//-----------------------------------------------------------------------------
// Variables
//-----------------------------------------------------------------------------
static HANDLE hI2C;
static int iPacketNo=0;
static BYTE TxData[512]; 
static BYTE RxData[512];


UINT32 WriteRegister( LPCWSTR devName, BYTE i2cBaseAddress, UINT16 usRegNum, UINT16 usValue )
{
	UINT32 uiStatus = 0;
    I2C_TRANSFER_BLOCK I2CXferBlock;
    I2C_PACKET I2CPacket;
    INT iResult;
    UINT8 data[4];

    // set data buffer with register value and data
    data[0] = (UINT8)(( usRegNum >> 8 ) & 0xFF);
    data[1] = (UINT8)(( usRegNum ) & 0xFF);
    data[2] = (UINT8)(( usValue >> 8 ) & 0xFF);
    data[3] = (UINT8)(( usValue ) & 0xFF);

    I2CPacket.wLen = 4;
    I2CPacket.byRW = I2C_RW_WRITE;
    I2CPacket.pbyBuf = (PBYTE)&data;
    I2CPacket.byAddr = i2cBaseAddress;
    // if I2C encounters an error, it will write to *lpiResult. 
    I2CPacket.lpiResult = &iResult;

    I2CXferBlock.pI2CPackets = &I2CPacket;
    I2CXferBlock.iNumPackets = 1;

    // Write register via I2C
	RETAILMSG(TRUE, (_T("%s(0x%x)::WriteRegister(0x%x)=0x%x\r\n"), 
         devName, i2cBaseAddress, usRegNum, usValue));
    //uiStatus = I2CTransfer(hI2C,&I2CXferBlock);
	uiStatus = I2C_MACRO_TRANSFER(hI2C, &I2CXferBlock);
    if (TRUE != uiStatus)
    {
        RETAILMSG(TRUE, (_T("%s(0x%x)::WriteRegister(0x%x)=0x%x failed!\r\n"), 
         devName, i2cBaseAddress, usRegNum, usValue));
    }

    return uiStatus;
}

UINT32 ReadRegister( LPCWSTR devName, BYTE i2cBaseAddress, UINT16 usRegNum )
{
    UINT32 uiStatus = 0;
    I2C_TRANSFER_BLOCK I2CXferBlock;
    I2C_PACKET I2CPktArray[2];
    INT32 iResult;
    UINT8 uiRegData[2];
    UINT8 uiRegValue[2];
	UINT16 usValue = 0;

    // set data buffer with register value
    uiRegData[0] = (UINT8)(( usRegNum & 0xFF00) >> 8);
    uiRegData[1] = (UINT8)(usRegNum & 0x00FF);

    I2CPktArray[0].pbyBuf = (PBYTE) &uiRegData[0];
    I2CPktArray[0].wLen = 2; // let I2C know how much to transmit

    I2CPktArray[0].byRW = I2C_RW_WRITE;
    I2CPktArray[0].byAddr = i2cBaseAddress;
    // if I2C encounters an error, it will write to *lpiResult.    
    I2CPktArray[0].lpiResult = &iResult; 

    I2CPktArray[1].pbyBuf = (PBYTE) &uiRegValue;
    I2CPktArray[1].wLen = 2; // let I2C know how much to receive

    I2CPktArray[1].byRW = I2C_RW_READ;
    I2CPktArray[1].byAddr = i2cBaseAddress;
    // if I2C encounters an error, it will write to *lpiResult.
    I2CPktArray[1].lpiResult = &iResult; 

    I2CXferBlock.pI2CPackets = I2CPktArray;
    I2CXferBlock.iNumPackets = 2;

    // Read register via I2C
    //uiStatus = I2CTransfer(hI2C,&I2CXferBlock);
	uiStatus = I2C_MACRO_TRANSFER(hI2C, &I2CXferBlock);

    // Form 16-bit register value
    usValue = ((UINT16)uiRegValue[0] << 8) | (UINT16)uiRegValue[1];
	
    if (TRUE != uiStatus)
    {
		RETAILMSG(1, (_T("%s(0x%x)::ReadRegister(0x%x)=0x%x failed!\r\n"), 
            devName, i2cBaseAddress, usRegNum, usValue));
	} else {
		RETAILMSG(1, (_T("%s(0x%x)::ReadRegister(0x%x)=0x%x\r\n"), 
            devName, i2cBaseAddress, usRegNum, usValue));
	}

    return uiStatus;
}

int I2COpen(LPCWSTR devName, DWORD dwFreq)
{
	// Create I2C file handle
    hI2C = CreateFile(devName, 
        GENERIC_READ|GENERIC_WRITE, 
        FILE_SHARE_READ|FILE_SHARE_WRITE, 
        NULL, 
        OPEN_EXISTING, 
        FILE_FLAG_RANDOM_ACCESS, 
        NULL); 

    dprintf(TEXT("Create I2C file %s\r\n"),devName );

    if (hI2C == INVALID_HANDLE_VALUE)
    {
        dprintf(TEXT("Create I2C file handle failed \r\n"));
        return -1;
    }

	// Set the master modes
    I2C_MACRO_SET_MASTER_MODE(hI2C);

	// Set frequency
	I2C_MACRO_SET_FREQUENCY(hI2C, dwFreq);
	return 1;
}

void I2CClose()
{
	// Close the I2C file handle
    if (hI2C != NULL)
    {
        CloseHandle(hI2C);
    }
}

//-----------------------------------------------------------------------------
// MasterFunc
//
// The function for I2C master
//-----------------------------------------------------------------------------
void MasterFunc(void)
{
	UINT16 usValue = 1;

	// Test I2C1
	if (I2COpen(I2C1_DEVICE_NAME, I2C1_SCLK_FREQ) >= 0) {
		usValue = 1;
		for (int i = 0; i < 10; i++) {
			usValue *= 2;
			WriteRegister(I2C1_DEVICE_NAME, I2C1_ADDR_SLAVE, 0x0032, usValue);
			ReadRegister(I2C1_DEVICE_NAME, I2C1_ADDR_SLAVE, 0x0032);
		}
		I2CClose();
	}

	// Test I2C2
	if (I2COpen(I2C2_DEVICE_NAME, I2C2_SCLK_FREQ) >= 0) {
		usValue = 1;
		for (int i = 0; i < 10; i++) {
			usValue *= 2;
			WriteRegister(I2C2_DEVICE_NAME, I2C2_ADDR_SLAVE, 0, usValue);
			ReadRegister(I2C2_DEVICE_NAME, I2C2_ADDR_SLAVE, 0);
		}
		I2CClose();
	}

	// Test I2C3
	if (I2COpen(I2C3_DEVICE_NAME, I2C3_SCLK_FREQ) >= 0) {
		usValue = 1;
		for (int i = 0; i < 10; i++) {
			usValue *= 2;
			WriteRegister(I2C3_DEVICE_NAME, I2C3_ADDR_SLAVE, 0x00, usValue);
			ReadRegister(I2C3_DEVICE_NAME, I2C3_ADDR_SLAVE, 0x00);
		}
		I2CClose();
	}
}




//-----------------------------------------------------------------------------
// Usage
//
// Prints brief usage instructions
//-----------------------------------------------------------------------------
void Usage(void)
{
    int i;

    static PTSTR usage_text[] = 
    {
        TEXT("Usage: i2capp"), 
        TEXT("")
    };

    for (i = 0; i < NELEMS(usage_text); i++)
    {
        dprintf(TEXT("%s\r\n"), usage_text[i]);
    }
}


//-----------------------------------------------------------------------------
// WinMain
//
// The main function
//-----------------------------------------------------------------------------
int WINAPI WinMain ( HINSTANCE hInstance, HINSTANCE hPrevInstance, 
    LPTSTR pCmdLine, int nCmdShow)
{
    UNREFERENCED_PARAMETER(hInstance);
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(nCmdShow);
    // Parse command line
    //TCHAR ws[] = TEXT(" \t");
    //PTSTR pOption;
    //TCHAR cFlag;
    //BOOL bMaster = FALSE;
    //BOOL bInvalid = FALSE;

   /*
   pOption = _tcstok(pCmdLine, ws);
    if (pOption!=NULL && pOption[0]!='0')
    {
        cFlag = pOption[1];
    }
    else
    {
        cFlag = '?';
    }

    switch (cFlag)
    {
    case 'm':
        bMaster = TRUE;
        break;

    case 's':
        bMaster = FALSE;
        break;

    case '?':
    case 'h':
        Usage();
        bInvalid = TRUE;
            break;

    default:
        dprintf(TEXT("Unrecognized option %s\r\n"), pOption);
        Usage();
        bInvalid = TRUE;
        break;
    }

    if (bInvalid)
    {
        // we've already issued complaint, now just exit
        return -1;
    }*/
    
	/*
    // Create I2C file handle
    hI2C = CreateFile((LPCWSTR) I2C_DEVICE_NAME, 
        GENERIC_READ|GENERIC_WRITE, 
        FILE_SHARE_READ|FILE_SHARE_WRITE, 
        NULL, 
        OPEN_EXISTING, 
        FILE_FLAG_RANDOM_ACCESS, 
        NULL); 

    dprintf(TEXT("Create I2C file %s\r\n"),I2C_DEVICE_NAME );

    if (hI2C == INVALID_HANDLE_VALUE)
    {
        dprintf(TEXT("Create I2C file handle failed \r\n"));
        return -1;
    }
	*/

    //if (bMaster)
        MasterFunc();
    //else
        //SlaveFunc();

	/*
    // Close the I2C file handle
    if (hI2C != NULL)
    {
        CloseHandle(hI2C);
    }
	*/

    dprintf(TEXT("i2capp quit.\r\n"));

    return 0;
}
