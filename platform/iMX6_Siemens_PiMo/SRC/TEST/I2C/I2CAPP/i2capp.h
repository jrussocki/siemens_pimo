//-----------------------------------------------------------------------------
//
// i2capp.h
// IOCTL_MACROS
// 
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Defines
//-----------------------------------------------------------------------------

// IOCTL_MACROS
// Please note that literal values cannot be used. Whatever values that is to be passed into the macro must
// reside in a variable.
//
// Example: to check if the bus is in master mode
//
//  BOOL bIsMaster = FALSE;
//
//  I2C_MACRO_IS_MASTER(hI2C, bIsMaster);
//
//  if (bIsMaster == TRUE)
//      printf("I2C Bus is in master mode");
//  else
//      printf("I2C Bus is in slave mode");
//

//-----------------------------------------------------------------------------
//
// Macro:   I2C_MACRO_SET_SLAVE_MODE
//
// This macro set the I2C device in slave mode.
//
// Parameters:
//      hDev
//          [in]    The I2C device handle retrieved from CreateFile().
//
// Returns:
//      Return TRUE or FALSE. If the result is TRUE, the operation is
//      successful.
//
//-----------------------------------------------------------------------------
#define I2C_MACRO_SET_SLAVE_MODE(hDev) \
    (DeviceIoControl(hDev, I2C_IOCTL_SET_SLAVE_MODE, NULL, 0, NULL, 0, NULL, NULL))

//-----------------------------------------------------------------------------
//
// Macro:   I2C_MACRO_SET_MASTER_MODE
//
// This macro set the I2C device in master mode.
//
// Parameters:
//      hDev
//          [in]    The I2C device handle retrieved from CreateFile().
//
// Returns:
//      Return TRUE or FALSE. If the result is TRUE, the operation is
//      successful.
//
//-----------------------------------------------------------------------------
#define I2C_MACRO_SET_MASTER_MODE(hDev) \
    (DeviceIoControl(hDev, I2C_IOCTL_SET_MASTER_MODE, NULL, 0, NULL, 0, NULL, NULL))

//-----------------------------------------------------------------------------
//
// Macro:   I2C_MACRO_IS_MASTER
//
// This macro determines whether the I2C is currently in Master mode.
//
// Parameters:
//      hDev
//          [in]    The I2C device handle retrieved from CreateFile().
//      bIsMaster
//          [out]   TRUE if the I2C device is in Master mode.
//
// Returns:
//      Return TRUE or FALSE. If the result is TRUE, the operation is
//      successful.
//
//-----------------------------------------------------------------------------
#define I2C_MACRO_IS_MASTER(hDev, bIsMaster) \
    (DeviceIoControl(hDev, I2C_IOCTL_IS_MASTER, NULL, 0, (PBYTE) &bIsMaster, sizeof(bIsMaster), NULL, NULL))

//-----------------------------------------------------------------------------
//
// Macro:   I2C_MACRO_IS_SLAVE
//
// This macro determines whether the I2C is currently in Slave mode.
//
// Parameters:
//      hDev
//          [in]    The I2C device handle retrieved from CreateFile().
//      bIsSlave
//          [out]   TRUE if the I2C device is in Slave mode.
//
// Returns:
//      Return TRUE or FALSE. If the result is TRUE, the operation is
//      successful.
//
//-----------------------------------------------------------------------------
#define I2C_MACRO_IS_SLAVE(hDev, bIsSlave) \
    (DeviceIoControl(hDev, I2C_IOCTL_IS_SLAVE, NULL, 0, (PBYTE) &bIsSlave, sizeof(bIsSlave), NULL, NULL))

//-----------------------------------------------------------------------------
//
// Macro:   I2C_MACRO_GET_CLOCK_RATE
//
// This macro will retrieve the clock rate divisor. Note that the value is not
// the absolute peripheral clock frequency. The value retrieved should be
// compared against the I2C specifications to obtain the true frequency.
//
// Parameters:
//      hDev
//          [in]    The I2C device handle retrieved from CreateFile().
//      wClkRate
//          [out]   Contains the divisor index. Refer to I2C specification to
//          obtain the true clock frequency.
//
// Returns:  
//      Return TRUE or FALSE. If the result is TRUE, the operation is
//      successful.
//
//-----------------------------------------------------------------------------
#define I2C_MACRO_GET_CLOCK_RATE(hDev, wClkRate) \
    (DeviceIoControl(hDev, I2C_IOCTL_GET_CLOCK_RATE, NULL, 0, (PBYTE) &wClkRate, sizeof(wClkRate), NULL, NULL))

//-----------------------------------------------------------------------------
//
// Macro:   I2C_MACRO_SET_CLOCK_RATE
//
// This macro will initialize the I2C device with the given clock rate. Note
// that this macro does not expect to receive the absolute peripheral clock
// frequency. Rather, it will be expecting the clock rate divisor index stated
// in the I2C specification. If absolute clock frequency must be used, please
// use the macro I2C_MACRO_SET_FREQUENCY.
//
// Parameters:
//      hDev
//          [in]    The I2C device handle retrieved from CreateFile().
//      wClkRate
//          [in]    Contains the divisor index. Refer to I2C specification to
//          obtain the true clock frequency.
//
// Returns:  
//      Return TRUE or FALSE. If the result is TRUE, the operation is
//      successful.
//
//-----------------------------------------------------------------------------
#define I2C_MACRO_SET_CLOCK_RATE(hDev, wClkRate) \
    (DeviceIoControl(hDev, I2C_IOCTL_SET_CLOCK_RATE, (PBYTE) &wClkRate, sizeof(wClkRate), NULL, 0, NULL, NULL))

//-----------------------------------------------------------------------------
//
// Macro:   I2C_MACRO_SET_FREQUENCY
//
// This macro will estimate the nearest clock rate acceptable for I2C device
// and initialize the I2C device to use the estimated clock rate divisor. If
// the estimated clock rate divisor index is required, please refer to the macro
// I2C_MACRO_GET_CLOCK_RATE to determine the estimated index.
//
// Parameters:
//      hDev
//          [in]    The I2C device handle retrieved from CreateFile().
//      dwFreq
//          [in]    The desired frequency.
//
// Returns:  
//      Return TRUE or FALSE. If the result is TRUE, the operation is
//      successful.
//
//-----------------------------------------------------------------------------
#define I2C_MACRO_SET_FREQUENCY(hDev, dwFreq) \
    (DeviceIoControl(hDev, I2C_IOCTL_SET_FREQUENCY, (PBYTE) &dwFreq, sizeof(dwFreq), NULL, 0, NULL, NULL))

//-----------------------------------------------------------------------------
//
// Macro:   I2C_MACRO_SET_SELF_ADDR
//
// This macro will initialize the I2C device with the given address. The device
// will be expected to respond when any master within the I2C bus wish to
// proceed with any transfer. Note that this macro will have no effect if the
// I2C device is in Master mode.
//
// Parameters:
//      hDev
//          [in]    The I2C device handle retrieved from CreateFile().
//      bySelfAddr
//          [in]    The expected I2C device address. The valid range of address
//          is [0x00, 0x7F].
//
// Returns:  
//      Return TRUE or FALSE. If the result is TRUE, the operation is
//      successful.
//
//-----------------------------------------------------------------------------
#define I2C_MACRO_SET_SELF_ADDR(hDev, bySelfAddr) \
    (DeviceIoControl(hDev, I2C_IOCTL_SET_SELF_ADDR, (PBYTE) &bySelfAddr, sizeof(bySelfAddr), NULL, 0, NULL, NULL))

//-----------------------------------------------------------------------------
//
// Macro:   I2C_MACRO_GET_SELF_ADDR
//
// This macro will retrieve the address of the I2C device. Note that this macro
// is only meaningful if it is currently in Slave mode.
//
// Parameters:
//      hDev
//          [in]    The I2C device handle retrieved from CreateFile().
//      bySelfAddr
//          [out]   The current I2C device address. The valid range of address
//          is [0x00, 0x7F].
//
// Returns:  
//      Return TRUE or FALSE. If the result is TRUE, the operation is
//      successful.
//
//-----------------------------------------------------------------------------
#define I2C_MACRO_GET_SELF_ADRR(hDev, bySelfAddr) \
    (DeviceIoControl(hDev, I2C_IOCTL_GET_SELF_ADRR, NULL, 0, (PBYTE) &bySelfAddr, sizeof(bySelfAddr), NULL, NULL))

//-----------------------------------------------------------------------------
//
// Macro:   I2C_MACRO_TRANSFER
//
// This macro performs one or more I2C read or write operations.  pPackets contains
// a pointer to the first of an array of I2C packets to be processed by the I2C.
// All the required information for the I2C operations should be contained
// in the array elements of pPackets.
//
// Parameters:
//      hDev
//          [in]    The I2C device handle retrieved from CreateFile().
//      pI2CTransferBlock
//          [in]    I2C_TRANSFER_BLOCK structure type. 
//                  The fields of I2C_TRANSFER_BLOCK are described below:
//                  pI2CPackets[in]     Holds the pointer to the I2C Packets
//                                      to transfer.
//                  iNumPackets[in]     Number of packets in pI2CPackets array.
//
// Returns:  
//      Return TRUE or FALSE. If the result is TRUE, the operation is
//      successful.
//
//-----------------------------------------------------------------------------
#define I2C_MACRO_TRANSFER(hDev, pI2CTransferBlock) \
    (DeviceIoControl(hDev, I2C_IOCTL_TRANSFER, (PBYTE) pI2CTransferBlock, sizeof(I2C_TRANSFER_BLOCK), NULL, 0, NULL, NULL))

//-----------------------------------------------------------------------------
//
// Macro:   I2C_MACRO_RESET
//
// This macro perform a hardware reset. Note that the I2C driver will still
// maintain all the current information of the device, which includes all the
// initialized addresses.
//
// Parameters:
//      hDev
//          [in]    The I2C device handle retrieved from CreateFile().
//
// Returns:  
//      Return TRUE or FALSE. If the result is TRUE, the operation is
//      successful.
//
//-----------------------------------------------------------------------------
#define I2C_MACRO_RESET(hDev) \
    (DeviceIoControl(hDev, I2C_IOCTL_RESET, NULL, 0, NULL, 0, NULL, NULL))

//-----------------------------------------------------------------------------
//
// Macro:   I2C_MACRO_ENABLE_SLAVE
//
// This macro enable a I2C slave access from the bus. Note that after the I2C slave
// interface enabled, I2C slave driver wait for master access all the time.
//
// Parameters:
//      hDev
//          [in]    The I2C device handle retrieved from CreateFile().
//
// Returns:  
//      Return TRUE or FALSE. If the result is TRUE, the operation is
//      successful.
//
//-----------------------------------------------------------------------------
#define I2C_MACRO_ENABLE_SLAVE(hDev) \
    (DeviceIoControl(hDev, I2C_IOCTL_ENABLE_SLAVE, NULL, 0, NULL, 0, NULL, NULL))

//-----------------------------------------------------------------------------
//
// Macro:   I2C_MACRO_DISABLE_SLAVE
//
// This macro disable I2C slave access from the bus. Note that after the I2C slave
// interface disabled, I2C slave module can be turn off.
//
// Parameters:
//      hDev
//          [in]    The I2C device handle retrieved from CreateFile().
//
// Returns:  
//      Return TRUE or FALSE. If the result is TRUE, the operation is
//      successful.
//
//-----------------------------------------------------------------------------
#define I2C_MACRO_DISABLE_SLAVE(hDev) \
    (DeviceIoControl(hDev, I2C_IOCTL_DISABLE_SLAVE, NULL, 0, NULL, 0, NULL, NULL))

//-----------------------------------------------------------------------------
//
// Macro:   I2C_MACRO_GET_SLAVESIZE
//
// This macro return I2C slave interface buffer length. Note that I2C slave driver
// directly return data to master from interface buffer. The interface buffer can
// be set at any time, even when I2C slave module has been turned off.
//
// Parameters:
//      hDev
//          [in]    The I2C device handle retrieved from CreateFile().
//      bySize
//          [in]    UINT32 variable that store interface buffer length.
//
// Returns:  
//      Return TRUE or FALSE. If the result is TRUE, the operation is
//      successful.
//
//-----------------------------------------------------------------------------
#define I2C_MACRO_GET_SLAVESIZE(hDev, bySize) \
    (DeviceIoControl(hDev, I2C_IOCTL_GET_SLAVESIZE, NULL, 0, (PBYTE)(&bySize), sizeof(bySize), NULL, NULL))

//-----------------------------------------------------------------------------
//
// Macro:   I2C_MACRO_GET_SLAVETEXT
//
// This macro return I2C slave interface buffer text. Note that I2C slave driver
// directly return data to master from interface buffer. The interface buffer can
// be accessed at any time, even when I2C slave module has been turned off.
//
// Parameters:
//      hDev
//          [in]    The I2C device handle retrieved from CreateFile().
//      pbyText
//          [out]   User buffer to store text returned from interface buffer.
//      iSize
//          [in]    User buffer size.
//      iLen
//          [out]   Actual data bytes returned.
//
// Returns:  
//      Return TRUE or FALSE. If the result is TRUE, the operation is
//      successful.
//
//-----------------------------------------------------------------------------
#define I2C_MACRO_GET_SLAVETEXT(hDev, pbyText, iSize, iLen) \
    (DeviceIoControl(hDev, I2C_IOCTL_GET_SLAVE_TXT, NULL, 0, (PBYTE)(pbyText), iSize,  &iLen, NULL))

//-----------------------------------------------------------------------------
//
// Macro:   I2C_MACRO_SET_SLAVESIZE
//
// This macro set I2C slave interface buffer length. The max acceptable length is 
// I2CSLAVEBUFSIZE, if input length is longer than I2CSLAVEBUFSIZE, the operation
// fail, and origin buffer length not changed. Note that I2C slave driver
// directly return data to master from interface buffer. The interface buffer can
// be set at any time, even when I2C slave module has been turned off.
//
// Parameters:
//      hDev
//          [in]    The I2C device handle retrieved from CreateFile().
//      bySize
//          [in]    UINT32 variable that store interface buffer length.
//
// Returns:  
//      Return TRUE or FALSE. If the result is TRUE, the operation is
//      successful.
//
//-----------------------------------------------------------------------------
#define I2C_MACRO_SET_SLAVESIZE(hDev, bySize) \
    (DeviceIoControl(hDev, I2C_IOCTL_SET_SLAVESIZE, (PBYTE)(&bySize), sizeof(bySize), NULL, 0, NULL, NULL))

//-----------------------------------------------------------------------------
//
// Macro:   I2C_MACRO_SET_SLAVETEXT
//
// This macro store I2C slave text to interface buffer. Note that I2C slave driver
// directly return data to master from interface buffer. The interface buffer can
// be accessed at any time, even when I2C slave module has been turned off.
//
// Parameters:
//      hDev
//          [in]    The I2C device handle retrieved from CreateFile().
//      pbyText
//          [out]   User buffer to store text to interface buffer.
//      iLen
//          [in]    User buffer size.
//
// Returns:  
//      Return TRUE or FALSE. If the result is TRUE, the operation is
//      successful.
//
//-----------------------------------------------------------------------------

#define I2C_MACRO_SET_SLAVETEXT(hDev, pbyText, iLen) \
    (DeviceIoControl(hDev, I2C_IOCTL_SET_SLAVE_TXT, (PBYTE)(pbyText), iLen, NULL, 0, NULL, NULL))

