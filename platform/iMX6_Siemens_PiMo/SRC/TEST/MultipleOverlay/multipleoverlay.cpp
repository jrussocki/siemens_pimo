//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES.
//
//-----------------------------------------------------------------------------
//
//  Copyright (C) 2009-2010, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
// File: MultipleOverlay.cpp
//
// Desc: MultipleOverlay is a DirectDraw sample application that demonstates the
//       use of video overlay. It creates a flipable overlay, loads a small
//       animation into the various back buffers, then flips the buffers as
//       it moves the overlay around the screen. Press F12 or ESC to quit.
//
//-----------------------------------------------------------------------------

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
//-----------------------------------------------------------------------------
// Include files
//-----------------------------------------------------------------------------
#include <windows.h>
#include <ddraw.h>
#include "resource.h"

//-----------------------------------------------------------------------------
// Local definitions
//-----------------------------------------------------------------------------
#define NAME                TEXT("MultipleOverlayWndClass")
#define TITLE               TEXT("MultipleOverlay")

#define BUG_WIDTH           320
#define BUG_HEIGHT          200

#define DDSCAPS_PRIMARYSURFACE2 (DDSCAPS_PRIMARYSURFACE|DDSCAPS_VIDEOPORT)

#ifdef UNDER_CE
#define RAND_INT(x) (Random() % x)
#else
#define RAND_INT(x) (rand()*x/RAND_MAX)
#endif
#define RANDOM_VELOCITY() (int)(((RAND_INT(5)+3)*2))

#define NUM_OVERLAYS       1
#define ENABLE_SLIDESHOW   1
#define MOVE_MOSQUITOES    1
#define TEST_GLOBAL_ALPHA  0
#define TEST_LOCAL_ALPHA   1
#define TEST_ZORDER_CHANGE 0
#define ROTATE_TO_FRONT    0

static RECT g_rs;
static RECT g_rd;
DWORD                       g_dwUpdateFlags;
DDOVERLAYFX                 g_ovfx;

#if (TEST_GLOBAL_ALPHA == 1)
UINT32 alpha_values[5] = 
{
    0x00, // fully transparent overlay
    0x20, // slightly opaque
    0x80, // 50% opaque
    0xB0, // 75% opaque
    0xFF  // fully opaque
};
#endif

//-----------------------------------------------------------------------------
// Default settings
//-----------------------------------------------------------------------------
#define TIMER_ID            1
#define TIMER_RATE          200

//-----------------------------------------------------------------------------
// Global data
//-----------------------------------------------------------------------------
LPDIRECTDRAW                g_pDD = NULL;        // DirectDraw object
LPDIRECTDRAWSURFACE         g_pDDSPrimary = NULL; // Primary Surface.
LPDIRECTDRAWSURFACE         g_pDDSOverlay[NUM_OVERLAYS]; // Overlay array
BOOL                        g_bActive = FALSE;   // Is application active?
int                         g_RotationAngles = 0; // Supported rotation angles.
int                         g_CurrentAngle = 0;   // Current rotation angle.

// Secondary surface data for slideshow
LPDIRECTDRAWSURFACE         g_pDDSPrimary2 = NULL; // Primary Surface.
LPDIRECTDRAWSURFACE         g_pDDSBack[3]; // The backup primary.
BOOL                        g_bEnableSlideshow;

// Overlay position and velocity data.
int g_nOverlayXPos[NUM_OVERLAYS], g_nOverlayYPos[NUM_OVERLAYS];
int g_nOverlayXVel[NUM_OVERLAYS], g_nOverlayYVel[NUM_OVERLAYS];
int g_nOverlayWidth[NUM_OVERLAYS], g_nOverlayHeight[NUM_OVERLAYS];

DWORD g_dwOverlayXPositionAlignment;

// Our instance handle.
HINSTANCE g_hInstance;


//-----------------------------------------------------------------------------
// Local data
//-----------------------------------------------------------------------------
static TCHAR                szImg1[] = TEXT("IDB_BUGIMAGE1");
static TCHAR                szImg2[] = TEXT("IDB_BUGIMAGE2");
static TCHAR                szImg3[] = TEXT("IDB_BUGIMAGE3");
static TCHAR                szImgBlue1[] = TEXT("IDB_BUGIMAGEBLUE1");
static TCHAR                szImgBlue2[] = TEXT("IDB_BUGIMAGEBLUE2");
static TCHAR                szImgBlue3[] = TEXT("IDB_BUGIMAGEBLUE3");
static TCHAR                szImgRed1[] = TEXT("IDB_BUGIMAGERED1");
static TCHAR                szImgRed2[] = TEXT("IDB_BUGIMAGERED2");
static TCHAR                szImgRed3[] = TEXT("IDB_BUGIMAGERED3");
static TCHAR                szImgGreen1[] = TEXT("IDB_BUGIMAGEGREEN1");
static TCHAR                szImgGreen2[] = TEXT("IDB_BUGIMAGEGREEN2");
static TCHAR                szImgGreen3[] = TEXT("IDB_BUGIMAGEGREEN3");

static TCHAR                szLSImg1[] = TEXT("IDB_LANDSCAPEIMAGE1");
static TCHAR                szLSImg2[] = TEXT("IDB_LANDSCAPEIMAGE2");
static TCHAR                szLSImg3[] = TEXT("IDB_LANDSCAPEIMAGE3");

// These are the pixel formats this app supports.  Most display adapters
// with overlay support will recognize one or more of these formats.
// We start with YUV format, then work down to RGB. (All 16 bpp.)

static DDPIXELFORMAT ddpfOverlayFormats[] = {
//    {sizeof(DDPIXELFORMAT), DDPF_FOURCC, MAKEFOURCC('Y','V','1','2'),0,0,0,0,0},  // YVU420
//    {sizeof(DDPIXELFORMAT), DDPF_FOURCC, MAKEFOURCC('Y','V','1','P'),0,0,0,0,0},  // YVU420p
//    {sizeof(DDPIXELFORMAT), DDPF_FOURCC, MAKEFOURCC('Y','U','Y','V'),0,0,0,0,0},  // YUYV
//    {sizeof(DDPIXELFORMAT), DDPF_FOURCC, MAKEFOURCC('U','Y','V','Y'),0,0,0,0,0},  // UYVY
    {sizeof(DDPIXELFORMAT), DDPF_RGB, 0, 16,  0xF800, 0x07e0, 0x001F, 0},         // 16-bit RGB 5:6:5
    {sizeof(DDPIXELFORMAT), DDPF_RGB|DDPF_ALPHAPIXELS, 0, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000 }    // 32-bit RGB 8:8:8 + 8-bit Alpha 
};

#define PF_TABLE_SIZE (sizeof(ddpfOverlayFormats) / sizeof(ddpfOverlayFormats[0]))

static RECT rs;
static RECT rd;
DWORD                       dwUpdateFlags;
DDOVERLAYFX                 ovfx;

//-----------------------------------------------------------------------------
// Prototypes
//-----------------------------------------------------------------------------
static void ReleaseAllObjects(void);
static HRESULT InitFail(HWND, HRESULT, LPCTSTR, ...);
static HRESULT RestoreAllSurfaces();
static void MoveOverlay();
long FAR PASCAL WindowProc(HWND, UINT, WPARAM, LPARAM);
static BOOL CopyBitmapToYUVSurface(LPDIRECTDRAWSURFACE, HBITMAP);
static BOOL LoadImageOntoSurface(LPDIRECTDRAWSURFACE, LPCTSTR);
static HRESULT WINAPI EnumSurfacesCallback(LPDIRECTDRAWSURFACE, LPDDSURFACEDESC, LPVOID);
static HRESULT LoadBugImages(INT ovelayNum);
static HRESULT InitApp(HINSTANCE hInstance, int nCmdShow);

//-----------------------------------------------------------------------------
// Name: ReleaseAllObjects()
// Desc: Finished with all objects we use; release them
//-----------------------------------------------------------------------------
void ReleaseAllObjects(void)
{
    for (int i = 0; i < NUM_OVERLAYS; i++)
    {
        if (g_pDDSOverlay[i] != NULL)
        {
            // Use UpdateOverlay() with the DDOVER_HIDE flag to remove an overlay 
            // from the display.
            g_pDDSOverlay[i]->UpdateOverlay(NULL, g_pDDSPrimary, NULL, DDOVER_HIDE, NULL);
            g_pDDSOverlay[i]->Release();
            g_pDDSOverlay[i] = NULL;
        }
    }

    if (g_pDDSPrimary != NULL)
    {
        g_pDDSPrimary->Release();
        g_pDDSPrimary = NULL;
    }

    if (g_pDDSPrimary2 != NULL)
    {
        g_pDDSPrimary2->Release();
        g_pDDSPrimary2 = NULL;
    }

    if (g_pDD != NULL)
    {
        g_pDD->Release();
        g_pDD = NULL;
    }
}

//-----------------------------------------------------------------------------
// Name: InitFail()
// Desc: This function is called if an initialization function fails
//-----------------------------------------------------------------------------
#define PREFIX      TEXT("MULTIPLEOVERLAY: ")
#define PREFIX_LEN  10

HRESULT InitFail(HWND hWnd, HRESULT hRet, LPCTSTR szError,...)
{
    TCHAR                       szBuff[128] = PREFIX;
    va_list                     vl;

    RETAILMSG(1,(TEXT("hRet:%d"),hRet));
    va_start(vl, szError);
    StringCchVPrintf(szBuff + PREFIX_LEN, (128-PREFIX_LEN), szError, vl);
    size_t len = wcslen(szBuff);
    StringCchPrintf(szBuff + len, 128 - len, TEXT("\r\n"));
    ReleaseAllObjects();
    OutputDebugString(szBuff);
    DestroyWindow(hWnd);
    va_end(vl);
    return hRet;
}

#undef PREFIX_LEN
#undef PREFIX



//-----------------------------------------------------------------------------
// Name: RestoreAllSurfaces
// Desc: Called in case we lose our surface's vram.
//-----------------------------------------------------------------------------
static HRESULT
RestoreAllSurfaces()
{
    HRESULT hRet;

    // Try Restoring the primary surface.
    hRet = g_pDDSPrimary->Restore();
    if (hRet != DD_OK)
        return hRet;

    if (g_pDDSPrimary2)
    {
        // Try Restoring the primary surface.
        hRet = g_pDDSPrimary2->Restore();
        if (hRet != DD_OK)
            return hRet;
    }

    for (int i = 0; i < NUM_OVERLAYS; i++)
    {
        // Try Restoring the overlay surfaces.
        hRet = g_pDDSOverlay[i]->Restore();
        if (hRet != DD_OK)
            return hRet;

        // Reload the images.
        hRet = LoadBugImages(i);
        if (hRet != DD_OK)
            return hRet;

        // Show the overlays.
        hRet = g_pDDSOverlay[i]->UpdateOverlay(&rs, g_pDDSPrimary, &rd, DDOVER_SHOW, NULL);
    }

    return hRet;
}



//-----------------------------------------------------------------------------
// Name: MoveOverlay()
// Desc: Called on the timer, this function moves the overlay around the
//       screen, periodically calling flip to animate the mosquito.
//-----------------------------------------------------------------------------
static void
MoveOverlay()
{
    HRESULT         hRet;
    DWORD           dwXAligned[NUM_OVERLAYS];
    static INT      nTestIterator = 0;
    static INT      nMoveCount = 0;
    BOOL            bUpdateSize = FALSE;
    int             iNumOverlays = NUM_OVERLAYS;
    int i;
    static INT      iFrontOverlay = 1;
    BOOL            bPreventResize = TEST_LOCAL_ALPHA; // No IC used with local alpha, so we must
                                                       // prevent resizing for per-pixel alpha to work.

    nMoveCount++;

    // Every 50 moves, check to see if we need to resize or re-order overlays.
    // We resize before computing our new position, to ensure that our position
    // is properly on the screen.
    if (nMoveCount == 50)
    {
        nTestIterator++;
        if (nTestIterator == 3) {nTestIterator++;} // Skip overlay re-ordering step

        switch (nTestIterator)
        {
            case 1:
                if (!bPreventResize)
                {
                    // Increase the size of Overlay 0
                    g_dwUpdateFlags = DDOVER_SHOW | DDOVER_KEYSRCOVERRIDE;

                    // New size is 160x288
                    g_nOverlayWidth[0] = 160;
                    g_nOverlayHeight[0] = 96;

                    // We will call UpdateOverlay to update the size after our position has been updated
                    bUpdateSize = TRUE;
                }

                break;
            case 2:
                // Only try to change overlay 1 size if we have more than 1 overlay
                if ((iNumOverlays > 1) && !bPreventResize)
                {
                    // Increase the size of Overlay 1
                    g_dwUpdateFlags = DDOVER_SHOW | DDOVER_KEYSRCOVERRIDE;

                    g_nOverlayWidth[1] = 400;
                    g_nOverlayHeight[1] = 288;

                    // We will call UpdateOverlay to update the size after our position has been updated
                    bUpdateSize = TRUE;
                }
                break;
            case 3:
                // Only try to reorder overlay 1 if we have more than 1 overlay
                if (iNumOverlays > 1)
                {
                    // Hide overlay before changing order
                    g_pDDSOverlay[1]->UpdateOverlay(NULL, g_pDDSPrimary, NULL, DDOVER_HIDE, NULL);

                    // Set overlay surface 1 to the front
                    hRet = g_pDDSOverlay[1]->UpdateOverlayZOrder(DDOVERZ_SENDTOFRONT, NULL);
                    if (hRet != DD_OK)
                    {
                        OutputDebugString(L"MULTIPLEOVERLAY: Unable to set Z order for Overlay1!");
                    }   
                    /*
                    // Set overlay surface 1 in front of overlay surface 0
                    hRet = g_pDDSOverlay[1]->UpdateOverlayZOrder(DDOVERZ_INSERTINFRONTOF, g_pDDSOverlay[0]);
                    if (hRet != DD_OK)
                    {
                        OutputDebugString(L"MULTIPLEOVERLAY: Unable to set Overlay1 in front of Overlay0!");
                    }
                    */

                    // We will call UpdateOverlay to show the newly reordered overlay after our position has been updated
                    bUpdateSize = TRUE;
                }

                break;
            case 4:
                if (!bPreventResize)
                {
                    // Make Overlay 0 full screen
                    g_dwUpdateFlags = DDOVER_SHOW | DDOVER_KEYSRCOVERRIDE;

                    // ***Note***:  This resize will fail if we are not using a portrait VGA display
                    // e.g. if we have switched to NTSC or PAL TV mode
                    g_nOverlayWidth[0] = 480;
                    g_nOverlayHeight[0] = 640;

                    // We will call UpdateOverlay to update the size after our position has been updated
                    bUpdateSize = TRUE;
                }
                break;
            case 5:
                if (!bPreventResize)
                {
                    // Make Overlay 0 small
                    g_dwUpdateFlags = DDOVER_SHOW | DDOVER_KEYSRCOVERRIDE;

                    g_nOverlayWidth[0] = 160;
                    g_nOverlayHeight[0] = 90;

                    // We will call UpdateOverlay to update the size after our position has been updated
                    bUpdateSize = TRUE;
                }
                break;
            default:
                // After 5, we just shift the front overlay surface
                if (iFrontOverlay < iNumOverlays-1)
                {
// Rotating order is enabled/disabled via #define ROTATE_TO_FRONT
#if (ROTATE_TO_FRONT == 1)
                    // Hide overlay before changing order
                    g_pDDSOverlay[iFrontOverlay]->UpdateOverlay(NULL, g_pDDSPrimary, NULL, DDOVER_HIDE, NULL);

                    // Set overlay surface 1 to the front
                    hRet = g_pDDSOverlay[iFrontOverlay]->UpdateOverlayZOrder(DDOVERZ_SENDTOFRONT, NULL);
                    if (hRet != DD_OK)
                    {
                        OutputDebugString(L"MULTIPLEOVERLAY: Unable to set Z order for Overlay1!");
                    }

                    // We will call UpdateOverlay to show the newly reordered overlay after our position has been updated
                    bUpdateSize = TRUE;
#endif
                }
                else
                {
                    // reset overlay counter
                    iFrontOverlay = 0;
                }
                break;
        }

        // reset move count
        nMoveCount = 0;
    }

    // Now, update coordinates for each overlay
    for (i = 0; i < iNumOverlays; i++)
    {
        // Add the current velocity vectors to the position.
        g_nOverlayXPos[i] += g_nOverlayXVel[i];
        g_nOverlayYPos[i] += g_nOverlayYVel[i];

        // Check to see if this new position puts the overlay off the edge of the screen.
        // SetOverlayPosition() won't like that.

        // Have we gone off the left edge?
        if (g_nOverlayXPos[i] < 0) {
            g_nOverlayXPos[i] = 0;
            g_nOverlayXVel[i] = RANDOM_VELOCITY();
        }

        // Have we gone off the right edge?
        if ((g_nOverlayXPos[i]+g_nOverlayWidth[i]) >  GetSystemMetrics(SM_CXSCREEN)){
            g_nOverlayXPos[i] = GetSystemMetrics(SM_CXSCREEN) - g_nOverlayWidth[i];
            g_nOverlayXVel[i] = -RANDOM_VELOCITY();
        }

        // Have we gone off the top edge?
        if (g_nOverlayYPos[i] < 0) {
            g_nOverlayYPos[i] = 0;
            g_nOverlayYVel[i] = RANDOM_VELOCITY();
        }

        // Have we gone off the bottom edge?
        if ( (g_nOverlayYPos[i]+g_nOverlayHeight[i]) >  GetSystemMetrics(SM_CYSCREEN)) {
            g_nOverlayYPos[i] = GetSystemMetrics(SM_CYSCREEN) - g_nOverlayHeight[i];
            g_nOverlayYVel[i] = -RANDOM_VELOCITY();
        }

        // We need to check for any alignment restrictions on the X position.
        if (g_dwOverlayXPositionAlignment)
            dwXAligned[i] = g_nOverlayXPos[i] - g_nOverlayXPos[i] % g_dwOverlayXPositionAlignment;
        else
            dwXAligned[i] = g_nOverlayXPos[i];
    }

#if (MOVE_MOSQUITOES == 1)
    for (i = 0; i < NUM_OVERLAYS; i++)
    {
        // Set the overlays to their new position.
        hRet = g_pDDSOverlay[i]->SetOverlayPosition(dwXAligned[i], g_nOverlayYPos[i]);
        if (hRet == DDERR_SURFACELOST)
        {
            if (FAILED(RestoreAllSurfaces())) 
                return;
        }
    }
#endif

    // If we are changing the overlay size, come through here to complete the update
    if (bUpdateSize)
    {
        if (nTestIterator == 1)
        {
            if (!bPreventResize)
            {
                // Changing size to 160x96
                g_rd.left = g_nOverlayXPos[0];
                g_rd.top = g_nOverlayYPos[0];
                g_rd.right = g_rd.left + g_nOverlayWidth[0];
                g_rd.bottom = g_rd.top + g_nOverlayHeight[0];

                // Update the overlay parameters for Overlay 0.
                hRet = g_pDDSOverlay[0]->UpdateOverlay(&g_rs, g_pDDSPrimary, &g_rd, g_dwUpdateFlags, &g_ovfx);
                if (hRet != DD_OK)
                    OutputDebugString(L"MULTIPLEOVERLAY: Can't change Overlay 0 size!");
            }
        }
        else if (nTestIterator == 2)
        {
            if ((iNumOverlays > 1) && !bPreventResize)
            {
                // Changing size to 400x288
                g_rd.left = g_nOverlayXPos[1];
                g_rd.top = g_nOverlayYPos[1];
                g_rd.right = g_rd.left + g_nOverlayWidth[1];
                g_rd.bottom = g_rd.top + g_nOverlayHeight[1];

                // Update the overlay parameters for Overlay 1.
                hRet = g_pDDSOverlay[1]->UpdateOverlay(&g_rs, g_pDDSPrimary, &g_rd, g_dwUpdateFlags, &g_ovfx);
                if (hRet != DD_OK)
                    OutputDebugString(L"MULTIPLEOVERLAY: Can't change Overlay 1 size!");
            }
        }
        else if (nTestIterator == 3)
        {
            if (iNumOverlays > 1)
            {
                // Show Overlay 1 after hiding it and re-ordering it.
                hRet = g_pDDSOverlay[1]->UpdateOverlay(&g_rs, g_pDDSPrimary, &g_rd, g_dwUpdateFlags, &g_ovfx);
                if (hRet != DD_OK)
                    OutputDebugString(L"MULTIPLEOVERLAY: Can't change Overlay 1 size!");
            }
        }
        else if (nTestIterator == 4)
        {
            if (!bPreventResize)
            {
                // Changing size to 480x640
                g_rd.left = g_nOverlayXPos[0];
                g_rd.top = g_nOverlayYPos[0];
                g_rd.right = g_rd.left + g_nOverlayWidth[0];
                g_rd.bottom = g_rd.top + g_nOverlayHeight[0];

                // Update the overlay parameters for Overlay 1.
                hRet = g_pDDSOverlay[0]->UpdateOverlay(&g_rs, g_pDDSPrimary, &g_rd, g_dwUpdateFlags, &g_ovfx);
                if (hRet != DD_OK)
                    OutputDebugString(L"MULTIPLEOVERLAY: Can't change Overlay 0 size!");
            }
        }
        else if (nTestIterator == 5)
        {
            if (!bPreventResize)
            {
                // Changing size to 160x96
                g_rd.left = g_nOverlayXPos[0];
                g_rd.top = g_nOverlayYPos[0];
                g_rd.right = g_rd.left + g_nOverlayWidth[0];
                g_rd.bottom = g_rd.top + g_nOverlayHeight[0];

                // Update the overlay parameters for Overlay 0.
                hRet = g_pDDSOverlay[0]->UpdateOverlay(&g_rs, g_pDDSPrimary, &g_rd, g_dwUpdateFlags, &g_ovfx);
                if (hRet != DD_OK)
                    OutputDebugString(L"MULTIPLEOVERLAY: Can't change Overlay 0 size!");
            }
        }
        else
        {
#if (ROTATE_TO_FRONT == 1)
            if (iFrontOverlay < iNumOverlays-1)
            {
                // Show our new front overlay after hiding it and re-ordering it.
                hRet = g_pDDSOverlay[iFrontOverlay]->UpdateOverlay(&g_rs, g_pDDSPrimary, &g_rd, g_dwUpdateFlags, &g_ovfx);
                if (hRet != DD_OK)
                    OutputDebugString(L"MULTIPLEOVERLAY: Can't change Overlay 1 size!");
            }

            iFrontOverlay++;
#endif
        }
    }

#if (TEST_GLOBAL_ALPHA == 1)
    static UINT32 alpha_index = 0;

    g_dwUpdateFlags = DDOVER_ALPHACONSTOVERRIDE | DDOVER_SHOW;
    g_ovfx.dwAlphaConstBitDepth = 8;
    g_ovfx.dwAlphaConst=alpha_values[alpha_index]; // update transparency

    // Update the overlay parameters for Overlay 1.
    hRet = g_pDDSOverlay[0]->UpdateOverlay(&g_rs, g_pDDSPrimary, &g_rd, g_dwUpdateFlags, &g_ovfx);
    if (hRet != DD_OK)
        OutputDebugString(L"MULTIPLEOVERLAY: Can't change ALPHA value!");

    alpha_index++;
    if (alpha_index > 4) {alpha_index = 0;}
#endif

    // Flip.
    for (i = 0; i < iNumOverlays; i++)
    {
        for (;;)
        {
            hRet = g_pDDSOverlay[i]->Flip(NULL, DDFLIP_WAITNOTBUSY);
            if (hRet == DD_OK)
                break;
            if (hRet == DDERR_SURFACELOST)
            {
                hRet = RestoreAllSurfaces();
                if (hRet != DD_OK)
                    break;
            }
            if (hRet != DDERR_WASSTILLDRAWING)
                break;
        }
    }

    if (g_bEnableSlideshow)
    {
        RECT ssRect; // slideshow rect
        ssRect.left = 0;
        ssRect.top = 0;
        ssRect.right = 480;
        ssRect.bottom = 640;

        // Lastly, every 10 moves, we update the slideshow
        // Flip.
        if (nMoveCount % 5 == 0)
        {
            static int j = 0;
            g_pDDSPrimary2->Blt(&ssRect, g_pDDSBack[j++], &ssRect, NULL, NULL);
            j=j%3;
        }
    }
}


//-----------------------------------------------------------------------------
// Name: WindowProc()
// Desc: The Main Window Procedure
//-----------------------------------------------------------------------------
long FAR PASCAL
WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    int                         NewAngle;
    DEVMODE                     DevMode;

    switch (message)
    {
#ifdef UNDER_CE
        case WM_ACTIVATE:
#else
        case WM_ACTIVATEAPP:
#endif
            // Pause if minimized or not the top window
            g_bActive = (wParam == WA_ACTIVE) || (wParam == WA_CLICKACTIVE);
            return 0L;

        case WM_KILLFOCUS:
            // We do not allow anyone else to have the keyboard focus until
            // we are done.
            SetFocus(hWnd);
            return 0L;

        case WM_DESTROY:
            // Clean up and close the app
            ReleaseAllObjects();
            PostQuitMessage(0);
            return 0L;

        case WM_KEYDOWN:
            // Handle any non-accelerated key commands
            switch (wParam)
            {
                case VK_ESCAPE:
                case VK_F12:
                    PostMessage(hWnd, WM_CLOSE, 0, 0);
                    return 0L;

                case VK_SPACE:

                    // Rotate to the "next" angle.

                    if (g_CurrentAngle >= 0 && g_RotationAngles >= 0) {

                        NewAngle = g_CurrentAngle;

                        do
                        {
                            NewAngle <<= 1;

                            if (NewAngle == DMDO_0)
                            {
                                NewAngle = DMDO_90;
                            }

                            if (NewAngle > DMDO_270)
                            {
                                NewAngle = DMDO_0;
                            }
                        } while (!(NewAngle & g_RotationAngles) && (NewAngle != DMDO_0));

                        memset(&DevMode, 0, sizeof (DevMode));
                        DevMode.dmSize               = sizeof (DevMode);
                        DevMode.dmFields             = DM_DISPLAYORIENTATION;
                        DevMode.dmDisplayOrientation = NewAngle;

                        if (DISP_CHANGE_SUCCESSFUL == ChangeDisplaySettingsEx(NULL, &DevMode, NULL, CDS_RESET, NULL)) {

                            g_CurrentAngle = NewAngle;

                            RestoreAllSurfaces();
                        }
                    }
                    return 0L;
            }
            break;

        case WM_TIMER:
            // Update and flip surfaces
            if (g_bActive && TIMER_ID == wParam)
            {
                MoveOverlay();
            }
            break;
    }
    return DefWindowProc(hWnd, message, wParam, lParam);
}



//-----------------------------------------------------------------------------
//  Function: CopyBitmapToYUVSurface
//  Description:
//      Copies an RGB GDI bitmap to a YUV surface. Both bitmap and surface
//      must be a multiple of 2 pixels in width for the supported YUV formats.
//      The following formats are supported:
//              YUYV
//              UYVY
//
//      The "YUYV" YUV pixel format looks like this:
//          As a series of BYTES:    [Y0][U][Y1][V] (reverse it for a DWORD)
//
//      The "UYVY" YUV pixel format looks like this:
//          As a series of BYTES:    [U][Y0][V][Y1] (reverse it for a DWORD)
//
//      As you can see, both formats pack two pixels into a single DWORD. The
//      pixels share U and V components and have separate Y components.
//
//  Returns: TRUE if successful, otherwise FALSE.
//-----------------------------------------------------------------------------
static BOOL
CopyBitmapToYUVSurface(LPDIRECTDRAWSURFACE lpDDSurf, HBITMAP hbm)
{
    HDC                 hdcImage;
    HRESULT             ddrval;
    DDSURFACEDESC       ddsd;
    DWORD               x, y, dwWidth, dwHeight;
    DWORD               dwPitch;
    LPBYTE              pSurf;
    DWORD               dwBytesInRow;
    COLORREF            color;
    BYTE                R,G,B, Y0,Y1,U,V;
    UINT32              UOffset,VOffset,YOffset,iOffset;

    if (hbm == NULL || lpDDSurf == NULL)
    return FALSE;

    //
    //  select bitmap into a memoryDC so we can use it.
    //
    hdcImage = CreateCompatibleDC(NULL);
    SelectObject(hdcImage, hbm);

    memset(&ddsd, 0, sizeof(ddsd));
    ddsd.dwSize = sizeof(ddsd);
    // Lock down the surface so we can modify it's contents.
    ddrval=lpDDSurf->Lock( NULL, &ddsd, DDLOCK_WAITNOTBUSY, NULL);
    if (FAILED(ddrval))
        goto CleanUp;

    dwWidth=ddsd.dwWidth;
    dwHeight=ddsd.dwHeight;
    dwPitch=ddsd.lPitch;
    pSurf=(LPBYTE)ddsd.lpSurface;
    dwBytesInRow=ddsd.dwWidth*2;
    VOffset= dwPitch * dwHeight;
    UOffset= VOffset + VOffset/4;
    YOffset=0;
    iOffset=0;
    //RETAILMSG(1,(TEXT("=============Surface virtual address: 0x%x==========\r\n"),pSurf));
    // Go through the image 2 pixels at a time and convert to YUV
    for(y=0; y<dwHeight; y++)
    {
    for(x=0; x<dwWidth; x+=2)
        {
        // The equations for color conversion used here, probably aren't
        // exact, but they seem to do an OK job.
            color=GetPixel(hdcImage, x,y);
            R=GetRValue(color);
            G=GetGValue(color);
            B=GetBValue(color);
            Y0= (BYTE)(0.29*R + 0.59*G + 0.14*B);
            U= (BYTE)(128.0 - 0.14*R - 0.29*G + 0.43*B);

            color=GetPixel(hdcImage, x+1,y);
            R=GetRValue(color);
            G=GetGValue(color);
            B=GetBValue(color);
            Y1= (BYTE)(0.29*R + 0.57*G + 0.14*B);
            V= (BYTE)(128.0 + 0.36*R - 0.29*G - 0.07*B);

            switch (ddsd.ddpfPixelFormat.dwFourCC)
            {
                case MAKEFOURCC('Y','U','Y','V'):
                    *(pSurf++) = Y0;
                    *(pSurf++) = U;
                    *(pSurf++) = Y1;
                    *(pSurf++) = V;
                    break;
                case MAKEFOURCC('U','Y','V','Y'):
                    *(pSurf++) = U;
                    *(pSurf++) = Y0;
                    *(pSurf++) = V;
                    *(pSurf++) = Y1;
                    break;
                case MAKEFOURCC('Y','V','1','2'):
                    *(pSurf+YOffset) = Y0;
                    if((y%2)==0)
                    {
                        *(pSurf+VOffset+iOffset) = V;
                        *(pSurf+UOffset+iOffset) = U;
                        iOffset++;
                    }
                    YOffset++;
                    *(pSurf+YOffset) = Y1;
                    YOffset++;
                    break;
                //For customized format, the return will be 0.
                //case MAKEFOURCC('Y','V','1','P'):
                case 0:
                    *(pSurf+YOffset) = Y0;
                    if((y%2)==0)
                    {
                        *(pSurf+VOffset+iOffset) = U;
                        *(pSurf+VOffset+iOffset+1) = V;
                        iOffset+=2;
                    }
                    YOffset++;
                    *(pSurf+YOffset) = Y1;
                    YOffset++;
                    break;
            }
        }
        if((ddsd.ddpfPixelFormat.dwFourCC != MAKEFOURCC('Y','V','1','2'))
            &&(ddsd.ddpfPixelFormat.dwFourCC != 0))
            pSurf+=(dwPitch-dwBytesInRow);

        YOffset+=dwPitch-dwWidth;

        if((y%2)==0)
            iOffset+=(dwPitch-dwWidth)/2;
    }

    lpDDSurf->Unlock(NULL);

CleanUp:
    if(hdcImage)
    DeleteDC(hdcImage);

    return TRUE;
}


static BOOL 
AddAlphaChannelToBitmapSurface(LPDIRECTDRAWSURFACE lpDDSurf, HBITMAP hbm)
{
    HDC                 hdcImage;
    HRESULT             ddrval;
    DDSURFACEDESC       ddsd;
    DWORD               x, y, dwWidth, dwHeight;
    DWORD               dwPitch;
    LPBYTE              pSurf, pSurfCurrent;
    DWORD               dwBytesInRow;
    COLORREF            color;
    BYTE                R,G,B,A;
    DWORD                pixelCount = 0;

    if (hbm == NULL || lpDDSurf == NULL)
    return FALSE;

    //
    //  select bitmap into a memoryDC so we can use it.
    //
    hdcImage = CreateCompatibleDC(NULL);
    SelectObject(hdcImage, hbm);

    memset(&ddsd, 0, sizeof(ddsd));
    ddsd.dwSize = sizeof(ddsd);
    // Lock down the surface so we can modify it's contents.
    ddrval=lpDDSurf->Lock( NULL, &ddsd, DDLOCK_WAITNOTBUSY, NULL);
    if (FAILED(ddrval))
        goto CleanUp;

    dwWidth=ddsd.dwWidth;
    dwHeight=ddsd.dwHeight;
    dwPitch=ddsd.lPitch;
    pSurf=(LPBYTE)ddsd.lpSurface;
    pSurfCurrent = pSurf;
    dwBytesInRow=ddsd.dwWidth*4;

//    RETAILMSG(1, (TEXT("dwPitch = %u, dwBytesInRow = %u"), dwPitch, dwBytesInRow));

    // Go through the image 2 pixels at a time and convert to YUV
    for(y=0; y<dwHeight; y++)
    {
        for(x=0; x<dwWidth; x++)
        {
            color=GetPixel(hdcImage, x, y);
            //A=(x>50 && x<100)?0x00:0xFE;
              //A=0x80; // 50% alpha

            R=GetRValue(color);
            G=GetGValue(color);
            B=GetBValue(color);

            if (x < dwWidth/3)
            {
                A = 0x33; // 80% transparent for 1st 3rd
            }
            else if (x < dwWidth*2/3)
            {
                A = 0x80; // 50% transparent for 2nd 3rd
            }
            else
            {
                A = 0xFF; // Fully opaque
            }
//            A=(BYTE)x%255; 

/*
            if (R || G || B) {
                RETAILMSG(1, (TEXT("(%u,%u): A=0x%02x R=0x%02x G=0x%02x B=0x%02x"),y,x,A,R,G,B));
            }
*/

            *(pSurfCurrent++) = B;
            *(pSurfCurrent++) = G;
            *(pSurfCurrent++) = R;
            *(pSurfCurrent++) = A;

            pixelCount++;
        }
        pSurfCurrent+=(dwPitch-dwBytesInRow);
    }

    lpDDSurf->Unlock(NULL);

CleanUp:
    if(hdcImage)
    DeleteDC(hdcImage);

    return TRUE;
}


//-----------------------------------------------------------------------------
//  Function: LoadImageOnToSurface
//  Description:
//      Loads a resource based bitmap image onto a DirectDraw surface.  Can
//      covert the bitmap to all RGB formats, plus a couple YUV formats.
//-----------------------------------------------------------------------------
static BOOL
LoadImageOntoSurface(LPDIRECTDRAWSURFACE lpdds, LPCTSTR lpstrResID)
{
    HBITMAP hbm = NULL;
    HDC     hdcImage = NULL;
    HDC     hdcSurf = NULL;
    BOOL bRetVal = FALSE;
    HRESULT ddrval;
    DDSURFACEDESC ddsd;

    if (!lpdds) return FALSE;

    //
    // get surface size and format.
    //
    memset(&ddsd, 0, sizeof(ddsd));
    ddsd.dwSize = sizeof(ddsd);
    ddsd.dwFlags = DDSD_HEIGHT | DDSD_WIDTH;
    ddrval = lpdds->GetSurfaceDesc(&ddsd);
    if (FAILED(ddrval))
        goto Exit;

    // Load the bitmap resource.  We'll use LoadImage() since it'll scale the
    // image to fit our surface, and maintain the color information in the
    // bitmap.
    hbm = (HBITMAP)LoadImage(g_hInstance, lpstrResID, IMAGE_BITMAP, 0, 0, 0);
    if (hbm == NULL)
        goto Exit;


    // If our surface is a FOURCC YUV format, we need to do a little work to convert
    // our RGB resource bitmap into the appropriate YUV format.
    if (ddsd.ddpfPixelFormat.dwFlags == DDPF_FOURCC)
    {
        if (!CopyBitmapToYUVSurface(lpdds, hbm))
            goto Exit;        
    }
    else if (ddsd.ddpfPixelFormat.dwFlags & DDPF_ALPHAPIXELS)
    {
        if (!AddAlphaChannelToBitmapSurface(lpdds, hbm))
            goto Exit;
    }        
    else  //Looks like we're just using a standard RGB surface format, let GDI do the work.
    {
        // Create a DC and associate the bitmap with it.
        hdcImage = CreateCompatibleDC(NULL);
        SelectObject(hdcImage, hbm);

        ddrval = lpdds->GetDC(&hdcSurf);
        if (FAILED(ddrval))
            goto Exit;

        if (BitBlt(hdcSurf, 0, 0, ddsd.dwWidth, ddsd.dwHeight, hdcImage, 0, 0, SRCCOPY) == FALSE)
            goto Exit;
    }

    bRetVal = TRUE;

Exit:
    if (hdcSurf)
        lpdds->ReleaseDC(hdcSurf);
    if (hdcImage)
        DeleteDC(hdcImage);
    if (hbm)
        DeleteObject(hbm);

    // Debug code to retrieve the buffer address and see that we have written it correctly
#if 0
    {
        DDSURFACEDESC ddsd;
        memset(&ddsd, 0, sizeof(ddsd));
        ddsd.dwSize = sizeof(ddsd);
        // Lock down the surface so we can modify it's contents.
        lpdds->Lock( NULL, &ddsd, DDLOCK_WAITNOTBUSY, NULL);
        LPBYTE pSurf=(LPBYTE)ddsd.lpSurface;
        lpdds->Unlock(NULL);
        
        RETAILMSG(1, (TEXT("pSurf = 0x%08x"), pSurf));
        RETAILMSG(1, (TEXT("")));
    }
#endif

    return bRetVal;
}



//-----------------------------------------------------------------------------
// Name: EnumSurfacesCallback()
// Desc: Used by LoadBugImages to aid it loading all three bug images.
//-----------------------------------------------------------------------------
static HRESULT WINAPI
EnumSurfacesCallback(LPDIRECTDRAWSURFACE lpDDSurface,
                     LPDDSURFACEDESC lpDDSurfaceDesc,
                     LPVOID lpContext)
{
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(lpDDSurfaceDesc);

    int * CallCount = (int *)lpContext;
    HRESULT hr = (HRESULT)DDENUMRET_OK;
    LPCTSTR ResName;

    // Load the Bug Image appropriate...

    if (*CallCount == 0) {
        ResName = szImg2;
    }
    else if (*CallCount == 1) {
        ResName = szImg3;
    }
    else if (*CallCount == 2) {
        ResName = szImgBlue2;
    }
    else if (*CallCount == 3) {
        ResName = szImgBlue3;
    }
    else if (*CallCount == 4) {
        ResName = szImgRed2;
    }
    else if (*CallCount == 5) {
        ResName = szImgRed3;
    }
    else if (*CallCount == 6) {
        ResName = szImgGreen2;
    }
    else if (*CallCount == 7) {
        ResName = szImgGreen3;
    }
    else {
        // Eh?
        hr = (HRESULT)DDENUMRET_CANCEL;
        goto exit;
    }

    if (!LoadImageOntoSurface(lpDDSurface, ResName)) {
        hr = (HRESULT)DDENUMRET_CANCEL;
        goto exit;
    }

    // Bump the count.

    (*CallCount)++;

exit:
    lpDDSurface->Release();
    return hr;
}



//-----------------------------------------------------------------------------
// Name: LoadBugImages()
// Desc: Load the bug resource images into our various flipping surfaces.
//-----------------------------------------------------------------------------
static HRESULT
LoadBugImages(INT overlayNum)
{
    HRESULT hRet = DD_OK;
    int CallCount = 0;

    if (overlayNum == 0)
    {
        // Put the first landscape image onto the first buffer of our complex surface.
        if (!LoadImageOntoSurface(g_pDDSOverlay[0], szImg1))
            return (E_FAIL);

        // Use the enumeration attachment function to load the other images.
        hRet = g_pDDSOverlay[0]->EnumAttachedSurfaces((LPVOID)&CallCount,EnumSurfacesCallback);
    }
    else if (overlayNum == 1)
    {
        // Put the first landscape image onto the first buffer of our complex surface.
        if (!LoadImageOntoSurface(g_pDDSOverlay[1], szImgBlue1))
            return (E_FAIL);

        CallCount = 2;

        // Use the enumeration attachment function to load the other images.
        hRet = g_pDDSOverlay[1]->EnumAttachedSurfaces((LPVOID)&CallCount,EnumSurfacesCallback);
    }
    else if (overlayNum == 2)
    {
        // Put the first landscape image onto the first buffer of our complex surface.
        if (!LoadImageOntoSurface(g_pDDSOverlay[2], szImgRed1))
            return (E_FAIL);

        CallCount = 4;

        // Use the enumeration attachment function to load the other images.
        hRet = g_pDDSOverlay[2]->EnumAttachedSurfaces((LPVOID)&CallCount,EnumSurfacesCallback);
    }
    else if (overlayNum == 3)
    {
        // Put the first landscape image onto the first buffer of our complex surface.
        if (!LoadImageOntoSurface(g_pDDSOverlay[3], szImgGreen1))
            return (E_FAIL);

        CallCount = 6;

        // Use the enumeration attachment function to load the other images.
        hRet = g_pDDSOverlay[3]->EnumAttachedSurfaces((LPVOID)&CallCount,EnumSurfacesCallback);
    }

    return (hRet);
}

//-----------------------------------------------------------------------------
// Name: InitApp()
// Desc: Do work required for every instance of the application:
//          Create the window, initialize data
//-----------------------------------------------------------------------------
static HRESULT
InitApp(HINSTANCE hInstance, int nCmdShow)
{
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(nCmdShow);

    HWND                        hWnd;
    WNDCLASS                    wc;
    DDSURFACEDESC               ddsd;
    DDCAPS                      ddcaps;
    HRESULT                     hRet;
    DWORD                       dwUpdateFlags = 0;
    DDOVERLAYFX                 ovfx;
    DEVMODE                     DevMode;

    // Check for rotation support by getting the rotation angles supported.

    memset(&DevMode, 0, sizeof(DevMode));
    DevMode.dmSize = sizeof(DevMode);
    DevMode.dmFields = DM_DISPLAYQUERYORIENTATION;

    if (DISP_CHANGE_SUCCESSFUL == ChangeDisplaySettingsEx(NULL, &DevMode, NULL, CDS_TEST, NULL)) {

        g_RotationAngles = DevMode.dmDisplayOrientation;
    }
    else {

        OutputDebugString(L"MULTIPLEOVERLAY: Device does not support any rotation modes. Rotation disabled.");
        g_RotationAngles = -1;
    }

    // Get the current rotation angle.

    memset(&DevMode, 0, sizeof (DevMode));
    DevMode.dmSize = sizeof (DevMode);
    DevMode.dmFields = DM_DISPLAYORIENTATION;

    if (DISP_CHANGE_SUCCESSFUL == ChangeDisplaySettingsEx(NULL, &DevMode, NULL, CDS_TEST, NULL)) {

        g_CurrentAngle = DevMode.dmDisplayOrientation;
    }
    else {

        OutputDebugString(L"MULTIPLEOVERLAY: Unable to read current rotation. Rotation disabled.");
        g_CurrentAngle = -1;
    }

    // Set up and register window class.

    wc.style = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc = WindowProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = hInstance;
    wc.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MAIN_ICON));
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH )GetStockObject(BLACK_BRUSH);
    wc.lpszMenuName = NULL;
    wc.lpszClassName = NAME;
    RegisterClass(&wc);

    // Create a window.

    hWnd = CreateWindowEx(WS_EX_TOPMOST,
                          NAME,
                          TITLE,
                          WS_POPUP,
                          0,
                          0,
                          GetSystemMetrics(SM_CXSCREEN),
                          GetSystemMetrics(SM_CYSCREEN),
                          NULL,
                          NULL,
                          hInstance,
                          NULL);
    if (!hWnd)
        return FALSE;
    // We never show the window, only set focus to it.
    SetFocus(hWnd);

    // Create the main DirectDraw object

    hRet = DirectDrawCreate(NULL, &g_pDD, NULL);
    if (hRet != DD_OK)
    {
        return InitFail(hWnd, hRet, TEXT("DirectDrawCreate FAILED"));
    }

    // Get normal mode.

    hRet = g_pDD->SetCooperativeLevel(hWnd, DDSCL_NORMAL);
    if (hRet != DD_OK)
    {
        return InitFail(hWnd, hRet, TEXT("SetCooperativeLevel FAILED"));
    }

    // Get a primary surface interface pointer (only needed for init.)
    memset(&ddsd, 0, sizeof(ddsd));
    ddsd.dwSize = sizeof(ddsd);
    ddsd.dwFlags = DDSD_CAPS;
    ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE;
    hRet = g_pDD->CreateSurface(&ddsd, &g_pDDSPrimary, NULL);
    if (hRet != DD_OK)
    {
        return InitFail(hWnd, hRet, TEXT("CreateSurface FAILED"));
    }
    g_bEnableSlideshow = ENABLE_SLIDESHOW;

    if (g_bEnableSlideshow == TRUE)
    {
        // Create secondary primary surface, for use when we
        // are showing slideshow on 2nd display
        ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE2;
        hRet = g_pDD->CreateSurface(&ddsd, &g_pDDSPrimary2, NULL);
        if (hRet != DD_OK)
        {
            g_bEnableSlideshow = FALSE;
            OutputDebugString(L"MULTIPLEOVERLAY: Couldn't create primary surface 2 for slideshow.  Perhaps we are not in TV mode?");
        }
        else
        {
            // Create surfaces for slideshow slides
            memset(&ddsd, 0, sizeof(ddsd));
            ddsd.dwSize = sizeof(ddsd);
            ddsd.ddsCaps.dwCaps = DDSCAPS_SYSTEMMEMORY ;
            ddsd.dwFlags = DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH|DDSD_PIXELFORMAT;
            ddsd.dwWidth = 480;
            ddsd.dwHeight = 640;
            ddsd.ddpfPixelFormat = ddpfOverlayFormats[0];
            hRet = g_pDD->CreateSurface(&ddsd, &g_pDDSBack[0], NULL);
            hRet = g_pDD->CreateSurface(&ddsd, &g_pDDSBack[1], NULL);
            hRet = g_pDD->CreateSurface(&ddsd, &g_pDDSBack[2], NULL);
            if (hRet != DD_OK)
            {
                return InitFail(hWnd, hRet, TEXT("CreateSurface for backup FAILED"));
            }
            // Load the images.
            if (!LoadImageOntoSurface(g_pDDSBack[0], szLSImg1))
                    return InitFail(hWnd, hRet, TEXT("Unable to load images to surface!"));
            if (!LoadImageOntoSurface(g_pDDSBack[1], szLSImg2))
                    return InitFail(hWnd, hRet, TEXT("Unable to load images to surface!"));
            if (!LoadImageOntoSurface(g_pDDSBack[2], szLSImg3))
                    return InitFail(hWnd, hRet, TEXT("Unable to load images to surface!"));
        }
    }
    // See if we can support overlays.
    memset(&ddcaps, 0, sizeof(ddcaps));
    ddcaps.dwSize = sizeof(ddcaps);
    hRet = g_pDD->GetCaps(&ddcaps, NULL);
    if (hRet != DD_OK)
    {
        return InitFail(hWnd, hRet, TEXT("GetCaps FAILED"));
    }

    if (ddcaps.dwOverlayCaps == 0)
        return InitFail(hWnd, hRet, TEXT("Overlays are not supported in hardware!"));

    // Get alignment info to compute our overlay surface size.
    rs.left = 0;
    rs.top = 0;
    rs.right = BUG_WIDTH;
    rs.bottom = BUG_HEIGHT;
    if (ddcaps.dwAlignSizeSrc != 0)
        rs.right += rs.right % ddcaps.dwAlignSizeSrc;

    // Set up attributes for overlay surfaces 1 and 2
    memset(&ddsd, 0, sizeof(ddsd));
    ddsd.dwSize = sizeof(ddsd);
    ddsd.ddsCaps.dwCaps = DDSCAPS_OVERLAY | DDSCAPS_FLIP;
    ddsd.dwFlags = DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_BACKBUFFERCOUNT |
                   DDSD_PIXELFORMAT;
    ddsd.dwWidth = rs.right;
    ddsd.dwHeight = rs.bottom;

#if (TEST_LOCAL_ALPHA == 1)
    ddsd.ddpfPixelFormat = ddpfOverlayFormats[1];
#else
    ddsd.ddpfPixelFormat = ddpfOverlayFormats[0];
#endif
    ddsd.dwBackBufferCount = 2;

    // Initialize overlay pointers to NULL
    for (int i = 0; i < NUM_OVERLAYS; i++)
    {
        g_pDDSOverlay[i] = NULL;
    }

    // Create our overlays
    for (i = 0; i < NUM_OVERLAYS; i++)
    {
        // Create the Overlay flipping surface, with two backbuffers. 
        hRet = g_pDD->CreateSurface(&ddsd, &g_pDDSOverlay[i], NULL);
        if (hRet != DD_OK)
            return InitFail(hWnd, hRet, TEXT("CreateSurface for overlay FAILED"));
    }

    // Load images for our overlays
    for (i = 0; i < NUM_OVERLAYS; i++)
    {
        if (LoadBugImages(i) != DD_OK)
            return InitFail(hWnd, hRet, TEXT("Unable to load images to overlay surface!"));
    }
    // Finish setting up the overlay.
    int StretchFactor1000 = ddcaps.dwMinOverlayStretch > 1000 ? ddcaps.dwMinOverlayStretch : 1000;
    rd.left = 0;
    rd.top = 0;
    // Adding 999 takes care of integer truncation problems.
    rd.right  = (rs.right * StretchFactor1000 + 999) / 1000;
    rd.bottom = rs.bottom * StretchFactor1000 / 1000;
    
    if (ddcaps.dwAlignSizeDest != 0)
        rd.right = (int)((rd.right + ddcaps.dwAlignSizeDest - 1)/ ddcaps.dwAlignSizeDest) *
                    ddcaps.dwAlignSizeDest;

    // Set a bunch of position and velocity module vars.
    for (i = 0; i < NUM_OVERLAYS; i++)
    {
        g_nOverlayXPos[i] = 0;
        g_nOverlayYPos[i] = 0;
        g_nOverlayXVel[i] = RANDOM_VELOCITY();
        g_nOverlayYVel[i] = RANDOM_VELOCITY();
        g_nOverlayWidth[i] = rd.right - rd.left;
        g_nOverlayHeight[i] = rd.bottom - rd.top;
    }

    // Set the flags we'll send to UpdateOverlay
    dwUpdateFlags = DDOVER_SHOW;
    // Does the overlay hardware support source color keying?
    // If so, we can hide the black background around the image.
    // This probably won't work with YUV formats
    memset(&ovfx, 0, sizeof(ovfx));
    ovfx.dwSize = sizeof(ovfx);
    if (ddcaps.dwOverlayCaps & DDOVERLAYCAPS_CKEYSRC)
    {
        dwUpdateFlags |= DDOVER_KEYSRCOVERRIDE;

        // Create an overlay FX structure so we can specify a source color key.
        // This information is ignored if the DDOVER_SRCKEYOVERRIDE flag 
        // isn't set.
        ovfx.dckSrcColorkey.dwColorSpaceLowValue=0; // black as the color key
        ovfx.dckSrcColorkey.dwColorSpaceHighValue=0;
    }

#if (TEST_LOCAL_ALPHA == 1)
    // Per-Pixel Alpha Blend
    dwUpdateFlags |= DDOVER_ALPHASRC;
#endif

    // Save off global data for UpdateOverlay
    memset(&g_ovfx, 0, sizeof(g_ovfx));
    g_ovfx.dwSize = sizeof(g_ovfx);

    g_ovfx.dckSrcColorkey.dwColorSpaceLowValue=ovfx.dckSrcColorkey.dwColorSpaceLowValue;
    g_ovfx.dckSrcColorkey.dwColorSpaceHighValue=ovfx.dckSrcColorkey.dwColorSpaceHighValue;

    g_dwUpdateFlags = dwUpdateFlags;
    memcpy(&g_rs, &rs, sizeof(RECT));
    memcpy(&g_rd, &rd, sizeof(RECT));

    for (i = 0; i < NUM_OVERLAYS; i++)
    {
        if (i == 1)
        {
            // start Overlay 1 at a different position, offset vertically
            rd.top = 192;
            rd.bottom = rd.bottom + rd.top;

            g_nOverlayXPos[1] = 0;
            g_nOverlayYPos[1] = 300;
        }
        if (i == 2)
        {
            // start Overlay 2 at a different position, offset vertically and horizontally
            rd.left = 100;
            rd.right = rd.right + rd.left;

            g_nOverlayXPos[2] = 100;
            g_nOverlayYPos[2] = 300;
        }
        if (i == 3)
        {
            // start Overlay 3 at a different position, offset vertically and horizontally
            rd.top = 0;
            rd.bottom = BUG_HEIGHT;

            g_nOverlayXPos[3] = 100;
            g_nOverlayYPos[3] = 0;
        }
        // Update the overlay parameters for each overlay.
        hRet = g_pDDSOverlay[i]->UpdateOverlay(&rs, g_pDDSPrimary, &rd, dwUpdateFlags, &ovfx);
        if (hRet != DD_OK)
            return InitFail(hWnd, hRet, TEXT("Unable to show overlay surface!"));
    }

#if (TEST_ZORDER_CHANGE == 1)
    // Set overlay surface 0 to the back
    hRet = g_pDDSOverlay[0]->UpdateOverlayZOrder(DDOVERZ_SENDTOBACK, NULL);
    if (hRet != DD_OK)
    {
        OutputDebugString(L"MULTIPLEOVERLAY: Unable to set Z order for Overlay0!");
    }
    // Set overlay surface 1 to the front
    hRet = g_pDDSOverlay[1]->UpdateOverlayZOrder(DDOVERZ_SENDTOFRONT, NULL);
    if (hRet != DD_OK)
    {
        OutputDebugString(L"MULTIPLEOVERLAY: Unable to set Z order for Overlay1!");
    }
    // Set overlay surface 1 in front of overlay surface 0
    hRet = g_pDDSOverlay[1]->UpdateOverlayZOrder(DDOVERZ_INSERTINFRONTOF, g_pDDSOverlay[0]);
    if (hRet != DD_OK)
    {
        OutputDebugString(L"MULTIPLEOVERLAY: Unable to set Overlay1 in front of Overlay0!");
    }
    // Set overlay surface 0 in back of overlay surface 1
    hRet = g_pDDSOverlay[0]->UpdateOverlayZOrder(DDOVERZ_INSERTINBACKOF, g_pDDSOverlay[1]);
    if (hRet != DD_OK)
    {
        OutputDebugString(L"MULTIPLEOVERLAY: Unable to set Overlay0 in back of Overlay1!");
    }
    // Move overlay surface 0 forward by 1
    hRet = g_pDDSOverlay[0]->UpdateOverlayZOrder(DDOVERZ_MOVEFORWARD, NULL);
    if (hRet != DD_OK)
    {
        OutputDebugString(L"MULTIPLEOVERLAY: Unable to move Overlay0 forward by 1!");
    }
    // Move overlay surface 1 back by 1
    hRet = g_pDDSOverlay[1]->UpdateOverlayZOrder(DDOVERZ_MOVEBACKWARD, NULL);
    if (hRet != DD_OK)
    {
        OutputDebugString(L"MULTIPLEOVERLAY: Unable to move Overlay1 back by 1!");
    }
    
    // Set overlay surface 0 in front of overlay surface 1
    hRet = g_pDDSOverlay[0]->UpdateOverlayZOrder(DDOVERZ_INSERTINFRONTOF, g_pDDSOverlay[1]);
    if (hRet != DD_OK)
    {
        OutputDebugString(L"MULTIPLEOVERLAY: Unable to set Overlay0 in front of Overlay1!");
    }
#endif
    
    // Set the "destination position alignment" global so we won't have to
    // keep calling GetCaps() everytime we move the overlay surface.

    g_dwOverlayXPositionAlignment = ddcaps.dwAlignBoundaryDest;

    // Create a timer to flip the pages.
    if (TIMER_ID != SetTimer(hWnd, TIMER_ID, TIMER_RATE, NULL))
        return InitFail(hWnd, hRet, TEXT("SetTimer FAILED"));

    return DD_OK;
}

//-----------------------------------------------------------------------------
// Name: WinMain()
// Desc: Initialization, message loop
//-----------------------------------------------------------------------------
int PASCAL
WinMain(HINSTANCE hInstance,
        HINSTANCE hPrevInstance,
#ifdef UNDER_CE
        LPWSTR lpCmdLine,
#else
        LPSTR lpCmdLine,
#endif
        int nCmdShow)
{
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    MSG                         msg;

    g_hInstance = hInstance;

    if (InitApp(hInstance, nCmdShow) != DD_OK)
        return FALSE;

    while (GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    return msg.wParam;
}
