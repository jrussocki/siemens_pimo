// TEST_ALS.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "string.h"
#include <als.h>

BOOL IoCtlReturn(BOOL, char*);
void Test_STARTSENSOR(HANDLE);
void Test_STOPSENSOR(HANDLE);
void Test_GETVALUE(HANDLE);
void Test_START_THREAD(HANDLE);
void Test_STOP_THREAD(HANDLE);

int _tmain(int argc, TCHAR *argv[])
{
    HANDLE hALS;
    int i;

    hALS = CreateFile(L"ALS1:",            // name of device
        GENERIC_READ|GENERIC_WRITE,         // desired access
        FILE_SHARE_READ|FILE_SHARE_WRITE,   // sharing mode
        NULL,                               // security attributes (ignored)
        OPEN_EXISTING,                      // creation disposition
        FILE_FLAG_RANDOM_ACCESS,            // flags/attributes
        NULL);                              // template file (ignored)

    if (hALS == INVALID_HANDLE_VALUE) {
        DEBUGMSG(ZONE_ERROR, (L"Invalid handle value!\r\n"));
        return FALSE;
    }
    
    for ( i = 1; i < argc; i++) {
        if ( _tcscmp(argv[i],_T("-s")) == 0 ) {
            Test_STARTSENSOR(hALS);
        } else if ( _tcscmp(argv[i],_T("-S")) == 0 ) {
            Test_STOPSENSOR(hALS);
        } else if ( _tcscmp(argv[i],_T("-getLight")) == 0 ) {
            Test_GETVALUE(hALS);
        }
#ifdef CONFIG_ALS_BKL        
        else if ( _tcscmp(argv[i],_T("-sT")) == 0 ) {
            Test_START_THREAD(hALS);
        } else if ( _tcscmp(argv[i],_T("-ST")) == 0 ) {
            Test_STOP_THREAD(hALS);
        }
#endif // CONFIG_ALS_BKL  
    }

    CloseHandle(hALS);
    return 0;
}

BOOL IoCtlReturn(BOOL ret, char* function) {
    if(ret == FALSE) {
        RETAILMSG(TRUE, (TEXT("\r\nIOControl Error in %s\r\n"), function ));
    }
    return ret;
}

void Test_STARTSENSOR(HANDLE hALS) {
    BOOL ret;

    ret = DeviceIoControl( hALS, IOCTL_ALS_STARTSENSOR,
    NULL, 0,
    NULL, 0,
    NULL, NULL);

    IoCtlReturn(ret, __FUNCTION__);
}

void Test_STOPSENSOR(HANDLE hALS) {
    BOOL ret;

    ret = DeviceIoControl( hALS, IOCTL_ALS_STOPSENSOR,
    NULL, 0,
    NULL, 0,
    NULL, NULL);

    IoCtlReturn(ret, __FUNCTION__);
}

void Test_GETVALUE(HANDLE hALS) {
    UINT16 uValue = 0;
    BOOL ret;

    ret = DeviceIoControl( hALS, IOCTL_ALS_GETVALUE,
    NULL, 0,
    &uValue, sizeof(UINT16),
    NULL, NULL);

    if( IoCtlReturn(ret, __FUNCTION__) ) {
        RETAILMSG(TRUE, (TEXT("\r\nALS Value %u\r\n"), uValue));
    }
}


#ifdef CONFIG_ALS_BKL

void Test_START_THREAD(HANDLE hALS) {
    BOOL ret;

    ret = DeviceIoControl( hALS, IOCTL_ALS_START_THREAD,
    NULL, 0,
    NULL, 0,
    NULL, NULL);

    IoCtlReturn(ret, __FUNCTION__);
}

void Test_STOP_THREAD(HANDLE hALS) {
    BOOL ret;

    ret = DeviceIoControl( hALS, IOCTL_ALS_STOP_THREAD,
    NULL, 0,
    NULL, 0,
    NULL, NULL);

    IoCtlReturn(ret, __FUNCTION__);
}

#endif // CONFIG_ALS_BKL
