#include <windows.h>
#include <winioctl.h>
#include <accapi.h>

DWORD acc_cb(ACC_DATA* pAccData, LPVOID plvCallbackParam){
	
	if(pAccData->hdr.msgType != MESSAGE_ACCELEROMETER_3D_DATA) {
		_tprintf(L"No 3D Data at %d\n", pAccData->hdr.dwTimeStampMs);
        RETAILMSG(TRUE, (TEXT("NO 3D Data at %d\r\n"), pAccData->hdr.dwTimeStampMs ));
		return 1;
	}

	_tprintf(L"ACC : [%f, %f, %f] @ %d\n", pAccData->x,
				pAccData->y,
				pAccData->z,
				pAccData->hdr.dwTimeStampMs
				);
    RETAILMSG(TRUE, (TEXT("ACC : [%d, %d, %d] @ %d\r\n"), (int)(pAccData->x*10),
				(int)(pAccData->y*10),
				(int)(pAccData->z*10),
				pAccData->hdr.dwTimeStampMs
				));
	Sleep(100);

	return ERROR_SUCCESS;
}

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPWSTR lpCmdLine, int nCmShow)
{
	HSENSOR hAcc;

    _tprintf(_T("Accelerometer test app\n"));
    RETAILMSG(TRUE, (TEXT("Accelerometer test app\r\n") ));

	hAcc = AccelerometerOpen(L"ACC1:", NULL);

	if(hAcc == INVALID_HANDLE_VALUE) {
		_tprintf(_T("Invalid Handle Value!\n"));
        RETAILMSG(TRUE, (TEXT("Invalid Handle Value!\r\n") ));
        Sleep(5000);
		return E_FAIL;
	} else if ( hAcc == INVALID_HSENSOR_VALUE) {
		_tprintf(_T("Invalid Sensor Value!\n"));
        RETAILMSG(TRUE, (TEXT("Invalid Sensor Value!\r\n") ));
        Sleep(5000);
		return E_FAIL;
	}
    RETAILMSG(TRUE, (TEXT("AccelerometerOpen [OK]\r\n") ));

	if(ERROR_SUCCESS != AccelerometerCreateCallback(hAcc, acc_cb, NULL)) {
		_tprintf(_T("Error registering callback!\n"));
        RETAILMSG(TRUE, (TEXT("Error registering callback!\r\n") ));
		CloseHandle(hAcc);
        Sleep(5000);
		return E_FAIL;
	}
    RETAILMSG(TRUE, (TEXT("AccelerometerCreateCallback [OK]\r\n") ));

	Sleep(60*1000);

	CloseHandle(hAcc);

    Sleep(5000);
    return 0;
}
