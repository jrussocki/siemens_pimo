//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//

#include "stdafx.h"
#include <winioctl.h>
#include <cs.h>
#include <csmedia.h>

CComPtr<ICaptureGraphBuilder2>  m_pCaptureGraphBuilder;
CComPtr<IBaseFilter>			m_pVideoCaptureFilter;
CComPtr<IMediaControl>			m_pMediaControl;
CComPtr<IVideoWindow>			m_pVideoWindow;
CComPtr<IAMVideoRendererMode>	m_pVideoRenderMode;

static WCHAR					m_wzDeviceName[ MAX_PATH + 1 ];
static HANDLE					m_hCamDriver = NULL;

#define NotifyMessage RETAILMSG

HRESULT 
GetFirstCameraDriver( WCHAR *pwzName )
{
	HRESULT hr = S_OK;
	HANDLE	handle = NULL;
	DEVMGR_DEVICE_INFORMATION di;
	GUID guidCamera = { 0xCB998A05, 0x122C, 0x4166, 0x84, 0x6A, 0x93, 0x3E, 0x4D, 0x7E, 0x3C, 0x86 };
	// Note about the above: The driver material doesn't ship as part of the SDK. This GUID is hardcoded
	// here to be able to enumerate the camera drivers and pass the name of the driver to the video capture filter

    RETAILMSG(1, (TEXT("+GetFirstCameraDriver:\r\n")));

	if( pwzName == NULL )
	{
		return E_POINTER;
	}

	di.dwSize = sizeof(di);

	handle = FindFirstDevice( DeviceSearchByGuid, &guidCamera, &di );
	if(( handle == NULL ) || ( di.hDevice == NULL ) || ( handle == INVALID_HANDLE_VALUE ))
	{
	    RETAILMSG(1, (TEXT("GetFirstCameraDriver: error on FindFirstDevice\r\n")));
		ERR( HRESULT_FROM_WIN32( GetLastError() ));
	}

	StringCchCopy( pwzName, MAX_PATH, di.szLegacyName );

Cleanup:
	FindClose( handle );
    RETAILMSG(1, (TEXT("-GetFirstCameraDriver:\r\n")));
	return hr;
}
//****************************************************************************************
//
//RunCaptureGraph:This function is to used to start or Run  the graph
//
//****************************************************************************************
HRESULT
BuildCaptureGraph(HWND m_hwnd)
{
    HRESULT       hr = S_OK;
    CComVariant   varCamName;
    CPropertyBag  PropBag;

    CComPtr<IMediaEvent>            pMediaEvent;
    CComPtr<IGraphBuilder>          pFilterGraph;
    CComPtr<IPersistPropertyBag>    pPropertyBag;
    CComPtr<IDMOWrapperFilter>      pWrapperFilter;
    CComPtr<IBaseFilter>			pImageSinkFilter;
	CComPtr<IBaseFilter>			pVideoRenderer;
	CComPtr<IAMStreamConfig>		m_pCaptureStreamConfig;
	RECT							m_rcLocation;
	CComPtr<IAMStreamConfig> spConfig;
	AM_MEDIA_TYPE *pmtConfig;

    RETAILMSG(1, (TEXT("+BuildCaptureGraph:\r\n")));
    //*********************************************************************************
    // 1. Create the capture graph builder and register the filtergraph manager. 
    //*********************************************************************************
    RETAILMSG(1, (TEXT("    calling m_pCaptureGraphBuilder.CoCreateInstance\r\n")));
    CHK( m_pCaptureGraphBuilder.CoCreateInstance( CLSID_CaptureGraphBuilder ));
    RETAILMSG(1, (TEXT("    calling pFilterGraph.CoCreateInstance\r\n")));
    CHK( pFilterGraph.CoCreateInstance( CLSID_FilterGraph ));
    RETAILMSG(1, (TEXT("    calling m_pCaptureGraphBuilder->SetFiltergraph\r\n")));
    CHK( m_pCaptureGraphBuilder->SetFiltergraph( pFilterGraph ));

    //*********************************************************************************
    // 2. Create and initialize the video capture filter
    //*********************************************************************************
    RETAILMSG(1, (TEXT("    calling m_pVideoCaptureFilter.CoCreateInstance\r\n")));
    CHK( m_pVideoCaptureFilter.CoCreateInstance( CLSID_VideoCapture ));
    RETAILMSG(1, (TEXT("    calling m_pVideoCaptureFilter.QueryInterface\r\n")));
    CHK( m_pVideoCaptureFilter.QueryInterface( &pPropertyBag ));

    // We are loading the driver CAM1 in the video capture filter. 
    RETAILMSG(1, (TEXT("    calling GetFirstCameraDriver\r\n")));
	CHK( GetFirstCameraDriver( m_wzDeviceName ));
    varCamName = m_wzDeviceName;
    if( varCamName.vt != VT_BSTR )
    {
        ERR( E_OUTOFMEMORY );
    }

    RETAILMSG(1, (TEXT("    calling PropBag.Write with camera name %s\r\n"), m_wzDeviceName));
    CHK( PropBag.Write( L"VCapName", &varCamName ));   
    RETAILMSG(1, (TEXT("    calling pPropertyBag->Load\r\n")));
    CHK( pPropertyBag->Load( &PropBag, NULL ));
    // Everything succeeded, now adding the video capture filter to the filtergraph
    RETAILMSG(1, (TEXT("    calling pFilterGraph->AddFilter\r\n")));
    CHK( pFilterGraph->AddFilter( m_pVideoCaptureFilter, L"Video Capture Filter Source" ));

	//*********************************************************************************
	//2.5 Setup Resolution and color format
	//*********************************************************************************
//#if 0
    RETAILMSG(1, (TEXT("    calling m_pCaptureGraphBuilder->FindInterface IAMStreamConfig\r\n")));
	hr = m_pCaptureGraphBuilder->FindInterface(&PIN_CATEGORY_CAPTURE, NULL, m_pVideoCaptureFilter, IID_IAMStreamConfig, reinterpret_cast<VOID**>(&spConfig));
	if (SUCCEEDED(hr))
	{
		int iCount = 0, iSize = 0;
		VIDEOINFOHEADER *pVih;
		BOOL bDone = FALSE;
	    RETAILMSG(1, (TEXT("    calling spConfig->GetNumberOfCapabilities\r\n")));
		CHK(spConfig->GetNumberOfCapabilities(&iCount, &iSize));
	    RETAILMSG(1, (TEXT("    spConfig->GetNumberOfCapabilities count=%d, size=%d\r\n"), iCount, iSize));
		if (iSize==sizeof(VIDEO_STREAM_CONFIG_CAPS))
		{
			for (int iFormat=0; iFormat<iCount; iFormat++)
			{
				VIDEO_STREAM_CONFIG_CAPS scc;
				hr = spConfig->GetStreamCaps(iFormat, &pmtConfig, (BYTE*)&scc);
				if (SUCCEEDED(hr))
				{
				    RETAILMSG(1, (TEXT("    StreamCaps index=%d\r\n"), iFormat));
					if (pmtConfig->majortype==MEDIATYPE_Video)
					{
					    RETAILMSG(1, (TEXT("               MEDIATYPE_Video\r\n")));
					}
					else
					{
					    RETAILMSG(1, (TEXT("               unknown media type\r\n")));
					}
					if (pmtConfig->subtype==MEDIASUBTYPE_YUY2)
					{
					    RETAILMSG(1, (TEXT("               MEDIASUBTYPE_YUY2\r\n")));
					}
					else if (pmtConfig->subtype==MEDIASUBTYPE_RGB565)
					{
					    RETAILMSG(1, (TEXT("               MEDIASUBTYPE_RGB565\r\n")));
					}
					else
					{
					    RETAILMSG(1, (TEXT("               unknown media subtype\r\n")));
					}

					pVih = (VIDEOINFOHEADER *)pmtConfig->pbFormat;
				    RETAILMSG(0, (TEXT("               size=%dx%d bpp=%d, compression=0x%08X, color used=%d frametime=%d\r\n"), pVih->bmiHeader.biWidth, pVih->bmiHeader.biHeight, pVih->bmiHeader.biBitCount, pVih->bmiHeader.biCompression, pVih->bmiHeader.biClrUsed, pVih->AvgTimePerFrame));

//					if (!bDone && (pVih->bmiHeader.biWidth==CAMERA_PREVIEW_WIDTH) && (pVih->bmiHeader.biHeight==CAMERA_PREVIEW_HEIGHT) && (pVih->AvgTimePerFrame==666666))
//					if (!bDone && (pVih->bmiHeader.biWidth==(CAMERA_PREVIEW_WIDTH/2)) && (pVih->bmiHeader.biHeight==(CAMERA_PREVIEW_HEIGHT/2)) && (pVih->AvgTimePerFrame==666666))
//					if (!bDone && (pVih->bmiHeader.biWidth==(CAMERA_PREVIEW_WIDTH/2)) && (pVih->bmiHeader.biHeight==(CAMERA_PREVIEW_HEIGHT/2)) && (pVih->AvgTimePerFrame==333333))
//					if (!bDone && (pVih->bmiHeader.biWidth==CAMERA_PREVIEW_WIDTH) && (pVih->bmiHeader.biHeight==CAMERA_PREVIEW_HEIGHT) && (pVih->AvgTimePerFrame==333333))
					if (!bDone && (pVih->bmiHeader.biWidth==CAMERA_PREVIEW_WIDTH) && (-pVih->bmiHeader.biHeight==CAMERA_PREVIEW_HEIGHT) && (pVih->AvgTimePerFrame==666666))
					{
						bDone = TRUE;
						hr = spConfig->SetFormat(pmtConfig);
						if (SUCCEEDED(hr))
						{
						    RETAILMSG(1, (TEXT("    SetFormat %d success\r\n"), iFormat));
						}
						else
						{
						    RETAILMSG(1, (TEXT("    SetFormat %d failed\r\n"), iFormat));
						}
					}
				}
				else
				{
				    RETAILMSG(1, (TEXT("               failed\r\n")));
				}
			}
		}
		else
		{
		    RETAILMSG(1, (TEXT("    spConfig->GetNumberOfCapabilities skipped iSize=%d\r\n"), iSize));
		}
	}
//#endif

	//*********************************************************************************
	//3. Create and initialize the Video Renderer Filter
	//*********************************************************************************
    RETAILMSG(1, (TEXT("    calling pVideoRenderer.CoCreateInstance\r\n")));
	CHK( pVideoRenderer.CoCreateInstance( CLSID_VideoMixingRenderer ));
   	NotifyMessage( MESSAGE_INFO, (L"Builing the CLSID_VideoMixingRenderer completed hr=0x%x\r\n" ,hr));	
	//Add the Video Renderer to the Filter Graph
    RETAILMSG(1, (TEXT("    calling pFilterGraph->AddFilter\r\n")));
    CHK( pFilterGraph->AddFilter( pVideoRenderer, L"VideoMixingRenderer" ));
	
#if 0
	hr = pVideoRenderer->FindInterface(
	IVMRFilterConfig::GetRenderingPrefs

// Set the rendering mode and number of streams.   
IVMRFilterConfig* pConfig; 
hr = pVmr->QueryInterface(IID_IVMRFilterConfig, (void**)&pConfig); 
if (SUCCEEDED(hr))  
{ 
    pConfig->SetRenderingMode(VMRMode_Windowless); 
 
    // Set the VMR-7 to mixing mode if you want more than one video 
    // stream, or you want to mix a static bitmap over the video. 
    // (The VMR-9 defaults to mixing mode with four inputs.) 
    if (dwNumStreams > 1 || fBlendAppImage)  
    { 
        pConfig->SetNumberOfStreams(dwNumStreams); 
    } 
    pConfig->Release(); 
 
    hr = pVmr->QueryInterface(IID_IVMRWindowlessControl, (void**)&pWc); 
    if (SUCCEEDED(hr))  
    { 
        pWc->SetVideoClippingWindow(hwndApp); 
        *ppWc = pWc;  // The caller must release this interface. 
    } 
} 
pVmr->Release(); 

#endif

	//*********************************************************************************
	//4. Now render the Graph with Video Capture and Video renderer Filter.
	//*********************************************************************************
    RETAILMSG(1, (TEXT("    calling  m_pCaptureGraphBuilder->RenderStream PIN_CATEGORY_PREVIEW\r\n")));
//	CHK( m_pCaptureGraphBuilder->RenderStream( &PIN_CATEGORY_CAPTURE, &MEDIATYPE_Video, m_pVideoCaptureFilter, NULL, pVideoRenderer ));
	CHK( m_pCaptureGraphBuilder->RenderStream( &PIN_CATEGORY_PREVIEW, &MEDIATYPE_Video, m_pVideoCaptureFilter, NULL, pVideoRenderer ));
#if 0
	IBaseFilter *pMuxBaseFilter;
	IFileSinkFilter *pFileSink;
	IBaseFilter *pVideoDMOWrapperFilter;
	IDMOWrapperFilter *pVideoEncoderDMO;
    RETAILMSG(1, (TEXT("    calling  CoCreateInstance for DMOWrapperFilter\r\n")));
	CHK(CoCreateInstance(CLSID_DMOWrapperFilter, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (LPVOID *)&pVideoDMOWrapperFilter));
    RETAILMSG(1, (TEXT("    calling  pVideoDMOWrapperFilter->QueryInterface\r\n")));
	CHK(pVideoDMOWrapperFilter->QueryInterface(IID_IDMOWrapperFilter, (void **)&pVideoEncoderDMO));
    RETAILMSG(1, (TEXT("    calling  pVideoEncoderDMO->Init\r\n")));
	CHK(pVideoEncoderDMO->Init(CLSID_CWMV9EncMediaObject, DMOCATEGORY_VIDEO_ENCODER));
    RETAILMSG(1, (TEXT("    calling  pFilterGraph->AddFilter for pVideoDMOWrapperFilter\r\n")));
	CHK( pFilterGraph->AddFilter(pVideoDMOWrapperFilter, TEXT("WMV9 DMO Encoder")));
    RETAILMSG(1, (TEXT("    calling  m_pCaptureGraphBuilder->SetOutputFileName\r\n")));
	CHK(m_pCaptureGraphBuilder->SetOutputFileName(&MEDIASUBTYPE_Asf, L"\\SDMemory\\test3.asf", &pMuxBaseFilter, &pFileSink));
    RETAILMSG(1, (TEXT("    calling  m_pCaptureGraphBuilder->RenderStream file capture\r\n")));
//	CHK(m_pCaptureGraphBuilder->RenderStream(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Video, m_pVideoCaptureFilter, NULL, pMuxBaseFilter));
	CHK(m_pCaptureGraphBuilder->RenderStream(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Video, m_pVideoCaptureFilter, pVideoDMOWrapperFilter, pMuxBaseFilter));
//	CHK( m_pCaptureGraphBuilder->RenderStream( &PIN_CATEGORY_CAPTURE, &MEDIATYPE_Video, m_pVideoCaptureFilter, pMuxBaseFilter, pVideoRenderer ));
//    RETAILMSG(1, (TEXT("    calling  m_pCaptureGraphBuilder->RenderStream file capture\r\n")));
//	CHK(m_pCaptureGraphBuilder->RenderStream(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Video, m_pVideoCaptureFilter, NULL, pMuxBaseFilter));
//    RETAILMSG(1, (TEXT("    calling  m_pCaptureGraphBuilder->ControlStream\r\n")));
//	CHK(m_pCaptureGraphBuilder->ControlStream(&PIN_CATEGORY_CAPTURE, NULL, NULL, 0,0,0,0));

//	CHK( m_pCaptureGraphBuilder->RenderStream( &PIN_CATEGORY_STILL, &MEDIATYPE_Video, m_pVideoCaptureFilter, NULL, pVideoRenderer ));
//	CHK( m_pCaptureGraphBuilder->RenderStream( NULL, &MEDIATYPE_Video, m_pVideoCaptureFilter, NULL, pVideoRenderer ));
//    RETAILMSG(1, (TEXT("    calling  m_pCaptureGraphBuilder->RenderStream PIN_CATEGORY_PREVIEW\r\n")));
//	CHK( m_pCaptureGraphBuilder->RenderStream( &PIN_CATEGORY_PREVIEW, &MEDIATYPE_Video, m_pVideoCaptureFilter, NULL, pVideoRenderer ));
//	CHK( m_pCaptureGraphBuilder->RenderStream( NULL, &MEDIATYPE_Video, m_pVideoCaptureFilter, NULL, NULL ));
#endif

	//*********************************************************************************
	//5. Query the VideoWindow interface to set the preview rendering window size.
	//*********************************************************************************
    RETAILMSG(1, (TEXT("    calling  pFilterGraph->QueryInterface m_pVideoWindow\r\n")));
	CHK( pFilterGraph->QueryInterface( &m_pVideoWindow ));
	if(m_pVideoWindow)
    {
        // if the user gave a valid owner window, then set it.  otherwise don't.
        if(m_hwnd)
        {
            // configure the video window
            hr = m_pVideoWindow->put_Owner((OAHWND) m_hwnd);
            if(FAILED(hr))
            {
				NKDbgPrintfW(TEXT("BuildCaptureGraph: Setting the owner window failed.\r\n"));
            }
        }		
		m_rcLocation.left = (GetSystemMetrics(SM_CXSCREEN)/2) - (CAMERA_PREVIEW_WIDTH/2);
		m_rcLocation.right = m_rcLocation.left + CAMERA_PREVIEW_WIDTH;
		m_rcLocation.top = (GetSystemMetrics(SM_CYSCREEN)/2) - (CAMERA_PREVIEW_HEIGHT/2) - 36;
		m_rcLocation.bottom = m_rcLocation.top  + CAMERA_PREVIEW_HEIGHT;

		//RETAILMSG(1,(L"\r\nBefore Setting Right=%d,left=%d,top=%d,bottom=%d\r\n",m_rcLocation.right,m_rcLocation.left,m_rcLocation.top,m_rcLocation.bottom));
        // if the user gave a valid location rectangle to set, then set it, otherwise don't set it and we'll just use the default.
        if(m_rcLocation.left > 0 || m_rcLocation.top > 0 || m_rcLocation.right > 0 || m_rcLocation.bottom > 0)
        {
            hr = m_pVideoWindow->SetWindowPosition(m_rcLocation.left, m_rcLocation.top, m_rcLocation.right - m_rcLocation.left, m_rcLocation.bottom - m_rcLocation.top);
            if(FAILED(hr))
            {
                NKDbgPrintfW(TEXT("BuildCaptureGraph: Setting the window position failed.\r\n"));
            }
        }
        // update our internal location rectangle to reflect what is set in the system.
        // the position set may be different if the position requested isn't possible.
        hr = m_pVideoWindow->GetWindowPosition(&m_rcLocation.left, &m_rcLocation.top, &m_rcLocation.right, &m_rcLocation.bottom);
        if(FAILED(hr))
        {
            NKDbgPrintfW(TEXT("BuildCaptureGraph: Setting the window position failed.\r\n"));
        }
		//RETAILMSG(1,(L"\r\nWidth=%d,left=%d,top=%d,Height=%d\r\n",m_rcLocation.right,m_rcLocation.left,m_rcLocation.top,m_rcLocation.bottom));       
    }  
	
	//CHK( pFilterGraph->QueryInterface( &m_pVideoRenderMode ));
   	//NotifyMessage( MESSAGE_INFO, (L"Builing the m_pVideoRenderMode completed hr=0x%x\r\n" ),hr);	
	//Setting the Renderer to use DDRAW renderer(To Use Hardware Layer).
	//CHK(m_pVideoRenderMode->SetMode(AM_VIDEO_RENDERER_MODE_DDRAW));
	
	// Get the media control interface for controlling the connected graph
    RETAILMSG(1, (TEXT("    calling  pFilterGraph->QueryInterface m_pMediaControl\r\n")));
    CHK( pFilterGraph->QueryInterface( &m_pMediaControl ));

   	NotifyMessage( MESSAGE_INFO, (L"Builing the graph completed\r\n" ));

Cleanup:
	if( FAILED( hr ))
	{
		NotifyMessage( MESSAGE_ERROR, (L"BuildCaptureGraph: Builing the graph failed hr=0x%x\r\n",hr));
	}
    RETAILMSG(1, (TEXT("-BuildCaptureGraph:\r\n")));
    return hr;
}

#define MEDIAEVENT_TIMEOUT 4000

HRESULT
SendCamMessage()
{
//	PUCHAR pInBuf, DWORD InBufLen, PUCHAR pOutBuf, DWORD OutBufLen, PDWORD pdwBytesTransferred;

	if (m_hCamDriver==NULL)
	{
//		m_hCamDriver = CreateFile(m_wzDeviceName, GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		m_hCamDriver = CreateFile(m_wzDeviceName, GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if ((m_hCamDriver==INVALID_HANDLE_VALUE) || (m_hCamDriver==NULL))
		{
			m_hCamDriver = NULL;
			DWORD dwErr = GetLastError();
			RETAILMSG(1, (TEXT("SendCamMessage: Failed to create file %s error=0x%08X\r\n"), m_wzDeviceName, dwErr));
			return (HRESULT) dwErr;
		}
	}

	DWORD dwInBuf = 0x12345678;
	DWORD dwOutBuf = 0;
	DWORD dwReturned = 0;
	if (DeviceIoControl(m_hCamDriver, IOCTL_CS_PROPERTY, &dwInBuf, sizeof(dwInBuf), &dwOutBuf, sizeof(dwOutBuf), &dwReturned, NULL))
	{
		RETAILMSG(1, (TEXT("SendCamMessage: IOCTL success\r\n")));
	}
	else
	{
		RETAILMSG(1, (TEXT("SendCamMessage: IOCTL error=0x%08X\r\n"), GetLastError()));
	}

	if (m_hCamDriver!=NULL)
	{
		CloseHandle(m_hCamDriver);
		m_hCamDriver = NULL;
	}
	return S_OK;
}

//****************************************************************************************
//
//RunCaptureGraph:This function is to used to start or Run  the graph
//
//****************************************************************************************
HRESULT
RunCaptureGraph()
{
    HRESULT hr = S_OK;
	OAFilterState fsBefore;

	RETAILMSG(1, (TEXT("+RunCaptureGraph: hr=0x%08X\r\n"), hr));
    if(m_pMediaControl == NULL)
    {
        NKDbgPrintfW(TEXT("RunCaptureGraph: Core components needed for RunCaptureGraph unavailable.\r\n"));
        hr = E_FAIL;
        goto Cleanup;
    }
	CHK(m_pMediaControl->GetState(MEDIAEVENT_TIMEOUT, &fsBefore));    
	switch (fsBefore)
	{
	case State_Stopped:
		RETAILMSG(1, (TEXT("RunCaptureGraph: State_Stopped before run\r\n")));
		break;
	case State_Paused:
		RETAILMSG(1, (TEXT("RunCaptureGraph: State_Paused before run\r\n")));
		break;
	case State_Running:
		RETAILMSG(1, (TEXT("RunCaptureGraph: State_Running before run\r\n")));
		break;
	}

	//Run the Graph
    CHK( m_pMediaControl->Run());
	// Run can return S_FALSE, with everything being ok. We have two options here,
	// assume everything is ok and change return to S_OK, or wait for the GetState
	// to switch to the run state.
	BOOL bStateDone = FALSE;
	FILTER_STATE fsAfter;
	while (!bStateDone)
	{
		CHK(m_pMediaControl->GetState(MEDIAEVENT_TIMEOUT, (OAFilterState *) &fsAfter));
		if (hr==S_OK)
		{
			bStateDone = TRUE;
			switch (fsAfter)
			{
			case State_Stopped:
				RETAILMSG(1, (TEXT("RunCaptureGraph: State_Stopped after run\r\n")));
				hr = E_FAIL;
				break;
			case State_Paused:
				RETAILMSG(1, (TEXT("RunCaptureGraph: State_Paused after run\r\n")));
				hr = E_FAIL;
				break;
			case State_Running:
				RETAILMSG(1, (TEXT("RunCaptureGraph: State_Running after run\r\n")));
				break;
			}
		}
		else if (hr==VFW_S_CANT_CUE)
		{
			bStateDone = TRUE;
		}
	}
	RETAILMSG(1, (TEXT("+RunCaptureGraph:  m_pMediaControl->Run hr=0x%08X\r\n"), hr));
	
    NotifyMessage( MESSAGE_INFO,( L"The Graph is running\r\n" ));

Cleanup:
	if( FAILED( hr ))
	{
		NotifyMessage( MESSAGE_ERROR, (L"Runing the capture graph failed\r\n" ));
	}

	RETAILMSG(1, (TEXT("-RunCaptureGraph: return=0x%08X\r\n"), hr));
    return hr;
}

//****************************************************************************************
//
//PauseGraph:This function is to used to Pause the graph state.
//
//****************************************************************************************
HRESULT
PauseGraph()
{
    HRESULT hr = S_OK;

    if(m_pMediaControl == NULL)
    {
        NKDbgPrintfW(TEXT("PauseGraph: Core components needed for PauseGraph unavailable.\r\n"));
        hr = E_FAIL;
        goto cleanup;
    }

	//Pause the Filter Graph
    hr = m_pMediaControl->Pause();
    if(FAILED(hr))
    {
        NKDbgPrintfW(TEXT("PauseGraph: Pausing the graph failed.\r\n"));
        goto cleanup;
    }

    OAFilterState fs;
    hr = m_pMediaControl->GetState(MEDIAEVENT_TIMEOUT, &fs);
    if(FAILED(hr))
    {
		NKDbgPrintfW(TEXT("PauseGraph:Retrieving the graph state failed.\r\n"));
        goto cleanup;
    }

    if(fs != State_Paused)
    {
		NKDbgPrintfW(TEXT("PauseGraph:Failed to change the filter state.\r\n"));
        goto cleanup;
    }

cleanup:
    return hr;
}
//****************************************************************************************
//
//StopGraph:This function is to used to stop the Filter graph state.
//
//****************************************************************************************
HRESULT StopGraph()
{
    HRESULT hr = S_OK;

	RETAILMSG(1, (TEXT("+StopGraph:\r\n")));
    if(m_pMediaControl == NULL)
    {
        NKDbgPrintfW(TEXT("StopGraph: Core components needed for StopGraph unavailable.\r\n"));
        hr = E_FAIL;
        goto cleanup;
    }

    // if something fails when stopping the graph, and we weren't initialized, then it's not necessarily a failure.
    // if we were initialized, then everything should succeed.

    NKDbgPrintfW(TEXT("StopGraph: calling m_pMediaControl->Stop\r\n"));
    hr = m_pMediaControl->Stop();
    if(FAILED(hr))
    {
        NKDbgPrintfW(TEXT("StopGraph: Stopping the graph failed.\r\n"));
        goto cleanup;
    }

    OAFilterState fs;
    hr = m_pMediaControl->GetState(MEDIAEVENT_TIMEOUT, &fs);
    if(FAILED(hr))
    {
        NKDbgPrintfW(TEXT("StopGraph: Retrieving the graph state failed.\r\n"));
        goto cleanup;
    }
	else
	{
		switch (fs)
		{
		case State_Stopped:
			RETAILMSG(1, (TEXT("StopGraph: State_Stopped\r\n")));
			break;
		case State_Paused:
			RETAILMSG(1, (TEXT("StopGraph: State_Paused\r\n")));
			break;
		case State_Running:
			RETAILMSG(1, (TEXT("StopGraph: State_Running\r\n")));
			break;
		}
	}

cleanup:
	RETAILMSG(1, (TEXT("-StopGraph: return=0x%08X\r\n"), hr));
    return hr;
}
//****************************************************************************************
//
//ReleaseAllDshowObjects:This function is to used to CleanUp the Filter graph objects.
//
//****************************************************************************************
HRESULT
ReleaseAllDshowObjects()
{
	if(m_pVideoWindow)
    {
        // Clear the owner window for the video window
        // this may fail depending on the owners and such, so ignore any failures.
        m_pVideoWindow->put_Owner((OAHWND) NULL);
    }
	PauseGraph();
	StopGraph();

	m_pVideoWindow  = NULL;
	m_pVideoRenderMode = NULL;
	m_pMediaControl = NULL;
	m_pVideoCaptureFilter = NULL;
	m_pCaptureGraphBuilder = NULL;

	return true;
}

