//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//
//-----------------------------------------------------------------------------
// File: DDrawTest.cpp
//
// Desc: DDrawTest is a DirectDraw sample application that demonstates the
//       use of video overlay. It creates a flipable overlay, loads a small
//       text over the Camera output. Press F12 or ESC to quit.
//       
//
//-----------------------------------------------------------------------------

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
//-----------------------------------------------------------------------------
// Include files
//-----------------------------------------------------------------------------
#include <windows.h>
#include <ddraw.h>
#include <winbase.h>
#include <objbase.h>
#include <commctrl.h>

#include <streams.h>
#include <dmodshow.h>
#include <dmoreg.h>
#include <wmcodecids.h>

#include "struct.h"
#include "CPropertyBag.h"

//-----------------------------------------------------------------------------
// Local definitions
//-----------------------------------------------------------------------------
#define CLASSNAME           TEXT("DShowCamTestWndClass")
#define TITLE               TEXT("Camera Demo")

#define IDC_BTN_CLOSE		1001
#define IDC_BTN_PLAY		1002
#define IDC_BTN_STOP		1003
#define IDC_BTN_REWIND		1004
#define IDC_BTN_FASTFORWARD	1005
#define IDC_BTN_PAUSE		1006
#define IDC_BTN_RESUME		1007

//-----------------------------------------------------------------------------
// Default settings
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Global data
//-----------------------------------------------------------------------------
static BOOL g_bActive = FALSE;   // Is application active?
static BOOL g_bGraphRunning = FALSE;
static HWND g_hWndBtnExit = NULL;
static HWND g_hWndBtnPlay = NULL;
static HWND g_hWndBtnStop = NULL;
static HWND g_hWndBtnPause = NULL;
static HWND g_hWndBtnResume = NULL;
static HWND g_hWndBtnRewind = NULL;
static HWND g_hWndBtnFastForward = NULL;

// Our instance handle.

HINSTANCE g_hInstance;

//-----------------------------------------------------------------------------
// Local data
//-----------------------------------------------------------------------------

static RECT rs;
static RECT rd;

//-----------------------------------------------------------------------------
// Prototypes
//-----------------------------------------------------------------------------

HRESULT InitFail(HWND, HRESULT, LPCTSTR, ...);
long FAR PASCAL WindowProc(HWND, UINT, WPARAM, LPARAM);
HRESULT InitApp(HINSTANCE hInstance, int nCmdShow);
void HandleError(HRESULT hr);

HRESULT StartCam(BOOL bHandleErr=FALSE);
HRESULT StopCam(BOOL bHandleErr=FALSE);
HRESULT PauseCam(BOOL bHandleErr=FALSE);
HRESULT ResumeCam(BOOL bHandleErr=FALSE);
HRESULT RewindCam(BOOL bHandleErr=FALSE);
HRESULT FastForwardCam(BOOL bHandleErr=FALSE);
BOOL InitCamControl(void);
BOOL DeInitCamControl(void);
HRESULT SendCamCommand(HANDLE hEvt);

static HANDLE m_hEvtPause = NULL;
static HANDLE m_hEvtResume = NULL;
static HANDLE m_hEvtRewind = NULL;
static HANDLE m_hEvtFastForward = NULL;

//-----------------------------------------------------------------------------
// Name: WindowProc()
// Desc: The Main Window Procedure
//-----------------------------------------------------------------------------
long FAR PASCAL
WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
	case WM_CREATE:
		{
		int nBtnX, nBtnY, nWid, nHgt, nBtnSpace;
		nBtnX = 20;
		nWid = 100;
		nBtnY = 40;
		nHgt = 25;
		nBtnSpace = 20 + nHgt;
		g_hWndBtnPlay = CreateWindow(TEXT("BUTTON"), TEXT("Play"), WS_CHILD|WS_VISIBLE|WS_TABSTOP|WS_DISABLED|BS_PUSHBUTTON,
			nBtnX, nBtnY, nWid, nHgt, hWnd, (HMENU) IDC_BTN_PLAY, g_hInstance, NULL);
		nBtnY += nBtnSpace;
		g_hWndBtnStop = CreateWindow(TEXT("BUTTON"), TEXT("Stop"), WS_CHILD|WS_VISIBLE|WS_TABSTOP|WS_DISABLED|BS_PUSHBUTTON,
			nBtnX, nBtnY, nWid, nHgt, hWnd, (HMENU) IDC_BTN_STOP, g_hInstance, NULL);
		nBtnY += nBtnSpace;
		nBtnY += nBtnSpace;
		g_hWndBtnPause = CreateWindow(TEXT("BUTTON"), TEXT("Pause"), WS_CHILD|WS_VISIBLE|WS_TABSTOP|WS_DISABLED|BS_PUSHBUTTON,
			nBtnX, nBtnY, nWid, nHgt, hWnd, (HMENU) IDC_BTN_PAUSE, g_hInstance, NULL);
		nBtnY += nBtnSpace;
		g_hWndBtnResume = CreateWindow(TEXT("BUTTON"), TEXT("Resume"), WS_CHILD|WS_VISIBLE|WS_TABSTOP|WS_DISABLED|BS_PUSHBUTTON,
			nBtnX, nBtnY, nWid, nHgt, hWnd, (HMENU) IDC_BTN_RESUME, g_hInstance, NULL);
		nBtnY += nBtnSpace;
		g_hWndBtnRewind = CreateWindow(TEXT("BUTTON"), TEXT("Rewind"), WS_CHILD|WS_VISIBLE|WS_TABSTOP|WS_DISABLED|BS_PUSHBUTTON,
			nBtnX, nBtnY, nWid, nHgt, hWnd, (HMENU) IDC_BTN_REWIND, g_hInstance, NULL);
		nBtnY += nBtnSpace;
		g_hWndBtnFastForward = CreateWindow(TEXT("BUTTON"), TEXT("Fast Forward"), WS_CHILD|WS_VISIBLE|WS_TABSTOP|WS_DISABLED|BS_PUSHBUTTON,
			nBtnX, nBtnY, nWid, nHgt, hWnd, (HMENU) IDC_BTN_FASTFORWARD, g_hInstance, NULL);
		nBtnY += nBtnSpace;
		nBtnY += nBtnSpace;
		g_hWndBtnExit = CreateWindow(TEXT("BUTTON"), TEXT("Close"), WS_CHILD|WS_VISIBLE|WS_TABSTOP|BS_PUSHBUTTON,
			nBtnX, nBtnY, nWid, nHgt, hWnd, (HMENU) IDC_BTN_CLOSE, g_hInstance, NULL);
		}
		return 1;

#ifdef UNDER_CE
        case WM_ACTIVATE:
#else
        case WM_ACTIVATEAPP:
#endif
            // Pause if minimized or not the top window
            g_bActive = (wParam == WA_ACTIVE) || (wParam == WA_CLICKACTIVE);
            return 0L;
#if 0
        case WM_KILLFOCUS:
            // We do not allow anyone else to have the keyboard focus until
            // we are done.
            SetFocus(hWnd);
            return 0L;
#endif
#if 0
		case WM_PAINT:
			{
	            PAINTSTRUCT ps;
		        HDC hdc;
				RECT rectClient;
				TEXTMETRIC tm;

			    hdc = BeginPaint(hWnd, &ps);

				GetClientRect(hWnd, &rectClient);
				ZeroMemory(&tm, sizeof(tm));
				GetTextMetrics(hdc, &tm);
				DrawText(hdc, TEXT("Tap outside camera preview or hit ESC to exit"), -1, &rectClient, DT_CENTER|DT_TOP);
				rectClient.top += ((tm.tmHeight*3)/2);
				DrawText(hdc, TEXT("Hit SPACE to start and stop capture"), -1, &rectClient, DT_CENTER|DT_TOP);

		        EndPaint(hWnd, &ps);
			}
			break;
#endif
        case WM_DESTROY:
            // Clean up and close the app
			ReleaseAllDshowObjects();
            PostQuitMessage(0);
            return 0L;

		case WM_COMMAND:
			{
				WORD wIdItem, wNotifyCode;
				HWND hwndCtrl;

				wIdItem = (WORD) LOWORD(wParam);
				wNotifyCode = (WORD) HIWORD(wParam);
				hwndCtrl = (HWND) lParam;
				RETAILMSG(1, (TEXT("DShowCamTest:WindowProc WM_COMMAND ID=%d or notify code=%d\r\n"), wIdItem, wNotifyCode));

				if (wNotifyCode==BN_CLICKED)
				{
					switch (wIdItem)
					{
					case IDC_BTN_CLOSE:
						RETAILMSG(1, (TEXT("DShowCamTest:WindowProc IDC_BTN_CLOSE\r\n")));
		                PostMessage(hWnd, WM_CLOSE, 0, 0);
						break;
					case IDC_BTN_PLAY:
						RETAILMSG(1, (TEXT("DShowCamTest:WindowProc IDC_BTN_PLAY\r\n")));
						StartCam(TRUE);
						break;
					case IDC_BTN_STOP:
						RETAILMSG(1, (TEXT("DShowCamTest:WindowProc IDC_BTN_STOP\r\n")));
						StopCam();
						break;
					case IDC_BTN_PAUSE:
						RETAILMSG(1, (TEXT("DShowCamTest:WindowProc IDC_BTN_PAUSE\r\n")));
						PauseCam();
						break;
					case IDC_BTN_RESUME:
						RETAILMSG(1, (TEXT("DShowCamTest:WindowProc IDC_BTN_RESUME\r\n")));
						ResumeCam();
						break;
					case IDC_BTN_REWIND:
						RETAILMSG(1, (TEXT("DShowCamTest:WindowProc IDC_BTN_REWIND\r\n")));
						RewindCam();
						break;
					case IDC_BTN_FASTFORWARD:
						RETAILMSG(1, (TEXT("DShowCamTest:WindowProc IDC_BTN_FASTFORWARD\r\n")));
						FastForwardCam();
						break;
					}
				}
			}
			return 0;
#if 0
        case WM_LBUTTONDOWN:
			RETAILMSG(1, (TEXT("DShowCamTest:WindowProc left mouse button down\r\n")));
            PostMessage(hWnd, WM_CLOSE, 0, 0);
            return 0L;              
#endif
        case WM_KEYDOWN:
            // Handle any non-accelerated key commands
            switch (wParam)
            {
			case VK_SPACE:
#if 0
				// start/stop capture
				if (g_bGraphRunning)
				{
					// graph running, need to turn it off
					hr = StopGraph();
					if (!SUCCEEDED(hr))
					{
						// failure stopping graph
						_stprintf_s(&tcMsg[0], sizeof(tcMsg)/sizeof(TCHAR), TEXT("Failed stopping capture, error 0x%08X."), hr);
						MessageBox(hWnd, &tcMsg[0], NULL, MB_OK|MB_ICONERROR);
					}
					g_bGraphRunning = FALSE;
				}
				else
				{
					hr = RunCaptureGraph();
					if (SUCCEEDED(hr))
					{
						// run graph successful
						g_bGraphRunning = TRUE;
					}
					else
					{
						// failure starting graph
						_stprintf_s(&tcMsg[0], sizeof(tcMsg)/sizeof(TCHAR), TEXT("Failed starting capture, error 0x%08X."), hr);
						MessageBox(hWnd, &tcMsg[0], NULL, MB_OK|MB_ICONERROR);
					}
				}
				return 0L;
#endif
				break;

			case VK_ESCAPE:
            case VK_F12:
#if 0
				RETAILMSG(1, (TEXT("DShowCamTest:WindowProc escape key\r\n")));
                PostMessage(hWnd, WM_CLOSE, 0, 0);
                return 0L;
#endif
	            break;        
            }
			break;
    }
    return DefWindowProc(hWnd, message, wParam, lParam);
}

//-----------------------------------------------------------------------------
// Name: WinMain()
// Desc: Initialization, message loop
//-----------------------------------------------------------------------------
int PASCAL
WinMain(HINSTANCE hInstance,
        HINSTANCE hPrevInstance,
#ifdef UNDER_CE
        LPWSTR lpCmdLine,
#else
        LPSTR lpCmdLine,
#endif
        int nCmdShow)
{
    MSG                         msg;
	HWND                        hWnd;
    WNDCLASS                    wc;
	RECT						rcWorkArea;

	RETAILMSG(1, (TEXT("+DShowCamTest:WinMain\r\n")));
	CoInitializeEx(NULL, COINIT_MULTITHREADED);

    g_hInstance = hInstance;

	    // Set up and register window class.

    wc.style = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc = WindowProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = hInstance;
    wc.hIcon = NULL;//LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MAIN_ICON));
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH )GetStockObject(WHITE_BRUSH);
    wc.lpszMenuName = NULL;
//    wc.lpszClassName = NAME;
    wc.lpszClassName = CLASSNAME;
    RegisterClass(&wc);

    // Create a window.
    // Now, create the main window
	SystemParametersInfo(SPI_GETWORKAREA, 0, &rcWorkArea, 0);

	hWnd = CreateWindow(CLASSNAME, TITLE, WS_VISIBLE | WS_SYSMENU | WS_OVERLAPPED,
		rcWorkArea.left,
            rcWorkArea.top,
            rcWorkArea.right - rcWorkArea.left,
            rcWorkArea.bottom - rcWorkArea.top,NULL, NULL, hInstance, NULL);
    if (!hWnd)
        return FALSE;

	SetFocus(hWnd);

	//Build the DirectShow Camera Filter graph For rendering the video on the screen
	RETAILMSG(1, (TEXT("DShowCamTest:WinMain calling BuildCaptureGraph\r\n")));
	HRESULT hr = BuildCaptureGraph(hWnd);

	if (hr!=S_OK)
	{
		// handle error
		HandleError(hr);

		// want to exit after this
		PostQuitMessage(0);
	}
	else
	{
		//Run the Filter graph to start the streaming.
		hr = StartCam();

		if (hr!=S_OK)
		{
			// handle error
			HandleError(hr);

			// want to exit after this
			PostQuitMessage(0);
		}
	}

	if (hr==S_OK)
	{
		if (!InitCamControl())
		{
			// handle error
			HandleError(E_FAIL);

			// want to exit after this
			PostQuitMessage(0);
		}
	}

	while (GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

	DeInitCamControl();

	CoUninitialize();
	RETAILMSG(1, (TEXT("-DShowCamTest:WinMain\r\n")));
    return msg.wParam;
}

void HandleError(HRESULT hr)
{
	TCHAR tcMsg[MAX_PATH];

	switch (hr)
	{
//	case 0x80070012:
//	case 0x80070002:
	case __HRESULT_FROM_WIN32(ERROR_NO_MORE_FILES):
	case __HRESULT_FROM_WIN32(ERROR_FILE_NOT_FOUND):
		_stprintf_s(&tcMsg[0], sizeof(tcMsg)/sizeof(TCHAR), TEXT("Camera not found, error 0x%08X."), hr);
		MessageBox(NULL, &tcMsg[0], NULL, MB_OK|MB_ICONERROR);
		break;

	default:
		_stprintf_s(&tcMsg[0], sizeof(tcMsg)/sizeof(TCHAR), TEXT("Unknown Error 0x%08X."), hr);
		MessageBox(NULL, &tcMsg[0], NULL, MB_OK|MB_ICONERROR);
		break;
	}
}

HRESULT StartCam(BOOL bHandleErr/*=FALSE*/)
{
	HRESULT hr = S_OK;

	if (!g_bGraphRunning)
	{
		RETAILMSG(1, (TEXT("DShowCamTest:StartCam calling RunCaptureGraph\r\n")));
		hr = RunCaptureGraph();
		if (hr==S_OK)
		{
			g_bGraphRunning = TRUE;

			// disable play button
			if (g_hWndBtnPlay!=NULL)
				EnableWindow(g_hWndBtnPlay, FALSE);
			// enable stop button
			if (g_hWndBtnStop!=NULL)
				EnableWindow(g_hWndBtnStop, TRUE);
			// enable pause button
			if (g_hWndBtnPause!=NULL)
				EnableWindow(g_hWndBtnPause, TRUE);
			// disable resume button
			if (g_hWndBtnResume!=NULL)
				EnableWindow(g_hWndBtnResume, FALSE);
			// disable rewind button
			if (g_hWndBtnRewind!=NULL)
				EnableWindow(g_hWndBtnRewind, FALSE);
			// disable fast forward button
			if (g_hWndBtnFastForward!=NULL)
				EnableWindow(g_hWndBtnFastForward, FALSE);
		}
		else
		{
			RETAILMSG(1, (TEXT("DShowCamTest: RunCaptureGraph error=0x%08X\r\n"), hr));
			if (bHandleErr)
			{
				// handle error here instead of caller handling this
				HandleError(hr);
			}
		}
	}

	return hr;
}

HRESULT StopCam(BOOL bHandleErr/*=FALSE*/)
{
	HRESULT hr = S_OK;

	if (g_bGraphRunning)
	{
		RETAILMSG(1, (TEXT("DShowCamTest:StopCam calling StopCaptureGraph\r\n")));
		hr = StopGraph();

		// resume cam first so we are not left in rewind mode if we try to start again
		ResumeCam();

		if (hr==S_OK)
		{
			g_bGraphRunning = FALSE;

			// disable stop button
			if (g_hWndBtnStop!=NULL)
				EnableWindow(g_hWndBtnStop, FALSE);
			// enable play button
			if (g_hWndBtnPlay!=NULL)
				EnableWindow(g_hWndBtnPlay, TRUE);
			// disable pause button
			if (g_hWndBtnPause!=NULL)
				EnableWindow(g_hWndBtnPause, FALSE);
			// disable resume button
			if (g_hWndBtnResume!=NULL)
				EnableWindow(g_hWndBtnResume, FALSE);
			// disable rewind button
			if (g_hWndBtnRewind!=NULL)
				EnableWindow(g_hWndBtnRewind, FALSE);
			// disable fast forward button
			if (g_hWndBtnFastForward!=NULL)
				EnableWindow(g_hWndBtnFastForward, FALSE);
		}
		else
		{
			RETAILMSG(1, (TEXT("DShowCamTest: RunCaptureGraph error=0x%08X\r\n"), hr));
			if (bHandleErr)
			{
				// handle error here instead of caller handling this
				HandleError(hr);
			}
		}
	}

	return hr;
}

HRESULT PauseCam(BOOL bHandleErr/*=FALSE*/)
{
	HRESULT hr = S_OK;

	RETAILMSG(1, (TEXT("+DShowCamTest:PauseCam\r\n")));
	if (g_bGraphRunning)
	{
		hr = SendCamCommand(m_hEvtPause);
		if (hr==S_OK)
		{
			RETAILMSG(1, (TEXT("DShowCamTest: PauseCam success\r\n")));
			// disable pause button
			if (g_hWndBtnPause!=NULL)
				EnableWindow(g_hWndBtnPause, FALSE);
			// enable resume button
			if (g_hWndBtnResume!=NULL)
				EnableWindow(g_hWndBtnResume, TRUE);
			// enable rewind button
			if (g_hWndBtnRewind!=NULL)
				EnableWindow(g_hWndBtnRewind, TRUE);
			// enable fast forward button
			if (g_hWndBtnFastForward!=NULL)
				EnableWindow(g_hWndBtnFastForward, TRUE);
		}
		else
		{
			RETAILMSG(1, (TEXT("DShowCamTest: PauseCam error=0x%08X\r\n"), hr));
			if (bHandleErr)
			{
				// handle error here instead of caller handling this
				HandleError(hr);
			}
		}
	}

	return hr;
}

HRESULT ResumeCam(BOOL bHandleErr/*=FALSE*/)
{
	HRESULT hr = S_OK;

	RETAILMSG(1, (TEXT("+DShowCamTest:ResumeCam\r\n")));
	if (g_bGraphRunning)
	{
		hr = SendCamCommand(m_hEvtResume);
		if (hr==S_OK)
		{
			RETAILMSG(1, (TEXT("DShowCamTest: ResumeCam success\r\n")));

			// enable pause button
			if (g_hWndBtnPause!=NULL)
				EnableWindow(g_hWndBtnPause, TRUE);
			// disable resume button
			if (g_hWndBtnResume!=NULL)
				EnableWindow(g_hWndBtnResume, FALSE);
			// disable rewind button
			if (g_hWndBtnRewind!=NULL)
				EnableWindow(g_hWndBtnRewind, FALSE);
			// disable fast forward button
			if (g_hWndBtnFastForward!=NULL)
				EnableWindow(g_hWndBtnFastForward, FALSE);
		}
		else
		{
			RETAILMSG(1, (TEXT("DShowCamTest: ResumeCam error=0x%08X\r\n"), hr));
			if (bHandleErr)
			{
				// handle error here instead of caller handling this
				HandleError(hr);
			}
		}
	}

	return hr;
}

HRESULT RewindCam(BOOL bHandleErr/*=FALSE*/)
{
	HRESULT hr = S_OK;

	RETAILMSG(1, (TEXT("+DShowCamTest:RewindCam\r\n")));
	if (g_bGraphRunning)
	{
		hr = SendCamCommand(m_hEvtRewind);
		if (hr==S_OK)
		{
			RETAILMSG(1, (TEXT("DShowCamTest: RewindCam success\r\n")));
		}
		else
		{
			RETAILMSG(1, (TEXT("DShowCamTest: RewindCam error=0x%08X\r\n"), hr));
			if (bHandleErr)
			{
				// handle error here instead of caller handling this
				HandleError(hr);
			}
		}
	}

	return hr;
}

HRESULT FastForwardCam(BOOL bHandleErr/*=FALSE*/)
{
	HRESULT hr = S_OK;

	RETAILMSG(1, (TEXT("+DShowCamTest:FastForwardCam\r\n")));
	if (g_bGraphRunning)
	{
		hr = SendCamCommand(m_hEvtFastForward);
		if (hr==S_OK)
		{
			RETAILMSG(1, (TEXT("DShowCamTest: FastForwardCam success\r\n")));
		}
		else
		{
			RETAILMSG(1, (TEXT("DShowCamTest: FastForwardCam error=0x%08X\r\n"), hr));
			if (bHandleErr)
			{
				// handle error here instead of caller handling this
				HandleError(hr);
			}
		}
	}

	return hr;
}

BOOL InitCamControl(void)
{
	m_hEvtPause = CreateEvent(NULL, FALSE, FALSE, TEXT("USBCAM_PAUSE"));
	m_hEvtResume = CreateEvent(NULL, FALSE, FALSE, TEXT("USBCAM_RESUME"));
	m_hEvtRewind = CreateEvent(NULL, FALSE, FALSE, TEXT("USBCAM_REWIND"));
	m_hEvtFastForward = CreateEvent(NULL, FALSE, FALSE, TEXT("USBCAM_FASTFORWARD"));

	if ((m_hEvtPause!=NULL) && (m_hEvtResume!=NULL) && (m_hEvtRewind!=NULL) && (m_hEvtFastForward!=NULL))
		return TRUE;

	return FALSE;
}

BOOL DeInitCamControl(void)
{
	if (m_hEvtPause!=NULL)
	{
		CloseHandle(m_hEvtPause);
		m_hEvtPause = NULL;
	}
	if (m_hEvtResume!=NULL)
	{
		CloseHandle(m_hEvtResume);
		m_hEvtResume = NULL;
	}
	if (m_hEvtRewind!=NULL)
	{
		CloseHandle(m_hEvtRewind);
		m_hEvtRewind = NULL;
	}
	if (m_hEvtFastForward!=NULL)
	{
		CloseHandle(m_hEvtFastForward);
		m_hEvtFastForward = NULL;
	}

	return TRUE;
}

HRESULT SendCamCommand(HANDLE hEvt)
{
	RETAILMSG(1, (TEXT("DShowCamTest: SendCamCommand hEvt=0x%08X\r\n"), hEvt));
	if (hEvt!=NULL)
	{
		if (SetEvent(hEvt))
		{
			RETAILMSG(1, (TEXT("DShowCamTest: SendCamCommand SetEvent success\r\n")));
			return S_OK;
		}
		else
		{
			RETAILMSG(1, (TEXT("DShowCamTest: SendCamCommand SetEvent error=0x%08X\r\n"), GetLastError()));
		}
	}

	return E_FAIL;
}

