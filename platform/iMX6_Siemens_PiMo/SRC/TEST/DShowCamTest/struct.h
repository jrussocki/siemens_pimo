
//MACRO Definitions
//#define CAMERA_PREVIEW_WIDTH	160
//#define CAMERA_PREVIEW_HEIGHT	120
//#define CAMERA_PREVIEW_WIDTH	176
//#define CAMERA_PREVIEW_HEIGHT	144
//#define CAMERA_PREVIEW_WIDTH	320
//#define CAMERA_PREVIEW_HEIGHT	240
//#define CAMERA_PREVIEW_WIDTH	640
//#define CAMERA_PREVIEW_HEIGHT	480
//#define CAMERA_PREVIEW_WIDTH	480
//#define CAMERA_PREVIEW_HEIGHT	640

#define CAMERA_PREVIEW_WIDTH	640
#define CAMERA_PREVIEW_HEIGHT	480

//Global Enumerations
typedef enum
{
    MESSAGE_INFO=1,
    MESSAGE_ERROR,
    MESSAGE_ENDRECORDING,
    MESSAGE_FILECAPTURED
} DSHOW_MESSAGE;

//Global Structure definitions
typedef struct
{
    DSHOW_MESSAGE   dwMessage;
    WCHAR*  wzMessage;
} Message;

//Function Declarations
HRESULT BuildCaptureGraph(HWND);
HRESULT RunCaptureGraph();
HRESULT StopGraph();
HRESULT ReleaseAllDshowObjects();
//HRESULT SendCamMessage(PUCHAR pInBuf, DWORD InBufLen, PUCHAR pOutBuf, DWORD OutBufLen, PDWORD pdwBytesTransferred);
HRESULT SendCamMessage();


 