/**
 * @file    test_app_cantest.cpp
 *
 * @brief   CANTEST test application
 *
 * @copyright  All rights reserved Adeneo Embedded 2012
 *
 * @addtogroup TEST
 * @{
 * 
 */

/*** INCLUDES FILES **********************************************************/
#include <windows.h>

void Usage()
{
	wprintf(L"Usage : \r\n");
	wprintf(L"TEST_APP_SATA --create <filename> <size>\r\n");
	wprintf(L"  Creates a file with given name of given size, written values\r\n");
	wprintf(L"  are uint64, incremented on each written value. Size must be\r\n");
	wprintf(L"  a multiple of 8\r\n");
	wprintf(L"TEST_APP_SATA --check <filename>\r\n");
	wprintf(L"  Reads a file created with --create option and checks that read\r\n");
	wprintf(L"  values are correctly incremented UINT64 values\r\n");
	wprintf(L"TEST_APP_SATA --busyloop <durationMs>\r\n");
	wprintf(L"  Performs operations on all CPUs for the given duration\r\n");
	wprintf(L"TEST_APP_SATA --seek <filename> <count>\r\n");
	wprintf(L"  Reads a test file, created with --create switch\r\n");
	wprintf(L"  calls SetFilePointer with <count> offsets and checks\r\n");
	wprintf(L"  that read data are correctly incremented UINT64 values\r\n");
}

#define SEEK_BUF_SIZE_KB	512

// This function reads a file until its end and checks data
BOOL TestCheckSeek(TCHAR *filename, DWORD count)
{
	BOOL bRet = FALSE;
	HANDLE hFile = INVALID_HANDLE_VALUE;
	DWORD bufSize = 1024 * SEEK_BUF_SIZE_KB; // Read buffer size in bytes
	UINT64 *pBuf = NULL;
	BOOL bErr = FALSE;

	wprintf(L"%S: Using buffers of %d kB\r\n", __FUNCTION__, SEEK_BUF_SIZE_KB);

	hFile = CreateFile(filename, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);

	if (hFile == INVALID_HANDLE_VALUE) {
		wprintf(L"%S: Failed in CreateFile for file %s\r\n", __FUNCTION__, filename);
		goto _cleanup;
	}

	pBuf = (UINT64 *)LocalAlloc(LPTR, bufSize);

	if (pBuf == NULL) {
		wprintf(L"%S: Failed in LocalAlloc\r\n", __FUNCTION__);
		goto _cleanup;
	}

	DWORD  fileSizeLow;
	DWORD  fileSizeHigh;
	UINT64 fileSize;
	UINT64 seekInc;
	UINT64 readBytes = 0;

	fileSizeLow = GetFileSize(hFile, &fileSizeHigh);
	fileSize = ((UINT64)(fileSizeHigh)) << 32 | fileSizeLow;

	seekInc = fileSize / (count + 1);

	while (seekInc % 8 != 0) {
		seekInc++;
	}

	wprintf(L"%S: File %s has size %I64d, will seek with increment %I64u bytes\r\n", __FUNCTION__, filename, fileSize, seekInc);

	UINT64 index = 0; // Index of the UINT64 currently read
	//UINT64 pc = 0;
	//UINT64 prevPc = (UINT64)-1;
	DWORD tStart = GetTickCount();
	UINT64 seekPos = 0;
	//DWORD seekIndex = 0;
	//for (seekIndex = 0; seekIndex < count; seekIndex++) {
	while (seekPos < fileSize) {
		DWORD r;
		/*
		pc = 100 * index * sizeof(UINT64) / fileSize;
		if (pc != prevPc) {
			wprintf(L"%S: Reading offset %I64d of %I64d (%I64d %%), bufSize %d\r\n", __FUNCTION__, 
					index, fileSize, pc, bufSize);
			prevPc = pc;
		}
		*/
		memset(pBuf, 0, bufSize);
		r = 0;
		seekPos += seekInc;
		LONG lSeek = (LONG)seekPos;
		LONG hSeek = (LONG)(seekPos >> 32);
		wprintf(L"%S: Setting file pointer %I64u (0x%x 0x%x)\r\n", __FUNCTION__, 
				seekPos, lSeek, hSeek);
		if ((LONG)SetFilePointer(hFile, lSeek, &hSeek, FILE_BEGIN) != lSeek) {
			wprintf(L"%S: Failed in SetFilePointer for offset %I64u\r\n", __FUNCTION__,
				seekPos);
			goto _cleanup;
		}
		// Read a bunch of UINT64 values from file
		if (!ReadFile(hFile, (LPVOID)pBuf, bufSize, &r, NULL)) {
			RETAILMSG(1, (L"%S: Failed in ReadFile\r\n", __FUNCTION__));
			break;
		}

		readBytes += bufSize;
		
		index = seekPos / sizeof(UINT64);
		// Go through read buffer and check values
		for (UINT64 i = 0; i < (UINT64)r / sizeof(UINT64); i++) {
			if (pBuf[i] != (index) ) {
				//wprintf(L"%S: Found bad value at offset %I64d (%I64d), bufSize %d\r\n", __FUNCTION__,
				//		index, pBuf[i], bufSize);
				//wprintf(L"%S: i is %d\r\n", __FUNCTION__, i);
				wprintf(L"%S: Bad value, 0x%I64x (%I64u), expected 0x%I64x(%I64u)\r\n", __FUNCTION__,
						pBuf[i], pBuf[i], index, index);
				wprintf(L"%S: Offset in file is %I64u\r\n", __FUNCTION__, index);
				wprintf(L"%S: Offset in buffer is %d\r\n", __FUNCTION__, i);
				wprintf(L"%S: Dumping buffer content\r\n", __FUNCTION__);
				DWORD loop = 0;
				DWORD j = (DWORD)i*sizeof(UINT64);	// Go to wrong value
				// And try to dump values 32 bytes before wrong value
				if (j > 32) {
					j -= 32;
				}
				else {
					j = 0;
				}
				// Dump buffer
				for (;;) {
					if (j >= bufSize)
						break;
					if (loop > 30) // Don't dump more than XX bytes
						break;
					j++;
					loop++;
					wprintf(L"%S: Byte %d: 0x%x\r\n", __FUNCTION__, j, ((UCHAR *)(pBuf))[j]);
				}
				bErr = TRUE;
				goto _cleanup;
			}
			else {
				//wprintf(L"Got value %I64d at offset %I64d\r\n", pBuf[i], readBytes + i);
			}
			index++;
		}

		// We didn't read as many data as requested, assume this is the end of the file
		if (r < bufSize) {
			wprintf(L"%S: Reached end of file\r\n", __FUNCTION__);
			break;
		}

		// Set new position in file
		seekPos += seekInc;
	}

	DWORD tEnd = GetTickCount();

	wprintf(L"%S: Read %I64d bytes in %d ms (%f MB/s)\r\n", __FUNCTION__,
			readBytes, tEnd - tStart, (float)readBytes / (float)(tEnd - tStart) * 1000. / 1024. / 1024.);

	bRet = !bErr;

_cleanup :

	if (hFile != INVALID_HANDLE_VALUE) {
		CloseHandle(hFile);
	}

	if (pBuf != NULL) {
		LocalFree(pBuf);
	}

	if (bRet) {
		wprintf(L"%S: Test OK\r\n", __FUNCTION__);
	}
	else {
		wprintf(L"%S: Test failed !!\r\n", __FUNCTION__);
	}

	return bRet;
}

// 8 ok
// 16 ok
// 32 nok
// 64 nok
#define READ_BUF_SIZE_KB 512

// This function reads a file until its end and checks data
BOOL TestCheckFile(TCHAR *filename)
{
	BOOL bRet = FALSE;
	HANDLE hFile = INVALID_HANDLE_VALUE;
	DWORD bufSize = 1024 * READ_BUF_SIZE_KB; // Read buffer size in bytes
	UINT64 *pBuf = NULL;
	BOOL bErr = FALSE;

	wprintf(L"%S: Using buffers of %d kB\r\n", __FUNCTION__, READ_BUF_SIZE_KB);

	hFile = CreateFile(filename, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);

	if (hFile == INVALID_HANDLE_VALUE) {
		wprintf(L"%S: Failed in CreateFile for file %s\r\n", __FUNCTION__, filename);
		goto _cleanup;
	}

	pBuf = (UINT64 *)LocalAlloc(LPTR, bufSize);

	if (pBuf == NULL) {
		wprintf(L"%S: Failed in LocalAlloc\r\n", __FUNCTION__);
		goto _cleanup;
	}

	DWORD  fileSizeLow;
	DWORD  fileSizeHigh;
	UINT64 fileSize;

	fileSizeLow = GetFileSize(hFile, &fileSizeHigh);
	fileSize = ((UINT64)(fileSizeHigh)) << 32 | fileSizeLow;

	wprintf(L"%S: File %s has size %I64d\r\n", __FUNCTION__, filename, fileSize);

	UINT64 index = 0; // Index of the UINT64 currently read
	UINT64 pc = 0;
	UINT64 prevPc = (UINT64)-1;
	DWORD tStart = GetTickCount();
	for (;;) {
		DWORD r;
		pc = 100 * index * sizeof(UINT64) / fileSize;
		if (pc != prevPc) {
			wprintf(L"%S: Reading offset %I64d of %I64d (%I64d %%), bufSize %d\r\n", __FUNCTION__, 
					index, fileSize, pc, bufSize);
			prevPc = pc;
		}
		memset(pBuf, 0, bufSize);
		r = 0;
		// Read a bunch of UINT64 values from file
		if (!ReadFile(hFile, (LPVOID)pBuf, bufSize, &r, NULL)) {
			RETAILMSG(1, (L"%S: Failed in ReadFile\r\n", __FUNCTION__));
			break;
		}
		// Go through read buffer and check values
		for (UINT64 i = 0; i < (UINT64)r / sizeof(UINT64); i++) {
			if (pBuf[i] != (index) ) {
				//wprintf(L"%S: Found bad value at offset %I64d (%I64d), bufSize %d\r\n", __FUNCTION__,
				//		index, pBuf[i], bufSize);
				//wprintf(L"%S: i is %d\r\n", __FUNCTION__, i);
				wprintf(L"%S: Bad value, 0x%I64x (%I64u), expected 0x%I64x(%I64u)\r\n", __FUNCTION__,
						pBuf[i], pBuf[i], index, index);
				wprintf(L"%S: Offset in file is %I64u\r\n", __FUNCTION__, index);
				wprintf(L"%S: Offset in buffer is %d\r\n", __FUNCTION__, i);
				wprintf(L"%S: Dumping buffer content\r\n", __FUNCTION__);
				DWORD loop = 0;
				DWORD j = (DWORD)i*sizeof(UINT64);	// Go to wrong value
				// And try to dump values 32 bytes before wrong value
				if (j > 32) {
					j -= 32;
				}
				else {
					j = 0;
				}
				// Dump buffer
				for (;;) {
					if (j >= bufSize)
						break;
					if (loop > 30) // Don't dump more than XX bytes
						break;
					j++;
					loop++;
					wprintf(L"%S: Byte %d: 0x%x\r\n", __FUNCTION__, j, ((CHAR *)(pBuf))[j]);
				}
				bErr = TRUE;
				goto _cleanup;
			}
			else {
				//wprintf(L"Got value %I64d at offset %I64d\r\n", pBuf[i], readBytes + i);
			}
			index++;
		}

		// We didn't read as many data as requested, assume this is the end of the file
		if (r < bufSize) {
			wprintf(L"%S: Reached end of file\r\n", __FUNCTION__);
			break;
		}
	}

	DWORD tEnd = GetTickCount();

	wprintf(L"%S: Read %I64d bytes in %d ms (%f MB/s)\r\n", __FUNCTION__,
			fileSize, tEnd - tStart, (float)fileSize / (float)(tEnd - tStart) * 1000. / 1024. / 1024.);

	bRet = !bErr;

_cleanup :

	if (hFile != INVALID_HANDLE_VALUE) {
		CloseHandle(hFile);
	}

	if (pBuf != NULL) {
		LocalFree(pBuf);
	}

	if (bRet) {
		wprintf(L"%S: Test OK\r\n", __FUNCTION__);
	}
	else {
		wprintf(L"%S: Test failed !!\r\n", __FUNCTION__);
	}

	return bRet;
}

#define WRITE_BUF_SIZE_KB 256 // 128

BOOL TestCreateFile(TCHAR *filename, UINT64 size, BOOL bCorrupt)
{
	BOOL bRet = FALSE;
	HANDLE hFile = INVALID_HANDLE_VALUE;
	UINT64 written = 0;
	UINT64 bufSize = 256 * 1024;
	UINT64 *pBuf = NULL;

	wprintf(L"%S: Using buffers of %d kB\r\n", __FUNCTION__, WRITE_BUF_SIZE_KB);

	if ( (size % 8) != 0) {
		wprintf(L"%S: Size must be a multiple of 8\r\n", __FUNCTION__);
		goto _cleanup;
	}

	hFile = CreateFile(filename, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, 0, NULL);

	if (hFile == INVALID_HANDLE_VALUE) {
		wprintf(L"%S: Failed in CreateFile for file %s\r\n", __FUNCTION__, filename);
		goto _cleanup;
	}

	DWORD toWrite = (DWORD)(bufSize) * sizeof(INT64);
	pBuf = (UINT64 *)LocalAlloc(LPTR, toWrite);

	if (pBuf == NULL) {
		wprintf(L"%S: Failed in LocalAlloc\r\n", __FUNCTION__);
		goto _cleanup;
	}

	UINT64 index = 0;
	UINT64 pc = 0;
	UINT64 prevPc = (UINT64)-1;
	DWORD tStart = GetTickCount();
	while (written < size) {
		for (UINT64 i = 0; i < (UINT64)bufSize; i++) {
			pBuf[i] = index;
			index++;
		}
		DWORD w = 0;
		WriteFile(hFile, (LPVOID)pBuf, toWrite, &w, NULL);
		pc = 100 * written / size;
		if (pc != prevPc) {
			wprintf(L"%S: Writting byte %I64d of %I64d (%I64d %%)\r\n", __FUNCTION__,
					written, size, pc);
			prevPc = pc;
		}
		if (w != toWrite) {
			wprintf(L"%S: Failed in WriteFile (written %d instead of %d)\r\n",
					w, toWrite);
			goto _cleanup;
		}
		written += toWrite;
	}

	DWORD tEnd = GetTickCount();

	wprintf(L"%S: Written %I64d bytes in %d ms (%f MB/s)\r\n", __FUNCTION__,
			size, tEnd - tStart, size / (float)(tEnd - tStart) * 1000. / 1024. / 1024.);


	bRet = TRUE;

_cleanup :

	if (hFile != INVALID_HANDLE_VALUE) {
		CloseHandle(hFile);
	}

	if (pBuf != NULL) {
		LocalFree(pBuf);
	}

	return bRet;
}

DWORD cbBusyLoop(LPVOID arg)
{
	DWORD dwDuration = *((DWORD *)(arg));
	volatile calc = 2;

	wprintf(L"%S++\r\n", __FUNCTION__);

	DWORD dwStart = GetTickCount();

	while (GetTickCount() - dwStart < dwDuration) {
		calc *= 2;
	}

	wprintf(L"%S--\r\n", __FUNCTION__);

	return 0;
}

void TestBusyLoop(DWORD dwDuration)
{
	DWORD procsCount = CeGetTotalProcessors();
	HANDLE * phThreads = NULL;
	DWORD i;
	
	phThreads = (HANDLE *)LocalAlloc(LPTR, procsCount * sizeof(HANDLE));

	if (phThreads == NULL) {
		wprintf(L"%S: Failed in LocalAlloc for phThreads\r\n", __FUNCTION__);
		goto _cleanup;
	}

	for (i = 0; i < procsCount; i++) {
		phThreads[i] = NULL;
	}

	wprintf(L"Will create %d threads\r\n", procsCount);

	for (i = 0; i < procsCount; i++) {
		phThreads[i] = CreateThread(NULL, 0, cbBusyLoop, &dwDuration, 0, NULL);
		if (phThreads[i] == NULL) {
			wprintf(L"%S: Failed in CreateThread\r\n", __FUNCTION__);
			goto _cleanup;
		}
		Sleep(10);
	}

	for (;;) {
		wprintf(L"%S: %d ms remaining\r\n", __FUNCTION__, dwDuration);
		if (dwDuration < 1000) {
			Sleep(dwDuration);
			dwDuration = 0;
			break;
		}
		else {
			dwDuration -= 1000;
			Sleep(1000);
		}
	}

_cleanup :
	if (phThreads != NULL) {
		for (DWORD i = 0; i < procsCount; i++) {
			if (phThreads[i] != NULL) {
				wprintf(L"Waiting for thread %d\r\n", i);
				WaitForSingleObject(phThreads[i], INFINITE);
				CloseHandle(phThreads[i]);
			}
		}
		LocalFree(phThreads);
	}

	return;
}

int _tmain(int argc, TCHAR *argv[])
{
	if ( (argc == 4) && wcscmp(argv[1], L"--create") == 0) {
		wprintf(L"Will test file creation\r\n");
		TCHAR *filename = argv[2];
		UINT64 size = (UINT64)_wtoi64(argv[3]);
		TestCreateFile(filename, size, FALSE);
	}
	else if ( (argc == 3) && wcscmp(argv[1], L"--check") == 0) {
		wprintf(L"Will check file created\r\n");
		TCHAR *filename = argv[2];
		TestCheckFile(filename);
	}
	else if ( (argc == 3) && wcscmp(argv[1], L"--busyloop") == 0) {
		DWORD dwDuration = _wtoi(argv[2]);
		wprintf(L"Will occupy CPUs for %d ms\r\n", dwDuration);
		TestBusyLoop(dwDuration);
	}
	else if ( (argc == 4) && wcscmp(argv[1], L"--seek") == 0) {
		TCHAR *filename = argv[2];
		DWORD count = _wtoi(argv[3]);
		wprintf(L"Will test SetFilePointer on %d positions\r\n", filename);
		TestCheckSeek(filename, count);
	}
	else {
		wprintf(L"Invalid arguments\r\n");
		Usage();
	}

}