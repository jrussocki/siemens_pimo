//-----------------------------------------------------------------------------
// 
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES.
//
//-----------------------------------------------------------------------------
//
// pwmapp.cpp
// This application demonstrates how to use the pwm driver.
// See Usage() below for arguments, options, etc.
// 
//-----------------------------------------------------------------------------
#include <windows.h>
#include "acc.h"

//-----------------------------------------------------------------------------
// Defines
//-----------------------------------------------------------------------------
#define dprintf NKDbgPrintfW
#define ACC_LOG                 TRUE

#define ACC1_DEVICE_NAME        _T("ACC1:")

// IOCTL_MACROS
#define ACC_MACRO_ENABLE(hDev) \
    (DeviceIoControl(hDev, IOCTL_ACC_ENABLE, NULL, 0, NULL, 0, NULL, NULL))

#define ACC_MACRO_DISABLE(hDev, eMode) \
    (DeviceIoControl(hDev, IOCTL_ACC_DISABLE, (PBYTE) &eMode, sizeof(eMode), NULL, 0, NULL, NULL))

/**********************SET command ***********************************/
#define ACC_MACRO_SET_MODE(hDev, eMode) \
    (DeviceIoControl(hDev, IOCTL_ACC_SET_MODE, (PBYTE) &eMode, sizeof(eMode), NULL, 0, NULL, NULL))

#define ACC_MACRO_SET_GSEL(hDev, eGSel) \
    (DeviceIoControl(hDev, IOCTL_ACC_SET_GSEL, (PBYTE) &eGSel, sizeof(eGSel), NULL, 0, NULL, NULL))

#define ACC_MACRO_SET_PLEN(hDev, fEnable) \
    (DeviceIoControl(hDev, IOCTL_ACC_SET_PLEN, (PBYTE) &fEnable, sizeof(fEnable), NULL, 0, NULL, NULL))

#define ACC_MACRO_SET_DR(hDev, eDR) \
    (DeviceIoControl(hDev, IOCTL_ACC_SET_DR, (PBYTE) &eDR, sizeof(eDR), NULL, 0, NULL, NULL))

// Note: Calibration is done only when accelerometer starts
#define ACC_MACRO_SET_CALIBRATION(hDev) \
    (DeviceIoControl(hDev, IOCTL_ACC_SET_CALIBRATION, NULL, 0, NULL, 0, NULL, NULL))

#define ACC_MACRO_SET_PLCOUNT(hDev, bPLCount) \
    (DeviceIoControl(hDev, IOCTL_ACC_SET_PLCOUNT, (PBYTE) &bPLCount, sizeof(bPLCount), NULL, 0, NULL, NULL))

#define ACC_MACRO_SET_OUTPUTWIDTH(hDev, eOutWid) \
    (DeviceIoControl(hDev, IOCTL_ACC_SET_OUTPUTWIDTH, (PBYTE) &eOutWid, sizeof(eOutWid), NULL, 0, NULL, NULL))

#define ACC_MACRO_SET_SET_XOFFSET(hDev, bOffset) \
    (DeviceIoControl(hDev, IOCTL_ACC_SET_XOFFSET, (PBYTE) &bOffset, sizeof(bOffset), NULL, 0, NULL, NULL))

#define ACC_MACRO_SET_YOFFSET(hDev, bOffset) \
    (DeviceIoControl(hDev, IOCTL_ACC_SET_YOFFSET, (PBYTE) &bOffset, sizeof(bOffset), NULL, 0, NULL, NULL))

#define ACC_MACRO_SET_ZOFFSET(hDev, bOffset) \
    (DeviceIoControl(hDev, IOCTL_ACC_SET_ZOFFSET, (PBYTE) &bOffset, sizeof(bOffset), NULL, 0, NULL, NULL))

/**************************GET command *********************************/
#define ACC_MACRO_GET_PLSTATUS(hDev, eStatus) \
    (DeviceIoControl(hDev, IOCTL_ACC_GET_PLSTATUS, NULL, 0, (PBYTE) &eStatus, sizeof(eStatus), NULL, NULL))

#define ACC_MACRO_GET_OUTPUT(hDev, eOutput) \
    (DeviceIoControl(hDev, IOCTL_ACC_GET_OUTPUT, NULL, 0, (PBYTE) &eOutput, sizeof(eOutput), NULL, NULL))

#define ACC_MACRO_GET_MODE(hDev, eMode) \
    (DeviceIoControl(hDev, IOCTL_ACC_GET_MODE, NULL, 0, (PBYTE) &eMode, sizeof(eMode), NULL, NULL))

#define ACC_MACRO_GET_GSEL(hDev, eGSel) \
    (DeviceIoControl(hDev, IOCTL_ACC_GET_GSEL, NULL, 0, (PBYTE) &eGSel, sizeof(eGSel), NULL, NULL))

#define ACC_MACRO_GET_OUTPUTWIDTH(hDev, eOutWid) \
    (DeviceIoControl(hDev, IOCTL_ACC_GET_OUTPUTWIDTH, NULL, 0, (PBYTE) &eOutWid, sizeof(eOutWid), NULL, NULL))

#define ACC_MACRO_GET_DR(hDev, eDR) \
    (DeviceIoControl(hDev, IOCTL_ACC_GET_DR, NULL, 0, (PBYTE) &eDR, sizeof(eDR), NULL, NULL))

#define ACC_MACRO_GET_XOFFSET(hDev, bOffset) \
    (DeviceIoControl(hDev, IOCTL_ACC_GET_XOFFSET, NULL, 0, (PBYTE) &bOffset, sizeof(bOffset), NULL, NULL))

#define ACC_MACRO_GET_YOFFSET(hDev, bOffset) \
    (DeviceIoControl(hDev, IOCTL_ACC_GET_YOFFSET, NULL, 0, (PBYTE) &bOffset, sizeof(bOffset), NULL, NULL))

#define ACC_MACRO_GET_ZOFFSET(hDev, bOffset) \
    (DeviceIoControl(hDev, IOCTL_ACC_GET_ZOFFSET, NULL, 0, (PBYTE) &bOffset, sizeof(bOffset), NULL, NULL))

#define ACC_MACRO_GET_STATUS(hDev, eStatus) \
    (DeviceIoControl(hDev, IOCTL_ACC_GET_STATUS, NULL, 0, (PBYTE) &eStatus, sizeof(eStatus), NULL, NULL))

//-----------------------------------------------------------------------------
// Variables
//-----------------------------------------------------------------------------
static HANDLE hACC;

void EnableACC(LPCWSTR devName, ACC_MODE eMode)
{
    ACC_MACRO_SET_MODE(hACC, eMode);

    ACC_MACRO_ENABLE(hACC);

    RETAILMSG(ACC_LOG, (_T("%s::EnableACC\r\n"), devName));
}

void DisableACC(LPCWSTR devName, ACC_MODE eMode)
{
    ACC_MACRO_DISABLE(hACC, eMode);

    RETAILMSG(ACC_LOG, (_T("%s::DisableACC\r\n"), devName));
}

int ACCOpen(LPCWSTR devName)
{
    // Create ACC file handle
    hACC = CreateFile(devName, 
        GENERIC_READ|GENERIC_WRITE, 
        FILE_SHARE_READ|FILE_SHARE_WRITE, 
        NULL, 
        OPEN_EXISTING, 
        FILE_FLAG_RANDOM_ACCESS, 
        NULL); 

    dprintf(TEXT("Create ACC file %s\r\n"),devName );

    if (hACC == INVALID_HANDLE_VALUE)
    {
        dprintf(TEXT("Create ACC file handle failed \r\n"));
        return -1;
    }

    return 1;
}

void ACCClose()
{
    // Close the PWM file handle
    if (hACC != NULL)
    {
        CloseHandle(hACC);
    }
}

//-----------------------------------------------------------------------------
// TestFunc
//
// function for PWM test
//-----------------------------------------------------------------------------
void TestFunc(void)
{
    ACC_OUTPUT          eOutput;
    ACC_MODE            eMode;

    // Test ACC1
    if (ACCOpen(ACC1_DEVICE_NAME) >= 0)
    {
        EnableACC(ACC1_DEVICE_NAME, ACC_MODE_STREAMING);

        for (int i = 0; i < 100; i++)
        {
            ACC_MACRO_GET_OUTPUT(hACC, eOutput);

            if (0 == eOutput.eOutputWidth)
            {
                RETAILMSG(ACC_LOG, (_T("Output witdth: 8-bit\r\n")));
                RETAILMSG(ACC_LOG, (_T("X: 0x%X\r\n"), eOutput.iXOutput8));
                RETAILMSG(ACC_LOG, (_T("Y: 0x%X\r\n"), eOutput.iYOutput8));
                RETAILMSG(ACC_LOG, (_T("Z: 0x%X\r\n\n\n"), eOutput.iZOutput8));
            }
            else if (1 == eOutput.eOutputWidth)
            {
                RETAILMSG(ACC_LOG, (_T("Output witdth: 14-bit\r\n")));
                RETAILMSG(ACC_LOG, (_T("X: 0x%X\r\n"), eOutput.iXOutput14));
                RETAILMSG(ACC_LOG, (_T("Y: 0x%X\r\n"), eOutput.iYOutput14));
                RETAILMSG(ACC_LOG, (_T("Z: 0x%X\r\n\n\n"), eOutput.iZOutput14));
            }
            else
            {
                RETAILMSG(ACC_LOG, (_T("ERROR: undefinded Output witdth: 0x%X\r\n"), eOutput.eOutputWidth));
            }

            Sleep(100);
        }

        ACC_MACRO_GET_MODE(hACC, eMode);
        DisableACC(ACC1_DEVICE_NAME, eMode);
        
        ACCClose();
    }
}

//-----------------------------------------------------------------------------
// WinMain
//
// The main function
//-----------------------------------------------------------------------------
int WINAPI WinMain ( HINSTANCE hInstance, HINSTANCE hPrevInstance, 
    LPTSTR pCmdLine, int nCmdShow)
{
    UNREFERENCED_PARAMETER(hInstance);
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(nCmdShow);

    dprintf(TEXT("test_accelerometer enter.\r\n"));

    TestFunc();

    dprintf(TEXT("test_accelerometer quit.\r\n"));

    return 0;
}
