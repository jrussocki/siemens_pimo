//
//  Copyright (c) Microsoft Corporation.  All rights reserved.
//
//  Use of this source code is subject to the terms of the Microsoft end-user
//  license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
//  If you did not accept the terms of the EULA, you are not authorized to use
//  this source code. For a copy of the EULA, please see the LICENSE.RTF on your
//  install media.
//
//------------------------------------------------------------------------------
//
//  Copyright (C) 2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT 
//
//------------------------------------------------------------------------------
//
//  File:  pcitest.cpp
//
//  This module is designed to test the imx6 PCI bus driver with an E1000E intel
//  netword device.
//
//------------------------------------------------------------------------------

#pragma warning(push)
#pragma warning(disable: 4115 4201 4204 4214)
#include <windows.h>
#include <Devload.h>
#include <windev.h>
#include <ceddk.h>
#include <ddkreg.h>
#include <giisr.h>
#pragma warning(pop)

#include "bsp.h"

#include <common_pcie.h>
#include "pcitest.h"

//------------------------------------------------------------------------------
// External Functions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// External Variables
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Defines
//------------------------------------------------------------------------------

DBGPARAM dpCurSettings = {
    TEXT("PciTest"), {
        TEXT("Init"),TEXT("Deinit"),TEXT("Open"),TEXT("Close"),
        TEXT("IOCtl"),TEXT("Thread"),TEXT(""),TEXT(""),
        TEXT(""),TEXT(""),TEXT(""),TEXT(""),
        TEXT(""),TEXT("Function"),TEXT("Warning"),TEXT("Error") },
		0
};

//------------------------------------------------------------------------------
// Types
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Global Variables
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Local Variables
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Local Functions
//------------------------------------------------------------------------------

DWORD cbIstThread(PVOID ptr);
VOID DumpDeviceStatus(PPCITEST_CONTEXT pDeviceContext);

//-----------------------------------------------------------------------------
//
// Function: PCT_Init
//
// The Device Manager calls this function as a result of a call to the
//      ActivateDevice() function.
//
// Parameters:
//      pContext
//          [in] Pointer to a string containing the registry path to the
//                active key for the stream interface driver.
//
// Returns:
//      Returns a handle to the device context created if successful. Returns
//      zero if not successful.
//
//-----------------------------------------------------------------------------
DWORD PCT_Init(LPCTSTR pContext)
{    
    HKEY hConfig = NULL;
    DWORD dwRet = 0;
    DDKISRINFO dii;
    DDKWINDOWINFO dwi;
    DDKPCIINFO dpi;
	TCHAR IsrDll[MAX_PATH];
	DWORD cchIsrDll = MAX_PATH;
	TCHAR IsrHandler[MAX_PATH];
	DWORD cchIsrHandler = MAX_PATH;
	PHYSICAL_ADDRESS phyAddr;
	ULONG space;
	HANDLE hPCIBus;

	PPCITEST_CONTEXT pDeviceContext;

	// Allocate device context structure
	pDeviceContext = (PPCITEST_CONTEXT)LocalAlloc(LPTR, sizeof(PCITEST_CONTEXT));

	// Obtain an handle on parent bus
	hPCIBus = CreateBusAccessHandle(pContext);
	if (hPCIBus == NULL) {
		RETAILMSG(1, (L"%S: Failed in CreateBusAccessHandle\r\n", __FUNCTION__));
		goto _exit;
	}

    // get a pointer to our configuration key in the registry
    hConfig = OpenDeviceKey(pContext);
    if (hConfig == NULL)
    {
		RETAILMSG(1, (L"%S: Failed in OpenDeviceKey\r\n", __FUNCTION__));
        goto _exit;
    }

    // read window configuration from the registry
    dwi.cbSize = sizeof(dwi);
    dwRet = DDKReg_GetWindowInfo(hConfig, &dwi);
    if (dwRet != ERROR_SUCCESS)
    {
		RETAILMSG(1, (L"%S: Failed in DDKReg_GetWindowInfo\r\n", __FUNCTION__));
        goto _exit;
    }

    // get ISR configuration information
    dii.cbSize = sizeof(dii);
    dwRet = DDKReg_GetIsrInfo(hConfig, &dii);
    if (dwRet != ERROR_SUCCESS)
    {
		RETAILMSG(1, (L"%S: Failed in DDKReg_GetIsrInfo\r\n", __FUNCTION__));
        goto _exit;
    }
    else if (dii.dwSysintr == SYSINTR_NOP)
    {
		RETAILMSG(1, (L"%S: Got sysintr %d (SYSINTR_NOP)!\r\n", __FUNCTION__, dii.dwSysintr));
        goto _exit;
    }
    else
    {
        // get installable ISR information
        if (dii.szIsrDll[0] != 0)
        {
            if (dii.szIsrHandler[0] == 0 || dii.dwIrq == IRQ_UNSPECIFIED)
            {
				RETAILMSG(1, (L"%S: garbled or missing installable ISR settings\r\n", __FUNCTION__));
                goto _exit;
            }
            else
            {
                if (FAILED(StringCchCopy(IsrDll, cchIsrDll, dii.szIsrDll)))
                {
					RETAILMSG(1, (L"%S: Failed in StringCchCopy(1)\r\n", __FUNCTION__));
                    goto _exit;
                }
                if (FAILED(StringCchCopy(IsrHandler, cchIsrHandler, dii.szIsrHandler)))
                {
					RETAILMSG(1, (L"%S: Failed in StringCchCopy(2)\r\n", __FUNCTION__));
                    goto _exit;
                }
            }
        }

		RETAILMSG(1, (L"%S: Got IRQ information, IsrDll is %s, IsrHandler is %s\r\n", __FUNCTION__,
					 IsrDll, IsrHandler));

        // was an IRQ specified?
        if (dii.dwIrq != IRQ_UNSPECIFIED)
        {
			//*Irq = dii.dwIrq;
			RETAILMSG(1, (L"%S: Using IRQ 0x%x\r\n", __FUNCTION__, dii.dwIrq));
        }

		pDeviceContext->dwIrq = dii.dwIrq;
		pDeviceContext->dwSysIntr = dii.dwSysintr;
    }

    // get device ID information
    dpi.cbSize = sizeof(dpi);
    dwRet = DDKReg_GetPciInfo(hConfig, &dpi);
    if (dwRet != ERROR_SUCCESS)
    {
		RETAILMSG(1, (L"%S: Failed in DDKReg_GetPciInfo\r\n", __FUNCTION__));
        goto _exit;
    }
    else if ((dpi.dwWhichIds & (PCIIDM_DEVICEID | PCIIDM_REVISIONID)) != (PCIIDM_DEVICEID | PCIIDM_REVISIONID))
    {
		RETAILMSG(1, (L"%S: missing device or revision ID\r\n", __FUNCTION__));
        goto _exit;
    }
    else
    {
		RETAILMSG(1, (L"%S: Device ID 0x%x, rev 0x%x\r\n", __FUNCTION__,
					  dpi.idVals[PCIID_DEVICEID], dpi.idVals[PCIID_REVISIONID]));
    }

	//
	// Initialize interruption
	//

	pDeviceContext->hIstEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	if (pDeviceContext->hIstEvent == NULL) 
	{
		RETAILMSG(1, (L"%S: Failed in CreateEvent for IST\r\n", __FUNCTION__));
		goto _exit;
	}

	if (!InterruptInitialize(pDeviceContext->dwSysIntr, pDeviceContext->hIstEvent, NULL, 0)) 
	{
		RETAILMSG(1, (L"%S: Failed in InterruptInitialize\r\n", __FUNCTION__));
		goto _exit;
	}

	pDeviceContext->bStopped = FALSE;
	pDeviceContext->hIstThread = CreateThread(NULL,
											  0,
											  cbIstThread,
											  pDeviceContext,
											  0,
											  NULL);
	if (pDeviceContext->hIstThread == NULL) 
	{
		RETAILMSG(1, (L"%S: Failed in CreateThread for IST\r\n", __FUNCTION__));
		goto _exit;
	}

	// Sleep (just to keep messages clean)
	Sleep(100);

	// Load installable ISR
	pDeviceContext->hIsr = LoadIntChainHandler(IsrDll, IsrHandler, (BYTE)pDeviceContext->dwIrq);
	if (pDeviceContext->hIsr == NULL)
	{
		DWORD dwErr = GetLastError();
		RETAILMSG(1, (L"%S: Failed in LoadIntChainHandler, error %d\r\n", __FUNCTION__, dwErr));
		goto _exit;
	}

	// Obtain a static address on device interrupt cause register, so chain handler can check value
	// from here to determine if device triggered the (shared) interrupt
	space = 0; // Memory space
	phyAddr.QuadPart = dwi.memWindows[0].dwBase + 0xC0; // ICR is at offset 0xC0 from mem space 0
	LPVOID isrPortAddr; // Static address obtained and passed to chain handler
	if (!BusTransBusAddrToStatic(hPCIBus,
								 PCIBus,
								 dwi.dwBusNumber,
								 phyAddr,
								 sizeof(UINT32),
								 &space,
								 &isrPortAddr)) {
		RETAILMSG(1, (L"%S: Failed in BusTransBusAddrToStatic !\r\n", __FUNCTION__));
		goto _exit;
	}

	GIISR_INFO isrInfo;
	isrInfo.SysIntr = pDeviceContext->dwSysIntr;
	isrInfo.CheckPort = TRUE;
	isrInfo.PortIsIO = FALSE;
	isrInfo.UseMaskReg = FALSE;
	isrInfo.PortAddr = (DWORD)isrPortAddr;
	isrInfo.PortSize = sizeof(UINT32);
	isrInfo.Mask = 0x1U << 31; // Interrupt asserted bit is bit 31

	if (!KernelLibIoControl(pDeviceContext->hIsr,
							IOCTL_GIISR_INFO,
							&isrInfo,
							sizeof(isrInfo),
							NULL,
							0,
							NULL)) {
		RETAILMSG(1, (L"%S: Failed in IOCTL_GIISR_INFO !\r\n", __FUNCTION__));
	}

	//
	// Map memory space
	//

	DWORD region;

	for (region = 0; region < dwi.dwNumIoWindows; region++) {
		RETAILMSG(1, (L"%S: IO region %d, start 0x%x, length %d\r\n", __FUNCTION__,
					region,
					dwi.ioWindows[region].dwBase,
					dwi.ioWindows[region].dwLen));
	}

	for (region = 0; region < dwi.dwNumMemWindows; region++) {
		RETAILMSG(1, (L"%S: Memory region %d, start 0x%x, length %d\r\n", __FUNCTION__,
					region,
					dwi.memWindows[region].dwBase,
					dwi.memWindows[region].dwLen));
	}

	UINT32 *pMem0;
	UINT32 *pMem1;
	UINT32 *pMem2;
	UINT32 *pIo0;

	phyAddr.QuadPart = dwi.memWindows[0].dwBase;
	space = 0; // Mem
	if (!BusTransBusAddrToVirtual(hPCIBus,
								  PCIBus,
								  dwi.dwBusNumber,
								  phyAddr,
								  dwi.memWindows[0].dwLen,
								  &space,
								  (PPVOID)&pMem0)) {
		RETAILMSG(1, (L"Failed in BusTransBusAddrToVirtual for mem0\r\n"));
		goto _exit;
	}

	phyAddr.QuadPart = dwi.memWindows[1].dwBase;
	space = 0; // Mem
	if (!BusTransBusAddrToVirtual(hPCIBus,
								  PCIBus,
								  0,
								  phyAddr,
								  dwi.memWindows[1].dwLen,
								  &space,
								  (PPVOID)&pMem1)) {
		RETAILMSG(1, (L"Failed in BusTransBusAddrToVirtual for mem1\r\n"));
		goto _exit;
	}

	phyAddr.QuadPart = dwi.memWindows[2].dwBase;
	space = 0; // Mem
	if (!BusTransBusAddrToVirtual(hPCIBus,
								  PCIBus,
								  0,
								  phyAddr,
								  dwi.memWindows[2].dwLen,
								  &space,
								  (PPVOID)&pMem2)) {
		RETAILMSG(1, (L"Failed in BusTransBusAddrToVirtual for mem2\r\n"));
		goto _exit;
	}

	phyAddr.QuadPart = dwi.ioWindows[0].dwBase;
	space = 1; // Io
	if (!BusTransBusAddrToVirtual(hPCIBus,
								  PCIBus,
								  0,
								  phyAddr,
								  dwi.ioWindows[0].dwLen,
								  &space,
								  (PPVOID)&pIo0)) {
		RETAILMSG(1, (L"Failed in BusTransBusAddrToVirtual for io0\r\n"));
		goto _exit;
	}
	// Io address is not mapped by BusTransBusAddrToVirtual, do it..
	phyAddr.QuadPart = (UINT32)pIo0;
	pIo0 = (UINT32 *)MmMapIoSpace(phyAddr, dwi.ioWindows[0].dwLen, FALSE);

	// For testing, we only need first memory space
	pDeviceContext->pGR = (PE1000E_GR)pMem0;

	// Disable all interrupts
	OUTREG32(&pDeviceContext->pGR->IMC, 0xFFFFFFFF);

	// Reset device
	UINT32 ctrl;
	ctrl = INREG32(&pDeviceContext->pGR->CTRL);
	OUTREG32(&pDeviceContext->pGR->CTRL, ctrl | E1000E_GR_CTRL_RST_MASK);

	// Wait until reset is over
	for (;;) {
		ctrl = INREG32(&pDeviceContext->pGR->CTRL);
		if (!(ctrl & E1000E_GR_CTRL_RST_MASK)) {
			break;
		}
		Sleep(10);
	}

	//
	RETAILMSG(1, (L"%S: Reset OK\r\n", __FUNCTION__));

	// Disable all interrupts again
	OUTREG32(&pDeviceContext->pGR->IMC, 0xFFFFFFFF);

	// Clear interrupt mask
	OUTREG32(&pDeviceContext->pGR->ICR, 0xFFFFFFFF);

	// Enable Link Change interrupt
	OUTREG32(&pDeviceContext->pGR->IMS, E1000E_GR_IRQ_LSC_MASK);

	// Enable link up
	ctrl |= E1000E_GR_CTRL_SLU_MASK;
	OUTREG32(&pDeviceContext->pGR->CTRL, ctrl);

	// Wait until link is up. We should just end init here, but since PCIe stable is *very* unstable,
	// we keep displaying a message here to check that PCIe link is still alive. This loop will
	// disappear with a more stable hardware
	RETAILMSG(1, (L"%S: Waiting for link up indication\r\n", __FUNCTION__));

	DWORD tick = GetTickCount();

	for (;;) {
		UINT32 status = INREG32(&pDeviceContext->pGR->STATUS);
		RETAILMSG(1, (L"%S: Got status register 0x%x, %d ms elapsed\r\n", __FUNCTION__, status,
			GetTickCount() - tick));
		if (status & E1000E_STATUS_LU_MASK) {
			break;
		}
		Sleep(300);
	}

_exit:
    if (hConfig != NULL)
    {
        RegCloseKey(hConfig);
    }

	RETAILMSG(1, (L"%S--\r\n", __FUNCTION__));
	
	// Otherwise return the created instance
    return (DWORD)pContext;
}


//-----------------------------------------------------------------------------
//
// Function: PTC_Deinit
//
// This function uninitializes a device.
//
// Parameters:
//      hDeviceContext
//          [in] Handle to the device context.
//
// Returns:
//      TRUE indicates success. FALSE indicates failure.
//
//-----------------------------------------------------------------------------
BOOL PCT_Deinit(DWORD hDeviceContext)
{
	PPCITEST_CONTEXT pDeviceContext = (PPCITEST_CONTEXT)hDeviceContext;

	RETAILMSG(1, (L"%S++\r\n", __FUNCTION__));

	if (pDeviceContext == NULL)
		return TRUE;
	
	if (pDeviceContext->hIstThread == NULL) {
		return TRUE;
	}

	if (pDeviceContext->hIstEvent == NULL) {
		return TRUE;
	}

	// Stop and wait for ist thread
	pDeviceContext->bStopped = TRUE;
	SetEvent(pDeviceContext->hIstEvent);
	WaitForSingleObject(pDeviceContext->hIstThread, 10000);

    RETAILMSG(1, (L"%S--\r\n", __FUNCTION__));

    return TRUE;
}


//-----------------------------------------------------------------------------
//
// Function: PCT_Open
//
// This function opens a device for reading, writing, or both.
//
// Parameters:
//      hDeviceContext
//          [in] Handle to the device context. The XXX_Init function creates
//                and returns this handle.
//      AccessCode
//          [in] Access code for the device. The access is a combination of
//                read and write access from CreateFile.
//      ShareMode
//          [in] File share mode of the device. The share mode is a
//                combination of read and write access sharing from CreateFile.
//
// Returns:
//      This function returns a handle that identifies the open context of
//      the device to the calling application.
//
//-----------------------------------------------------------------------------
DWORD PCT_Open(DWORD hDeviceContext, DWORD AccessCode, DWORD ShareMode)
{
	RETAILMSG(1, (L"%S++\r\n", __FUNCTION__));

    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(AccessCode);
    UNREFERENCED_PARAMETER(ShareMode);
    
	RETAILMSG(1, (L"%S--\r\n", __FUNCTION__));

	return hDeviceContext;
}

//-----------------------------------------------------------------------------
//
// Function: PCT_Close
//
// This function opens a device for reading, writing, or both.
//
// Parameters:
//      hOpenContext
//          [in] Handle returned by the XXX_Open function, used to identify
//                the open context of the device.
//
// Returns:  
//      TRUE indicates success. FALSE indicates failure.
//
//-----------------------------------------------------------------------------
BOOL PCT_Close(DWORD hOpenContext)
{
	RETAILMSG(1, (L"%S++\r\n", __FUNCTION__));

	RETAILMSG(1, (L"%S--\r\n", __FUNCTION__));
	
	return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function: PCT_IOControl
//
// This function sends a command to a device.
//
// Parameters:
//      hOpenContext 
//          [in] Handle to the open context of the device. The XXX_Open 
//                function creates and returns this identifier.
//      dwCode 
//          [in] I/O control operation to perform. These codes are 
//                device-specific and are usually exposed to developers through 
//                a header file.
//      pBufIn 
//          [in] Pointer to the buffer containing data to transfer to the 
//                device. 
//      dwLenIn 
//         [in] Number of bytes of data in the buffer specified for pBufIn.
//
//      pBufOut 
//          [out] Pointer to the buffer used to transfer the output data 
//                  from the device.
//      dwLenOut 
//          [in] Maximum number of bytes in the buffer specified by pBufOut.
//
//      pdwActualOut 
//          [out] Pointer to the DWORD buffer that this function uses to 
//                  return the actual number of bytes received from the device.
//
// Returns:  
//      The new data pointer for the device indicates success. A value of -1 
//      indicates failure.
//
//-----------------------------------------------------------------------------
BOOL PCT_IOControl(DWORD hOpenContext, DWORD dwCode, PBYTE pBufIn, 
                   DWORD dwLenIn, PBYTE pBufOut, DWORD dwLenOut,
                   PDWORD pdwActualOut)
{
    BOOL bRet = FALSE;

    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(hOpenContext);
    UNREFERENCED_PARAMETER(pdwActualOut);

    switch (dwCode)
    {
		case 0 :
		default:
        {
			RETAILMSG(1, (L"%S: Unsupported IOControl %d\r\n", dwCode));
            bRet = FALSE;
            break;
        }
    }

    return bRet;
}

//-----------------------------------------------------------------------------
//
// Function: PCT_DllEntry
//
// This function is called when the driver is initialized
//
// Returns:  
//      TRUE indicates success. FALSE indicates failure.
//
//-----------------------------------------------------------------------------
BOOL WINAPI PCT_DllEntry(HANDLE hInstDll, DWORD dwReason, LPVOID lpvReserved)
{
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(lpvReserved);

    switch (dwReason) {
        case DLL_PROCESS_ATTACH:
            DisableThreadLibraryCalls((HMODULE) hInstDll);
            break;
         
        case DLL_PROCESS_DETACH:
			break;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function: cbIstThread
//
// This function is the interrupt thread entry point
//
// Returns:  
//	 0
//
//-----------------------------------------------------------------------------
DWORD cbIstThread(PVOID ptr)
{
	PPCITEST_CONTEXT pDeviceContext = (PPCITEST_CONTEXT)ptr;
	DWORD dwRes;

	RETAILMSG(1, (L"%S++\r\n", __FUNCTION__));

	for (;;) 
	{
		dwRes = WaitForSingleObject(pDeviceContext->hIstEvent, 2000);

		if (pDeviceContext->bStopped) {
			RETAILMSG(1, (L"%S: bStopped is set, leaving IST thread\r\n", __FUNCTION__));
			break;
		}

		if (dwRes == WAIT_TIMEOUT) {
			continue;
		}
		if (dwRes != WAIT_OBJECT_0) {
			RETAILMSG(1, (L"%S: Error waiting for interruption: %d\r\n", __FUNCTION__, dwRes));
			break;
		}

		// Interrupt status is automatically reset when read by chain handler,
		// nothing to do to clea

		RETAILMSG(1, (L"%S: Got an interruption\r\n", __FUNCTION__));
		DumpDeviceStatus(pDeviceContext);

		InterruptDone(pDeviceContext->dwSysIntr);
	}

	RETAILMSG(1, (L"%S--\r\n", __FUNCTION__));

	return 0;
}

//-----------------------------------------------------------------------------
//
// Function: DumpDeviceStatus
//
// This function displays the link status
//
// Returns:  
//	 None
//
//-----------------------------------------------------------------------------
VOID DumpDeviceStatus(PPCITEST_CONTEXT pDeviceContext)
{
	UINT32 status = INREG32(&pDeviceContext->pGR->STATUS);
	UINT32 speed;

	RETAILMSG(1, (L"************************\r\n"));
	RETAILMSG(1, (L"*** E1000E link status :\r\n"));

	if (status & E1000E_STATUS_LU_MASK) {
		RETAILMSG(1, (L"*** - Link up\r\n"));
	}
	else {
		RETAILMSG(1, (L"*** - Link down\r\n"));
		goto _exit;
	}

	speed = (status & E1000E_STATUS_SPEED_MASK) >> E1000E_STATUS_SPEED_OFFSET;
	switch (speed) {
		case E1000E_SPEED_10M :
			RETAILMSG(1, (L"*** - Link speed 10Mbps\r\n"));
			break;
		case E1000E_SPEED_100M :
			RETAILMSG(1, (L"*** - Link speed 100Mbps\r\n"));
			break;
		case E1000E_SPEED_1000M1 :
		case E1000E_SPEED_1000M2 :
			RETAILMSG(1, (L"*** - Link speed 1Gbps\r\n"));
			break;
	}

	if (status & E1000E_STATUS_FD_MASK) {
		RETAILMSG(1, (L"*** - Full duplex\r\n"));
	}
	else {
		RETAILMSG(1, (L"*** - Half duplex\r\n"));
	}

_exit :
	RETAILMSG(1, (L"************************\r\n"));
}
