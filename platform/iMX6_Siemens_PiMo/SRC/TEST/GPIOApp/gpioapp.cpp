//-----------------------------------------------------------------------------
// 
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES.
//
//-----------------------------------------------------------------------------
//
// gpio.cpp
// This application demonstrates how to use the gpio driver.
// See Usage() below for arguments, options, etc.
// 
//-----------------------------------------------------------------------------
#include <windows.h>
#include "csp.h"
#include "sdk_gpio.h"

//-----------------------------------------------------------------------------
// Defines
//-----------------------------------------------------------------------------
#define dprintf NKDbgPrintfW
#define USDHC4_CD_GPIO_PORT     1
#define USDHC4_CD_GPIO_PIN      6

#define GPIO_DEVICE_NAME        _T("GIO1:")

#define GPIO_MACRO_SET_DATA(hDev, gpio) \
    (DeviceIoControl(hDev, IOCTL_GPIO_SET, (PBYTE) &gpio, sizeof(gpio), NULL, 0, NULL, NULL))

#define GPIO_MACRO_READ_DATA(hDev, gpio) \
    (DeviceIoControl(hDev, IOCTL_GPIO_GET, (PBYTE) &gpio, sizeof(gpio), NULL, 0, NULL, NULL))

#define GPIO_MACRO_SET_DIR(hDev, gpio) \
	(DeviceIoControl(hDev, IOCTL_GPIO_CONFIG, (PBYTE) &gpio, sizeof(gpio), NULL, 0, NULL, NULL))


//-----------------------------------------------------------------------------
// Variables
//-----------------------------------------------------------------------------
static HANDLE hGPIO;

//-----------------------------------------------------------------------------
// TestFunc
//
// function for PWM test
//-----------------------------------------------------------------------------
void TestFunc(void)
{
    GPIO_PORT					CardDetectPort;
    DWORD						dwCardDetectPin;
	T_IOCTL_GPIO_GET_PARAM		gget;
	T_IOCTL_GPIO_SET_PARAM		gset;
	T_IOCTL_GPIO_CONFIG_PARAM	gParam;
	UINT32						dwPrevVal, dwPinVal = 1;
	int							t = 0;
	BOOL						b = FALSE;

    dwCardDetectPin	= USDHC4_CD_GPIO_PIN;
	CardDetectPort	= GPIO_PORT2;
	gget.pin		= dwCardDetectPin;
	gget.port		= CardDetectPort;
	gset.pin		= dwCardDetectPin;
	gset.port		= CardDetectPort;
	gset.data		= TRUE;
	gParam.port		= CardDetectPort;
	gParam.pin		= dwCardDetectPin;
	gParam.dir		= GPIO_DIR_IN;

	dprintf(TEXT("Testing the Card Detection GPIO pin, please insert and remove three time the SDCard.\r\n") );

	b = GPIO_MACRO_SET_DIR(hGPIO, gParam);

	RETAILMSG(TRUE, (TEXT("%s: GPIO_MACRO_SET_DIR INPUT %s"), __WFUNCTION__,  b ? L"Succeded" : L"FAILED :" ) );
	while(t < 4){
		dwPrevVal = dwPinVal;
		if(GPIO_MACRO_READ_DATA(hGPIO, gget) == FALSE)
		{
			dprintf(TEXT("GPIO reading data failed \r\n"));
			dprintf(TEXT("Last error %u\r\n"),GetLastError());

			return;
		}

		dwPinVal = gget.data;
		
		if (dwPrevVal != dwPinVal){
			t++;
			if (dwPinVal==0)
				dprintf(TEXT("SDCard insered.\r\n") );
			else
				dprintf(TEXT("SDCard removed.\r\n") );
		}
		Sleep(100);

	}

	RETAILMSG(TRUE, ( TEXT("Testing GPIO in OUTPUT\r\n") ) );

	gParam.dir		= GPIO_DIR_OUT;

	b = GPIO_MACRO_SET_DIR(hGPIO, gParam);

	RETAILMSG(TRUE, (TEXT("%s: GPIO_MACRO_SET_DIR OUTPUT %s"), __WFUNCTION__, b ? L"Succeded" : L"FAILED :" ) );
	t = 0;

	while(t < 20)
	{
		if(gset.data)
			gset.data = FALSE;
		else
			gset.data = TRUE;

		b = GPIO_MACRO_SET_DATA(hGPIO, gset);
		RETAILMSG(TRUE, (TEXT("%s: GPIO_MACRO_SET_DATA %s"), __WFUNCTION__, b ? L"Succeded" : L"FAILED :" ) );

		t++;
		Sleep(500);
	}
	dprintf(TEXT("test complete.\r\n") );
}

//-----------------------------------------------------------------------------
// WinMain
//
// The main function
//-----------------------------------------------------------------------------
int WINAPI WinMain ( HINSTANCE hInstance, HINSTANCE hPrevInstance, 
    LPTSTR pCmdLine, int nCmdShow)
{
    LPCTSTR devName = GPIO_DEVICE_NAME;
    // Create GPIO file handle
    hGPIO = CreateFile(devName, 
        GENERIC_READ|GENERIC_WRITE, 
        FILE_SHARE_READ|FILE_SHARE_WRITE, 
        NULL, 
        OPEN_EXISTING, 
        FILE_FLAG_RANDOM_ACCESS, 
        NULL);

    if (hGPIO == INVALID_HANDLE_VALUE)
    {
        dprintf(TEXT("Create GPIO file handle failed \r\n"));
        dprintf(TEXT("Last error %u\r\n"),GetLastError());

        return -1;
    }

    dprintf(TEXT("Create GPIO file %s\r\n"),devName );

    UNREFERENCED_PARAMETER(hInstance);
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(nCmdShow);

    TestFunc();

    dprintf(TEXT("GPIOapp quit.\r\n"));
    CloseHandle(hGPIO);

    return 0;
}
