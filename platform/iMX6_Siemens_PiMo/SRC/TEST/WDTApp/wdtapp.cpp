//-----------------------------------------------------------------------------
// 
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES.
//
//-----------------------------------------------------------------------------
//
// wdtApp.cpp
// This application demonstrates how to use the wdt.
// See Usage() below for arguments, options, etc.
// 
//-----------------------------------------------------------------------------
#include <windows.h>
#pragma warning(disable: 4702)
//-----------------------------------------------------------------------------
// Defines
//-----------------------------------------------------------------------------

#define dprintf NKDbgPrintfW

//-----------------------------------------------------------------------------
// Variables
//-----------------------------------------------------------------------------
HANDLE  hThread;

//-----------------------------------------------------------------------------
// TestFunc
//
// function for WDT test
//-----------------------------------------------------------------------------
DWORD TestFunc(LPVOID lpParam)
{

    BOOL loop = TRUE;
    dprintf(TEXT("Wait until the watchdog fire.\r\n"));
    while(loop)
    {
    }
	dprintf(TEXT("We should never reach this code.\r\n"));
	return 0;
}

//-----------------------------------------------------------------------------
// WinMain
//
// The main function
//-----------------------------------------------------------------------------
int WINAPI WinMain ( HINSTANCE hInstance, HINSTANCE hPrevInstance, 
    LPTSTR pCmdLine, int nCmdShow)
{
	BOOL loop = TRUE;
    UNREFERENCED_PARAMETER(hInstance);
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(nCmdShow);

		
	//Create a thread
	dprintf(TEXT("Create a thread.\r\n"));
	hThread=CreateThread(NULL, 0, TestFunc, NULL, 0, NULL);

   
   if(!hThread)
   {
       DEBUGMSG(1,(TEXT("WDTapp - Fail to create a thread .\r\n")));
       return FALSE;
   }

	//Change the priority of the thread
    dprintf(TEXT("Change the priority of the thread.\r\n"));
	CeSetThreadPriority(hThread, 0);
	
	//Infinite Loop
	while(loop)
    {

    }
	dprintf(TEXT("We should never reach this code.\r\n"));
    return 0;
}
