//-----------------------------------------------------------------------------
//
// UARTapp.cpp
// This application demonstrates how to use the UART driver.
// 
//-----------------------------------------------------------------------------

#include <windows.h>

//-----------------------------------------------------------------------------
// Defines
//-----------------------------------------------------------------------------
// Standard output
#define TRACE(str, ...)	RETAILMSG(1, (TEXT(str), ##__VA_ARGS__))

#define BUFFER_SIZE	16

DBGPARAM dpCurSettings = { TEXT("UARTApp"),
{ TEXT(""), TEXT(""), TEXT(""), TEXT(""),
  TEXT(""), TEXT(""), TEXT(""), TEXT(""),
  TEXT(""), TEXT(""), TEXT(""), TEXT(""),
  TEXT(""), TEXT(""), TEXT(""), TEXT("") },
  0
};

BOOL SetBaudrate(HANDLE hCOM, ULONG baudrate)
{
    DCB dcb;
	COMMTIMEOUTS timeouts;

    if (!GetCommState(hCOM, &dcb))
    {
        TRACE("ERROR: GetCommState returns false\r\n");
        return FALSE;
    }

    dcb.BaudRate    = CBR_115200;
    dcb.ByteSize    = 8;
    dcb.Parity      = NOPARITY;
    dcb.StopBits    = ONESTOPBIT;
    dcb.fParity     = FALSE;
    
//	dcb.fDtrControl = DTR_CONTROL_ENABLE;
//	dcb.fRtsControl = RTS_CONTROL_ENABLE;

    dcb.fDtrControl = DTR_CONTROL_DISABLE;
    dcb.fRtsControl = RTS_CONTROL_DISABLE;
	//dcb.fRtsControl = RTS_CONTROL_ENABLE;

    dcb.fOutxCtsFlow        = FALSE;
    dcb.fOutxDsrFlow        = FALSE;
    dcb.fDsrSensitivity     = FALSE;
    dcb.fOutX               = FALSE;
    dcb.fInX                = FALSE;
    dcb.fTXContinueOnXoff   = FALSE;

	GetCommTimeouts(hCOM, &timeouts);
	timeouts.ReadIntervalTimeout		= 1000;
	timeouts.ReadTotalTimeoutConstant	= 2000;
	timeouts.ReadTotalTimeoutMultiplier	= 20;
	SetCommTimeouts(hCOM, &timeouts);

    if(!SetCommState(hCOM, &dcb))
    {
        TRACE("ERROR: SetCommState returns false\r\n");
        return FALSE;
    }
    else
        return TRUE;
}

int WINAPI WinMain (HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
#ifdef UNDER_CE
    LPWSTR pwszCmdLine,
#else
    LPSTR lpCmdLine,
#endif
    int nCmdShow)
{
    WCHAR		*pwsz;
	HANDLE		hComm;
	char		buffer_out[BUFFER_SIZE] = "Hello World ";
	char		buffer_in[BUFFER_SIZE]  = {0};
	DWORD		bytes_read  = 0;

    pwsz = (pwszCmdLine && pwszCmdLine[0]) ? pwszCmdLine : TEXT("COM1:");
    TRACE("Try to open serial line '%s'\r\n", pwsz);
	

	hComm = CreateFile( pwsz,  
                    GENERIC_READ | GENERIC_WRITE, 
                    0, 
                    0, 
                    OPEN_EXISTING,
                    FILE_FLAG_OVERLAPPED,
                    0);

	if (hComm == INVALID_HANDLE_VALUE)
	{
		TRACE("Open %s FAILED !\r\n", pwsz );
		goto cleanup;
	}

	if (!SetBaudrate(hComm, CBR_115200))
    {
        TRACE("ERROR: Failed to set COM port parameters\r\n");
        goto cleanup;
    }

	for(;;)
    {
        // Write to the COM port
        DWORD bytes_written = 0;

        if (!WriteFile(hComm, &buffer_out, BUFFER_SIZE, &bytes_written, NULL))
        {
            TRACE("ERROR: Failed to write to COM port (error = %d)\r\n", GetLastError());
            goto cleanup;
        }
        else
        {
			TRACE("INFO: %d bytes written : '%S'\r\n", bytes_written, buffer_out);

			bytes_read  = 0;
			if(!ReadFile (hComm, &buffer_in, BUFFER_SIZE, &bytes_read, NULL))
			{
				TRACE("ERROR: Failed to READ from COM port (error = %d)\r\n", GetLastError());
			}
			
			if(bytes_read > 0)
			{
				TRACE("INFO: %d bytes read : '%S'\r\n", bytes_read, buffer_in);

				if(strcmp(buffer_in, "q") == 0)
				{
					goto cleanup;
				}
				memset(buffer_in, 0, BUFFER_SIZE);
			}
        }
        Sleep(100);
    }

cleanup:
    if (hComm != INVALID_HANDLE_VALUE)
        CloseHandle(hComm);

    return 0;
}
