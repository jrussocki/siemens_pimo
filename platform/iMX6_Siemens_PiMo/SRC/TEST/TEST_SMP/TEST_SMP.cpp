// TEST_SMP.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

void TestSmp(UINT32 nCpus, UINT32 durationMs);

void Usage()
{
	wprintf(L"Usage :\r\n");
	wprintf(L"TestSmp <args>\r\n");
	wprintf(L"  --cpus <nCpus>\r\n");
}

int _tmain(int argc, TCHAR *argv[], TCHAR *envp[])
{
	UINT32 nCpus = 4;

	for (int i = 1; i < argc; i++)
	{
		if (wcscmp(argv[i], L"--cpus") == 0)
		{
			i++;
			if (i > argc)
			{
				wprintf(L"Arg expected for %s switch\r\n", argv[i-1]);
				Usage();
			}
			else
			{
				nCpus = _wtoi(argv[i]);
			}
		}
		else
		{
			wprintf(L"Invalid switch %s\r\n", argv[i]);
		}
	}

	TestSmp(nCpus, 10000);

    return 0;
}

typedef struct _SMP_THREAD_PARAM
{
	UINT32 durationMs;
	UINT32 cpuId;
	UINT64 loops;
	BOOL   bSetAffinity;
	UINT32 sandBoxSize;
	LPVOID sandBoxStart;
}SMP_THREAD_PARAM, *PSMP_THREAD_PARAM;

ULONG cbTestSmp(LPVOID param)
{
	PSMP_THREAD_PARAM threadParam = (PSMP_THREAD_PARAM)param;
	DWORD tStart = GetTickCount();
	UINT8 *buffer = (UINT8 *)threadParam->sandBoxStart;


	wprintf(L"Ahou, Thread %d, running for %d ms\r\n", threadParam->cpuId, threadParam->durationMs);

	// Write to sandbox
	for (UINT32 i = 0; i < threadParam->sandBoxSize; i++)
	{
		buffer[i] = (UINT8)(i);// * (UINT32)threadParam->loops);
	}

	while ( (GetTickCount() - tStart) < threadParam->durationMs)
	{
		// Read back from sandbox
		for (UINT32 i = 0; i < threadParam->sandBoxSize; i++)
		{
			//if (buffer[i] != (UINT8)(i * (UINT32)threadParam->loops) )
			if (buffer[i] != (UINT8)(i))// * (UINT32)threadParam->loops) )
			{
				wprintf(L"Thread %d : error at byte %d for loop %I64d !!\r\n", 
					threadParam->cpuId, i, threadParam->loops);
			}
		}
	
		threadParam->loops++;
	}

	wprintf(L"Thread %d : terminating\r\n", threadParam->cpuId);

	return 0;
}

void TestSmp(UINT32 nCpus, UINT32 durationMs)
{
	UINT32 i;

	PSMP_THREAD_PARAM threadParams;
	HANDLE *threadHandles;
	UINT64 totalLoops = 0;
	UINT32 sandBoxSize = 1024 * 128; // 128 kB

	HANDLE h_array[1];

	threadParams = (PSMP_THREAD_PARAM)malloc(nCpus * sizeof(SMP_THREAD_PARAM));

	if (threadParams == NULL)
	{
		wprintf(L"Failed allocating thread params !\r\n");
		return;
	}

	threadHandles = (HANDLE *)malloc(nCpus * sizeof(HANDLE));

	if (threadHandles == NULL)
	{
		wprintf(L"Failed allocating thread handles !\r\n");
		return;
	}

	for (i = 0; i < nCpus; i++)
	{
		threadParams[i].bSetAffinity = TRUE;
		threadParams[i].cpuId = i;
		threadParams[i].durationMs = durationMs;
		threadParams[i].loops = 0;
		threadParams[i].sandBoxSize = sandBoxSize;
		threadParams[i].sandBoxStart = malloc(sandBoxSize);
		threadHandles[i] = CreateThread(NULL, 0, cbTestSmp, &threadParams[i], 0, NULL);
		wprintf(L"Thread handle %d : 0x%x\r\n", i, threadHandles[i]);
	}

	h_array[0] = threadHandles[0];
	//DWORD res = WaitForMultipleObjects(nCpus, threadHandles, TRUE, INFINITE);//durationMs + 5000);
	//DWORD res = WaitForMultipleObjects(1, h_array, TRUE, INFINITE);//durationMs + 5000);
	DWORD res = WaitForSingleObject(h_array[0], INFINITE);

	Sleep(500);

	if (res == WAIT_TIMEOUT)
	{
		wprintf(L"Timeout waiting for threads !\r\n");
	}
	else
	{
		wprintf(L"All threads exited\r\n");
	}

	for (i = 0; i < nCpus; i++)
	{
		wprintf(L"Thread %d did run %I64d loops\r\n", i, threadParams[i].loops);
		totalLoops += threadParams[i].loops;
		free(threadParams[i].sandBoxStart);
		CloseHandle(threadHandles[i]);
	}

	wprintf(L"Total number of loops %I64d (%I64d millions)\r\n", totalLoops, totalLoops / 1000000);

	free(threadHandles);
	free(threadParams);
}