//-----------------------------------------------------------------------------
// 
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES.
//
//-----------------------------------------------------------------------------
//
// testhive.cpp
// This application demonstrates how to use the wdt.
// See Usage() below for arguments, options, etc.
// 
//-----------------------------------------------------------------------------
#include <windows.h>
#include <winreg.h>

BOOL SetHiveTestKey (void);
BOOL ReadHiveTestKey (void);
BOOL DeleteHiveTestKey (void);

//-----------------------------------------------------------------------------
// WinMain
//
// The main function
//-----------------------------------------------------------------------------
int _tmain(int argc, TCHAR *argv[], TCHAR *envp[])
{
	DWORD* test = NULL;
	*test = 0;
    return 0;
}