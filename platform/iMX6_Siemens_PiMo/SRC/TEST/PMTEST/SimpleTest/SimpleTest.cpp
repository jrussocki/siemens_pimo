// SimpleTest.cpp : Defines the entry point for the application.
//

#include <windows.h>
#include "pm.h"
#include "dbgapi.h"

// while (1) will generates a warning otherwise
#pragma warning(disable: 4127)

#define MAX_THREADS		4
#define MAX_CPU_COUNT   4

unsigned int	globalA = 0;
unsigned int	globalB = 0;
unsigned int	globalC = 0;

DWORD g_threadNum = 0;

DWORD WorkThread();

int WINAPI WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR     lpCmdLine,
                     int       nCmdShow)
{
 //   DWORD	msTime;
	//DWORD	dwId = GetCurrentThreadId();
	//HANDLE	hThreads[MAX_THREADS];
 //   DWORD   count;

	////
	//// setting processor affinity for main thread
	////
	//NKDbgPrintfW(L"Setting primary thread affinity to 1...\r\n");
	//CeSetThreadAffinity (GetCurrentThread(), 1);

	//NKDbgPrintfW(L"I'm starting some worker threads...\r\n");

	//for (count = 0; count < MAX_THREADS; count++) {

	//	//
	//	// create thread and set it's affinity
	//	//
	//	hThreads[count] = CreateThread(NULL,65536,(LPTHREAD_START_ROUTINE)WorkThread,NULL,0,NULL);
	//	CeSetThreadAffinity (hThreads[count],count+1);
	//	
	//	if (hThreads[count] == NULL) {
	//		NKDbgPrintfW(L"\n\r\n\rError: Failed to create thread[%d]\r\n",count);
	//	}
	//}

	//do {		
	//	msTime = GetTickCount();
 //       NKDbgPrintfW(L"ThreadID=%08X: Count: %d, CPUID=%d, Tick:%d\r\n",dwId,count,GetCurrentProcessorNumber(), msTime);

	//	//
	//	// do some non-sense work and spin...
	//	//
	//	do {
	//		globalA = 1;
	//		globalB = globalA + 1;
	//		globalC = globalB;
	//	}
	//	while ((GetTickCount() - msTime) < 2000);
 //       
 //   
 //   } while (1);
	//
	////
	//// wait for worker threads to finish...
	////
	//WaitForMultipleObjects(2,hThreads,TRUE,INFINITE);
	return 0;
}

DWORD WorkThread()
{
	DWORD	dwId = GetCurrentThreadId();
	DWORD	msTime;
    const DWORD threadNum = g_threadNum++;
	static unsigned int	varA = 0;
	static unsigned int	varB = 0;
	static unsigned int	varC = 0;

	do {
		msTime = GetTickCount();
        NKDbgPrintfW(L"ThreadID=%08x: Count: %d, CPUID=%x, Tick: %d\r\n",dwId,threadNum,GetCurrentProcessorNumber(), msTime);
		do {
			varA = 1;
			varB = varA + 1;
			varC = varB;
		}
		while ((GetTickCount() - msTime) < 1000);
	} while(1);
	
	return 0;
}



