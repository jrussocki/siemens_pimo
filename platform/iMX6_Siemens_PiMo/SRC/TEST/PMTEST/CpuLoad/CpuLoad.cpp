// CpuLoad.cpp : Defines the entry point for the application.
//
#include <windows.h>

#define MAX_CPU_COUNT   4

BOOL GetAllCPUIdlePercent(DWORD cpuCount, DWORD *idleTable) {
    DWORD dwStartTick;
    DWORD dwStopTick;
    DWORD dwIdleSt[MAX_CPU_COUNT];
    DWORD dwIdleEd[MAX_CPU_COUNT];
    DWORD i;

    if (cpuCount > MAX_CPU_COUNT) {
        RETAILMSG(1,(L"ERROR: GetAllCPUIdlePercent supports up to %d CPUs\r\n", MAX_CPU_COUNT));
        return FALSE;
    }

    if (idleTable == NULL)
        return FALSE;

    dwStartTick = GetTickCount();

    for (i = 0; i < cpuCount; i++) 
        if (!CeGetIdleTimeEx(i+1, &dwIdleSt[i]))     // The processor number is a 1-based index.
            return FALSE;

    Sleep(1000);
     
    for (i = 0; i < cpuCount; i++)
        if (!CeGetIdleTimeEx(i+1, &dwIdleEd[i]))
            return FALSE;

    dwStopTick = GetTickCount();

    for (i = 0; i < cpuCount; i++) 
        idleTable[i] = ((100*(dwIdleEd[i] - dwIdleSt[i])) / (dwStopTick - dwStartTick));

    return TRUE;
}

int WINAPI WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR     lpCmdLine,
                     int       nCmdShow)
{
    //DWORD   idleTime[MAX_CPU_COUNT] = {0};
    //DWORD   cpuCount = CeGetTotalProcessors();
    //DWORD   i;

    //for(;;) {

    //    // Get cpu idle times
    //    if (!GetAllCPUIdlePercent(cpuCount, idleTime)) {
    //        RETAILMSG(1,(L"Failed to get CPU idle time\r\n"));
    //    } else {
    //        for (i = 0; i < cpuCount; i++) {
    //            RETAILMSG(1,(L"Load CPU_%d = %d%%\r\n", i+1, 100 - idleTime[i]));
    //        }
    //    }
    //}

    return 0;
}



