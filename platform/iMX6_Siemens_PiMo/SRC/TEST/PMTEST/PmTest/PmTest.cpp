// PmTest.cpp : Defines the entry point for the console application.
//

#include <windows.h>
#include "winioctl.h"
#include "pm.h"
#include "dbgapi.h"
#include <pkfuncs.h>
#include <common_ioctl.h>
#include <platform_ioctl.h>
#include <notify.h>
#include <nkintr.h>

DWORD	count = 0;
DWORD	dwTest = 0;

HANDLE  g_hNotify = NULL;

BOOL VerifyWakeupResources()
{
    //check RTC
    DWORD dwWakeSrc = SYSINTR_RTC_ALARM;

    BOOL fIoctlRet = FALSE;
    fIoctlRet = KernelIoControl(IOCTL_HAL_ENABLE_WAKE,                
                &dwWakeSrc,                            
                sizeof(dwWakeSrc),                    
                NULL,                                
                0,                                    
                NULL);

    if(fIoctlRet == FALSE){
        NKDbgPrintfW(_T("WARN: The system does not support enabling waking up from suspend using RTC interrupt!\r\n"));
    }

    return fIoctlRet;
}

BOOL SetupRTCWakeupTimer(WORD wSeconds)
{
    SYSTEMTIME sysTime;
    WORD wRemain, wQuotient;
    CE_NOTIFICATION_TRIGGER trigger;
   
    // set RTC alarm
    GetLocalTime(&sysTime);

    wQuotient = wSeconds;

    wRemain = sysTime.wSecond + wQuotient;
    sysTime.wSecond = wRemain % 60;
    wQuotient = wRemain / 60;

    wRemain = sysTime.wMinute + wQuotient;
    sysTime.wMinute = wRemain % 60;
    wQuotient = wRemain / 60;

    wRemain = sysTime.wHour + wQuotient;
    sysTime.wHour = wRemain % 24;
    wQuotient = wRemain / 24;

    memset((PBYTE)&trigger, 0, sizeof(trigger));
    
    trigger.dwSize = sizeof(trigger);
    trigger.dwType = CNT_TIME;
    trigger.dwEvent = 0;
    trigger.lpszApplication = L"\\\\.\\Notifications\\NamedEvents\\SetAlarm";
    trigger.lpszArguments = NULL;
    trigger.stStartTime = sysTime;

    g_hNotify = CeSetUserNotificationEx(NULL, &trigger, NULL);
    if (g_hNotify == NULL){
        NKDbgPrintfW(_T("CeSetUserNotificationEx failed. Error =%d\r\n"), GetLastError());
        return FALSE;
    }

    NKDbgPrintfW(_T("The RTC Alarm is set to wake up the device in %d sec.\r\n"), wSeconds);

    return TRUE;    
}

VOID CleanupRTCTimer()
{

    //clear timer notification
    if(g_hNotify!= NULL){
        CeClearUserNotification(g_hNotify);
        g_hNotify = NULL;
    }
}

TCHAR *SystemTimeToString(SYSTEMTIME *sysTime) {
    static TCHAR szSysTime[256];
    swprintf(szSysTime, L"%d/%d/%d %d:%d:%d", 
        sysTime->wYear, sysTime->wMonth, sysTime->wDay,
        sysTime->wHour, sysTime->wMinute, sysTime->wSecond);

    return szSysTime;
}

int WINAPI WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR     lpCmdLine,
                     int       nCmdShow)
{
	DWORD	dwRet = 0;
    SYSTEMTIME curSysTime;
    BOOL    bRTCWakeUp;

	if (dwTest == 0) {

        // Check if RTC can be used as a wakeup source:
        bRTCWakeUp = VerifyWakeupResources();

        /*for (;;)*/ {
	        dwRet = 5;
		    do {
			    NKDbgPrintfW(L"Count down to SUSPEND: %d...\r\n",dwRet);
			    Sleep(5000);
            } while(dwRet--);

            if (bRTCWakeUp)
                SetupRTCWakeupTimer(15);

            GetLocalTime(&curSysTime);
            RETAILMSG(1,(L"Current time: %s\r\n", SystemTimeToString(&curSysTime)));

		    NKDbgPrintfW(L"Calling SetSystemPowerState...\r\n");
		    if ((dwRet = SetSystemPowerState(NULL,POWER_STATE_SUSPEND,POWER_FORCE)) != ERROR_SUCCESS) {

			    NKDbgPrintfW(L"SetSystemPowerState failed: 0x%0X...\r\n",dwRet);
            } else {
			    NKDbgPrintfW(L"SetSystemPowerState succeeded: 0x%0X...\r\n",dwRet);
            }

            GetLocalTime(&curSysTime);
            RETAILMSG(1,(L"Current time: %s\r\n", SystemTimeToString(&curSysTime)));

            count = 5;
		    do {
			    NKDbgPrintfW(L"Back from the dead: %d...\r\n",count--);
                Sleep(3000);
		    } while(count);
        }

	} else if (dwTest == 1) {

		NKDbgPrintfW(L"Calling KernelIoControl:IOCTL_HAL_TEST_PM...\r\n");
		KernelIoControl(IOCTL_HAL_TEST_PM,NULL,0,NULL,0,NULL);
	
    } else if (dwTest == 2) {
	    NKDbgPrintfW(L"Calling SetSystemPowerState (POWER_STATE_OFF)...\r\n");
	    if ((dwRet = SetSystemPowerState(NULL,POWER_STATE_OFF,POWER_FORCE)) != ERROR_SUCCESS) {

		    NKDbgPrintfW(L"SetSystemPowerState failed: 0x%0X...\r\n",dwRet);
        } else {

		    NKDbgPrintfW(L"SetSystemPowerState succeeded: 0x%0X...\r\n",dwRet);
        }

    } else {

		NKDbgPrintfW(L"Invalid test number...\r\n");
	}

	return 0;
}
