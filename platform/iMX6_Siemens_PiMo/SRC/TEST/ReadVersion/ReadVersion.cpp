//-----------------------------------------------------------------------------
// 
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES.
//
//-----------------------------------------------------------------------------
//
// ReadVersion.cpp
// This application demonstrates how to use the wdt.
// See Usage() below for arguments, options, etc.
// 
//-----------------------------------------------------------------------------
#include <windows.h>
#include <platform_ioctl.h>
#include <eboot_version.h>
#include <bsp_version.h>

//-----------------------------------------------------------------------------
// WinMain
//
// The main function
//-----------------------------------------------------------------------------
int _tmain(int argc, TCHAR *argv[], TCHAR *envp[])
{
	EBOOT_VERSION ebootVer = {0};
	BSP_VERSION bspVer = {0};
	BOOL fRet = FALSE;

	RETAILMSG(TRUE,(L"ReadVersion.exe :\r\n"));
	if(argc != 2)
	{
		RETAILMSG(TRUE,(L"Too many or too few arguments\r\n"));
		goto cleanUp;
	}
	if(wcscmp(argv[1], L"-eboot") == 0)
	{
		fRet = KernelIoControl(IOCTL_HAL_GET_EBOOT_VERSION, NULL, 0, &ebootVer, sizeof(EBOOT_VERSION), NULL);
		if(fRet == 1)
		{
			RETAILMSG(TRUE,(L"Eboot version = %d.%d.%d\r\n", ebootVer.major, ebootVer.minor, ebootVer.incremental));
		}
		else
		{
			RETAILMSG(TRUE,(L"ReadVersion Eboot failed. Last error : %d\r\n",GetLastError()));
		}
	}
	else if(wcscmp(argv[1], L"-nk") == 0)
	{
		fRet = KernelIoControl(IOCTL_HAL_GET_KERNEL_VERSION, NULL, 0, &bspVer, sizeof(EBOOT_VERSION), NULL);
		if(fRet == 1)
		{
			RETAILMSG(TRUE,(L"Kernel version = %d.%d.%d\r\n", bspVer.major, bspVer.minor, bspVer.incremental));
		}
		else
		{
			RETAILMSG(TRUE,(L"ReadVersion Kernel failed. Last error : %d\r\n",GetLastError()));
		}
	}
	else
	{
		RETAILMSG(TRUE,(L"Only '-eboot' or '-nk' are accepted as arguments\r\n"));
	}
cleanUp:
    return 0;
}
