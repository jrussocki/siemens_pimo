//-----------------------------------------------------------------------------
// 
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES.
//
//-----------------------------------------------------------------------------
//
// testhive.cpp
// This application demonstrates how to use the wdt.
// See Usage() below for arguments, options, etc.
// 
//-----------------------------------------------------------------------------
#include <windows.h>
#include <winreg.h>

BOOL SetHiveTestKey (void);
BOOL ReadHiveTestKey (void);
BOOL DeleteHiveTestKey (void);

//-----------------------------------------------------------------------------
// WinMain
//
// The main function
//-----------------------------------------------------------------------------
int _tmain(int argc, TCHAR *argv[], TCHAR *envp[])
{
	BOOL fRet = FALSE;

	RETAILMSG(TRUE,(L"testhive.exe :\r\n"));
	if(argc != 2)
	{
		RETAILMSG(TRUE,(L"Too many or too few arguments\r\n"));
		goto cleanUp;
	}
	if(wcscmp(argv[1], L"0") == 0)
	{
		fRet = SetHiveTestKey();
		if(fRet == FALSE)
		{
			RETAILMSG(TRUE,(L"SetHiveTestKey failed\r\n"));
		}
	}
	else if(wcscmp(argv[1], L"1") == 0)
	{
		fRet = ReadHiveTestKey();
		if(fRet == FALSE)
		{
			RETAILMSG(TRUE,(L"ReadHiveTestKey failed\r\n"));
		}
	}
	else if(wcscmp(argv[1], L"2") == 0)
	{
		fRet = DeleteHiveTestKey();
		if(fRet == FALSE)
		{
			RETAILMSG(TRUE,(L"DeleteHiveTestKey failed\r\n"));
		}
	}
	else
	{
		RETAILMSG(TRUE,(L"Only '0', '1' or '2' are accepted as arguments\r\n"));
	}
cleanUp:
    return 0;
}


/* Set a new key and save it in the hive registry */

BOOL SetHiveTestKey (void)
{
    HKEY hKey = NULL;
    BOOL fRet = TRUE;
	DWORD dwHiveTest = 1;
    DWORD dwErrorCode = 0;
    DWORD dwDispostition = 0;
    INT dwResult = -1;

	// Open a key to HKEY_LOCAL_MACHINE\Software\HiveKey
    dwResult = RegCreateKeyEx(
        HKEY_LOCAL_MACHINE,
        L"Software\\HiveKey",
        0,
        NULL, 
        0, 
        0,
        NULL, 
        &hKey, 
        &dwDispostition);
    dwErrorCode = GetLastError();
    if(ERROR_SUCCESS != dwResult)
    {
        fRet = FALSE;
        RETAILMSG(TRUE,(L"Error in RegCreateKey. Error code : %d\r\n",dwErrorCode));
        goto cleanUp;
    }

	// Set the value HiveTest in HKEY_LOCAL_MACHINE\Software\HiveKey
    dwResult = RegSetValueEx(
        hKey, 
        TEXT("HiveTest"), 
        0, 
        REG_DWORD, 
        (LPBYTE)&dwHiveTest, 
        sizeof(DWORD));
    dwErrorCode = GetLastError();
    if(ERROR_SUCCESS != dwResult)
    {
        fRet = FALSE;
        RETAILMSG(TRUE,(L"Error in RegSetValue. Error code : %d\r\n",dwErrorCode));
		goto cleanUp;
    }

	// Store the new value in the Hive
	dwResult = RegFlushKey(hKey);
	dwErrorCode = GetLastError();
	if(ERROR_SUCCESS != dwResult)
    {
        fRet = FALSE;
        RETAILMSG(TRUE,(L"Error in RegFlushKey. Error code : %d\r\n",dwErrorCode));
    }

	RETAILMSG(TRUE,(L"The key HKEY_LOCAL_MACHINE\\Software\\HiveKey\\HiveTest is correctly set\r\n"));

    // Close the key
cleanUp:
    if(hKey)
    {
        RegCloseKey(hKey);
        hKey = NULL;
    } 
    return fRet;
}

/* Read the key HKEY_LOCAL_MACHINE\Software\HiveKey if it is existing */

BOOL ReadHiveTestKey (void)
{
    HKEY hKey = NULL;
    BOOL fRet = TRUE;
    DWORD dwErrorCode = 0;
    DWORD dwDispostition = 0;
    INT dwResult = -1;

    // Open a key to HKEY_LOCAL_MACHINE\Software\HiveKey
    dwResult = RegCreateKeyEx(
        HKEY_LOCAL_MACHINE,
        L"Software\\HiveKey",
        0,
        NULL, 
        0, 
        0,
        NULL, 
        &hKey, 
        &dwDispostition);
    dwErrorCode = GetLastError();
    if(ERROR_SUCCESS != dwResult)
    {
        fRet = FALSE;
        RETAILMSG(TRUE,(L"Error in RegCreateKey. Error code : %d\r\n",dwErrorCode));
        goto cleanUp;
    }

	// Check if the key exist or not
    if(dwDispostition == REG_CREATED_NEW_KEY)
    {
        RETAILMSG(TRUE,(L"Key has not be set yet"));
        RegCloseKey(hKey);
        RegDeleteKey(
			HKEY_LOCAL_MACHINE,
			L"Software\\HiveKey");
        return fRet;
    }
	// If the key exist then we read the value of HKEY_LOCAL_MACHINE\Software\HiveKey
	else
	{
		WCHAR *wcName = new WCHAR[20];
        BYTE  *bHiveTest = new BYTE[20];
        DWORD dwNbCharName = 20;
        DWORD dwSizeOfHiveTest = 20;

		dwResult = RegEnumValue(
			hKey,
			0,
			wcName,
			&dwNbCharName,
			NULL,
			NULL,
			bHiveTest,
			&dwSizeOfHiveTest);
		dwErrorCode = GetLastError();

		if(ERROR_SUCCESS != dwResult)
		{
			fRet = FALSE;
			RETAILMSG(TRUE,(L"Error in RegEnumValue. Error code : %d\r\n",dwErrorCode));
			goto cleanUp;
		}
		RETAILMSG(TRUE,(L"Value of ''%s'' is : %d", wcName, (DWORD)*bHiveTest));
	}
cleanUp:
    if(hKey)
    {
        RegCloseKey(hKey);
        hKey = NULL;
    } 
    return fRet;
}

/* Delete the key HKEY_LOCAL_MACHINE\Software\HiveKey if it is existing */

BOOL DeleteHiveTestKey (void)
{
    HKEY hKey = NULL;
    BOOL fRet = TRUE;
    DWORD dwErrorCode = 0;
	DWORD dwDispostition = 0;
    INT dwResult = -1;

	dwResult = RegCreateKeyEx(
        HKEY_LOCAL_MACHINE,
        L"Software\\HiveKey",
        0,
        NULL, 
        0, 
        0,
        NULL, 
        &hKey, 
        &dwDispostition);
    dwErrorCode = GetLastError();
    if(ERROR_SUCCESS != dwResult)
    {
        fRet = FALSE;
        RETAILMSG(TRUE,(L"Error in RegCreateKey. Error code : %d\r\n",dwErrorCode));
        goto cleanUp;
    }

    RegCloseKey(hKey);

    // We delete all the key in HKEY_LOCAL_MACHINE\Software\HiveKey
    dwResult = RegDeleteKey(
        HKEY_LOCAL_MACHINE,
        L"Software\\HiveKey");
    dwErrorCode = GetLastError();
    if(ERROR_SUCCESS != dwResult)
    {
        fRet = FALSE;
        RETAILMSG(TRUE,(L"Error in RegDeleteKey. Error code : %d\r\n",dwErrorCode));
		goto cleanUp;
    }

	dwResult = RegFlushKey(HKEY_LOCAL_MACHINE);
	dwErrorCode = GetLastError();
	if(ERROR_SUCCESS != dwResult)
    {
        fRet = FALSE;
        RETAILMSG(TRUE,(L"Error in RegFlushKey. Error code : %d\r\n",dwErrorCode));
		goto cleanUp;
    }

	RETAILMSG(TRUE,(L"The key HKEY_LOCAL_MACHINE\\Software\\HiveKey\\HiveTest is correctly deleted\r\n"));

cleanUp:
    if(hKey)
    {
        RegCloseKey(hKey);
        hKey = NULL;
    } 
    return fRet;
}