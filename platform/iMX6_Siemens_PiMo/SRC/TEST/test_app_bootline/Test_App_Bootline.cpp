//-----------------------------------------------------------------------------
//
//  Copyright (C) 2014, Adeneo Embedded. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//-----------------------------------------------------------------------------
//
//  File: Test_App_Bootline.cpp
//
//	This application test whether the loaded kernel is an operative
//		or a recovery kernel.
//	
//
//-----------------------------------------------------------------------------

#include <windows.h>
#include <bsp_bootline.h>
#include <platform_ioctl.h>

BOOL IsKernelRecovery(void)
{
	BOOTLINE BootLine;

	if(KernelIoControl(
		IOCTL_HAL_GET_KERNEL_BOOTLINE,
		NULL, 0, 
		&BootLine, sizeof(DWORD),
		NULL)==TRUE)
	{

		if(BootLine==BOOTLINE_OPERATIVE)
			_tprintf(L"Kernel version : \"operative\"\r\n");
		else if(BootLine==BOOTLINE_RECOVERY)
			_tprintf(L"Kernel version : \"recovery\"\r\n");
		else if(BootLine==BOOTLINE_UNKNOWN)
			_tprintf(L"Neither operative nor recovery kernel\"\n");
		else
			_tprintf(L"Unable to get VALID bootline\r\n");
	}
	else
	{
		_tprintf(L"Unable to get boot line\r\n");
		BootLine = BOOTLINE_UNKNOWN;
	}
	return BootLine;
}

//-----------------------------------------------------------------------------
// _tmain
//
// The main function
//-----------------------------------------------------------------------------

int _tmain(int argc, TCHAR *argv[], TCHAR *envp[])
{
	IsKernelRecovery();
	return 0;
}


