/**
 * @file    test_app_cantest.cpp
 *
 * @brief   CANTEST test application
 *
 * @copyright  All rights reserved Adeneo Embedded 2012
 *
 * @addtogroup TEST
 * @{
 * 
 */

/*** INCLUDES FILES **********************************************************/
#include <windows.h>
#include "canbus.h"
#include "cantest_iocontrol.h"

/*** DEFINES *****************************************************************/

#define TEST_DEVICE_KEY		L"\\Drivers\\CanTest"
#define TEST_DEVICE_PREFIX	L"CTE"

// Maximum possible CAN message IDs
#define CAN_MAX_ID_STANDARD	0x000007FF
#define CAN_MAX_ID_EXTENDED	0x1FFFFFFF

// CAN IDs definitions
#define COMMAND_ID       0x2AA			// CAN ID used for commands
#define TEST_STANDARD_ID 0x5B7			// CAN UD used for tests with standard ID
#define TEST_EXTENDED_ID 0x1DE6C47B		// CAN ID used for tests with extended ID

// Tests definitions
#define TEST_RX_THROUGHPUT_STANDARD 0
#define TEST_TX_THROUGHPUT_STANDARD 1
#define TEST_RX_THROUGHPUT_EXTENDED 2
#define TEST_TX_THROUGHPUT_EXTENDED	3
#define TEST_RX_REMOTE_STANDARD		4
#define TEST_TX_REMOTE_STANDARD		5
#define TEST_RX_REMOTE_EXTENDED		6
#define TEST_TX_REMOTE_EXTENDED		7
#define TEST_PRIORITY				8
#define TEST_TX_ABORT				9
#define TEST_RX_ABORT				10
#define TEST_FILTERS				11
#define TEST_TX_SIZE				12
#define TEST_RX_SIZE				13
#define TEST_LOCAL_PRIORITY			14

#define TEST_MAX					14

TCHAR * TESTS_NAMES[] = {
	L"RX Throughput (Standard ID)",
	L"TX Throughput (Standard ID)",
	L"RX Throughput (Extended ID)",
	L"TX Throughput (Extended ID)",
	L"RX Remote frames (Standard ID)",
	L"TX Remote frames (Standard ID)",
	L"RX Remote frames (Extended ID)",
	L"TX Remote frames (Extended ID)",
	L"TX Priority",
	L"TX Abort",
	L"RX Abort",
	L"Filters",
	L"TX with various buffer sizes",
	L"RX with various buffer sizes",
};

// Default parameters
#define DEFAULT_ID			0x2AA
#define DEFAULT_LEN			1
#define DEFAULT_DATA_VAL	0xAA
#define DEFAULT_DURATION	10000
#define DEFAULT_ID_LEN      11
#define DEFAULT_REMOTE      0
#define DEFAULT_ABORT		0
#define DEFAULT_TIMEOUT		1000

#define DEFAULT_PS			0
#define DEFAULT_PROP		0
#define DEFAULT_P1			0
#define DEFAULT_P2			0
#define DEFAULT_RJW			0

#define DEFAULT_BITRATE		500000
#define DEFAULT_TEST_ID		-1

/*** LOCAL FUNCTIONS *********************************************************/
BOOL TestActivate(TCHAR * dev_key);
BOOL TestDeactivate(TCHAR * dev_prefix);
BOOL StartTest(DWORD dw_test_zone, DWORD dw_test_id);
HANDLE OpenCANTESTFile(TCHAR * dev_prefix);
BOOL TestWrite(DWORD data_len, DWORD id, DWORD idlen, UINT8 val, BOOL remote, DWORD timeout, BOOL abort);
BOOL TestRead(DWORD timeout);
BOOL TestSetLoopback(BOOL enable);
BOOL TestGetBitrate();
BOOL TestSetBitrate(UINT32 bitrate);
BOOL TestLoopback(DWORD duration, DWORD data_len, DWORD id, UINT8 val);
BOOL TestSetTiming(UINT16 ps, UINT8 prop, UINT8 p1, UINT8 p2, UINT8 rjw);
VOID TestGetTiming();
BOOL TestRXThroughput(HANDLE handle, DWORD duration, DWORD id, CAN_FRAME_FORMAT format);
BOOL RunTest(INT8 testId, DWORD duration, UINT32 bitrate);

//-----------------------------------------------------------------------------
//
// Function:  Usage()
//
// This function displays the test program usage page
// 
// Parameters:
//      None
//
// Returns:
//		None
//
//-----------------------------------------------------------------------------
VOID Usage()
{
	wprintf(L"\r\n");
	wprintf(L"Usage :\r\n");
	wprintf(L"\r\n");
	wprintf(L"TEST_APP_CANTEST --activate\r\n");
	wprintf(L"	If driver is not builtin, this activates the driver\r\n");
	wprintf(L"	ex: TEST_APP_CANTEST --activate\r\n");
	wprintf(L"		activates the CANTEST driver\r\n");
	wprintf(L"\r\n");
	wprintf(L"TEST_APP_CANTEST --deactivate\r\n");
	wprintf(L"	If driver is not builtin, this deactivates the driver\r\n");
	wprintf(L"	ex: TEST_APP_CANTEST --deactivate\r\n");
	wprintf(L"\r\n");
	wprintf(L"TEST_APP_CANTEST --write [--len <len> --id <id> --idlen <idlen> --val <val> --remote <remote>]\r\n");
	wprintf(L"  Send data on CAN bus.\r\n");
	wprintf(L"  Options: --len: set length of data\r\n");
	wprintf(L"           --id:  set bmessage ID (decimal or hexa)\r\n");
	wprintf(L"           --val: set value of sent bytes\r\n");
	wprintf(L"           --idlen: length of ID (may be 11 of 29)\r\n");
	wprintf(L"           --remote: send a remote frame if set to 1\r\n");
	wprintf(L"\r\n");
	wprintf(L"TEST_APP_CANTEST --read [--timeout <timeout>\r\n");
	wprintf(L"  Read data from CAN bus with given timeout.\r\n");
	wprintf(L"\r\n");
	wprintf(L"TEST_APP_CANTEST --enableloopback\r\n");
	wprintf(L"  Enable loopback mode\r\n");
	wprintf(L"\r\n");
	wprintf(L"TEST_APP_CANTEST --disableloopback\r\n");
	wprintf(L"  Disableloopback mode\r\n");
	wprintf(L"\r\n");
	wprintf(L"TEST_APP_CANTEST --settiming [--ps <ps> --prop <prop> --p1 <p1> --p2 <p2> --rjw <rjw>]\r\n");
	wprintf(L"  Set CAN bus timing parameters\r\n");
	wprintf(L"  Options: --ps:   clock prescaler (1-256)\r\n");
	wprintf(L"           --prop: propagation time\r\n");
	wprintf(L"           --p1:   phase segment 1\r\n");
	wprintf(L"           --p2:   phase segment 2\r\n");
	wprintf(L"           --rjw:  resynchronisation jump width\r\n");
	wprintf(L"\r\n");
	wprintf(L"TEST_APP_CANTEST --gettiming\r\n");
	wprintf(L"  Get and display CAN bus timing parameters\r\n");
	wprintf(L"\r\n");
	wprintf(L"TEST_APP_CANTEST --setbitrate --bitrate <rate>\r\n");
	wprintf(L"  Modify prescaler to reach bitrate, other timing parameters are not modified\r\n");
	wprintf(L"\r\n");
	wprintf(L"TEST_APP_CANTEST --getbitrate\r\n");
	wprintf(L"  Display current bitrate\r\n");
	wprintf(L"\r\n");
	wprintf(L"TEST_APP_CANTEST --rxthroughput [options]\r\n");
	wprintf(L"  Options: --duration: test duration in ms\r\n");
	wprintf(L"           --id:       message ID\r\n");
	wprintf(L"\r\n");
	wprintf(L"TEST_APP_CANTEST --runtest [options]\r\n");
	wprintf(L"  Options: --testId:   test ID\r\n");
	wprintf(L"           --duration: test duration in ms\r\n");
	wprintf(L"           --bitrate:  use bitrate for test\r\n");
	wprintf(L"\r\n");
}

//-----------------------------------------------------------------------------
//
// Function:  _tmain(int argc, TCHAR *argv[])
//
// Main function.
// 
// Parameters:
//      argc	[in] Number of arguments
//		argv	[in] Array of arguments
//
// Returns:
//		An integer, 0 if success, -1 if something failed
//
//-----------------------------------------------------------------------------
int _tmain(int argc, TCHAR *argv[])
{
	int i = 1;
	TCHAR *command;
	DWORD val;
	TCHAR *sw;

	BOOL bRes = TRUE;

	DWORD id = DEFAULT_ID;
	DWORD len = DEFAULT_LEN;
	DWORD dataval = DEFAULT_DATA_VAL;
	DWORD duration = DEFAULT_DURATION;
	DWORD idlen = DEFAULT_ID_LEN;
	DWORD remote = DEFAULT_REMOTE;
	DWORD abort = DEFAULT_ABORT;
	DWORD timeout = DEFAULT_TIMEOUT;
	DWORD bitrate = DEFAULT_BITRATE;
	INT8  testId = DEFAULT_TEST_ID;

	UINT16 ps   = DEFAULT_PS;
	UINT8  prop = DEFAULT_PROP;
	UINT8  p1   = DEFAULT_P1;
	UINT8  p2   = DEFAULT_P2;
	UINT8  rjw  = DEFAULT_RJW;

	if (argc < 2) {
		Usage();
		return -1;
	}

	// Get command
	command = argv[1];

	// Get parameter switches
	i = 2;
	while (i < argc) {
		// Get parameter name
		sw = argv[i];
		i++;
		
		// We expect a parameter for every switch
		if (i == argc) {
			wprintf(L"Expecting parameter for switch %s\r\n", sw);
			return -1;
		}

		//////////////////////////////////////
		// Get value associated to a switch //
		//////////////////////////////////////

		val = _wtoi(argv[i]);

		if (wcscmp(sw, L"--id") == 0) {
			id = wcstoul(argv[i], NULL, 0);
			SetLastError(0);
			if (GetLastError() != 0) {
				wprintf(L"Invalid parameter %s for switch %s\r\n", argv[i], sw);
				return -1;
			}
		}
		else if (wcscmp(sw, L"--len") == 0) {
			len = val;
		}
		else if (wcscmp(sw, L"--idlen") == 0) {
			idlen = val;
			if (idlen != 11 && idlen != 29) {
				wprintf(L"Valid ID lengths are 11 and 29\r\n");
				return -1;
			}
		}
		else if (wcscmp(sw, L"--remote") == 0) {
			remote = val;
			if (remote != 1 && remote != 0)  {
				wprintf(L"Valid values for remote are 0 and 1\r\n");
				return -1;
			}
		}
		else if (wcscmp(sw, L"--val") == 0) {
			dataval = wcstoul(argv[i], NULL, 0);
		}
		else if (wcscmp(sw, L"--ps") == 0) {
			ps = (UINT16)val;
		}
		else if (wcscmp(sw, L"--prop") == 0) {
			prop = (UINT8)val;
		}
		else if (wcscmp(sw, L"--p1") == 0) {
			p1 = (UINT8)val;
		}
		else if (wcscmp(sw, L"--p2") == 0) {
			p2 = (UINT8)val;
		}
		else if (wcscmp(sw, L"--rjw") == 0) {
			rjw = (UINT8)val;
		}
		else if (wcscmp(sw, L"--timeout") == 0) {
			timeout = wcstoul(argv[i], NULL, 0);
		}
		else if (wcscmp(sw, L"--abort") == 0) {
			abort = val;
		}
		else if (wcscmp(sw, L"--duration") == 0) {
			duration = val;
		}
		else if (wcscmp(sw, L"--bitrate") == 0) {
			bitrate = val;
		}
		else if (wcscmp(sw, L"--testId") == 0) {
			testId = (INT8)val;
		}
		else {
			wprintf(L"Invalid switch %s\r\n", sw);
			return -1;
		}

		i++;
	}

	////////////////////////
	// Get command switch //
	////////////////////////

	if (wcscmp(command, (L"--activate")) == 0)
	{
		TestActivate(TEST_DEVICE_KEY);
	}
	else if (wcscmp(command, (L"--deactivate")) == 0)
	{
		TestDeactivate(TEST_DEVICE_PREFIX);
	}
	else if (wcscmp(command, L"--write") == 0) 
	{
		TestWrite(len, id, idlen, (UINT8)dataval, (BOOL)remote, timeout, (BOOL)abort);
	}
	else if (wcscmp(command, L"--read") == 0) 
	{
		TestRead(timeout);
	}
	else if (wcscmp(command, L"--enableloopback") == 0)
	{
		TestSetLoopback(TRUE);
	}
	else if (wcscmp(command, L"--disableloopback") == 0)
	{
		TestSetLoopback(FALSE);
	}
	else if (wcscmp(command, L"--testloopback") == 0)
	{
		TestLoopback(duration, len, id, (UINT8)dataval);
	}
	else if (wcscmp(argv[1], L"--settiming") == 0) 
	{
		TestSetTiming(ps, prop, p1, p2, rjw);
	}
	else if (wcscmp(argv[1], L"--gettiming") == 0)
	{
		TestGetTiming();
	}
	else if (wcscmp(argv[1], L"--setbitrate") == 0) 
	{
		TestSetBitrate(bitrate);
	}
	else if (wcscmp(argv[1], L"--getbitrate") == 0)
	{
		TestGetBitrate();
	}
	else if (wcscmp(argv[1], L"--runtest") == 0) {
		bRes = RunTest(testId, duration, bitrate);
	}
	else {
		wprintf(L"Invalid command %s\r\n", argv[1]);
		wprintf(L"\r\n");
	}

	if (!bRes)
		return -1;

    return 0;
}

//-----------------------------------------------------------------------------
//
// Function:  TestActivate(TCHAR * dev_key)
//
// Activates the CAN test driver
// 
// Parameters:
//      dev_key [in] The prefix of the can test driver
//
// Returns:
//		TRUE if success, FALSE if failure
//
//-----------------------------------------------------------------------------
BOOL TestActivate(TCHAR * dev_key)
{
	HANDLE h_dev;

	h_dev = ActivateDeviceEx(dev_key,
							 NULL,
							 0,
							 NULL);

	if (h_dev == NULL)
	{
		wprintf(L"Failed in ActivateDeviceEx\r\n");
		return FALSE;
	}

	return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  TestDeactivate(TCHAR * dev_prefix)
//
// Deactivates the CAN test driver
// 
// Parameters:
//      dev_prefix [in] The prefix of the CAN test driver
//
// Returns:
//		TRUE if success, FALSE if failure
//
//-----------------------------------------------------------------------------
BOOL TestDeactivate(TCHAR * dev_prefix)
{
	HANDLE h_file;
	HANDLE h_dev;
	DEVMGR_DEVICE_INFORMATION dev_info;

	dev_info.dwSize = sizeof(DEVMGR_DEVICE_INFORMATION);

	h_file = OpenCANTESTFile(dev_prefix);

	if (h_file == INVALID_HANDLE_VALUE)
	{
		wprintf(L"Failed in OpenCANTESTFile\r\n");
		return FALSE;
	}

	if (GetDeviceInformationByFileHandle(h_file, &dev_info) == FALSE)
	{
		wprintf(L"Failed in GetDeviceInformationByFileHandle\r\n");
		return FALSE;
	}

	CloseHandle(h_file);

	h_dev = dev_info.hDevice;

	if (h_dev == INVALID_HANDLE_VALUE)
	{
		wprintf(L"Invalid device handle\r\n");
		return FALSE;
	}

	if (DeactivateDevice(h_dev) == FALSE)
	{
		wprintf(L"Failed in DeactivateDevice, error %d\r\n",
						GetLastError());

		return FALSE;
	}

	return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  TestWrite(DWORD data_len, 
//						DWORD id, 
//						DWORD idlen, 
//						UINT8 val, 
//						BOOL bRemote, 
//						DWORD timeout, 
//						BOOL abort)
//
// Writes data on the CAN bus
// 
// Parameters:
//      data_len [in] Length of data to write
//		id		 [in] Message ID
//		idlen	 [in] ID length (11:standard message, 29: extended message)
//		val		 [in] Value to write into data
//		bRemote  [in] Should remote flag be set ?
//		timeout  [in] Timeout for transmission
//		abort	 [in] Shoud transmission be aborted after timeout expires ?
//
// Returns:
//		TRUE if success, FALSE if failure
//
//-----------------------------------------------------------------------------
BOOL TestWrite(DWORD data_len, DWORD id, DWORD idlen, UINT8 val, BOOL bRemote, DWORD timeout, BOOL abort)
{
	HANDLE h_file = INVALID_HANDLE_VALUE;
	CANTEST_TRANSFER xfer;
	BOOL ret = FALSE;
	xfer.Data = NULL;
	xfer.dwTimeout = timeout;
	xfer.bAbortTxIfTimeout = abort;

	xfer.Data = (UINT8 *)LocalAlloc(LPTR, data_len);
	if (xfer.Data == NULL) 
	{
		wprintf(L"Failed in LocalAlloc\r\n");
		goto cleanup;
	}
	for (DWORD i = 0; i < data_len; i++) {
		xfer.Data[i] = (UINT8)i;
	}

	xfer.bRemote = bRemote;
	if (bRemote) {
		wprintf(L"Sending a remote frame\r\n");
	}

	wprintf(L"Sending %d bytes, ID %d (0x%x), data 0x%x\r\n", data_len, id, id, val);

	xfer.ID = id;
	xfer.IDLen = idlen;

	for (DWORD i = 0; i < data_len; i++) {
		xfer.Data[i] = val + (UINT8)i;
	}

	xfer.DataLen = data_len;

	h_file = OpenCANTESTFile(TEST_DEVICE_PREFIX);
	if (h_file == INVALID_HANDLE_VALUE)
	{
		wprintf(L"TestWrite: Failed in OpenCANTESTFile, check that driver is activated\r\n");
		goto cleanup;
	}

	if (!DeviceIoControl(h_file,
						CANTEST_IOCTL_WRITE,
						(LPVOID)&xfer,
						sizeof(xfer),
						NULL,
						0,	
						NULL,
						NULL)) {
		wprintf(L"TestWrite: Failed in DeviceIoControl\r\n");
		goto cleanup;
	}

	wprintf(L"Xfer id is now %d\r\n", xfer.ID);

	ret = TRUE;
	wprintf(L"Done iocontrol, result %d\r\n", xfer.iResult);

cleanup :
	if (xfer.Data != NULL) {
		LocalFree(xfer.Data);
	}

	if (h_file != INVALID_HANDLE_VALUE) {
		CloseHandle(h_file);
	}

	return ret;
}

//-----------------------------------------------------------------------------
//
// Function:  BOOL TestRead(DWORD timeout);
//
// Reads data from the CAN bus
// 
// Parameters:
//		timeout  [in] Timeout for reception
//
// Returns:
//		TRUE if success, FALSE if failure
//
//-----------------------------------------------------------------------------
BOOL TestRead(DWORD timeout)
{
	HANDLE h_file = INVALID_HANDLE_VALUE;
	CANTEST_TRANSFER xfer;
	BOOL ret = FALSE;
	UINT8 data[8];
	xfer.Data = data;
	xfer.DataLen = 8;
	xfer.dwTimeout = timeout;
	CAN_FILTER f;
	HANDLE hF;

	if (xfer.Data == NULL) 
	{
		wprintf(L"Failed in LocalAlloc\r\n");
		goto cleanup;
	}
	h_file = OpenCANTESTFile(TEST_DEVICE_PREFIX);
	if (h_file == INVALID_HANDLE_VALUE)
	{
		wprintf(L"TestWrite: Failed in OpenCANTESTFile, check that driver is activated\r\n");
		goto cleanup;
	}

	f.filterFormat = FALSE; // Accept all formats
	f.filterRemote = FALSE;	// Accept remote or not
	f.IDMask = 0;			// Check no bit of the ID
	// Add a filter to accept all data
	if (!DeviceIoControl(h_file,
						 CANTEST_IOCTL_ADD_FILTER,
						 &f,
						 sizeof(f),
						 &hF,
						 sizeof(hF),
						 NULL,
						 NULL)) {
		wprintf(L" Failed in CANTEST_IOCTL_ADD_FILTER\r\n");
		goto cleanup;
	}

	// Read data
	if (!DeviceIoControl(h_file,
						CANTEST_IOCTL_READ,
						(LPVOID)&xfer,
						sizeof(xfer),
						NULL,
						0,	
						NULL,
						NULL)) {
		wprintf(L"TestWrite: Failed in DeviceIoControl\r\n");
		goto cleanup;
	}

	if (xfer.iResult != 0) {
		wprintf(L"Got transfer error %d\r\n", xfer.iResult);
		goto cleanup;
	}

	wprintf(L"%S: Got message with id 0x%x, remote %d, id len %d, data length %d\r\n",
			__FUNCTION__,
			xfer.ID,
			xfer.bRemote,
			xfer.IDLen,
			xfer.DataLen);

	for (DWORD i = 0; i < xfer.DataLen; i++)
	{
		wprintf(L"%S: Got byte %d : 0x%x\r\n", __FUNCTION__, i, xfer.Data[i]);
	}

	ret = TRUE;

cleanup :

	if (h_file != INVALID_HANDLE_VALUE) {
		CloseHandle(h_file);
	}

	return ret;
}

//-----------------------------------------------------------------------------
//
// Function:  BOOL TestRXThroughput(HANDLE h_file, 
//									DWORD duration, 
//									DWORD id, 
//									CAN_FRAME_FORMAT format)
//
// Start a TX throughput test. The PC test app should be running.
// This test measures the CAN throughput during a transfer from PC to device.
// 
// Parameters:
//      h_file	 [in] Handle on the CAN device
//		duration [in] Duration of the test
//		id		 [id] Message ID
//		format	 [id] Message format (CAN_STANDARD or CAN_EXTENDED)
//
// Returns:
//		TRUE if success, FALSE if failure
//
//-----------------------------------------------------------------------------
BOOL TestRXThroughput(HANDLE h_file, DWORD duration, DWORD id, CAN_FRAME_FORMAT format)
{
	CANTEST_TRANSFER xfer;
	BOOL ret = TRUE;
	UINT8 data[8];
	memset(data, 0, 8);
	BOOL bSeqJump = FALSE;
	DWORD expectedIdLen = (format == CAN_STANDARD) ? 11 : 29;

	xfer.Data = (PUINT8)data;
	xfer.DataLen = 8;
	xfer.ID = expectedIdLen;
	xfer.IDLen = 
	xfer.bAbortTxIfTimeout = FALSE;
	xfer.dwTimeout = 30000;
	xfer.bRemote = CAN_DATA;

	UINT8 prevSeq = 255, seq = 0;

	DWORD tFirstFrame = 0;
	DWORD tLastFrame = 0;
	DWORD tLastMessage = 0;

	// Add a filter to accept incoming frames
	CAN_FILTER f;
	HANDLE hf;				// Handle on filter (used to remove filter)
	f.filterFormat = FALSE;	// Don't check format (11 or 29 bits ID)
	f.filterRemote = FALSE;	// Don't check if frame is remote
	f.ID = id;				// Match ID 2AA
	f.IDMask = 0xFFFFFFFF;	// Entire ID must match
	DeviceIoControl(h_file, CANTEST_IOCTL_ADD_FILTER, &f, sizeof(f), &hf, sizeof(hf), NULL, 0);

	xfer.dwTimeout = 3000;
	DWORD loop = 0;
	for (;;) {
		if (!DeviceIoControl(h_file,
							CANTEST_IOCTL_READ,
							(LPVOID)&xfer,
							sizeof(xfer),
							NULL,
							0,	
							NULL,
							NULL)) {
			wprintf(L"TestWrite: Failed in DeviceIoControl\r\n");
			ret = FALSE;
			break;
		}

		if (xfer.iResult != 0) {
			wprintf(L"Got result %d\r\n", xfer.iResult);
			if (xfer.iResult == CAN_ERR_TRANSFER_TIMEOUT) {
				wprintf(L"Timeout, leaving test\r\n");
				break;
			}
			continue;
		}

		//wprintf(L"Got ID %d\r\n", xfer.ID);

		// Check flag and ID
		if (xfer.ID != id) {
			wprintf(L"ID 0x%x differs from expected ID 0x%x, aborting\r\n", xfer.ID, id);
			ret = FALSE;
			break;
		}

		if (xfer.IDLen != expectedIdLen) {
			wprintf(L"ID len %d differs from expected %d, aborting\r\n", xfer.IDLen, expectedIdLen);
			ret = FALSE;
			break;
		}

		if (loop == 0) {
			tFirstFrame = GetTickCount();
			tLastMessage = tFirstFrame;
		}

		seq = xfer.Data[0];
		if ((UINT8)(seq - 1) != prevSeq) {
			wprintf(L"Seq jumped from %d to %d\r\n", prevSeq, seq);
			bSeqJump = TRUE;
		}
		prevSeq = seq;
		
		
		memcpy(&data, xfer.Data, xfer.DataLen);
		tLastFrame = GetTickCount();
		loop++;

		if (tLastFrame - tLastMessage > 500) {
			tLastMessage = tLastFrame;
			wprintf(L"Received %d messages\r\n", loop);
		}

	}

	if (tLastFrame - tFirstFrame == 0) {
		wprintf(L"No data received\r\n");
		ret = FALSE;
	}
	else {
		wprintf(L"Read %d bytes in %d msecs (%d bytes/sec)\r\n",
				loop * 8, tLastFrame - tFirstFrame, (loop * 8 * 1000) / (tLastFrame - tFirstFrame));
	}

	if (bSeqJump) {
		printf("Some sequence jumps were detected, assuming data loss !\r\n");
		ret = FALSE;
	}

	// Remove filter
	DeviceIoControl(h_file, CANTEST_IOCTL_REMOVE_FILTER, &hf, sizeof(HANDLE), NULL, 0, NULL, NULL);

	wprintf(L"Done test\r\n");

	return ret;
}

//-----------------------------------------------------------------------------
//
// Function:  BOOL TestRXRemoteFrames(HANDLE h_file, 
//									  DWORD duration, 
//									  CAN_FRAME_FORMAT format)
//
// Start a RX throughput test with remote frames. The PC test app should be running.
// This test measures the throughput during a CAN transfer from PC to device with remote
// frames.
// 
// Parameters:
//      h_file	 [in] Handle on the CAN device
//		duration [in] Duration of the test
//		format	 [id] Message format (CAN_STANDARD or CAN_EXTENDED)
//
// Returns:
//		TRUE if success, FALSE if failure
//
//-----------------------------------------------------------------------------
BOOL TestRXRemoteFrames(HANDLE h_file, DWORD duration, CAN_FRAME_FORMAT format)
{
	CANTEST_TRANSFER xfer;
	BOOL ret = FALSE;
	BOOL bSeqJump = FALSE;

	UINT32 maxId;

	if (format == CAN_STANDARD)
		maxId = CAN_MAX_ID_STANDARD; // 11 bits max ID
	else 
		maxId = CAN_MAX_ID_EXTENDED; // 29 bits max ID

	UINT8 data[8];
	xfer.Data = data;
	xfer.DataLen = 8;
	xfer.bAbortTxIfTimeout = FALSE;
	xfer.dwTimeout = 30000;
	xfer.bRemote = CAN_DATA;

	UINT32 prevSeq = maxId, seq = 0;

	DWORD tFirstFrame = 0;
	DWORD tLastFrame = 0;
	DWORD tLastMessage = 0;

	// Add a filter to accept incoming remote frames
	CAN_FILTER f;
	HANDLE hf;				// Handle on filter (used to remove filter)
	f.filterFormat = TRUE;	// Filter format (accept 11 or 29 bits ID)
	f.format = format;		// Only accept format passed in args
	f.filterRemote = TRUE;	// Filter based on remote field
	f.remote = CAN_REMOTE;  // Remote field must be set
	f.IDMask = 0x00000000;	// No ID matching
	DeviceIoControl(h_file, CANTEST_IOCTL_ADD_FILTER, &f, sizeof(f), &hf, sizeof(hf), NULL, 0);

	xfer.dwTimeout = 3000;
	DWORD loop = 0;
	for (;;) {
		xfer.Data = data;
		xfer.DataLen = 8;
		if (!DeviceIoControl(h_file,
							CANTEST_IOCTL_READ,
							(LPVOID)&xfer,
							sizeof(xfer),
							NULL,
							0,	
							NULL,
							NULL)) {
			wprintf(L"%S: Failed in CANTEST_IOCTL_READ\r\n", __FUNCTION__);
			break;
		}

		if (xfer.iResult != 0) {
			wprintf(L"Got result %d\r\n", xfer.iResult);
			if (xfer.iResult == CAN_ERR_TRANSFER_TIMEOUT) {
				wprintf(L"Timeout, leaving test\r\n");
				break;
			}
			continue;
		}

		if (loop == 0) {
			tFirstFrame = GetTickCount();
			tLastMessage = tFirstFrame;
		}

		seq = xfer.ID;
		if ( (seq != prevSeq + 1) && (seq != 0 || prevSeq != maxId) ) {
			wprintf(L"ID jumped from %d to %d\r\n", prevSeq, seq);
			bSeqJump = TRUE;
		}
		prevSeq = seq;

		tLastFrame = GetTickCount();
		loop++;

		if (tLastFrame - tLastMessage > 500) {
			tLastMessage = tLastFrame;
			wprintf(L"Received %d messages\r\n", loop);
		}
	}

	if (tLastFrame - tFirstFrame == 0) {
		wprintf(L"No data received\r\n");
		ret = FALSE;
		goto _exit;
	}
	else {
		wprintf(L"Read %d frames in %d msecs (%d frames/sec)\r\n",
				loop, tLastFrame - tFirstFrame, (loop * 1000) / (tLastFrame - tFirstFrame));
		ret = TRUE;
	}

	if (bSeqJump) {
		printf("Some sequence jumps were detected, assuming data loss !\r\n");
		ret = FALSE;
		goto _exit;
	}

_exit :

	// Remove filter
	DeviceIoControl(h_file, CANTEST_IOCTL_REMOVE_FILTER, &hf, sizeof(HANDLE), NULL, 0, NULL, NULL);

	wprintf(L"Done test\r\n");

	return ret;
}

//-----------------------------------------------------------------------------
//
// Function:  BOOL TestTXThroughput(HANDLE h_file, 
//									DWORD duration, 
//									DWORD id, 
//									CAN_FRAME_FORMAT format)
//
// Start a TX throughput test with remote frames. The PC test app should be running.
// This test measures the throughput during a transmission from device to PC.
// 
// Parameters:
//      h_file	 [in] Handle on the CAN device
//		duration [in] Duration of the test
//		id		 [in] Message ID
//		format	 [id] Message format (CAN_STANDARD or CAN_EXTENDED)
//
// Returns:
//		TRUE if success, FALSE if failure
//
//-----------------------------------------------------------------------------
BOOL TestTXThroughput(HANDLE h_file, DWORD duration, DWORD id, CAN_FRAME_FORMAT format)
{
	CANTEST_TRANSFER xfer;
	BOOL ret = FALSE;
	UINT8 data[8];
	memset(data, 0, 8);

	xfer.Data = (PUINT8)data;
	xfer.DataLen = 8;
	xfer.ID = id;
	xfer.IDLen = format == CAN_STANDARD ? 11 : 29;
	xfer.bAbortTxIfTimeout = TRUE;
	xfer.bRemote = FALSE;
	xfer.dwTimeout = 3000;
	DWORD loop = 0;
	DWORD tStart = GetTickCount();

	// Make sure PC has time to set bitrate
	Sleep(100);

	while (GetTickCount() - tStart < duration) {
		xfer.Data[0] = (UINT8)loop;
		loop++;
		if (!DeviceIoControl(h_file,
							CANTEST_IOCTL_WRITE,
							(LPVOID)&xfer,
							sizeof(xfer),
							NULL,
							0,	
							NULL,
							NULL)) {
			wprintf(L"TestWrite: Failed in DeviceIoControl\r\n");
			break;
		}

		if (xfer.iResult != 0) {
			// All TX mailboxes are full, just continue;
			if (xfer.iResult == CAN_ERR_TX_OVERFLOW) {
				wprintf(L"TX Overflow\r\n");
				continue;
			}
			else {
				wprintf(L"TX error %d, aborting test\r\n", xfer.iResult);
				return FALSE;
			}
		}

		// Start counting when 1st frame was succesfully transmitted
		if (loop == 0)
			tStart = GetTickCount();

		//loop++;
	}

	wprintf(L"%S: Written %d bytes in %d msecs (%d bytes/sec)\r\n",
			__FUNCTION__, loop * 8, duration, (loop * 8 * 1000) / (duration));

	ret = TRUE;

	return ret;
}

//-----------------------------------------------------------------------------
//
// Function:  BOOL TestPriority(HANDLE h_file, 
//								DWORD duration)
//
// Start a TX Priority test. The PC test app should be running.
// This tests validates that the messages are sent depending on the
// CAN message ID.
// 
// Parameters:
//      h_file	 [in] Handle on the CAN device
//		duration [in] Duration of the test
//
// Returns:
//		TRUE if success, FALSE if failure
//
//-----------------------------------------------------------------------------
BOOL TestPriority(HANDLE h_file, DWORD duration)
{
	CANTEST_TRANSFER xfer;
	BOOL ret = FALSE;
	UINT8 data[8];
	memset(data, 0, 8);

	xfer.Data = (PUINT8)data;
	xfer.DataLen = 8;
	xfer.bAbortTxIfTimeout = FALSE;
	xfer.bRemote = FALSE;
	xfer.dwTimeout = 0;	// Small timeout since we expect to fall into timeout

	// Make sure PC has time to get off the bus, and try send several messages
	Sleep(100);

	DWORD tStart = GetTickCount();
	// Remove 500ms to make sure that we don't keep writting while PC is on bus
	int turn, i = 0;
	while ( (GetTickCount() - tStart) < (duration - 500) ) {
		turn = i % 3;
		i++;
		DWORD r = rand() * rand();
		switch (turn) {
			case 0 :
				// Standard ID
				xfer.ID = r & CAN_MAX_ID_STANDARD;
				xfer.IDLen = 11;
				break;
			case 1 :
				// Extended ID
				xfer.ID = r & CAN_MAX_ID_EXTENDED;
				xfer.IDLen = 29;
				break;
			case 2 :
				// Extended ID with ID in standard ID range
				xfer.ID = r & CAN_MAX_ID_STANDARD;
				xfer.IDLen = 29;
				break;
		}
		if (!DeviceIoControl(h_file,
							CANTEST_IOCTL_WRITE,
							(LPVOID)&xfer,
							sizeof(xfer),
							NULL,
							0,	
							NULL,
							NULL)) {
			wprintf(L"TestWrite: Failed in DeviceIoControl\r\n");
			break;
		}

		if (xfer.iResult != 0) {
			// All TX mailboxes are full, break
			if (xfer.iResult == CAN_ERR_TX_OVERFLOW) {
				wprintf(L"All mailboxes are full, stopping queueing\r\n");
				break;
			}
			// This is what we expect here...
			else if (xfer.iResult == CAN_ERR_TRANSFER_TIMEOUT || 
					 xfer.iResult == CAN_ERR_NO_ACK_ISSUED) {
			}
			else {
				wprintf(L"TX error %d, aborting test\r\n", xfer.iResult);
				return FALSE;
			}
		}
		wprintf(L"Queued message with id 0x%x\r\n", xfer.ID);
	}

	ret = TRUE;
	wprintf(L"Done test, please check on PC app that IDs are received in the right order\r\n");
	wprintf(L"(Smaller first, bigger last)\r\n");

	return ret;
}

//-----------------------------------------------------------------------------
//
// Function:  BOOL TestTXAbort(HANDLE h_file)
//
// Start a TX Abort test. The PC test app should be running.
// This test validates the ability of the driver to abort a TX transfer when a
// timeout occurs.
// 
// Parameters:
//      h_file	 [in] Handle on the CAN device
//
// Returns:
//		TRUE if success, FALSE if failure
//
//-----------------------------------------------------------------------------
BOOL TestTXAbort(HANDLE h_file)
{
	CANTEST_TRANSFER xfer;
	UINT8 data[8];
	memset(data, 0, 8);
	xfer.Data = (PUINT8)data;
	xfer.DataLen = 8;
	xfer.bRemote = FALSE;
	xfer.IDLen = 11;

	// Make sure PC has time to get off the bus
	Sleep(100);

	// Send a message while PC is offline with timeout and abort set

	xfer.ID = 1;
	xfer.dwTimeout = 1000;
	xfer.bAbortTxIfTimeout = TRUE;
	if (!DeviceIoControl(h_file,
						CANTEST_IOCTL_WRITE,
						(LPVOID)&xfer,
						sizeof(xfer),
						NULL,
						0,	
						NULL,
						NULL)) {
		wprintf(L"TestWrite: Failed in DeviceIoControl\r\n");
		return FALSE;
	}

	if (xfer.iResult != CAN_ERR_TRANSFER_TIMEOUT) {
		wprintf(L"__FUNCTION__: got result %d while expecting timeout (%d), aborting !\r\n",
			xfer.iResult, CAN_ERR_TRANSFER_TIMEOUT);
		return FALSE;
	}

	wprintf(L"Aborted transfer with ID %d\r\n", xfer.ID);

	// Send a message with abort not set
	xfer.ID = 2;
	xfer.dwTimeout = 1000;
	xfer.bAbortTxIfTimeout = FALSE;

	if (!DeviceIoControl(h_file,
						CANTEST_IOCTL_WRITE,
						(LPVOID)&xfer,
						sizeof(xfer),
						NULL,
						0,	
						NULL,
						NULL)) {
		wprintf(L"TestWrite: Failed in DeviceIoControl\r\n");
		return FALSE;
	}

	if (xfer.iResult != CAN_ERR_TRANSFER_TIMEOUT) {
		wprintf(L"%S : got result %d while expecting timeout (%d), aborting !\r\n",
			__FUNCTION__, xfer.iResult, CAN_ERR_TRANSFER_TIMEOUT);
		return FALSE;
	}

	wprintf(L"Not aborted transfer with ID %d\r\n", xfer.ID);

	wprintf(L"Done test, please check on PC app that message with ID 1 was NOT received\r\n");
	wprintf(L"and that message with ID 2 WAS received\r\n");

	return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BOOL TestTXSize(HANDLE h_file)
//
// Start a TX Size test. The PC test app should be running.
// This test checks that TX is functional with various sizes of buffer
// 
// Parameters:
//      h_file	 [in] Handle on the CAN device
//
// Returns:
//		TRUE if success, FALSE if failure
//
//-----------------------------------------------------------------------------
BOOL TestTXSize(HANDLE h_file)
{
	CANTEST_TRANSFER xfer;
	UINT8 data[8];
	xfer.Data = (PUINT8)data;
	xfer.bRemote = FALSE;
	xfer.IDLen = 11;
	xfer.ID = 0x33;
	xfer.dwTimeout = 1;
	xfer.bAbortTxIfTimeout = TRUE;

	// Make sure PC has time to get off the bus
	Sleep(100);

	// Setup data
	for (int i = 0; i < 8; i++) {
		data[i] = (UINT8)i;
	}

	// Send a message with size from 0 to 8
	for (int i = 0; i <= 8; i++) {
		xfer.DataLen = i;
		if (!DeviceIoControl(h_file,
							CANTEST_IOCTL_WRITE,
							(LPVOID)&xfer,
							sizeof(xfer),
							NULL,
							0,	
							NULL,
							NULL)) {
			wprintf(L"%S: Failed in DeviceIoControl\r\n", __FUNCTION__);
			return FALSE;
		}

		if (xfer.iResult != CAN_NO_ERROR) {
			wprintf(L"%S: Failed in CANTEST_IOCTL_WRITE, error %d, aborting test\r\n", __FUNCTION__, xfer.iResult);
			return FALSE;
		}
		wprintf(L"%S: Transmitted message with size %d\r\n", __FUNCTION__, i);
	}

	wprintf(L"%S: Done test, please check on PC app that messages with sizes from 0 to 8 were received\r\n", __FUNCTION__);

	return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BOOL TestRXSize(HANDLE h_file)
//
// Start a RX Size test. The PC test app should be running.
// This test checks that RX is functional with various sizes of buffer
// 
// Parameters:
//      h_file	 [in] Handle on the CAN device
//
// Returns:
//		TRUE if success, FALSE if failure
//
//-----------------------------------------------------------------------------
BOOL TestRXSize(HANDLE h_file)
{
	CANTEST_TRANSFER xfer;
	UINT8 data[8];
	xfer.Data = (PUINT8)data;
	xfer.DataLen = 8;
	xfer.dwTimeout = 1000;

	// Setup a filter to receive all incoming messages
	CAN_FILTER f;
	HANDLE hF;
	f.filterFormat = FALSE;
	f.filterRemote = FALSE;
	f.IDMask = 0x0;

	if (!DeviceIoControl(h_file,
						 CANTEST_IOCTL_ADD_FILTER,
						 &f,
						 sizeof(f),
						 &hF,
						 sizeof(hF),
						 NULL,
						 NULL)) {
		wprintf(L"%S: Failed in CANTEST_IOCTL_ADD_FILTER\r\n");
		return FALSE;
	}

	// Setup data
	for (int i = 0; i < 8; i++) {
		data[i] = (UINT8)i;
	}

	// Receive messages with size from 0 to 8, expect success
	for (int i = 0; i <= 8; i++) {
		xfer.DataLen = i;
		if (!DeviceIoControl(h_file,
							CANTEST_IOCTL_READ,
							(LPVOID)&xfer,
							sizeof(xfer),
							NULL,
							0,	
							NULL,
							NULL)) {
			wprintf(L"%S: Failed in DeviceIoControl\r\n", __FUNCTION__);
			return FALSE;
		}

		if (xfer.iResult != CAN_NO_ERROR) {
			wprintf(L"%S: Failed in CANTEST_IOCTL_READ, error %d, aborting test\r\n", __FUNCTION__, xfer.iResult);
			return FALSE;
		}

		// Check data
		for (int j = 0; j < i; j++) {
			if (data[j] != j) {
				wprintf(L"S: Data at index %d:0x%x is not valid, aborting test\r\n", __FUNCTION__, j, data[j]);
				return FALSE;
			}
		}
		wprintf(L"%S: Received message with size %d, data are conform\r\n", __FUNCTION__, i);
	}

	// Receive messages with size 0, expect success on 1st transfer only
	for (int i = 0; i <= 8; i++) {
		xfer.DataLen = 0;
		if (!DeviceIoControl(h_file,
							CANTEST_IOCTL_READ,
							(LPVOID)&xfer,
							sizeof(xfer),
							NULL,
							0,	
							NULL,
							NULL)) {
			wprintf(L"%S: Failed in DeviceIoControl\r\n", __FUNCTION__);
			return FALSE;
		}

		// 1st transfer should be 0 length
		if (i == 0) {
			if (xfer.iResult != CAN_NO_ERROR) {
				wprintf(L"%S: Failed in CANTEST_IOCTL_READ, error %d, aborting test\r\n", __FUNCTION__, xfer.iResult);
				return FALSE;
			}
			wprintf(L"%S: Received message with size %d\r\n", __FUNCTION__, i);
		}
		else if (xfer.iResult == CAN_ERR_INSUFFICIENT_SPACE) {
			wprintf(L"%S: Failed in CANTEST_IOCTL_READ, error CAN_ERR_INSUFFICIENT_SPACE as expected\r\n", __FUNCTION__);
		}
		else {
			wprintf(L"%S: Unexepected error %d in CANTEST_IOCTL_READ, aborting test\r\n", __FUNCTION__);
			return FALSE;
		}
	}

	return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BOOL TestRXAbort(HANDLE h_file)
//
// Start a RX Abort test. The PC test app should be running.
// This test checks that RX timeout is functional.
// 
// Parameters:
//      h_file	 [in] Handle on the CAN device
//
// Returns:
//		TRUE if success, FALSE if failure
//
//-----------------------------------------------------------------------------
BOOL TestRXAbort(HANDLE h_file, DWORD duration)
{
	CANTEST_TRANSFER xfer;
	UINT8 data[8];
	memset(data, 0, 8);
	xfer.Data = (PUINT8)data;
	xfer.DataLen = 8;
	xfer.bRemote = FALSE;
	xfer.dwTimeout = duration;
	xfer.bAbortTxIfTimeout = TRUE;

	wprintf(L"Reading data with timeout %d\r\n", xfer.dwTimeout);

	DWORD tStart = GetTickCount();

	// Wait for a message, not expecting to receive anything
	if (!DeviceIoControl(h_file,
						CANTEST_IOCTL_READ,
						(LPVOID)&xfer,
						sizeof(xfer),
						NULL,
						0,	
						NULL,
						NULL)) {
		wprintf(L"%S: TestWrite: Failed in DeviceIoControl\r\n", __FUNCTION__);
		return FALSE;
	}

	if (xfer.iResult != CAN_ERR_TRANSFER_TIMEOUT) {
		wprintf(L"%S: got result %d while expecting timeout (%d), aborting !\r\n",
			__FUNCTION__, xfer.iResult, CAN_ERR_TRANSFER_TIMEOUT);
		return FALSE;
	}

	DWORD diff = GetTickCount() - tStart;

	// Check that wait time was requested time +/- 500 msec
	wprintf(L"Did wait %d ms\r\n", diff);

	if ( (diff < (duration - 500) ) || (diff > (duration + 500) ) ) {
		wprintf(L"Wait value is too far from timeout !\r\n");
		return FALSE;
	}

	wprintf(L"Timeout reading data as expected, wait value (%d) is conform to expected time (%d)\r\n",
		diff, duration);

	return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BOOL TestTXRemoteFrames(HANDLE h_file, 
//									  DWORD duration, 
//									  CAN_FRAME_FORMAT format)
//
// Start a TX remote frames test. The PC test app should be running.
// This test checks that emission of remote frames from device to PC is
// functional.
// 
// Parameters:
//      h_file	 [in] Handle on the CAN device
//		duration [in] Duration of the test
//		format   [in] Messages format (CAN_STANDARD or CAN_EXTENDED)
//
// Returns:
//		TRUE if success, FALSE if failure
//
//-----------------------------------------------------------------------------
BOOL TestTXRemoteFrames(HANDLE h_file, DWORD duration, CAN_FRAME_FORMAT format)
{
	CANTEST_TRANSFER xfer;
	BOOL ret = FALSE;
	DWORD id = 0;
	DWORD idLen = (format == CAN_STANDARD ? 11 : 29);

	xfer.bAbortTxIfTimeout = TRUE;
	xfer.bRemote = TRUE;
	xfer.dwTimeout = 3000;
	DWORD tStart = GetTickCount();

	// Make sure PC has time to set bitrate
	Sleep(100);

	while (GetTickCount() - tStart < duration) {
		xfer.ID = id;
		xfer.IDLen = idLen;
		xfer.Data = NULL;
		xfer.DataLen = id % 8; // On TX remote frames, len field indicates length of expected response
		xfer.dwTimeout = 3000;

		if (!DeviceIoControl(h_file,
							CANTEST_IOCTL_WRITE,
							(LPVOID)&xfer,
							sizeof(xfer),
							NULL,
							0,	
							NULL,
							NULL)) {
			wprintf(L"%S: Failed in DeviceIoControl\r\n", __FUNCTION__);
			break;
		}

		if (xfer.iResult != 0) {
			// All TX mailboxes are full, just continue;
			if (xfer.iResult == CAN_ERR_TX_OVERFLOW) {
				wprintf(L"TX Overflow\r\n");
				continue;
			}
			else {
				wprintf(L"TX error %d, aborting test\r\n", xfer.iResult);
				return FALSE;
			}
		}

		// Start counting when 1st frame was succesfully transmitted
		if (id == 1)
			tStart = GetTickCount();

		id++;
	}

	wprintf(L"Written %d frames in %d msecs (%d bytes/sec)\r\n",
			id, duration, id * 1000 / duration);

	ret = TRUE;
	wprintf(L"Done test\r\n");

	return ret;
}

//-----------------------------------------------------------------------------
//
// Function:  BOOL TestFilters(HANDLE h_file, 
//							   DWORD duration)
//
// Start a filters test. The PC test app should be running.
// This test checks that reception filters are functional.
// 
// Parameters:
//      h_file	 [in] Handle on the CAN device
//		duration [in] Duration of the test
//
// Returns:
//		TRUE if success, FALSE if failure
//
//-----------------------------------------------------------------------------
BOOL TestFilters(HANDLE h_file, DWORD duration)
{
	CAN_FILTER f1, f2, f3;
	HANDLE hf1, hf2, hf3;
	CANTEST_TRANSFER xfer;
	UINT8 data[8];
	DWORD msgCount;
	DWORD nbPasses = 4;
	DWORD pass;
	DWORD tStart;
	duration -= 1000;

	wprintf(L"%S: Adding filter 1: accept all standard frames with id 0x33\r\n", __FUNCTION__);

	// Setup filter 1
	// This filter accepts all frames with ID 0x33
	f1.filterFormat = FALSE;	// Don't filter format (accept 11 and 29 bits IDs)
	f1.filterRemote = FALSE;	// Don't filter remote (accept data and remote frames)
	f1.ID = 0x33;				// Accept ID 0x33
	f1.IDMask = 0xFFFFFFFF;		// Check all ID bits
	if (!DeviceIoControl(h_file,
						CANTEST_IOCTL_ADD_FILTER,
						&f1,
						sizeof(CAN_FILTER),
						&hf1,
						sizeof(HANDLE),
						NULL,
						NULL)) {
		wprintf(L"%S: Failed in CANTEST_IOCTL_ADD_FILTER for f1, aborting test\r\n", __FUNCTION__);
		return FALSE;
	}
	wprintf(L"%S: Added filter 1: accept all messages with ID 0x33\r\n", __FUNCTION__);

	// Start a RX transfer and check that incoming messages all have 0x33 ID
	msgCount = 0;
	tStart = GetTickCount();
	pass = 1;
	wprintf(L"%S: Entering pass %d\r\n", __FUNCTION__, pass);
	while (GetTickCount() - tStart < duration / nbPasses) {
		xfer.Data = data;
		xfer.DataLen = 8;
		xfer.dwTimeout = 300;
		if (!DeviceIoControl(h_file, CANTEST_IOCTL_READ, &xfer, sizeof(xfer), NULL, 0, NULL, NULL)) {
			wprintf(L"%S: Failed in CANTEST_IOCTL_READ in pass %d, aborting test\r\n", __FUNCTION__, pass);
			return FALSE;
		}
		if (xfer.iResult == CAN_NO_ERROR) {
			wprintf(L"%S: Got message with ID 0x%x\r\n", __FUNCTION__, xfer.ID);
			if (xfer.ID != 0x33) {
				wprintf(L"%S: Unexpected message ID 0x%x in pass %d\r\n", __FUNCTION__, xfer.ID, pass);
				return FALSE;
			}
			msgCount++;
		}
		else {
			wprintf(L"%S: Got transfer error %d in pass %d, aborting test\r\n", __FUNCTION__, xfer.iResult, pass);
			return FALSE;
		}
	}

	if (msgCount == 0) {
		wprintf(L"%S: No message received after pass %d, aborting test\r\n", __FUNCTION__, pass);
		return FALSE;
	}

	wprintf(L"%S: Adding filter 2: accept all messages with remote flag\r\n", __FUNCTION__);

	// Setup filter 2
	// This filter accepts all remote frames
	f2.filterFormat = FALSE;	// Don't filter format
	f2.filterRemote = TRUE;		// Filter remote
	f2.remote = CAN_REMOTE;		// Only accept remote frames
	f2.IDMask = 0x0;			// Disable ID check (no bit is checked)
	if (!DeviceIoControl(h_file,
						CANTEST_IOCTL_ADD_FILTER,
						&f2,
						sizeof(CAN_FILTER),
						&hf2,
						sizeof(HANDLE),
						NULL,
						NULL)) {
		wprintf(L"%S: Failed in CANTEST_IOCTL_ADD_FILTER for f2\r\n", __FUNCTION__);
		return FALSE;
	}

	// Start a RX transfer and check that incoming messages all have 0x33 ID
	// are remote messages
	tStart = GetTickCount();
	msgCount = 0;
	pass = 2;
	wprintf(L"%S: Entering pass %d\r\n", __FUNCTION__, pass);
	while (GetTickCount() - tStart < duration / nbPasses) {
		xfer.Data = data;
		xfer.DataLen = 8;
		xfer.dwTimeout = 300;
		if (!DeviceIoControl(h_file, CANTEST_IOCTL_READ, &xfer, sizeof(xfer), NULL, 0, NULL, NULL)) {
			wprintf(L"%S: Failed in CANTEST_IOCTL_READ in pass %d, aborting test\r\n", __FUNCTION__, pass);
			return FALSE;
		}
		if (xfer.iResult == CAN_NO_ERROR) {
			wprintf(L"%S: Got message with ID 0x%x, remote flag %d\r\n", __FUNCTION__, xfer.ID, xfer.bRemote);
			if (xfer.ID != 0x33 && !xfer.bRemote) {
				wprintf(L"%S: Unexpected message ID 0x%x without remote flag in pass %d\r\n", __FUNCTION__, xfer.ID, pass);
				return FALSE;
			}
			msgCount++;
		}
		else {
			wprintf(L"%S: Got transfer error %d in pass %d, aborting test\r\n", __FUNCTION__, xfer.iResult, pass);
			return FALSE;
		}
	}

	if (msgCount == 0) {
		wprintf(L"%S: No message received after pass %d, aborting test\r\n", __FUNCTION__, pass);
		return FALSE;
	}

	wprintf(L"%S: Adding filter 3: accept all messages with extended flag\r\n", __FUNCTION__);

	// Setup filter 3
	// This filter accepts all extended messages
	f3.filterFormat = TRUE;		// Filter format
	f3.format = CAN_EXTENDED;	// Only accept extended messages
	f3.filterRemote = FALSE;	// Don't filter remote
	f3.IDMask = 0x0;			// Disable ID check (no bit is checked)
	if (!DeviceIoControl(h_file,
						CANTEST_IOCTL_ADD_FILTER,
						&f3,
						sizeof(CAN_FILTER),
						&hf3,
						sizeof(HANDLE),
						NULL,
						NULL)) {
		wprintf(L"%S: Failed in CANTEST_IOCTL_ADD_FILTER for f3\r\n", __FUNCTION__);
		return FALSE;
	}

	// Start a RX transfer and check that incoming messages all have 0x33 ID
	// are remote messages
	tStart = GetTickCount();
	msgCount = 0;
	pass = 3;
	wprintf(L"%S: Entering pass %d\r\n", __FUNCTION__, pass);
	while (GetTickCount() - tStart < duration / nbPasses) {
		xfer.Data = data;
		xfer.DataLen = 8;
		xfer.dwTimeout = 300;
		if (!DeviceIoControl(h_file, CANTEST_IOCTL_READ, &xfer, sizeof(xfer), NULL, 0, NULL, NULL)) {
			wprintf(L"%S: Failed in CANTEST_IOCTL_READ in pass %d, aborting test\r\n", __FUNCTION__, pass);
			return FALSE;
		}
		if (xfer.iResult == CAN_NO_ERROR) {
			wprintf(L"%S: Got message with ID 0x%x, remote flag %d, extended flag %d\r\n", __FUNCTION__, xfer.ID, xfer.bRemote, xfer.IDLen == 29);
			if (xfer.ID != 0x33 && !xfer.bRemote && (xfer.IDLen != 29) ) {
				wprintf(L"%S: Unexpected message ID 0x%x without remote or extended flag in pass %d\r\n", __FUNCTION__, xfer.ID, pass);
				return FALSE;
			}
			msgCount++;
		}
		else {
			wprintf(L"%S: Got transfer error %d in pass %d, aborting test\r\n", __FUNCTION__, xfer.iResult, pass);
			return FALSE;
		}
	}

	if (msgCount == 0) {
		wprintf(L"%S: No message received after pass %d, aborting test\r\n", __FUNCTION__, pass);
		return FALSE;
	}

	wprintf(L"%S: Removing filter 1: accept standard messages with ID 0x33\r\n", __FUNCTION__);

	if (!DeviceIoControl(h_file,
						 CANTEST_IOCTL_REMOVE_FILTER,
						 &hf1,
						 sizeof(HANDLE),
						 NULL,
						 0,
						 NULL,
						 NULL)) {
		wprintf(L"%S: Failed in CANTEST_IOCTL_REMOVE_FILTER for f1, aborting test\r\n", __FUNCTION__);
		return FALSE;
	}

	// Start a RX transfer and check that incoming messages all have 0x33 ID
	// are remote messages
	tStart = GetTickCount();
	msgCount = 0;
	pass = 4;
	wprintf(L"%S: Entering pass %d\r\n", __FUNCTION__, pass);
	while (GetTickCount() - tStart < duration / nbPasses) {
		xfer.Data = data;
		xfer.DataLen = 8;
		xfer.dwTimeout = 300;
		if (!DeviceIoControl(h_file, CANTEST_IOCTL_READ, &xfer, sizeof(xfer), NULL, 0, NULL, NULL)) {
			wprintf(L"%S: Failed in CANTEST_IOCTL_READ in pass %d, aborting test\r\n", __FUNCTION__, pass);
			return FALSE;
		}
		if (xfer.iResult == CAN_NO_ERROR) {
			wprintf(L"%S: Got message with ID 0x%x, remote flag %d, extended flag %d\r\n", __FUNCTION__, xfer.ID, xfer.bRemote, xfer.IDLen == 29);
			if (!xfer.bRemote && (xfer.IDLen != 29) ) {
				wprintf(L"%S: Unexpected message ID 0x%x without remote or extended flag in pass %d\r\n", __FUNCTION__, xfer.ID, pass);
				return FALSE;
			}
			msgCount++;
		}
		else {
			wprintf(L"%S: Got transfer error %d in pass %d, aborting test\r\n", __FUNCTION__, xfer.iResult, pass);
			return FALSE;
		}
	}

	if (msgCount == 0) {
		wprintf(L"%S: No message received after pass %d, aborting test\r\n", __FUNCTION__, pass);
		return FALSE;
	}

	return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BOOL TestSetLoopback(BOOL enable)
//
// This function enables the CAN controller loopback mode.
// 
// Parameters:
//		enable [in] Should loopback mode be enabled ?
//
// Returns:
//		TRUE if success, FALSE if failure
//
//-----------------------------------------------------------------------------
BOOL TestSetLoopback(BOOL enable)
{
	HANDLE h_file = INVALID_HANDLE_VALUE;
	BOOL b_ret = FALSE;
	DWORD dw_code;

	h_file = OpenCANTESTFile(TEST_DEVICE_PREFIX);

	if (h_file == INVALID_HANDLE_VALUE)
	{
		wprintf(L"TestEnableLoopback: Failed in OpenCANTESTFile, check that driver is activated\r\n");
		goto cleanup;
	}

	dw_code = enable ? CANTEST_IOCTL_ENABLE_LOOPBACK : CANTEST_IOCTL_DISABLE_LOOPBACK;

	if (!DeviceIoControl(h_file,
						dw_code,
						NULL,
						0,
						NULL,
						0,	
						NULL,
						NULL)) {
		wprintf(L"TestEnableLoopback: Failed in DeviceIoControl\r\n");
		goto cleanup;
	}

	b_ret = TRUE;

cleanup :
	if (h_file != INVALID_HANDLE_VALUE) {
		CloseHandle(h_file);
	}

	return b_ret;
}

//-----------------------------------------------------------------------------
//
// Function:  BOOL TestSetTiming(UINT16 ps, 
//								 UINT8 prop, 
//								 UINT8 p1, 
//								 UINT8 p2, 
//								 UINT8 rjw)
//
// This function sets the timing parameters of the CAN controller.
// 
// Parameters:
//		ps		[in] Prescaler value
//		prop	[in] Propagation segment length
//		p1		[in] Phase 1 segment length
//		p2		[in] Phase 2 segment length
//		rjw		[in] Resync jump width
//
// Returns:
//		TRUE if success, FALSE if failure
//
//-----------------------------------------------------------------------------
BOOL TestSetTiming(UINT16 ps, UINT8 prop, UINT8 p1, UINT8 p2, UINT8 rjw)
{
	HANDLE h_file = INVALID_HANDLE_VALUE;
	BOOL b_ret = FALSE;
	CAN_TIMING timing;

	timing.Prescaler = ps;
	timing.PropSeg = prop;
	timing.Phase1Seg = p1;
	timing.Phase2Seg = p2;
	timing.RJW = rjw;
	
	h_file = OpenCANTESTFile(TEST_DEVICE_PREFIX);

	if (h_file == INVALID_HANDLE_VALUE)
	{
		wprintf(L"%S: Failed in OpenCANTESTFile, check that driver is activated\r\n", __FUNCTION__);
		goto cleanup;
	}

	if (!DeviceIoControl(h_file,
						CANTEST_IOCTL_GET_TIMING,
						NULL,
						0,
						&timing,
						sizeof(CAN_TIMING),	
						NULL,
						NULL)) {
		wprintf(L"%S: Failed in CANTEST_IOCTL_GET_TIMING\r\n", __FUNCTION__);
		goto cleanup;
	}

	if (ps != 0)
		timing.Prescaler = ps;
	if (p1 != 0)
		timing.Phase1Seg = p1;
	if (p2 != 0)
		timing.Phase2Seg = p2;
	if (prop != 0)
		timing.PropSeg = prop;

	if (!DeviceIoControl(h_file,
						CANTEST_IOCTL_SET_TIMING,
						&timing,
						sizeof(timing),
						NULL,
						0,	
						NULL,
						NULL)) {
		wprintf(L"%S: Failed in CANTEST_IOCTL_SET_TIMING\r\n", __FUNCTION__);
		goto cleanup;
	}

	b_ret = TRUE;

	// This will display new timings
	TestGetTiming();

cleanup :
	if (h_file != INVALID_HANDLE_VALUE) {
		CloseHandle(h_file);
	}

	return b_ret;
}

//-----------------------------------------------------------------------------
//
// Function:  void TestGetTiming()
//
// This function gets and displays the timing parameters of the CAN controller.
// 
// Parameters:
//		None
//
// Returns:
//		TRUE if success, FALSE if failure
//
//-----------------------------------------------------------------------------
void TestGetTiming()
{
	HANDLE h_file = INVALID_HANDLE_VALUE;
	BOOL b_ret = FALSE;
	CAN_TIMING timing;

	h_file = OpenCANTESTFile(TEST_DEVICE_PREFIX);

	if (h_file == INVALID_HANDLE_VALUE)
	{
		wprintf(L"%S: Failed in OpenCANTESTFile, check that driver is activated\r\n", __FUNCTION__);
		goto cleanup;
	}

	if (!DeviceIoControl(h_file,
						CANTEST_IOCTL_GET_TIMING,
						NULL,
						0,
						&timing,
						sizeof(timing),	
						NULL,
						NULL)) {
		wprintf(L"%S: Failed in DeviceIoControl\r\n", __FUNCTION__);
		goto cleanup;
	}

	wprintf(L"*****\r\n");
	wprintf(L"Current timing parameters are:\r\n");
	wprintf(L" - Base clock  : %d Hz\r\n", timing.BaseClock);
	wprintf(L" - Prescaler   : %d\r\n", timing.Prescaler);
	wprintf(L" - Propagation : %d quanta\r\n", timing.PropSeg);
	wprintf(L" - Phase seg 1 : %d quanta\r\n", timing.Phase1Seg);
	wprintf(L" - Phase seg 2 : %d quanta\r\n", timing.Phase2Seg);
	wprintf(L" - RJW         : %d quanta\r\n", timing.RJW);
	wprintf(L"Bitrate : %d bps, sample point is at %d %%\r\n", 
		(timing.BaseClock / timing.Prescaler) / (1 + timing.Phase1Seg + timing.Phase2Seg + timing.PropSeg),
		100 * (1 + timing.PropSeg + timing.Phase1Seg) / (1 + timing.PropSeg + timing.Phase1Seg + timing.Phase2Seg));

	b_ret = TRUE;

cleanup :
	if (h_file != INVALID_HANDLE_VALUE) {
		CloseHandle(h_file);
	}

	return;
}

//-----------------------------------------------------------------------------
//
// Function:  BOOL TestSetBitrate(UINT32 rate)
//
// This function sets the requested bitrate.
// Note: the timing parameters are not modified, except the prescaler.
// 
// Parameters:
//		rate [in] : The bitrate to set, in bps
//
// Returns:
//		TRUE if success, FALSE if failure
//
//-----------------------------------------------------------------------------
BOOL TestSetBitrate(UINT32 rate)
{
	HANDLE h_file = INVALID_HANDLE_VALUE;
	BOOL b_ret = FALSE;
	UINT32 actRate;
	
	h_file = OpenCANTESTFile(TEST_DEVICE_PREFIX);

	if (h_file == INVALID_HANDLE_VALUE)
	{
		wprintf(L"%S: Failed in OpenCANTESTFile, check that driver is activated\r\n", __FUNCTION__);
		goto cleanup;
	}

	if (!DeviceIoControl(h_file,
						CANTEST_IOCTL_SET_BITRATE,
						&rate,
						sizeof(UINT32),
						&actRate,
						sizeof(UINT32),	
						NULL,
						NULL)) {
		wprintf(L"%S: Failed in DeviceIoControl\r\n", __FUNCTION__);
		goto cleanup;
	}

	wprintf(L"Requested bitrate %d bps, actual %d bps\r\n", rate, actRate);

	b_ret = TRUE;

cleanup :
	if (h_file != INVALID_HANDLE_VALUE) {
		CloseHandle(h_file);
	}

	return b_ret;
}

//-----------------------------------------------------------------------------
//
// Function:  BOOL TestGetBitrate()
//
// This function gets and displays the current bitrate
// 
// Parameters:
//		None
//
// Returns:
//		TRUE if success, FALSE if failure
//
//-----------------------------------------------------------------------------
BOOL TestGetBitrate()
{
	HANDLE h_file = INVALID_HANDLE_VALUE;
	BOOL b_ret = FALSE;
	UINT32 rate;

	h_file = OpenCANTESTFile(TEST_DEVICE_PREFIX);

	if (h_file == INVALID_HANDLE_VALUE)
	{
		wprintf(L"%S: Failed in OpenCANTESTFile, check that driver is activated\r\n", __FUNCTION__);
		goto cleanup;
	}

	if (!DeviceIoControl(h_file,
						CANTEST_IOCTL_GET_BITRATE,
						NULL,
						0,
						&rate,
						sizeof(UINT32),	
						NULL,
						NULL)) {
		wprintf(L"%S: Failed in DeviceIoControl\r\n", __FUNCTION__);
		goto cleanup;
	}

	wprintf(L"Current bitrate is %d bps\r\n", rate);

	b_ret = TRUE;

cleanup :
	if (h_file != INVALID_HANDLE_VALUE) {
		CloseHandle(h_file);
	}

	return b_ret;
}

//-----------------------------------------------------------------------------
//
// Function:  VOID ListTests()
//
// This function displays available tests
// 
// Parameters:
//		None
//
// Returns:
//		None
//
//-----------------------------------------------------------------------------
VOID ListTests() 
{
	wprintf(L"Available tests:\r\n");
	for (int i = 0; i <= TEST_MAX; i++) {
		wprintf(L"  - %02d: %s\r\n", i, TESTS_NAMES[i]);
	}
}

//-----------------------------------------------------------------------------
//
// Function:  BOOL RunTest(INT8 testId, 
//						   DWORD durationMs, 
//						   UINT32 bitrate)
//
// This function runs the given test
// 
// Parameters:
//		testId		[in] ID of the test to run
//		durationMs  [in] Duration of the test (some tests may ignore this value)
//		bitrate		[in] Bitrate at which the test should be run (some tests may ignore this value)
//
// Returns:
//		None
//
//-----------------------------------------------------------------------------
BOOL RunTest(INT8 testId, DWORD durationMs, UINT32 bitrate)
{
	HANDLE h_file = INVALID_HANDLE_VALUE;
	BOOL b_ret = TRUE;
	TCHAR sMessage[512];
	TCHAR sStars[512];

	// Check that test id is valid
	if ( (testId < 0) || (testId > TEST_MAX) ) {
		if (testId > 0)
			wprintf(L"Test ID %d is not valid (max %d) !\r\n", testId, TEST_MAX);
		ListTests();
		return FALSE;
	}

	h_file = OpenCANTESTFile(TEST_DEVICE_PREFIX);

	if (h_file == INVALID_HANDLE_VALUE)
	{
		wprintf(L"%S: Failed in OpenCANTESTFile, check that driver is activated\r\n", __FUNCTION__);
		goto cleanup;
	}


	// Formatting
	wsprintf(sMessage, L"*** Starting test %03d \"%s\", bitrate %d, duration %d ms ***\r\n",
		testId, TESTS_NAMES[testId], bitrate, durationMs);
	wcscpy(sStars, L"");
	for (size_t i = 0; i < wcslen(sMessage) - 2; i++) {
		wcscat(sStars, L"*");
	}
	wcscat(sStars, L"\r\n");
	wprintf(L"%s%s%s", sStars, sMessage, sStars);

	// Set default bitrate for tests commands (500kpbs)
	UINT32 defRate = DEFAULT_BITRATE;
	UINT32 actRate;
	if (!DeviceIoControl(h_file,
						CANTEST_IOCTL_SET_BITRATE,
						&defRate,
						sizeof(UINT32),
						&actRate,
						sizeof(UINT32),
						NULL,
						NULL)) {
		wprintf(L"%S: Failed in CANTEST_IOCTL_SET_BITRATE\r\n", __FUNCTION__);
		goto cleanup;
	}

	wprintf(L"%S: Set bitrate %d bps to transmit command\r\n", __FUNCTION__, defRate);

	// Send the test command frame to the device
	UINT8 data[8];
	data[0] = testId;
	data[1] = (UINT8)(1000000 / bitrate);
	data[2] = (UINT8)(durationMs / 1000);
	CANTEST_TRANSFER xfer;
	xfer.bAbortTxIfTimeout = TRUE;
	xfer.bRemote = FALSE;
	xfer.Data = data;
	xfer.DataLen = 3;
	xfer.dwTimeout = 2000;
	xfer.ID = COMMAND_ID;
	xfer.IDLen = 11;
	if (DeviceIoControl(h_file,
						CANTEST_IOCTL_WRITE,
						&xfer,
						sizeof(CANTEST_TRANSFER),
						NULL,
						0,
						NULL,
						NULL) == FALSE) {
		wprintf(L"%S: Failed sending command frame, error %d\r\n", __FUNCTION__, xfer.iResult);
		goto cleanup;
	}

	wprintf(L"%S: Sent command frame, will set test bitrate %d\r\n", __FUNCTION__, bitrate);

	// Set bitrate for test
	if (!DeviceIoControl(h_file,
						CANTEST_IOCTL_SET_BITRATE,
						&bitrate,
						sizeof(UINT32),
						&actRate,
						sizeof(UINT32),
						NULL,
						NULL)) {
		wprintf(L"%S: Failed in CANTEST_IOCTL_SET_BITRATE\r\n", __FUNCTION__);
		goto cleanup;
	}

	wprintf(L"%S: Set bitrate %d bps for test\r\n", __FUNCTION__, bitrate);

	switch (testId) {
		case TEST_RX_THROUGHPUT_STANDARD :
		{
			b_ret = TestRXThroughput(h_file, durationMs, TEST_STANDARD_ID, CAN_STANDARD);
		}
		break;

		case TEST_TX_THROUGHPUT_STANDARD :
		{
			b_ret = TestTXThroughput(h_file, durationMs, TEST_STANDARD_ID, CAN_STANDARD);
		}
		break;

		case TEST_RX_THROUGHPUT_EXTENDED :
		{
			b_ret = TestRXThroughput(h_file, durationMs, TEST_EXTENDED_ID, CAN_EXTENDED);
		}
		break;

		case TEST_TX_THROUGHPUT_EXTENDED :
		{
			b_ret = TestTXThroughput(h_file, durationMs, TEST_EXTENDED_ID, CAN_EXTENDED);
		}
		break;

		case TEST_RX_REMOTE_STANDARD :
		{
			b_ret = TestRXRemoteFrames(h_file, durationMs, CAN_STANDARD);
		}
		break;

		case TEST_TX_REMOTE_STANDARD :
		{
			b_ret = TestTXRemoteFrames(h_file, durationMs, CAN_STANDARD);
		}
		break;

		case TEST_RX_REMOTE_EXTENDED :
		{
			b_ret = TestRXRemoteFrames(h_file, durationMs, CAN_EXTENDED);
		}
		break;

		case TEST_TX_REMOTE_EXTENDED :
		{
			b_ret = TestTXRemoteFrames(h_file, durationMs, CAN_EXTENDED);
		}
		break;

		case TEST_PRIORITY :
		{
			b_ret = TestPriority(h_file, durationMs);
		}
		break;

		case TEST_TX_ABORT :
		{
			b_ret = TestTXAbort(h_file);
		}
		break;

		case TEST_RX_ABORT :
		{
			b_ret = TestRXAbort(h_file, durationMs);
		}
		break;

		case TEST_FILTERS :
		{
			b_ret = TestFilters(h_file, durationMs);
		}
		break;

		case TEST_TX_SIZE :
		{
			b_ret = TestTXSize(h_file);
		}
		break;

		case TEST_RX_SIZE :
		{
			b_ret = TestRXSize(h_file);
		}
		break;

		default :
		{
			wprintf(L"%S: Bad test ID %d\r\n", __FUNCTION__, testId);
			b_ret = FALSE;
			break;
		}
	}

	wprintf(L"**********************\r\n");
	wprintf(L"*** TEST RESULT ******\r\n");
	wprintf(L"*** TEST ID %03d ******\r\n", testId);
	if (b_ret)
	wprintf(L"*** SUCCESS **********\r\n");
	else {
	wprintf(L"*** FAILURE !!! ******\r\n");
	}
	wprintf(L"**********************\r\n");
cleanup :
	if (h_file != INVALID_HANDLE_VALUE) {
		CloseHandle(h_file);
	}

	return b_ret;
}

// This structure is used to share information amongst loopback test main function and loopback thread
typedef struct
{
	DWORD duration;
	DWORD id;
	UINT8 val;
	DWORD len;
	BOOL  success;
	BOOL  aborted;
}TEST_LOOPBACK_ARG, *PTEST_LOOPBACK_ARG;

//-----------------------------------------------------------------------------
//
// Function:  DWORD cbTestLoopbackRx(VOID *arg)
//
// This function is the test loopback thread entry point
// 
// Parameters:
//		arg		[in] A pointer on a TEST_LOOPBACK_ARG structure
//
// Returns:
//		0 if success, -1 if failure
//
//-----------------------------------------------------------------------------
DWORD cbTestLoopbackRx(VOID *arg)
{
	DWORD t_start;
	PTEST_LOOPBACK_ARG p_test_arg = (PTEST_LOOPBACK_ARG)arg;
	CANTEST_TRANSFER xfer;
	PUINT8 data = NULL;
	HANDLE h_file = INVALID_HANDLE_VALUE;
	p_test_arg->success = FALSE;

	wprintf(L"In cbTestLoopback\r\n");

	data = (PUINT8)LocalAlloc(LPTR, p_test_arg->len);
	if (data == NULL) {
		wprintf(L"cbTestLoopback: failed in LocalAlloc\r\n");
		goto cleanup;
	}

	h_file = OpenCANTESTFile(TEST_DEVICE_PREFIX);
	if (h_file == INVALID_HANDLE_VALUE) {
		wprintf(L"cbTestLoopback: failed in OpenCANTESTFile\r\n");
		goto cleanup;
	}

	t_start = GetTickCount();

	while (GetTickCount() - t_start < p_test_arg->duration) {
		memset(data, 0, p_test_arg->len);
		xfer.Data = data;
		xfer.DataLen = p_test_arg->len;

		if (!DeviceIoControl(h_file, CANTEST_IOCTL_READ, &xfer, sizeof(xfer), 
			NULL, 0, NULL, NULL)) {
				wprintf(L"cbTestLoopbackRx: Failed in CANTEST_IOCTL_READ\r\n");
				goto cleanup;
		}
		else {
			wprintf(L"RX thread: success in read !\r\n");
			for (DWORD i = 0; i < p_test_arg->len; i++) {
				if (data[i] == p_test_arg->val + i) {
					wprintf(L"Byte 0x%02x at index %d (OK)\r\n", data[i], i);
				}
				else {
					wprintf(L"Byte 0x%02x at index %d (NOK !!)\r\n", data[i], i);
					goto cleanup;
				}
			}
		}
		Sleep(1000);
	}

	p_test_arg->success = TRUE;

cleanup :

	if (!p_test_arg->success)
		p_test_arg->aborted = TRUE;

	if (data)
		LocalFree(data);

	if (h_file != INVALID_HANDLE_VALUE)
		CloseHandle(h_file);

	wprintf(L"cbTestLoopbackRx: leaving loop\r\n");

	return 0;
}

//-----------------------------------------------------------------------------
//
// Function:  BOOL TestLoopback(DWORD duration, 
//								DWORD data_len, 
//								DWORD id, 
//								UINT8 val)
//
// This function runs the loopback test
// 
// Parameters:
//		duration    [in] Duration of the test
//		data_len	[in] Length of data in each message
//		id			[in] ID to use in each message
//		val			[in] Value of the data in each message
//
// Returns:
//		TRUE if success, FALSE if failure
//
//-----------------------------------------------------------------------------
BOOL TestLoopback(DWORD duration, DWORD data_len, DWORD id, UINT8 val)
{
	HANDLE h_file = INVALID_HANDLE_VALUE;
	BOOL b_ret = FALSE;
	TEST_LOOPBACK_ARG arg;
	HANDLE h_thread_rx = NULL;
	DWORD t_start;
	CANTEST_TRANSFER xfer;
	PUINT8 data = NULL;

	arg.duration = duration;
	arg.id = id;
	arg.val = val;
	arg.len = data_len;
	arg.aborted = FALSE;
	arg.success = FALSE;
	
	h_file = OpenCANTESTFile(TEST_DEVICE_PREFIX);

	if (h_file == INVALID_HANDLE_VALUE) {
		wprintf(L"TestLoopback: Failed in OpenCANTESTFile, check that driver is activated\r\n");
		goto cleanup;
	}

	wprintf(L"TestLoopback: enabling loopback mode\r\n");
	DeviceIoControl(h_file, CANTEST_IOCTL_ENABLE_LOOPBACK, NULL, 0, NULL, 0, NULL, NULL);

	h_thread_rx = CreateThread(NULL, 0, cbTestLoopbackRx, &arg, 0, NULL);

	if (h_thread_rx == NULL) {
		wprintf(L"Failed creating RX thread\r\n");
		goto cleanup;
	}

	data = (PUINT8)LocalAlloc(LPTR, data_len);
	for (DWORD i = 0; i < data_len; i++) {
		data[i] = val + (UINT8)i;
	}

	Sleep(500);
	wprintf(L"Waiting for RX thread\r\n");

	t_start = GetTickCount();
	
	while (GetTickCount() - t_start < duration) {
		xfer.Data = data;
		xfer.DataLen = data_len;
		xfer.ID = id;
		if (!DeviceIoControl(h_file, CANTEST_IOCTL_WRITE, &xfer, sizeof(xfer),
			NULL, 0, NULL, NULL)) {
				wprintf(L"Failed in CANTEST_IOCTL_WRITE\r\n");
				goto cleanup;
		}
		if (arg.aborted) {
			wprintf(L"*** Test aborted by RX thread, leaving\r\n");
			goto cleanup;
		}

		Sleep(1000);
	}

	if (WaitForSingleObject(h_thread_rx, duration + 5000) != WAIT_OBJECT_0)
	{
		wprintf(L"Timeout waiting for RX thread\r\n");
		TerminateThread(h_thread_rx, 0);
	}

	b_ret = TRUE;

cleanup :

	if (h_file != INVALID_HANDLE_VALUE) {			
		wprintf(L"TestLoopback: disabling loopback mode\r\n");
		DeviceIoControl(h_file, CANTEST_IOCTL_DISABLE_LOOPBACK, NULL, 0, NULL, 0, NULL, NULL);
		CloseHandle(h_file);
	}

	if (h_thread_rx != NULL)
		CloseHandle(h_thread_rx);

	if (data)
		LocalFree(data);

	wprintf(L"*****\r\n");
	wprintf(L"End of test, test %s\r\n", arg.success ? L"succeeded" : L"failed");
	wprintf(L"*****\r\n");

	return b_ret;
}

//-----------------------------------------------------------------------------
//
// Function:  HANDLE OpenCANTESTFile(TCHAR *dev_prefix)
//
// This function opens an handle on the CANTEST driver
// 
// Parameters:
//		dev_prefix [in] Prefix of the driver
//
// Returns:
//		A handle if success, INVALID_HANDLE_VALUE if failure
//
//-----------------------------------------------------------------------------
HANDLE OpenCANTESTFile(TCHAR *dev_prefix)
{
	HANDLE h_file;
	TCHAR file_name[MAX_PATH];

	wsprintf(file_name, L"%s%d:", dev_prefix, 1);

	h_file = CreateFile(file_name,
					   GENERIC_READ | GENERIC_WRITE,
					   0,
					   NULL,
					   CREATE_ALWAYS,                 // creation disposition
					   FILE_ATTRIBUTE_NORMAL,         // flags and attributes
					   NULL);

	if (h_file == INVALID_HANDLE_VALUE)
	{
		wprintf(L"Failed in CreateFile (filename %s)\r\n", file_name);
	}

	return h_file;
}
