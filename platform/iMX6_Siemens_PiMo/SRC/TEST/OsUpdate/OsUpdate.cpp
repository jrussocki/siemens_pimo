//-----------------------------------------------------------------------------
//
//  Copyright (C) 2014, Adeneo Embedded. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//-----------------------------------------------------------------------------
//
//  File: OsUpdate.cpp
//
//  This application read file from filesystem and write it "raw" at a given address
// in a specified block device.
//	
//
//-----------------------------------------------------------------------------

#include "stdafx.h"
#include <StoreMgr.H>

/*Copy buffer has fixed size : 4K*/
#define READ_WRITE_BUFF_SIZE 4096
#define DEFAULT_SECTOR_SIZE 512

typedef enum
{
	BLOCK_DEVICE_NONE,
	BLOCK_DEVICE_SD,
	BLOCK_DEVICE_MMC
} DEVICE;

BOOL GetDeviceNameFromProfile(WCHAR * ProfileName, WCHAR * DeviceName);

//-----------------------------------------------------------------------------
//
// Function: DeviceOpen
//
// Open the Windows Block device (DSK<x>) whose profile is "Device",
//	either SDMemoryCard or MMC.
//
// Parameters:
//      Device
//          [in] either "SD" or "MMC", physical device.
//
// Returns:
//      Returns a handle to the block device created if successful. Returns
//      INVALID_HANDLE_VALUE if not successful.
//
//-----------------------------------------------------------------------------

HANDLE DeviceOpen(DEVICE Device)
{
	HANDLE hDev = INVALID_HANDLE_VALUE;
	WCHAR DeviceName[DEVICENAMESIZE];
	BOOL DeviceExist = FALSE;

	switch(Device)
	{
		case BLOCK_DEVICE_SD:
			DeviceExist	= GetDeviceNameFromProfile(L"SDHCMemory", DeviceName);
			if(!DeviceExist)
				DeviceExist = GetDeviceNameFromProfile(L"SDMemory", DeviceName);
			break;
		case BLOCK_DEVICE_MMC:
			DeviceExist=GetDeviceNameFromProfile(L"MMC", DeviceName);
			break;
		default:
			DeviceExist=FALSE;
			break;
	}

	if(DeviceExist)
	{
		hDev = CreateFile(DeviceName,
			GENERIC_READ|GENERIC_WRITE,
			0,
			NULL,OPEN_EXISTING,
			0,
			NULL);
	}

	if(hDev == INVALID_HANDLE_VALUE)
	{
		_tprintf(L"ERROR : unable to open  device : %s. \r\n", DeviceName);
	}
	return hDev;
}

//-----------------------------------------------------------------------------
//
// Function: GetSectorSize
//
// Get sector size of block device.
//
// Parameters:
//      hDevice
//          HANDLE to block device.
//
// Returns:
//      Sector size in bytes.
//
//-----------------------------------------------------------------------------

DWORD GetSectorSize(HANDLE hDevice)
{
	DISK_INFO diskInfo;
	DWORD SectorSize;

	if(DeviceIoControl(hDevice,IOCTL_DISK_GETINFO,
						NULL, 0, 
						&diskInfo, sizeof(DISK_INFO), 
						NULL, FALSE)!=TRUE)
	{
		_tprintf(L"IOCTL_DISK_GETINFO error : %d\r\n", GetLastError());
		SectorSize = DEFAULT_SECTOR_SIZE;
	}
	else SectorSize = diskInfo.di_bytes_per_sect;
	return SectorSize;
}

//-----------------------------------------------------------------------------
//
// Function: BlockDeviceRead
//
// Read a given number of block device sectors in RAM.
//	Warning : no check is proceeded on the size of Output Buffer.
//
// Parameters:
//      hDevice
//          HANDLE to block device 
//		Sector Start
//			First Sector
//		pDataOutput
//			Output Buffer
//		Sector Count
//			Number of sector to read.
//
// Returns:
//      Returns a TRUE if success and FALSE otherwise.
//-----------------------------------------------------------------------------

BOOL BlockDeviceRead(HANDLE hDevice, DWORD SectorStart,
					 LPBYTE pDataOutput, DWORD SectorCount)
{
	SG_REQ lpInBuf;
	DWORD BytesRead;
	DWORD OutputDataSize;
	BOOL bRet;

	OutputDataSize = SectorCount*GetSectorSize(hDevice);
	bRet=FALSE;

	lpInBuf.sr_start = SectorStart; // physical sector to read 
	lpInBuf.sr_num_sec = SectorCount; // read 1 sector 
	lpInBuf.sr_num_sg = 1; 
	lpInBuf.sr_status = 0; //ERROR_SUCCESS; 
	lpInBuf.sr_callback = NULL;
	lpInBuf.sr_sglist[0].sb_buf = ((LPBYTE) MapPtrToProcess(pDataOutput, GetCurrentProcess())); 
	lpInBuf.sr_sglist[0].sb_len = OutputDataSize /** lpInBuf.sr_num_sec*/; 

	if(DeviceIoControl(hDevice, // Handle to the device 
						IOCTL_DISK_READ, // IOCTL for the operation 
						&lpInBuf, // LP to a buffer (input data) 
						sizeof(lpInBuf), // Size in Bytes of input data 
						pDataOutput, // LP to a buffer for output data 
						OutputDataSize, // Size in Bytes of output buffer 
						&BytesRead, // LP to variable (size of data in out buffer) 
						NULL)==TRUE)
	{
		if(BytesRead==OutputDataSize)
		{
			bRet=TRUE;
		}
	}
	else
	{
		_tprintf(L"Error : unable to read to sd raw%d\r\n", GetLastError());
	}
	return bRet;
}

//-----------------------------------------------------------------------------
//
// Function: BlockDeviceWrite
//
// Writes a given RAM data buffer to block device.
//	Warning : no check is proceeded on the size of Output Buffer.
//
// Parameters:
//      hDevice
//          HANDLE to block device 
//		Sector Start
//			First Sector
//		pDataOutput
//			Output Buffer
//		Sector Count
//			Number of sector to read.
//
// Returns:
//      Returns a TRUE if success and FALSE otherwise.
//-----------------------------------------------------------------------------

BOOL BlockDeviceWrite(HANDLE hDevice, DWORD SectorStart, LPBYTE pDataInput, DWORD SectorCount)
{
	SG_REQ lpInBuf;
	DWORD BytesWrite = 0;
	DWORD InputDataSize;
	BOOL bRet;

	InputDataSize = SectorCount*GetSectorSize(hDevice);
	bRet=FALSE;


	lpInBuf.sr_start = SectorStart; // physical sector to read 
	lpInBuf.sr_num_sec = SectorCount; // read 1 sector 
	lpInBuf.sr_num_sg = 1; 
	lpInBuf.sr_status = 0; //ERROR_SUCCESS; 
	lpInBuf.sr_callback = NULL;
	lpInBuf.sr_sglist[0].sb_buf = ((LPBYTE) MapPtrToProcess(pDataInput, GetCurrentProcess())); 
	lpInBuf.sr_sglist[0].sb_len = InputDataSize /** lpInBuf.sr_num_sec*/; 

	if(DeviceIoControl(hDevice, // Handle to the device 
						IOCTL_DISK_WRITE, // IOCTL for the operation 
						&lpInBuf, // LP to a buffer (input data) 
						sizeof(lpInBuf), // Size in Bytes of input data 
						NULL, // LP to a buffer for output data 
						0, //  Size in Bytes of output buffer 
						&BytesWrite, //LP to variable (size of data in out buffer) 
						NULL)==TRUE)
	{
		if(BytesWrite==InputDataSize)
		{
			bRet=TRUE;
		}
	}
	else
	{
		_tprintf(L"Error : unable to write to sd raw%d\r\n", GetLastError());
	}
	return bRet;
}

//-----------------------------------------------------------------------------
//
// Function: GetDeviceNameFromProfile
//
// Gets the device name (DSK1, DSK2...) of the block device whose profile is given.
//
// Parameters:
//      ProfileName [in]
//          Profile name : either "SDMemory", "SDHCMemory" or "MMC" 
//		DeviceName [out]
//			Name string of profile (for instance : "DSK8")
//
// Returns:
//      Returns a TRUE if success and FALSE otherwise.
//-----------------------------------------------------------------------------

BOOL GetDeviceNameFromProfile(WCHAR * ProfileName, WCHAR * DeviceName)
{
	STOREINFO si = {0};
	HANDLE hFind = INVALID_HANDLE_VALUE;
	si.cbSize = sizeof(STOREINFO);
	BOOL bRet = FALSE;
	BOOL equal;

	if(ProfileName==NULL || DeviceName==NULL) goto end;
	
	hFind = FindFirstStore(&si);
	if( hFind != INVALID_HANDLE_VALUE)
    {
        do
        {
			if((equal=wcsncmp(si.sdi.szProfile, ProfileName, PROFILENAMESIZE))==0)
			{
				bRet = TRUE;
				wcscpy(DeviceName, si.szDeviceName);
				break;
			}
        }
        while(FindNextStore(hFind, &si));
        FindClose(hFind);
	}
	else
	{
		_tprintf(L"Unable to open store manager\r\n");
	}
end:
	if(!bRet) _tprintf(L"No store found\r\n");
	return bRet;
}

//-----------------------------------------------------------------------------
//
// Function: WriteFileToBlockDevice
//
// Write the whole content file at FilePath
//
// Parameters:
//      Device [in]
//          either SD or MMC.
//		FilePath [in]
//			Full path of file (\"Mounted Volume"\NK.nb0 for instance)
//		DeviceRawAddress
//			Raw address in block device where data will be written sequentially
//
// Returns:
//      Returns a TRUE if success and FALSE otherwise.
//-----------------------------------------------------------------------------

BOOL WriteFileToBlockDevice(DEVICE Device, WCHAR * FilePath, DWORD DeviceRawAddress)
{
	HANDLE hInputFile, hDev;
	DWORD FileSize;
	DWORD SectorSize;
	DWORD CurrentSector;
	DWORD SectorsPerIteration;
	BYTE ReadWriteBuffer[READ_WRITE_BUFF_SIZE];
	DWORD SectorStart, SectorCount;
	DWORD BytesRead;
	BOOL bRet;

	bRet=FALSE;
	hDev=INVALID_HANDLE_VALUE;

	/*Open input file*/
	hInputFile=CreateFile(FilePath,
		GENERIC_READ,0,
		NULL,OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,NULL);

	if(hInputFile==INVALID_HANDLE_VALUE)
	{
		_tprintf(L"Unable to find input file\r\n");
		goto end;
	}

	/*Open SD block device descriptor*/
	if((hDev=DeviceOpen(Device))==INVALID_HANDLE_VALUE)
	{
		_tprintf(L"Unable to find output device file\r\n");
		goto end;
	}

	FileSize			= GetFileSize(hInputFile,NULL);
	SectorSize			= GetSectorSize(hDev);
	SectorStart			= DeviceRawAddress/SectorSize;
	SectorCount			= ((FileSize-1)/SectorSize)+1;
	SectorsPerIteration = READ_WRITE_BUFF_SIZE/SectorSize;

	for(CurrentSector=SectorStart;
		CurrentSector<SectorStart+SectorCount;
		CurrentSector+=SectorsPerIteration)
	{
		if(ReadFile(hInputFile, ReadWriteBuffer,READ_WRITE_BUFF_SIZE, &BytesRead, NULL))
		{
			if((BytesRead==READ_WRITE_BUFF_SIZE)
				&& BlockDeviceWrite(hDev, CurrentSector, ReadWriteBuffer, SectorsPerIteration))
			{
				bRet=TRUE;
			}
			else
			{
				_tprintf(L"Write to Raw SD error %d\r\n", GetLastError());
				bRet=FALSE;
				goto end;
			}
		}
		else
		{
			_tprintf(L"ReadFile Error %d\r\n", GetLastError());
			bRet=FALSE;
			goto end;
		}	
	}

end:
	if(hInputFile!=INVALID_HANDLE_VALUE) CloseHandle(hInputFile);
	if(hDev!=INVALID_HANDLE_VALUE) CloseHandle(hDev);
	return bRet;
}

//-----------------------------------------------------------------------------
//
// Function: DisplayBlockDeviceRawData
//
// Display the first bytes at a given address in a given device.
//
// Parameters:
//      Device [in]
//          either SD or MMC.
//		DeviceRawAddress
//			Raw address in block device from which data will be read
//		Length
//			How many bytes will be displayed.
//
// Returns:
//      no return value.
//-----------------------------------------------------------------------------

void DisplayBlockDeviceRawData(DEVICE Device, DWORD DeviceRawAddress, DWORD Length)
{
	HANDLE hDev;
	DWORD SectorSize;
	LPBYTE pBuffer;
	DWORD SectorStart;
	DWORD SectorCount;
	DWORD i;

	_tprintf(L"Dumping memory range %08x - %08x\r\n", DeviceRawAddress, 
		DeviceRawAddress+Length-1);
	hDev=DeviceOpen(Device);
	pBuffer=(BYTE*)malloc(Length);
	memset(pBuffer, 0xFF, Length);

	if(hDev==INVALID_HANDLE_VALUE)
	{
		_tprintf(L"Unable to find output device file\r\n");
		goto end;
	}

	SectorSize=GetSectorSize(hDev);
	SectorStart=DeviceRawAddress/SectorSize;

	SectorCount=((Length-1)/SectorSize)+1;
	if(BlockDeviceRead(hDev, SectorStart, pBuffer, SectorCount)==TRUE)
	{
		for(i=0;i<Length;i++)
			_tprintf(L"%02x ", pBuffer[i]);
	}
	else
	{
		_tprintf(L"Unable to get raw data from block device");
	}
end:
	CloseHandle(hDev);
}

//-----------------------------------------------------------------------------
//
// Function: _tmain
//
// Main function. Parse command line and calls the matching function.
//
// Usage : %s <file> <SD|MMC> <offset (hex)>\r\n
//
// Returns:
//      no return value.
//-----------------------------------------------------------------------------

int _tmain(int argc, TCHAR *argv[], TCHAR *envp[])
{
	DWORD Offset;
	wchar_t * pEnd;
	DEVICE Device;
	BOOL bRet=-1;

	if(argc==4)
	{
		Offset=wcstoul(argv[3], &pEnd, 16);
		if(wcscmp(argv[2], L"SD")==0)
		{
			Device = BLOCK_DEVICE_SD;
		}
		else if(wcscmp(argv[2], L"MMC")==0)
		{
			Device = BLOCK_DEVICE_MMC;
		}
		else
		{
			Device = BLOCK_DEVICE_NONE;
		}

		_tprintf(L"Writing file %s to offset 0x%08x\r\n", argv[1], Offset);

		if(WriteFileToBlockDevice(Device, argv[1], Offset))
		{
			_tprintf(L"File copy success!\r\n");
			DisplayBlockDeviceRawData(Device, Offset, 512);
			bRet=0;
		}
		else
		{
			_tprintf(L"File copy failure\r\n");
		}
	}
	else
	{
		_tprintf(L"Usage : %s <file> <SD|MMC> <offset (hex)>\r\n", argv[0]);
	}
    return bRet;
}
