//-----------------------------------------------------------------------------
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//-----------------------------------------------------------------------------
//
//  Copyright (C) 2007-2010, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//-----------------------------------------------------------------------------
//
//  File: ioctl.c
//
//  This file implements the OEM's IO Control (IOCTL) functions and declares
//  global variables used by the IOCTL component.
//
//-----------------------------------------------------------------------------
#include <bsp.h>
#include <usbkitl.h>
#include <common_pm.h>
#include <args.h>
#include <bsp_version.h>
#include <platform_ioctl.h>

//-----------------------------------------------------------------------------
// External Functions
extern DWORD    OALCPUEnterSuspend();

//-----------------------------------------------------------------------------
// External Variables
extern UINT32 g_SREV;
extern DWORD g_dwBoardID;
extern POAL_PLAT_INFO  g_pPlatInfo;
extern BOOL g_bPowerOffSystem;

//-----------------------------------------------------------------------------
// Global Variables
BOOL g_bOalPostInit = FALSE;

//------------------------------------------------------------------------------
//
//  Global: g_oalIoctlPlatformType/OEM
//
//  Platform Type/OEM
//
LPCWSTR g_oalIoCtlPlatformType = IOCTL_PLATFORM_TYPE;
LPCWSTR g_oalIoCtlPlatformOEM  = IOCTL_PLATFORM_OEM;

//------------------------------------------------------------------------------
//
//  Global: g_oalIoctlProcessorVendor/Name/Core
//
//  Processor information
//
LPCWSTR g_oalIoCtlProcessorVendor = IOCTL_PROCESSOR_VENDOR;
LPCWSTR g_oalIoCtlProcessorName   = IOCTL_PROCESSOR_NAME;
LPCWSTR g_oalIoCtlProcessorCore   = IOCTL_PROCESSOR_CORE;

//------------------------------------------------------------------------------
//
//  Global: g_oalIoctlInstructionSet
//
//  Processor instruction set identifier
//
UINT32 g_oalIoCtlInstructionSet = IOCTL_PROCESSOR_INSTRUCTION_SET;
UINT32 g_oalIoCtlClockSpeed = IOCTL_PROCESSOR_CLOCK_SPEED;


//------------------------------------------------------------------------------
//
//  Function:  OALIoCtlTestPm
//
//  This function is used for calling OEMPowerOff() called from apps.
//
//------------------------------------------------------------------------------
BOOL OALIoCtlTestPm(
    UINT32 code, VOID *pInpBuffer, UINT32 inpSize, VOID *pOutBuffer,
    UINT32 outSize, UINT32 *pOutSize
    )
{
    
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(code);
    UNREFERENCED_PARAMETER(pInpBuffer);
    UNREFERENCED_PARAMETER(inpSize);
    UNREFERENCED_PARAMETER(pOutBuffer);
    UNREFERENCED_PARAMETER(outSize);
    UNREFERENCED_PARAMETER(pOutSize);
    
    OALCPUEnterSuspend();

    return TRUE;
}
    

//------------------------------------------------------------------------------
//
//  Function:  OALIoCtlHalSetPowerOff
//
//  This function is used to inform OEMPowerOff that we are not suspending,
//  but in fact fully powering down.
//
//------------------------------------------------------------------------------
BOOL OALIoCtlHalSetPowerOff(
    UINT32 code, VOID *pInpBuffer, UINT32 inpSize, VOID *pOutBuffer,
    UINT32 outSize, UINT32 *pOutSize
    )
{
    DWORD dwErr = 0;

    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(code);
    UNREFERENCED_PARAMETER(pOutBuffer);
    UNREFERENCED_PARAMETER(outSize);
    UNREFERENCED_PARAMETER(pOutSize);

    //
    // input buffer should be 
    //
    PREFAST_SUPPRESS(6320, "Generic exception handler");

    //
    // any positive we'll set TRUE, FALSE otherwise.
    //
    __try {
        g_bPowerOffSystem = ((*((PDWORD)pInpBuffer)) ? TRUE : FALSE);

    } __except (EXCEPTION_EXECUTE_HANDLER) {

        dwErr = ERROR_INVALID_PARAMETER;
    }

    if (dwErr) {
        NKSetLastError (dwErr);
    }

    return !dwErr;
}

//------------------------------------------------------------------------------
//
//  Function:  OALIoCtlHalPostInit
//
//  This function is the next OAL routine called by the kernel after OEMInit and
//  provides a context for initializing other aspects of the device prior to
//  general boot.
//
//------------------------------------------------------------------------------
BOOL OALIoCtlHalPostInit(
    UINT32 code, VOID *pInpBuffer, UINT32 inpSize, VOID *pOutBuffer,
    UINT32 outSize, UINT32 *pOutSize)
{    
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(code);
    UNREFERENCED_PARAMETER(pInpBuffer);
    UNREFERENCED_PARAMETER(inpSize);
    UNREFERENCED_PARAMETER(pOutBuffer);
    UNREFERENCED_PARAMETER(outSize);
    UNREFERENCED_PARAMETER(pOutSize);

    g_bOalPostInit = TRUE;

    return(TRUE);
}


BOOL OALIoCtlChipType(UINT32 code, VOID *lpInBuf, 
					  UINT32 nInBufSize, VOID *lpOutBuf, UINT32 nOutBufSize, UINT32 *lpBytesReturned)
{
	if(lpOutBuf == NULL)
	{
		NKSetLastError (ERROR_INVALID_PARAMETER);
		return FALSE;
	}

	if(nOutBufSize != sizeof(OAL_PLAT_INFO))
	{
		NKSetLastError (ERROR_INSUFFICIENT_BUFFER);
		return FALSE;

	}

	if(g_pPlatInfo == NULL)
	{
		return FALSE;
	} 
	
	memcpy(lpOutBuf,g_pPlatInfo,nOutBufSize);

	return TRUE;
}

//------------------------------------------------------------------------------
//
//  Function:  OALIoCtlEbootVersion
//
//  This function retrieves the value of the bootloader version by reading
//  the boot arguments from the BSP_ARGS structure.
//
//------------------------------------------------------------------------------

BOOL OALIoCtlEbootVersion (
    UINT32 code, VOID *pInpBuffer, UINT32 inpSize, VOID *pOutBuffer,
    UINT32 outSize, UINT32 *pOutSize)
{
    BOOL rc = FALSE;

    OALMSG(OAL_IOCTL&&OAL_FUNC, (L"+OALIoCtlEbootVersion\r\n"));

    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(code);
    UNREFERENCED_PARAMETER(pInpBuffer);
    UNREFERENCED_PARAMETER(inpSize);

    // Check buffer size
    if (pOutBuffer == NULL || outSize != sizeof(EBOOT_VERSION) )
    {
        NKSetLastError(ERROR_INSUFFICIENT_BUFFER);
        OALMSG(OAL_WARN, (L"WARN: OALIoCtlEbootVersion: Buffer incorrect size\r\n"));
    }
    else
    {
        // query args
        UINT8 *pArgs = OALArgsQuery(OAL_ARGS_QUERY_EBOOT_VERSION);
        if(pArgs == NULL) 
        {
            OALMSG(1, (L"OALIoCtlEbootVersion: Failed to get Args.\r\n"));
        }
        else
        {       
            // Copy the bootloader version from ARGS to caller buffer
            memcpy(pOutBuffer, pArgs, sizeof(EBOOT_VERSION));

            if (pOutSize != NULL)
            {
                *pOutSize = sizeof(EBOOT_VERSION);
            }

            rc = TRUE;
        }
    }

    // Indicate status
    OALMSG(OAL_IOCTL&&OAL_FUNC, (L"-OALIoCtlEbootVersion(rc = %d)\r\n", rc));
    return rc;
}

BOOL OALIoCtlBootLine (
    UINT32 code, VOID *pInpBuffer, UINT32 inpSize, VOID *pOutBuffer,
    UINT32 outSize, UINT32 *pOutSize)
{
	BOOL rc;
	BSP_ARGS	* pArgs;
	DWORD * pBootLine;

	rc		=	FALSE;
	pArgs	=	IMAGE_SHARE_ARGS_UA_START;
	OALMSG(1, (L"+OALIoCtlBootLine\r\n"));

	UNREFERENCED_PARAMETER(code);
	UNREFERENCED_PARAMETER(pInpBuffer);
	UNREFERENCED_PARAMETER(inpSize);

	if (pOutBuffer == NULL || outSize != sizeof(DWORD))
    {
        NKSetLastError(ERROR_INSUFFICIENT_BUFFER);
        OALMSG(1, (L"ERROR: OALIoCtlBootLine: Buffer incorrect size\r\n"));
    }
	else
	{
		pBootLine  = (DWORD*)pOutBuffer;
		*pBootLine = pArgs->recoveryMode;
		rc=TRUE;
		OALMSG(1, (L"--OALIoCtlBootLine. rc = %d. Bootline : %d\r\n", rc, *pBootLine));
	}

	//OALMSG(1, (L"--OALIoCtlBootLine. rc = %d. Bootline : %d\r\n", rc, *pBootLine));
	return rc;
}
//------------------------------------------------------------------------------
//
//  Function:  OALIoCtlKernelVersion
//
//  This function retrieves the value of the bootloader version by reading
//  the boot arguments from the BSP_ARGS structure.
//
//------------------------------------------------------------------------------

BOOL OALIoCtlKernelVersion (
    UINT32 code, VOID *pInpBuffer, UINT32 inpSize, VOID *pOutBuffer,
    UINT32 outSize, UINT32 *pOutSize)
{
    BOOL rc = FALSE;
	BSP_VERSION bspVersion = {0};

    OALMSG(OAL_IOCTL&&OAL_FUNC, (L"+OALIoCtlKernelVersion\r\n"));

    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(code);
    UNREFERENCED_PARAMETER(pInpBuffer);
    UNREFERENCED_PARAMETER(inpSize);

	bspVersion.major = BSP_VERSION_MAJOR;
	bspVersion.minor = BSP_VERSION_MINOR;
	bspVersion.incremental = BSP_VERSION_INCREMENTAL;

    // Check buffer size
    if (pOutBuffer == NULL || outSize != sizeof(BSP_VERSION) )
    {
        NKSetLastError(ERROR_INSUFFICIENT_BUFFER);
        OALMSG(OAL_WARN, (L"WARN: OALIoCtlKernelVersion: Buffer incorrect size\r\n"));
    }
    else
    {  
        // Copy the bsp version from bsp_version.h to caller buffer
        memcpy(pOutBuffer, &bspVersion, sizeof(BSP_VERSION));

        if (pOutSize != NULL)
        {
            *pOutSize = sizeof(BSP_VERSION);
        }

        rc = TRUE;
    }

    // Indicate status
    OALMSG(OAL_IOCTL&&OAL_FUNC, (L"-OALIoCtlKernelVersion(rc = %d)\r\n", rc));
    return rc;
}


//------------------------------------------------------------------------------
//
//  Function:  OALIoCtlHalPresuspend
//
//  This function implements IOCTL_HAL_PRESUSPEND which provides the OAL 
//  the time needed to prepare for a suspend operation. Any preparation is 
//  completed while the system is still in threaded mode.
//
//------------------------------------------------------------------------------
BOOL OALIoCtlHalPresuspend(
    UINT32 code, __inout_bcount_opt(inSize) VOID *pInBuffer, 
    UINT32 inSize, __inout_bcount_opt(outSize) VOID *pOutBuffer, 
    UINT32 outSize, __out_opt UINT32 *pOutSize) 
{
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(code);
    UNREFERENCED_PARAMETER(pInBuffer);
    UNREFERENCED_PARAMETER(inSize);
    UNREFERENCED_PARAMETER(pOutBuffer);
    UNREFERENCED_PARAMETER(outSize);
    UNREFERENCED_PARAMETER(pOutSize);

    // Do nothing for now.
    //
    return(TRUE);
}


//------------------------------------------------------------------------------
//
//  Function:  OALIoCtlQueryDispSettings
//
//  This function implements IOCTL_HAL_QUERY_DISPLAYSETTINGS and is used by 
//  graphics device interface (GDI) to query the kernel for information about 
//  a preferred resolution for the system to use.
//
//------------------------------------------------------------------------------
BOOL OALIoCtlQueryDispSettings (
    UINT32 code, VOID *lpInBuf, UINT32 nInBufSize, VOID *lpOutBuf, 
    UINT32 nOutBufSize, UINT32 *lpBytesReturned) 
{
    DWORD dwErr = 0;

    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(code);
    UNREFERENCED_PARAMETER(lpInBuf);
    UNREFERENCED_PARAMETER(nInBufSize);

    if (lpBytesReturned) {
        *lpBytesReturned = 0;
    }

    if (!lpOutBuf) {
        dwErr = ERROR_INVALID_PARAMETER;
    } 
    else if (sizeof(DWORD)*3 > nOutBufSize) {
        dwErr = ERROR_INSUFFICIENT_BUFFER;
    } else {
        PREFAST_SUPPRESS(6320, "Generic exception handler");
        __try {

            ((PDWORD)lpOutBuf)[0] = (DWORD) BSP_PREF_DISPLAY_WIDTH;
            ((PDWORD)lpOutBuf)[1] = (DWORD) BSP_PREF_DISPLAY_HEIGHT;
            ((PDWORD)lpOutBuf)[2] = (DWORD) BSP_PREF_DISPLAY_BPP;

            if (lpBytesReturned) {
                *lpBytesReturned = sizeof (DWORD) * 3;
            }

        } __except (EXCEPTION_EXECUTE_HANDLER) {
            dwErr = ERROR_INVALID_PARAMETER;
        }
    }

    if (dwErr) {
        NKSetLastError (dwErr);
    }

    return !dwErr;
}


//------------------------------------------------------------------------------
//
//  Function:  OALIoCtlQuerySiVersion
//
//  This function implements IOCTL_HAL_QUERY_SI_VERSION and is used to query the
//  silicon version.  The result returned by this IOCTL may be used to 
//  conditionally execute code for a specific silicon version of the SoC.
//
//------------------------------------------------------------------------------
BOOL OALIoCtlQuerySiVersion (
    UINT32 code, VOID *lpInBuf, UINT32 nInBufSize, VOID *lpOutBuf, 
    UINT32 nOutBufSize, UINT32 *lpBytesReturned) 
{
    DWORD dwErr = 0;

    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(code);
    UNREFERENCED_PARAMETER(lpInBuf);
    UNREFERENCED_PARAMETER(nInBufSize);

    if (lpBytesReturned) {
        *lpBytesReturned = 0;
    }

    if (!lpOutBuf) {
        dwErr = ERROR_INVALID_PARAMETER;
    } 
    else if (sizeof(DWORD) > nOutBufSize) {
        dwErr = ERROR_INSUFFICIENT_BUFFER;
    } else {
        PREFAST_SUPPRESS(6320, "Generic exception handler");
        __try {

            // Return silicon rev that was was read during OEMInit
            *((PDWORD) lpOutBuf) = (DWORD) g_SREV;

            if (lpBytesReturned) {
                *lpBytesReturned = sizeof (DWORD);
            }

        } __except (EXCEPTION_EXECUTE_HANDLER) {
            dwErr = ERROR_INVALID_PARAMETER;
        }
    }

    if (dwErr) {
        NKSetLastError (dwErr);
    }

    return !dwErr;
}

//------------------------------------------------------------------------------
// Function: OALIoCtlHalGetPowerDisposition
//
// IOCTL_HAL_GET_POWER_DISPOSITION IOCTL Handler
//

BOOL OALIoCtlHalGetPowerDisposition(UINT32 code, VOID *pInpBuffer, UINT32 inpSize, VOID *pOutBuffer, UINT32 outSize, UINT32 *pOutSize)
{
    if ((!pOutBuffer) || (outSize<sizeof(DWORD)))
    {
        OALMSG(TRUE, (L"***Error: Incorrect use of IOCTL_HAL_GET_POWER_DISPOSITION.\r\n"));
        return FALSE;
    }

    /* see pkfuncs.h for meaning of this value */
    
    *((DWORD *)pOutBuffer) = (DWORD)POWER_DISPOSITION_SUSPENDRESUME_MANUAL;
    if (pOutSize)
        *pOutSize = sizeof(DWORD);
       
    return(TRUE);
}

//------------------------------------------------------------------------------
//
//  Function:  OALIoCtlQueryBoardId
//
//  This function implements IOCTL_HAL_QUERY_BOARD_ID and is used to query the
//  board id of the EVK.  The result returned by this IOCTL may be used to 
//  conditionally execute code for a specific board revision of the EVK.
//
//------------------------------------------------------------------------------
BOOL OALIoCtlQueryBoardId (
    UINT32 code, VOID *lpInBuf, UINT32 nInBufSize, VOID *lpOutBuf, 
    UINT32 nOutBufSize, UINT32 *lpBytesReturned) 
{
    DWORD dwErr = 0;

    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(code);
    UNREFERENCED_PARAMETER(lpInBuf);
    UNREFERENCED_PARAMETER(nInBufSize);

    if (lpBytesReturned) {
        *lpBytesReturned = 0;
    }

    if (!lpOutBuf) {
        dwErr = ERROR_INVALID_PARAMETER;
    } 
    else if (sizeof(DWORD) > nOutBufSize) {
        dwErr = ERROR_INSUFFICIENT_BUFFER;
    } else {
        PREFAST_SUPPRESS(6320, "Generic exception handler");
        __try {

            // Return board ID that was was read during OEMInit
            *((PDWORD) lpOutBuf) = (DWORD) g_dwBoardID;

            if (lpBytesReturned) {
                *lpBytesReturned = sizeof (DWORD);
            }

        } __except (EXCEPTION_EXECUTE_HANDLER) {
            dwErr = ERROR_INVALID_PARAMETER;
        }
    }

    if (dwErr) {
        NKSetLastError (dwErr);
    }

    return !dwErr;
}

BOOL OALIoCtlGetMacAddress(
    UINT32 dwIoControlCode, VOID *lpInBuf, UINT32 nInBufSize, VOID *lpOutBuf,
    UINT32 nOutBufSize, UINT32* lpBytesReturned)
{
    BOOL rc = FALSE;

    OALMSG(OAL_IOCTL&&OAL_FUNC, (L"+OALIoCtlGetMacAddress\r\n"));

    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(dwIoControlCode);
    UNREFERENCED_PARAMETER(lpInBuf);
    UNREFERENCED_PARAMETER(nInBufSize);

    // Check buffer size
    if (lpOutBuf == NULL || nOutBufSize < 6 )
    {
        NKSetLastError(ERROR_INSUFFICIENT_BUFFER);
        OALMSG(OAL_WARN, (L"WARN: OALIoCtlGetMacAddress: Buffer too small\r\n"));
    }
    else
    {
        // query args
        UINT8 *pArgs = OALArgsQuery(OAL_ARGS_QUERY_MACADDR);
        if(pArgs == NULL) 
        {
            OALMSG(1, (L"OALIoCtlGetMacAddress: Failed to get Args.\r\n"));
        }
        else
        {       
            // Copy the MAC address from ARGS to caller buffer
            memcpy(lpOutBuf, pArgs, 6);

            if (lpBytesReturned != NULL)
            {
                *lpBytesReturned = 6;
            }

            rc = TRUE;
        }
    }

    // Indicate status
    OALMSG(OAL_IOCTL&&OAL_FUNC, (L"-OALIoCtlGetMacAddress(rc = %d)\r\n", rc));
    return rc;
}

//------------------------------------------------------------------------------
//
//  Global: g_oalIoCtlTable[]
//
//  IOCTL handler table. This table includes the IOCTL code/handler pairs
//  defined in the IOCTL configuration file. This global array is exported
//  via oal_ioctl.h and is used by the OAL IOCTL component.
//
#pragma warning(disable: 4028)
const OAL_IOCTL_HANDLER g_oalIoCtlTable[] = {
#include "ioctl_tab.h"
};

//------------------------------------------------------------------------------