//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//-----------------------------------------------------------------------------
//
//  Copyright (C) 2007-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//-----------------------------------------------------------------------------
//
//  File:  power.c
//
//  Power BSP callback functions implementation. This function are called as
//  last function before OALCPUPowerOff. The KITL was already disabled by
//  OALKitlPowerOff.
//
//-----------------------------------------------------------------------------

#include <bsp.h>
#include <Common_pm.h>

// Need to disable function pointer cast warnings.  We use memcpy to move 
// the bus scaling calibration routine into IRAM and then cast this address 
// as a function pointer.
#pragma warning(disable: 4054 4055)

//-----------------------------------------------------------------------------
// Types
typedef VOID (*FUNC_ENTER_WFI) (VOID);

//-----------------------------------------------------------------------------
// forward references
VOID OALCPUPowerOffSave();
VOID OALCPUPowerOffRestore();

//-----------------------------------------------------------------------------
// External Functions
extern void OALCPUEnterSuspend(void);
extern void OALCPUEnterSuspendEnd(void);
extern void OALCPUEnterWFI(void);
extern PUINT32 OALGetPageTableEntryPtr(UINT32 va);
extern BOOL OEMMpCpuPowerFunc(DWORD dwProcessor, BOOL fOnOff, DWORD dwHint);

extern void PMOALStall(int milliseconds);

//-----------------------------------------------------------------------------
// External Variables
extern POAL_PLAT_INFO  g_pPlatInfo;
extern PCSP_CCM_REGS g_pCCM;
extern PDDK_CLK_CONFIG g_pDdkClkConfig;
extern UINT32 g_SREV;
extern PCSP_IOMUX_REGS g_pIOMUX;
//extern DWORD g_dwBoardID;
extern IMX6_SUSPEND_MODE    g_SuspendMode;
extern UINT32               g_CpuType;

//-----------------------------------------------------------------------------
// Global Variables
PCSP_GPC_REGS g_pGPC;
PCSP_ANAMISC_REGS   g_pANAMISC;
FUNC_ENTER_WFI      g_pFuncIramEnterSuspend;

// #define OAL_JTAG_DEBUG
#define L1_PT_SECTION_XN    (1 << 4)

//-----------------------------------------------------------------------------
//
//  Function:  OALPowerInit
//
//  This function initializes the power management hardware on the CPU.
//
VOID OALPowerInit()
{
    PUINT32 pUSB_PHY_CTRL0 = OALPAtoUA(CSP_BASE_REG_PA_USB + 0x808);
    PCSP_REGS_MODULE pSATA = (PCSP_REGS_MODULE) OALPAtoUA(CSP_BASE_REG_PA_SATA);
    PUINT pPTE;

    // TODO:  Port
    
#if 0
    PCSP_GPIO_REGS pGPIO1 = (PCSP_GPIO_REGS) OALPAtoUA(CSP_BASE_REG_PA_GPIO1);
    PCSP_GPIO_REGS pGPIO2 = (PCSP_GPIO_REGS) OALPAtoUA(CSP_BASE_REG_PA_GPIO2);
    PCSP_GPIO_REGS pGPIO3 = (PCSP_GPIO_REGS) OALPAtoUA(CSP_BASE_REG_PA_GPIO3);
    PCSP_GPIO_REGS pGPIO4 = (PCSP_GPIO_REGS) OALPAtoUA(CSP_BASE_REG_PA_GPIO4);
#endif

	//
	// initialize some general PM constructs in Pm.c
	//
	PmPowerOffInit(IMX6_BOARD_SABRE_SDB);  // initialize general PM stuff
	PmPowerEnableWaitMode(TRUE);            // use simple WFI for wait (i.e., clocks on, DDR enabled)

	// Disable USB PHY
	// PHY_CTRL0[10] = UTMI_ON_CLK = 0 => Clocks remain off when PHY suspended
	CLRREG32(pUSB_PHY_CTRL0, (1 << 10));

	// Power off SATA PHY needed only for imx6QD
	if(g_pPlatInfo->ChipType == CHIP_MX6DQ)
	{
		INSREG32BF(&pSATA->PR[0].SCTL, SATA_PR_SCTL_DET, 4);
		SETREG32(&pSATA->PR[0].PHYCR, (1 << 20));
	}
	// Initialize IRAM for low-power code
	g_pFuncIramEnterSuspend = (FUNC_ENTER_WFI) OALPAtoUA((IMAGE_WINCE_OAL_IRAM_PA_START));

    if (g_pFuncIramEnterSuspend == NULL)
    {
        // Error message is all we can do since OEMInit has no return
        OALMSG(OAL_ERROR, (L"ERROR: OALPAtoUA failed for IMAGE_WINCE_OAL_IRAM_PA_START\r\n"));
    }

    // Kernel marks all non-cached regions as execute never (XN = 1).  Clear XN bit in
    // the page table entry for uncached IRAM so we can execute low-power code
    pPTE = OALGetPageTableEntryPtr((UINT32) g_pFuncIramEnterSuspend);
    *pPTE &= (~(L1_PT_SECTION_XN));

    // Copy the calibration code to IRAM
    memcpy((void *) g_pFuncIramEnterSuspend, (void *)OALCPUEnterSuspend, IMAGE_WINCE_OAL_IRAM_SIZE);

    OALMSG(OAL_POWER&&OAL_VERBOSE,(TEXT("OALPowerInit: g_pFuncIramEnterSuspend=0x%8X \r\n"),g_pFuncIramEnterSuspend));

    //
    // disble all unused clocks...
    //
/*
    CLRREG32(&g_pCCM->CCGR[0],
             CCM_CGR0_CHEETAH_DBG_MASK |
             CCM_CGR0_CAAM_SECURE_MEM_MASK |
             CCM_CGR0_CAAM_WRAPPER_ACLK_MASK |
             CCM_CGR0_CAAM_WRAPPER_IPG_MASK |
             CCM_CGR0_CAN1_MASK  |
             CCM_CGR0_CAN1_SERIAL_MASK |
             CCM_CGR0_CAN2_MASK |
             CCM_CGR0_CAN2_SERIAL_MASK |
             CCM_CGR0_DCIC1_MASK |
             CCM_CGR0_DCIC2_MASK |
             CCM_CGR0_DTCP_DTCP_MASK
            );

    CLRREG32(&g_pCCM->CCGR[1],
             CCM_CGR1_ECSPI1_MASK |
             CCM_CGR1_ECSPI2_MASK |
             CCM_CGR1_ECSPI3_MASK |
             CCM_CGR1_ECSPI4_MASK |
             CCM_CGR1_ECSPI5_MASK
            );

    CLRREG32(&g_pCCM->CCGR[2],
             CCM_CGR2_HDMI_TX_IAHBCLK_MASK |
             CCM_CGR2_HDMI_TX_ISFR_MASK |
             CCM_CGR2_I2C1_SERIAL_MASK |
             CCM_CGR2_I2C2_SERIAL_MASK |
             CCM_CGR2_I2C3_SERIAL_MASK
            );

    CLRREG32(&g_pCCM->CCGR[4],
             CCM_CGR4_PERFMON1_APB_MASK |
             CCM_CGR4_PERFMON2_APB_MASK |
             CCM_CGR4_PERFMON3_APB_MASK
            );

    CLRREG32(&g_pCCM->CCGR[5],
             CCM_CGR5_SPDIF_MASK |
             CCM_CGR5_SSI1_MASK |
             CCM_CGR5_SSI2_MASK |
             CCM_CGR5_SSI3_MASK
            );

    CLRREG32(&g_pCCM->CCGR[6],
             CCM_CGR6_USDHC1_MASK |
             CCM_CGR6_USDHC2_MASK |
             CCM_CGR6_USDHC3_MASK |
             CCM_CGR6_USDHC4_MASK
            );
*/
}

//-----------------------------------------------------------------------------
//
//  Function:  OALCPUPowerOff
//
//  This function powers off CPU.
//
VOID OALCPUPowerOff()
{
    UINT32      PendingIRQ[4];
    
    //
    // map some regs we need for suspend/resume here
    //
    g_pGPC = (PCSP_GPC_REGS) OALPAtoUA(CSP_BASE_REG_PA_GPC);
    g_pANAMISC = (PCSP_ANAMISC_REGS) OALPAtoUA(CSP_BASE_REG_PA_ANAMISC);

    //
    // check for pending IRQ, if so, we're done before we begin...
    //
    PendingIRQ[0] = (INREG32(&g_pGPC->GPC_ISR1) & g_PMWakeSources[0]);
    PendingIRQ[1] = (INREG32(&g_pGPC->GPC_ISR2) & g_PMWakeSources[1]);
    PendingIRQ[2] = (INREG32(&g_pGPC->GPC_ISR3) & g_PMWakeSources[2]);
    PendingIRQ[3] = (INREG32(&g_pGPC->GPC_ISR4) & g_PMWakeSources[3]);
        
    if (PendingIRQ[0] | PendingIRQ[1] | PendingIRQ[2] | PendingIRQ[3]) {
    
        //
        // something is pending right here, so just return...
        //
        //OALMSG(OAL_POWER&&OAL_VERBOSE,(TEXT("OALCPUPowerOff: pending IRQ's during suspend, resuming\r\n")));
        //OALMSG(OAL_POWER&&OAL_VERBOSE,(TEXT("OALCPUPowerOff: IRQ[0-3]: 0x%x, 0x%x, 0x%x, 0x%x\r\n"),
        //    PendingIRQ[0],PendingIRQ[1],PendingIRQ[2],PendingIRQ[3]));
        return;
    }

    //
    // turn off CacheRangeFlush and UpdateBarrier calls so kernel cannot 
    // call them right now.
    //
    PmClearFunctionPointers();

    //
    // looks like we're going down, better save what we need for
    // when the sun comes up...
    //
    PmPowerOffSave();
    
    //
    // configure CLPCR register for power mode
    //
    PmPowerConfigureLPMode(CCM_WAIT_POWER_OFF);

    //
    // If we're on MX6SL or using Dormant Mode for suspend, power down display
    // and USB
    //
    if (g_CpuType == IMX_CPU_MX6SL || g_SuspendMode == IMX6_SUSPEND_MODE_DORMANT) {

        PmPowerDownDisp();
        PmPowerDownUSB();
}

//
    // save GIC state
//
    PmPowerGicCpuSave();
    PmPowerGicDistSave();

    
    //
    // flush the entire cache and TLB
    //
    OEMCacheRangeFlush(NULL, 0, CACHE_SYNC_ALL);
    
    //
    // Suspend...
    //
    //g_pFuncIramEnterSuspend();
    PMOALStall(15000);

    //
    // mask all interrupts, which will be restored from PmPowerOffRestore()
    //
    SETREG32(&g_pGPC->GPC_IMR1,0xFFFFFFFF);
    SETREG32(&g_pGPC->GPC_IMR2,0xFFFFFFFF);
    SETREG32(&g_pGPC->GPC_IMR3,0xFFFFFFFF);
    SETREG32(&g_pGPC->GPC_IMR4,0xFFFFFFFF);

    //
    // Clear the RBC counter and RBC_EN bit and disable the REG_BYPASS_COUNTER
    //
    CLRREG32(&g_pCCM->CCR, CSP_BITFMASK(CCM_CCR_RBC_EN));
    CLRREG32(&g_pCCM->CCR, CSP_BITFMASK(CCM_CCR_REG_BYPASS_COUNT));

    //
    // need some cycles for the counters to clear and reset
    //
    PMOALStall(200);

    //
    // restore GIC state
    //
    PmPowerGicDistRestore();
    PmPowerGicCpuRestore();

    if (g_CpuType == IMX_CPU_MX6SL || g_SuspendMode == IMX6_SUSPEND_MODE_DORMANT) {

        PmPowerUpUSB();
        PmPowerUpDisp();
    }
 
    //
    // restore saved state
    //
    PmPowerOffRestore();  

    //
    // re-configure the analog behavior in stop mode.
    //
    SETREG32(&g_pANAMISC->ANA_MISC0_CLR, CSP_BITFMASK(ANALOG_MISC0_STOP_MODE_CONFIG));


    //
    // restore the CacheRangeFlush and UpdateBarrier calls so kernel can 
    // now call them.
    //
    PmSetFunctionPointers();

}


//-----------------------------------------------------------------------------
VOID BSPPowerOff()
{
}

//-----------------------------------------------------------------------------
VOID BSPPowerOn()
{
}
