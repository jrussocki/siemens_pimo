//-----------------------------------------------------------------------------
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//-----------------------------------------------------------------------------
//
//  Copyright (C) 2008-2010, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//-----------------------------------------------------------------------------
//
//  File:  vfp.c
//
//  VFP / NEON support code.
//
//-----------------------------------------------------------------------------

#include <bsp.h>
#include <vfp.h>


//-----------------------------------------------------------------------------
// Types


//-----------------------------------------------------------------------------
// External Functions
extern void VFP_Enable(void);
extern void VFP_Disable(void);
extern VOID NEON_SaveRegs(LPBYTE pExtraRegs);
extern VOID NEON_RestoreRegs(LPBYTE pExtraRegs);


//-----------------------------------------------------------------------------
// External Variables
extern PCSP_GPC_REGS g_pGPC;


//-----------------------------------------------------------------------------
// Global Variables


//------------------------------------------------------------------------------
//
//  Function:  VFP_InitExtraRegs
//
//  This function is called during the thread creation to initial 
//  the coprocessor register save area for each thread.
//
void VFP_InitExtraRegs(LPBYTE pExtraRegs)
{
    // Init the memory
    memset(pExtraRegs,0x0,VFP_EXTRA_REG_SIZE);
}


//------------------------------------------------------------------------------
//
//  Function:  VFP_Init
//
//  This function is called from OEMInit to setup the VFP / NEON coprocessor
//  and initial necessary data / functions in g_pOemGlobal.
//  This will enable OS to support iMX6 VFP / NEON context switch.
//
void VFP_Init(void)
{
    OALMSG(OAL_FUNC, (L"+VFP_Init\r\n"));

#ifdef BSP_OAL_NEON
    // Enable VFP
    VFP_Enable();

    // Init related data / functions in g_pOemGlobal
    g_pOemGlobal->fSaveCoProcReg = TRUE;
    g_pOemGlobal->cbCoProcRegSize = VFP_EXTRA_REG_SIZE;
    g_pOemGlobal->pfnInitCoProcRegs = VFP_InitExtraRegs;
    g_pOemGlobal->pfnSaveCoProcRegs = NEON_SaveRegs;
    g_pOemGlobal->pfnRestoreCoProcRegs = NEON_RestoreRegs;
#else
    // Disable VFP
    VFP_Disable();
    
    // Power down the NEON hardware
    INSREG32BF(&g_pGPC->NEON, GPC_NEON_NEONPDR, 1);
    while (EXTREG32BF(&g_pGPC->NEON, GPC_NEON_NEONFSMST) != 0);
#endif
    
    OALMSG(OAL_FUNC, (L"-VFP_Init\r\n"));
}
