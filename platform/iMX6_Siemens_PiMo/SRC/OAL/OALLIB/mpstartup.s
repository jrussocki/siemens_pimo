;******************************************************************************
;*
;* Copyright (c) Microsoft Corporation.  All rights reserved.
;*
;* Use of this source code is subject to the terms of the Microsoft end-user
;* license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
;* If you did not accept the terms of the EULA, you are not authorized to use
;* this source code. For a copy of the EULA, please see the LICENSE.RTF on your
;* install media.
;*
;******************************************************************************
;*
;* Copyright (C) 2010-2011, Freescale Semiconductor, Inc. All Rights Reserved.
;* THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
;* AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
;*
;******************************************************************************
;*
;* FILE:    mpstartup.s
;*
;* PURPOSE: Before control is transferred to the kernel, the boot loader
;*          calls this StartUp code to put the CPU into an initialized state.
;*
;******************************************************************************

	; Cache operations
    IMPORT  OALInvICache
    IMPORT  OALInvDCache
    IMPORT  OALInvITLB
    IMPORT  OALInvDTLB
    IMPORT  OALInvUTLB
    IMPORT  ConfigCpuAuxFeatures
    IMPORT  BSP_GetPlatInfoOffset

    INCLUDE mx6_base_regs.inc
    INCLUDE mx6_base_mem.inc
    INCLUDE image_cfg.inc
    
    EXPORT MpStartUp

;
; ARM constants
;
ARM_CPSR_PRECISE            EQU     (1 << 8)
ARM_CPSR_IRQDISABLE         EQU     (1 << 7)
ARM_CPSR_FIQDISABLE         EQU     (1 << 6)
ARM_CPSR_MODE_SVC           EQU     0x13
ARM_CPSR_MODE_IRQ           EQU     0x12

ARM_CTRL_MMU                EQU     (1 << 0)
ARM_CTRL_ALIGN              EQU     (1 << 1)
ARM_CTRL_DCACHE             EQU     (1 << 2)
ARM_CTRL_FLOW               EQU     (1 << 11)
ARM_CTRL_ICACHE             EQU     (1 << 12)
ARM_CTRL_VECTORS            EQU     (1 << 13)
ARM_CTRL_TRE                EQU     (1 << 28)

ARM_CACR_FULL               EQU     0x3

; VFP uses coproc 10 for single-precision instructions
ARM_VFP_SP_COP              EQU     10
ARM_VFP_SP_ACCESS           EQU     (ARM_CACR_FULL << (ARM_VFP_SP_COP*2))

; VFP uses coproc 11 for double-precision instructions
ARM_VFP_DP_COP              EQU     11
ARM_VFP_DP_ACCESS           EQU     (ARM_CACR_FULL << (ARM_VFP_DP_COP*2))

; Configure coprocessor access control
ARM_CACR_CONFIG             EQU     (ARM_VFP_SP_ACCESS | ARM_VFP_DP_ACCESS)

;from versatile
ARM_CP15_CTRL_DEF			EQU		0x10C50878	; BP enabled, TEX enabled
ARM_CP15_PRRR_DEF			EQU     0xFF098AA4
ARM_CP15_NMRR_DEF			EQU     0x40404040


;
; GIC constants
;
GIC_INTF_ICCICR_OFFSET      EQU     0x0000
GIC_INTF_ICCPMR_OFFSET      EQU     0x0004

GIC_DIST_ICDDCR_OFFSET      EQU     0x0000
GIC_DIST_ICDISR_OFFSET      EQU     0x0080
GIC_DIST_ICDICER_OFFSET     EQU     0x0180
GIC_DIST_ICDIPR_OFFSET      EQU     0x0400
GIC_DIST_ICDIPTR_OFFSET     EQU     0x0800
GIC_DIST_ICDICFR_OFFSET     EQU     0x0C00

;
; OAL_MP_CONTEXT offsets
;
MP_CONTEXT_SCTLR_OFFSET     EQU     0x0000
MP_CONTEXT_TTBR0_OFFSET     EQU     0x0004
MP_CONTEXT_TTBR1_OFFSET     EQU     0x0008
MP_CONTEXT_TTBCR_OFFSET     EQU     0x000C
MP_CONTEXT_DACR_OFFSET      EQU     0x0010
MP_CONTEXT_ACTRL_OFFSET     EQU     0x0014
MP_CONTEXT_PFNCONT_OFFSET   EQU     0x0018
MP_CONTEXT_PACTIVE_OFFSET   EQU     0x001C

    OPT 2                                       ; disable listing
    INCLUDE kxarm.h
    INCLUDE armmacros.s
    OPT 1                                       ; reenable listing


;******************************************************************************
;*
;* FUNCTION:    MpStartUp
;*
;* DESCRIPTION: System bootstrap function
;*
;* PARAMETERS:  None
;*
;* RETURNS:     None
;*
;******************************************************************************

    STARTUPTEXT
    ALIGN 4
    NESTED_ENTRY MpStartUp, |.astart|

    PROLOG_END MpStartUp

    ;--------------------------------------------------------------------------
    ; MS RECOMMENDATION:
    ; Put the processor in supervisor mode
    ; Disable the interrupt request (IRQ) and fast interrupt request (FIQ)
    ; inputs
    ;--------------------------------------------------------------------------

    mrs  R0, cpsr                     ; r0 = CPSR
    BIC  R0, R0, #0x1F                ; Clear mode bits.
    ORR  R0, R0, #ARM_CPSR_MODE_SVC   ; enter supervisor mode
    ORR  R0, R0, #ARM_CPSR_IRQDISABLE ; disable (mask out) normal IRQ
    ORR  R0, R0, #ARM_CPSR_FIQDISABLE ; disable (mask out) fast IRQ
    ;ORR R0, R0, #ARM_CPSR_PRECISE    ; enable precise data aborts
    MSR  cpsr_xc, R0                  ; update CPSR control bits

    ;--------------------------------------------------------------------------
    ; MS RECOMMENDATION:
    ; Disable memory management unit (MMU) and both the instruction and data
    ; caches
    ;--------------------------------------------------------------------------
    
    mrc     p15, 0, r0, c1, c0, 0               ; r0 = system control reg
    bic     r0, r0, #ARM_CTRL_ICACHE            ; disable ICache
    bic     r0, r0, #ARM_CTRL_DCACHE            ; disable DCache
    bic     r0, r0, #ARM_CTRL_MMU               ; disable MMU
    bic     r0, r0, #ARM_CTRL_VECTORS           ; set vector base to 0x00000000
    orr     r0, r0, #ARM_CTRL_FLOW              ; program flow prediction enabled
    bic     r0, r0, #ARM_CTRL_TRE               ; disable TEX remap
    bic     r0, r0, #ARM_CTRL_ALIGN             ; disable Alignments Fault Checking
    mcr     p15, 0, r0, c1, c0, 0               ; update system control reg

    dsb
    isb

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	; Setup initial stack pointer.
	; Each CPU uses a diffent stack (offset by 0x1000 * (CPU Num - 1))
	;
	mrc     p15, 0, r0, c0, c0, 5
	and     r1, r0, #0x3
	mov     r2, #0
add_loop
	subs    r1, r1, #1
	addne   r2, r2, #0x1000
	bne     add_loop
	mov     r3, pc
    and     r3, r3, #0xF0000000
	ldr     r0, =IMAGE_BOOT_STACK_RAM_PA_START
	sub     r0, r0, r2
	add     r3, r3, r0
	mov		sp, r3

	bl BSP_GetPlatInfoOffset
	mov r1, r0
	mov     r0, pc
	and     r0, r0, #0xF0000000
	add     r0, r0, r1
	bl      ConfigCpuAuxFeatures

	dsb
	isb

    ;--------------------------------------------------------------------------
    ; MS RECOMMENDATION:
    ; Flush or invalidate the instruction and data caches and the translation
    ; look-aside buffer (TLB) and empty the write buffers
    ;--------------------------------------------------------------------------
    
    ;
    ; TLB and Cache cleanups
    ; Also invokes appropriate barrier instructions
    bl      OALInvICache
    mov     r0, #0                          ; L1 cache only
    bl      OALInvDCache
    bl 		OALInvUTLB						; invalidate entire unified TLB
    bl      OALInvITLB					; invalidate entire instruction TLB
    bl      OALInvDTLB					; invalidate entire data TLB
    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        ; ----- ARM errata #751472 -----
    ; Description:  An interrupted ICIALLUIS operation may prevent the 
    ;   completion of a following broadcasted operation.
    ;
    ; Workaround:  Set bit[11] in the undocumented Diagnostic Control 
    ;   register placed in CP15 c15 0 c0 1.
    ;
    mrc     p15, 0, r0, c15, c0, 1
    orr     r0, r0, #(1 << 11)
    mcr     p15, 0, r0, c15, c0, 1
    
    ; ARM: 743622�Faulty logic in the Store Buffer may lead to data corruption
    mrc		p15, 0, r0, c15, c0, 1	; read diagnostic register
	orr		r0, r0, #(1 << 6)		; set bit #6
	mcr		p15, 0, r0, c15, c0, 1	; write diagnostic register
	
	; ARM: errata 742230 DMB operation may be faulty
	mrc	p15, 0, r0, c15, c0, 1		; read diagnostic register;
	orr	r0, r0, #1 << 4				; set bit #4
	mcr	p15, 0, r0, c15, c0, 1		; write diagnostic register

    ;
    ; Configure ARM coprocessor access control register
    ;
    ldr     r0, =ARM_CACR_CONFIG                ; r0 = CACR configuration
    mcr     p15, 0, r0, c1, c0, 2               ; update CACR
    
GIC_CONFIG
    ; 128 shared peripheral + 16 private + 16 software = 160 interrupts
    ; 160 interrupts are covered by the following register types:
    ;       1 bit/interrupt = 5 registers (ICDICER, ICDISR)
    ;       2 bits/interrupt = 10 registers (ICDICFR)
    ;       8 bits/interrupt = 40 registers (ICDIPR)

    ; Disable interrupts at GIC distributor level
    ldr     r0, =0
    ldr     r1, =CSP_BASE_REG_PA_GIC_DIST
    str     r0, [r1, #GIC_DIST_ICDDCR_OFFSET]
    
    ; Configure all interrupts for level-sensitive, 1-N model.
    ldr     r0, =0x55555555
    ldr     r1, =(CSP_BASE_REG_PA_GIC_DIST+GIC_DIST_ICDICFR_OFFSET)
    str     r0, [r1, #0x00]
    str     r0, [r1, #0x04]
    str     r0, [r1, #0x08]
    str     r0, [r1, #0x0C]
    str     r0, [r1, #0x10]
    str     r0, [r1, #0x14]
    str     r0, [r1, #0x18]
    str     r0, [r1, #0x1C]
    str     r0, [r1, #0x20]
    str     r0, [r1, #0x24]

    ; Configure priority for all interrupts as 128
    ldr     r0, =0x80808080
    ldr     r1, =(CSP_BASE_REG_PA_GIC_DIST+GIC_DIST_ICDIPR_OFFSET)
    str     r0, [r1, #0x00]
    str     r0, [r1, #0x04]
    str     r0, [r1, #0x08]
    str     r0, [r1, #0x0C]
    str     r0, [r1, #0x10]
    str     r0, [r1, #0x14]
    str     r0, [r1, #0x18]
    str     r0, [r1, #0x1C]
    str     r0, [r1, #0x20]
    str     r0, [r1, #0x24]
    str     r0, [r1, #0x28]
    str     r0, [r1, #0x2C]
    str     r0, [r1, #0x30]
    str     r0, [r1, #0x34]
    str     r0, [r1, #0x38]
    str     r0, [r1, #0x3C]
    str     r0, [r1, #0x40]
    str     r0, [r1, #0x44]
    str     r0, [r1, #0x48]
    str     r0, [r1, #0x4C]
    str     r0, [r1, #0x50]
    str     r0, [r1, #0x54]
    str     r0, [r1, #0x58]
    str     r0, [r1, #0x5C]
    str     r0, [r1, #0x60]
    str     r0, [r1, #0x64]
    str     r0, [r1, #0x68]
    str     r0, [r1, #0x6C]
    str     r0, [r1, #0x70]
    str     r0, [r1, #0x74]
    str     r0, [r1, #0x78]
    str     r0, [r1, #0x7C]
    str     r0, [r1, #0x80]
    str     r0, [r1, #0x84]
    str     r0, [r1, #0x88]
    str     r0, [r1, #0x8C]
    str     r0, [r1, #0x90]
    str     r0, [r1, #0x94]
    str     r0, [r1, #0x98]
    str     r0, [r1, #0x9C]

    ; Configure interrupt priority mask.  
    ; Priority mask = 255 ==> all interrupts with priority value less than
    ; 255 will be unmasked
    ldr     r0, =255
    ldr     r1, =(CSP_BASE_REG_PA_GIC_INTF)
    str     r0, [r1, #GIC_INTF_ICCPMR_OFFSET]
    
    ; Enable interrupts at GIC distributor level
    ldr     r0, =3
    ldr     r1, =CSP_BASE_REG_PA_GIC_DIST
    str     r0, [r1, #GIC_DIST_ICDDCR_OFFSET]

    ; Enable interrupts at GIC interface level
    ldr     r0, =7
    ldr     r1, =(CSP_BASE_REG_PA_GIC_INTF)
    str     r0, [r1, #GIC_INTF_ICCICR_OFFSET]

    ; 
    ; Restore context provided by primary CPU
    ;

    ldr     r1, =IMAGE_WINCE_OAL_IRAM_PA_START
    ldr     r0, [r1, #MP_CONTEXT_ACTRL_OFFSET]
    mcr     p15, 0, r0, c1, c0, 1               ; Update ACTRL
    ldr     r0, [r1, #MP_CONTEXT_DACR_OFFSET]
    mcr     p15, 0, r0, c3, c0, 0               ; Update DACR
    ldr     r0, [r1, #MP_CONTEXT_TTBR0_OFFSET]
    mcr     p15, 0, r0, c2, c0, 0               ; Update TTBR0
    ldr     r0, [r1, #MP_CONTEXT_TTBR1_OFFSET]
    mcr     p15, 0, r0, c2, c0, 1               ; Update TTBR1
    ldr     r0, [r1, #MP_CONTEXT_TTBCR_OFFSET]
    mcr     p15, 0, r0, c2, c0, 2               ; Update TTBCR
        
    ldr     r3, [r1, #MP_CONTEXT_PACTIVE_OFFSET]
    ldr     r4, [r1, #MP_CONTEXT_PFNCONT_OFFSET]

    ldr     r2, =MpVirtualStart     ; Get virtual address of 'VirtualStart' label.

    ; Enable the MMU
    ldr     r0, [r1, #MP_CONTEXT_SCTLR_OFFSET]

    ORR    R0, R0, #ARM_CTRL_ICACHE ; enable ICache
    ORR    R0, R0, #ARM_CTRL_DCACHE ; enable DCache
    ORR    R0, R0, #ARM_CTRL_MMU    ; enable MMU

    ORR     R0, R0, #(1 :SHL: 22)   ; Enable unaligned access support (U bit)
    BIC     R0, R0, #(1 :SHL: 1)    ; Disable alignment faults (A bit)

    mcr     p15, 0, r0, c1, c0, 0   ; All memory accesses are now virtual.

    ; Jump to the virtual address of the 'blVirtualStart' label.
    mov     pc, r2  
    nop
    nop
    nop

    ALIGN
MpVirtualStart

    NOP

    ; invalidate cache again

    ; match ACTRL settings from core zero

    MRC p15, 0, R0, C1, C0, 1 ; Read ACTRL
    ORR R0, R0, #0x40         ; Turn on ACTRL.SMP
    ORR R0, R0, #1            ; Turn on ACTRL.FW
    ORR R0, R0, #2            ; Enable L2 prefetch
    ORR R0, R0, #4            ; Enable L1 prefetch
    MCR p15, 0, R0, C1, C0, 1 ; Write ACTRL

    ; Query CPU ID in lower 2 bits of MPIDR
    mrc     p15, 0, r2, c0, c0, 5
    and     r2, r2, #3

    ; Convert CPU ID into mask
    mov     r1, #1
    mov     r1, r1, lsl r2

    ; no need to use exclusive access as we're starting CPU 1-by-1
    ldr     r0, [r3]
    orr     r0, r0, r1
    str     r0, [r3]
       
    mov     pc, r4
    nop
    nop
    nop

MpSpin
    b       MpSpin

    EPILOG_ENTRY MpStartUp

    NESTED_END MpStartUp

    END
