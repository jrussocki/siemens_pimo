//-----------------------------------------------------------------------------
//
// Copyright (C) 2010-2011, Freescale Semiconductor, Inc. All Rights Reserved.
// THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
// AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//-----------------------------------------------------------------------------
//
// Module: smp.c
//
// This module provides the BSP-specific interfaces required to support
// the OAL SMP code.
//
//-----------------------------------------------------------------------------

#include <bsp.h>
#include <armintr.h>
#include <oal_utils.h>
#include <common_pm.h>
#include "intrinsic.h"

#pragma warning(push, 4)

// Need to disable function pointer cast warnings.  We need to pass 
// physical mapping of MpStartUp in SCR GPR register
#pragma warning(disable: 4054)

//-----------------------------------------------------------------------------
// Types
typedef struct
{
    unsigned int SCTLR;                         // 0x0000
    unsigned int TTBR0;                         // 0x0004
    unsigned int TTBR1;                         // 0x0008
    unsigned int TTBCR;                         // 0x000c
    unsigned int DACR;                          // 0x0010
    unsigned int ACTRL;                         // 0x0014

    FARPROC pContinue;                          // 0x0018

    volatile unsigned int* volatile pCpuActive; // 0x001c
    volatile unsigned int cpuActive;            // 0x0020

}OAL_MP_CONTEXT;

typedef void (*PFNIPICCALLBACK)(DWORD);

//------------------------------------------------------------------------------
// External Function Prototypes

extern void MpStartUp();

extern void MpSetACTRLSMPFW();

//extern void OALCachePL310GlobalsInit(UINT32 baseAddr);

//------------------------------------------------------------------------------
// External Variables
extern PCSP_GIC_DIST_REGS g_pGIC_DIST;
extern PCSP_EPIT_REG g_pEPIT;

//------------------------------------------------------------------------------
// Defines
#define CPU0_MASK   (1U << 0)
#define CPU1_MASK   (1U << 1)
#define CPU2_MASK   (1U << 2)
#define CPU3_MASK   (1U << 3)
#define CPUALL_MASK (CPU0_MASK|CPU1_MASK|CPU2_MASK|CPU3_MASK)
#define CPUAll_NUM  4

//-----------------------------------------------------------------------------
// Global Variables

// g_OalSmpEnable is a FIXUPVAR controlled by IMGMPENABLE. SMP is disabled by default.
const BOOL volatile g_bOalSmpEnable = FALSE;

volatile unsigned int* volatile g_pOalMpCpuActive;

extern volatile UINT32 g_PmCpuActiveMask;

//------------------------------------------------------------------------------
// Local Variables
static PCSP_SCU_REGS g_pSCU = NULL;
static PCSP_SRC_REGS g_pSRC = NULL;

//------------------------------------------------------------------------------
// Local Function Prototypes
static DWORD OEMMpPerCPUInit();
static void OEMMpCacheRangeFlush(void* pAddress, DWORD length, DWORD flags);

void OALMpSendSGI(UINT32 sgi)
{
    OALMSG(OAL_FUNC, (L"[+OALMpSendSGI]\r\n") );

    OUTREG32(&g_pGIC_DIST->ICDSGIR, sgi);

    OALMSG(OAL_FUNC, (L"[-OALMpSendSGI]\r\n") );
}

BOOL OEMMpStartAllCPUs(PLONG pnCpus, FARPROC pContinue)
{
    BOOL rc = FALSE;
    volatile OAL_MP_CONTEXT* volatile pOalMpContext = NULL;
    UINT32 actrl = 0;

    OALMSG(OAL_FUNC, (L"[+OEMMpStartAllCPUs]\r\n") );

    // Map SCU
    g_pSCU = (PCSP_SCU_REGS)OALPAtoUA(CSP_BASE_REG_PA_SCU);
    if(g_pSCU==NULL)
    {
        OALMSG(OAL_ERROR, (L"OEMMpStartAllCPUs:  SCU null pointer!\r\n") );
        goto cleanUp;
    }

    // Map SRC
    g_pSRC = (PCSP_SRC_REGS)OALPAtoUA(CSP_BASE_REG_PA_SRC);
    if(g_pSRC==NULL)
    {
        OALMSG(OAL_ERROR, (L"OEMMpStartAllCPUs:  SRC null pointer!\r\n") );
        goto cleanUp;
    }

    // Use OAL reserved IRAM region for passing primary CPU context to
    // secondary CPUs.
    pOalMpContext = (volatile OAL_MP_CONTEXT*)OALPAtoUA(IMAGE_WINCE_OAL_IRAM_PA_START);
    if(pOalMpContext==NULL)
    {
        OALMSG(OAL_ERROR, (L"OEMMpStartAllCPUs:  IRAM null pointer!\r\n") );
        goto cleanUp;
    }

    // Query for the number of CPUs
    *pnCpus = EXTREG32BF(&g_pSCU->CFG, SCU_CFG_NCPU) + 1;
    OALMSG(1, (L"Detected %d CPUs\r\n", *pnCpus) );

    // Initialize SCU
    INSREG32BF(&g_pSCU->CTRL, SCU_CTRL_SCUEN, 0);
    OUTREG32(&g_pSCU->INV, 0x0000FFFF);
    OUTREG32(&g_pSCU->SAC, 0x0000000F);
    INSREG32BF(&g_pSCU->CTRL, SCU_CTRL_SCUEN, 1);

    // Enable SMP in ACTRL
    actrl = _MoveFromCoprocessor(15, 0, 1, 0, 1);
    actrl |= (1U << 6);
    _MoveToCoprocessor(actrl, 15, 0, 1, 0, 1);

    // Save context of primary CPU to IRAM
    pOalMpContext->SCTLR = _MoveFromCoprocessor(15, 0, 1, 0, 0);
    pOalMpContext->TTBR0 = _MoveFromCoprocessor(15, 0, 2, 0, 0);
    pOalMpContext->TTBR1 = _MoveFromCoprocessor(15, 0, 2, 0, 1);
    pOalMpContext->TTBCR = _MoveFromCoprocessor(15, 0, 2, 0, 2);
    pOalMpContext->DACR = _MoveFromCoprocessor(15, 0, 3, 0, 0);
    pOalMpContext->ACTRL = _MoveFromCoprocessor(15, 0, 1, 0, 1);
    pOalMpContext->pContinue = pContinue;

    // Physical jump address of secondary CPUs will be read from ROM code as follows:
    // GPR3 = CPU1 start address
    // GPR5 = CPU2 start address
    // GPR7 = CPU3 start address

    g_pSRC->GPR3 = (UINT32)OALVAtoPA( (void*)MpStartUp);
    g_pSRC->GPR4 = 0;

    g_pSRC->GPR5 = (UINT32)OALVAtoPA( (void*)MpStartUp);
    g_pSRC->GPR6 = 0;

    g_pSRC->GPR7 = (UINT32)OALVAtoPA( (void*)MpStartUp);
    g_pSRC->GPR8 = 0;
    
#if (_WINCEOSVER<=700)
#else
    g_pSRC->GPR3 |= 1;
    g_pSRC->GPR5 |= 1;
    g_pSRC->GPR7 |= 1;
#endif

    g_pOalMpCpuActive = &pOalMpContext->cpuActive;
    pOalMpContext->pCpuActive = g_pOalMpCpuActive;

	RETAILMSG(1, (L"Waiting for %d CPUs\r\n", *pnCpus));

	// CPU0 is already active
    OALMSG(1, (L" CPU0 is started\r\n") );
    pOalMpContext->cpuActive = CPU0_MASK;
	
	if (*pnCpus > 1)
	{
		// Wake up secondary CPUs
		INSREG32BF(&g_pSRC->SCR, SRC_SCR_CORE1_ENABLE, 1);
		while( (pOalMpContext->cpuActive & CPU1_MASK) != CPU1_MASK);
		OALMSG(1, (L" CPU1 is started\r\n") );
	}

	if (*pnCpus > 2)
	{
		INSREG32BF(&g_pSRC->SCR, SRC_SCR_CORE2_ENABLE, 1);
		while( (pOalMpContext->cpuActive & CPU2_MASK) != CPU2_MASK);
		OALMSG(1, (L" CPU2 is started\r\n") );
	}

	if (*pnCpus > 3)
	{
		INSREG32BF(&g_pSRC->SCR, SRC_SCR_CORE3_ENABLE, 1);
		while( (pOalMpContext->cpuActive & CPU3_MASK) != CPU3_MASK);
		OALMSG(1, (L" CPU3 is started\r\n") );
	}

	MpSetACTRLSMPFW();

#if 0
    OALMSG(1, (L"[OEMMpStartAllCPUs:g_oalCacheInfo.L1Flags==%u]\r\n", g_oalCacheInfo.L1Flags) );
    OALMSG(1, (L"[OEMMpStartAllCPUs:g_oalCacheInfo.L1DLineSize==%u]\r\n", g_oalCacheInfo.L1DLineSize) );
    OALMSG(1, (L"[OEMMpStartAllCPUs:g_oalCacheInfo.L1DNumWays==%u]\r\n", g_oalCacheInfo.L1DNumWays) );
    OALMSG(1, (L"[OEMMpStartAllCPUs:g_oalCacheInfo.L1DSetsPerWay==%u]\r\n", g_oalCacheInfo.L1DSetsPerWay) );
    OALMSG(1, (L"[OEMMpStartAllCPUs:g_oalCacheInfo.L1DSize==%u]\r\n", g_oalCacheInfo.L1DSize) );
    OALMSG(1, (L"[OEMMpStartAllCPUs:g_oalCacheInfo.L1ILineSize==%u]\r\n", g_oalCacheInfo.L1ILineSize) );
    OALMSG(1, (L"[OEMMpStartAllCPUs:g_oalCacheInfo.L1INumWays==%u]\r\n", g_oalCacheInfo.L1INumWays) );
    OALMSG(1, (L"[OEMMpStartAllCPUs:g_oalCacheInfo.L1ISetsPerWay==%u]\r\n", g_oalCacheInfo.L1ISetsPerWay) );
    OALMSG(1, (L"[OEMMpStartAllCPUs:g_oalCacheInfo.L1ISize==%u]\r\n", g_oalCacheInfo.L1ISize) );
#endif


#if 0
    OALMSG(1, (L"[OEMMpStartAllCPUs:g_oalCacheInfo.L2Flags==%u]\r\n", g_oalCacheInfo.L2Flags) );
    OALMSG(1, (L"[OEMMpStartAllCPUs:g_oalCacheInfo.L2DLineSize==%u]\r\n", g_oalCacheInfo.L2DLineSize) );
    OALMSG(1, (L"[OEMMpStartAllCPUs:g_oalCacheInfo.L2DNumWays==%u]\r\n", g_oalCacheInfo.L2DNumWays) );
    OALMSG(1, (L"[OEMMpStartAllCPUs:g_oalCacheInfo.L2DSetsPerWay==%u]\r\n", g_oalCacheInfo.L2DSetsPerWay) );
    OALMSG(1, (L"[OEMMpStartAllCPUs:g_oalCacheInfo.L2DSize==%u]\r\n", g_oalCacheInfo.L2DSize) );
    OALMSG(1, (L"[OEMMpStartAllCPUs:g_oalCacheInfo.L2ILineSize==%u]\r\n", g_oalCacheInfo.L2ILineSize) );
    OALMSG(1, (L"[OEMMpStartAllCPUs:g_oalCacheInfo.L2INumWays==%u]\r\n", g_oalCacheInfo.L2INumWays) );
    OALMSG(1, (L"[OEMMpStartAllCPUs:g_oalCacheInfo.L2ISetsPerWay==%u]\r\n", g_oalCacheInfo.L2ISetsPerWay) );
    OALMSG(1, (L"[OEMMpStartAllCPUs:g_oalCacheInfo.L2ISize==%u]\r\n", g_oalCacheInfo.L2ISize) );
#endif

    OEMMpPerCPUInit();

    g_pOemGlobal->pfnCacheRangeFlush = OEMMpCacheRangeFlush;

    rc = TRUE;

cleanUp:

    OALMSG(OAL_FUNC, (L"[-OEMMpStartAllCPUs]\r\n") );

    return rc;
}

static DWORD OEMMpPerCPUInit()
{
    DWORD cpuId = OALGetCpuId();
   
	MpSetACTRLSMPFW();  
	OALMSG(1, (L"[OEMMpPerCPUInit CPU[%d] Online]\r\n",cpuId));
  
	return cpuId;
}

void OEMIpiHandler(DWORD dwCommand, DWORD dwData)
{
    DWORD cpuId = 0;
    PFNIPICCALLBACK pfnIPICallBack = NULL;
    OALMSG(OAL_FUNC, (L"[+OEMIpiHandler]\r\n") );

    switch(dwCommand)
    {
    case IPI_COMMAND_RESCHED:
        OALTimerUpdateRescheduleTime(dwData);
        break;

   case IPI_COMMAND_CACHE:
	    //We'll only get here if the entire L1D has to be cleaned/flushed.
	    //In other cases the cache operations are taken care of by the SCU, 
	    //they are broadcast automaticly, so no need to do them twice.
        OEMCacheRangeFlush(NULL, 0, dwData);
        break;

    case IPI_TEST_CALL_FUNCTION_PTR:
        //For oal IPI test
        cpuId = GetCurrentProcessorNumber();
        pfnIPICallBack = (PFNIPICCALLBACK)dwData;
        (pfnIPICallBack)(cpuId);
        break;
    }

    OALMSG(OAL_FUNC, (L"[-OEMIpiHandler]\r\n") );
}

//-----------------------------------------------------------------------------
//
// Function: OEMSendIPI
//
// 
// This function sends an interprocessor interrupt (IPI).
//
// Parameters:
//     dwType 
//         [in] Specifies the type of IPI that you want to send.
//
//     dwTarget
//         [in] If the dwType value is IPI_TYPE_SPECIFIC_CPU, specifies 
//         the CPU to which OEMSendIpi sends the IPI.
//
// Returns:
//     Returns true if successful; otherwise, returns false..
//
//-----------------------------------------------------------------------------
BOOL OEMSendIPI(DWORD dwType, DWORD dwTarget)
{
    UINT32 sgi = IRQ_SGI_IPI;
    UINT32 idMask = 0;

    OALMSG(OAL_FUNC, (L"[+OEMSendIPI]\r\n") );

    switch(dwType)
    {
    case IPI_TYPE_ALL_INCLUDE_SELF:
        CSP_BITFINS(sgi, GIC_ICDSGIR_TARGETLISTFILTER, GIC_ICDSGIR_TARGETLISTFILTER_SELF);
        OALMpSendSGI(sgi);

    // Fall through
    case IPI_TYPE_ALL_BUT_SELF:
		idMask = *g_pOalMpCpuActive & ~(1<<OALGetCpuId() );
        break;

    case IPI_TYPE_SPECIFIC_CPU:
        idMask = *g_pOalMpCpuActive & (1<<dwTarget);
        break;

    default:
        sgi = 0;
        break;
    }

    if(idMask)
    {
        CSP_BITFINS(sgi, GIC_ICDSGIR_TARGETLISTFILTER, GIC_ICDSGIR_TARGETLISTFILTER_LIST);
        CSP_BITFINS(sgi, GIC_ICDSGIR_CPUTARGETLIST, idMask);
        OALMpSendSGI(sgi);
    }

    OALMSG(OAL_FUNC, (L"[-OEMSendIPI]\r\n") );

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  OEMMpCpuPowerFunc
//
// This function changes the power state of a specific CPU in a multiprocessor platform.
//
// Parameters:
//      dwProcessor
//          Hardware ID (CPU ID) of the processor to be turned on or off.
//
//      fOn
//          Indicates whether to turn the processor on or off. The value true indicates on, and false indicates off.
//
//      dwHint
//          A processor-specific hint that is passed to this function by the CePowerOffProcessor function. 
//          OEMMpCpuPowerFunc uses the dwHint value only when the processor is being turned off (fOnOff is set to false). 
//          This parameter is a hint to the OEM, and it can indicate whether to implement a partial power-down or a full shutdown. 
//          A value of 1 indicates full shutdown, and other values are implementation-specific.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
BOOL OEMMpCpuPowerFunc(DWORD dwProcessor, BOOL fOn, DWORD dwHint)
{
    BOOL rc = FALSE;
    UINT oldVal = 0;
    ULONG sgi = IRQ_SGI_PWR;

    dwHint;

    OALMSG(OAL_FUNC, (L"[+OEMMpCpuPowerFunc]\r\n") );

    // Processor number validation, cpu 0 should be ignored
    if(dwProcessor==0 || dwProcessor>=CPUAll_NUM)
    {
        OALMSG(OAL_ERROR, (L"[OEMMpCpuPowerFunc: Invalid parameter]\r\n") );
    }
    else
    {
        NKAcquireOalSpinLock();

        // __dsb is done before call to OEMMpCpuPowerFunc

        if(fOn)
        {
            //Send IPI to turn on processor and set the flag
            CSP_BITFINS(sgi, GIC_ICDSGIR_TARGETLISTFILTER, GIC_ICDSGIR_TARGETLISTFILTER_LIST);
            CSP_BITFINS(sgi, GIC_ICDSGIR_CPUTARGETLIST, 1<<dwProcessor);
            OALMpSendSGI(sgi);

            do
            {
                oldVal = *g_pOalMpCpuActive;
            }
            while( (UINT32)InterlockedTestExchange( (LPLONG)g_pOalMpCpuActive, oldVal, oldVal | (1<<dwProcessor) ) != oldVal);
           
            do 
            {
                oldVal = g_PmCpuActiveMask;
            } 
            while( (UINT32)InterlockedTestExchange( (LPLONG)&g_PmCpuActiveMask, oldVal, oldVal | (1<<dwProcessor) ) != oldVal);
        }
        else
        {
            // Remove processor from active list.
            do
            {
                oldVal = *g_pOalMpCpuActive;
            }
            while( (UINT32)InterlockedTestExchange( (LPLONG)g_pOalMpCpuActive, oldVal, oldVal & ( ~(1<<dwProcessor) ) ) != oldVal);

            do 
            {
                oldVal = g_PmCpuActiveMask;
            }
            while( (UINT32)InterlockedTestExchange( (LPLONG)&g_PmCpuActiveMask, oldVal, oldVal & ( ~(1<<dwProcessor) ) ) != oldVal);
            // The processor will enter WFI soon.
        }

        NKReleaseOalSpinLock();

        rc = TRUE;
    }

	// Hit some type of system freeze when turning cores on and off very
	// quickly; Reference tux test case "tux -o -n -d ksmpdepth.dll -x 2112";
	// Adding OALMSG here increases stability of the test case though it still
	// fails roughly 1 in 25 times (the test case also fails roughly 1 in 5
	// times in the step that creates the threads);
	//
    //OALMSG(1, (L"[OEMMpCpuPowerFunc1]\r\n") );
    //OALMSG(1, (L"[OEMMpCpuPowerFunc2]\r\n") );
	//OALMSG(1, (L"[OEMMpCpuPowerFunc3]\r\n") );
	//OALMSG(1, (L"[OEMMpCpuPowerFunc4]\r\n") );
	//OALMSG(1, (L"[OEMMpCpuPowerFunc5]\r\n") );
	//OALMSG(1, (L"[OEMMpCpuPowerFunc6]\r\n") );
	//OALMSG(1, (L"[OEMMpCpuPowerFunc7]\r\n") );
	//OALMSG(1, (L"[OEMMpCpuPowerFunc8]\r\n") );

    OALMSG(OAL_FUNC, (L"[-OEMMpCpuPowerFunc]\r\n") );

    return rc;
}

void OEMIdleEx(LARGE_INTEGER* pliIdleTime)
{
    UINT32 countBeforeIdle = 0;
    UINT32 countAfterIdle = 0;
    INT32 idleCounts = 0;
	
    OALMSG(OAL_FUNC, (L"[+OEMIdleEx]\r\n") );

	// Find how many hi-res ticks were are consumed in the current system tick
    // before idle mode is entered
    countBeforeIdle = INREG32(&g_pEPIT->CNT);

    // Move SoC/CPU to idle mode
    OALCPUIdle(OEM_IDLE_SMP);

    // Find how many hi-res ticks were are consumed in the current system tick
    // after idle mode is exited
    countAfterIdle = INREG32(&g_pEPIT->CNT);

    // Get real idle value. If result is negative we didn't idle at all.

    idleCounts = countBeforeIdle - countAfterIdle;

    if(idleCounts<0) 
    {
        idleCounts = 0;
    }
    
    // Update pliIdleTime
    pliIdleTime->QuadPart += idleCounts;
    
    OALMSG(OAL_FUNC, (L"[-OEMIdleEx]\r\n") );
}

void OALTimerUpdateRescheduleTimeEx(DWORD time)
{
    //OALMSG(OAL_FUNC, (L"[+OALTimerUpdateRescheduleTimeEx]\r\n") );

    // We only want the primary CPU to update the OS tick timer.  If
    // request to update reschedule time comes from secondary CPU,
    // send IPI to primary CPU to do the update.

    if(OALGetCpuId()==0)
    {
       OALTimerUpdateRescheduleTime(time);
    }
    else
    {
        NKSendInterProcessorInterrupt(IPI_TYPE_SPECIFIC_CPU, 0, IPI_COMMAND_RESCHED, time);
    }

    //OALMSG(OAL_FUNC, (L"[-OALTimerUpdateRescheduleTimeEx]\r\n") );
}

static void OEMMpCacheRangeFlush(LPVOID pAddr, DWORD dwLength, DWORD dwFlags)
{
	//Cache operations to be passed to the other CPUS
	DWORD dwSMPFlags = dwFlags;
	UINT32 addrVal, dwLen;

    // Check if maintenance is for local CPU only
	if(dwFlags & CSF_CURR_CPU_ONLY)
	{
		dwSMPFlags = 0;
	}	
	else
	{
        // TLB, L2 and I cache maintenance need not be broadcast
        dwSMPFlags &= ~(CACHE_SYNC_FLUSH_TLB   | CACHE_SYNC_L2_DISCARD | 
							   CACHE_SYNC_L2_WRITEBACK);
	}
	
    if(dwLength)
    {
        // No need to notify other CPUs
		// Ops by MVA are broadcast. 
		// Invalidate all will be broadcast if we use ICIALLUIS	
		dwSMPFlags &= ~CACHE_SYNC_INSTRUCTIONS; 
  
        if(dwSMPFlags & (CACHE_SYNC_WRITEBACK | CACHE_SYNC_DISCARD))
        {
			addrVal = (UINT32)pAddr;
			// Normalize address to cache line alignment.  
			addrVal &= ~(g_oalCacheInfo.L1DLineSize - 1);
			dwLen = ((UINT32)pAddr - (UINT32)addrVal);

		    //Check if partial cache flush is requested	
			if(dwLen < g_oalCacheInfo.L1DSize)
			{
				//Ops by MVA are broadcast.
                dwSMPFlags &= ~(CACHE_SYNC_WRITEBACK | CACHE_SYNC_DISCARD);
			}
        }
    }
	
	// If we still need to broadcast
    if (dwSMPFlags & CACHE_SYNC_ALL)
    {
        NKSendInterProcessorInterrupt(IPI_TYPE_ALL_BUT_SELF, 0, IPI_COMMAND_CACHE, dwSMPFlags);
    }
        
    // Call OEMCacheRangeFlush for local CPU 
    OEMCacheRangeFlush (pAddr, dwLength, dwFlags);
        
}

//-----------------------------------------------------------------------------
//
// Function: OALMpInit
//
// This function is called to initialize the OAL SMP support.
//
// Parameters:
// None.
//
// Returns:
// None.
//
//-----------------------------------------------------------------------------
void OALMpInit()
{
    OALMSG(OAL_FUNC, (L"+OALMpInit\r\n") );

    g_pOemGlobal->fMPEnable = g_bOalSmpEnable;

    if(g_pOemGlobal->fMPEnable)
    {
        g_pOemGlobal->pfnStartAllCpus      = OEMMpStartAllCPUs;
        g_pOemGlobal->pfnMpPerCPUInit      = (PFN_MpPerCPUInit)OEMMpPerCPUInit;
        g_pOemGlobal->pfnIpiHandler        = OEMIpiHandler;
        g_pOemGlobal->pfnSendIpi           = OEMSendIPI;
        g_pOemGlobal->pfnMpCpuPowerFunc    = OEMMpCpuPowerFunc;
        g_pOemGlobal->pfnIdleEx            = OEMIdleEx;
        g_pOemGlobal->pfnUpdateReschedTime = OALTimerUpdateRescheduleTimeEx;

        OALMSG(1, (L"SMP support enabled\r\n") );
    }
    else
    {
        OALMSG(1, (L"SMP support disabled\r\n") );
    }
    
    OALMSG(OAL_FUNC, (L"-OALMpInit\r\n") );
}
