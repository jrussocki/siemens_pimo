//-----------------------------------------------------------------------------
//
//  Copyright (C) 2007-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//-----------------------------------------------------------------------------
//
//  Module: timer.c
//
//  This module provides the BSP-specific interfaces required to support
//  the PQOAL timer code.
//
//-----------------------------------------------------------------------------

#include <bsp.h>
#include "dvfs.h"

#include "common_pm.h"
#include "intrinsic.h"

//-----------------------------------------------------------------------------
// Types
typedef VOID (*FUNC_ENTER_WFI) (VOID);


//------------------------------------------------------------------------------
// External Functions
extern void OALCPUEnterWFI(void);
extern VOID OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX index, 
    DDK_CLOCK_GATE_MODE mode);
FUNC_ENTER_WFI g_pFuncIramEnterWfi;

//------------------------------------------------------------------------------
// External Variables
extern PCSP_GIC_DIST_REGS g_pGIC_DIST;
extern UINT32 g_SREV;
extern PCSP_CCM_REGS g_pCCM;
extern PCSP_EPIT_REG g_pEPIT;
extern OAL_TIMER_STATE g_oalTimer;
extern PDDK_CLK_CONFIG g_pDdkClkConfig;
extern PCSP_IOMUX_REGS g_pIOMUX;


//------------------------------------------------------------------------------
// Defines
#define WDOG_GET_WT(msec)   ((UINT16)((msec/WDOG_TIMEOUT_RES) & WDOG_WCR_WT_MASK))
#define WDOG_TIMEOUT_RES    500    // (4096*4*1000)/32768

#define WD_REFRESH_PERIOD               3000    // tell the OS to refresh watchdog every 3 second.
#define WD_RESET_PERIOD                 4500    // tell the wdog to reset the system after 4.5 seconds.

//-----------------------------------------------------------------------------
// Global Variables


//------------------------------------------------------------------------------
// Local Variables
static PCSP_WDOG_REGS g_pWDOG;


//-----------------------------------------------------------------------------
//
//  Function: OALTimerNotifyReschedule
//
//  This function is called when the new thread is ready to run.  The BSP
//  uses this kernel function to perform CPU load tracking.
//
//  Parameters:
//      dwThrdId 
//          [in] Identifier of the next running thread.
//
//      dwPrio 
//          [in] Priority of the next running thread.
//
//      dwQuantum 
//          [in] Quantum of the next running thread.
//
//      dwFlags 
//          [in] Reserved for future use.
//
//  Returns:
//      None.
//
//-----------------------------------------------------------------------------
VOID OALTimerNotifyReschedule(DWORD dwThrdId, DWORD dwPrio, DWORD dwQuantum, 
                              DWORD dwFlags)
{
    DWORD idleTime, idlePercent, timeWindow;
    static DWORD lastMSec = 0, lastIdleTime = 0;
    static BOOL bWindowRestart = TRUE;
    static UINT32 upMSec = 0, dnMSec = 0;
    static DDK_DVFC_SETPOINT lastSetpointCpu;
    static DDK_DVFC_SETPOINT lastSetpointPer;
    DDK_DVFC_SETPOINT setpointCpu;
    DDK_DVFC_SETPOINT setpointPer;

    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(dwThrdId);
    UNREFERENCED_PARAMETER(dwPrio);
    UNREFERENCED_PARAMETER(dwQuantum);
    UNREFERENCED_PARAMETER(dwFlags);

    // Set restart flag if DVFC is inactive
    if (!g_pDdkClkConfig->bDvfcActive)
    {
        bWindowRestart = TRUE;
        return;
    }

    setpointCpu = g_pDdkClkConfig->setpointCur[DDK_DVFC_DOMAIN_CPU];
    if (setpointCpu != lastSetpointCpu)
    {   
        // If setpoint has changed since the last reschedule, restart
        // the window and base load setpoint on current setpoint
        lastSetpointCpu = setpointCpu;
        g_pDdkClkConfig->setpointLoad[DDK_DVFC_DOMAIN_CPU] = setpointCpu;
    }        

    setpointPer = g_pDdkClkConfig->setpointCur[DDK_DVFC_DOMAIN_PERIPH];
    if (setpointPer != lastSetpointPer)
    {
        // If setpoint has changed since the last reschedule, restart
        // the window and base load setpoint on current setpoint
        lastSetpointPer = setpointPer;
        g_pDdkClkConfig->setpointLoad[DDK_DVFC_DOMAIN_PERIPH] = setpointPer;
    }

    if (bWindowRestart)
    {
        // Restart load tracking window parameters
        bWindowRestart = FALSE;
        lastMSec = CurMSec;
        upMSec = 0;
        dnMSec = 0;
        lastIdleTime = (DWORD)(g_pNKGlobal->liIdle.QuadPart/g_pNKGlobal->dwIdleConv);
        return;
    }

    // Calculate load tracking duration
    timeWindow = CurMSec - lastMSec;
        
    // Check if tracking duration exceeds minimum tracking window
    if (timeWindow > BSP_DVFS_LOADTRACK_MSEC)
    {
        // Calculate idle percentage of the tracking window
        idleTime = (DWORD)(g_pNKGlobal->liIdle.QuadPart/g_pNKGlobal->dwIdleConv);
        idlePercent = (idleTime - lastIdleTime) * 100 / timeWindow;

        // Restart tracking window
        lastMSec = CurMSec;
        lastIdleTime = idleTime;

        // Check for setpoint increase
        if (idlePercent < BSP_DVFS_LOADTRACK_UP_PCT)
        {
            dnMSec = 0;
            upMSec += timeWindow;

            if (upMSec >= BSP_DVFS_LOADTRACK_UP_MSEC)
            {
                upMSec = 0;
                
                // Increase peripheral setpoint to highest level possible, then begin
                // increasing CPU setpoint
                
                // Avoid setpoint increase request if current setpoint is already 
                // highest
                if (setpointPer != g_pDdkClkConfig->setpointMax[DDK_DVFC_DOMAIN_PERIPH])
                {
                    g_pDdkClkConfig->setpointLoad[DDK_DVFC_DOMAIN_PERIPH] = setpointPer - 1;
                    // OALMSGS(TRUE, (_T("DOM1 = UP:  %d, %d\r\n"), idlePercent, timeWindow));
                    OUTREG32(&g_pGIC_DIST->ICDISPR[BSP_DVFS_IRQ >> 5], 1U << (BSP_DVFS_IRQ % 32));
                }
                else if (setpointCpu != g_pDdkClkConfig->setpointMax[DDK_DVFC_DOMAIN_CPU])
                {
                    g_pDdkClkConfig->setpointLoad[DDK_DVFC_DOMAIN_CPU] = setpointCpu - 1;
                    // OALMSGS(TRUE, (_T("DOM0 = UP:  %d, %d\r\n"), idlePercent, timeWindow));
                    OUTREG32(&g_pGIC_DIST->ICDISPR[BSP_DVFS_IRQ >> 5], 1U << (BSP_DVFS_IRQ % 32));
                }
            }
        }
        // Check for setpoint decrease
        else if (idlePercent > BSP_DVFS_LOADTRACK_DN_PCT)
        {
            upMSec = 0;
            dnMSec += timeWindow;
            
            if (dnMSec >= BSP_DVFS_LOADTRACK_DN_MSEC)
            {
                dnMSec = 0;
            
                // Decrease CPU setpoint to lowest level possible, then begin 
                // reducing peripheral setpoint.  
                
                // Avoid setpoint decrease request if current setpoint is already 
                // lowest or if setpoint is being held by CSPDDK/Power Manager.
                if ((setpointCpu != g_pDdkClkConfig->setpointMin[DDK_DVFC_DOMAIN_CPU]) &&
                    (setpointCpu == g_pDdkClkConfig->setpointLoad[DDK_DVFC_DOMAIN_CPU]))
                {
                    g_pDdkClkConfig->setpointLoad[DDK_DVFC_DOMAIN_CPU] = setpointCpu + 1;
                    // OALMSGS(TRUE, (_T("DOM0 = DOWN:  %d, %d\r\n"), idlePercent, timeWindow));
                    OUTREG32(&g_pGIC_DIST->ICDISPR[BSP_DVFS_IRQ >> 5], 1U << (BSP_DVFS_IRQ % 32));
                }
                else if ((setpointPer != g_pDdkClkConfig->setpointMin[DDK_DVFC_DOMAIN_PERIPH]) &&
                         (setpointPer == g_pDdkClkConfig->setpointLoad[DDK_DVFC_DOMAIN_PERIPH]))
                {
                    g_pDdkClkConfig->setpointLoad[DDK_DVFC_DOMAIN_PERIPH] = setpointPer + 1;
                    // OALMSGS(TRUE, (_T("DOM1 = DOWN:  %d, %d\r\n"), idlePercent, timeWindow));
                    OUTREG32(&g_pGIC_DIST->ICDISPR[BSP_DVFS_IRQ >> 5], 1U << (BSP_DVFS_IRQ % 32));
                }
            }
        }
        else
        {
            upMSec = 0;
            dnMSec = 0;
        }
    }
} 

//-----------------------------------------------------------------------------
//
//  Function: OALTimerGetClkSrc
//
//  This function returns the clock source setting used to program the EPIT_CR
//  CLKSRC bits.
//
//  Parameters:
//      None.
//
//  Returns:
//      EPIT clock source selection.
//
//-----------------------------------------------------------------------------
UINT32 OALTimerGetClkSrc(void)
{
#if 1   // MX6_BRING_UP
    // CCM related, to rewrite
#else
    // Enable clocks for OS tick timer
    INSREG32(&g_pCCM->CCGR[2], CCM_CGR2_EPIT1_IPG_MASK, 
        CCM_CGR_VAL(DDK_CLOCK_GATE_INDEX_EPIT1_IPG, DDK_CLOCK_GATE_MODE_ENABLED_RUN));

#ifndef BSP_OAL_TIMER32K
    // Unless EPIT is clocked from CKIL, HIGHFREQ clock node is required
    INSREG32(&g_pCCM->CCGR[2], CCM_CGR2_EPIT1_HIGHFREQ_MASK, 
        CCM_CGR_VAL(DDK_CLOCK_GATE_INDEX_EPIT1_HIGHFREQ, DDK_CLOCK_GATE_MODE_ENABLED_ALL));
#endif 

#endif
    // Map reschedule notification kernel function to perform CPU load tracking
    g_pOemGlobal->pfnNotifyReschedule = OALTimerNotifyReschedule;

    return BSP_SYSTIMER_CLKSRC;
}


//-----------------------------------------------------------------------------
//
//  Function: OALTimerGetClkPrescalar
//
//  This function returns the clock prescalar used to program the EPIT_CR
//  PRESCALER bits.
//
//  Parameters:
//      None.
//
//  Returns:
//      EPIT prescalar.
//
//-----------------------------------------------------------------------------
UINT32 OALTimerGetClkPrescalar(void)
{
    return BSP_SYSTIMER_PRESCALAR;
}


//-----------------------------------------------------------------------------
//
//  Function: OALTimerGetClkFreq
//
//  This function returns the frequency of the EPIT input clock.
//
//  Parameters:
//      None.
//
//  Returns:
//      EPIT input clock.
//
//-----------------------------------------------------------------------------
UINT32 OALTimerGetClkFreq(void)
{
#if (BSP_SYSTIMER_CLKSRC == EPIT_CR_CLKSRC_CKIL)
    return BSP_CLK_CKIL_FREQ;
#else
    BSP_ARGS *pBspArgs = (BSP_ARGS *)IMAGE_SHARE_ARGS_UA_START;

    return pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_PERCLK];
#endif
}

//-----------------------------------------------------------------------------
//
//  Function: OALCPUIdle
//
//  This function puts the CPU or SOC in idle state. The CPU or SOC 
//  should exit the idle state when an interrupt occurs. This function is 
//  called with interrupts are disabled. When this function returns, interrupts 
//  must be disabled also.
//
//  Parameters:
//      OEM_IDLE_TYPE type - indicates if we are coming from SMP idle or 
//      single core idle. There are differences in how we handle idel on
//      multicore vs single core.
//
//  Returns:
//      None.
//
//-----------------------------------------------------------------------------
VOID OALCPUIdle(OEM_IDLE_TYPE type)
{

    //
    // call the PmPowerIdle function that will idle the processor.
    //
    PmPowerIdleEntry(type);

}

//NOTE: old version...

//-----------------------------------------------------------------------------
//
// Function: WdogService
//
//  This function services the watchdog timer.
//
// Parameters:
//
// Returns:
//      None.
//
//-----------------------------------------------------------------------------
BOOL WdogService(void)
{
    if (g_pWDOG == NULL) {
        OALMSG(OAL_ERROR, (L"WdogService:  Watchdog not initialized!\r\n"));
        return FALSE;
    }
    // 1. write 0x5555
    OUTREG16(&g_pWDOG->WSR, WDOG_WSR_WSR_RELOAD1);

    // 2. write 0xAAAA
    OUTREG16(&g_pWDOG->WSR, WDOG_WSR_WSR_RELOAD2);

    return TRUE;
}


//-----------------------------------------------------------------------------
//
// Function: WdogInit
//
//  This function asserts a system reset after the desired timeout.
//
// Parameters:
//      timeoutMSec
//          [in] watchdog timeout in msec.
//
// Returns:
//      This function returns the actual timeout period(msec).
//
//-----------------------------------------------------------------------------
UINT32 WdogInit(UINT32 TimeoutMSec)
{
    UINT16 wcr;

    // On EVK v2.5, if WDI is asserted then PMIC will go through a cold boot, resulting in powering off the board,
    // so leave this muxing commented for now. Default mux will be GPIO as input, and external pullup on this pad will
    // ensure WDI is not asserted
#if 0
    // Board v2.5: Muxing
    if (g_dwBoardID == 0)
    {
        // WDOG_B is connected to WDI of PMIC via GPIO1_4 ATL2 mode
        OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_GPIO1_4, DDK_IOMUX_PIN_MUXMODE_ALT2, DDK_IOMUX_PIN_SION_REGULAR);        
        OAL_IOMUX_SET_PAD(g_pIOMUX, DDK_IOMUX_PAD_GPIO1_4, DDK_IOMUX_PAD_SLEW_FAST, DDK_IOMUX_PAD_DRIVE_HIGH,
                                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_UP_100K, DDK_IOMUX_PAD_HYSTERESIS_ENABLE, 
                                         DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);

    }
#endif
    
    // Get uncached virtual addresses for Watchdog
    g_pWDOG = (PCSP_WDOG_REGS) OALPAtoUA(CSP_BASE_REG_PA_WDOG1);
    if (g_pWDOG == NULL) {
        OALMSG(OAL_ERROR, (L"WdogInit:  Watchdog null pointer!\r\n"));
        return 0;
    }

    // Watchdog is configured as follows:
    //
    //  WDW = continue timer operation in low-power wait mode
    //  WOE = tri-state WDOG output pin
    //  WDA = no software assertion of WDOG output pin
    //  SRS = no software reset of WDOG
    //  WRE = generate reset signal upon watchdog timeout
    //  WDE = disable watchdog (will be enabled after configuration)
    //  WDBG = suspend timer operation in debug mode
    //  WDZST = suspend timer operation in low-power stop mode
    wcr = CSP_BITFVAL(WDOG_WCR_WOE, WDOG_WCR_WOE_TRISTATE) |
            CSP_BITFVAL(WDOG_WCR_WDA, WDOG_WCR_WDA_NOEFFECT) |
            CSP_BITFVAL(WDOG_WCR_SRS, WDOG_WCR_SRS_NOEFFECT) |
            CSP_BITFVAL(WDOG_WCR_WRE, WDOG_WCR_WRE_SIG_RESET) |
            CSP_BITFVAL(WDOG_WCR_WDE, WDOG_WCR_WDE_DISABLE) |
            CSP_BITFVAL(WDOG_WCR_WDBG, WDOG_WCR_WDBG_SUSPEND) |
            CSP_BITFVAL(WDOG_WCR_WDZST, WDOG_WCR_WDZST_SUSPEND) |
            CSP_BITFVAL(WDOG_WCR_WT, WDOG_GET_WT(TimeoutMSec));    

    // Configure and then enable the watchdog
    OUTREG16(&g_pWDOG->WCR, wcr);
    wcr |= CSP_BITFVAL(WDOG_WCR_WDE, WDOG_WCR_WDE_ENABLE);
    OUTREG16(&g_pWDOG->WCR,  wcr);
    
    // Service the watchdog
    WdogService();

    return (WDOG_GET_WT(TimeoutMSec)*WDOG_TIMEOUT_RES);
}


//
// function to refresh watchdog timer
//
void RefreshWatchdogTimer (void)
{
    static BOOL bFirstTime = TRUE;

    OALMSG(OAL_FUNC, (L"+RefreshWatchdogTimer\r\n"));

    if (bFirstTime)
    {
        OALMSG(OAL_FUNC, (L"+RefreshWatchdogTimer: First call, init the Wdog to timeout reset in 4.5 secs\r\n"));
        WdogInit(WD_RESET_PERIOD);
        bFirstTime = FALSE;
    }
    else
    {
        OALMSG(OAL_FUNC, (L"+RefreshWatchdogTimer: Subsequence calls, refresh the Wdog timeout to 4.5 secs again\r\n"));
        WdogService();
    }

    OALMSG(OAL_FUNC, (L"-RefreshWatchdogTimer\r\n"));
}

//------------------------------------------------------------------------------
//
//  Function:  InitWatchDogTimer
//
//  This is the function to enable hardware watchdog timer support by kernel.
//
void InitWatchDogTimer (void)
{
    OALMSG(OAL_FUNC, (L"+InitWatchDogTimer\r\n"));

    pfnOEMRefreshWatchDog = RefreshWatchdogTimer;
    dwOEMWatchDogPeriod   = WD_REFRESH_PERIOD;

    OALMSG(OAL_FUNC, (L"-InitWatchDogTimer\r\n"));
}


//------------------------------------------------------------------------------
