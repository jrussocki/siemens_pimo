;
; Copyright (c) Microsoft Corporation.  All rights reserved.
;
;
; Use of this source code is subject to the terms of the Microsoft end-user
; license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
; If you did not accept the terms of the EULA, you are not authorized to use
; this source code. For a copy of the EULA, please see the LICENSE.RTF on your
; install media.
;
;------------------------------------------------------------------------------
;
;  Copyright (C) 2008-2011, Freescale Semiconductor, Inc. All Rights Reserved.
;  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
;  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
;
;------------------------------------------------------------------------------
;
;  File:  vfp.s
;
;  Implement low level VFP operations.
;
;------------------------------------------------------------------------------

VFP_NEON_ENABLE            EQU     (1 << 30)
VFP_DN_ENABLE              EQU     (1 << 24)
VFP_FZ_ENABLE              EQU     (1 << 25)
VFP_RUN_FAST               EQU     (VFP_DN_ENABLE | VFP_FZ_ENABLE)

    INCLUDE kxarm.h
    INCLUDE armmacros.s

    EXPORT VFP_Enable
    EXPORT NEON_SaveRegs
    EXPORT NEON_RestoreRegs

    TEXTAREA

;------------------------------------------------------------------------------
;
;  Function: VFP_Enable
;
;  This function Enable and init VFP.  
;
;  Parameters:
;      None.
;
;  Returns:
;      None.
;
;------------------------------------------------------------------------------
    ALIGN 4
    LEAF_ENTRY VFP_Enable

    ; Enable VFP
    VMRS     R0, FPEXC
    orr      r0, r0, #VFP_NEON_ENABLE
    VMSR     FPEXC, R0

    ; Set VFP to runfast mode
    VMRS     R0, FPSCR
    orr      r0, r0, #VFP_RUN_FAST
    VMSR     FPSCR, R0

    RETURN

    LEAF_END VFP_Enable

;------------------------------------------------------------------------------
;
;  Function: VFP_Disable
;
;  This function disables the VFP.  
;
;  Parameters:
;      None.
;
;  Returns:
;      None.
;
;------------------------------------------------------------------------------
    ALIGN 4
    LEAF_ENTRY VFP_Disable

    ; Disable VFP
    VMRS     R0, FPEXC
    bic      r0, r0, #VFP_NEON_ENABLE
    VMSR     FPEXC, R0

    ; Disable access to NEON unit
    mrc     p15, 0, r0, c1, c0, 2   ; Read Coprocessor Access Control Register
    bic     r0, r0, #0xF00000       ; Disable access to CP10 and CP11
    mcr     p15, 0, r0, c1, c0, 2   ; Write Coprocessor Access Control Register
    RETURN

    LEAF_END VFP_Disable

;------------------------------------------------------------------------------
;
;  Function: NEON_SaveRegs
;
;  This function saves extra registers of NEON.  
;
;  Parameters:
;      pExtraRegs (r0) - Memory address to save extra registers.
;
;  Returns:
;      None.
;
;------------------------------------------------------------------------------
    ALIGN 4
    LEAF_ENTRY NEON_SaveRegs

    ; save fpcsr
    VMRS     R1, FPSCR
    str     r1, [r0], #4

    ; save all the general purpose registers
    VSTMIA.64 R0!, {D0-D15}
    VSTMIA   R0, {D16-D31}

    RETURN

    LEAF_END NEON_SaveRegs

;------------------------------------------------------------------------------
;
;  Function: NEON_RestoreRegs
;
;  This function restores extra registers of NEON.  
;
;  Parameters:
;      pExtraRegs (r0) - Memory address to restore extra registers.
;
;  Returns:
;      None.
;
;------------------------------------------------------------------------------
    ALIGN 4
    LEAF_ENTRY NEON_RestoreRegs

    ; restore fpscr
    ldr     r1, [r0], #4
    VMSR     FPSCR, R1
    
    ; restore all the general purpose registers
    VLDMIA.64 R0!, {D0-D15}
    VLDMIA   R0, {D16-D31}

    RETURN

    LEAF_END NEON_RestoreRegs

    END

