//-----------------------------------------------------------------------------
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//-----------------------------------------------------------------------------
//
//  Copyright (C) 2007-2010, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//-----------------------------------------------------------------------------
//
//  File:  init.c
//
//  Board initialization code.
//
//-----------------------------------------------------------------------------

#include <bsp.h>
#include <kitl_cfg.h>
#include <bldver.h>
#include <pehdr.h>
#pragma warning(push)
#pragma warning(disable: 4201)
#include <Romldr.h>
#pragma warning(pop)

#include <vfpsupport.h>

//-----------------------------------------------------------------------------
// Types

//-----------------------------------------------------------------------------
// External Functions
extern VOID InitDebugSerial(void);
extern VOID OALPowerInit(void);
extern UINT32 OALTimerGetClkFreq(void);
extern UINT32 OALTimerGetClkPrescalar(void);
extern void VFP_Init(void);
extern PCRamTable OEMGetRamTable(void);
extern VOID OALMpInit(VOID);

// Cache Management
extern VOID OALCachePL310GlobalsInit(UINT32 baseAddr);
extern UINT32 BSP_GetPlatInfoAddress();
extern VOID OALDisplayPlatInfo(BOOL bShow,POAL_PLAT_INFO pInfo);
extern VOID OALCacheInfoDisplay(BOOL bShow);

extern void OALPTEUpdateBarrier(LPVOID pte, DWORD cbSize);
extern ULONG OEMInterruptHandler(ULONG ra);
extern UINT32 Imx6ChipType();
extern void OEMInterruptHandlerFIQ();

#define OAL_CACHE_DEBUG		1

//-----------------------------------------------------------------------------
// External Variables
extern PCSP_CCM_REGS   g_pCCM;
extern PCSP_IOMUX_REGS g_pIOMUX;
POAL_PLAT_INFO  g_pPlatInfo;

// Define a static data structure that we can use to pass data between
// the OAL and KITL. We will assign the address of this data structure
// to the "pKitlInfo" data member in OEMGlobals.
//
// Note that we make a "static" declaration here so that all external
// accesses to this data structure will be forced to use the pKitlInfo
// pointer for consistency.
//
static _OALKITLSharedDataStruct g_OALKITLSharedData = { 0 };

//-----------------------------------------------------------------------------
// Global Variables
PCSP_SDMA_REGS g_pSDMA;
UINT32 g_SREV;
DWORD g_dwBoardID = 0;
PCSP_GPIO_REGS g_pGPIO1;
UINT8 g_UPID[8];
UINT8 *g_pIIM;

// WinCE 6.0: These global variables are now required.

//
//  Global:  dwOEMDrWatsonSize
//
//  Global variable which specify DrWatson buffer size. It can be fixed
//  in config.bib via FIXUPVAR.
//
#define DR_WATSON_SIZE_NOT_FIXEDUP (-1)
DWORD dwOEMDrWatsonSize = (DWORD) DR_WATSON_SIZE_NOT_FIXEDUP;

//-----------------------------------------------------------------------------
//
// Function: OEMHaltSystem
//
// Function turns on leds to indicate error condition before system halt.
//
// Parameters:
//
// Returns:
//
//
//-----------------------------------------------------------------------------
void OEMHaltSystem(void)
{
    // TODO:  Implement with EVK debug LEDs
}


//------------------------------------------------------------------------------
//
//  Function:  OEMInit
//
//  This is Windows CE OAL initialization function. It is called from kernel
//  after basic initialization is made.
//
void OEMInit()
{
    BSP_ARGS *pBspArgs = (BSP_ARGS *)IMAGE_SHARE_ARGS_UA_START;
    UINT32 countsPerMsec;
    
    OALMSG(OAL_FUNC, (L"+OEMInit\r\n"));

	//Stored CPU feature list
	g_pPlatInfo = (POAL_PLAT_INFO)(OALPAtoUA(BSP_GetPlatInfoAddress()));
	g_pPlatInfo->ChipType = Imx6ChipType();

    // Set memory size for DrWatson kernel support.
    dwNKDrWatsonSize = 0;
    if (dwOEMDrWatsonSize != DR_WATSON_SIZE_NOT_FIXEDUP)
    {
        dwNKDrWatsonSize = dwOEMDrWatsonSize;
    }

    // Define optional kernel supported features.
    pOEMIsProcessorFeaturePresent = OALIsProcessorFeaturePresent;
    
    // Initialize system halt handler.
    g_pOemGlobal->pfnHaltSystem = OEMHaltSystem;

    // Give kernel access to the profiling functions.
    g_pOemGlobal->pfnProfileTimerEnable  = OEMProfileTimerEnable;
    g_pOemGlobal->pfnProfileTimerDisable = OEMProfileTimerDisable;

    //----------------------------------------------------------------------
    // set values of globals used in IOCTL_HAL_GET_DEVICE_INFO handler
    //----------------------------------------------------------------------
    g_oalIoCtlPlatformName          = IOCTL_PLATFORM_NAME;
    g_oalIoCtlPlatformManufacturer  = IOCTL_PLATFORM_MANUFACTURER;

    CEProcessorType = PROCESSOR_ARM_CORTEX;
    CEInstructionSet = PROCESSOR_ARM_V7_INSTRUCTION;

    // Initilize L1 cache globals
    OALCacheGlobalsInit();

	// Inirialize external L2 cache globals
    OALCachePL310GlobalsInit(CSP_BASE_REG_PA_L2CC);

    g_pOemGlobal->pfnCacheRangeFlush = OEMCacheRangeFlush;

	//Show Cache and Platform info
	OALCacheInfoDisplay(OAL_CACHE_DEBUG);
	OALDisplayPlatInfo(OAL_CACHE_DEBUG, g_pPlatInfo);

    // Initialize the shared OAL+KITL data structure. This allows KITL.DLL
    // to access these pointers to the hardware control registers.
    g_OALKITLSharedData.g_pCCM   = g_pCCM;
    g_OALKITLSharedData.g_pIOMUX = g_pIOMUX;

    // Provide a pointer that KITL can use to access the shared data structure.
    g_pOemGlobal->pKitlInfo = (LPVOID)&g_OALKITLSharedData;

    // Get snapshot of silicon rev.  Use ROM ID to uniquely identify
    // the silicon version:
    //
    //      ROM ID      Silicon Rev
    //      -----------------------
    //      0x10        TO1.0
    //
    g_SREV = INREG32(OALPAtoUA(CSP_BASE_MEM_PA_ROM+0x48));
    OALMSG(OAL_INFO, (L"OEMInit:  silicon rev = 0x%x\r\n", g_SREV));

    // Map access to IIM
    g_pIIM = (UINT8 *) OALPAtoUA(CSP_BASE_REG_PA_IIM);
    if (g_pIIM == NULL)
    {
        // Error message is all we can do since OEMInit has no return
        OALMSG(OAL_ERROR, (L"OEMInit:  IIM null pointer!\r\n"));
        g_dwBoardID = 0xffff; 
    }
    else
    {
        g_dwBoardID  = (UINT32)INREG8(g_pIIM + 0x0878);               //GP[15:8]
        g_dwBoardID |= ((UINT32)INREG8(g_pIIM + 0x087C))<<8;          //GP[7:0]
        //g_dwBoardID = 0xffff; // temperarily use.

	    // TODO:  Port to MX6Q IIM fuse map
	    // Read 64-bit unique part ID from Fuse Bank 0
	    g_UPID[0] = (UINT8) (INREG32(&g_pIIM[0x0800 + 0x0020]) & 0xFF);
	    g_UPID[1] = (UINT8) (INREG32(&g_pIIM[0x0800 + 0x0024]) & 0xFF);
	    g_UPID[2] = (UINT8) (INREG32(&g_pIIM[0x0800 + 0x0028]) & 0xFF);
	    g_UPID[3] = (UINT8) (INREG32(&g_pIIM[0x0800 + 0x002C]) & 0xFF);
	    g_UPID[4] = (UINT8) (INREG32(&g_pIIM[0x0800 + 0x0030]) & 0xFF);
	    g_UPID[5] = (UINT8) (INREG32(&g_pIIM[0x0800 + 0x0034]) & 0xFF);
	    g_UPID[6] = (UINT8) (INREG32(&g_pIIM[0x0800 + 0x0038]) & 0xFF);
	    g_UPID[7] = (UINT8) (INREG32(&g_pIIM[0x0800 + 0x003C]) & 0xFF);
    }

    OALMSG(1, (L"BoardID = 0x%x.\r\n", g_dwBoardID));

    // Create GUID from MAC
    pBspArgs->guid.Data1 = pBspArgs->mac[0];
    pBspArgs->guid.Data2 = pBspArgs->mac[1];
    pBspArgs->guid.Data3 = pBspArgs->mac[2];
    pBspArgs->guid.Data4[0] = pBspArgs->mac[3];
    pBspArgs->guid.Data4[1] = pBspArgs->mac[4];
    pBspArgs->guid.Data4[2] = pBspArgs->mac[5];
    pBspArgs->guid.Data4[3] = 0;

    OALPowerInit();

    // Set clock speed for OALIoCtlProcessorInfo() query
    g_oalIoCtlClockSpeed = pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_ARM];
    
    NKForceCleanBoot();

    // Initialize interrupts
    if (!OALIntrInit())
	{
        OALMSG(OAL_ERROR, (
            L"ERROR: OEMInit: failed to initialize interrupts\r\n"
        ));
        goto cleanUp;
    }

    // Initialize the system clock.
    countsPerMsec = OALTimerGetClkFreq() / ((OALTimerGetClkPrescalar() + 1) *
                                            1000);

    if (!OALTimerInit(RESCHED_PERIOD, countsPerMsec, countsPerMsec / 100 + 2))
    {
        OALMSG(OAL_ERROR, (
            L"ERROR: OEMInit: Failed to initialize system clock\r\n"
        ));
        goto cleanUp;
    }

    // Initialize SDMA with address of shared region
    g_pSDMA = OALPAtoUA(CSP_BASE_REG_PA_SDMA);
    if (g_pSDMA == NULL)
    {
        // Error message is all we can do since OEMInit has no return
        OALMSG(OAL_ERROR, (L"ERROR: OEMInit:  SDMA null pointer!\r\n"));
        goto cleanUp;
    }
    else
    {
        // Set the channel 0 pointer to the shared region physical address
        OUTREG32(&g_pSDMA->MC0PTR, BSP_SDMA_MC0PTR);

        // Configure SDMA/AHB clock ratio
        if (pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_AHB_132M] == 
            pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_IPG])
        {
            INSREG32BF(&g_pSDMA->CONFIG, SDMA_CONFIG_ACR, SDMA_CONFIG_ACR_AHB1X);
        }
        else
        {
            INSREG32BF(&g_pSDMA->CONFIG, SDMA_CONFIG_ACR, SDMA_CONFIG_ACR_AHB2X);
        }

        // Configure SDMA for static context switch
        INSREG32BF(&g_pSDMA->CONFIG, SDMA_CONFIG_CSM, SDMA_CONFIG_CSM_STATIC);
    }

    // Initialize the KITL connection if required
    // TODO:  KITL init hangs, comment out line below to allow NK boot
    KITLIoctl(IOCTL_KITL_STARTUP, NULL, 0, NULL, 0, NULL);

    //
    // A9 MP supports page table walk in L1.  dwTTBRCacheBits are
    // applied to TTBR register.  TTBR format for A9 MP is
    //  TTBR[0] = IRGN[1] = 0 (see below)
    //  TTBR[1] = 1 = shared
    //  TTBR[3] = RGN[0] = 0 (see below)
    //  TTBR[4] = RGN[1] = 1 (see below)
    //  TTBR[5] = 1 = inner shareable
    //  TTBR[6] = IRGN[0] = 1 (see below)
    //
    //  IRGN[1:0] = 10 => normal inner C_WT
    //  RGN[1:0] = 10 => normal outer C_WT

    g_pOemGlobal->dwTTBRCacheBits      = (1 << 5) | // NOS      - Inner (L1) Shareable
                                         (1 << 1) | // S        - Shared
                                         (1 << 3) | // RGN[4:3] - Outer (L2) Write-Back/Write-Allocate (Normal memory)
                                         (0 << 0) | // IRGN[1]  - Inner (L1) Write-Back/Write-Allocate (Normal memory)
                                         (1 << 6) | // IRGN[0]
                                         (0);

    g_pOemGlobal->dwPageTableCacheBits = (1 << 10) | // S        - Shared
                                         (1 << 6)  | // TEX[2:0] = 001, C=1, B=1   Inner (L1) & Outer (L2) Write-Back/Write-Allocate (Normal memory)
                                         (3 << 2)  | // C, B
                                         (1 << 1)  | // S        - Shared
                                         (1 << 0)  | // XN       - Execute Never
                                         (0);

    g_pOemGlobal->dwARMCacheMode = OEMARMCacheMode();

    g_pOemGlobal->pfnPTEUpdateBarrier = OALPTEUpdateBarrier;

    //----------------------------------------------------------------------
    // Initialize SMP support
    //----------------------------------------------------------------------
    OALMpInit();

    //----------------------------------------------------------------------
    // Initialize Vector Floating Point co-processor
    //----------------------------------------------------------------------
    VfpOemInit(g_pOemGlobal, VFP_AUTO_DETECT_FPSID);


    // enable RAM > 512M     
    g_pOemGlobal->pfnGetOEMRamTable = OEMGetRamTable;

    // other stuff

    g_pOemGlobal->pfnInterruptHandler = OEMInterruptHandler;
    g_pOemGlobal->pfnFIQHandler = OEMInterruptHandlerFIQ;

    // Init PCI
    if(!OALPCIInit() )
    {
        OALMSGS( OAL_ERROR, (_T("OEMInit: OALPCIInit Error!\r\n")));
    }

cleanUp:
    OALMSG(TRUE, (L"-OEMInit\r\n") );
}

//------------------------------------------------------------------------------
