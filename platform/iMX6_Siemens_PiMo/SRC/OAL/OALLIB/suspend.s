;-----------------------------------------------------------------------------
;
;   Copyright (C) 2007-2011, Freescale Semiconductor, Inc. All Rights Reserved.
;   THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
;   AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
;
;-----------------------------------------------------------------------------
;
;  Module: suspend.s
;
;  This module implements the OAL assembly-level support for placing the
;  system in suspend mode.
;
;-----------------------------------------------------------------------------
    INCLUDE kxarm.h
    INCLUDE armmacros.s
    INCLUDE mx6_base_regs.inc

	EXTERN 	g_L2Base
;
; L2CC constants
;
L2CC_CR_OFFSET              EQU     0x100
L2CC_TAG_LAT_OFFSET         EQU     0x108
L2CC_DATA_LAT_OFFSET        EQU     0x10C
L2CC_INVWAY_OFFSET          EQU     0x77C
L2CC_PREFETCH_OFFSET        EQU     0xF60
CACHE_SYNC_L2_DISCARD		EQU		0x040

BSP_STALL_DELAY             EQU     792

    TEXTAREA

	IMPORT	OALClnInvDCache
    IMPORT  OALInvICacheIS
    IMPORT  OEMCacheRangeFlush
   
	MACRO   
	INVALIDATE_L1_DCACHE
;-----------------------------------------------------------------------------
; Invalidate l1 dcache, r0-r4, r6, r7 used
;-----------------------------------------------------------------------------

	mov 	r0, #0
	mcr 	p15, 2, r0, c0, c0, 0
	mrc 	p15, 1, r0, c0, c0, 0

	ldr 	r1, =0x7fff
	and 	r2, r1, r0, lsr #13

	ldr 	r1, =0x3ff

	and 	r3, r1, r0, lsr #3		; NumWays - 1
	add 	r2, r2, #1				; NumSets

	and 	r0, r0, #0x7
	add 	r0, r0, #4				; SetShift

	clz 	r1, r3					; WayShift
	add 	r4, r3, #1				; NumWays

10	sub 	r2, r2, #1				; NumSets--
	mov 	r3, r4					; Temp = NumWays

20	subs	r3, r3, #1				; Temp--
	mov 	r7, r3, lsl r1
	mov 	r6, r2, lsl r0
	orr 	r7, r7, r6
	mcr 	p15, 0, r7, c7, c6, 2
	bgt 	%b20
	cmp 	r2, #0
	bgt 	%b10
	dsb
	isb

	MEND

;------------------------------------------------------------------------------
;
;  Function: OALCPUEnterSuspend
;
;       This function provides the instruction sequence for requesting the 
;       ARM CPU to enter the WFI (wait-for-interrupt).  This routine will 
;       be called by OEMPowerOff.  
;
;  Parameters:
;      None.
;
;  Returns:
;      None.
;
;  NOTES:
;
;		If we want to use "dormant" mode,(i.e., put DDR into self refresh) 
;		we'll have to do a lot more work here to save the state properly.
;		Such as save ARM context, change the drive strength of MMDC PADs 
;		as "low" to minimize the power leakage in DDR PADs, etc.
;
;------------------------------------------------------------------------------
    ALIGN 4
    NESTED_ENTRY OALCPUEnterSuspend
 
    stmfd   sp!, {r0-r12, lr}

    PROLOG_END OALCPUEnterSuspend

    ;
    ; Flush all data from the L1 data cache before disabling
    ; SCTLR.C bit.
    ;
    stmfd   sp!, {r0-r12, lr}
    ldr		r0,=1
    bl		OALClnInvDCache
    ldmfd 	sp!, {r0-r12, lr}
    
    ;
    ; disable hardware cache synchronization
    ;
    mrc     p15, 0, r0, c1, c0, 1 ; Read ACTRL
    bic     r0, r0, #0x40         ; Turn off ACTRL.SMP
    bic     r0, r0, #1            ; Turn off ACTRL.FW
    bic     r0, r0, #2            ; Turn off ACTRL.FW
    bic     r0, r0, #4            ; Turn off ACTRL.FW
    mcr     p15, 0, r0, c1, c0, 1 ; Write ACTRL
    isb

    ;
    ; Clear the SCTLR.C bit to prevent further data cache
    ; allocation. Clearing SCTLR.C would make all the data accesses
    ; strongly ordered and would not hit the cache.
    ;
    mrc 	p15, 0, r0, c1, c0, 0
    bic 	r0, r0, #(1 << 2)			; Disable the C bit
    mcr 	p15, 0, r0, c1, c0, 0
    isb

    ;
    ; Execute a barrier instruction to ensure that all cache,
    ; TLB and branch predictor maintenance operations issued
    ; by any CPU in the cluster have completed.
    ;
    dsb
    dmb

    ;
    ; Flush ICache
    ;
    stmfd   sp!, {r0-r12, lr}
    bl		OALInvICacheIS
    ldmfd 	sp!, {r0-r12, lr}

 
    ; -----------------------
    ; Need to clean L2 dcache
    ; -----------------------
    stmfd   sp!, {r0-r12, lr}
    ldr		r5, =CACHE_SYNC_L2_DISCARD    
    mov     r2,r5
    mov     r1,#0
	mov     r0,#0
	bl      OEMCacheRangeFlush
    ldmfd 	sp!, {r0-r12, lr}

    ;
    ; Disable L2 cache
    ;
    ldr 	r2, =g_L2Base
    mov 	r4, #0x0
    str 	r4, [r2, #L2CC_CR_OFFSET]

    wfi

    nop
    nop
    nop

    ;
    ; Invalidate L1 I-cache first
    ;
    mov 	r1, #0x0
    mcr 	p15, 0, r1, c7, c5, 0 		; Invalidate I-Cache


    ;
    ; Need to Invalidate L1 DCache, as the power is dropped 
    ;
    INVALIDATE_L1_DCACHE

    ;
    ; Enable L1 dcache first
    ;
    mrc 	p15, 0, r0, c1, c0, 0
    orr 	r0, r0, #(1 << 2)
    mcr 	p15, 0, r0, c1, c0, 0

    ;
    ; Enable L2 cache
    ;
    ldr 	r2, =g_L2Base
    mov 	r4, #0x1
    str 	r4, [r2, #L2CC_CR_OFFSET]

    ;
    ; enable hardware cache synchronization
    ;
    mrc     p15, 0, r0, c1, c0, 1 ; Read ACTRL
    orr     r0, r0, #0x40         ; Turn on ACTRL.SMP
    bic     r0, r0, #1            ; Turn on ACTRL.FW
    orr     r0, r0, #2            ; Turn on ACTRL.FW
    orr     r0, r0, #4            ; Turn on ACTRL.FW
    mcr     p15, 0, r0, c1, c0, 1 ; Write ACTRL


    EPILOG_ENTRY OALCPUEnterSuspend

    ldmfd 	sp!, {r0-r12, lr}

    RETURN

    NESTED_END OALCPUEnterSuspend

;------------------------------------------------------------------------------
;
;  Function: OALCPUCoreSuspend
;
;       This function puts a non-boot CPU core into low-power state.  
;
;  Parameters:
;       None.
;
;  Returns:
;       None.
;
;  NOTES:
;
;------------------------------------------------------------------------------
    ALIGN 4

    NESTED_ENTRY OALCPUCoreSuspend
 
    stmfd   sp!, {r0-r12, lr}

    PROLOG_END OALCPUCoreSuspend

    ;
    ; Flush ICache
    ;
    bl 	OALInvICacheIS

    ;
    ; disable hardware cache synchronization
    ;
    mrc     p15, 0, r0, c1, c0, 1 ; Read ACTRL
    bic     r0, r0, #0x40         ; Turn off ACTRL.SMP
    mcr     p15, 0, r0, c1, c0, 1 ; Write ACTRL
    isb

    ;
    ; Clear the SCTLR.C bit to prevent further data cache
    ; allocation. Clearing SCTLR.C would make all the data accesses
    ; strongly ordered and would not hit the cache.
    ;
    mrc 	p15, 0, r0, c1, c0, 0
    bic 	r0, r0, #(1 << 2)			; Disable the C bit
    mcr 	p15, 0, r0, c1, c0, 0
    isb

    wfi
    nop
    nop
    nop

    ;
    ; Enable L1 dcache first
    ;
    mrc 	p15, 0, r0, c1, c0, 0
    orr 	r0, r0, #(1 << 2)
    mcr 	p15, 0, r0, c1, c0, 0

    ;
    ; enable hardware cache synchronization
    ;
    mrc     p15, 0, r0, c1, c0, 1 ; Read ACTRL
    orr     r0, r0, #0x40         ; Turn on ACTRL.SMP
    mcr     p15, 0, r0, c1, c0, 1 ; Write ACTRL
    
    EPILOG_ENTRY OALCPUCoreSuspend

    ldmfd 	sp!, {r0-r12, lr}

    RETURN

    NESTED_END OALCPUCoreSuspend

;------------------------------------------------------------------------------
;
;  Function: PMOALStall
;
;       This function provides a timed stall at the lowest level;
;       Provides the same functionality as OALStall
;
;  Parameters:
;       4 bytes integer (R0 in assembly or a single parameter as a C function)
;
;  Returns:
;       None
;
;  NOTES:
;      Hard coded based on 1 gigahertz speed core of iMX6Q;
;      Parameter is in milliseconds;
;      Tested with 60,000 the time stalled was 60.22 seconds;
;      The error is less than 0.5%;
;      The parameter range is [0, 524287] (up to about 8.7 minutes)
;
;------------------------------------------------------------------------------
    ALIGN 4
    LEAF_ENTRY PMOALStall

    MOV R0, R0, LSL #13 ; Multiply by 8,192
    ORR R0, R0, #1 ; Set the lowest bit in case the parameter is zero

LoopCountDown
    SUBS R0, R0, #1

    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP ; 10
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP ; 20
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP ; 30
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP ; 40
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP ; 50
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP ; 60
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP ; 70
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP ; 80
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP ; 90
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP ; 96

    BGT LoopCountDown

    RETURN

    LEAF_END PMOALStall

    END
