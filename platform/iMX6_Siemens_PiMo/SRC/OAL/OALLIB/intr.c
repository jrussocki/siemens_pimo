//-----------------------------------------------------------------------------
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//-----------------------------------------------------------------------------
//
//  Copyright (C) 2007-2010, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//-----------------------------------------------------------------------------
//
//  File:  intr.h
//
//  This file contains board-specific interrupt code.
//
//-----------------------------------------------------------------------------
#include <cmnintrin.h>
#include "bsp.h"

extern PCSP_GPIO_REGS g_pGPIO1;
extern PCSP_IOMUX_REGS g_pIOMUX;

// Board-level IRQs
#define IRQ_ETHER                   (IRQ_GPIO2_PIN31)
#define IRQ_ETHER_GPIO_PORT         DDK_GPIO_PORT2
#define IRQ_ETHER_GPIO_PIN          31
#define IRQ_ETHER_GPIO_LINE         IRQ_GPIO2_PIN31


//------------------------------------------------------------------------------
//
//  Function:  BSPIntrInit
//
BOOL BSPIntrInit()
{
    return TRUE;
}


//------------------------------------------------------------------------------

BOOL BSPIntrRequestIrqs(__in DEVICE_LOCATION *pDevLoc, 
                        __out UINT32 *pCount, 
                        __out_ecount(*pCount) UINT32 *pIrqs)
{
    BOOL rc = FALSE;

    OALMSGS(OAL_INTR&&OAL_FUNC, (
        L"+BSPIntrRequestIrq(0x%08x->%d/%d/0x%08x/%d, 0x%08x, 0x%08x)\r\n",
        pDevLoc, pDevLoc->IfcType, pDevLoc->BusNumber, pDevLoc->LogicalLoc,
        pDevLoc->Pin, pCount, pIrqs
    ));

    if (pIrqs == NULL || pCount == NULL || *pCount < 1) goto cleanUp;

    switch (pDevLoc->IfcType)
    {
    case Internal:
        switch ((ULONG)pDevLoc->LogicalLoc)
        {
        case BSP_BASE_REG_PA_LAN911x_IOBASE:
            pIrqs[0] = IRQ_ETHER;
            *pCount = 1;
            rc = TRUE;
            break;
        }
        break;
    }

cleanUp:
    OALMSGS(OAL_INTR&&OAL_FUNC, (L"-BSPIntrRequestIrq(rc = %d)\r\n", rc));
    return rc;
}


//------------------------------------------------------------------------------
//
//  Function:  BSPIntrEnableIrq
//
//  This function is called from OALIntrEnableIrq to enable interrupt on
//  board-level interrupt controller.
//
UINT32 BSPIntrEnableIrq(UINT32 irq)
{    
    OALMSGS(OAL_INTR&&OAL_VERBOSE, (L"+BSPIntrEnableIrq(%d)\r\n", irq));

    // Check if it is a valid board-level interrupt
    switch(irq)
    {
    case IRQ_ETHER:
        // Return SoC level IRQ line
        irq = IRQ_ETHER_GPIO_LINE;
        break;
    }

    OALMSGS(OAL_INTR&&OAL_VERBOSE, (L"-BSPIntrEnableIrq(irq = %d)\r\n", irq));
    return irq;
}


//------------------------------------------------------------------------------
//
//  Function:  BSPIntrDisableIrq
//
//  This function is called from OALIntrDisableIrq to disable interrupt on
//  board-level interrupt controller.
//
UINT32 BSPIntrDisableIrq(UINT32 irq)
{
    OALMSGS(OAL_INTR&&OAL_VERBOSE, (L"+BSPIntrDisableIrq(%d)\r\n", irq));

    // Check if it is a valid board-level interrupt
    switch(irq)
    {
    case IRQ_ETHER:
        // Return SoC level IRQ line
        irq = IRQ_ETHER_GPIO_LINE;
        break;
    }

    OALMSGS(OAL_INTR&&OAL_VERBOSE, (L"-BSPIntrDisableIrq(irq = %d)\r\n", irq));

    return irq;
}


//------------------------------------------------------------------------------
//
//  Function:  BSPIntrDoneIrq
//
//  This function is called from OALIntrDoneIrq to finish interrupt on
//  board-level interrupt controller.
//
UINT32 BSPIntrDoneIrq(UINT32 irq)
{
    OALMSGS(OAL_INTR&&OAL_VERBOSE, (L"+BSPIntrDoneIrq(%d)\r\n", irq));

    // Check if it is a valid board-level interrupt
    switch(irq)
    {
    case IRQ_ETHER:
        // Return SoC level IRQ line
        irq = IRQ_ETHER_GPIO_LINE;
        break;
    }

    OALMSGS(OAL_INTR&&OAL_VERBOSE, (L"-BSPIntrDoneIrq(irq = %d)\r\n", irq));
    return irq;
}


//------------------------------------------------------------------------------
//
//  Function:  BSPIntrActiveIrq
//
//  This function is called from interrupt handler to give BSP chance to
//  translate IRQ in case of board-level interrupt controller.
//
UINT32 BSPIntrActiveIrq(UINT32 irq)
{
    OALMSGS(OAL_INTR&&OAL_VERBOSE, (L"+BSPIntrActiveIrq(%d)\r\n", irq));

    // Check if it is a valid board-level interrupt
    switch(irq)
    {
    case IRQ_ETHER_GPIO_LINE:
        // Return SoC level IRQ line
        irq = IRQ_ETHER;
        break;
    }

    return irq;
}
