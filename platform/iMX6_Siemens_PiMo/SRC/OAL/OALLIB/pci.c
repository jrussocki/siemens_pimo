//-----------------------------------------------------------------------------
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//-----------------------------------------------------------------------------
//
//  Copyright (C) 2007-2010, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//-----------------------------------------------------------------------------
//
//  File:  pci.c
//
//  PCI specific code
//
//-----------------------------------------------------------------------------

#include <bsp.h>
#include <common_pcie.h>

// Note:
// Currently, only BDF 0/0/0 is accepted, since hardware only provides ony slot,
// and our PCIe card has a single function.
// Cf. https://community.freescale.com/thread/302251 To implement a correct BDF access

//-----------------------------------------------------------------------------
//
// Function: OALPCICfgRead
//
// Read PCI data from configuration space
//
// Parameters:
//		busId:	Bus Identifier
//		pciLoc:	Device location
//		offset:	Position of data to read
//		size:	Size of data to read
//		data:	Data buffer
//
// Returns:
//		Amount of written data
//
//-----------------------------------------------------------------------------
UINT32 OALPCICfgRead(UINT32 busId, 
					 OAL_PCI_LOCATION pciLoc, 
					 UINT32 offset, 
					 UINT32 size, 
					 VOID *pData) 
{
	UINT32 address;
	UINT32 value;
	UINT32 count = 0;
	UINT8 *p = (UINT8*)pData;

	if (busId != 0) {
		return 0;
	}

	if (pciLoc.bus != 0) {
		return 0;
	}

	if (pciLoc.dev != 0) {
		return 0;
	}

	address = (UINT32)OALPAtoUA(PCI_CFG_BASE);

    // First read any unaligned data on start
    if ((offset & 0x03) != 0) {
        value = INREG32(address + (offset & ~0x03));
        value >>= (offset & 0x03) << 3;
        while ((offset & 0x03) != 0 && size > 0) {
            *p++ = (UINT8)value;
            value >>= 8;
            offset++;
            size--;
            count++;
        }
    }      
    
    // Then read full DWORDs
    while (size >= 4) {
        value = INREG32(address + offset);
        *p++ = (UINT8)value;
        *p++ = (UINT8)(value >> 8);
        *p++ = (UINT8)(value >> 16);
        *p++ = (UINT8)(value >> 24);
        offset += 4;
        size -= 4;
        count += 4;
    }   
    
    // And remaining data at end
    if (size > 0) {
        value = INREG32(address + offset);
        while (size > 0) {
            *p++ = (UINT8)value;
            value >>= 8;
            size--;
            count++;
        }
    }	

	return count;
}

//-----------------------------------------------------------------------------
//
// Function: OALPCICfgWrite
//
// Write PCI data to configuration space
//
// Parameters:
//		busId:	Bus Identifier
//		pciLoc:	Device location
//		offset:	Position of data to write
//		size:	Size of data to write
//		data:	Data buffer
//
// Returns:
//		Amount of written data
//
//-----------------------------------------------------------------------------
UINT32 OALPCICfgWrite(UINT32 busId, 
					  OAL_PCI_LOCATION pciLoc, 
					  UINT32 offset, 
					  UINT32 size,
					  VOID *pData)
{
	UINT32 address = PCI_CFG_BASE;
	UINT32 value;
	UINT32 count = 0;
	UINT8 *p = (UINT8*)pData;

	if (busId != 0) {
		return 0;
	}

	if (pciLoc.bus != 0) {
		return 0;
	}

	if (pciLoc.dev != 0) {
		return 0;
	}

	// Transform address
    address = (UINT32)OALPAtoUA(address);

    // First write any unaligned data on start
    if ((offset & 0x03) != 0) {
        value = INREG32(address + (offset & ~0x03));
        while ((offset & 0x03) != 0 && size > 0) {
            value &= ~(0xFF << ((offset & 0x03) << 3));
            value |= (UINT32)(*p++ << ((offset & 0x03) << 3));
            offset++;
            size--;
            count++;
        }
        OUTREG32(address + ((offset - 1)&~0x03), value);
    }
    
    // Then write full DWORDs
    while (size >= 4) {
       value =  (UINT32)(*p++);
       value |= (UINT32)(*p++ << 8);
       value |= (UINT32)(*p++ << 16);
       value |= (UINT32)(*p++ << 24);
       OUTREG32(address + offset, value);
       offset += 4;
       size -= 4;
       count += 4;
    }
    
    // And remaining data at end    
    if (size > 0) {
        value = INREG32(address + offset);
        while (size > 0) {
            value &= ~(0xFF << ((offset & 0x03) << 3));
            value |= (UINT32)(*p++ << ((offset & 0x03) << 3));
            offset++;
            size--;
            count++;
        }
        OUTREG32(address + ((offset - 1)&~0x03), value);
    }

	return 0;
}

//-----------------------------------------------------------------------------
//
// Function: OALPCIPowerOn
//
// Enable power on a given bus. This function currently does nothing
//
// Parameters:
//		busId:			Bus Identifier
//
// Returns:
//
//-----------------------------------------------------------------------------
VOID OALPCIPowerOn(UINT32 busId)
{
}

//-----------------------------------------------------------------------------
//
// Function: OALPCIPowerOff
//
// Disable power on a given bus. This function currently does nothing
//
// Parameters:
//		busId:			Bus Identifier
//
// Returns:
//
//-----------------------------------------------------------------------------
VOID OALPCIPowerOff(UINT32 busId)
{
}

//-----------------------------------------------------------------------------
//
// Function: OALPCITransBusAddress
//
// Translate an address from a PCI device address to a system address
//
// Parameters:
//		busId:			Bus Identifier
//		busAddress:		PCI address
//		pAddressSpace:	Address space (0 is memory, 1 I/O space)
//		pSystemAddress:	System address (output)
//
// Returns:
//		FALSE if the conversion failed, TRUE otherwise
//
//-----------------------------------------------------------------------------
BOOL OALPCITransBusAddress(UINT32 busId,
						   UINT64 busAddress,
						   UINT32 *pAddressSpace,
						   UINT64 *pSystemAddress)
{
	if (busId != 0)
		return FALSE;

	if (*pAddressSpace == 0) { // Memory
		if (busAddress >= PCI_MEM_SIZE) {
			return FALSE;
		}
		*pSystemAddress = busAddress + PCI_MEM_BASE;
	}
	else if (*pAddressSpace == 1) { // IO
		if (busAddress >= PCI_IO_SIZE) {
			return FALSE;
		}
		*pSystemAddress = busAddress + PCI_IO_BASE;
	}
	else {
		return FALSE;
	}

	return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function: OALPCIInit
//
// Initialize the PCI subsystem. This function currently does nothing, init is done
// in the PCI bus driver
//
// Parameters:
//
// Returns:
//		TRUE
//
//-----------------------------------------------------------------------------
BOOL OALPCIInit()
{
	return TRUE;
}
