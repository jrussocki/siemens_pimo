//-----------------------------------------------------------------------------
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//-----------------------------------------------------------------------------
//
//  Copyright (C) 2010-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//-----------------------------------------------------------------------------
//
//  File:  ramtable.c
//
//  Ram table to support ram > 512M in Windows CE 7.
//
//-----------------------------------------------------------------------------

#if 0

------------------------------------------------------------------------------
512M kernel space cached mapping, defined in oemaddrtab_cfg.inc

      ---          +---- 8000.0000 ----+
       |           |                   | 
       |           |                   |
       |           |                   |
       |           |                   |                   
       |           |                   |
       |           |     RAM(464 MB)   |
       |           |                   |                   
       |           |                   |
       |           |                   |
       |           |                   |                   
       |           |                   |
       |           |                   |
       |           |                   |
       |           +---- 9D00.0000 ----+
       |           |    WEIM (32 MB)   |
       |           +---- 9F00.0000 ----+
       |           |      blank        |
       |           +---- 9F10.0000 ----+
      512M         |  IPU regs (1 MB)  |
       |           +---- 9F20.0000 ----+
       |           |  IPU regs (1 MB)  |
       |           +---- 9F30.0000 ----+
       |           |      blank        |
       |           +---- 9F50.0000 ----+
       |           |  NFC regs (5 MB)  |
       |           +---- 9FA0.0000 ----+
       |           |    IRAM  (1 MB)   |
       |           +---- 9FB0.0000 ----+
       |           |  TZIC regs (1 MB) |
       |           +---- 9FC0.0000 ----+
       |           |      ROM (1 MB)   |
       |           +---- 9FD0.0000 ----+
       |           | AIPS1 regs (1 MB) |
       |           +---- 9FE0.0000 ----+
       |           | AIPS1 regs (1 MB) |
       |           +---- 9FF0.0000 ----+
       |           | AIPS2 regs (1 MB) |
      ---          +---- A000.0000 ----+ 
                   
------------------------------------------------------------------------------
------------------------------------------------------------------------------
Physical ram mapping :

                   +---- RAM START  ---+
                   |                   |
                   |                   |
                   |                   |    
                   |     report to     |                   
                   |     system by     |
                   |  oaladdresstable  |
                   |                   |        
                   |    refer to       |
                   |   config.bib and  |
                   | oemaddrtab_cfg.inc|
                   |                   |
                   |                   | 
                   +---- 9D00.0000 ----+
                   |                   |
                   |                   |
                   |                   |
                   |                   |
                   |                   |
                   |     report to     |                   
                   |      system       |
                   |    by ramtable    |
                   |                   |                   
                   |                   |                   
                   |                   |
                   |                   |
                   +---- 2G RAM END ---+       
                               
------------------------------------------------------------------------------

------------------------------------------------------------------------------
How to config ram?
sample code provides ram size of 512M and 2G.
to config other ram size, please carefully understand above figures and
changing configuration needs you modify oemaddrtab_cfg.inc, config.bib and ramtable.c
static mapping definition is defined in image_cfg.h and image_cfg.inc
------------------------------------------------------------------------------
#endif

#include <bsp.h>
#include <bldver.h>
#pragma warning(push)
#pragma warning(disable: 4201)
#pragma warning(pop)

//-----------------------------------------------------------------------------
// External Variables
extern DWORD g_dwBoardID;

//-----------------------------------------------------------------------------
// Global Variables
RAMTableEntry g_RamTableEntry[1] = 
{
    // System has 2G physical memory starting from CSP_BASE_MEM_PA_DRAM_LOW
    //physical memory start address >> 8, memory size, attribute must be 0
    {(CSP_BASE_MEM_PA_DRAM_LOW + (STATIC_MAPPING_RAM_SIZE * 1024 * 1024)) >> 8, (1024 - STATIC_MAPPING_RAM_SIZE) * 1024 * 1024, 0},    
};

RamTable g_RAMTable = {MAKELONG(CE_MINOR_VER, CE_MAJOR_VER), sizeof(g_RamTableEntry) / sizeof(g_RamTableEntry[0]), g_RamTableEntry};

//-----------------------------------------------------------------------------
//
// Function: OEMGetRamTable
//      This function is implemented by the OEM to return the OEMRamTable structure, 
//      which allows your platform to support more than 512 MB of physical memory.
// Parameters:
//
// Returns: 
//      Returns an OEMRamTable structure, as defined in %_WINCEROOT%\Public\Common\Oak\Inc\OEMGlobal.h.
//
//
//-----------------------------------------------------------------------------
PCRamTable OEMGetRamTable(void)
{
#ifdef IMGRAM512
    g_RamTableEntry[0].RamSize = (512 - STATIC_MAPPING_RAM_SIZE) * 1024 * 1024;
#endif

    return &g_RAMTable;
}
