;******************************************************************************
;*
;* Copyright (c) Microsoft Corporation.  All rights reserved.
;*
;* Use of this source code is subject to the terms of the Microsoft end-user
;* license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
;* If you did not accept the terms of the EULA, you are not authorized to use
;* this source code. For a copy of the EULA, please see the LICENSE.RTF on your
;* install media.
;*
;******************************************************************************
;*
;* Copyright (C) 2007-2011, Freescale Semiconductor, Inc. All Rights Reserved.
;* THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
;* AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
;*
;******************************************************************************
;*
;* FILE:    startup.s
;*
;* PURPOSE: Before control is transferred to the kernel, the boot loader
;*          calls this StartUp code to put the CPU into an initialized state.
;*
;******************************************************************************

    INCLUDE mx6_base_regs.inc
    INCLUDE mx6_base_mem.inc
    INCLUDE image_cfg.inc

    ; System scan    
    IMPORT  OALSysScan
    IMPORT  ConfigCpuAuxFeatures
    IMPORT  ConfigCpuDiagCtrl
    IMPORT  BSP_GetPlatInfoOffset    
    
	;Cache operations
	IMPORT  OALInvICache
    IMPORT  OALInvDCache
    IMPORT  OALInvITLB
    IMPORT  InvExtL2PA
    IMPORT  OALInvDTLB
	IMPORT  OALInvUTLB
                
;
; ARM constants
;
ARM_CPSR_PRECISE            EQU     (1 << 8)
ARM_CPSR_IRQDISABLE         EQU     (1 << 7)
ARM_CPSR_FIQDISABLE         EQU     (1 << 6)
ARM_CPSR_MODE_SVC           EQU     0x13
ARM_CPSR_MODE_IRQ           EQU     0x12

ARM_CTRL_MMU                EQU     (1 << 0)
ARM_CTRL_ALIGN              EQU     (1 << 1)
ARM_CTRL_DCACHE             EQU     (1 << 2)
ARM_CTRL_FLOW               EQU     (1 << 11)
ARM_CTRL_ICACHE             EQU     (1 << 12)
ARM_CTRL_VECTORS            EQU     (1 << 13)
ARM_CTRL_TRE                EQU     (1 << 28)

ARM_CACR_FULL               EQU     0x3

ARM_AUXCR_L2EN              EQU     (1 << 1)

; VFP uses coproc 10 for single-precision instructions
ARM_VFP_SP_COP              EQU     10
ARM_VFP_SP_ACCESS           EQU     (ARM_CACR_FULL << (ARM_VFP_SP_COP*2))

; VFP uses coproc 11 for double-precision instructions
ARM_VFP_DP_COP              EQU     11
ARM_VFP_DP_ACCESS           EQU     (ARM_CACR_FULL << (ARM_VFP_DP_COP*2))

; Configure coprocessor access control
ARM_CACR_CONFIG             EQU     (ARM_VFP_SP_ACCESS | ARM_VFP_DP_ACCESS)

;
; L2CC constants
;
L2CC_CR_OFFSET              EQU     0x100
L2CC_TAG_LAT_OFFSET         EQU     0x108
L2CC_DATA_LAT_OFFSET        EQU     0x10C
L2CC_INVWAY_OFFSET          EQU     0x77C
L2CC_PREFETCH_OFFSET        EQU     0xF60

;
; GIC constants
;
GIC_INTF_ICCICR_OFFSET      EQU     0x0000
GIC_INTF_ICCPMR_OFFSET      EQU     0x0004

GIC_DIST_ICDDCR_OFFSET      EQU     0x0000
GIC_DIST_ICDISR_OFFSET      EQU     0x0080
GIC_DIST_ICDICER_OFFSET     EQU     0x0180
GIC_DIST_ICDIPR_OFFSET      EQU     0x0400
GIC_DIST_ICDIPTR_OFFSET     EQU     0x0800
GIC_DIST_ICDICFR_OFFSET     EQU     0x0C00

;
; SNVS Constants
;
SNVS_LPCR                   EQU     0x0038

;
; AIPS Constants
;
AIPSREG_MPR0_OFFSET         EQU     0x0000
AIPSREG_MPR1_OFFSET         EQU     0x0004
AIPSREG_OPACR0_OFFSET       EQU     0x0040
AIPSREG_OPACR1_OFFSET       EQU     0x0044
AIPSREG_OPACR2_OFFSET       EQU     0x0048
AIPSREG_OPACR3_OFFSET       EQU     0x004C
AIPSREG_OPACR4_OFFSET       EQU     0x0050

;
; CCM constants
;
CCM_CCDR_OFFSET             EQU     0x0004
CCM_CCSR_OFFSET             EQU     0x000C
CCM_CBCDR_OFFSET            EQU     0x0014
CCM_CBCMR_OFFSET            EQU     0x0018
CCM_CSCMR1_OFFSET           EQU     0x001C
CCM_CSCMR2_OFFSET           EQU     0x0020
CCM_CSCDR1_OFFSET           EQU     0x0024
CCM_CS1CDR_OFFSET           EQU     0x0028
CCM_CSCDR2_OFFSET           EQU     0x0038
CCM_CSCDR3_OFFSET           EQU     0x003C
CCM_CSCDR4_OFFSET           EQU     0x0040
CCM_CDHIPR_OFFSET           EQU     0x0048
CCM_CCGR0_OFFSET            EQU     0x0068
CCM_CCGR1_OFFSET            EQU     0x006C
CCM_CCGR2_OFFSET            EQU     0x0070
CCM_CCGR3_OFFSET            EQU     0x0074
CCM_CCGR4_OFFSET            EQU     0x0078
CCM_CCGR5_OFFSET            EQU     0x007C
CCM_CCGR6_OFFSET            EQU     0x0080
CCM_CCGR7_OFFSET            EQU     0x0084

CCM_CCSR_PLL1_SW_CLK_SEL    EQU     (0x1 << 2)

CCM_CBCDR_PERIPH_CLK_SEL    EQU     (0x1 << 25)
CCM_CBCDR_NFC_PODF          EQU     (0x7 << 13)
CCM_CBCDR_NFC_PODF3         EQU     (0x2 << 13)

CCM_CBCMR_PERIPH_APM_SEL0   EQU     (0x1 << 12)
CCM_CBCMR_PERIPH_APM_SEL1   EQU     (0x1 << 13)

CCM_CSCDR1_UART_CLK_PRED    EQU     (0x7 << 3)
CCM_CSCDR1_UART_CLK_PODF    EQU     (0x7 << 0)
CCM_CSCDR1_UART_CLK_PRED5   EQU     (0x4 << 3)
CCM_CSCDR1_UART_CLK_PODF2   EQU     (0x1 << 0)

;
; IOMUXC constants
;
IOMUXC_GPR1_OFFSET          EQU     0x0004

;
; WEIM constants
;
WEIM_CS1GCR1_OFFSET         EQU     0x0018
WEIM_CS1GCR2_OFFSET         EQU     0x001C
WEIM_CS1RCR1_OFFSET         EQU     0x0020
WEIM_CS1RCR2_OFFSET         EQU     0x0024
WEIM_CS1WCR1_OFFSET         EQU     0x0028
WEIM_CS1WCR2_OFFSET         EQU     0x002C
WEIM_WCR_OFFSET             EQU     0x0090

    OPT 2                                       ; disable listing
    INCLUDE kxarm.h
    INCLUDE armmacros.s
    OPT 1                                       ; reenable listing

    IF :DEF:BOOTLOADER
    ELSE
        IMPORT  KernelStart
    ENDIF

;******************************************************************************
;*
;* FUNCTION:    StartUp
;*
;* DESCRIPTION: System bootstrap function
;*
;* PARAMETERS:  None
;*
;* RETURNS:     None
;*
;******************************************************************************

    ARM

    STARTUPTEXT
    ALIGN 4
    NESTED_ENTRY StartUp, |.astart|

    PROLOG_END StartUp

    ; enters at 0x10042000
    ; starts at offset 0x1000 in eboot.nb0
    ; processor is in arm-encoded instruction mode

    ARM2THUMB R0

    THUMB

    ;--------------------------------------------------------------------------
    ; MS RECOMMENDATION:
    ; Put the processor in supervisor mode
    ; Disable the interrupt request (IRQ) and fast interrupt request (FIQ)
    ; inputs
    ;--------------------------------------------------------------------------

    mrs  R0, cpsr                     ; r0 = CPSR
    BIC  R0, R0, #0x1F                ; Clear mode bits
    ORR  R0, R0, #ARM_CPSR_MODE_SVC   ; enter supervisor mode
    ORR  R0, R0, #ARM_CPSR_IRQDISABLE ; disable (mask out) normal IRQ
    ORR  R0, R0, #ARM_CPSR_FIQDISABLE ; disable (mask out) fast IRQ
    ;ORR R0, R0, #ARM_CPSR_PRECISE    ; enable precise data aborts
    MSR  cpsr_xc, R0                  ; update CPSR control bits

    ;--------------------------------------------------------------------------
    ; MS RECOMMENDATION:
    ; Disable memory management unit (MMU) and both the instruction and data
    ; caches
    ;--------------------------------------------------------------------------
		
    mrc     p15, 0, r0, c1, c0, 0               ; r0 = system control reg
    bic     r0, r0, #ARM_CTRL_ICACHE            ; disable ICache
    bic     r0, r0, #ARM_CTRL_DCACHE            ; disable DCache
    bic     r0, r0, #ARM_CTRL_MMU               ; disable MMU
    bic     r0, r0, #ARM_CTRL_VECTORS           ; set vector base to 0x00000000
    orr     r0, r0, #ARM_CTRL_FLOW              ; program flow prediction enabled
    bic     r0, r0, #ARM_CTRL_TRE               ; disable TEX remap
    bic     r0, r0, #ARM_CTRL_ALIGN             ; disable Alignments Fault Checking
    mcr     p15, 0, r0, c1, c0, 0               ; update system control reg

    ;
    ; Disable L2 cache
    ;

    ; ldr     r1, =CSP_BASE_REG_PA_L2CC
    ; ldr     r0, =0x0
    ; str     r0, [r1, #L2CC_CR_OFFSET]

    ;
    ; Configure ARM coprocessor access control register
    ;

    ldr     r0, =ARM_CACR_CONFIG                ; r0 = CACR configuration
    mcr     p15, 0, r0, c1, c0, 2               ; update CACR
    dsb											; DSB
    isb                                         ; ISB

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    ;--------------------------------------------------------------------------
    ; MS RECOMMENDATION:
    ; Flush or invalidate the instruction and data caches and the translation
    ; look-aside buffer (TLB) and empty the write buffers
    ;--------------------------------------------------------------------------
	
	; Initialize Stack Pointer
	mov     r3, pc
    and     r3, r3, #0xF0000000
	ldr     r0, =IMAGE_BOOT_STACK_RAM_PA_START
	add     r3, r3, r0
	mov		sp,r3
	
	
	; Data cache maintenance routines, called from cachev7 Lib
	bl      OALInvICache    ; L1 instruction cache invalidate
    
	mov     r0, #0          
    bl      OALInvDCache	; L1 data cache invalidate
    
	mov     r0, #1          
    bl      OALInvDCache	; L2 cache (if any)
    ldr		r0, =CSP_BASE_REG_PA_L2CC
    bl      InvExtL2PA      ; Ext L2 cache (if any)
	
    bl 		OALInvUTLB				; invalidate entire unified TLB
    bl      OALInvITLB			; invalidate entire instruction TLB
    bl      OALInvDTLB			; invalidate entire data TLB

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    ; ----- ARM errata #751472 -----
    ; Description:  An interrupted ICIALLUIS operation may prevent the 
    ;   completion of a following broadcasted operation.
    ;
    ; Workaround:  Set bit[11] in the undocumented Diagnostic Control 
    ;   register placed in CP15 c15 0 c0 1.
    ;
    mrc     p15, 0, r0, c15, c0, 1
    orr     r0, r0, #(1 << 11)
    mcr     p15, 0, r0, c15, c0, 1
    
    ; ARM: 743622�Faulty logic in the Store Buffer may lead to data corruption
    mrc		p15, 0, r0, c15, c0, 1	; read diagnostic register
	orr		r0, r0, #(1 << 6)		; set bit #6
	mcr		p15, 0, r0, c15, c0, 1	; write diagnostic register
	
	; ARM: errata 742230 DMB operation may be faulty
	mrc	p15, 0, r0, c15, c0, 1		; read diagnostic register;
	orr	r0, r0, #1 << 4				; set bit #4
	mcr	p15, 0, r0, c15, c0, 1		; write diagnostic register
	
    
    ; 
    ; Configure AHB<->IP-bus interface (AIPS) registers
    ;
    ldr     r1, =CSP_BASE_REG_PA_AIPS1
    ldr     r2, =CSP_BASE_REG_PA_AIPS2

    ; Except for AIPS regs, configure all peripherals as follows:
    ;   unbuffered writes (MBW=0)
    ;   disable supervisor protect (SP=0)
    ;   disable write protect (WP=0)
    ;   disable trusted protect (TP=0)
    mov     r0, #0
    str     r0, [r1, #AIPSREG_OPACR0_OFFSET]
    str     r0, [r1, #AIPSREG_OPACR1_OFFSET]
    str     r0, [r1, #AIPSREG_OPACR2_OFFSET]
    str     r0, [r1, #AIPSREG_OPACR3_OFFSET]
    str     r0, [r1, #AIPSREG_OPACR4_OFFSET]
    str     r0, [r2, #AIPSREG_OPACR0_OFFSET]
    str     r0, [r2, #AIPSREG_OPACR1_OFFSET]
    str     r0, [r2, #AIPSREG_OPACR2_OFFSET]
    str     r0, [r2, #AIPSREG_OPACR3_OFFSET]
    str     r0, [r2, #AIPSREG_OPACR4_OFFSET]

    ; Set all MPRx to be non-bufferable, trusted for R/W,
    ; not forced to user-mode.
    ldr     r0, =(0x77777777)
    str     r0, [r1, #AIPSREG_MPR0_OFFSET]
    str     r0, [r1, #AIPSREG_MPR1_OFFSET]
    str     r0, [r2, #AIPSREG_MPR0_OFFSET]
    str     r0, [r2, #AIPSREG_MPR1_OFFSET]
    
    ;--------------------------------------------------------------------------
    ; MS RECOMMENDATION:
    ; Determine the reason you are in the startup code, such as cold reset,
    ; watchdog reset, GPIO reset, and sleep reset.
    ;--------------------------------------------------------------------------


    ;--------------------------------------------------------------------------
    ; MS RECOMMENDATION:
    ; Configure the GPIO lines per the requirements of the board. GPIO lines
    ; must be enabled for on-board features like LED.
    ;--------------------------------------------------------------------------


    ;--------------------------------------------------------------------------
    ; MS RECOMMENDATION:
    ; Configure the memory controller, set refresh frequency, and enable
    ; clocks.  Program data width and memory timing values and power up the
    ; banks.
    ;--------------------------------------------------------------------------

    ; Retain clock configuration from bootloader
    IF :DEF:BOOTLOADER
    ELSE
        B CACHE_INIT
    ENDIF

    ldr     r1, =CSP_BASE_REG_PA_PLL_CPU
    ldr     r2, =CSP_BASE_REG_PA_PLL_BUS
    ldr     r3, =CSP_BASE_REG_PA_PLL_USBOTG
    ldr     r4, =CSP_BASE_REG_PA_CCM

    ldr     r0, [r4, #CCM_CBCMR_OFFSET]
    bic     r0, r0, #(0x3 << (2*2))         ; GPU 3D CORE 528M
    orr     r0, r0, #(0x2 << (4*2))         ; 
    bic     r0, r0, #(0x1 << (4*2))         ; GPU 3D SHADER 594M
    str     r0, [r4, #CCM_CBCMR_OFFSET]    
    
CACHE_INIT
    ldr     r4, =CSP_BASE_REG_PA_CCM
    ldr     r0, =0xFFFFFFFF
    str     r0, [r4, #CCM_CCGR0_OFFSET]
    str     r0, [r4, #CCM_CCGR1_OFFSET]
    str     r0, [r4, #CCM_CCGR2_OFFSET]
    str     r0, [r4, #CCM_CCGR3_OFFSET]
    str     r0, [r4, #CCM_CCGR4_OFFSET]
    str     r0, [r4, #CCM_CCGR5_OFFSET]
    str     r0, [r4, #CCM_CCGR6_OFFSET]
    str     r0, [r4, #CCM_CCGR7_OFFSET]

    ; Configure WEIM
WEIM_CONFIG
    ; Configure WEIM for 64MB on CS0, 64MB on CS1
    ldr     r1, =CSP_BASE_REG_PA_IOMUXC
    ldr     r0, [r1, #IOMUXC_GPR1_OFFSET]
    bic     r0, r0, #0x3F
    orr     r0, r0, #0x1B    
    str     r0, [r1, #IOMUXC_GPR1_OFFSET]

    ldr     r1, =CSP_BASE_REG_PA_WEIM
    ldr     r0, =0x00020001
    str     r0, [r1, #WEIM_CS1GCR1_OFFSET]
    ldr     r0, =0x00000000
    str     r0, [r1, #WEIM_CS1GCR2_OFFSET]
    ldr     r0, =0x16000202
    str     r0, [r1, #WEIM_CS1RCR1_OFFSET]
    ldr     r0, =0x00000002
    str     r0, [r1, #WEIM_CS1RCR2_OFFSET]
    ldr     r0, =0x16002082
    str     r0, [r1, #WEIM_CS1WCR1_OFFSET]
    ldr     r0, =0x00000000
    str     r0, [r1, #WEIM_CS1WCR2_OFFSET]
    ldr     r0, =0x00000000
    str     r0, [r1, #WEIM_WCR_OFFSET]

GIC_CONFIG
    ; 128 shared peripheral + 16 private + 16 software = 160 interrupts
    ; 160 interrupts are covered by the following register types:
    ;       1 bit/interrupt = 5 registers (ICDICER, ICDISR)
    ;       2 bits/interrupt = 10 registers (ICDICFR)
    ;       8 bits/interrupt = 40 registers (ICDIPR)

    ; Disable interrupts at GIC distributor level
    ldr     r0, =0
    ldr     r1, =CSP_BASE_REG_PA_GIC_DIST
    str     r0, [r1, #GIC_DIST_ICDDCR_OFFSET]

    ; Disable all interrupts by writing 1 to interrupt clear-enable register
    ldr     r0, =0xFFFFFFFF
    ldr     r1, =(CSP_BASE_REG_PA_GIC_DIST+GIC_DIST_ICDICER_OFFSET)
    str     r0, [r1, #0x00]
    str     r0, [r1, #0x04]
    str     r0, [r1, #0x08]
    str     r0, [r1, #0x0C]
    str     r0, [r1, #0x10]

    ; Configure all shared peripheral interrupts as secure.
    ldr     r0, =0x0
    ldr     r1, =(CSP_BASE_REG_PA_GIC_DIST+GIC_DIST_ICDISR_OFFSET)
    str     r0, [r1, #0x00]
    str     r0, [r1, #0x04]
    str     r0, [r1, #0x08]
    str     r0, [r1, #0x0C]
    str     r0, [r1, #0x10]
    
    ; Configure all interrupts for level-sensitive, 1-N model.
    ldr     r0, =0x55555555
    ldr     r1, =(CSP_BASE_REG_PA_GIC_DIST+GIC_DIST_ICDICFR_OFFSET)
    str     r0, [r1, #0x00]
    str     r0, [r1, #0x04]
    str     r0, [r1, #0x08]
    str     r0, [r1, #0x0C]
    str     r0, [r1, #0x10]
    str     r0, [r1, #0x14]
    str     r0, [r1, #0x18]
    str     r0, [r1, #0x1C]
    str     r0, [r1, #0x20]
    str     r0, [r1, #0x24]

    ; Configure priority for all interrupts as 128
    ldr     r0, =0x80808080
    ldr     r1, =(CSP_BASE_REG_PA_GIC_DIST+GIC_DIST_ICDIPR_OFFSET)
    str     r0, [r1, #0x00]
    str     r0, [r1, #0x04]
    str     r0, [r1, #0x08]
    str     r0, [r1, #0x0C]
    str     r0, [r1, #0x10]
    str     r0, [r1, #0x14]
    str     r0, [r1, #0x18]
    str     r0, [r1, #0x1C]
    str     r0, [r1, #0x20]
    str     r0, [r1, #0x24]
    str     r0, [r1, #0x28]
    str     r0, [r1, #0x2C]
    str     r0, [r1, #0x30]
    str     r0, [r1, #0x34]
    str     r0, [r1, #0x38]
    str     r0, [r1, #0x3C]
    str     r0, [r1, #0x40]
    str     r0, [r1, #0x44]
    str     r0, [r1, #0x48]
    str     r0, [r1, #0x4C]
    str     r0, [r1, #0x50]
    str     r0, [r1, #0x54]
    str     r0, [r1, #0x58]
    str     r0, [r1, #0x5C]
    str     r0, [r1, #0x60]
    str     r0, [r1, #0x64]
    str     r0, [r1, #0x68]
    str     r0, [r1, #0x6C]
    str     r0, [r1, #0x70]
    str     r0, [r1, #0x74]
    str     r0, [r1, #0x78]
    str     r0, [r1, #0x7C]
    str     r0, [r1, #0x80]
    str     r0, [r1, #0x84]
    str     r0, [r1, #0x88]
    str     r0, [r1, #0x8C]
    str     r0, [r1, #0x90]
    str     r0, [r1, #0x94]
    str     r0, [r1, #0x98]
    str     r0, [r1, #0x9C]

    ; Configured all interrupts targeted to CPU0 only
    ldr     r0, =0x01010101
    ldr     r1, =(CSP_BASE_REG_PA_GIC_DIST+GIC_DIST_ICDIPTR_OFFSET)
    str     r0, [r1, #0x00]
    str     r0, [r1, #0x04]
    str     r0, [r1, #0x08]
    str     r0, [r1, #0x0C]
    str     r0, [r1, #0x10]
    str     r0, [r1, #0x14]
    str     r0, [r1, #0x18]
    str     r0, [r1, #0x1C]
    str     r0, [r1, #0x20]
    str     r0, [r1, #0x24]
    str     r0, [r1, #0x28]
    str     r0, [r1, #0x2C]
    str     r0, [r1, #0x30]
    str     r0, [r1, #0x34]
    str     r0, [r1, #0x38]
    str     r0, [r1, #0x3C]
    str     r0, [r1, #0x40]
    str     r0, [r1, #0x44]
    str     r0, [r1, #0x48]
    str     r0, [r1, #0x4C]
    str     r0, [r1, #0x50]
    str     r0, [r1, #0x54]
    str     r0, [r1, #0x58]
    str     r0, [r1, #0x5C]
    str     r0, [r1, #0x60]
    str     r0, [r1, #0x64]
    str     r0, [r1, #0x68]
    str     r0, [r1, #0x6C]
    str     r0, [r1, #0x70]
    str     r0, [r1, #0x74]
    str     r0, [r1, #0x78]
    str     r0, [r1, #0x7C]
    str     r0, [r1, #0x80]
    str     r0, [r1, #0x84]
    str     r0, [r1, #0x88]
    str     r0, [r1, #0x8C]
    str     r0, [r1, #0x90]
    str     r0, [r1, #0x94]
    str     r0, [r1, #0x98]
    str     r0, [r1, #0x9C]

    ; Configure interrupt priority mask.  
    ; Priority mask = 255 ==> all interrupts with priority value less than
    ; 255 will be unmasked
    ldr     r0, =255
    ldr     r1, =(CSP_BASE_REG_PA_GIC_INTF)
    str     r0, [r1, #GIC_INTF_ICCPMR_OFFSET]
    
    ; Enable interrupts at GIC distributor level
    ldr     r0, =3
    ldr     r1, =CSP_BASE_REG_PA_GIC_DIST
    str     r0, [r1, #GIC_DIST_ICDDCR_OFFSET]

    ; Enable interrupts at GIC interface level
    ldr     r0, =7
    ldr     r1, =(CSP_BASE_REG_PA_GIC_INTF)
    str     r0, [r1, #GIC_INTF_ICCICR_OFFSET]
    
    dsb											; DSB
    isb                                         ; ISB
    ;--------------------------------------------------------------------------
    ; MS RECOMMENDATION:
    ; Initialize the real-time clock count register to 0 and enable the
    ; real-time clock. 
    ;--------------------------------------------------------------------------
    ; TODO:  Initize SNVS
    ldr     r1, =(CSP_BASE_REG_PA_SNVS_HP)
    ldr     r0, [r1, #SNVS_LPCR]
    orr     r0, r0, #(1 << 0)
    str     r0, [r1, #SNVS_LPCR]

    ;--------------------------------------------------------------------------
    ; MS RECOMMENDATION:
    ; Set up the power management/monitoring registers. Set conditions during
    ; sleep modes. 
    ;--------------------------------------------------------------------------

    ;--------------------------------------------------------------------------
    ; MS RECOMMENDATION:
    ; Turn on all board-level clocks and on chip peripheral clocks.
    ;--------------------------------------------------------------------------
    
    ;--------------------------------------------------------------------------
    ; Setup stack pointer,
    ; Invalidate L1 cache and
    ; Scan system (get processor version and features)
    ;--------------------------------------------------------------------------

	
	; Scan system	
	; * Get PlatInfo offset, store it to r1
    bl BSP_GetPlatInfoOffset
    mov r1, r0
	mov     r0, pc
    and     r0, r0, #0xF0000000
    add     r0, r0, r1
    ; r0 now contains the address of PlatInfo structure
    bl		OALSysScan
    

	; Configure ACTRL - Auxillary Features
    ; * enable L1/L2 prefetch  ACTLR bit [2] / ACTLR bit [1]
	; * enable ACTLR.SMP bit [6]
    ; 
    bl BSP_GetPlatInfoOffset
    mov r1, r0
	mov     r0, pc
    and     r0, r0, #0xF0000000
    add     r0, r0, r1
    bl      ConfigCpuAuxFeatures
    

    ; Config CPU DiagCTRL register 
	; * fixed some A9 erratas
    ;
    bl BSP_GetPlatInfoOffset
    mov r1, r0
	mov     r0, pc
    and     r0, r0, #0xF0000000
    add     r0, r0, r1
    bl      ConfigCpuDiagCtrl
     
    ;--------------------------------------------------------------------------
    ; MS RECOMMENDATION:
    ; Get the physical base address of the OEMAddressTable and store in r0.
    ;--------------------------------------------------------------------------
    ADR R0, g_oalAddressTable

    ;--------------------------------------------------------------------------
    ; MS RECOMMENDATION:
    ; Jump to KernelStart to boot WindowsCE or BootloaderMain for bootloader
    ;--------------------------------------------------------------------------

    IF :DEF:BOOTLOADER
        B KernelStart
    ELSE
        BL KernelStart
    ENDIF

SPIN
    b       SPIN

    EPILOG_ENTRY StartUp

    NESTED_END StartUp

    ; Include memory configuration file with g_oalAddressTable
    STARTUPDATA
    ALIGN 4
    INCLUDE oemaddrtab_cfg.inc

    END
