;******************************************************************************
;*
;* Copyright (c) Microsoft Corporation.  All rights reserved.
;*
;* Use of this source code is subject to the terms of the Microsoft end-user
;* license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
;* If you did not accept the terms of the EULA, you are not authorized to use
;* this source code. For a copy of the EULA, please see the LICENSE.RTF on your
;* install media.
;*
;******************************************************************************
;*
;* Copyright (C) 2010-2011, Freescale Semiconductor, Inc. All Rights Reserved.
;* THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
;* AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
;*
;******************************************************************************
;*
;* FILE:    actrl.s
;*
;*
;******************************************************************************

    INCLUDE kxarm.h
    INCLUDE armmacros.s

    EXPORT MpSetACTRLSMPFW

    TEXTAREA

    NESTED_ENTRY MpSetACTRLSMPFW

    PUSHSTACK2 R0, R1

    PROLOG_END MpSetACTRLSMPFW

    MRC p15, 0, R0, C1, C0, 1 ; Read ACTRL
    ORR R0, R0, #0x40         ; Turn on ACTRL.SMP
    ORR R0, R0, #1            ; Turn on ACTRL.FW
    ORR R0, R0, #2            ; Enable L2 prefetch
    ORR R0, R0, #4            ; Enable L1 prefetch
    MCR p15, 0, R0, C1, C0, 1 ; Write ACTRL

    EPILOG_ENTRY MpSetACTRLSMPFW

    POPSTACK2 R0, R1
    
    RETURN

    NESTED_END MpSetACTRLSMPFW

    END
