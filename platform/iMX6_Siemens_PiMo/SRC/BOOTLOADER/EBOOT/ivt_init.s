;------------------------------------------------------------------------------
;
;  Copyright (C) 2012, Adeneo Embedded All Rights Reserved.
;  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
;  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
;
;   Copyright (C) 2011, Freescale Semiconductor, Inc. All Rights Reserved.
;   THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
;   AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
;
;------------------------------------------------------------------------------
;
;   FILE:   ivt_init.s
;   
;   First instruction to be executed is this jump instruction, control 
;   is passed to the entry point of eboot from this function
;   This object file should be located at first address of Storage.
;------------------------------------------------------------------------------
    INCLUDE armmacros.s
    INCLUDE mx6q_base_regs.inc
    INCLUDE mx6q_base_mem.inc
    INCLUDE image_cfg.inc

IRAM_FREE_SPACE_START   EQU     0x00907000      ; Free space, IRAM_START + 7 * 4K
NFC_BUFFER_ADDR         EQU     0xF7FF0000      ; TODO: 
IMAGE_HDR_SIZE          EQU     0x1000          ; Reserve 4K for plugin code
IVT_HEADER              EQU     0x402000D1      ; 0xD1002040
BASE_ADDR_OFFSET_4K     EQU     0x1000
BASE_ADDR_OFFSET_1K     EQU     0x0400
BASE_ADDR_OFFSET_256    EQU     0x0100
BIN_OFFSET              EQU     0x1000          ;This offset is from bin file generation by romimage.
                                                ;We cut 4k when programming the image into sd card so 
                                                ;that we rom code can get correct ivt, when move image
                                                ;from storage to ram, we must add it back, so that all address 
                                                ;in code is correct.

;TODO clean up
IOMUXC_BASE_ADDR	EQU		CSP_BASE_REG_PA_IOMUXC
MMDC_P0_BASE_ADDR	EQU		CSP_BASE_REG_PA_MMDC0
MMDC_P1_BASE_ADDR	EQU		CSP_BASE_REG_PA_MMDC1

;
; ROM constants
;
ROM_VER_OFFSET          EQU     0x0048
ROM_API_VECTOR_OFFSET	EQU		0x000000C0

;
; CCM constants
;
CCM_CBCDR_OFFSET        EQU     0x0014


; Image destination address is the ram address which is aligned with address 0 of 
; corresponding boot device. 
    IF :DEF: IMGSDMMC
; Config for SD/MMC Boot
IVT_OFFSET              EQU    BASE_ADDR_OFFSET_1K
BOOT_DATA_IMG_LEN       EQU    IMAGE_BOOT_BOOTIMAGE_SD_SIZE+IVT_OFFSET  ; 512K + OFFSET
    ELSE

    IF :DEF: IMGATA
; Config for ATA Boot
IVT_OFFSET              EQU    BASE_ADDR_OFFSET_1K
BOOT_DATA_IMG_LEN       EQU    IMAGE_BOOT_BOOTIMAGE_ATA_SIZE+IVT_OFFSET
    ELSE
    
    IF :DEF: IMGNAND
; Config for NAND Boot
IVT_OFFSET              EQU    BASE_ADDR_OFFSET_1K
BOOT_DATA_IMG_LEN       EQU    IMAGE_BOOT_BOOTIMAGE_RAM_SIZE+IVT_OFFSET
    ELSE
; Config for SPI Boot
IVT_OFFSET              EQU    BASE_ADDR_OFFSET_1K
BOOT_DATA_IMG_LEN       EQU    IMAGE_BOOT_BOOTIMAGE_SD_SIZE+IVT_OFFSET; IMAGE_BOOT_BOOTIMAGE_SPI_SIZE+IVT_OFFSET
    ENDIF
    ENDIF

    ENDIF
IVT_ADDRESS             EQU    IMAGE_BOOT_BOOTIMAGE_RAM_PA_START+BIN_OFFSET  

    MACRO
    RESERVE_CSF     $size
        LCLA    Counter
Counter     SETA    $size
        WHILE   Counter > 0
Counter     SETA    Counter - 1
            DCB 0x00 
        WEND
    MEND
    
	; Macro used for Little Endian value conversion to Big Endian
    MACRO   
    DCD_BE  $value
        DCD   (((($value) & 0xFF) << 24) :OR: ((($value) & 0xFF00) << 8) :OR: ((($value) & 0xFF0000) >> 8) :OR: ((($value) & 0xFF000000) >> 24))
    MEND

	; Macro used for ivt, dcd header
    MACRO   
    DCD_HEADER  $tag, $length, $version
        DCD_BE	((($tag & 0xFF) << 24) :OR: (($length & 0xFFFF) << 8) :OR: ($version & 0xFF))
    MEND

	; Macro used for DCD table content
    MACRO
    MXC_DCD_ITEM  $i, $addr,$value
        DCD_BE  $addr
        DCD_BE  $value
    MEND

    ; required for TEXT AREA definition
    OPT 2                               ; disable listing
    INCLUDE kxarm.h
    OPT 1 
    
    TEXTAREA
    
    IMPORT StartUp                      ; entry point for eboot

    AREA .asecure,ALIGN=4,CODE

    ; IVT (Image Vector Table) format
    ;
    ;   0x00    HEADER (tag, length, version)
    ;   0x04    ENTRY ADDRESS
    ;   0x08    RESERVED1
    ;   0x0C    DCD ADDRESS
    ;   0x10    BOOT DATA ADDRESS
    ;   0X14    SELF ADDRESS
    ;   0X18    CSF ADDRESS
    ;   0X1C    RESERVED2


; Second IVT to give entry point into the bootloader copied to DDR 
: Table 8-24
IVT_ADDR
     DCD IVT_HEADER                                                  ; HEADER
     DCD IVT_ADDRESS + IMAGE_HDR_SIZE								 ; IMAGE START ADDRESS FOR EXECUTE
     DCD 0                                                           ; RESERVED1
     DCD IVT_ADDRESS + (DCD_ADDR-IVT_ADDR)							 ; DCD ADDRESS
     DCD IVT_ADDRESS + (BOOT_DATA_ADDR-IVT_ADDR)						; BOOT DATA ADDRESS
     DCD IVT_ADDRESS + (IVT_ADDR-IVT_ADDR)							 ; SELF ADDRESS
     DCD 0                                                           ; CSF ADDRESS
     DCD 0                                                           ; RESERVE2

    ; IVT boot data format
    ;   0x00    IMAGE START ADDR
    ;   0x04    IMAGE SIZE
    ;   0x08    PLUGIN FLAG    
BOOT_DATA_ADDR
    DCD IVT_ADDRESS - IVT_OFFSET	; START OF DESTINTATION ADDRESS
    DCD BOOT_DATA_IMG_LEN           ; IMAGE SIZE
    DCD 0                           ; PLUGIN FLAG


; DCD description for platform init
DCD_ADDR
	; DCD Header
	; TODO check why 0x40 while specified 0x41 in the documentation
	DCD_HEADER	0xD2, (DCD_ADDR_END - DCD_ADDR), 0x40
	
DCD_WC
	; DCD Write Command Header
	DCD_HEADER	0xCC, (DCD_ADDR_END - DCD_WC),0x04

; DDR IO TYPE
	MXC_DCD_ITEM 1, IOMUXC_BASE_ADDR + 0x774, 0x000C0000
	MXC_DCD_ITEM 2, IOMUXC_BASE_ADDR + 0x754, 0x00000000

; clock                           
	MXC_DCD_ITEM 3, IOMUXC_BASE_ADDR + 0x4ac, 0x00000030
	MXC_DCD_ITEM 4, IOMUXC_BASE_ADDR + 0x4b0, 0x00000030

; address                         
	MXC_DCD_ITEM 5, IOMUXC_BASE_ADDR + 0x464, 0x00000030
	MXC_DCD_ITEM 6, IOMUXC_BASE_ADDR + 0x490, 0x00000030
	MXC_DCD_ITEM 7, IOMUXC_BASE_ADDR + 0x74c, 0x00000030

; control 
	MXC_DCD_ITEM 8, IOMUXC_BASE_ADDR + 0x494, 0x00000030
	MXC_DCD_ITEM 9, IOMUXC_BASE_ADDR + 0x4a0, 0x00000000
	MXC_DCD_ITEM 10, IOMUXC_BASE_ADDR + 0x4b4, 0x00003030
	MXC_DCD_ITEM 11, IOMUXC_BASE_ADDR + 0x4b8, 0x00003030
	MXC_DCD_ITEM 12, IOMUXC_BASE_ADDR + 0x76c, 0x00000030

; data strobe 
	MXC_DCD_ITEM 13, IOMUXC_BASE_ADDR + 0x750, 0x00020000

	MXC_DCD_ITEM 14, IOMUXC_BASE_ADDR + 0x4bc, 0x00000030
	MXC_DCD_ITEM 15, IOMUXC_BASE_ADDR + 0x4c0, 0x00000030
	MXC_DCD_ITEM 16, IOMUXC_BASE_ADDR + 0x4c4, 0x00000030
	MXC_DCD_ITEM 17, IOMUXC_BASE_ADDR + 0x4c8, 0x00000030
	MXC_DCD_ITEM 18, IOMUXC_BASE_ADDR + 0x4cc, 0x00000030
	MXC_DCD_ITEM 19, IOMUXC_BASE_ADDR + 0x4d0, 0x00000030
	MXC_DCD_ITEM 20, IOMUXC_BASE_ADDR + 0x4d4, 0x00000030
	MXC_DCD_ITEM 21, IOMUXC_BASE_ADDR + 0x4d8, 0x00000030

; data 
	MXC_DCD_ITEM 22, IOMUXC_BASE_ADDR + 0x760, 0x00020000

	MXC_DCD_ITEM 23, IOMUXC_BASE_ADDR + 0x764, 0x00000030
	MXC_DCD_ITEM 24, IOMUXC_BASE_ADDR + 0x770, 0x00000030
	MXC_DCD_ITEM 25, IOMUXC_BASE_ADDR + 0x778, 0x00000030
	MXC_DCD_ITEM 26, IOMUXC_BASE_ADDR + 0x77c, 0x00000030
	MXC_DCD_ITEM 27, IOMUXC_BASE_ADDR + 0x780, 0x00000030
	MXC_DCD_ITEM 28, IOMUXC_BASE_ADDR + 0x784, 0x00000030
	MXC_DCD_ITEM 29, IOMUXC_BASE_ADDR + 0x78c, 0x00000030
	MXC_DCD_ITEM 30, IOMUXC_BASE_ADDR + 0x748, 0x00000030

	MXC_DCD_ITEM 31, IOMUXC_BASE_ADDR + 0x470, 0x00000030
	MXC_DCD_ITEM 32, IOMUXC_BASE_ADDR + 0x474, 0x00000030
	MXC_DCD_ITEM 33, IOMUXC_BASE_ADDR + 0x478, 0x00000030
	MXC_DCD_ITEM 34, IOMUXC_BASE_ADDR + 0x47c, 0x00000030
	MXC_DCD_ITEM 35, IOMUXC_BASE_ADDR + 0x480, 0x00000030
	MXC_DCD_ITEM 36, IOMUXC_BASE_ADDR + 0x484, 0x00000030
	MXC_DCD_ITEM 37, IOMUXC_BASE_ADDR + 0x488, 0x00000030
	MXC_DCD_ITEM 38, IOMUXC_BASE_ADDR + 0x48c, 0x00000030

; calibrations 
; ZQ 
	MXC_DCD_ITEM 39, MMDC_P0_BASE_ADDR + 0x800, 0xA1390003
	MXC_DCD_ITEM 40, MMDC_P1_BASE_ADDR + 0x800, 0xA1390003

; write leveling 
	MXC_DCD_ITEM 41, MMDC_P0_BASE_ADDR + 0x80c, 0x002C0030
	MXC_DCD_ITEM 42, MMDC_P0_BASE_ADDR + 0x810, 0x001C0022

	MXC_DCD_ITEM 43, MMDC_P1_BASE_ADDR + 0x80c, 0x002E0031
	MXC_DCD_ITEM 44, MMDC_P1_BASE_ADDR + 0x810, 0x003A004A

; DQS gating, read delay, write delay calibration values based on calibration compare of 0x00ffff00 
	MXC_DCD_ITEM 45, MMDC_P0_BASE_ADDR + 0x83c, 0x420A0207
	MXC_DCD_ITEM 46, MMDC_P0_BASE_ADDR + 0x840, 0x01710177
	MXC_DCD_ITEM 47, MMDC_P1_BASE_ADDR + 0x83c, 0x42160222
	MXC_DCD_ITEM 48, MMDC_P1_BASE_ADDR + 0x840, 0x02010213

; read calibration                       
	MXC_DCD_ITEM 49, MMDC_P0_BASE_ADDR + 0x848, 0x484B4A48
	MXC_DCD_ITEM 50, MMDC_P1_BASE_ADDR + 0x848, 0x4B4F4C49

; write calibration                      
	MXC_DCD_ITEM 51, MMDC_P0_BASE_ADDR + 0x850, 0x412A262B
	MXC_DCD_ITEM 52, MMDC_P1_BASE_ADDR + 0x850, 0x2E2F2F2C

; read data bit delay: (3 is the recommended default value, although out of reset value is 0) 
	MXC_DCD_ITEM 53, MMDC_P0_BASE_ADDR + 0x81c, 0x33333333
	MXC_DCD_ITEM 54, MMDC_P0_BASE_ADDR + 0x820, 0x33333333
	MXC_DCD_ITEM 55, MMDC_P0_BASE_ADDR + 0x824, 0x33333333
	MXC_DCD_ITEM 56, MMDC_P0_BASE_ADDR + 0x828, 0x33333333
	MXC_DCD_ITEM 57, MMDC_P1_BASE_ADDR + 0x81c, 0x33333333
	MXC_DCD_ITEM 58, MMDC_P1_BASE_ADDR + 0x820, 0x33333333
	MXC_DCD_ITEM 59, MMDC_P1_BASE_ADDR + 0x824, 0x33333333
	MXC_DCD_ITEM 60, MMDC_P1_BASE_ADDR + 0x828, 0x33333333

; complete calibration by forced measurment 
	MXC_DCD_ITEM 61, MMDC_P0_BASE_ADDR + 0x8b8, 0x00000800
	MXC_DCD_ITEM 62, MMDC_P1_BASE_ADDR + 0x8b8, 0x00000800

; MMDC init 
; in DDR3, 64-bit mode, only MMDC0 is initiated 
	MXC_DCD_ITEM 63, MMDC_P0_BASE_ADDR + 0x004, 0x00020036
	MXC_DCD_ITEM 64, MMDC_P0_BASE_ADDR + 0x008, 0x09333030
	MXC_DCD_ITEM 65, MMDC_P0_BASE_ADDR + 0x00c, 0x555A79A4
	MXC_DCD_ITEM 66, MMDC_P0_BASE_ADDR + 0x010, 0xDB538F63
	MXC_DCD_ITEM 67, MMDC_P0_BASE_ADDR + 0x014, 0x01ff00db
	MXC_DCD_ITEM 68, MMDC_P0_BASE_ADDR + 0x018, 0x00081740

	MXC_DCD_ITEM 69, MMDC_P0_BASE_ADDR + 0x01c, 0x00008000

	MXC_DCD_ITEM 70, MMDC_P0_BASE_ADDR + 0x02c, 0x000026d2
	MXC_DCD_ITEM 71, MMDC_P0_BASE_ADDR + 0x030, 0x005a0e21
	MXC_DCD_ITEM 72, MMDC_P0_BASE_ADDR + 0x040, 0x00000027

	MXC_DCD_ITEM 73, MMDC_P0_BASE_ADDR + 0x000, 0x831a0000

; Initialize 1GB DDR3 - Micron MT41J128M 
	MXC_DCD_ITEM 74, MMDC_P0_BASE_ADDR + 0x01c, 0x04008032
	MXC_DCD_ITEM 75, MMDC_P0_BASE_ADDR + 0x01c, 0x00008033
	MXC_DCD_ITEM 76, MMDC_P0_BASE_ADDR + 0x01c, 0x00048031
	MXC_DCD_ITEM 77, MMDC_P0_BASE_ADDR + 0x01c, 0x09308030

; DDR device ZQ calibration 
	MXC_DCD_ITEM 78, MMDC_P0_BASE_ADDR + 0x01c, 0x04008040
	MXC_DCD_ITEM 79, MMDC_P0_BASE_ADDR + 0x01c, 0x04008048

; final DDR setup, before operation start 
	MXC_DCD_ITEM 80, MMDC_P0_BASE_ADDR + 0x020, 0x00005800
	MXC_DCD_ITEM 81, MMDC_P0_BASE_ADDR + 0x818, 0x00011117
	MXC_DCD_ITEM 82, MMDC_P1_BASE_ADDR + 0x818, 0x00011117

	MXC_DCD_ITEM 83, MMDC_P0_BASE_ADDR + 0x004, 0x00025576
	MXC_DCD_ITEM 84, MMDC_P0_BASE_ADDR + 0x404, 0x00011006
	MXC_DCD_ITEM 85, MMDC_P0_BASE_ADDR + 0x01c, 0x00000000

; enable AXI cache for VDOA/VPU/IPU 
	MXC_DCD_ITEM 86, IOMUXC_BASE_ADDR + 0x010, 0xf00000ff
; set IPU AXI-id0 Qos=0xf(bypass) AXI-id1 Qos=0x7 
	MXC_DCD_ITEM 87, IOMUXC_BASE_ADDR + 0x018, 0x007f007f
	MXC_DCD_ITEM 88, IOMUXC_BASE_ADDR + 0x01c, 0x007f007f

DCD_ADDR_END

    LTORG

; Reserved region for CSF data
CSF_ADDR
    RESERVE_CSF     (IMAGE_HDR_SIZE - (CSF_ADDR - IVT_ADDR))

    END 