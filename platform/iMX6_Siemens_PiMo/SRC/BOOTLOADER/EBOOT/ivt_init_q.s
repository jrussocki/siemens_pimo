;------------------------------------------------------------------------------
;
;  Copyright (C) 2012, Adeneo Embedded All Rights Reserved.
;  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
;  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
;
;   Copyright (C) 2011, Freescale Semiconductor, Inc. All Rights Reserved.
;   THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
;   AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
;
;------------------------------------------------------------------------------
;
;   FILE:   ivt_init.s
;   
;   First instruction to be executed is this jump instruction, control 
;   is passed to the entry point of eboot from this function
;   This object file should be located at first address of Storage.
;------------------------------------------------------------------------------

    INCLUDE mx6_base_regs.inc
    INCLUDE mx6_base_mem.inc
    INCLUDE image_cfg.inc

IRAM_FREE_SPACE_START   EQU     0x00907000      ; Free space, IRAM_START + 7 * 4K
NFC_BUFFER_ADDR         EQU     0xF7FF0000      ; TODO: 
IMAGE_HDR_SIZE          EQU     0x1000          ; Reserve 4K for plugin code
IVT_HEADER              EQU     0x402000D1      ; 0xD1002040
BASE_ADDR_OFFSET_4K     EQU     0x1000
BASE_ADDR_OFFSET_1K     EQU     0x0400
BASE_ADDR_OFFSET_256    EQU     0x0100
BIN_OFFSET              EQU     0x1000          ;This offset is from bin file generation by romimage.
                                                ;We cut 4k when programming the image into sd card so 
                                                ;that we rom code can get correct ivt, when move image
                                                ;from storage to ram, we must add it back, so that all address 
                                                ;in code is correct.

;
; ROM constants
;
ROM_VER_OFFSET          EQU     0x0048
ROM_API_VECTOR_OFFSET	EQU		0x000000C0

;
; CCM constants
;
CCM_CBCDR_OFFSET        EQU     0x0014

;
; Board ID constant
;
;IIM_BOARDID_HIGH_OFFSET EQU     0x0878
;IIM_BOARDID_LOW_OFFSET  EQU     0x087C

;BOARD_REV_ARD_REVA      EQU     0x1
;BOARD_REV_ARD_REVB      EQU     0x2

; Image destination address is the ram address which is aligned with address 0 of 
; corresponding boot device. 
    IF :DEF: IMGSDMMC
; Config for SD/MMC Boot
IVT_OFFSET              EQU    BASE_ADDR_OFFSET_1K
BOOT_DATA_IMG_LEN       EQU    IMAGE_BOOT_BOOTIMAGE_SD_SIZE+IVT_OFFSET  ; 512K + OFFSET
    ELSE

    IF :DEF: IMGATA
; Config for ATA Boot
IVT_OFFSET              EQU    BASE_ADDR_OFFSET_1K
BOOT_DATA_IMG_LEN       EQU    IMAGE_BOOT_BOOTIMAGE_ATA_SIZE+IVT_OFFSET
    ELSE
    
    IF :DEF: IMGNAND
; Config for NAND Boot
IVT_OFFSET              EQU    BASE_ADDR_OFFSET_1K
BOOT_DATA_IMG_LEN       EQU    IMAGE_BOOT_BOOTIMAGE_RAM_SIZE+IVT_OFFSET
    ELSE
	
	IF :DEF: IMGNOEBOOT
IVT_OFFSET              EQU    BASE_ADDR_OFFSET_1K
BOOT_DATA_IMG_LEN       EQU    IMAGE_BOOT_NKIMAGE_SD_OFFSET+IMAGE_BOOT_NKIMAGE_SIZE+IVT_OFFSET;
	ELSE
	
; Config for SPI Boot
IVT_OFFSET              EQU    BASE_ADDR_OFFSET_1K
BOOT_DATA_IMG_LEN       EQU    IMAGE_BOOT_BOOTIMAGE_SPI_SIZE+IVT_OFFSET
;BOOT_DATA_IMG_LEN       EQU    IMAGE_BOOT_NKIMAGE_SD_OFFSET+IMAGE_BOOT_NKIMAGE_SIZE+IVT_OFFSET;
    ENDIF
    ENDIF
	ENDIF

    ENDIF
	
	IF :DEF: IMGNOEBOOT
IVT_ADDRESS             EQU    IMAGE_BOOT_NKIMAGE_RAM_PA_START - IMAGE_BOOT_NKIMAGE_SD_OFFSET + IVT_OFFSET - BIN_OFFSET + IMAGE_HDR_SIZE
	ELSE
IVT_ADDRESS             EQU    IMAGE_BOOT_BOOTIMAGE_RAM_PA_START+BIN_OFFSET   
    ENDIF   
;IVT_ADDRESS             EQU    IMAGE_BOOT_NKIMAGE_RAM_PA_START - IMAGE_BOOT_NKIMAGE_SD_OFFSET + IVT_OFFSET - BIN_OFFSET + IMAGE_HDR_SIZE
	
    MACRO
    RESERVE_CSF     $size
        LCLA    Counter
Counter     SETA    $size
        WHILE   Counter > 0
Counter     SETA    Counter - 1
            DCB 0x00 
        WEND
    MEND

    OPT 2                               ; disable listing
    INCLUDE kxarm.h
    INCLUDE armmacros.s
    OPT 1                               ; reenable listing

    ARM

    SECURETEXT
    ALIGN 4
    ; IVT (Image Vector Table) format
    ;
    ;   0x00    HEADER (tag, length, version)
    ;   0x04    ENTRY ADDRESS
    ;   0x08    RESERVED1
    ;   0x0C    DCD ADDRESS
    ;   0x10    BOOT DATA ADDRESS
    ;   0X14    SELF ADDRESS
    ;   0X18    CSF ADDRESS
    ;   0X1C    RESERVED2

IVT_ADDR

    DCD IVT_HEADER                                                  ; HEADER
    DCD IRAM_FREE_SPACE_START+IVT_OFFSET+(PLUGIN_ENTRY-IVT_ADDR)    ; PLUGIN ENTRY POINT
    DCD 0                                                           ; RESERVED1
    DCD 0                                                           ; DCD ADDRESS
    DCD IRAM_FREE_SPACE_START+IVT_OFFSET+(BOOT_DATA_ADDR-IVT_ADDR)  ; BOOT DATA ADDRESS
    DCD IRAM_FREE_SPACE_START+IVT_OFFSET                            ; SELF ADDRESS
    DCD 0                                                           ; CSF ADDRESS
    DCD 0                                                           ; RESERVED2


; Second IVT to give entry point into the bootloader copied to DDR 
IVT2_ADDR

	; Boot NK witout eboot
	IF :DEF: IMGNOEBOOT     
	
    DCD IVT_HEADER                                                  ; HEADER
    DCD IMAGE_BOOT_NKIMAGE_RAM_PA_START                         ; IMAGE START ADDRESS FOR EXECUTE
	DCD 0                                                           ; RESERVED1
    DCD 0                                                           ; DCD ADDRESS
    DCD IRAM_FREE_SPACE_START+IVT_OFFSET+(BOOT_DATA2_ADDR-IVT_ADDR) ; BOOT DATA ADDRESS
    DCD IRAM_FREE_SPACE_START+IVT_OFFSET+(IVT2_ADDR-IVT_ADDR)       ; SELF ADDRESS
    DCD 0                                                           ; CSF ADDRESS
    DCD 0                                                           ; RESERVED
	
	ELSE 
	
	DCD IVT_HEADER                                                  ; HEADER
    DCD IMAGE_BOOT_BOOTIMAGE_RAM_PA_START+BIN_OFFSET+IMAGE_HDR_SIZE ; IMAGE START ADDRESS FOR EXECUTE
	DCD 0                                                           ; RESERVED1
    DCD 0                                                           ; DCD ADDRESS
    DCD IRAM_FREE_SPACE_START+IVT_OFFSET+(BOOT_DATA2_ADDR-IVT_ADDR) ; BOOT DATA ADDRESS
    DCD IRAM_FREE_SPACE_START+IVT_OFFSET+(IVT2_ADDR-IVT_ADDR)       ; SELF ADDRESS
    DCD 0                                                           ; CSF ADDRESS
    DCD 0                                                           ; RESERVED
	
	ENDIF

    ; IVT boot data format
    ;   0x00    IMAGE START ADDR
    ;   0x04    IMAGE SIZE
    ;   0x08    PLUGIN FLAG    
BOOT_DATA_ADDR

    DCD IRAM_FREE_SPACE_START       ; PLUGIN DESTINTATION ADDRESS 
    DCD IMAGE_HDR_SIZE              ; PLUGIN SIZE
    DCD 1                           ; PLUGIN FLAG

DDR_DEST_ADDR   DCD   (IVT_ADDRESS - IVT_OFFSET)
COPY_SIZE       DCD   BOOT_DATA_IMG_LEN

    ; IVT boot data format
    ;   0x00    IMAGE START ADDR
    ;   0x04    IMAGE SIZE
    ;   0x08    PLUGIN FLAG    
BOOT_DATA2_ADDR

    DCD IVT_ADDRESS - IVT_OFFSET    ; START OF DESTINTATION ADDRESS
    DCD BOOT_DATA_IMG_LEN           ; IMAGE SIZE
    DCD 0                           ; PLUGIN FLAG

    ; ROM uses pu_irom_run_plugin to execute plugin.  The plugin is called
    ; as a function pointer with the following prototype
    ;   BOOL *(plugin_f)(void **start, size_t *bytes, UINT32 *ivt_offset)
PLUGIN_ENTRY

    ; Save the return address and plugin function parameters 
    ; (saved on ROM code stack)
    ;
    ;   r0 = **start
    ;   r1 = *bytes
    ;   r2 = *ivt_offset
    ;

    ; enters at 0x907460
    ; starts at offset 0x60 in eboot.nb0
    ; processor is in arm-encoded instruction mode

    PUSHSTACK2LR_ORIG R0, R7

    ;We should distinguish USB recovery mode(SDP mode) and internal boot mode.
    ;If ROM runs in SDP mode, then it needn't load boot code from storage media.
    ;If ROM runs in SDP mode, then r0 must be 0x00
    ;If ROM runs in internal boot mode, then r0 should be larger than IRAM base address.

    mov     r7, r0
    cmp     r0, #0x00900000   ; check if in iram        00900000 - 0093FFFF 256K IRAM

    ldr r3, DDR_DEST_ADDR
    strhi r3, [r0]              ; STR if HI
    ldr r3, =BOOT_DATA_IMG_LEN
    strhi r3, [r1]
    ldr r3, =(IVT_OFFSET+(IVT2_ADDR-IVT_ADDR))
    strhi r3, [r2]              ; change content in location pointed by r0~r2

    ; Save the necessary address.
    adr r4, DDR_DEST_ADDR
    adr r5, COPY_SIZE
    adr r6, BOOT_DATA2_ADDR

    ; Check ROM ID to determine silicon revision
    ;ldr     r0, =CSP_BASE_MEM_PA_ROM
    ;ldr     r3, [r0, #ROM_VER_OFFSET]
    ;cmp     r3, #0x20               ;check if it's TO1 silicon

    ; Disable WDOG
    ; setmem /16 0x020bc000 = 0x30
    ;ldr     r0, =CSP_BASE_REG_PA_WDOG1
    ;ldr     r1, =0x30
    ;ldr     r2, =0x0
    ;str     r1, [r0, r2]
   
    ; Configure CCM // Enable all clocks (they are disabled by ROM code)
    ldr     r0, =CSP_BASE_REG_PA_CCM

    ; setmem /32 0x020c4068 = 0xffffffff
    ldr     r1, =0xffffffff
    ldr     r2, =0x068
    str     r1, [r0, r2]

    ; setmem /32 0x020c406c = 0xffffffff
    ldr     r1, =0xffffffff
    ldr     r2, =0x06c
    str     r1, [r0, r2]

    ; setmem /32 0x020c4070 = 0xffffffff
    ldr     r1, =0xffffffff
    ldr     r2, =0x070
    str     r1, [r0, r2]

    ; setmem /32 0x020c4074 = 0xffffffff
    ldr     r1, =0xffffffff
    ldr     r2, =0x074
    str     r1, [r0, r2]

    ; setmem /32 0x020c4078 = 0xffffffff
    ldr     r1, =0xffffffff
    ldr     r2, =0x078
    str     r1, [r0, r2]

    ; setmem /32 0x020c407c = 0xffffffff
    ldr     r1, =0xffffffff
    ldr     r2, =0x07c
    str     r1, [r0, r2]

    ; setmem /32 0x020c4080 = 0xffffffff
    ldr     r1, =0xffffffff
    ldr     r2, =0x080
    str     r1, [r0, r2]

    ; setmem /32 0x020c4084 = 0xffffffff
    ldr     r1, =0xffffffff
    ldr     r2, =0x084
    str     r1, [r0, r2]

DDR3_ENTRY

    ; Initialization for DDR3 Memory
    ; DDR3 IOMUX configuration
    ldr     r0, =CSP_BASE_REG_PA_IOMUXC
    ; Global pad control options

    ; setmem /32 0x020e05a8 = 0x00000030 // IOMUXC_SW_PAD_CTL_PAD_DRAM_SDQS0 - DSE=110
    ldr     r1, =0x00000030
    ldr     r2, =0x5a8
    str     r1, [r0, r2]

    ; setmem /32 0x020e05b0 = 0x00000030 // IOMUXC_SW_PAD_CTL_PAD_DRAM_SDQS1 - DSE=110
    ldr     r1, =0x00000030
    ldr     r2, =0x5b0
    str     r1, [r0, r2]

    ; setmem /32 0x020e0524 = 0x00000030 // IOMUXC_SW_PAD_CTL_PAD_DRAM_SDQS2 - DSE=110
    ldr     r1, =0x00000030
    ldr     r2, =0x524
    str     r1, [r0, r2]

    ; setmem /32 0x020e051c = 0x00000030 // IOMUXC_SW_PAD_CTL_PAD_DRAM_SDQS3 - DSE=110
    ldr     r1, =0x00000030
    ldr     r2, =0x51c
    str     r1, [r0, r2]

    ; setmem /32 0x020e0518 = 0x00000030 // IOMUXC_SW_PAD_CTL_PAD_DRAM_SDQS4 - DSE=110
    ldr     r1, =0x00000030
    ldr     r2, =0x518
    str     r1, [r0, r2]

    ; setmem /32 0x020e050c = 0x00000030 // IOMUXC_SW_PAD_CTL_PAD_DRAM_SDQS5 - DSE=110
    ldr     r1, =0x00000030
    ldr     r2, =0x50c
    str     r1, [r0, r2]

    ; setmem /32 0x020e05b8 = 0x00000030 // IOMUXC_SW_PAD_CTL_PAD_DRAM_SDQS6 - DSE=110
    ldr     r1, =0x00000030
    ldr     r2, =0x5b8
    str     r1, [r0, r2]

    ; setmem /32 0x020e05c0 = 0x00000030 // IOMUXC_SW_PAD_CTL_PAD_DRAM_SDQS7 - DSE=110
    ldr     r1, =0x00000030
    ldr     r2, =0x5c0
    str     r1, [r0, r2]

    ; setmem /32 0x020e05ac = 0x00020030 // IOMUXC_SW_PAD_CTL_PAD_DRAM_DQM0 - DSE=110, DDR_INPUT=1, HYS=0
    ldr     r1, =0x00020030
    ldr     r2, =0x5ac
    str     r1, [r0, r2]

    ; setmem /32 0x020e05b4 = 0x00020030 // IOMUXC_SW_PAD_CTL_PAD_DRAM_DQM1 - DSE=110, DDR_INPUT=1, HYS=0
    ldr     r1, =0x00020030
    ldr     r2, =0x5b4
    str     r1, [r0, r2]

    ; setmem /32 0x020e0528 = 0x00020030 // IOMUXC_SW_PAD_CTL_PAD_DRAM_DQM2 - DSE=110, DDR_INPUT=1, HYS=0
    ldr     r1, =0x00020030
    ldr     r2, =0x528
    str     r1, [r0, r2]

    ; setmem /32 0x020e0520 = 0x00020030 // IOMUXC_SW_PAD_CTL_PAD_DRAM_DQM3 - DSE=110, DDR_INPUT=1, HYS=0
    ldr     r1, =0x00020030
    ldr     r2, =0x520
    str     r1, [r0, r2]

    ; setmem /32 0x020e0514 = 0x00020030 // IOMUXC_SW_PAD_CTL_PAD_DRAM_DQM4 - DSE=110, DDR_INPUT=1, HYS=0
    ldr     r1, =0x00020030
    ldr     r2, =0x514
    str     r1, [r0, r2]

    ; setmem /32 0x020e0510 = 0x00020030 // IOMUXC_SW_PAD_CTL_PAD_DRAM_DQM5 - DSE=110, DDR_INPUT=1, HYS=0
    ldr     r1, =0x00020030
    ldr     r2, =0x510
    str     r1, [r0, r2]

    ; setmem /32 0x020e05bc = 0x00020030 // IOMUXC_SW_PAD_CTL_PAD_DRAM_DQM6 - DSE=110, DDR_INPUT=1, HYS=0
    ldr     r1, =0x00020030
    ldr     r2, =0x5bc
    str     r1, [r0, r2]

    ; setmem /32 0x020e05c4 = 0x00020030 // IOMUXC_SW_PAD_CTL_PAD_DRAM_DQM7 - DSE=110, DDR_INPUT=1, HYS=0
    ldr     r1, =0x00020030
    ldr     r2, =0x5c4
    str     r1, [r0, r2]

    ; setmem /32 0x020e056c = 0x00020030 // IOMUXC_SW_PAD_CTL_PAD_DRAM_CAS - DSE=110, DDR_INPUT=1, HYS=0
    ldr     r1, =0x00020030
    ldr     r2, =0x56c
    str     r1, [r0, r2]

    ; setmem /32 0x020e0578 = 0x00020030 // IOMUXC_SW_PAD_CTL_PAD_DRAM_RAS - DSE=110, DDR_INPUT=1, HYS=0
    ldr     r1, =0x00020030
    ldr     r2, =0x578
    str     r1, [r0, r2]

    ; setmem /32 0x020e0588 = 0x00020030 // IOMUXC_SW_PAD_CTL_PAD_DRAM_SDCLK_0 - DSE=110, DDR_INPUT=1, HYS=0
    ldr     r1, =0x00020030
    ldr     r2, =0x588
    str     r1, [r0, r2]

    ; setmem /32 0x020e0594 = 0x00020030 // IOMUXC_SW_PAD_CTL_PAD_DRAM_SDCLK_1 - DSE=110, DDR_INPUT=1, HYS=0
    ldr     r1, =0x00020030
    ldr     r2, =0x594
    str     r1, [r0, r2]

    ; setmem /32 0x020e057c = 0x00020030 // IOMUXC_SW_PAD_CTL_PAD_DRAM_RESET - DSE=110, DDR_INPUT=1, HYS=0, DDR_SEL=00
    ldr     r1, =0x00020030
    ldr     r2, =0x57c
    str     r1, [r0, r2]

    ; setmem /32 0x020e0590 = 0x00003000 // IOMUXC_SW_PAD_CTL_PAD_DRAM_SDCKE0
    ldr     r1, =0x00003000
    ldr     r2, =0x590
    str     r1, [r0, r2]

    ; setmem /32 0x020e0598 = 0x00003000 // IOMUXC_SW_PAD_CTL_PAD_DRAM_SDCKE1
    ldr     r1, =0x00003000
    ldr     r2, =0x598
    str     r1, [r0, r2]

    ; setmem /32 0x020e058c = 0x00000000 // IOMUXC_SW_PAD_CTL_PAD_DRAM_SDBA2
    ldr     r1, =0x00000000
    ldr     r2, =0x58c
    str     r1, [r0, r2]

    ; setmem /32 0x020e059c = 0x00003030 // IOMUXC_SW_PAD_CTL_PAD_DRAM_SDODT0
    ldr     r1, =0x00003030
    ldr     r2, =0x59c
    str     r1, [r0, r2]

    ; setmem /32 0x020e05a0 = 0x00003030 // IOMUXC_SW_PAD_CTL_PAD_DRAM_SDODT1
    ldr     r1, =0x00003030
    ldr     r2, =0x5a0
    str     r1, [r0, r2]

    ; setmem /32 0x020e0784 = 0x00000030 // IOMUXC_SW_PAD_CTL_GRP_B0DS - DSE=110
    ldr     r1, =0x00000030
    ldr     r2, =0x784
    str     r1, [r0, r2]

    ; setmem /32 0x020e0788 = 0x00000030 // IOMUXC_SW_PAD_CTL_GRP_B1DS - DSE=110
    ldr     r1, =0x00000030
    ldr     r2, =0x788
    str     r1, [r0, r2]

    ; setmem /32 0x020e0794 = 0x00000030 // IOMUXC_SW_PAD_CTL_GRP_B2DS - DSE=110
    ldr     r1, =0x00000030
    ldr     r2, =0x794
    str     r1, [r0, r2]

    ; setmem /32 0x020e079c = 0x00000030 // IOMUXC_SW_PAD_CTL_GRP_B3DS - DSE=110
    ldr     r1, =0x00000030
    ldr     r2, =0x79c
    str     r1, [r0, r2]

    ; setmem /32 0x020e07a0 = 0x00000030 // IOMUXC_SW_PAD_CTL_GRP_B4DS - DSE=110
    ldr     r1, =0x00000030
    ldr     r2, =0x7a0
    str     r1, [r0, r2]

    ; setmem /32 0x020e07a4 = 0x00000030 // IOMUXC_SW_PAD_CTL_GRP_B5DS - DSE=110
    ldr     r1, =0x00000030
    ldr     r2, =0x7a4
    str     r1, [r0, r2]

    ; setmem /32 0x020e07a8 = 0x00000030 // IOMUXC_SW_PAD_CTL_GRP_B6DS - DSE=110
    ldr     r1, =0x00000030
    ldr     r2, =0x7a8
    str     r1, [r0, r2]

    ; setmem /32 0x020e0748 = 0x00000030 // IOMUXC_SW_PAD_CTL_GRP_B7DS - DSE=110
    ldr     r1, =0x00000030
    ldr     r2, =0x748
    str     r1, [r0, r2]

    ; setmem /32 0x020e074c = 0x00000030 // IOMUXC_SW_PAD_CTL_GRP_ADDDS - DSE=110
    ldr     r1, =0x00000030
    ldr     r2, =0x74c
    str     r1, [r0, r2]

    ; setmem /32 0x020e0750 = 0x00020000 // IOMUXC_SW_PAD_CTL_GRP_DDRMODE_CTL - DDR_INPUT=1
    ldr     r1, =0x00020000
    ldr     r2, =0x750
    str     r1, [r0, r2]

    ; setmem /32 0x020e0758 = 0x00000000 // IOMUXC_SW_PAD_CTL_GRP_DDRPKE - PKE=0
    ldr     r1, =0x00000000
    ldr     r2, =0x758
    str     r1, [r0, r2]

    ; setmem /32 0x020e0774 = 0x00020000 // IOMUXC_SW_PAD_CTL_GRP_DDRMODE- DDR_INPUT=1
    ldr     r1, =0x00020000
    ldr     r2, =0x774
    str     r1, [r0, r2]

    ; setmem /32 0x020e078c = 0x00000030 // IOMUXC_SW_PAD_CTL_GRP_CTLDS - DSE=110
    ldr     r1, =0x00000030
    ldr     r2, =0x78c
    str     r1, [r0, r2]

    ; setmem /32 0x020e0798 = 0x000c0000 // IOMUXC_SW_PAD_CTL_GRP_DDR_TYPE - DDR_SEL=00 
    ldr     r1, =0x000c0000
    ldr     r2, =0x798
    str     r1, [r0, r2]

    ;;;;;;;;;;;;;;
    ; DDR3 Memory Initialization
    ;;;;;;;;;;;;;;
    ldr     r0, =CSP_BASE_REG_PA_MMDC0
    ; Initialize 2GB DDR3 - Micron MT41J128M

     ; setmem /32 0x021b081c = 0x33333333      // DDR_PHY_P0_MPREDQBY0DL3
    ldr     r1, =0x33333333
    str     r1, [r0, #0x081c]

    ; setmem /32 0x021b0820 = 0x33333333      // DDR_PHY_P0_MPREDQBY1DL3
    ldr     r1, =0x33333333
    str     r1, [r0, #0x0820]

    ; setmem /32 0x021b0824 = 0x33333333      // DDR_PHY_P0_MPREDQBY2DL3
    ldr     r1, =0x33333333
    str     r1, [r0, #0x0824]

    ; setmem /32 0x021b0828 = 0x33333333      // DDR_PHY_P0_MPREDQBY3DL3
    ldr     r1, =0x33333333
    str     r1, [r0, #0x0828]
    
    ; setmem /32 0x021b481c = 0x33333333      // DDR_PHY_P1_MPREDQBY0DL3
    ldr     r1, =0x33333333
    ldr     r2, =0x481c
    str     r1, [r0, r2]
    
    ; setmem /32 0x021b4820 = 0x33333333      // DDR_PHY_P1_MPREDQBY1DL3
    ldr     r1, =0x33333333
    ldr     r2, =0x4820
    str     r1, [r0, r2]

    ; setmem /32 0x021b4824 = 0x33333333      // DDR_PHY_P1_MPREDQBY2DL3
    ldr     r1, =0x33333333
    ldr     r2, =0x4824
    str     r1, [r0, r2]
    
    ; setmem /32 0x021b4828 = 0x33333333      // DDR_PHY_P1_MPREDQBY3DL3
    ldr     r1, =0x33333333
    ldr     r2, =0x4828
    str     r1, [r0, r2]
    
    ; setmem /32 0x021b0018 = 0x00081740      // MMDC0_MDMISC
    ldr     r1, =0x00081740
    str     r1, [r0, #0x0018]

    ; setmem /32 0x021b001c = 0x00008000      // MMDC0_MDSCR
    ldr     r1, =0x00008000
    str     r1, [r0, #0x001c]

    ;; setmem /32 0x021b000c = 0x555b99a4      // MMDC0_MDCFG0 - tCL - 7ck; tFAW - 27ck; tXPDLL - 13ck (24ns); tXP - 5ck (7.5ns); tXS - 92ck; tRFC - 86ck
    ; setmem /32 0x021b000c = 0x555A7975      // MMDC0_MDCFG0 see spread sheet for timings
    ldr     r1, =0x555A7975
    str     r1, [r0, #0x000c]

    ;; setmem /32 0x021b0010 = 0xfe730e64      // MMDC0_MDCFG1 - tWCL - 6ck; tMRD - 4ck; tWR - 8ck; tRPA = 0; tRAS - 20ck; tRC - 28ck; tRP - 8ck; tRCD - 8ck 
    ; setmem /32 0x021b0010 = 0xFF538E64      // MMDC0_MDCFG1 see spread sheet for timings
    ldr     r1, =0xFF538E64
    str     r1, [r0, #0x0010]

    ; setmem /32 0x021b0014 = 0x01ff00db      // MMDC0_MDCFG2 - tRRD - 4ck; tWTR - 4ck; tRTP - 4ck; tDLLK - 512ck
    ldr     r1, =0x01ff00db
    str     r1, [r0, #0x0014]

    ; setmem /32 0x021b002c = 0x000026d2      // MMDC0_MDRWD
    ldr     r1, =0x000026d2
    str     r1, [r0, #0x002c]

    ; setmem /32 0x021b0030 = 0x005b0e21      // MMDC0_MDOR - tXPR - 92ck; SDE_to_RST - 13ck; RST_to_CKE - 32ck
    ldr     r1, =0x005b0e21
    str     r1, [r0, #0x0030]

    ;; setmem /32 0x021b0008 = 0x1b334000      // MMDC0_MDOTC
    ; setmem /32 0x021b0008 = 0x094444040       // MMDC0_MDOTC see spread sheet for timings
    ldr     r1, =0x094444040
    str     r1, [r0, #0x0008]

    ;; setmem /32 0x021b0004 = 0x0003002d      // MMDC0_MDPDC
    ; setmem /32 0x021b0004 = 0x00020036        // MMDC0_MDPDC see spread sheet for timings
    ldr     r1, =0x0003002d
    str     r1, [r0, #0x0004]

    ; setmem /32 0x021b0040 = 0x00000027      // MMDC0_MDASP CS0_END - 0x4fffffff 
    ldr     r1, =0x00000027
    str     r1, [r0, #0x0040]

    ; setmem /32 0x021b0000 = 0xc81a0000      // MMDC0_MDCTL - row - 14bits; col = 10bits; burst length 8; 64-bit data bus
    ldr     r1, =0xc31a0000
    str     r1, [r0, #0x0000]

    ; setmem /32 0x021b001c = 0x04088032      // MMDC0_MDSCR
    ldr     r1, =0x04088032
    str     r1, [r0, #0x001c]

    ; setmem /32 0x021b001c = 0x0408803a      // MMDC0_MDSCR
    ldr     r1, =0x0408803a
    str     r1, [r0, #0x001c]

    ; setmem /32 0x021b001c = 0x00008033      // MMDC0_MDSCR
    ldr     r1, =0x00008033
    str     r1, [r0, #0x001c]

    ; setmem /32 0x021b001c = 0x0000803b      // MMDC0_MDSCR
    ldr     r1, =0x0000803b
    str     r1, [r0, #0x001c]

    ; setmem /32 0x021b001c = 0x00428031      // MMDC0_MDSCR
    ldr     r1, =0x00428031
    str     r1, [r0, #0x001c]

    ; setmem /32 0x021b001c = 0x00428039      // MMDC0_MDSCR
    ldr     r1, =0x00428039
    str     r1, [r0, #0x001c]

    ;; setmem /32 0x021b001c = 0x09308030      // MMDC0_MDSCR
    ; setmem /32 0x021b001c = 0x09408030      // MMDC0_MDSCR, CL=0x8, 8 clks
    ldr     r1, =0x09408030
    str     r1, [r0, #0x001c]

    ;; setmem /32 0x021b001c = 0x09308038      // MMDC0_MDSCR
    ; setmem /32 0x021b001c = 0x09408038      // MMDC0_MDSCR, CL=0x8, 8 clks
    ldr     r1, =0x09408038
    str     r1, [r0, #0x001c]
    
    ; setmem /32 0x021b001c = 0x04008040      // MMDC0_MDSCR
    ldr     r1, =0x04008040
    str     r1, [r0, #0x001c]

    ; setmem /32 0x021b001c = 0x04008048      // MMDC0_MDSCR
    ldr     r1, =0x04008048
    str     r1, [r0, #0x001c]

    ; setmem /32 0x021b0800 = 0xa5380003      // DDR_PHY_P0_MPZQHWCTRL
    ldr     r1, =0xa5380003
    str     r1, [r0, #0x0800]

    ; setmem /32 0x021b4800 = 0xa5380003      // DDR_PHY_P1_MPZQHWCTRL
    ldr     r1, =0xa5380003
    ldr     r2, =0x4800
    str     r1, [r0, r2]

    ; setmem /32 0x021b0020 = 0x00005800      // MMDC0_MDREF
    ldr     r1, =0x00005800
    str     r1, [r0, #0x0020]

    ; setmem /32 0x021b0818 = 0x00022227      // DDR_PHY_P0_MPODTCTRL
    ldr     r1, =0x00022227
    str     r1, [r0, #0x0818]

    ; setmem /32 0x021b4818 = 0x00022227     // DDR_PHY_P1_MPODTCTRL
    ldr     r1, =0x00022227
    ldr     r2, =0x4818
    str     r1, [r0, r2]

    ; calibrated at 528MHz
    ; setmem /32 0x021b083c = 0x433f033f        //MMDC0_MPDGCTRL0 , add 1/2 cycle delay to DQS gating
    ldr     r1, =0x433f033f
    str     r1, [r0, #0x083c]

    ; setmem /32 0x021b0840 = 0x033f033f  //MMDC0_MPDGCTRL1, add 1/2 cycle delay to DQS gating
    ldr     r1, =0x033f033f
    str     r1, [r0, #0x0840]
    
    ; setmem /32 0x021b483c = 0x433f033f  //MMDC1_MPDGCTRL0, add 1/2 cycle delay to DQS gating
    ldr     r1, =0x433f033f
    ldr     r2, =0x483c
    str     r1, [r0, r2]

    ; setmem /32 0x021b4840 = 0x0344033b  //MMDC1_MPDGCTRL1, add 1/2 cycle delay to DQS gating
    ldr     r1, =0x0344033b
    ldr     r2, =0x4840
    str     r1, [r0, r2]

    ; setmem /32 0x021b0848 = 0x4337373e
    ldr     r1, =0x4337373e
    str     r1, [r0, #0x0848]
    
    ; setmem /32 0x021b4848 = 0x3634303d
    ldr     r1, =0x3634303d
    ldr     r2, =0x4848
    str     r1, [r0, r2]

    ; setmem /32 0x021b0850 = 0x35374640
    ldr     r1, =0x35374640
    str     r1, [r0, #0x0850]
    
    ; setmem /32 0x021b4850 = 0x4a294b35
    ldr     r1, =0x4a294b35
    ldr     r2, =0x4850
    str     r1, [r0, r2]

    ; // write leveling
    ; setmem /32 0x021b080c = 0x001F001F
    ldr     r1, =0x001F001F
    str     r1, [r0, #0x080c]
    
    ; setmem /32 0x021b0810 = 0x001F001F
    ldr     r1, =0x001F001F
    str     r1, [r0, #0x0810]
    
    ; setmem /32 0x021b480c = 0x00440044
    ldr     r1, =0x00440044
    ldr     r2, =0x480c
    str     r1, [r0, r2]
    ; setmem /32 0x021b4810 = 0x00440044
    ldr     r1, =0x00440044
    ldr     r2, =0x4810
    str     r1, [r0, r2]
    
    ; setmem /32 0x021b08b8 = 0x00000800      // DDR_PHY_P0_MPMUR0, frc_msr
    ldr     r1, =0x00000800
    str     r1, [r0, #0x08b8]
    
    ; setmem /32 0x021b48b8 = 0x00000800      // DDR_PHY_P0_MPMUR0, frc_msr
    ldr     r1, =0x00000800
    ldr     r2, =0x48b8
    str     r1, [r0, r2]
    
    ; setmem /32 0x021b001c = 0x00000000      // MMDC0_MDSCR
    ldr     r1, =0x00000000
    str     r1, [r0, #0x001c]

; end of DDR3 Initialation Routine

;
;    The following is to fill in those arguments for this ROM function
;    pu_irom_hwcnfg_setup(void **start, size_t *bytes, const void *boot_data)
;
;    This function is used to copy data from the storage media into DDR.
;
;    start - Initial (possibly partial) image load address on entry.  
;            Final image load address on exit.
;    bytes - Initial (possibly partial) image size on entry.  
;            Final image size on exit.
;    boot_data - Initial @ref ivt Boot Data load address.
;
    ; Restore the necessary address.
PLUGIN_HOOK_ENTRY

    MOV R0, R4
    MOV R1, R5
    MOV R2, R6

    ;ROM API pu_irom_hwcnfg_setup(void **start, size_t *bytes, const void *boot_data)
    ;is the 3rd API in the ROM API Vector Table
    LDR R4, =ROM_API_VECTOR_OFFSET
    LDR R4, [R4, #0x8]

    ;We should distinguish USB recovery mode(SDP mode) and internal boot mode.
    ;If ROM runs in SDP mode, then it needn't load boot code from storage media.
    ;If ROM runs in SDP mode, then r0 must be 0x00
    ;If ROM runs in internal boot mode, then r0 should be larger than IRAM base address.    

    CMP R7, #0x00900000
    BLXHI R4            ; Address is subject to change in future ROM versions

    ;Workaround run plug-ins in SDP mode without USB re-enumeration.
    ;how it works:
    ;ROM running in usb download mode.
    ;Host manufacturing application sends SDP command to download plug-in image.
    ;Host manufacturing application sends SDP command to jump to plug-in image and run it.
    ;Plug-in starts execution and after its regular tasks plug-in will then call into ROM
    ;call into pl_parse_and_handle()

    ;ROM API pl_parse_and_handle()
    ;is the 2rd API in the ROM API Vector Table
    LDR R4, =ROM_API_VECTOR_OFFSET
    LDR R4, [R4, #0x4]

    CMP R7, #0x00900000
    BLXLS R4
        
; To return to ROM from plugin, we need to fill in these argument.
; Here is what need to do:
; Need to construct the paramters for this function before return to ROM:
; plugin_download(void **start, size_t *bytes, UINT32 *ivt_offset)

    ; Restore registers
    POPSTACK2LR_ORIG R0, R7

    ldr r3, DDR_DEST_ADDR
    str r3, [r0]
    ldr r3, =BOOT_DATA_IMG_LEN
    str r3, [r1]
    ldr r3, =(IVT_OFFSET+(IVT2_ADDR-IVT_ADDR))
    str r3, [r2]
    mov r0, #1

    RETURN_THUMB ; return back to ROM code

    LTORG

; Reserved region for CSF data
    SECUREDATA
    ALIGN 4
CSF_ADDR
    RESERVE_CSF     (IMAGE_HDR_SIZE - (CSF_ADDR - IVT_ADDR))

    END
