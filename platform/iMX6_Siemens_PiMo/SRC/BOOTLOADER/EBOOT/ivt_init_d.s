;------------------------------------------------------------------------------
;
;  Copyright (C) 2012, Adeneo Embedded All Rights Reserved.
;  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
;  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
;
;   Copyright (C) 2011, Freescale Semiconductor, Inc. All Rights Reserved.
;   THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
;   AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
;
;------------------------------------------------------------------------------
;
;   FILE:   ivt_init.s
;   
;   First instruction to be executed is this jump instruction, control 
;   is passed to the entry point of eboot from this function
;   This object file should be located at first address of Storage.
;------------------------------------------------------------------------------

    INCLUDE mx6_base_regs.inc
    INCLUDE mx6_base_mem.inc
    INCLUDE image_cfg.inc

IRAM_FREE_SPACE_START   EQU     0x00907000      ; Free space, IRAM_START + 7 * 4K
NFC_BUFFER_ADDR         EQU     0xF7FF0000      ; TODO: 
IMAGE_HDR_SIZE          EQU     0x1000          ; Reserve 4K for plugin code
IVT_HEADER              EQU     0x402000D1      ; 0xD1002040
BASE_ADDR_OFFSET_4K     EQU     0x1000
BASE_ADDR_OFFSET_1K     EQU     0x0400
BASE_ADDR_OFFSET_256    EQU     0x0100
BIN_OFFSET              EQU     0x1000          ;This offset is from bin file generation by romimage.
                                                ;We cut 4k when programming the image into sd card so 
                                                ;that we rom code can get correct ivt, when move image
                                                ;from storage to ram, we must add it back, so that all address 
                                                ;in code is correct.

;
; ROM constants
;
ROM_VER_OFFSET          EQU     0x0048
ROM_API_VECTOR_OFFSET	EQU		0x000000C0

;
; CCM constants
;
CCM_CBCDR_OFFSET        EQU     0x0014

;
; Board ID constant
;
;IIM_BOARDID_HIGH_OFFSET EQU     0x0878
;IIM_BOARDID_LOW_OFFSET  EQU     0x087C

;BOARD_REV_ARD_REVA      EQU     0x1
;BOARD_REV_ARD_REVB      EQU     0x2

; Image destination address is the ram address which is aligned with address 0 of 
; corresponding boot device. 
    IF :DEF: IMGSDMMC
; Config for SD/MMC Boot
IVT_OFFSET              EQU    BASE_ADDR_OFFSET_1K
BOOT_DATA_IMG_LEN       EQU    IMAGE_BOOT_BOOTIMAGE_SD_SIZE+IVT_OFFSET  ; 512K + OFFSET
    ELSE

    IF :DEF: IMGATA
; Config for ATA Boot
IVT_OFFSET              EQU    BASE_ADDR_OFFSET_1K
BOOT_DATA_IMG_LEN       EQU    IMAGE_BOOT_BOOTIMAGE_ATA_SIZE+IVT_OFFSET
    ELSE
	
    IF :DEF: IMGNAND
; Config for NAND Boot
IVT_OFFSET              EQU    BASE_ADDR_OFFSET_1K
BOOT_DATA_IMG_LEN       EQU    IMAGE_BOOT_BOOTIMAGE_RAM_SIZE+IVT_OFFSET
    ELSE
; Config for SPI Boot
IVT_OFFSET              EQU    BASE_ADDR_OFFSET_1K
BOOT_DATA_IMG_LEN       EQU    IMAGE_BOOT_BOOTIMAGE_SD_SIZE + IVT_OFFSET;IMAGE_BOOT_BOOTIMAGE_SPI_SIZE+IVT_OFFSET
    ENDIF
    ENDIF

    ENDIF
IVT_ADDRESS             EQU    IMAGE_BOOT_BOOTIMAGE_RAM_PA_START + BIN_OFFSET   
          
    MACRO
    RESERVE_CSF     $size
        LCLA    Counter
Counter     SETA    $size
        WHILE   Counter > 0
Counter     SETA    Counter - 1
            DCB 0x00 
        WEND
    MEND

    OPT 2                               ; disable listing
    INCLUDE kxarm.h
    INCLUDE armmacros.s
    OPT 1                               ; reenable listing

    ARM

    SECURETEXT
    ALIGN 4
    ; IVT (Image Vector Table) format
    ;
    ;   0x00    HEADER (tag, length, version)
    ;   0x04    ENTRY ADDRESS
    ;   0x08    RESERVED1
    ;   0x0C    DCD ADDRESS
    ;   0x10    BOOT DATA ADDRESS
    ;   0X14    SELF ADDRESS
    ;   0X18    CSF ADDRESS
    ;   0X1C    RESERVED2

IVT_ADDR

    DCD IVT_HEADER                                                  ; HEADER
    DCD IRAM_FREE_SPACE_START+IVT_OFFSET+(PLUGIN_ENTRY-IVT_ADDR)    ; PLUGIN ENTRY POINT
    DCD 0                                                           ; RESERVED1
    DCD 0                                                           ; DCD ADDRESS
    DCD IRAM_FREE_SPACE_START+IVT_OFFSET+(BOOT_DATA_ADDR-IVT_ADDR)  ; BOOT DATA ADDRESS
    DCD IRAM_FREE_SPACE_START+IVT_OFFSET                            ; SELF ADDRESS
    DCD 0                                                           ; CSF ADDRESS
    DCD 0                                                           ; RESERVED2


; Second IVT to give entry point into the bootloader copied to DDR 
IVT2_ADDR

    DCD IVT_HEADER                                                  ; HEADER
    DCD IMAGE_BOOT_BOOTIMAGE_RAM_PA_START+BIN_OFFSET+IMAGE_HDR_SIZE ; IMAGE START ADDRESS FOR EXECUTE
    DCD 0                                                           ; RESERVED1
    DCD 0                                                           ; DCD ADDRESS
    DCD IRAM_FREE_SPACE_START+IVT_OFFSET+(BOOT_DATA2_ADDR-IVT_ADDR) ; BOOT DATA ADDRESS
    DCD IRAM_FREE_SPACE_START+IVT_OFFSET+(IVT2_ADDR-IVT_ADDR)       ; SELF ADDRESS
    DCD 0                                                           ; CSF ADDRESS
    DCD 0                                                           ; RESERVED

    ; IVT boot data format
    ;   0x00    IMAGE START ADDR
    ;   0x04    IMAGE SIZE
    ;   0x08    PLUGIN FLAG    
BOOT_DATA_ADDR

    DCD IRAM_FREE_SPACE_START       ; PLUGIN DESTINTATION ADDRESS 
    DCD IMAGE_HDR_SIZE              ; PLUGIN SIZE
    DCD 1                           ; PLUGIN FLAG

DDR_DEST_ADDR   DCD   (IVT_ADDRESS - IVT_OFFSET)
COPY_SIZE       DCD   BOOT_DATA_IMG_LEN

    ; IVT boot data format
    ;   0x00    IMAGE START ADDR
    ;   0x04    IMAGE SIZE
    ;   0x08    PLUGIN FLAG    
BOOT_DATA2_ADDR

    DCD IVT_ADDRESS - IVT_OFFSET    ; START OF DESTINTATION ADDRESS
    DCD BOOT_DATA_IMG_LEN           ; IMAGE SIZE
    DCD 0                           ; PLUGIN FLAG

    ; ROM uses pu_irom_run_plugin to execute plugin.  The plugin is called
    ; as a function pointer with the following prototype
    ;   BOOL *(plugin_f)(void **start, size_t *bytes, UINT32 *ivt_offset)
PLUGIN_ENTRY
    ; Save the return address and plugin function parameters 
    ; (saved on ROM code stack)
    ;
    ;   r0 = **start
    ;   r1 = *bytes
    ;   r2 = *ivt_offset
    ;

    ; enters at 0x907460
    ; starts at offset 0x60 in eboot.nb0
    ; processor is in arm-encoded instruction mode

    PUSHSTACK2LR_ORIG R0, R7

    ;We should distinguish USB recovery mode(SDP mode) and internal boot mode.
    ;If ROM runs in SDP mode, then it needn't load boot code from storage media.
    ;If ROM runs in SDP mode, then r0 must be 0x00
    ;If ROM runs in internal boot mode, then r0 should be larger than IRAM base address.

    mov     r7, r0
    cmp     r0, #0x00900000   ; check if in iram        00900000 - 0093FFFF 256K IRAM

    ldr     r3, DDR_DEST_ADDR
    strhi r3, [r0]              ; STR if HI
    ldr     r3, =BOOT_DATA_IMG_LEN
    strhi r3, [r1]
    ldr     r3, =(IVT_OFFSET+(IVT2_ADDR-IVT_ADDR))
    strhi r3, [r2]              ; change content in location pointed by r0~r2

    ; Save the necessary address.
    adr     r4, DDR_DEST_ADDR
    adr     r5, COPY_SIZE
    adr     r6, BOOT_DATA2_ADDR

    ; Check ROM ID to determine silicon revision
    ;ldr     r0, =CSP_BASE_MEM_PA_ROM
    ;ldr     r3, [r0, #ROM_VER_OFFSET]
    ;cmp     r3, #0x20               ;check if it's TO1 silicon
			
;//=============================================================================			
;// Disable	WDOG		
;//=============================================================================			
    ; Disable WDOG
    ; setmem /16 0x020bc000 = 0x30
    ;ldr     r0, =CSP_BASE_REG_PA_WDOG1
    ;ldr     r1, =0x30
    ;ldr     r2, =0x0
    ;str     r1, [r0, r2]
			
;//=============================================================================			
;// ; Configure CCM //  Enable all clocks (they are disabled by ROM code)			
;//=============================================================================	
	ldr     r0, =CSP_BASE_REG_PA_CCM
		
    ; setmem /32 0x020c4068 = 0xffffffff
    ldr     r1, =0xffffffff
    ldr     r2, =0x068
    str     r1, [r0, r2]

    ;setmem /32	0x020c406c =	0xffffffff	
    ldr     r1, =0xffffffff
    ldr     r2, =0x06c
    str     r1, [r0, r2]

    ;setmem /32	0x020c4070 =	0xffffffff
    ldr     r1, =0xffffffff
    ldr     r2, =0x070
    str     r1, [r0, r2]

    ;setmem /32	0x020c4074 =	0xffffffff	
    ldr     r1, =0xffffffff
    ldr     r2, =0x074
    str     r1, [r0, r2]

    ;setmem /32	0x020c4078 =	0xffffffff	
    ldr     r1, =0xffffffff
    ldr     r2, =0x078
    str     r1, [r0, r2]

    ;setmem /32	0x020c407c =	0xffffffff	
    ldr     r1, =0xffffffff
    ldr     r2, =0x07c
    str     r1, [r0, r2]

    ;setmem /32	0x020c4080 =	0xffffffff	
    ldr     r1, =0xffffffff
    ldr     r2, =0x080
    str     r1, [r0, r2]

    ; setmem /32	0x020c4084 =	0xffffffff	
    ldr     r1, =0xffffffff
    ldr     r2, =0x084
    str     r1, [r0, r2]
			
;//=============================================================================			
;// IOMUX			
;//=============================================================================	
	
	ldr     r0, =CSP_BASE_REG_PA_IOMUXC; IOMUXC_BASE_ADDR		

;//DDR IO TYPE:
	;setmem /32	0x020e0774 =	0x000C0000	
	ldr     r1, =0x000C0000		;IOMUXC_SW_PAD_CTL_GRP_DDR_TYPE 
    ldr     r2, =0x774
    str     r1, [r0, r2]
	
	;setmem /32	0x020e0754 =	0x00000000
	ldr     r1, =0x00000000		;IOMUXC_SW_PAD_CTL_GRP_DDRPKE 
	ldr     r2, =0x754
	str     r1, [r0, r2]
			
;//CLOCK:			
	;setmem /32	0x020e04ac =	0x00000030	
	ldr     r1, =0x00000030		;IOMUXC_SW_PAD_CTL_PAD_DRAM_SDCLK_0
	ldr     r2, =0x4ac
	str     r1, [r0, r2]
	
	;setmem /32	0x020e04b0 =	0x00000030
	ldr     r1, =0x00000030		;IOMUXC_SW_PAD_CTL_PAD_DRAM_SDCLK_1
	ldr     r2, =0x4b0
	str     r1, [r0, r2]
				
;//ADDRESS:			
	;setmem /32	0x020e0464 =	0x00000030	// IOMUXC_SW_PAD_CTL_PAD_DRAM_CAS
	ldr     r1, =0x00000030
	ldr     r2, =0x464
	str     r1, [r0, r2]
	
	;setmem /32	0x020e0490 =	0x00000030	// IOMUXC_SW_PAD_CTL_PAD_DRAM_RAS
	ldr     r1, =0x00000030
	ldr     r2, =0x490
	str     r1, [r0, r2]
	
	;setmem /32	0x020e074c =	0x00000030	// IOMUXC_SW_PAD_CTL_GRP_ADDDS 
	ldr     r1, =0x00000030
	ldr     r2, =0x74c
	str     r1, [r0, r2]
			
;//CONTROL:			
	;setmem /32	0x020e0494 =	0x00000030	// IOMUXC_SW_PAD_CTL_PAD_DRAM_RESET
	ldr     r1, =0x00000030
	ldr     r2, =0x494
	str     r1, [r0, r2]
	
	;setmem /32	0x020e04a0 =	0x00000000	// IOMUXC_SW_PAD_CTL_PAD_DRAM_SDBA2 - DSE can be configured using Group Control Register: IOMUXC_SW_PAD_CTL_GRP_CTLDS
	ldr     r1, =0x00000000
	ldr     r2, =0x4a0
	str     r1, [r0, r2]
	
	;setmem /32	0x020e04b4 =	0x00000030	// IOMUXC_SW_PAD_CTL_PAD_DRAM_SDODT0
	ldr     r1, =0x00000030
	ldr     r2, =0x4b4
	str     r1, [r0, r2]
	
	;setmem /32	0x020e04b8 =	0x00000030	// IOMUXC_SW_PAD_CTL_PAD_DRAM_SDODT1
	ldr     r1, =0x00000030
	ldr     r2, =0x4b8
	str     r1, [r0, r2]
	
	;setmem /32	0x020e076c =	0x00000030	// IOMUXC_SW_PAD_CTL_GRP_CTLDS 
	ldr     r1, =0x00000030
	ldr     r2, =0x76c
	str     r1, [r0, r2]	

	
;//DATA STROBE:			
	;setmem /32	0x020e0750 =	0x00020000	// IOMUXC_SW_PAD_CTL_GRP_DDRMODE_CTL 
	ldr     r1, =0x00020000
	ldr     r2, =0x750
	str     r1, [r0, r2]
			
	;setmem /32	0x020e04bc =	0x00000030	// IOMUXC_SW_PAD_CTL_PAD_DRAM_SDQS0
	ldr     r1, =0x00000030
	ldr     r2, =0x4bc
	str     r1, [r0, r2]	
	
	;setmem /32	0x020e04c0 =	0x00000030	// IOMUXC_SW_PAD_CTL_PAD_DRAM_SDQS1
	ldr     r1, =0x00000030
	ldr     r2, =0x4c0
	str     r1, [r0, r2]
	
	;setmem /32	0x020e04c4 =	0x00000030	// IOMUXC_SW_PAD_CTL_PAD_DRAM_SDQS2
	ldr     r1, =0x00000030
	ldr     r2, =0x4c4
	str     r1, [r0, r2]	
	
	;setmem /32	0x020e04c8 =	0x00000030	// IOMUXC_SW_PAD_CTL_PAD_DRAM_SDQS3
	ldr     r1, =0x00000030
	ldr     r2, =0x4c8
	str     r1, [r0, r2]	
	
	;setmem /32	0x020e04cc =	0x00000030	// IOMUXC_SW_PAD_CTL_PAD_DRAM_SDQS4
	ldr     r1, =0x00000030
	ldr     r2, =0x4cc
	str     r1, [r0, r2]	
	
	;setmem /32	0x020e04d0 =	0x00000030	// IOMUXC_SW_PAD_CTL_PAD_DRAM_SDQS5
	ldr     r1, =0x00000030
	ldr     r2, =0x4d0
	str     r1, [r0, r2]	
	
	;setmem /32	0x020e04d4 =	0x00000030	// IOMUXC_SW_PAD_CTL_PAD_DRAM_SDQS6 
	ldr     r1, =0x00000030
	ldr     r2, =0x4d4
	str     r1, [r0, r2]
	
	;setmem /32	0x020e04d8 =	0x00000030	// IOMUXC_SW_PAD_CTL_PAD_DRAM_SDQS7 
	ldr     r1, =0x00000030
	ldr     r2, =0x4d8
	str     r1, [r0, r2]
			
;//DATA:			
	;setmem /32	0x020e0760 =	0x00020000	// IOMUXC_SW_PAD_CTL_GRP_DDRMODE
	ldr     r1, =0x00020000
	ldr     r2, =0x760
	str     r1, [r0, r2]		
	
	;setmem /32	0x020e0764 =	0x00000030	// IOMUXC_SW_PAD_CTL_GRP_B0DS
	ldr     r1, =0x00000030
	ldr     r2, =0x764
	str     r1, [r0, r2]	
	
	;setmem /32	0x020e0770 =	0x00000030	// IOMUXC_SW_PAD_CTL_GRP_B1DS 
	ldr     r1, =0x00000030
	ldr     r2, =0x770
	str     r1, [r0, r2]
	
	;setmem /32	0x020e0778 =	0x00000030	// IOMUXC_SW_PAD_CTL_GRP_B2DS
	ldr     r1, =0x00000030
	ldr     r2, =0x778
	str     r1, [r0, r2]	
	
	;setmem /32	0x020e077c =	0x00000030	// IOMUXC_SW_PAD_CTL_GRP_B3DS
	ldr     r1, =0x00000030
	ldr     r2, =0x77c
	str     r1, [r0, r2]	
	
	;setmem /32	0x020e0780 =	0x00000030	// IOMUXC_SW_PAD_CTL_GRP_B4DS
	ldr     r1, =0x00000030
	ldr     r2, =0x780
	str     r1, [r0, r2]	
	
	;setmem /32	0x020e0784 =	0x00000030	// IOMUXC_SW_PAD_CTL_GRP_B5DS
	ldr     r1, =0x00000030
	ldr     r2, =0x784
	str     r1, [r0, r2]	
	
	;setmem /32	0x020e078c =	0x00000030	// IOMUXC_SW_PAD_CTL_GRP_B6DS
	ldr     r1, =0x00000030
	ldr     r2, =0x78c
	str     r1, [r0, r2]	
	
	;setmem /32	0x020e0748 =	0x00000030	// IOMUXC_SW_PAD_CTL_GRP_B7DS
	ldr     r1, =0x00000030
	ldr     r2, =0x748
	str     r1, [r0, r2]		
	
	;setmem /32	0x020e0470 =	0x00000030	// IOMUXC_SW_PAD_CTL_PAD_DRAM_DQM0
	ldr     r1, =0x00000030
	ldr     r2, =0x470
	str     r1, [r0, r2]	
	
	;setmem /32	0x020e0474 =	0x00000030	// IOMUXC_SW_PAD_CTL_PAD_DRAM_DQM1
	ldr     r1, =0x00000030
	ldr     r2, =0x474
	str     r1, [r0, r2]	
	
	;setmem /32	0x020e0478 =	0x00000030	// IOMUXC_SW_PAD_CTL_PAD_DRAM_DQM2
	ldr     r1, =0x00000030
	ldr     r2, =0x478
	str     r1, [r0, r2]	
	
	;setmem /32	0x020e047c =	0x00000030	// IOMUXC_SW_PAD_CTL_PAD_DRAM_DQM3
	ldr     r1, =0x00000030
	ldr     r2, =0x47c
	str     r1, [r0, r2]	
	
	;setmem /32	0x020e0480 =	0x00000030	// IOMUXC_SW_PAD_CTL_PAD_DRAM_DQM4
	ldr     r1, =0x00000030
	ldr     r2, =0x480
	str     r1, [r0, r2]	
	
	;setmem /32	0x020e0484 =	0x00000030	// IOMUXC_SW_PAD_CTL_PAD_DRAM_DQM5
	ldr     r1, =0x00000030
	ldr     r2, =0x484
	str     r1, [r0, r2]	
	
	;setmem /32	0x020e0488 =	0x00000030	// IOMUXC_SW_PAD_CTL_PAD_DRAM_DQM6
	ldr     r1, =0x00000030
	ldr     r2, =0x488
	str     r1, [r0, r2]	
	
	;setmem /32	0x020e048c =	0x00000030	// IOMUXC_SW_PAD_CTL_PAD_DRAM_DQM7
	ldr     r1, =0x00000030
	ldr     r2, =0x48c
	str     r1, [r0, r2]	
			
;//=============================================================================			
;// DDR Controller Registers			
;//=============================================================================			
;// Manufacturer:	Micron		
;// Device Part Number:	MT41J128M16HA-15E		
;// Clock Freq.: 	400MHz		
;// Density per CS in Gb: 	8		
;// Chip Selects used:	1		
;// Number of Banks:	8		
;// Row address:    	14		
;// Column address: 	10		
;// Data bus width	64		
;//=============================================================================			
	ldr     r0, =CSP_BASE_REG_PA_MMDC0; 0x021b0000	
	
	;setmem /32	0x021b0800 =	0xa1390003 	// DDR_PHY_P0_MPZQHWCTRL, enable both one-time & periodic HW ZQ calibration.
	ldr     r1, =0xa1390003
	ldr     r2, =0x800
	str     r1, [r0, r2]	
			
;// write leveling, based on Freescale board layout and T topology			
;// For target board, may need to run write leveling calibration 			
;// to fine tune these settings			
;// If target board does not use T topology, then these registers			
;// should either be cleared or write leveling calibration can be run			
	
	;setmem /32	0x021b080c = 	0x001F001F	
	ldr     r1, =0x001F001F
	ldr     r2, =0x800
	str     r1, [r0, r2]	
	
	;setmem /32	0x021b0810 = 	0x001F001F	
	ldr     r1, =0x001F001F
	ldr     r2, =0x810
	str     r1, [r0, r2]	
	
	;setmem /32	0x021b480c = 	0x001F001F	
	ldr     r1, =0x001F001F
	ldr     r2, =0x480c
	str     r1, [r0, r2]	
	
	;setmem /32	0x021b4810 = 	0x001F001F	
	ldr     r1, =0x001F001F
	ldr     r2, =0x4810
	str     r1, [r0, r2]	
			
;//######################################################			
;//calibration values based on calibration compare of 0x00ffff00:			
;//Note, these calibration values are based on Freescale's board			
;//May need to run calibration on target board to fine tune these 			
;//######################################################			
			
;//Read DQS Gating calibration			
	;setmem /32	0x021b083c =	0x4220021F	// MPDGCTRL0 PHY0
	ldr     r1, =0x4220021F
	ldr     r2, =0x83c
	str     r1, [r0, r2]	
	
	;setmem /32	0x021b0840 =	0x0207017E	// MPDGCTRL1 PHY0
	ldr     r1, =0x0207017E
	ldr     r2, =0x840
	str     r1, [r0, r2]
	
	;setmem /32	0x021b483c =	0x4201020C	// MPDGCTRL0 PHY1
	ldr     r1, =0x4201020C
	ldr     r2, =0x483c
	str     r1, [r0, r2]
	
	;setmem /32	0x021b4840 =	0x01660172	// MPDGCTRL1 PHY1
	ldr     r1, =0x01660172
	ldr     r2, =0x4840
	str     r1, [r0, r2]
			
;//Read calibration			
	
	;setmem /32	0x021b0848 =	0x4A4D4E4D	// MPRDDLCTL PHY0
	ldr     r1, =0x4A4D4E4D
	ldr     r2, =0x848
	str     r1, [r0, r2]
	
	;setmem /32	0x021b4848 =	0x4A4F5049	// MPRDDLCTL PHY1
	ldr     r1, =0x4A4F5049
	ldr     r2, =0x4848
	str     r1, [r0, r2]
			
;//Write calibration			
	
	;setmem /32	0x021b0850 =	0x3F3C3D31	// MPWRDLCTL PHY0
	ldr     r1, =0x3F3C3D31
	ldr     r2, =0x850
	str     r1, [r0, r2]
	
	;setmem /32	0x021b4850 =	0x3238372B	// MPWRDLCTL PHY1
	ldr     r1, =0x3238372B
	ldr     r2, =0x4850
	str     r1, [r0, r2]
			
;//read data bit delay: (3 is the reccommended default value, although out of reset value is 0):			
	
	;setmem /32	0x021b081c =	0x33333333	// DDR_PHY_P0_MPREDQBY0DL3
	ldr     r1, =0x33333333
	ldr     r2, =0x81c
	str     r1, [r0, r2]
	
	;setmem /32	0x021b0820 =	0x33333333	// DDR_PHY_P0_MPREDQBY1DL3
	ldr     r1, =0x33333333
	ldr     r2, =0x820
	str     r1, [r0, r2]
	
	;setmem /32	0x021b0824 =	0x33333333	// DDR_PHY_P0_MPREDQBY2DL3
	ldr     r1, =0x33333333
	ldr     r2, =0x824
	str     r1, [r0, r2]
	
	;setmem /32	0x021b0828 =	0x33333333	// DDR_PHY_P0_MPREDQBY3DL3
	ldr     r1, =0x33333333
	ldr     r2, =0x828
	str     r1, [r0, r2]
	
	;setmem /32	0x021b481c =	0x33333333	// DDR_PHY_P1_MPREDQBY0DL3
	ldr     r1, =0x33333333
	ldr     r2, =0x481c
	str     r1, [r0, r2]
	
	;setmem /32	0x021b4820 =	0x33333333	// DDR_PHY_P1_MPREDQBY1DL3
	ldr     r1, =0x33333333
	ldr     r2, =0x4820
	str     r1, [r0, r2]
	
	;setmem /32	0x021b4824 =	0x33333333	// DDR_PHY_P1_MPREDQBY2DL3
	ldr     r1, =0x33333333
	ldr     r2, =0x4824
	str     r1, [r0, r2]
	
	;setmem /32	0x021b4828 =	0x33333333	// DDR_PHY_P1_MPREDQBY3DL3
	ldr     r1, =0x33333333
	ldr     r2, =0x4828
	str     r1, [r0, r2]
			
;//For i.mx6qd parts of versions A & B (v1.0, v1.1), uncomment the following lines. For version C (v1.2), keep commented			
;//setmem /32	0x021b08c0 =	0x24911492	// fine tune SDCLK duty cyc to low - seen to improve measured duty cycle of i.mx6
;//setmem /32	0x021b48c0 =	0x24911492	
			
;// Complete calibration by forced measurement:			
	
	;setmem /32	0x021b08b8 =	0x00000800 	// DDR_PHY_P0_MPMUR0, frc_msr
	ldr     r1, =0x00000800
	ldr     r2, =0x8b8
	str     r1, [r0, r2]
	
	;setmem /32	0x021b48b8 =	0x00000800 	// DDR_PHY_P0_MPMUR0, frc_msr
	ldr     r1, =0x00000800
	ldr     r2, =0x48b8
	str     r1, [r0, r2]
			
;//MMDC init:			
	
	;setmem /32	0x021b0004 =	0x0002002D	// MMDC0_MDPDC 
	ldr     r1, =0x0002002D
	ldr     r2, =0x0004
	str     r1, [r0, r2]
	
	;setmem /32	0x021b0008 =	0x00333030	// MMDC0_MDOTC
	ldr     r1, =0x00333030
	ldr     r2, =0x0008
	str     r1, [r0, r2]
	
	;setmem /32	0x021b000c =	0x3F435313	// MMDC0_MDCFG0
	ldr     r1, =0x3F435313
	ldr     r2, =0x000c
	str     r1, [r0, r2]
	
	;setmem /32	0x021b0010 =	0xB66E8B63	// MMDC0_MDCFG1
	ldr     r1, =0xB66E8B63
	ldr     r2, =0x0010
	str     r1, [r0, r2]
	
	;setmem /32	0x021b0014 =	0x01FF00DB	// MMDC0_MDCFG2
	ldr     r1, =0x01FF00DB
	ldr     r2, =0x0014
	str     r1, [r0, r2]
	
	;setmem /32	0x021b0018 =	0x00001740	// MMDC0_MDMISC
	ldr     r1, =0x00001740
	ldr     r2, =0x0018
	str     r1, [r0, r2]
;//NOTE about MDMISC RALAT:			
;//MDMISC: RALAT kept to the high level of 5 to ensure stable operation at 528MHz. 			
;//MDMISC: consider reducing RALAT if your 528MHz board design allow that. Lower RALAT benefits: 			
;//a. better operation at low frequency			
;//b. Small performence improvment			
			
	;setmem /32	0x021b001c =	0x00008000	// MMDC0_MDSCR, set the Configuration request bit during MMDC set up
	ldr     r1, =0x00008000
	ldr     r2, =0x001c
	str     r1, [r0, r2]
	;setmem /32	0x021b002c =	0x000026d2	// MMDC0_MDRWD; recommend to maintain the default values
	ldr     r1, =0x000026d2
	ldr     r2, =0x002c
	str     r1, [r0, r2]
	;setmem /32	0x021b0030 =	0x00431023	// MMDC0_MDOR
	ldr     r1, =0x00431023
	ldr     r2, =0x0030
	str     r1, [r0, r2]
	;setmem /32	0x021b0040 =	0x00000027	// CS0_END 
	ldr     r1, =0x00000027
	ldr     r2, =0x0040
	str     r1, [r0, r2]
			
	;setmem /32	0x021b0000 =	0x831A0000	// MMDC0_MDCTL
	ldr     r1, =0x831A0000
	ldr     r2, =0x0000
	str     r1, [r0, r2]
			
;// Mode register writes			
	
	;setmem /32	0x021b001c =	0x04008032	// MMDC0_MDSCR, MR2 write, CS0
	ldr     r1, =0x04008032
	ldr     r2, =0x001c
	str     r1, [r0, r2]
	
	;setmem /32	0x021b001c =	0x00008033	// MMDC0_MDSCR, MR3 write, CS0
	ldr     r1, =0x00008033
	ldr     r2, =0x001c
	str     r1, [r0, r2]
	
	;setmem /32	0x021b001c =	0x00048031	// MMDC0_MDSCR, MR1 write, CS0
	ldr     r1, =0x00048031
	ldr     r2, =0x001c
	str     r1, [r0, r2]
	
	;setmem /32	0x021b001c =	0x05208030	// MMDC0_MDSCR, MR0 write, CS0
	ldr     r1, =0x05208030
	ldr     r2, =0x001c
	str     r1, [r0, r2]
	
	;setmem /32	0x021b001c =	0x04008040	// MMDC0_MDSCR, ZQ calibration command sent to device on CS0
	ldr     r1, =0x04008040
	ldr     r2, =0x001c
	str     r1, [r0, r2]	
	
;//setmem /32	0x021b001c =	0x0400803A	// MMDC0_MDSCR, MR2 write, CS1
;//setmem /32	0x021b001c =	0x0000803B	// MMDC0_MDSCR, MR3 write, CS1
;//setmem /32	0x021b001c =	0x00048039	// MMDC0_MDSCR, MR1 write, CS1
;//setmem /32	0x021b001c =	0x05208038	// MMDC0_MDSCR, MR0 write, CS1
;//setmem /32	0x021b001c =	0x04008048	// MMDC0_MDSCR, ZQ calibration command sent to device on CS1
			
			
	;setmem /32	0x021b0020 =	0x00005800	// MMDC0_MDREF
	ldr     r1, =0x00005800
	ldr     r2, =0x0020
	str     r1, [r0, r2]	
			
	;setmem /32	0x021b0818 =	0x00011117	// DDR_PHY_P0_MPODTCTRL
	ldr     r1, =0x00011117
	ldr     r2, =0x0818
	str     r1, [r0, r2]	
	
	;setmem /32	0x021b4818 =	0x00011117	// DDR_PHY_P1_MPODTCTRL
	ldr     r1, =0x00011117
	ldr     r2, =0x0818
	str     r1, [r0, r2]	
			
	;setmem /32	0x021b0004 =	0x0002556D	// MMDC0_MDPDC with PWDT bits set
	ldr     r1, =0x0002556D
	ldr     r2, =0x0004
	str     r1, [r0, r2]	
	
	;setmem /32	0x021b0404 = 	0x00011006	// MMDC0_MAPSR ADOPT power down enabled, MMDC will enter automatically to self-refresh while the number of idle cycle reached.
	ldr     r1, =0x00011006
	ldr     r2, =0x0404
	str     r1, [r0, r2]			
	
	;setmem /32	0x021b001c =	0x00000000	// MMDC0_MDSCR, clear this register (especially the configuration bit as initialization is complete)
	ldr     r1, =0x00000000
	ldr     r2, =0x001c
	str     r1, [r0, r2]	
	; end of DDR3 Initialation Routine
;
;    The following is to fill in those arguments for this ROM function
;    pu_irom_hwcnfg_setup(void **start, size_t *bytes, const void *boot_data)
;
;    This function is used to copy data from the storage media into DDR.
;
;    start - Initial (possibly partial) image load address on entry.  
;            Final image load address on exit.
;    bytes - Initial (possibly partial) image size on entry.  
;            Final image size on exit.
;    boot_data - Initial @ref ivt Boot Data load address.
;
    ; Restore the necessary address.
PLUGIN_HOOK_ENTRY

    MOV R0, R4
    MOV R1, R5
    MOV R2, R6

    ;ROM API pu_irom_hwcnfg_setup(void **start, size_t *bytes, const void *boot_data)
    ;is the 3rd API in the ROM API Vector Table
    LDR     r4, =ROM_API_VECTOR_OFFSET
    LDR     r4, [R4, #0x08]

    ;We should distinguish USB recovery mode(SDP mode) and internal boot mode.
    ;If ROM runs in SDP mode, then it needn't load boot code from storage media.
    ;If ROM runs in SDP mode, then r0 must be 0x00
    ;If ROM runs in internal boot mode, then r0 should be larger than IRAM base address.    

    CMP R7, #0x00900000
    BLXHI R4            ; Address is subject to change in future ROM versions

    ;Workaround run plug-ins in SDP mode without USB re-enumeration.
    ;how it works:
    ;ROM running in usb download mode.
    ;Host manufacturing application sends SDP command to download plug-in image.
    ;Host manufacturing application sends SDP command to jump to plug-in image and run it.
    ;Plug-in starts execution and after its regular tasks plug-in will then call into ROM
    ;call into pl_parse_and_handle()

    ;ROM API pl_parse_and_handle()
    ;is the 2rd API in the ROM API Vector Table
    LDR     r4, =ROM_API_VECTOR_OFFSET
    LDR     r4, [R4, #0x4]

    CMP R7, #0x00900000
    BLXLS R4
        
; To return to ROM from plugin, we need to fill in these argument.
; Here is what need to do:
; Need to construct the paramters for this function before return to ROM:
; plugin_download(void **start, size_t *bytes, UINT32 *ivt_offset)

    ; Restore registers
    POPSTACK2LR_ORIG R0, R7

    ldr     r3, DDR_DEST_ADDR
    str     r3, [r0]
    ldr     r3, =BOOT_DATA_IMG_LEN
    str     r3, [r1]
    ldr     r3, =(IVT_OFFSET+(IVT2_ADDR-IVT_ADDR))
    str     r3, [r2]
    mov 	r0, #1

    RETURN_THUMB ; return back to ROM code

    LTORG

; Reserved region for CSF data
    SECUREDATA
    ALIGN 4
CSF_ADDR
    RESERVE_CSF     (IMAGE_HDR_SIZE - (CSF_ADDR - IVT_ADDR))

    END
