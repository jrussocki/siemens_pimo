;------------------------------------------------------------------------------
;
;   Copyright (C) 2006-2011, Freescale Semiconductor, Inc. All Rights Reserved.
;   THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
;   AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
;
;------------------------------------------------------------------------------
;
;   FILE: secureboot.s
;
;   This code is included in the startup.s file to create 
;   space for CSF data and to include Loader Security Header.
;
;------------------------------------------------------------------------------

    INCLUDE mx6q_base_regs.inc
    INCLUDE mx6q_base_mem.inc
    INCLUDE image_cfg.inc

JUMP_PAGE_SIZE      EQU 0x1000
CSF_SIZE            EQU 0x800
LS_CURRENT_VERSION  EQU 0x00030003

    ; Macro to reserve the CSF data
    MACRO
    RESERVE_CSF     $size
        LCLA    Counter
Counter     SETA    $size
        WHILE   Counter > 0
Counter     SETA    Counter - 1
            DCB 0x00 
        WEND
    MEND

    AREA        .asecure,ALIGN=4,CODE

    EXPORT CSF_DATA
CSF_DATA
    RESERVE_CSF  CSF_SIZE

;/** LS Application Header data structure.
; *
; * Contains all the information required by the LS functions,
; * specific to the particular Boot Image.
; * (Note: The size of the following data structure must be multiple
; * of 4 bytes)
; */
;typedef struct
;{
;    UINT32 loader_security_version_no;   /**< The version of Loader
;                                              Security Module that
;                                              the Software Component
;                                              wants to use */
;    UINT32 *entry_point;                 /**< Execution start address
;                                              */
;    UINT32 *re_entry_point;              /**< Image re-entry address
;                                              for Warm Boot. Ignored
;                                              unless LS_WARM_BOOT
;                                              defined */
;    UINT32 *app_code_csf_addr;           /**< App CSF address */
;    UINT32 rtic_dma_delay;               /**< RTIC DMA Delay. Ignored
;                                             if (rtic_watchdog_timeout
;                                             = 0) or
;                                              unless LS_RTIC_SWITCH
;                                              defined */
;    UINT32 rtic_watchdog_timeout;        /**< RTIC WATCHDOG Timeout,
;                                              0 to avoid activating
;                                              RTIC. Ignored unless
;                                              LS_RTIC_SWITCH
;                                              defined */
;    LS_RTIC_DMA_WORD_T rtic_dma_burst;   /**< RTIC DMA Burst Size.
;                                              Ignored if
;                                             (rtic_watchdog_timeout=0)
;                                             or unless LS_RTIC_SWITCH
;                                             defined */
;    UINT32 sw_security_id;               /**< SW component Identifier.
;                                              Ignored unless
;                                              LS_CODE_REVOCATION
;                                              defined */
;    UINT32 sw_security_version;          /**< SW Security Version.
;                                              Ignored unless
;                                              LS_CODE_REVOCATION
;                                              defined */
;    UINT32 loader_security_flags;        /**< Bit0 -
;                                              0 - Reload RTIC values
;                                              for run-time monitoring
;                                              1 - Recalculate RTIC
;                                              hashes
;                                              Bit1 - Bit31 Reserved
;                                              Ignored unless
;                                              LS_WARM_BOOT defined */
;    UINT32 released_partitions;          /**< The SCC partitions to be
;                                              released for SCCv2 */
;} LS_APP_HEADER_T;
loader_security_version_no  DCD LS_CURRENT_VERSION
entry_point                 DCD IMAGE_BOOT_BOOTIMAGE_RAM_PA_START
re_entry_point              DCD 0x00000000
app_code_csf_addr           DCD (IMAGE_BOOT_BOOTIMAGE_RAM_PA_START + JUMP_PAGE_SIZE)
rtic_dma_delay              DCD 0x00000000
rtic_watchdog_timeout       DCD 0x00000000
rtic_dma_burst              DCD 0x00000000
sw_security_id              DCD 0x00000000
sw_security_version         DCD 0x00000000
loader_security_flags       DCD 0x00000000
released_partitions         DCD 0x00000000

    END
