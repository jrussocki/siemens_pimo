//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//------------------------------------------------------------------------------
//
//  Copyright (C) 2007-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  menu.c
//
//  Menu routines for the ethernet bootloader.
//
//------------------------------------------------------------------------------
#include <bsp.h>
#include "loader.h"

//-----------------------------------------------------------------------------
// External Functions
extern BLMenuPrintDataSoc PrintSoc;
extern BOOL BLMenuShow(BLMENU_ITEM *pMenu);


//-----------------------------------------------------------------------------
// External Variables
extern BSP_ARGS     *g_pBSPArgs;
extern BOOT_CFG     g_BootCFG;
extern BOOL         g_DownloadImage;
extern UINT32 EthDevice;  // To determine if Ethernet has been initailized for download/KITL usage
extern BOOLEAN bCFGChanged;
extern DWORD g_SDSlotIndex;

//-----------------------------------------------------------------------------
// Defines


//-----------------------------------------------------------------------------
// Types


//-----------------------------------------------------------------------------
// Global Variables


//-----------------------------------------------------------------------------
// Local Variables


//------------------------------------------------------------------------------
// Local Functions
//
static BOOL SelectBootDevice(BLMENU_ITEM *pMenu);
static BOOL SelectEtherDevice(BLMENU_ITEM *pMenu);
static BOOL PrintData(WCHAR key,VOID * dwValue,VOID * mask);


//Soc Menu Item Macro
#define ATA_Utilities {\
    L'A', L"ATA Utilities", BLMenuATAUtilities,NULL, NULL, NULL\
    }


#define SocMenu ATA_Utilities,Null_Menu
#define MainMenu  { CommonMenu,SocMenu } 


// Local Variables
static BLMENU_ITEM g_menu[] = MainMenu;

//------------------------------------------------------------------------------
//
//  Function:  BLMenuATAUtilities
//
//  Provides utilities for ATA cards.
//
//  Parameters:
//      pMenu
//
//  Returns:
//      TRUE indicates exiting menu. FALSE indicates go back to menu.
//
//------------------------------------------------------------------------------
BOOL BLMenuATAUtilities(BLMENU_ITEM *pMenu)
{
    static BLMENU_ITEM atamenu[] = ATASubMenu;
    UNREFERENCED_PARAMETER(pMenu);
    
    BLMenuShow(atamenu);

    return FALSE;

}


//------------------------------------------------------------------------------
//
//  Function:  BLMenuCreateFileSystemOnATA
//
//  Provides Creation of MBR with 2 partitions on card.
//
//  Parameters:
//      pMenu
//
//  Returns:
//      TRUE indicates exiting menu. FALSE indicates go back to menu.
//
//------------------------------------------------------------------------------
BOOL BLMenuCreateFileSystemOnATA(BLMENU_ITEM *pMenu)
{
    UINT32 Selection;
    UNREFERENCED_PARAMETER(pMenu);

    KITLOutputDebugString("\r\nWARNING:  ATA File System partition requested on boot card.\r\n");
    KITLOutputDebugString("All previous file system data on the card may be lost.\r\n\r\n");
    KITLOutputDebugString("Do you want to continue (y/n)? ");
    do {
        Selection = tolower(OEMReadDebugByte());
    } while ((Selection != 'y') && (Selection != 'n'));
    KITLOutputDebugString("\r\n");
    
    if (Selection == 'y') 
    {
        if(ATACreateFileSystemPartition())
        {
            KITLOutputDebugString("\r\nIf partition creation was successful, please plug the card into a Windows PC to format the file system partition.\r\n\r\n");
            KITLOutputDebugString("Reboot the device manually...\r\n");
            SpinForever();
        }
        else
            return TRUE;
    }
    
    return FALSE;
}


//------------------------------------------------------------------------------
//
//  Function:  SelectBootDevice
//
//  Provides selecting boot device.
//
//  Parameters:
//      pMenu
//
//  Returns:
//      TRUE indicates exiting menu. FALSE indicates go back to menu.
//
//------------------------------------------------------------------------------
BOOL SelectBootDevice(BLMENU_ITEM *pMenu)
{
    UNREFERENCED_PARAMETER(pMenu);
    switch(g_BootCFG.autoDownloadImage)
    {
    case BOOT_CFG_AUTODOWNLOAD_NONE:
    //    g_BootCFG.autoDownloadImage = BOOT_CFG_AUTODOWNLOAD_NK_NOR;
    //    break;
    //case BOOT_CFG_AUTODOWNLOAD_NK_NOR:
        g_BootCFG.autoDownloadImage = BOOT_CFG_AUTODOWNLOAD_NK_NAND;
        break;
    case BOOT_CFG_AUTODOWNLOAD_NK_NAND:
    //    g_BootCFG.autoDownloadImage = BOOT_CFG_AUTODOWNLOAD_IPL_NAND;
    //    break;
    //case BOOT_CFG_AUTODOWNLOAD_IPL_NAND:
        g_BootCFG.autoDownloadImage = BOOT_CFG_AUTODOWNLOAD_NK_SD;
        break;
    case BOOT_CFG_AUTODOWNLOAD_NK_SD:
        g_BootCFG.autoDownloadImage = BOOT_CFG_AUTODOWNLOAD_NK_ATA;
        break;
    default:
        g_BootCFG.autoDownloadImage = BOOT_CFG_AUTODOWNLOAD_NONE;
        break;                       
    }
    bCFGChanged=TRUE;
    return FALSE;
}

//------------------------------------------------------------------------------
//
//  Function:  SelectEtherDevice
//
//  Provides selecting ethernet device.
//
//  Parameters:
//      pMenu
//
//  Returns:
//      TRUE indicates exiting menu. FALSE indicates go back to menu.
//
//------------------------------------------------------------------------------
BOOL SelectEtherDevice(BLMENU_ITEM *pMenu) 
{
    UNREFERENCED_PARAMETER(pMenu);      
    switch(g_BootCFG.EtherDevice)
    {
        case ETH_DEVICE_ENET:
            g_BootCFG.EtherDevice = ETH_DEVICE_FEC;
            break;
        case ETH_DEVICE_FEC:
            g_BootCFG.EtherDevice = ETH_DEVICE_USB;
            break;
        case ETH_DEVICE_USB:
            g_BootCFG.EtherDevice = SER_DEVICE_USB;
            break;
        case SER_DEVICE_USB:
            g_BootCFG.EtherDevice = ETH_DEVICE_ENET;
            break;
        default:
            g_BootCFG.EtherDevice = ETH_DEVICE_ENET;
            break;                       
    }
    
    bCFGChanged=TRUE;
    return FALSE;
}

//------------------------------------------------------------------------------
//
//  Function:  PrintData
//
//  Provides Printing the SOC data.
//
//  Parameters:
//      key
//      dwValue
//      mask
//
//  Returns:
//      TRUE indicates exiting menu. FALSE indicates go back to menu.
//
//------------------------------------------------------------------------------
BOOL  PrintData(WCHAR key,VOID * dwValue,VOID * mask)
{

    UNREFERENCED_PARAMETER(dwValue);
    UNREFERENCED_PARAMETER(mask);
    switch(key) 
    {         
        case L'5':
            switch(g_BootCFG.autoDownloadImage) 
            {
                //case BOOT_CFG_AUTODOWNLOAD_NK_NOR:
                //    KITLOutputDebugString(" : NK from NOR\r\n");
                //    break;
                case BOOT_CFG_AUTODOWNLOAD_NK_NAND:
                    KITLOutputDebugString(" : NK from NAND\r\n");
                    break;
                //case BOOT_CFG_AUTODOWNLOAD_IPL_NAND:
                //    KITLOutputDebugString(" : IPL from NAND\r\n");
                //    break;
                case BOOT_CFG_AUTODOWNLOAD_NK_SD:
                    KITLOutputDebugString(" : NK from SD/MMC\r\n");
                    break;
                case BOOT_CFG_AUTODOWNLOAD_NK_ATA:
                    KITLOutputDebugString(" : NK from ATA\r\n");
                    break;
                default:
                    KITLOutputDebugString(" : Disabled\r\n");
                    break;
            }
            break;  
        
        case L'E':
            switch(g_BootCFG.EtherDevice) 
            {
                case ETH_DEVICE_ENET:
                    KITLOutputDebugString(" : ENET\r\n");
                    break;
                case ETH_DEVICE_FEC:
                    KITLOutputDebugString(" : FEC\r\n");
                    break;
                case ETH_DEVICE_USB:
                    KITLOutputDebugString(" : USB RNDIS\r\n");
                    break;
                case SER_DEVICE_USB:
                    KITLOutputDebugString(" : USB Serial\r\n");
                    break;
                default:
                     KITLOutputDebugString(" : Invalid Device\r\n");
                     break;
            }
            break;  
                            
        default:            
            KITLOutputDebugString ( "\r\n");            
            break;
        }
    return TRUE;
}

//------------------------------------------------------------------------------
//
//  Function:  BLMenu
//
//  Provides boot loader menu interface.
//
//  Parameters:
//      None.
//
//  Returns:
//      TRUE indicates success. FALSE indicates failure.
//
//------------------------------------------------------------------------------
BOOL BLMenu()
{
    UINT32 AutoBootDelay = 0;
    UINT32 StartTime, CurrTime, PrevTime;
    UINT32 Selection;
    PrintSoc = (BLMenuPrintDataSoc)PrintData;    
    // If mac address has not been programmed, immediately drop into menu
    // so user can give us one.
    if (!g_BootCFG.mac[0] && !g_BootCFG.mac[1] && !g_BootCFG.mac[2])
    {
        EdbgOutputDebugString("WARNING:  Uninitialized MAC address.  Select valid address using menu.\r\n");
        Selection = 0x20;
    }
    else
    {
        AutoBootDelay = g_BootCFG.delay;
        switch(g_BootCFG.autoDownloadImage)
        {
        case BOOT_CFG_AUTODOWNLOAD_NK_NOR:
            g_DownloadImage = FALSE;
            EdbgOutputDebugString("\r\nPress [ENTER] to launch image stored in NOR flash or [SPACE] to cancel.\r\n");
            EdbgOutputDebugString("\r\nInitiating image launch in %d seconds. ", AutoBootDelay--);
            break;

        case BOOT_CFG_AUTODOWNLOAD_NK_NAND:
            g_DownloadImage = FALSE;
            EdbgOutputDebugString("\r\nPress [ENTER] to launch image stored in NAND flash or [SPACE] to cancel.\r\n");
            EdbgOutputDebugString("\r\nInitiating image launch in %d seconds. ", AutoBootDelay--);
            break;

        case BOOT_CFG_AUTODOWNLOAD_IPL_NAND:
            g_DownloadImage = FALSE;
            EdbgOutputDebugString("\r\nPress [ENTER] to launch IPL stored in NAND flash or [SPACE] to cancel.\r\n");
            EdbgOutputDebugString("\r\nInitiating IPL launch in %d seconds. ", AutoBootDelay--);
            break;

        case BOOT_CFG_AUTODOWNLOAD_NK_SD:
            g_DownloadImage = FALSE;
            EdbgOutputDebugString("\r\nPress [ENTER] to launch image stored in SD/MMC or [SPACE] to cancel.\r\n");
            EdbgOutputDebugString("\r\nInitiating image launch in %d seconds. ", AutoBootDelay--);
            break;

        case BOOT_CFG_AUTODOWNLOAD_NK_ATA:
            g_DownloadImage = FALSE;
            EdbgOutputDebugString("\r\nPress [ENTER] to launch image stored in ATA or [SPACE] to cancel.\r\n");
            EdbgOutputDebugString("\r\nInitiating image launch in %d seconds. ", AutoBootDelay--);
            break;

        default:
            g_DownloadImage = TRUE;
            EdbgOutputDebugString("\r\nPress [ENTER] to download now or [SPACE] to cancel.\r\n");
            EdbgOutputDebugString("\r\nInitiating image download in %d seconds. ", AutoBootDelay--);
            break;
        }

        // Get a snapshot of the RTC seconds count.
        //
        StartTime     = OEMEthGetSecs();
        PrevTime      = StartTime;
        CurrTime      = StartTime;
        Selection     = (UINT32)OEM_DEBUG_READ_NODATA;

        // Allow the user an amount of time to halt the auto boot/download process.
        // Count down to 0 before proceeding with default operation.
        //
        while ((CurrTime - StartTime) < g_BootCFG.delay)
        {
            UINT8 i=0;
            UINT8 j=0;

            Selection = OEMReadDebugByte(); 
            if ((Selection == 0x20) || (Selection == 0x0d))
            {
                break;
            }
            CurrTime = OEMEthGetSecs();   
            if (CurrTime > PrevTime)
            {
                PrevTime = CurrTime;
                if (AutoBootDelay < 9)
                    i = 11;
                else if (AutoBootDelay < 99)
                    i = 12;
                else if (AutoBootDelay < 999)
                    i = 13;

                for (j = 0; j < i; j++)
                {
                    OEMWriteDebugByte((BYTE)0x08); // print back space
                }

                KITLOutputDebugString ( "%d seconds. ", AutoBootDelay--);
            }
        }
    }

    switch (Selection)
    {
        case OEM_DEBUG_READ_NODATA: // fall through if nothing typed
        case 0x0d: // user canceled wait
        {
            if (g_BootCFG.autoDownloadImage)
            {
                KITLOutputDebugString ( "\r\nLaunching flash image  ... \r\n");
            }
            else
            {
                KITLOutputDebugString ( "\r\nStarting auto download ... \r\n");
            }
            break;
        }
        case 0x20:
        {
            Selection = 0;
            BLMenuShow(g_menu);
            break;
        }
        default:
            break;
    }
    
    if (bCFGChanged == TRUE)
    {    
        StoreBootCFG(&g_BootCFG);
        ConfigBootCFG(&g_BootCFG);
    }    
    
    // Provide MAC from the Boot configuration parameters previously loaded.
    memcpy(g_pBSPArgs->kitl.mac, g_BootCFG.mac, 6);

    // Don't initialize any ethernet controller or ser controller in case no image needs to be 
    // downloaded but just USB KITL is enabled. Otherwise initialize the ethernet
    // controller selected by user.
    if ((!g_DownloadImage) && (g_BootCFG.dwConfigFlags & CONFIG_FLAGS_KITL_ENABLE) && (g_BootCFG.EtherDevice == ETH_DEVICE_USB))
    {
        g_pBSPArgs->kitl.devLoc.PhysicalLoc = (PVOID)CSP_BASE_REG_PA_USB;
        g_pBSPArgs->kitl.devLoc.LogicalLoc = (DWORD)g_pBSPArgs->kitl.devLoc.PhysicalLoc ;
        return(TRUE);
    }

    if ((!g_DownloadImage) && (g_BootCFG.dwConfigFlags & CONFIG_FLAGS_KITL_ENABLE) && (g_BootCFG.EtherDevice == SER_DEVICE_USB))
    {
        g_pBSPArgs->kitl.devLoc.PhysicalLoc = (PVOID)(CSP_BASE_REG_PA_USB+1);
        g_pBSPArgs->kitl.devLoc.LogicalLoc = (DWORD)g_pBSPArgs->kitl.devLoc.PhysicalLoc ;
        return(TRUE);
    }

    // If active KITL is enabled at boot, or user selected the download option,
    // locate and initialize an Ethernet controller.
    //
    if (((g_BootCFG.dwConfigFlags & CONFIG_FLAGS_KITL_ENABLE) && ((g_BootCFG.dwConfigFlags & CONFIG_FLAGS_KITL_PASSIVE) == 0)) || g_DownloadImage)
    {
        EthDevice = InitSpecifiedEthDevice(&g_pBSPArgs->kitl, g_BootCFG.EtherDevice);
        if (EthDevice == -1)
        {
            // No device was found ... 
            //
            KITLOutputDebugString("ERROR: Failed to detect and initialize Ethernet controller.\r\n");
            return(FALSE);
        }
   
        // Make sure MAC address has been programmed.
        //
        if (!g_pBSPArgs->kitl.mac[0] && !g_pBSPArgs->kitl.mac[1] && !g_pBSPArgs->kitl.mac[2])
        {
            KITLOutputDebugString("ERROR: Invalid Ethernet address read from Ethernet controller.\n");
            return(FALSE);
        }
   
        KITLOutputDebugString("INFO: MAC address: %x-%x-%x-%x-%x-%x\r\n",
            g_pBSPArgs->kitl.mac[0] & 0x00FF, g_pBSPArgs->kitl.mac[0] >> 8,
            g_pBSPArgs->kitl.mac[1] & 0x00FF, g_pBSPArgs->kitl.mac[1] >> 8,
            g_pBSPArgs->kitl.mac[2] & 0x00FF, g_pBSPArgs->kitl.mac[2] >> 8);
    }
 
    KITLOutputDebugString("-BLMenu .\r\n");
    return(TRUE);
}

