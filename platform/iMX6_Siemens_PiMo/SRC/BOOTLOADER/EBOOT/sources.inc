!if 0

Copyright (C) 2007-2011, Freescale Semiconductor, Inc. All Rights Reserved.
THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT

!endif

!IF "$(WINCEDEBUG)" == "debug"
SKIPBUILD=1
!ENDIF
!IF "$(WINCEDEBUG)" == "checked"
SKIPBUILD=1
!ENDIF

TARGETTYPE=PROGRAM
RELEASETYPE=PLATFORM
WINCECPU=1
SYNCHRONIZE_DRAIN=1
WINCEREL=1
NOMIPS16CODE=1
WINCEOEM=1

WINCETARGETFILES=BootImage

NOLIBC=1

EXEENTRY=StartUp

INCLUDES=\
    $(INCLUDES); \
    ..\..\COMMON;

ADEFINES=-pd "_TGTCPU SETS \"$(_TGTCPU)\"" $(ADEFINES)

LDEFINES=-subsystem:native /DEBUG /DEBUGTYPE:CV /FIXED:NO

CDEFINES=$(CDEFINES) -DBOOTLOADER

TARGETLIBS=\
    $(_PLATCOMMONLIB)\$(_CPUDEPPATH)\BootPart.lib                              \
    $(_PLATCOMMONLIB)\$(_CPUINDPATH)\oal_blnk.lib                              \
    $(_PLATCOMMONLIB)\$(_CPUINDPATH)\oal_blcommon.lib                          \
    $(_PLATCOMMONLIB)\$(_CPUINDPATH)\oal_log.lib                               \
    $(_PLATCOMMONLIB)\$(_CPUINDPATH)\oal_cache_armv7.lib			           \
    $(_PLATCOMMONLIB)\$(_CPUINDPATH)\oal_kitl.lib                              \
    $(_PLATCOMMONLIB)\$(_CPUINDPATH)\oal_other.lib                             \
    $(_PLATCOMMONLIB)\$(_CPUINDPATH)\oal_blmemory_arm.lib                      \
    $(_PLATCOMMONLIB)\$(_CPUINDPATH)\oal_io.lib                                \
    $(_PLATCOMMONLIB)\$(_CPUINDPATH)\oal_ethdrv_lan9217_$(_COMMONSOCDIR).lib   \
    $(_PLATCOMMONLIB)\$(_CPUINDPATH)\oal_ethdrv_enet_$(_COMMONSOCDIR).lib      \
    $(_PLATCOMMONLIB)\$(_CPUINDPATH)\sdmmc_boot_$(_COMMONSOCDIR).lib           \
    $(_PLATCOMMONLIB)\$(_CPUINDPATH)\ata_boot_$(_COMMONSOCDIR).lib             \
    $(_PLATCOMMONLIB)\$(_CPUINDPATH)\oal_blserial.lib                          \
    $(_PLATLIB)\$(_CPUDEPPATH)\serialutils.lib                                 \
    $(_PLATLIB)\$(_CPUDEPPATH)\i2cutils.lib                                    \
    $(_PLATLIB)\$(_CPUDEPPATH)\bspcmn.lib                                      \
    $(_PLATLIB)\$(_CPUINDPATH)\bsp_bootshell.lib                               \
    $(_PLATLIB)\$(_CPUDEPPATH)\ethdrv.lib                                      \
    $(_PLATLIB)\$(_CPUDEPPATH)\bootcmn.lib                                     \
    $(_PLATLIB)\$(_CPUDEPPATH)\ebootdisplay.lib                                \
    $(_PLATCOMMONLIB)\$(_CPUDEPPATH)\eboot.lib                                 \
    $(_PLATCOMMONLIB)\$(_CPUINDPATH)\oal_usbdbgsermdd.lib                      \
    $(_COMMONOAKROOT)\lib\$(_CPUINDPATH)\gsnull.lib                            \
!IF "$(_WINCEOSVER)" <= "700"
    $(_COMMONOAKROOT)\lib\$(_CPUINDPATH)\fulllibc.lib                          \
!ELSE
    $(_COMMONOAKROOT)\lib\$(_CPUINDPATH)\bootcrt.lib                           \
!ENDIF
    $(_PLATLIB)\$(_CPUDEPPATH)\i2cutils.lib                                    \
    $(_PLATCOMMONLIB)\$(_CPUINDPATH)\fsl_usbdbgrndismdd_$(_COMMONSOCDIR).lib   \
    $(_PLATCOMMONLIB)\$(_CPUINDPATH)\fsl_usbdbgrndispdd_$(_SOCDIR).lib         \
    $(_PLATLIB)\$(_CPUDEPPATH)\fsl_usbfn_rndiskitl_$(_SOCDIR).lib              \
    $(_PLATLIB)\$(_CPUINDPATH)\bspenet.lib                                     \
    $(_PLATCOMMONLIB)\$(_CPUINDPATH)\fsl_rne_mdd_$(_COMMONSOCDIR).lib          \
    $(_PLATCOMMONLIB)\$(_CPUINDPATH)\usb_usbfn_$(_SOCDIR).lib                  \
    $(_PLATCOMMONLIB)\$(_CPUINDPATH)\usb_usbfn_eboot_$(_SOCDIR).lib            \
    $(_PLATCOMMONLIB)\$(_CPUDEPPATH)\bootshell_$(_COMMONSOCDIR).lib            \
    $(_PLATCOMMONLIB)\$(_CPUDEPPATH)\blmenu_$(_COMMONSOCDIR).lib               \
    $(_PLATCOMMONLIB)\$(_CPUINDPATH)\fsl_rne_pdd_$(_COMMONSOCDIR).lib          \
    $(_PLATLIB)\$(_CPUDEPPATH)\oal_spifmd.lib

SOURCES=\
    ..\eboot.c \
    ..\menu.c
	
TARGETLIBS=\
    $(TARGETLIBS) \
#   $(_PLATCOMMONLIB)\$(_CPUINDPATH)\nand_$(_SOCDIR).lib  
   
 
!IF "$(BSP_IMX6DL)" != ""

CDEFINES=$(CDEFINES) -DBSP_iMX6DL

TARGETLIBS=\
    $(TARGETLIBS) \
	$(_PLATLIB)\$(_CPUDEPPATH)\iomux_QMX6DL.lib  
	
ARM_SOURCES=\
    ..\ivt_init_d.s
!ENDIF 

!IF "$(BSP_IMX6Q)" != ""

CDEFINES=$(CDEFINES) -DBSP_iMX6Q

TARGETLIBS=\
    $(TARGETLIBS) \
	$(_PLATLIB)\$(_CPUDEPPATH)\iomux_QMX6DQ.lib  
	
ARM_SOURCES=\
    ..\ivt_init_q.s
!ENDIF 


LDEFINES=$(LDEFINES) /merge:.asecure=.astart 

!IF "$(IMGSDMMC)" != ""
ADEFINES=$(ADEFINES) -pd "IMGSDMMC SETL {TRUE}"
!ENDIF

!IF "$(IMGATA)" != ""
ADEFINES=$(ADEFINES) -pd "IMGATA SETL {TRUE}"
!ENDIF

!IF "$(IMGNAND)" != ""
ADEFINES=$(ADEFINES) -pd "IMGNAND SETL {TRUE}"
!ENDIF
