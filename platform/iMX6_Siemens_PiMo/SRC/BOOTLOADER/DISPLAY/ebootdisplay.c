//------------------------------------------------------------------------------
//
//  Copyright (C) 2010-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  ebootdisplay.cpp
//
//  Provides IPU Display Interface code for eboot.
//
//------------------------------------------------------------------------------
#include <bsp.h>
#include "Ipu_common.h"

#include "cm.h"
#include "dc.h"
#include "di.h"
#include "dp.h"
#include "cpmem.h"
#include "idmac.h"
#include "dmfc.h"
#include "panel.h"
#include "picdata.h"
//------------------------------------------------------------------------------
// External Functions


//------------------------------------------------------------------------------
// External Variables
extern PCSP_IOMUX_REGS g_pIOMUX;
extern BSP_ARGS *g_pBSPArgs;

//------------------------------------------------------------------------------
// Defines

#define DI_COMMAND_WAVEFORM 0
#define DI_DATAWR_WAVEFORM  1
#define DI_DATARD_WAVEFORM  2
#define DI_SDC_WAVEFORM     3
#define DI_SERIAL_WAVEFORM  4

#define DI_RS_SIGNAL        0
#define DI_WR_SIGNAL        1
#define DI_RD_SIGNAL        1
#define DI_SER_CLK_SIGNAL   1
#define DI_CS_SIGNAL        2 
#define DI_NOUSE_SIGNAL     3

#define DI_DEN_SIGNAL       0

#define DI_COUNTER_BASECLK  0
#define DI_COUNTER_IHSYNC   1
#define DI_COUNTER_OHSYNC   2
#define DI_COUNTER_OVSYNC   3
#define DI_COUNTER_ALINE    4
#define DI_COUNTER_ACLOCK   5

#define BGBUFFER_OFFSET (1024*1024)

typedef enum IPU_SUBMODULE_ENUM
{
    IPU_SUBMODULE_CSI0,  // Camera Sensor Interface 0 submodule
    IPU_SUBMODULE_CSI1,  // Camera Sensor Interface 1 submodule
    IPU_SUBMODULE_IC,    // Image Converter Submodule
    IPU_SUBMODULE_IRT,   // Image Rotation Submodule
    IPU_SUBMODULE_ISP,   // Image Rotation Submodule
    IPU_SUBMODULE_DP,    // Display Processor Submodule
    IPU_SUBMODULE_DI0,   // Display Interface 0 Submodule
    IPU_SUBMODULE_DI1,   // Display Interface 1 Submodule
    IPU_SUBMODULE_SMFC,  // Sensor Multi-FIFO Controller Submodule
    IPU_SUBMODULE_DC,    // Display Controller Submodule
    IPU_SUBMODULE_DMFC,  // Display Multi-FIFO Submodule
    IPU_SUBMODULE_SISG,  // Still Image Synchronization Generator Submodule
    IPU_SUBMODULE_VDI,   // Video De-Interlacer Submodule
    maxNumSubmodules,
} IPU_SUBMODULE;

typedef enum IPU_DRIVER_ENUM
{
    IPU_DRIVER_PP,       // Post-Processor driver
    IPU_DRIVER_PRP_VF,   // Pre-Processor (viewfinding) driver
    IPU_DRIVER_PRP_ENC,  // Pre-Processor (encoding) driver
    IPU_DRIVER_OTHER,    // Pre-Processor (encoding) driver
} IPU_DRIVER;
//------------------------------------------------------------------------------
// Types


//------------------------------------------------------------------------------
// Global Variables
PCSP_IPU_COMMON_REGS g_pIPUV3_COMMON;
PCSP_IPU_DC_REGS g_pIPUV3_DCCCD;
static UINT32 g_IPUGlobalFlag = 0;


dpGraphicWindowConfigData g_dispConfigData = {0};
BOOL g_bDisplayEnabled = FALSE;
UINT32 g_OverlayWidth,g_OverlayHeight;
INT32 g_OverlayXStart,g_OverlayXEnd,g_OverlayY;

//The line for progressing block
const UINT8 g_blockData[]=
{
    0x40,0xe0,0x00,
    0x40,0xe0,0x00,
    0x40,0xe0,0x00,
    0x40,0xe0,0x00,
    0x40,0xe0,0x00,
};

//------------------------------------------------------------------------------
// Local Functions

static void SyncDCConfig(DI_SELECT di_sel, PANEL_INFO* pDIPanelInfo);
static void SyncDIConfig(DI_SELECT di_sel, PANEL_INFO* pDIPanelInfo);
static void CPMEMSetup(DWORD dwIDMACChan, int width, int height, int bpp, int stride);
static void DMFCSetup(DWORD dwIDMACChan, DWORD dwWidth, DWORD dwHeight);
static BOOL DisplayInitialize(UINT32 *pPhysAddr);
static void DisplayConfigure(DI_SELECT di_sel);
static void DisplayIOMUXSetup();
DWORD IPUV3BaseGetBaseAddr(HANDLE hIPUBase);
BOOL IPUV3EnableModule(HANDLE hIPUBase, IPU_SUBMODULE submodule, IPU_DRIVER driver);

//------------------------------------------------------------------------------
//
// Function: DrawProgressBar
//
// This function draws the progress bar.
//
// Parameters:
//      uStartAddr [in]
//          The start address for drawing progress bar.
//
//      width [in]
//          The width of progress bar.
//
// Returns:
//      None.
//------------------------------------------------------------------------------

void DrawProgressBar(UINT32 uStartAddr, INT32 width)
{
    UINT16 i;
    //Draw the boundary
    // 1
    memset((UINT8 *)(uStartAddr+6),0x00,(width-4)*3);
    // 2
    memset((UINT8 *)(uStartAddr+3+g_PanelInfo.WIDTH*SCREEN_BPP/8),0x00,3);
    memset((UINT8 *)(uStartAddr+6+g_PanelInfo.WIDTH*SCREEN_BPP/8),0xFE,(width-4)*3);
    memset((UINT8 *)(uStartAddr+6+g_PanelInfo.WIDTH*SCREEN_BPP/8+(width-4)*3),0x00,3);
    // 3
    memset((UINT8 *)(uStartAddr+3+2*g_PanelInfo.WIDTH*SCREEN_BPP/8),0x00,3);
    memset((UINT8 *)(uStartAddr+6+2*g_PanelInfo.WIDTH*SCREEN_BPP/8),0xFE,(width-4)*3);
    memset((UINT8 *)(uStartAddr+6+2*g_PanelInfo.WIDTH*SCREEN_BPP/8+(width-4)*3),0x00,3);
    // 4
    memset((UINT8 *)(uStartAddr+3*g_PanelInfo.WIDTH*SCREEN_BPP/8),0x00,3);
    memset((UINT8 *)(uStartAddr+3+3*g_PanelInfo.WIDTH*SCREEN_BPP/8),0xFE,(width-2)*3);
    memset((UINT8 *)(uStartAddr+3+3*g_PanelInfo.WIDTH*SCREEN_BPP/8+(width-2)*3),0x00,3);
    // 5
    memset((UINT8 *)(uStartAddr+4*g_PanelInfo.WIDTH*SCREEN_BPP/8),0x00,3);
    memset((UINT8 *)(uStartAddr+3+4*g_PanelInfo.WIDTH*SCREEN_BPP/8),0xFE,(width-2)*3);
    memset((UINT8 *)(uStartAddr+3+4*g_PanelInfo.WIDTH*SCREEN_BPP/8+(width-2)*3),0x00,3);
    // 6
    memset((UINT8 *)(uStartAddr+5*g_PanelInfo.WIDTH*SCREEN_BPP/8),0x00,3);
    memset((UINT8 *)(uStartAddr+3+5*g_PanelInfo.WIDTH*SCREEN_BPP/8),0xFE,(width-2)*3);
    memset((UINT8 *)(uStartAddr+3+5*g_PanelInfo.WIDTH*SCREEN_BPP/8+(width-2)*3),0x00,3);
    // 7
    memset((UINT8 *)(uStartAddr+3+6*g_PanelInfo.WIDTH*SCREEN_BPP/8),0x00,3);
    memset((UINT8 *)(uStartAddr+6+6*g_PanelInfo.WIDTH*SCREEN_BPP/8),0xFE,(width-4)*3);
    memset((UINT8 *)(uStartAddr+6+6*g_PanelInfo.WIDTH*SCREEN_BPP/8+(width-4)*3),0x00,3);
    // 8
    memset((UINT8 *)(uStartAddr+3+7*g_PanelInfo.WIDTH*SCREEN_BPP/8),0x00,3);
    memset((UINT8 *)(uStartAddr+6+7*g_PanelInfo.WIDTH*SCREEN_BPP/8),0xFE,(width-4)*3);
    memset((UINT8 *)(uStartAddr+6+7*g_PanelInfo.WIDTH*SCREEN_BPP/8+(width-4)*3),0x00,3);
    // 9
    memset((UINT8 *)(uStartAddr+6+8*g_PanelInfo.WIDTH*SCREEN_BPP/8),0x00,(width-4)*3);

    //Draw the inside progress block.
    memset(OALPAtoUA(SCREEN_ADDR),0xFF,40*5*SCREEN_BPP);

    g_OverlayWidth = 40; //Should align with 8
    g_OverlayHeight = 5;
    //stride should be 120
    for(i = 0; i< 5; i++)
        memcpy(OALPAtoUA(SCREEN_ADDR + 21*i),g_blockData,15);
    for(i = 1; i< g_OverlayHeight; i++)
        memcpy(OALPAtoUA(SCREEN_ADDR + g_OverlayWidth*SCREEN_BPP/8*i),OALPAtoUA(SCREEN_ADDR),g_OverlayWidth*SCREEN_BPP/8);
}

//------------------------------------------------------------------------------
//
// Function: BMPDecoder
//
// This function decodes bmp data to the screen buffer for show.
//
// Parameters:
//      None
//
// Returns:
//      None.
//------------------------------------------------------------------------------
void BMPDecoder()
{

    UINT32 SrcStartAddr,DstStartAddr;
    UINT32 i;
    UINT8 uBGColor8[3];
    PBITMAPFILEHEADER pBMP_FH;
    PBITMAPINFOHEADER pBMP_IH;
    SrcStartAddr = (UINT32)PicData; 
    pBMP_FH = (PBITMAPFILEHEADER)SrcStartAddr;
    pBMP_IH = (PBITMAPINFOHEADER)(SrcStartAddr+sizeof(BITMAPFILEHEADER));
    SrcStartAddr += pBMP_FH->bfOffBits;
    if(g_PanelInfo.WIDTH < (UINT32)pBMP_IH->biWidth)
    {
        KITLOutputDebugString("Error: Picture width is too large for the screen.\r\n");
        memset(OALPAtoUA(SCREEN_ADDR+BGBUFFER_OFFSET),0x00,g_PanelInfo.WIDTH*g_PanelInfo.HEIGHT*SCREEN_BPP/8);
        return;
    }
    if(g_PanelInfo.HEIGHT < (UINT32)(labs(pBMP_IH->biHeight) ) )
    {
        KITLOutputDebugString("Error: Picture height is too large for the screen.\r\n");
        memset(OALPAtoUA(SCREEN_ADDR+BGBUFFER_OFFSET),0x00,g_PanelInfo.WIDTH*g_PanelInfo.HEIGHT*SCREEN_BPP/8);
        return;
    }
    //Only support 24bpp bmp
    if(pBMP_IH->biBitCount ==24)
    {
        uBGColor8[2] = *(UINT8 *)(SrcStartAddr+2);
        uBGColor8[1] = *(UINT8 *)(SrcStartAddr+1);
        uBGColor8[0] = *(UINT8 *)SrcStartAddr;
        for(i=0; i<(g_PanelInfo.WIDTH*g_PanelInfo.HEIGHT); i++)
            memcpy((UINT8 *)((UINT32)OALPAtoUA(SCREEN_ADDR+BGBUFFER_OFFSET)+i*3),uBGColor8,3);
        
        KITLOutputDebugString("Picture height:%d.width:%d\r\n",pBMP_IH->biHeight,pBMP_IH->biWidth);
        if(pBMP_IH->biHeight > 0)
        {
            DstStartAddr = ((g_PanelInfo.HEIGHT - pBMP_IH->biHeight - 20)/2 * g_PanelInfo.WIDTH 
                        + (g_PanelInfo.WIDTH - pBMP_IH->biWidth)/2)*SCREEN_BPP/8;
            for(i = 0;i < (UINT32)pBMP_IH->biHeight;i++)
            {            
                memcpy((UINT8 *)((UINT32)OALPAtoUA(SCREEN_ADDR+BGBUFFER_OFFSET)+DstStartAddr+i*g_PanelInfo.WIDTH*SCREEN_BPP/8),
                        (UINT8 *)(SrcStartAddr+(pBMP_IH->biHeight-i-1)*(pBMP_IH->biSizeImage/pBMP_IH->biHeight)),
                        pBMP_IH->biWidth*pBMP_IH->biBitCount/8);
            }
            DstStartAddr = ((g_PanelInfo.HEIGHT + pBMP_IH->biHeight +10 )/2 * g_PanelInfo.WIDTH 
                + (g_PanelInfo.WIDTH - pBMP_IH->biWidth)/2)*SCREEN_BPP/8;
            g_OverlayY = (g_PanelInfo.HEIGHT + pBMP_IH->biHeight +10 )/2 +2;    
                        
        }
        else
        {
            DstStartAddr = ((g_PanelInfo.HEIGHT + pBMP_IH->biHeight - 20)/2 * g_PanelInfo.WIDTH 
                        + (g_PanelInfo.WIDTH - pBMP_IH->biWidth)/2)*SCREEN_BPP/8;
            for(i = 0;i < (UINT32)(-pBMP_IH->biHeight);i++)
                memcpy((UINT8 *)((UINT32)OALPAtoUA(SCREEN_ADDR+BGBUFFER_OFFSET)+DstStartAddr+i*g_PanelInfo.WIDTH*SCREEN_BPP/8),
                        (UINT8 *)(SrcStartAddr+i*(pBMP_IH->biSizeImage/(-pBMP_IH->biHeight))),
                        pBMP_IH->biWidth*pBMP_IH->biBitCount/8);       
            DstStartAddr = ((g_PanelInfo.HEIGHT - pBMP_IH->biHeight +10 )/2 * g_PanelInfo.WIDTH 
                + (g_PanelInfo.WIDTH - pBMP_IH->biWidth)/2)*SCREEN_BPP/8;  
            g_OverlayY = (g_PanelInfo.HEIGHT - pBMP_IH->biHeight +10 )/2 +2;    
        }

        DrawProgressBar((UINT32)OALPAtoUA(SCREEN_ADDR+BGBUFFER_OFFSET)+DstStartAddr,
                        pBMP_IH->biWidth);
        g_OverlayXStart = (g_PanelInfo.WIDTH - pBMP_IH->biWidth)/2 - g_OverlayWidth;
        if(g_OverlayXStart < 0) 
            g_OverlayXStart = 0;
        g_OverlayXEnd = (g_PanelInfo.WIDTH + pBMP_IH->biWidth)/2;
        if(g_OverlayXEnd + g_OverlayWidth >= g_PanelInfo.WIDTH)
            g_OverlayXEnd = g_PanelInfo.WIDTH - g_OverlayWidth - 1;
    }
    else
    {
        KITLOutputDebugString("Error: Only 24bpp picture data is supported.(current bpp %d)\r\n",pBMP_IH->biBitCount);
        memset(OALPAtoUA(SCREEN_ADDR+BGBUFFER_OFFSET),0x00,g_PanelInfo.WIDTH*g_PanelInfo.HEIGHT*SCREEN_BPP/8);
    }
}

//------------------------------------------------------------------------------
//
// Function: EbootDisplayUpdate
//
// This function is used to update the content on the screen, it will be called in irq handler.
//
// Parameters:
//      None
//
// Returns:
//      None.
//------------------------------------------------------------------------------

void EbootDisplayUpdate()
{
    static short i = 1;
    static short j = 1;
    if(g_bDisplayEnabled)
    {
        if(*(UINT16 *)(PicData) != 0x4D42)
        {
            //Default logo, just change its position.
            if(g_dispConfigData.iXpos == g_PanelInfo.WIDTH-PICWIDTH-1)
                i = -1;
            else if(g_dispConfigData.iXpos == 0)
                i = 1;

            if(g_dispConfigData.iYpos == g_PanelInfo.HEIGHT-PICHEIGHT-1)
                j = -1;
            else if(g_dispConfigData.iYpos == 0)
                j = 1;

            g_dispConfigData.iXpos += i;
            g_dispConfigData.iYpos += j;
        }
        else
        {
            //BMP logo: Move the progress block.
            if(g_dispConfigData.iXpos < (UINT16)g_OverlayXEnd)
                g_dispConfigData.iXpos += 3;
            else
                g_dispConfigData.iXpos = (UINT16)g_OverlayXStart;
        }
        DPGraphicWindowConfigure(DP_CHANNEL_SYNC, &g_dispConfigData);
    }
    

}
//------------------------------------------------------------------------------
//
// Function: EbootDisplayInit
//
// This function initializes panel and show some content on the screen during eboot cycle.
//
// Parameters:
//      None
//
// Returns:
//      None.
//------------------------------------------------------------------------------
void EbootDisplayInit()
{
    UINT32 i = 0;
    UINT32 j = 0;
    UINT8 uBGColor8[3] = {0, 0, 0};
    UINT16 uBGColor16 = 0;

    DisplayInitialize((UINT32*)SCREEN_ADDR);

    //Generate logo
    if(*(UINT16 *)(PicData) != 0x4D42)
    {
        unsigned char* BlahBlahPixelPointer = NULL;
        unsigned int BlahBlahPixelNumBytes = 0;

        BlahBlahPixelPointer = (unsigned char*)OALPAtoUA(SCREEN_ADDR+BGBUFFER_OFFSET);
        BlahBlahPixelNumBytes = g_PanelInfo.WIDTH*g_PanelInfo.HEIGHT*SCREEN_BPP/8;

        //clean BG channel
        for(; BlahBlahPixelNumBytes>0; BlahBlahPixelNumBytes--)
        {
            *BlahBlahPixelPointer = 0;
            BlahBlahPixelPointer++;
        }

        //Generate default logo
        for(i=0;i< PICHEIGHT;i++)
        {
            for(j=0;j< PICWIDTH;j++)
            {
                uBGColor16 = *(UINT16 *)((UINT32)PicData+(i*PICWIDTH+j)*PICBPP/8); 
                uBGColor8[2] = (UINT8)((uBGColor16 & 0x00F800) >> 8) | ((uBGColor16 & 0x00E000) >> 13);  //r
                uBGColor8[1] = (UINT8)((uBGColor16 & 0x0007E0) >> 3) | ((uBGColor16 & 0x000600) >> 9);  //g
                uBGColor8[0] = (UINT8)((uBGColor16 & 0x00001F) << 3) | ((uBGColor16 & 0x00001C) >> 2);  //b

                BlahBlahPixelPointer = OALPAtoUA(SCREEN_ADDR);
                BlahBlahPixelPointer += (i*PICWIDTH+j)*(SCREEN_BPP/8);

                BlahBlahPixelPointer[0] = uBGColor8[0];
                BlahBlahPixelPointer[1] = uBGColor8[1];
                BlahBlahPixelPointer[2] = uBGColor8[2];
            }
        }        

        //Demo foreground colorkey and moveable overlay.
        g_dispConfigData.iXpos = (UINT16)(g_PanelInfo.WIDTH - PICWIDTH)/2;
        g_dispConfigData.iYpos = (UINT16)(g_PanelInfo.HEIGHT - PICHEIGHT)/2;
        g_dispConfigData.alpha = 0xFF;
        g_dispConfigData.bPartialPlane = DisplayPlane_1;
        g_dispConfigData.bGlobalAlpha = TRUE;
        uBGColor16 = *(UINT16 *)((UINT32)PicData);
        g_dispConfigData.colorKey = 
                    ((uBGColor16 & 0x00F800) << 8) | ((uBGColor16 & 0x00E000) << 3) |
                    ((uBGColor16 & 0x0007E0) << 5) | ((uBGColor16 & 0x000600) >> 1) |
                    ((uBGColor16 & 0x00001F) << 3) | ((uBGColor16 & 0x00001C) >> 2);    
    }
    else
    {
        //Demo bmp decoder and progress bar drawing.
        BMPDecoder();        
        g_dispConfigData.iXpos = (UINT16)g_OverlayXStart;
        g_dispConfigData.iYpos = (UINT16)g_OverlayY;
        g_dispConfigData.alpha = 0xFF;
        g_dispConfigData.bPartialPlane = DisplayPlane_0;
        g_dispConfigData.bGlobalAlpha = TRUE;
        g_dispConfigData.colorKey = 0x00fefefe;
    }

    DisplayConfigure(g_PanelInfo.DI_NUM);
    Panel_Enable(&g_PanelInfo);
    g_bDisplayEnabled = TRUE;
}
//------------------------------------------------------------------------------
//
// Function: EbootDisplayInit
//
// This function turns off display when eboot cycle is finished.
//
// Parameters:
//      None
//
// Returns:
//      None.
//------------------------------------------------------------------------------
void EbootDisplayOff()
{
    OUTREG32(&g_pIPUV3_COMMON->IPU_CONF,0);
    g_bDisplayEnabled = FALSE;
    Panel_Disable(&g_PanelInfo);
    DPGraphicWindowEnable(DP_CHANNEL_SYNC,FALSE);
    
}
//------------------------------------------------------------------------------
//
// Function: CPMEMSetup
//
// This function initializes the CPMEM based on the display properties.
//
// Parameters:
//      dwIDMACChan
//          [in] Selects IDMAC channel to configure
//
//      width
//          [in] Width of the frame.
//
//      height
//          [in] Height of the frame.
//
//      bpp
//          [in] Bits per pixel for the frame pixel data.
//
//      stride
//          [in] Stride value for the frame surface.
//
// Returns:
//      None.
//------------------------------------------------------------------------------
static void CPMEMSetup(DWORD dwIDMACChan, int width, int height, int bpp, int stride)
{
    CPMEMConfigData CPMEMData;
    
    //********************************
    // Special Mode parameters
    //********************************
    memset(&CPMEMData,0,sizeof(CPMEMData));
    CPMEMData.iAccessDimension = CPMEM_DIM_2D;
    CPMEMData.iScanOrder = CPMEM_SO_PROGRESSIVE;
    CPMEMData.iBandMode = CPMEM_BNDM_DISABLE;
    CPMEMData.iBlockMode = CPMEM_BM_DISABLE;
    CPMEMData.iThresholdEnable = CPMEM_THE_DISABLE;
    CPMEMData.iCondAccessEnable = CPMEM_CAE_COND_SKIP_DISABLE;
    CPMEMData.iAXI_Id = CPMEM_ID_0; 

    //********************************
    // Format and dimensions
    //********************************
    CPMEMData.iPixelFormat = CPMEM_PFS_INTERLEAVED_RGB;
    CPMEMData.iHeight = (UINT16)height - 1;
    CPMEMData.iWidth = (UINT16)width - 1;

    CPMEMData.iLineStride = stride - 1;

    switch (bpp)
    {
        case 16:
            CPMEMData.iBitsPerPixel = CPMEM_BPP_16;
            CPMEMData.iPixelBurst = CPMEM_PIXEL_BURST_16;
            CPMEMData.pixelFormatData.component0_width = 5-1;
            CPMEMData.pixelFormatData.component1_width = 6-1;
            CPMEMData.pixelFormatData.component2_width = 5-1;
            CPMEMData.pixelFormatData.component3_width = 0;
            CPMEMData.pixelFormatData.component0_offset = 0;
            CPMEMData.pixelFormatData.component1_offset = 5;
            CPMEMData.pixelFormatData.component2_offset = 11;
            CPMEMData.pixelFormatData.component3_offset = 0;
            break;
        case 24:
            CPMEMData.iBitsPerPixel = CPMEM_BPP_24;
            CPMEMData.iPixelBurst = CPMEM_PIXEL_BURST_16;
            CPMEMData.pixelFormatData.component0_width = 8-1;
            CPMEMData.pixelFormatData.component1_width = 8-1;
            CPMEMData.pixelFormatData.component2_width = 8-1;
            CPMEMData.pixelFormatData.component3_width = 0;
            CPMEMData.pixelFormatData.component0_offset = 0;
            CPMEMData.pixelFormatData.component1_offset = 8;
            CPMEMData.pixelFormatData.component2_offset = 16;
            CPMEMData.pixelFormatData.component3_offset = 0;
            break;
        case 32:
            CPMEMData.iBitsPerPixel = CPMEM_BPP_32;
            CPMEMData.iPixelBurst = CPMEM_PIXEL_BURST_16;
            CPMEMData.pixelFormatData.component0_width = 8-1;
            CPMEMData.pixelFormatData.component1_width = 8-1;
            CPMEMData.pixelFormatData.component2_width = 8-1;
            CPMEMData.pixelFormatData.component3_width = 8-1;
            CPMEMData.pixelFormatData.component0_offset = 8;
            CPMEMData.pixelFormatData.component1_offset = 16;
            CPMEMData.pixelFormatData.component2_offset = 24;
            CPMEMData.pixelFormatData.component3_offset = 0;
            break;
        default:
            // error
            break;
    }
    if((width%16)!=0)
        CPMEMData.iPixelBurst = CPMEM_PIXEL_BURST_8;   
    // We now write CPMEM data into the CPMEM
    CPMEMWrite(dwIDMACChan, &CPMEMData, TRUE);
}


//------------------------------------------------------------------------------
//
// Function: DMFCSetup
//
// This function initializes the DMFC based on the display properties.
//
// Parameters:
//      dwIDMACChan
//          [in] Selects IDMAC channel to configure in DMFC.
//
//      di_sel
//          [in] Selects DI0 or DI1 for setup.
//
//      dwWidth
//          [in] Frame width coming from IDMAC
//
//      bpp
//          [in] Bits per pixel for the frame pixel data.
//
// Returns:
//      None.
//------------------------------------------------------------------------------
static void DMFCSetup(DWORD dwIDMACChan, DWORD dwWidth, DWORD dwHeight)
{
    dmfcConfigData dmfcData;
    UINT32 iScale = 1;

    if(dwIDMACChan == IDMAC_CH_DP_PRIMARY_FLOW_MAIN_PLANE)
    {
        iScale = 2;
    }
    else if(dwIDMACChan == IDMAC_CH_DP_PRIMARY_FLOW_AUX_PLANE)
    {
        iScale = 3;
    }


    // There is a required relationship between the incoming frame's
    // width, and the FIFO size that must be configured for that frame
    if ((dwWidth * 32 * iScale) < (4 * 128))
    {
        DEBUGMSG(1, (TEXT("%s: IDMAC frame width is too small! DMFC FIFO size must be smaller than frame width * 2.\r\n"), __WFUNCTION__));
        // Use smallest setting and hope that it works.
        dmfcData.FIFOSize = IPU_DMFC_CHAN_DMFC_FIFO_SIZE_4x128;
    }
    else if ((dwWidth * 32 * iScale) < (8 * 128))
    {
        dmfcData.FIFOSize = IPU_DMFC_CHAN_DMFC_FIFO_SIZE_4x128;
    }
    else if ((dwWidth * 32 * iScale) < (16 * 128))
    {
        dmfcData.FIFOSize = IPU_DMFC_CHAN_DMFC_FIFO_SIZE_8x128;
    }
    else if ((dwWidth * 32 * iScale) < (32 * 128))
    {
        dmfcData.FIFOSize = IPU_DMFC_CHAN_DMFC_FIFO_SIZE_16x128;
    }
    else if ((dwWidth * 32 * iScale) < (64 * 128))
    {
        dmfcData.FIFOSize = IPU_DMFC_CHAN_DMFC_FIFO_SIZE_32x128;
    }
    else if ((dwWidth * 32 * iScale) < (128 * 128))
    {
        dmfcData.FIFOSize = IPU_DMFC_CHAN_DMFC_FIFO_SIZE_64x128;
    }
    else if (dwWidth > 1024)
    {
        dmfcData.FIFOSize = IPU_DMFC_CHAN_DMFC_FIFO_SIZE_256x128;
    }
    else
    {
        dmfcData.FIFOSize = IPU_DMFC_CHAN_DMFC_FIFO_SIZE_128x128;    
    }

    dmfcData.FrameWidth = (UINT16)dwWidth;
    dmfcData.FrameHeight = (UINT16)dwHeight;
    dmfcData.BurstSize = IPU_DMFC_BURST_SIZE_0_32_WORDS;
    if(dwIDMACChan == IDMAC_CH_DP_PRIMARY_FLOW_AUX_PLANE)
    {
        dmfcData.StartSeg = IPU_DMFC_CHAN_DMFC_ST_ADDR_SEGMENT_4;
    }
    else
    {
        dmfcData.StartSeg = IPU_DMFC_CHAN_DMFC_ST_ADDR_SEGMENT_0;
    }
    dmfcData.WaterMarkEnable = IPU_DMFC_WM_EN_DISABLE;

    //Only useful when watermarkenable is set.
    dmfcData.WaterMarkClear = 7; //When number of bursts in FIFO equal or less than 7 the watermark will clear. (decrease)
    dmfcData.WaterMarkSet = 5; //When number of bursts in FIFO equal or more than 5 the watermark will set.(increase) 
    // Don't care values
    dmfcData.DestChannel = 0; // Don't care unless using IC->DMFC path
    dmfcData.PixelPerWord = IPU_DMFC_PPW_C_8BPP; // Only care for DC Read Ch or IC->DMFC path

    DMFCConfigure(dwIDMACChan, &dmfcData);

}

//------------------------------------------------------------------------------
//
// Function: DisplayInitialize
//
// This function completes several initialization tasks:
//  1) Initializes structures needed to access IPUv3
//      submodule registers.
//  2) Initializes the IPUv3 registers to display to
//      dumb display device.
//  3) Initializes IOMUX settings for display-related pins.
//
// Parameters:
//      pPhysAddr
//          [in] Physical address of the frame buffer, used to
//          initialize the buffer pointers for the display flows.
//
// Returns:
//      TRUE if successful, FALSE if failure.
//------------------------------------------------------------------------------
static BOOL DisplayInitialize(UINT32 *pPhysAddr)
{
    BOOL retVal = TRUE;
    
    g_OverlayWidth = PICWIDTH;
    g_OverlayHeight = PICHEIGHT;

    // Perform one-time initialization of registers
    // for IPUv3 components that will be used
    g_pIPUV3_COMMON = (PCSP_IPU_COMMON_REGS)OALPAtoUA(IPUV3BaseGetBaseAddr(NULL)
                                            +CSP_IPUV3_IPU_COMMON_REGS_OFFSET);

    DCRegsInit();
    DIRegsInit();
    DMFCRegsInit();
    DPRegsInit();
    CPMEMRegsInit();
    IDMACRegsInit();
    //DMFCInitialize(); //Set a default value for all dmfc channel.


    //Configure displayIOMUX
    DisplayIOMUXSetup();

    // Initialize CPMEM source buffer before starting
    CPMEMWriteBuffer(IDMAC_CH_DP_PRIMARY_FLOW_MAIN_PLANE,0,(UINT32 *)((UINT32)pPhysAddr+BGBUFFER_OFFSET));
    CPMEMWriteBuffer(IDMAC_CH_DP_PRIMARY_FLOW_AUX_PLANE,0,pPhysAddr);
    
    return retVal;
}

//------------------------------------------------------------------------------
//
// Function: DisplayConfigure
//
// This function configures IPUv3 submodule for drive dumb display.
//
// Parameters:
//      di_sel
//          [in] Selects either DI0 or DI1 to configure
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
static void DisplayConfigure(DI_SELECT di_sel)
{
    INT32 bpp;
    UINT32 IPUClk;
    PANEL_INFO *pDIPanelInfo;
    pDIPanelInfo = &g_PanelInfo;

    bpp = SCREEN_BPP;

    if(pDIPanelInfo->SYNC_TYPE != IPU_PANEL_SYNC_TYPE_SYNCHRONOUS)
    {
        RETAILMSG(1,(TEXT("Incorrect panel type. Eboot only supports synchronous panel.\r\n")));
        return;
    }
    // Set up IPUv3 DC
    // Only support Configuration for non-interlaced synchronous displays
    // Call function to configure DC for Sync panel (progressive)
    SyncDCConfig(di_sel, pDIPanelInfo);


    // Set up IPUv3 DI
    // Retrieve HSP clock frequency value
    IPUClk = g_pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPU1_HSP];
    DISetIPUClkFreq(IPUClk);
    DISetDIClkFreq(di_sel,IPUClk);

    if(pDIPanelInfo->DICLK_SOURCE == DISPLAY_CLOCK_EXTERNAL)
    {
        //To set the correct di clock, revert the ipu clock temporarily.
        DISetIPUClkFreq(pDIPanelInfo->PIX_CLK_FREQ);
        DISetDIClkFreq(di_sel,pDIPanelInfo->PIX_CLK_FREQ );
        //Set the correct IPU clock.
        DISetIPUClkFreq(IPUClk);
    }

    // Configuration for non-interlaced synchronous displays
    SyncDIConfig(di_sel, pDIPanelInfo);

    if(pDIPanelInfo->SYNC_TYPE == IPU_PANEL_SYNC_TYPE_SYNCHRONOUS)
    {
        //************************************************************************
        // Configure Channel 23 for Sync display operation (both UI-only and UI+Video)
        //************************************************************************

        // Set up CPMEM
        CPMEMSetup(IDMAC_CH_DP_PRIMARY_FLOW_MAIN_PLANE, pDIPanelInfo->WIDTH, pDIPanelInfo->HEIGHT, bpp, pDIPanelInfo->WIDTH*bpp/8);

        // Set up IPUv3 DMFC
        DMFCSetup(IDMAC_CH_DP_PRIMARY_FLOW_MAIN_PLANE, pDIPanelInfo->WIDTH, pDIPanelInfo->HEIGHT);

        //enable DI
        if(di_sel == DI_SELECT_DI0)
            IPUV3EnableModule(NULL, IPU_SUBMODULE_DI0, IPU_DRIVER_OTHER);
        else
            IPUV3EnableModule(NULL, IPU_SUBMODULE_DI1, IPU_DRIVER_OTHER);
        //enable DP
        IPUV3EnableModule(NULL, IPU_SUBMODULE_DP, IPU_DRIVER_OTHER);
        //enable DC
        IPUV3EnableModule(NULL, IPU_SUBMODULE_DC, IPU_DRIVER_OTHER);
        //enable DMFC
        IPUV3EnableModule(NULL, IPU_SUBMODULE_DMFC, IPU_DRIVER_OTHER);

        Panel_Initialize(pDIPanelInfo);
        //CMSetDispFlow(IPU_DISP_FLOW_DP_SYNC0_SRC, 0 /* MCU is source */);
        IDMACChannelEnable(IDMAC_CH_DP_PRIMARY_FLOW_MAIN_PLANE);
        //OUTREG32(&g_pIPUV3_IDMAC->IDMAC_CH_EN_1, (1 << IDMAC_CH_DP_PRIMARY_FLOW_MAIN_PLANE));
        
        //Enable DC channel.
        DCChangeChannelMode(DC_CHANNEL_DP_PRIMARY, 
            IPU_DC_WR_CH_CONF_PROG_CHAN_TYP_NORMAL_NO_ANTI_TEARING);

        //Start DI Counters;
        if (di_sel == DI_SELECT_DI0)
        {
            OUTREG32(&g_pIPUV3_COMMON->IPU_DISP_GEN, (1 << IPU_IPU_DISP_GEN_DI0_COUNTER_RELEASE_LSH));
        }
        else
        {
            OUTREG32(&g_pIPUV3_COMMON->IPU_DISP_GEN, (1 << IPU_IPU_DISP_GEN_DI1_COUNTER_RELEASE_LSH));
        }

        
    }
    else
    {
        // TODO: Support not synchronous panel
    }
    
    //Configure and enable DP FG
    // Set up CPMEM
    CPMEMSetup(IDMAC_CH_DP_PRIMARY_FLOW_AUX_PLANE, g_OverlayWidth , g_OverlayHeight, SCREEN_BPP, g_OverlayWidth*SCREEN_BPP/8);
    
    // Set up IPUv3 DMFC
    DMFCSetup(IDMAC_CH_DP_PRIMARY_FLOW_AUX_PLANE, g_OverlayWidth , g_OverlayHeight);

    DPGraphicWindowConfigure(DP_CHANNEL_SYNC, &g_dispConfigData);
    IDMACChannelEnable(IDMAC_CH_DP_PRIMARY_FLOW_AUX_PLANE);
    DPGraphicWindowEnable(DP_CHANNEL_SYNC,TRUE);

}


//------------------------------------------------------------------------------
//
// Function: DisplayIOMUXSetup
//
// This function makes the DDK call to configure the IOMUX
// pins required for the Display.
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
static void DisplayIOMUXSetup()
{
#if 0
    // DATA 0
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DISP0_DAT0, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DISP0_DAT0, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);

    // DATA 1
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DISP0_DAT1, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DISP0_DAT1, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);


    // DATA 2
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DISP0_DAT2, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DISP0_DAT2, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);



    // DATA 3
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DISP0_DAT3, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DISP0_DAT3, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);



    // DATA 4
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DISP0_DAT4, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DISP0_DAT4, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);


    // DATA 5
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DISP0_DAT5, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DISP0_DAT5, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);


    // DATA 6
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DISP0_DAT6, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DISP0_DAT6, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);


    // DATA 7
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DISP0_DAT7, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DISP0_DAT7, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);


    // DATA 8
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DISP0_DAT8, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DISP0_DAT8, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);


    // DATA 9
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DISP0_DAT9, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DISP0_DAT9, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);


    // DATA 10
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DISP0_DAT10, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DISP0_DAT10, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);



    // DATA 11
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DISP0_DAT11, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DISP0_DAT11, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);


    // DATA 12
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DISP0_DAT12, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DISP0_DAT12, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);


    // DATA 13
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DISP0_DAT13, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DISP0_DAT13, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);


    // DATA 14
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DISP0_DAT14, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DISP0_DAT14, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);


    // DATA 15
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DISP0_DAT15, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DISP0_DAT15, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);


    // DATA 16
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DISP0_DAT16, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DISP0_DAT16, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);


    // DATA 17
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DISP0_DAT17, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DISP0_DAT17, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);

    // DATA 18
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DISP0_DAT18, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DISP0_DAT18, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);


    // DATA 19
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DISP0_DAT19, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DISP0_DAT19, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);


    // DATA 20
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DISP0_DAT20, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DISP0_DAT20, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);


    // DATA 21
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DISP0_DAT21, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DISP0_DAT21, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);


    // DATA 22
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DISP0_DAT22, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DISP0_DAT22, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);


    // DATA 23
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DISP0_DAT23, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DISP0_DAT23, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);



    // Additional configuration for Synchronous display panel pins

    // HSYNC - PIN2
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DI0_PIN2, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DI0_PIN2, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);


    // VSYNC - PIN3
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DI0_PIN3, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DI0_PIN3, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);


    // DRDY - PIN15 
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DI0_PIN15, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    // No IOMUX configuration required, as there is no IOMUXing for this pin
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DI0_PIN15, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);


    // PCLK - DISP_CLK
    OAL_IOMUX_SET_MUX(g_pIOMUX, DDK_IOMUX_PIN_DI0_DISP_CLK, DDK_IOMUX_PIN_MUXMODE_ALT0, 
                        DDK_IOMUX_PIN_SION_REGULAR);
    // No IOMUX configuration required, as there is no IOMUXing for this pin
    OAL_IOMUX_SET_PAD(g_pIOMUX,DDK_IOMUX_PAD_DI0_DISP_CLK, 
                         DDK_IOMUX_PAD_SLEW_FAST, 
                         DDK_IOMUX_PAD_DRIVE_240_OHM, 
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ, 
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE, 
                         DDK_IOMUX_PAD_PULL_UP_100K, 
                         DDK_IOMUX_PAD_HYSTERESIS_DISABLE,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);    
#endif
    return;
}

//------------------------------------------------------------------------------
//
// Function: SyncDCConfig
//
// This function configures the DC for a Synchronous panel taking
// non-interlaced (progressive) data.
//
// Parameters:
//      di_sel
//          [in] Selects either DI0 or DI1 to configure
//
//      pDIPanelInfo
//          [in] Panel info used in configuration process.
//
//      pMappingCounter
//          [in/out] Mapping counter used to configure bus mappings.  Updated
//          value will be passed back to calling function
//
//      pDispCounter
//          [in/out] Display number counter used to configure display data.  Updated
//          value will be passed back to calling function
//
//      pMicroCodeAddr
//          [in/out] Microcode address counter used to configure microcode instructions.
//          Updated value will be passed back to calling function
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
static void SyncDCConfig(DI_SELECT di_sel, PANEL_INFO* pDIPanelInfo)
{
    MicrocodeConfigData microcodeData;
    DCWriteChanConfData writeConfData;
    DCGeneralData genData;
    DCDispConfData dispConfData;
    BusMappingData mappingData;
    static int mappingCounterStartVal;
    int i;
    static UINT32 microcodeStartAddr[2] = {0};
    //UINT32 tempMicrocodeAddr;

    // Use local dereferenced value for mappingCounter, dispCounter, microcodeAddr
    // Restore reference values at end of function
    int mappingCounter = 0;
    UINT32 iMicroCodeAddr = 0;

    //********************************************************************
    // Configure Write Channel 5(main channel to write pixels to display)
    //********************************************************************
    writeConfData.dwStartTime = 0; //not needed: no anti tearing
    writeConfData.dwChanMode = IPU_DC_WR_CH_CONF_PROG_CHAN_TYP_DISABLE;
    writeConfData.dwEventMask = IPU_DC_CHAN_MASK_DEFAULT_HIGHEST_PRI;
    writeConfData.dwFieldMode = IPU_DC_WR_CH_CONF_FIELD_MODE_FRAME_MODE;
    writeConfData.dwDispNum = (DWORD)pDIPanelInfo->DISPLAY_NUM;
    writeConfData.dwDINum = di_sel;
    writeConfData.dwWordSize = IPU_DC_W_SIZE_24BITS;
    DCConfigureWriteChannel(DC_CHANNEL_DP_PRIMARY, &writeConfData);
    // Set Write Channel 5 start address
    DCSetWriteChannelAddress(DC_CHANNEL_DP_PRIMARY, 0);

    //*********************************************
    // Configure Write Channel 1 (Disabled it for DI0)
    //*********************************************
    writeConfData.dwStartTime = 0; //not needed: no anti tearing
    writeConfData.dwChanMode = IPU_DC_WR_CH_CONF_PROG_CHAN_TYP_DISABLE;
    writeConfData.dwEventMask = IPU_DC_CHAN_MASK_DEFAULT_HIGHEST_PRI;
    writeConfData.dwFieldMode = IPU_DC_WR_CH_CONF_FIELD_MODE_FRAME_MODE;
    writeConfData.dwDispNum = (DWORD)pDIPanelInfo->DISPLAY_NUM;
    writeConfData.dwDINum = 1;  // We must use the opposite DI setting as that used for Channel 1 (to disable it)
    writeConfData.dwWordSize = IPU_DC_W_SIZE_24BITS;
    DCConfigureWriteChannel(DC_CHANNEL_DC_SYNC_OR_ASYNC, &writeConfData);

    
    //*********************************************
    // Configure Display port
    //*********************************************
    dispConfData.dwDispType = IPU_DC_DISP_CONF1_DISP_TYP_PARALLEL_NO_BYTE_EN;
    dispConfData.dwAddrIncrement = IPU_DC_DISP_CONF1_ADDR_INCREMENT_1;
    dispConfData.dwByteEnableAddrIncrement = IPU_DC_DISP_CONF1_ADDR_BE_L_INC_0;
    dispConfData.dwAddrCompareMode = 0;
    dispConfData.dwPollingValueAndMaskSelect = 0;
    DCConfigureDisplay(pDIPanelInfo->DISPLAY_NUM, &dispConfData);

    DCSetDisplayStride(pDIPanelInfo->DISPLAY_NUM, pDIPanelInfo->WIDTH);

    //*********************************************
    // Configure DC General
    //*********************************************
    genData.dwBlinkEnable = IPU_DC_GEN_DC_BK_EN_DISABLE;
    genData.dwBlinkRate = 0;
    genData.dwCh5SyncSel = IPU_DC_GEN_DC_CH5_TYPE_SYNC;
    genData.dwCh1Priority = IPU_DC_GEN_SYNC_PRIORITY_1_LOW;
    genData.dwCh5Priority = IPU_DC_GEN_SYNC_PRIORITY_5_HIGH;
    genData.dwMaskChanSelect = IPU_DC_GEN_MASK4CHAN_5_DC;
    genData.dwMaskChanEnable = IPU_DC_GEN_MASK_EN_DISABLE;
    genData.dwCh1SyncSel = IPU_DC_GEN_SYNC_1_6_CH1_HANDLES_SYNC;
    DCConfigureGeneralData(&genData);

    //*********************************************
    // User events configuration
    //*********************************************
    // Disable Events 0-3
    DCDisableUserEventData(DC_USER_GENERAL_DATA_EVENT_0);
    DCDisableUserEventData(DC_USER_GENERAL_DATA_EVENT_1);
    DCDisableUserEventData(DC_USER_GENERAL_DATA_EVENT_2);
    DCDisableUserEventData(DC_USER_GENERAL_DATA_EVENT_3);


    //*********************************************
    // Create Bus Mappings
    //*********************************************
    // Configure Bus Mapping Data for data
    // This code can only be run one time for each panel.

    // Use mapping counter to assign pointers for each bus mapping
    // These pointers will be used later when creating microcode instructions
    mappingCounterStartVal = mappingCounter;

    // Create a bus mapping for each data transfer cycle
    while (mappingCounter < pDIPanelInfo->TRANSFER_CYCLES + mappingCounterStartVal)
    {
        DWORD mappingIndex = mappingCounter-mappingCounterStartVal;

        // Configure Bus Mapping Data for data
        mappingData.dwComponent2Offset = pDIPanelInfo->MAPPINGS[mappingIndex].dwComponent2Offset;
        mappingData.dwComponent2Mask = pDIPanelInfo->MAPPINGS[mappingIndex].dwComponent2Mask;
        mappingData.dwComponent1Offset = pDIPanelInfo->MAPPINGS[mappingIndex].dwComponent1Offset;
        mappingData.dwComponent1Mask = pDIPanelInfo->MAPPINGS[mappingIndex].dwComponent1Mask;
        mappingData.dwComponent0Offset = pDIPanelInfo->MAPPINGS[mappingIndex].dwComponent0Offset;
        mappingData.dwComponent0Mask = pDIPanelInfo->MAPPINGS[mappingIndex].dwComponent0Mask;
        DCConfigureDataMapping(mappingCounter, &mappingData);

        mappingCounter++;
    }

    // Mark the microcode start address
    microcodeStartAddr[di_sel] = iMicroCodeAddr;

    //*******************************************************************
    // Use several Events to trigger main screen update configuration
    // NEW_LINE
    //*******************************************************************
    DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_NEW_LINE, 3, iMicroCodeAddr);
    DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_NEW_FRAME, 0, 0);
    DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_NEW_FIELD, 0, 0);
    DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_END_OF_FRAME, 0, 0);
    DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_END_OF_FIELD, 0, 0);
    DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_NEW_CHAN, 0, 0);
    DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_NEW_ADDR, 0, 0);

    //*******************************************************
    // Microcode sequence to refresh sync panel
    //*******************************************************

    // Create a microcode instruction for each transfer cycle
    for (i= mappingCounterStartVal; i < pDIPanelInfo->TRANSFER_CYCLES + mappingCounterStartVal; i++)
    {
        // Write Data to Sync panel
        microcodeData.dwWord = iMicroCodeAddr++;
        microcodeData.dwStop = (i+1 >= pDIPanelInfo->TRANSFER_CYCLES + mappingCounterStartVal) ?
            DC_TEMPLATE_STOP_LAST_COMMAND : DC_TEMPLATE_STOP_CONTINUE; // If this is our last cycle, end the instruction sequence
        microcodeData.Command = DC_MICROCODE_COMMAND_WROD;
        microcodeData.dwLf = 0;
        microcodeData.dwAf = 0;
        microcodeData.dwOperand = 0;
        microcodeData.dwMapping = i + 1; // i here should equal the bus mapping pointer configured above
        microcodeData.dwWaveform = DI_SDC_WAVEFORM + 1;
        microcodeData.dwGluelogic = 0x8;
        microcodeData.dwSync = DI_COUNTER_ACLOCK;
        DCConfigureMicrocode(&microcodeData);
    }

    //*******************************************************************
    // Use several Events to trigger main screen update configuration
    // NEW_LINE
    //*******************************************************************
    DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_END_OF_LINE, 2, iMicroCodeAddr);

    // Create a microcode instruction for each transfer cycle
    for (i= mappingCounterStartVal; i < pDIPanelInfo->TRANSFER_CYCLES + mappingCounterStartVal; i++)
    {
        // Write Data to Sync panel
        microcodeData.dwWord = iMicroCodeAddr++;
        microcodeData.dwStop = (i+1 >= pDIPanelInfo->TRANSFER_CYCLES + mappingCounterStartVal) ?
            DC_TEMPLATE_STOP_LAST_COMMAND : DC_TEMPLATE_STOP_CONTINUE; // If this is our last cycle, end the instruction sequence
        microcodeData.Command = DC_MICROCODE_COMMAND_WROD;
        microcodeData.dwLf = 0;
        microcodeData.dwAf = 0;
        microcodeData.dwOperand = 0;
        microcodeData.dwMapping = i + 1; // i here should equal the bus mapping pointer configured above
        microcodeData.dwWaveform = DI_SDC_WAVEFORM + 1;
        microcodeData.dwGluelogic = 0x4;
        microcodeData.dwSync = DI_COUNTER_ACLOCK;
        DCConfigureMicrocode(&microcodeData);
    }

    //*******************************************************************
    // Use NEW_DATA Event to trigger write to panel
    //*******************************************************************
    // Create microcode event for DC channel 1 data incoming
    DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_NEW_DATA, 1, iMicroCodeAddr);

    //*******************************************************************
    // Create the main screen update instruction.
    // Here, we send pixel data to the panel as we receive it.
    //*******************************************************************
    // Create a microcode instruction for each transfer cycle
    for (i= mappingCounterStartVal; i < pDIPanelInfo->TRANSFER_CYCLES + mappingCounterStartVal; i++)
    {
        // Write Data to Sync panel
        microcodeData.dwWord = iMicroCodeAddr++;
        microcodeData.dwStop = (i+1 >= pDIPanelInfo->TRANSFER_CYCLES + mappingCounterStartVal) ?
            DC_TEMPLATE_STOP_LAST_COMMAND : DC_TEMPLATE_STOP_CONTINUE; // If this is our last cycle, end the instruction sequence
        microcodeData.Command = DC_MICROCODE_COMMAND_WROD;
        microcodeData.dwLf = 0;
        microcodeData.dwAf = 0;
        microcodeData.dwOperand = 0;
        microcodeData.dwMapping = i + 1; // i here should equal the bus mapping pointer configured above
        microcodeData.dwWaveform = DI_SDC_WAVEFORM + 1;
        microcodeData.dwGluelogic = 0x0;
        microcodeData.dwSync = DI_COUNTER_ACLOCK;
        DCConfigureMicrocode(&microcodeData);
    }


    // Set ISINITIALIZED to TRUE, unless we still need to configure serial setup, in
    // which case we wait and allow it to be set to TRUE after serial setup.
    if (!pDIPanelInfo->USESERIALPORT)
    {
        pDIPanelInfo->ISINITIALIZED = TRUE;
    }
}

//------------------------------------------------------------------------------
//
// Function: SyncDIConfig
//
// This function configures the DI for a synchronous panel.
//
// Parameters:
//      di_sel
//          [in] Selects either DI0 or DI1 to configure
//
//      pDIPanelInfo
//          [in] Panel info used in configuration process.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
static void SyncDIConfig(DI_SELECT di_sel, PANEL_INFO* pDIPanelInfo)
{
    DIPointerConfigData ptrConfData;
    DIUpDownConfigData upDownConfData;
    DIPolarityConfigData polConfData;
    DIGeneralConfigData genConfData;
    DISyncConfigData SyncConfigData;

    // Signal waveform for command write
    upDownConfData.dwPointer  = DI_SDC_WAVEFORM;
    upDownConfData.dwSet  = DI_DEN_SIGNAL;
    upDownConfData.dwUpPos = 0;
    upDownConfData.dwDownPos = (UINT32)((LONGLONG)1000000000*2/pDIPanelInfo->PIX_CLK_FREQ) + 1;
    DIConfigureUpDown(di_sel, &upDownConfData);

    upDownConfData.dwSet  = DI_NOUSE_SIGNAL;  //NULL SIGNAL
    upDownConfData.dwUpPos = 0;
    upDownConfData.dwDownPos = 0;
    DIConfigureUpDown(di_sel, &upDownConfData);

    ptrConfData.dwPointer  = upDownConfData.dwPointer;
    ptrConfData.dwAccessPeriod = (UINT32)((LONGLONG)1000000000/pDIPanelInfo->PIX_CLK_FREQ) + 1;
    ptrConfData.dwComponentPeriod = (UINT32)((LONGLONG)1000000000/pDIPanelInfo->PIX_CLK_FREQ) + 1;
    ptrConfData.dwCSPtr= DI_NOUSE_SIGNAL;
    ptrConfData.dwPinPtr[0]= DI_NOUSE_SIGNAL;  //pin11 clear
    ptrConfData.dwPinPtr[1]= DI_NOUSE_SIGNAL;  //pin12 clear
    ptrConfData.dwPinPtr[2]= DI_NOUSE_SIGNAL;  //pin13 clear 
    ptrConfData.dwPinPtr[3]= DI_NOUSE_SIGNAL;  //pin14  clear
    ptrConfData.dwPinPtr[4]= DI_DEN_SIGNAL;    //pin15  DEN
    ptrConfData.dwPinPtr[5]= DI_NOUSE_SIGNAL;  //pin16  clear
    ptrConfData.dwPinPtr[6]= DI_NOUSE_SIGNAL;  //pin17  clear
    DIConfigurePointer(di_sel, &ptrConfData);

    DIConfigureBaseSyncClockGen(di_sel, 
                                pDIPanelInfo->PIX_DATA_POS, 
                                pDIPanelInfo->PIX_CLK_FREQ,
                                pDIPanelInfo->PIX_CLK_UP,
                                pDIPanelInfo->PIX_CLK_DOWN);

    DISetScreenHeight(di_sel, pDIPanelInfo->HEIGHT+pDIPanelInfo->VSTARTWIDTH+pDIPanelInfo->VENDWIDTH+pDIPanelInfo->VSYNCWIDTH-1);

    // TODO: REMOVE THE HARDCODE
    //internal HSYNC
    SyncConfigData.dwPointer = DI_COUNTER_IHSYNC;
    SyncConfigData.dwRunValue = pDIPanelInfo->WIDTH+pDIPanelInfo->HSTARTWIDTH+pDIPanelInfo->HENDWIDTH+pDIPanelInfo->HSYNCWIDTH-1;
    SyncConfigData.dwRunResolution =DI_COUNTER_BASECLK+1;
    SyncConfigData.dwOffsetValue=0;
    SyncConfigData.dwOffsetResolution=0;
    SyncConfigData.dwPolarityClearSelect=0;
    SyncConfigData.dwToggleTriggerSelect=0;
    SyncConfigData.dwCounterClearSelect=0;
    SyncConfigData.bCounterAutoReload=1;
    SyncConfigData.dwPolarityGeneratorEnable=0;
    SyncConfigData.dwUpPos=0;   // in pixel clock
    SyncConfigData.dwDownPos=0; // in pixel clock
    SyncConfigData.dwStepRepeat=0;
    DIConfigureSync(di_sel,&SyncConfigData);

    //output HSYNC
    SyncConfigData.dwPointer = DI_COUNTER_OHSYNC;
    SyncConfigData.dwRunValue = pDIPanelInfo->WIDTH+pDIPanelInfo->HSTARTWIDTH+pDIPanelInfo->HENDWIDTH+pDIPanelInfo->HSYNCWIDTH-1;
    SyncConfigData.dwRunResolution =DI_COUNTER_BASECLK+1;
    SyncConfigData.dwOffsetValue=0;
    SyncConfigData.dwOffsetResolution=0;
    SyncConfigData.dwPolarityClearSelect=0;
    SyncConfigData.dwToggleTriggerSelect=IPU_DI_SW_GEN1_DI_CNT_POLARITY_TRIGGER_SEL_DISP_CLK;
    SyncConfigData.dwCounterClearSelect=0;
    SyncConfigData.bCounterAutoReload=1;
    SyncConfigData.dwPolarityGeneratorEnable=1;
    SyncConfigData.dwUpPos=0;                          // in pixel clocks
    SyncConfigData.dwDownPos=pDIPanelInfo->HSYNCWIDTH; // in units of pixel clocks
    SyncConfigData.dwStepRepeat=0;
    DIConfigureSync(di_sel,&SyncConfigData);

    //Output Vsync
    SyncConfigData.dwPointer = DI_COUNTER_OVSYNC;
    SyncConfigData.dwRunValue = pDIPanelInfo->HEIGHT+pDIPanelInfo->VSTARTWIDTH+pDIPanelInfo->VENDWIDTH+pDIPanelInfo->VSYNCWIDTH-1;
    SyncConfigData.dwRunResolution =DI_COUNTER_OHSYNC+1;
    SyncConfigData.dwOffsetValue=0;
    SyncConfigData.dwOffsetResolution=0;
    SyncConfigData.dwPolarityClearSelect=0;
    SyncConfigData.dwToggleTriggerSelect=DI_COUNTER_OHSYNC+1;
    SyncConfigData.dwCounterClearSelect=0;
    SyncConfigData.bCounterAutoReload=1;
    SyncConfigData.dwPolarityGeneratorEnable=1;
    SyncConfigData.dwUpPos=0;                           // in internal hsync periods
    SyncConfigData.dwDownPos=pDIPanelInfo->VSYNCWIDTH; // in internal hsync periods
    SyncConfigData.dwStepRepeat=0;
    DIConfigureSync(di_sel,&SyncConfigData);

    //Active Lines start points
    SyncConfigData.dwPointer = DI_COUNTER_ALINE;
    SyncConfigData.dwRunValue = 0;
    SyncConfigData.dwRunResolution =DI_COUNTER_OHSYNC+1;
    SyncConfigData.dwOffsetValue=pDIPanelInfo->VSTARTWIDTH + pDIPanelInfo->VSYNCWIDTH;
    SyncConfigData.dwOffsetResolution=DI_COUNTER_OHSYNC+1;
    SyncConfigData.dwPolarityClearSelect=0;
    SyncConfigData.dwToggleTriggerSelect=0;
    SyncConfigData.dwCounterClearSelect=DI_COUNTER_OVSYNC+1;
    SyncConfigData.bCounterAutoReload=0;
    SyncConfigData.dwPolarityGeneratorEnable=0;
    SyncConfigData.dwUpPos=0;                           // in hsync periods
    SyncConfigData.dwDownPos=0;                         // in hsync periods
    SyncConfigData.dwStepRepeat=pDIPanelInfo->HEIGHT;
    DIConfigureSync(di_sel,&SyncConfigData);

    //Active clock start points
    SyncConfigData.dwPointer = DI_COUNTER_ACLOCK;
    SyncConfigData.dwRunValue = 0;
    SyncConfigData.dwRunResolution =DI_COUNTER_BASECLK+1;
    SyncConfigData.dwOffsetValue=pDIPanelInfo->HSTARTWIDTH + pDIPanelInfo->HSYNCWIDTH;
    SyncConfigData.dwOffsetResolution=DI_COUNTER_BASECLK+1;
    SyncConfigData.dwPolarityClearSelect=0;
    SyncConfigData.dwToggleTriggerSelect=0;
    SyncConfigData.dwCounterClearSelect=DI_COUNTER_ALINE+1;
    SyncConfigData.bCounterAutoReload=0;
    SyncConfigData.dwPolarityGeneratorEnable=0;
    SyncConfigData.dwUpPos=0;                           // in hsync periods
    SyncConfigData.dwDownPos=0;                         // in hsync periods
    SyncConfigData.dwStepRepeat=pDIPanelInfo->WIDTH;
    DIConfigureSync(di_sel,&SyncConfigData);

    DISetSyncStart(di_sel,2);//2lines predictions
    DIVSyncCounterSelect(di_sel,2); //PIN3 as VSYNC

    genConfData.dwLineCounterSelect = 0;
    genConfData.dwClockStopMode = IPU_DI_GENERAL_DI_CLOCK_STOP_MODE_NEXT_EDGE;
    genConfData.dwDisplayClockInitMode = IPU_DI_GENERAL_DI_DISP_CLOCK_INIT_STOPPED;
    genConfData.dwMaskSelect = IPU_DI_GENERAL_DI_MASK_SEL_COUNTER_2;
    if(pDIPanelInfo->DICLK_SOURCE == DISPLAY_CLOCK_EXTERNAL)
    {    
        genConfData.dwClkSource = IPU_DI_GENERAL_DI_CLK_EXT_EXTERNAL;
        genConfData.dwVSyncSource = IPU_DI_GENERAL_DI_VSYNC_EXT_INTERNAL;
    }
    else
    {
        genConfData.dwClkSource = IPU_DI_GENERAL_DI_CLK_EXT_INTERNAL;
        genConfData.dwVSyncSource = IPU_DI_GENERAL_DI_VSYNC_EXT_INTERNAL;
    }
    genConfData.dwWatchdogMode = IPU_DI_GENERAL_DI_WATCHDOG_MODE_4_CYCLES;
    genConfData.dwClkPolarity = pDIPanelInfo->SYNC_SIG_POL.CLK_POL;
    genConfData.dwSyncFlowCounterSelect = 0;
    genConfData.dwSyncFlowErrorTreatment = IPU_DI_GENERAL_DI_ERR_TREATMENT_DRIVE_LAST_COMPONENT;
    genConfData.dwERMVSyncSelect = 0;
    genConfData.dwCS1Polarity = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW;
    genConfData.dwCS0Polarity = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW;
    genConfData.dwPinPolarity[0] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW;
    genConfData.dwPinPolarity[1] = pDIPanelInfo->SYNC_SIG_POL.HSYNC_POL;    //PIN2 as HSYNC
    genConfData.dwPinPolarity[2] = pDIPanelInfo->SYNC_SIG_POL.VSYNC_POL;    //PIN3 as VSYNC
    genConfData.dwPinPolarity[3] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    genConfData.dwPinPolarity[4] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    genConfData.dwPinPolarity[5] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW;
    genConfData.dwPinPolarity[6] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW;
    genConfData.dwPinPolarity[7] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    DIConfigureGeneral(di_sel, &genConfData);

    // Setup signal pin polarity
    // Use default setting should be ok.

    // Configure CS0 polarity data
    polConfData.PolaritySetSelect = DI_POLARITY_SET_CS0;
    polConfData.DataPolarity = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[0] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[1] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[2] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[3] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[4] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[5] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[6] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    DIConfigurePolarity(di_sel, &polConfData);

    // Configure CS1 polarity data
    polConfData.PolaritySetSelect = DI_POLARITY_SET_CS1;
    polConfData.DataPolarity = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[0] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[1] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[2] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[3] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[4] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[5] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[6] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    DIConfigurePolarity(di_sel, &polConfData);

    // Configure DRDY polarity data
    polConfData.PolaritySetSelect = DI_POLARITY_SET_DRDY;
    polConfData.DataPolarity = 0;
    polConfData.PinPolarity[0] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[1] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[2] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[3] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[4] = (UINT8)pDIPanelInfo->SYNC_SIG_POL.ENABLE_POL;
    polConfData.PinPolarity[5] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[6] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    DIConfigurePolarity(di_sel, &polConfData);

    // Configure Wait polarity data
    polConfData.PolaritySetSelect = DI_POLARITY_SET_WAIT;
    polConfData.WaitPolarity = 0;
    DIConfigurePolarity(di_sel, &polConfData);

    // Configure CS0 byte enable polarity data
    polConfData.PolaritySetSelect = DI_POLARITY_SET_CS0_BYTE_ENABLE;
    polConfData.CS0ByteEnablePolarity = 0;
    DIConfigurePolarity(di_sel, &polConfData);

    // Configure CS1 Byte enable polarity data
    polConfData.PolaritySetSelect = DI_POLARITY_SET_CS1_BYTE_ENABLE;
    polConfData.CS1ByteEnablePolarity = 0;
    DIConfigurePolarity(di_sel, &polConfData);

    // Set MCU_T to 2
    //CMSetMCU_T(2);
}

//===============dummy function=================
//These function may be linked in other library code
void CalibrateStallCounter(){}
void StallExecution(DWORD dwMicroSec)
{
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(dwMicroSec);

}
BOOL FSL_InitializeInterLock(PUINT32 pID)
{
    *pID = 0;
    return TRUE;    
}
UINT32 FSL_InterlockedCompareExchange(UINT32 ID, LPLONG Target,LONG NewValue,LONG OldValue)
{
    return InterlockedCompareExchange(Target,NewValue,OldValue);
}
BOOL IPUV3BaseCloseHandle(HANDLE hIPUBase)
{
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(hIPUBase);
    return TRUE;
}

PUINT32 IPUV3GetGetIPUGlobalFlag(HANDLE hIPUBase)
{
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(hIPUBase);
    return &g_IPUGlobalFlag;
}
BOOL IPUV3DisableModule(HANDLE hIPUBase, IPU_SUBMODULE submodule, IPU_DRIVER driver)
{
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(hIPUBase);
    UNREFERENCED_PARAMETER(submodule);
    UNREFERENCED_PARAMETER(driver);
    return TRUE;    
}
HANDLE IPUV3BaseOpenHandle(void)
{
    
    return (HANDLE) (CSP_IPUV3_IPU1_DISPLAY_PORT_OFFSET);
}
DWORD IPUV3BaseGetBaseAddr(HANDLE hIPUBase)
{
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(hIPUBase);
    return (CSP_IPUV3_IPU1_DISPLAY_PORT_OFFSET);
}
BOOL IPUV3EnableModule(HANDLE hIPUBase, IPU_SUBMODULE submodule, IPU_DRIVER driver)
{
    UINT32 oldVal, newVal, iMask, iBitval;
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(hIPUBase);
    UNREFERENCED_PARAMETER(driver);
    switch(submodule)
    {
        case IPU_SUBMODULE_DP:
            // Compute bitmask and shifted bit value for IPU_CONF register
            iMask = CSP_BITFMASK(IPU_IPU_CONF_DP_EN);
            iBitval = CSP_BITFVAL(IPU_IPU_CONF_DP_EN, IPU_IPU_CONF_DP_EN_ENABLE);
            break;
        case IPU_SUBMODULE_DI0:
            iMask = CSP_BITFMASK(IPU_IPU_CONF_DI0_EN);
            iBitval = CSP_BITFVAL(IPU_IPU_CONF_DI0_EN, IPU_IPU_CONF_DI0_EN_ENABLE);
            break;
        case IPU_SUBMODULE_DI1:
            iMask = CSP_BITFMASK(IPU_IPU_CONF_DI1_EN);
            iBitval = CSP_BITFVAL(IPU_IPU_CONF_DI1_EN, IPU_IPU_CONF_DI1_EN_ENABLE);
            break;
        case IPU_SUBMODULE_DC:
            iMask = CSP_BITFMASK(IPU_IPU_CONF_DC_EN);
            iBitval = CSP_BITFVAL(IPU_IPU_CONF_DC_EN, IPU_IPU_CONF_DC_EN_ENABLE);
            break;
        case IPU_SUBMODULE_DMFC:
            iMask = CSP_BITFMASK(IPU_IPU_CONF_DMFC_EN);
            iBitval = CSP_BITFVAL(IPU_IPU_CONF_DMFC_EN, IPU_IPU_CONF_DMFC_EN_ENABLE);
            break;
        default:
            return FALSE;
    }        
    oldVal = INREG32(&g_pIPUV3_COMMON->IPU_CONF);
    newVal = (oldVal & (~iMask)) | iBitval;
    OUTREG32(&g_pIPUV3_COMMON->IPU_CONF,newVal);

    return TRUE;
}
//===============dummy function end=================


