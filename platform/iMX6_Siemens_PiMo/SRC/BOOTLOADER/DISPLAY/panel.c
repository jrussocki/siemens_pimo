//------------------------------------------------------------------------------
//
//  Copyright (C) 2010-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  panel.cpp
//
//  Provides routines for enabling, disabling, and initializing
//  the proper panel.
//
//------------------------------------------------------------------------------

#include <bsp.h>
#include "Ipu_common.h"


//------------------------------------------------------------------------------
// External Functions
extern VOID OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX index, 
    DDK_CLOCK_GATE_MODE mode);
extern BOOL ConfigureExpandIO(UINT32 IOName, BOOL bData);


//------------------------------------------------------------------------------
// External Variables
extern PCSP_IOMUX_REGS g_pIOMUX;
extern PCSP_CCM_REGS g_pCCM;

extern PCSP_PLL_BASIC_REGS g_pPLL_USBOTG;
extern PCSP_PLL_BASIC_REGS g_pPLL_PFD_480MHZ;
extern PCSP_PLL_MEDIA_REGS g_pPLL_VIDEO;


//------------------------------------------------------------------------------
// Defines


#define FUSE_LVDS_BG_V_LSH  0
#define FUSE_LVDS_BG_V_WID  3

#define PWM_PERIOD	63 
#define PWM_SAMPLE  63

//------------------------------------------------------------------------------
// Types


//------------------------------------------------------------------------------
// Global Variables
PCSP_GPIO_REGS g_pGPIO1; 
PCSP_GPIO_REGS g_pGPIO3; 
PCSP_GPIO_REGS g_pGPIO4; 

//------------------------------------------------------------------------------
// Local Variables


//------------------------------------------------------------------------------
// Local Functions


//------------------------------------------------------------------------------
//
// Function: BSPInitializePanel
//
// This function performs panel-specific initialization for proper panel.
//
// Parameters:
//      pPanelInfo
//          [in] Structure describing panel to initialize
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void Panel_Initialize(PANEL_INFO *pPanelInfo)
{
    // Panel specific initialziation code insert here
}


void EnableDisplayPWM(DWORD pwm_register_address)
{
	PCSP_PWM_REG pPwm;

	/*SD1_DATA3 in configured on PWM1*/
	pPwm = (PCSP_PWM_REG) OALPAtoUA(pwm_register_address);

	//Enable clock gating
	OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_PWM1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
	
	// Disable PWM first, clear all bits in the control register
	OUTREG32(&pPwm->PWM_CR, 0);

	// Reset the controller
	INSREG32BF(&pPwm->PWM_CR, PWM_CR_SWR, PWM_CR_SWR_RESET);

    // Wait for the software reset to complete
    while(EXTREG32BF(&pPwm->PWM_CR, PWM_CR_SWR));

	// Set clock source 
	INSREG32BF(&pPwm->PWM_CR,PWM_CR_CLKSRC, PWM_PWMCR_CLKSRC_IPG_CLK_32K);

	// Set FIFO watermark to 1 empty slots 
	INSREG32BF(&pPwm->PWM_CR,PWM_CR_FWM, 0);

	// Set active polarity: 0 set at rollover
	INSREG32BF(&pPwm->PWM_CR,PWM_CR_PRESCALER, 0);

	// Set sample
	OUTREG32(&pPwm->PWM_SAR,PWM_SAMPLE);

	// Set period
	OUTREG32(&pPwm->PWM_PR, PWM_PERIOD);
	
	//Start PWM
	INSREG32(&pPwm->PWM_CR, CSP_BITFMASK(PWM_CR_EN), CSP_BITFVAL(PWM_CR_EN, PWM_CR_EN_ENABLE));
}

//------------------------------------------------------------------------------
//
// Function: Panel_Enable
//
// This function performs panel-specific enabling for proper panel.
//
// Parameters:
//      pPanelInfo
//          [in] Structure describing panel to enable
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------

void Panel_Enable(PANEL_INFO *pPanelInfo)
{
    PCSP_GPIO_REGS pGPIO;
#if 1
    //DDKClockSetFreq(DDK_CLOCK_SIGNAL_480M_PFD1_540M,  455MHZ);
#if 0  
    if(!EXTREG32BF(&g_pPLL_USBOTG->CTRL, PLL_USBOTG_CTRL_POWER) ||
       !EXTREG32BF(&g_pPLL_USBOTG->CTRL, PLL_USBOTG_CTRL_ENABLE))
    {  
        INSREG32BF(&g_pPLL_USBOTG->CTRL_SET, PLL_USBOTG_CTRL_POWER, 1);
        INSREG32BF(&g_pPLL_USBOTG->CTRL_SET, PLL_USBOTG_CTRL_ENABLE, 1);
        while(EXTREG32BF(&g_pPLL_USBOTG->CTRL, PLL_USBOTG_CTRL_LOCK) == PLL_CTL_NO_LOCKED);
    }
#endif
    
    //PFD1 frequency 480M*18/19=454.7MHz  
    OALMSG(0, (L"before SET FREQ, 480 PFD is %x\r\n", INREG32(&g_pPLL_PFD_480MHZ->CTRL)));
    INSREG32BF(&g_pPLL_PFD_480MHZ->CTRL, PLL_PFD_CTRL_PFD1_FRAC, 19);
    OALMSG(0, (L"after SET FREQ, 480 PFD is %x\r\n", INREG32(&g_pPLL_PFD_480MHZ->CTRL)));

    //DDKClockConfigBaud(DDK_CLOCK_SIGNAL_LDB_DI0_SERIAL, DDK_CLOCK_BAUD_SOURCE_480M_PDF1_540M, 0, 0, 0);
    //DDKClockConfigBaud(DDK_CLOCK_SIGNAL_LDB_DI0_IPU, DDK_CLOCK_BAUD_SOURCE_LDB_DI0_SERIAL, 0, 0, 1);
    //DDKClockConfigBaud(DDK_CLOCK_SIGNAL_IPU1_DI0, DDK_CLOCK_BAUD_SOURCE_LDB_DI0_IPU, 0, 0, 0);

    //ldb_di0_clk Source from 480M_PDF1 540M
    INSREG32BF(&g_pCCM->CS2CDR, CCM_CS2CDR_LDB_DI0_CLK_SEL, 3); 

    //divide by 7
    INSREG32BF(&g_pCCM->CSCMR2, CCM_CSCMR2_LDB_DI0_IPU_DIV, 1);
    
    //ipu1_di0_clk source from ldb_di0_ipu    
    INSREG32BF(&g_pCCM->CHSCCDR, CCM_CHSCCDR_IPU1_DI0_CLK_SEL,3); 

    //Enable LDB DI0 Clock
    OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
    OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU_DI0, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
    OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_LDB_DI0, DDK_CLOCK_GATE_MODE_ENABLED_ALL);

#else

    OUTREG32(&g_pPLL_VIDEO->NUM, 0xFF0D6C3);
    OUTREG32(&g_pPLL_VIDEO->CTRL, 0x12002);

    while(EXTREG32BF(&g_pPLL_VIDEO->CTRL, PLL_USBOTG_CTRL_LOCK) == PLL_CTL_NO_LOCKED);
    OUTREG32(&g_pPLL_VIDEO->CTRL, 0x80002002);

    //ldb_di0_clk Source from 480M_PDF1 540M
    INSREG32BF(&g_pCCM->CS2CDR, CCM_CS2CDR_LDB_DI0_CLK_SEL, 0); 
    //divide by 7
    INSREG32BF(&g_pCCM->CSCMR2, CCM_CSCMR2_LDB_DI0_IPU_DIV, 1);
    //ipu1_di0_clk source from ldb_di0_ipu    
    INSREG32BF(&g_pCCM->CHSCCDR, CCM_CHSCCDR_IPU1_DI0_CLK_SEL,3); 
 
#endif    
    //Set IOMUX, MX6Q LVDS0 and LVDS1 IOMUX setting not needed  
    //ldisp_pwm GPIO1_9

	//CABC_EN0/1 - LVDS0/1 enable pin : NANDF_CS2/3 as  GPIO6[15] and GPIO6[16]
	//AUX_5V_EN - Enable power to panel  : NANDF_RB0 as gpio6[10]
	 pGPIO = (PCSP_GPIO_REGS) OALPAtoUA(CSP_BASE_REG_PA_GPIO6);
	 SETREG32(&pGPIO->GDIR, GPIO_PIN_MASK(10)| GPIO_PIN_MASK(15) | GPIO_PIN_MASK(16)); //out  
	 SETREG32(&pGPIO->DR, GPIO_PIN_MASK(10)| GPIO_PIN_MASK(15) | GPIO_PIN_MASK(16));

	 //DISP0_CONTRAST - PWM/cofigured as gpio : SD1_DAT3 as gpio1[21]
	 //pGPIO = (PCSP_GPIO_REGS) OALPAtoUA(CSP_BASE_REG_PA_GPIO1);
	 //SETREG32(&pGPIO->GDIR, GPIO_PIN_MASK(21)); //out  
	 //SETREG32(&pGPIO->DR, GPIO_PIN_MASK(21));

    //LVDSConf.RouteMode = LVDS_ROUTE_DI0;
    //LVDSConf.DataWidth = LVDS_DW_24BIT;
    //LVDSConf.MappingType = LVDS_MAP_SPWG;  //All current supported panels use SPWG mode.
    //LVDSConf.VsyncPolarity = LVDS_VSPOL_LOW; //Always should be low.
	 
	 /*Set the desired output from the catalog*/
#if BSP_DISPLAY_LVDS2 /*LVDS1 only*/
	 EnableDisplayPWM(CSP_BASE_REG_PA_PWM1);
	 OUTREG32(&g_pIOMUX->GPR[2],0x284); //di0->ch1, 24 bit SPWG mode LVDS1
#else
#if BSP_DISPLAY_LVDS1 /*LVDS0 only*/
	 EnableDisplayPWM(CSP_BASE_REG_PA_PWM1);
	 OUTREG32(&g_pIOMUX->GPR[2],0x221); //di0->ch0, 24 bit SPWG mode LVDS0
#else
	 //EnableDisplayPWM(CSP_BASE_REG_PA_PWM1);
	 //OUTREG32(&g_pIOMUX->GPR[2],0x6a5); //di0->ch0, 24 bit SPWG mode (LVDS0 + LVDS1)
#endif 
#endif // BSP_DISPLAY_LVDS0
}

//------------------------------------------------------------------------------
//
// Function: Panel_Disable
//
// This function performs panel-specific disabling for proper panel.
//
// Parameters:
//      pPanelInfo
//          [in] Structure describing panel to enable
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void Panel_Disable(PANEL_INFO *pPanelInfo)
{
    PCSP_GPIO_REGS pGPIO;

    // Panel specific disable code insert here
    UNREFERENCED_PARAMETER(pPanelInfo);

    //OALMSG(1, (L"Before Close LDB_DI0, 480 PFD reg is %x\r\n", INREG32(&g_pPLL_PFD_480MHZ->CTRL)));

    // Disable backlight  
    //DDKGpioWriteDataPin(DDK_GPIO_PORT1, BSP_DISP_PWM_GPIO_PIN, 0);      
    pGPIO = (PCSP_GPIO_REGS) OALPAtoUA(CSP_BASE_REG_PA_GPIO1);
    CLRREG32(&pGPIO->DR, BSP_DISP_PWM_GPIO_PIN_MASK); 
    
    OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_LDB_DI0, DDK_CLOCK_GATE_MODE_DISABLED);
    OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU_DI0, DDK_CLOCK_GATE_MODE_DISABLED); 
    //OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU, DDK_CLOCK_GATE_MODE_DISABLED); 

    OUTREG32(&g_pIOMUX->GPR[2],0x0); //Reset the value.
 
    //OALMSG(1, (L"After Close LDB_DI0, 480 PFD reg is %x\r\n", INREG32(&g_pPLL_PFD_480MHZ->CTRL)));
}
