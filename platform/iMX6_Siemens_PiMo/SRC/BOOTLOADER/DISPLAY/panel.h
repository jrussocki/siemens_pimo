//------------------------------------------------------------------------------
//
//  Copyright (C) 2010, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  panel.h
//
//  Provides supported display panel parameter for the EBOOT
//
//------------------------------------------------------------------------------

#ifndef __PANEL_H__
#define __PANEL_H__

//------------------------------------------------------------------------------
// Defines

#define SCREEN_BPP  24
#define SCREEN_ADDR IMAGE_BOOT_PICTURE_RAM_START

//------------------------------------------------------------------------------
// Types

#if 1
//RGB666
BusMappingData LVDSMapping =
{
    21,     // R component Offset (component 2)
    0xFC,   // R component Mask
    13,     // G component Offset (component 1)
    0xFC,   // G component Mask
    5,      // B component Offset (component 0)
    0xFC    // B component Mask
};
#else
//RGB888
BusMappingData LVDSMapping =
{
    23,     // R component Offset (component 2)
    0xFF,   // R component Mask
    15,     // G component Offset (component 1)
    0xFF,   // G component Mask
    7,      // B component Offset (component 0)
    0xFF    // B component Mask
};
#endif


PANEL_INFO g_PanelInfo =
// LVDS 1024x768 Definitions
{
    (PUCHAR) "LVDS 1024x768 Panel",   // Name
    IPU_PANEL_SYNC_TYPE_SYNCHRONOUS, // Panel Sync Type
    DI_SELECT_DI0,               // Display Interface Number
    0,           // Panel ID
    0,           // Port type
    DISPLAY_CLOCK_EXTERNAL,      // di clk source
    IPU_PIX_FMT_RGB24,           // Pixel Format
    1024,                        // width
    768,                         // height
    62,                          // frequency
    1,                           // transfer cycles
    &LVDSMapping,                // Mapping for LVDS RGB data

    // Synchronous Panel Info
    9,                           // Vertical Sync width (in vertical lines)
    24,                          // Vertical Start Width (in vertical lines)
    5,                           // Vertical End Width (in vertical lines)
    58,                          // Horizontal Sync Width (in pixel clock cyles)
    174,                          // Horizontal Start Width (in pixel clock cyles)
    88,                          // Horizontal End Width (in pixel clock cyles)
    65000000,                    // Pixel Clock Cycle Frequency: IPU clock 133Mhz, 133/44.3=3 so that we choose 3 as dividor.
    0,                           // Pixel Data Offset Position
    0,                           // Pixel Clock Up, in ns
    8,                           // Pixel Clock Down, in ns
    {       // Display Interface signal polarities
        TRUE,                        // Data mask enable
        FALSE,                       // Clock Idle enable
        FALSE,                       // Clock Select Enable
        FALSE,                       // Vertical Sync Polarity
        TRUE,                        // Output enable polarity(straight polarity for sharp signals)
        FALSE,                       // Data Pol   Inverse polarity.
        FALSE,                       // Clock Pol  Inverse polarity.
        FALSE,                       // HSync Pol
    },
    // TV only sync panel info
    0,                            // Field0 VStart width in lines
    0,                            // Field0 VEnd width in lines
    0,                            // Field1 VStart width in lines
    0,                            // Field1 VEnd width in lines
    FALSE,                        // TRUE if TV output data is interlaced; FALSE if progressive

    // Asynchronous Panel Info
    0,                           // Read Cycle Period
    0,                           // Read Up Position
    0,                           // Read Down Position
    0,                           // Write Cycle Period
    0,                           // Write Up Position
    0,                           // Write Down Position
    {       // ADC Display Interface signal polarities
        0,
        IPU_POLARITY_ACTIVE_LOW,
        IPU_POLARITY_ACTIVE_LOW,
        IPU_POLARITY_ACTIVE_LOW,
    },

    // Serial Mapping Data
    FALSE,                       // Use IPU serial communication with panel?
    0,                           // Number of serial data transfer cycles
    NULL,                        // Pointer to serial bus mappings
    {
        0,                       // Serial Clk Period
        0,                       // CS Up Position
        0,                       // CS Down Position
        0,                       // Clk Up Position
        0,                       // Clk Down Position
        0,                       // RS Up Position
        0,                       // RS Down Position
    },

    // Dynamic panel data
    0,                           // Starting panel mode corresponding to array index.
                                 // Mode may change if array is rearranged during driver init.
    FALSE,                       // Has panel been initialized yet?  Must be set to FALSE here.
    FALSE,                       // Is panel active (always start out off)
    DC_DISPLAY_0,                // IPU Display number (initialized to 0)
};
//------------------------------------------------------------------------------
// Functions
void Panel_Initialize(PANEL_INFO *pPanelInfo);
void Panel_Enable(PANEL_INFO *pPanelInfo);
void Panel_Disable(PANEL_INFO *pPanelInfo);

#endif   // __PANEL_H__
