//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//-----------------------------------------------------------------------------
//
//  Copyright (C) 2007-2010, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//-----------------------------------------------------------------------------
//
//  File:  utils.c
//
//  Generic "utility" routines for the bootloader.
//
//-----------------------------------------------------------------------------
#include "bsp.h"
#include "loader.h"

#pragma warning(disable: 4115 4201 4214 4100)

//-----------------------------------------------------------------------------
// External Functions
extern VOID OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX index, DDK_CLOCK_GATE_MODE mode);

//-----------------------------------------------------------------------------
// External Variables
extern PCSP_CCM_REGS g_pCCM;

//-----------------------------------------------------------------------------
// Defines

#define CRC_POLY 0xEDB88320
//-----------------------------------------------------------------------------
// Types


//-----------------------------------------------------------------------------
// Global Variables


//-----------------------------------------------------------------------------
// Local Variables
static const UINT32 crc32_table[] =
{
	0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x076dc419, 0x706af48f, 0xe963a535, 
	0x9e6495a3, 0x0edb8832, 0x79dcb8a4, 0xe0d5e91e, 0x97d2d988, 0x09b64c2b, 0x7eb17cbd, 
	0xe7b82d07, 0x90bf1d91, 0x1db71064, 0x6ab020f2, 0xf3b97148, 0x84be41de, 0x1adad47d, 
	0x6ddde4eb, 0xf4d4b551, 0x83d385c7, 0x136c9856, 0x646ba8c0, 0xfd62f97a, 0x8a65c9ec, 
	0x14015c4f, 0x63066cd9, 0xfa0f3d63, 0x8d080df5, 0x3b6e20c8, 0x4c69105e, 0xd56041e4, 
	0xa2677172, 0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b, 0x35b5a8fa, 0x42b2986c, 
	0xdbbbc9d6, 0xacbcf940, 0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59, 0x26d930ac, 
	0x51de003a, 0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423, 0xcfba9599, 0xb8bda50f, 
	0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924, 0x2f6f7c87, 0x58684c11, 0xc1611dab, 
	0xb6662d3d, 0x76dc4190, 0x01db7106, 0x98d220bc, 0xefd5102a, 0x71b18589, 0x06b6b51f, 
	0x9fbfe4a5, 0xe8b8d433, 0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818, 0x7f6a0dbb, 
	0x086d3d2d, 0x91646c97, 0xe6635c01, 0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e, 
	0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457, 0x65b0d9c6, 0x12b7e950, 0x8bbeb8ea, 
	0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65, 0x4db26158, 0x3ab551ce, 
	0xa3bc0074, 0xd4bb30e2, 0x4adfa541, 0x3dd895d7, 0xa4d1c46d, 0xd3d6f4fb, 0x4369e96a, 
	0x346ed9fc, 0xad678846, 0xda60b8d0, 0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9, 
	0x5005713c, 0x270241aa, 0xbe0b1010, 0xc90c2086, 0x5768b525, 0x206f85b3, 0xb966d409, 
	0xce61e49f, 0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17, 0x2eb40d81, 
	0xb7bd5c3b, 0xc0ba6cad, 0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a, 0xead54739, 
	0x9dd277af, 0x04db2615, 0x73dc1683, 0xe3630b12, 0x94643b84, 0x0d6d6a3e, 0x7a6a5aa8, 
	0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1, 0xf00f9344, 0x8708a3d2, 0x1e01f268, 
	0x6906c2fe, 0xf762575d, 0x806567cb, 0x196c3671, 0x6e6b06e7, 0xfed41b76, 0x89d32be0, 
	0x10da7a5a, 0x67dd4acc, 0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5, 0xd6d6a3e8, 
	0xa1d1937e, 0x38d8c2c4, 0x4fdff252, 0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b, 
	0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60, 0xdf60efc3, 0xa867df55, 0x316e8eef, 
	0x4669be79, 0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236, 0xcc0c7795, 0xbb0b4703, 
	0x220216b9, 0x5505262f, 0xc5ba3bbe, 0xb2bd0b28, 0x2bb45a92, 0x5cb36a04, 0xc2d7ffa7, 
	0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d, 0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a, 
	0x9c0906a9, 0xeb0e363f, 0x72076785, 0x05005713, 0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 
	0x0cb61b38, 0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21, 0x86d3d2d4, 0xf1d4e242, 
	0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777, 0x88085ae6, 
	0xff0f6a70, 0x66063bca, 0x11010b5c, 0x8f659eff, 0xf862ae69, 0x616bffd3, 0x166ccf45, 
	0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2, 0xa7672661, 0xd06016f7, 0x4969474d, 
	0x3e6e77db, 0xaed16a4a, 0xd9d65adc, 0x40df0b66, 0x37d83bf0, 0xa9bcae53, 0xdebb9ec5, 
	0x47b2cf7f, 0x30b5ffe9, 0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6, 0xbad03605, 
	0xcdd70693, 0x54de5729, 0x23d967bf, 0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94, 
	0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d
};

//------------------------------------------------------------------------------
// Local Functions
//
void SetMAC(BOOT_CFG *pBootCfg);
void CvtMAC(USHORT MacAddr[3], char *pszDottedD);
static ULONG bstrtoul(PUCHAR pStr, UCHAR nBase);

void OEMReset()
{
    PCSP_WDOG_REGS pReg = (PCSP_WDOG_REGS)OALPAtoUA(CSP_BASE_REG_PA_WDOG1);
    pReg->WCR = pReg->WCR&(~(1<<WDOG_WCR_SRS_LSH));
}

//-----------------------------------------------------------------------------
//
//  Function:  SpinForever
//
//  Halts the bootloader.
//
//  Parameters:
//      None.
//
//  Returns:
//      None.
//
//-----------------------------------------------------------------------------
void SpinForever(void)
{
    UINT32 Selection;
    KITLOutputDebugString("SpinForever...\r\n");
    KITLOutputDebugString("Do you want to reset [Y\\N] \r\n");
    
    for (;;)
    {
        Selection=OEMReadDebugByte();
        if(Selection == 'Y' || Selection == 'y')
        {
            OEMReset();
        }
    }
}


//-----------------------------------------------------------------------------
//
//  Function:  OEMShowProgress
//
//  This function shows visual information, on an LED, for example, 
//  to let users know that the download is in progress. It is called as the 
//  download progresses.
//
//  Parameters:
//      dwPacketNum 
//          [in] Equal to the packet number currently downloading. Knowing 
//          the total number of packets, a download percentage can be computed.
//
//  Returns:
//      None.
//
//-----------------------------------------------------------------------------
void OEMShowProgress(DWORD dwPacketNum)
{
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(dwPacketNum);
}


//-----------------------------------------------------------------------------
//
//  Function: BootTimerInit
//
//  This function initializes EPIT which is being used by 
//  OALStall() implementation.
//
//  Parameters:
//      None.
//
//  Returns:
//      None.
//
//-----------------------------------------------------------------------------
void BootTimerInit(void)
{
    PCSP_EPIT_REG pEPIT;
    
    pEPIT = (PCSP_EPIT_REG) OALPAtoUA(CSP_BASE_REG_PA_EPIT1);
    if (pEPIT == NULL)
    {
        KITLOutputDebugString("BootTimerInit: EPIT mapping failed!\r\n");
        return;
    }


    OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_EPIT1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);

    // Disable EPIT and clear all configuration bits
    OUTREG32(&pEPIT->CR, 0);

    // Assert software reset for the timer
    OUTREG32(&pEPIT->CR, CSP_BITFMASK(EPIT_CR_SWR));

    // Wait for the software reset to complete
    while (INREG32(&pEPIT->CR) & CSP_BITFMASK(EPIT_CR_SWR));

    // Enable timer for "free-running" mode where timer rolls
    // over from 0x00000000 to 0xFFFFFFFF
    OUTREG32(&pEPIT->CR,
        CSP_BITFVAL(EPIT_CR_EN, EPIT_CR_EN_ENABLE) |
        CSP_BITFVAL(EPIT_CR_ENMOD, EPIT_CR_ENMOD_RESUME) |
        CSP_BITFVAL(EPIT_CR_OCIEN, EPIT_CR_OCIEN_DISABLE) |
        CSP_BITFVAL(EPIT_CR_RLD, EPIT_CR_RLD_ROLLOVER) |
        CSP_BITFVAL(EPIT_CR_PRESCALAR, BSP_SYSTIMER_PRESCALAR) |
        CSP_BITFVAL(EPIT_CR_SWR, EPIT_CR_SWR_NORESET) |
        CSP_BITFVAL(EPIT_CR_IOVW, EPIT_CR_IOVW_NOOVR) |
        CSP_BITFVAL(EPIT_CR_DBGEN, EPIT_CR_DBGEN_ACTIVE) |
        CSP_BITFVAL(EPIT_CR_WAITEN, EPIT_CR_WAITEN_ENABLE) |
        CSP_BITFVAL(EPIT_CR_DOZEN, EPIT_CR_DOZEN_ENABLE) |
        CSP_BITFVAL(EPIT_CR_STOPEN, EPIT_CR_STOPEN_ENABLE) |
        CSP_BITFVAL(EPIT_CR_OM, EPIT_CR_OM_DICONNECT) |
        CSP_BITFVAL(EPIT_CR_CLKSRC, BSP_SYSTIMER_CLKSRC));
}
//-----------------------------------------------------------------------------
//
//  Function: BootUpdateTimerInit
//
//  This function initializes EPIT2 which is being used for genternating drawing update interrupt.
//
//  Parameters:
//      uInterval
//           [in] the interval time between two draw operation. (ms)
//
//  Returns:
//      None.
//
//-----------------------------------------------------------------------------
void BootUpdateTimerInit(UINT32 uInterval)
{
    // TODO:  Port to GIC
    PCSP_EPIT_REG pEPIT;
    PCSP_GIC_DIST_REGS pGIC_DIST;
    BSP_ARGS *pBspArgs;
    UINT32 clkFreq;

    //Initialize epit2
    pEPIT = (PCSP_EPIT_REG) OALPAtoUA(CSP_BASE_REG_PA_EPIT2);

    OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_EPIT2,DDK_CLOCK_GATE_MODE_ENABLED_ALL);    
    
    // Disable EPIT and clear all configuration bits
    OUTREG32(&pEPIT->CR, 0);
    
    // Assert software reset for the timer
    OUTREG32(&pEPIT->CR, CSP_BITFMASK(EPIT_CR_SWR));
    
    // Wait for the software reset to complete
    while (INREG32(&pEPIT->CR) & CSP_BITFMASK(EPIT_CR_SWR));
    
    pBspArgs = (BSP_ARGS *)IMAGE_SHARE_ARGS_UA_START;
    
    switch(BSP_SYSTIMER_CLKSRC)
    {
    case EPIT_CR_CLKSRC_CKIL:
        clkFreq = pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_CKIL_SYNC];
        break;
    
    case EPIT_CR_CLKSRC_IPGCLK:
        clkFreq = pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_IPG];
        break;
    
    default:
        clkFreq = pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_PERCLK];
        break;
    }
    
    // Enable timer for "set and forgetting" mode where timer rolls
    // over from 0x00000000 to LDR
    OUTREG32(&pEPIT->CR,
        CSP_BITFVAL(EPIT_CR_EN, EPIT_CR_EN_ENABLE) |
        CSP_BITFVAL(EPIT_CR_ENMOD, EPIT_CR_ENMOD_LOAD) |
        CSP_BITFVAL(EPIT_CR_OCIEN, EPIT_CR_OCIEN_ENABLE) |
        CSP_BITFVAL(EPIT_CR_RLD, EPIT_CR_RLD_RELOAD) |
        CSP_BITFVAL(EPIT_CR_PRESCALAR, (1000-1)) |
        CSP_BITFVAL(EPIT_CR_SWR, EPIT_CR_SWR_NORESET) |
        CSP_BITFVAL(EPIT_CR_IOVW, EPIT_CR_IOVW_OVR) |
        CSP_BITFVAL(EPIT_CR_DBGEN, EPIT_CR_DBGEN_ACTIVE) |
        CSP_BITFVAL(EPIT_CR_WAITEN, EPIT_CR_WAITEN_ENABLE) |
        CSP_BITFVAL(EPIT_CR_DOZEN, EPIT_CR_DOZEN_ENABLE) |
        CSP_BITFVAL(EPIT_CR_STOPEN, EPIT_CR_STOPEN_ENABLE) |
        CSP_BITFVAL(EPIT_CR_OM, EPIT_CR_OM_DICONNECT) |
        CSP_BITFVAL(EPIT_CR_CLKSRC, BSP_SYSTIMER_CLKSRC));

    //Check if the interval value is valid. 
    if((uInterval == 0) || (uInterval > 1000000))
        uInterval = 33;
        
    OUTREG32(&pEPIT->LR, clkFreq/(1000000/uInterval)); 
    
    //Enable interrupt.
    pGIC_DIST = (PCSP_GIC_DIST_REGS) OALPAtoUA(CSP_BASE_REG_PA_GIC_DIST);
    OUTREG32(&pGIC_DIST->ICDISER[IRQ_EPIT2 >> 5], 1U << (IRQ_EPIT2 % 32));
}

//-----------------------------------------------------------------------------
//
//  Function: OALClockSetGatingMode
//
//  This function provides the OAL a safe mechanism for setting the clock
//  gating mode of peripherals.
//
//  Parameters:
//      index
//           [in] Index for referencing the peripheral clock gating control
//           bits.
//
//      mode
//           [in] Requested clock gating mode for the peripheral.
//
//  Returns:
//      None
//
//-----------------------------------------------------------------------------
VOID OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX index, DDK_CLOCK_GATE_MODE mode)
{
    // Update the clock gating mode
    INSREG32(&g_pCCM->CCGR[CCM_CGR_INDEX(index)], CCM_CGR_MASK(index),
             CCM_CGR_VAL(index, mode));
}

//------------------------------------------------------------------------------
//
//  Function:  SetMAC
//
//  Allows user to set a MAC address using the boot loader menu.
//
//  Parameters:
//      eBootCFG
//          [out] Points to bootloader configuration that will be updated with
//          the MAC address entered by the user.
//
//  Returns:
//      None.
//
//------------------------------------------------------------------------------
void SetMAC(BOOT_CFG *pBootCfg)
{
    CHAR szDottedD[24];
    USHORT cwNumChars = 0;
    USHORT InChar = 0;
    INT32  dwCharRead = OEM_DEBUG_READ_NODATA;

    memset(szDottedD, '0', 24);

    EdbgOutputDebugString ( "\r\nNOTE:  To force controller to use MAC from EEPROM,\r\n       enter MAC as FF.FF.FF.FF.FF.FF\r\n");
    EdbgOutputDebugString ( "\r\nEnter new MAC address in hexadecimal (hh.hh.hh.hh.hh.hh): ");

    while(!((InChar == 0x0d) || (InChar == 0x0a)))
    {
        dwCharRead = OEMReadDebugByte();
        if (dwCharRead != OEM_DEBUG_COM_ERROR && dwCharRead != OEM_DEBUG_READ_NODATA)
        {
            InChar = (USHORT)dwCharRead;
            InChar = (USHORT)tolower(InChar);

            // If it's a hex number or a period, add it to the string.
            //
            if (InChar == '.' || (InChar >= '0' && InChar <= '9') || (InChar >= 'a' && InChar <= 'f')) 
            {
                if (cwNumChars < 17) 
                {
                    szDottedD[cwNumChars++] = (char)InChar;
                    OEMWriteDebugByte((BYTE)InChar);
                }
            }
            else if (InChar == 8)       // If it's a backspace, back up.
            {
                if (cwNumChars > 0) 
                {
                    cwNumChars--;
                    OEMWriteDebugByte((BYTE)InChar);
                }
            }
        }
    }

    EdbgOutputDebugString ( "\r\n");

    // If it's a carriage return with an empty string, don't change anything.
    //
    if (cwNumChars) 
    {
        szDottedD[cwNumChars] = '\0';
        CvtMAC(pBootCfg->mac, szDottedD);
        EdbgOutputDebugString("INFO: MAC address set to: %x:%x:%x:%x:%x:%x\r\n",
                  pBootCfg->mac[0] & 0x00FF, pBootCfg->mac[0] >> 8,
                  pBootCfg->mac[1] & 0x00FF, pBootCfg->mac[1] >> 8,
                  pBootCfg->mac[2] & 0x00FF, pBootCfg->mac[2] >> 8);
        return;
    }
    EdbgOutputDebugString("WARNING: SetMAC: Invalid MAC address.\r\n");
}


//------------------------------------------------------------------------------
//
//  Function:  bstrtoul
//
//  Provides boot loader implementation for strtoul.
//
//  Parameters:
//      pStr 
//          [in] Null-terminated string to convert.
//
//      nBase
//          [in] Number base to use for coversion.
//
//  Returns:
//      None.
//
//------------------------------------------------------------------------------
static ULONG bstrtoul(PUCHAR pStr, UCHAR nBase)
{
    UCHAR nPos=0;
    BYTE c;
    ULONG nVal = 0;
    UCHAR nCnt=0;
    ULONG n=0;

    // fulllibc doesn't implement isctype or iswctype, which are needed by
    // strtoul, rather than including coredll code, here's our own simple strtoul.

    if (pStr == NULL)
        return(0);

    for (nPos=0 ; nPos < strlen((const CHAR *)pStr) ; nPos++)
    {
        c = (BYTE)tolower(*(pStr + strlen((const CHAR *)pStr) - 1 - nPos));
        if (c >= '0' && c <= '9')
            c -= '0';
        else if (c >= 'a' && c <= 'f')
        {
            c -= 'a';
            c  = (0xa + c);
        }

        for (nCnt = 0, n = 1 ; nCnt < nPos ; nCnt++)
        {
            n *= nBase;
        }
        nVal += (n * c);
    }

    return(nVal);
}


//------------------------------------------------------------------------------
//
//  Function:  CvtMAC
//
//  Converts MAC address specified by the user in dotted string format
//  into 16-bit numeric array.
//
//  Parameters:
//      MacAddr 
//          [out] 16-bit numeric array for converted MAC address.
//
//      pszDottedD
//          [in] Points to string containing MAC address.
//
//  Returns:
//      None.
//
//------------------------------------------------------------------------------
void CvtMAC(USHORT MacAddr[3], char *pszDottedD) 
{
    DWORD cBytes;
    char *pszLastNum;
    int atoi (const char *s);
    int i=0;
    BYTE *p = (BYTE *)MacAddr;

    // Replace the dots with NULL terminators
    pszLastNum = pszDottedD;
    for(cBytes = 0 ; cBytes < 6 ; cBytes++)
    {
        while(*pszDottedD != '.' && *pszDottedD != '\0')
        {
            pszDottedD++;
        }
        if (pszDottedD == '\0' && cBytes != 5)
        {
            // zero out the rest of MAC address
            while(i++ < 6)
            {
                *p++ = 0;
            }
            break;
        }
        *pszDottedD = '\0';
        *p++ = (BYTE)(bstrtoul((PUCHAR)pszLastNum, 16) & 0xFF);
        i++;
        pszLastNum = ++pszDottedD;
    }
}

//------------------------------------------------------------------------------
//
//  Function:  SetBootMe
//
//  Allows user to set a BOOTME packet count using the boot loader menu.
//
//  Parameters:
//      eBootCFG
//          [out] Points to bootloader configuration that will be updated with
//          the BOOTME packet count entered by the user.
//
//  Returns:
//      None.
//
//------------------------------------------------------------------------------
void SetBootMe(BOOT_CFG *pBootCFG)
{
    char szCount[16];
    WORD cwNumChars = 0;
    UINT16 InChar = 0;
    INT32  dwCharRead = OEM_DEBUG_READ_NODATA;

    KITLOutputDebugString ( "\r\nUse 0 for continuous boot me packets. \r\n");
    KITLOutputDebugString ( "Enter maximum number of boot me packets to send [0-255]: ");

    while (!((InChar == 0x0d) || (InChar == 0x0a)))
    {
        dwCharRead = OEMReadDebugByte();
        if (dwCharRead != OEM_DEBUG_COM_ERROR && dwCharRead != OEM_DEBUG_READ_NODATA)
        {
            InChar = (UINT16)dwCharRead;
            // If it's a number or a period, add it to the string
            if ((InChar >= '0' && InChar <= '9'))
            {
                if (cwNumChars < 16)
                {
                    szCount[cwNumChars++] = (char)InChar;
                    OEMWriteDebugByte((BYTE)InChar);
                }
            }
            // If it's a backspace, back up
            else if (InChar == 8)
            {
                if (cwNumChars > 0)
                {
                    cwNumChars--;
                    OEMWriteDebugByte((BYTE)InChar);
                }
            }
        }
    }

    // If it's a carriage return with an empty string, don't change anything.
    if (cwNumChars)
    {
        szCount[cwNumChars] = '\0';
        pBootCFG->numBootMe = atoi(szCount);
        if (pBootCFG->numBootMe > 255)
        {
            pBootCFG->numBootMe = 255;
        }
        else if (pBootCFG->numBootMe < 0)
        {
            pBootCFG->numBootMe = 1;
        }
    }
}


//------------------------------------------------------------------------------
//
//  Function:  SetDelay
//
//  Allows user to set a boot delay using the boot loader menu.
//
//  Parameters:
//      eBootCFG
//          [out] Points to bootloader configuration that will be updated with
//          the boot delay entered by the user.
//
//  Returns:
//      None.
//
//------------------------------------------------------------------------------
void SetDelay(BOOT_CFG *pBootCFG)
{
    char szCount[16];
    WORD cwNumChars = 0;
    UINT16 InChar = 0;
    INT32  dwCharRead = OEM_DEBUG_READ_NODATA;

    KITLOutputDebugString ( "\r\nEnter maximum number of seconds to delay [1-255]: ");

    while (!((InChar == 0x0d) || (InChar == 0x0a)))
    {
        dwCharRead = OEMReadDebugByte();
        if (dwCharRead != OEM_DEBUG_COM_ERROR && dwCharRead != OEM_DEBUG_READ_NODATA)
        {
            InChar = (UINT16)dwCharRead;

            // If it's a number or a period, add it to the string
            if ((InChar >= '0' && InChar <= '9'))
            {
                if (cwNumChars < 16)
                {
                    szCount[cwNumChars++] = (char)InChar;
                    OEMWriteDebugByte((BYTE)InChar);
                }
            }
            // If it's a backspace, back up
            else if (InChar == 8)
            {
                if (cwNumChars > 0)
                {
                    cwNumChars--;
                    OEMWriteDebugByte((BYTE)InChar);
                }
            }
        }
    }

    // If it's a carriage return with an empty string, don't change anything.
    if (cwNumChars)
    {
        szCount[cwNumChars] = '\0';
        pBootCFG->delay = atoi(szCount);
        if (pBootCFG->delay > 255)
        {
            pBootCFG->delay = 255;
        }
        else if (pBootCFG->delay < 1)
        {
            pBootCFG->delay = 1;
        }
    }
}


//------------------------------------------------------------------------------
//
//  Function:  NKCreateStaticMapping
//
//  Stub needed by fsl_usbfn_rndiskitl.lib. When linking with EBOOT.exe
//  use the stub. When linking with KITL.dll use the real function.
//
//------------------------------------------------------------------------------
VOID* NKCreateStaticMapping(DWORD phBase, DWORD size)
{
    UNREFERENCED_PARAMETER(size);
    return OALPAtoUA(phBase << 8);
}


void EnterCriticalSection(
  LPCRITICAL_SECTION lpCriticalSection
)
{
}
void InitializeCriticalSection(
  LPCRITICAL_SECTION lpCriticalSection
)
{
}

void DeleteCriticalSection(
  LPCRITICAL_SECTION lpCriticalSection
)
{
}

void  LeaveCriticalSection(
  LPCRITICAL_SECTION lpCriticalSection
)
{
}
LONG InterlockedCompareExchange(LONG volatile * Target, LONG NewValue, LONG OldValue)
{
    LONG value = *Target;
    if (value == OldValue)
        *Target = NewValue;
    return value;
}

void NKAcquireOalSpinLock(void)
{
}

void NKReleaseOalSpinLock(void)
{
}

//------------------------------------------------------------------------------
//
//  Function:  CRC32
//
//
//	Table driven CRC-32 computation using (MSB-first) polynom 0xEDB88320
//  Parameters:        
//			crc
//				[in] Initial CRC
//			buf
//				[in] Address of data buffer
//			len
//				[in] Length of data buffer
//
//  Returns:
//		(UINT32) CRC32 of data buffer. 
//------------------------------------------------------------------------------

UINT32 CRC32(UINT32 crc, BYTE* buf, size_t len)
{
	static UINT32 local_table[256];
	//static UINT32 * table;
	static int have_table = 1;
	DWORD * pdwP;
	DWORD * pdwQ;
	DWORD dwTempData;
	UCHAR rucByte;

	crc = ~crc;
	pdwQ = (DWORD*) (buf+len);

	for(pdwP=(DWORD*)buf; pdwP<pdwQ;pdwP++)
	{
		dwTempData = *pdwP;
		rucByte = (UCHAR)(dwTempData & 0xFF);
		crc = (crc >> 8) ^ crc32_table[(crc & 0xff) ^ rucByte];
		rucByte = (UCHAR)((dwTempData >> 8) & 0xFF);
		crc = (crc >> 8) ^ crc32_table[(crc & 0xff) ^ rucByte];
		rucByte = (UCHAR)((dwTempData >> 16) & 0xFF);
		crc = (crc >> 8) ^ crc32_table[(crc & 0xff) ^ rucByte];
		rucByte = (UCHAR)((dwTempData >> 24) & 0xFF);
		crc = (crc >> 8) ^ crc32_table[(crc & 0xff) ^ rucByte];
	}

	return ~crc;
}
