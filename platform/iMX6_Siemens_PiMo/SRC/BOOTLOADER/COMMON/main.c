//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//------------------------------------------------------------------------------
//
//  Copyright (C) 2012, Adeneo Embedded All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//  Copyright (C) 2007-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  main.c
//
//  Common routines for the bootloader.
//
//-----------------------------------------------------------------------------
#include "bsp.h"
#include <ethdbg.h>
#pragma warning(push)
#pragma warning(disable: 4115)
#include <fmd.h>
#pragma warning(pop)
#include "loader.h"
#include "sdfmd.h"
#include "atafmd.h"
#include "cspifmd.h"
#include <usbdbgser.h>
#include <eboot_version.h>

#define CLOCK_HOTFIX 0
//-----------------------------------------------------------------------------
// External Functions
#if NAND_USED
extern BOOL NANDBootInit();
extern BOOL NANDLoadIPL(VOID);
extern BOOL NANDLoadNK(VOID);
extern void NANDBootReserved();
#endif

extern void iomux_config();
extern BOOL pfd_init();

extern BOOL SDHCLoadNK(VOID);
extern VOID SDHCDisableBP(VOID);

extern BOOL ATALoadNK(VOID);
extern VOID ATADisableBP(VOID);

extern BOOL FlashLoadBootCFG(BYTE *pBootCfg, DWORD cbBootCfgSize);
extern BOOL FlashStoreBootCFG(BYTE *pBootCfg, DWORD cbBootCfgSize);
extern void ResetDefaultBootCFG(BOOT_CFG *pBootCFG);
extern void Launch(unsigned int uAddr);
extern BOOL BLMenu();
extern UINT32 OEMGetMagicNumber();

extern BOOL  SerialSendBlockAck(DWORD uBlockNumber);
extern BOOL  SerialSendBootRequest(const char * platformString);
extern BOOL  SerialWaitForBootAck(BOOL *pfJump);
extern DWORD SerialWaitForJump(VOID);

extern BOOL OALI2cInit(BYTE * pbySelfAddr,BYTE * pbyClkDiv);
extern BOOL OALPmicInit(VOID);

extern BOOL InitExpandIO();


extern void EbootDisplayInit();
extern void EbootDisplayOff();
extern void EbootDisplayUpdate();
extern BOOL OEMGetMACFromIIM(PUCHAR ucMAC);

extern void BootUpdateTimerInit(UINT32 uInterval);
extern void ReConfigureDDR();
extern BOOL FECEnableIsolate(UINT8 *pAddress);

extern VOID OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX index,
    DDK_CLOCK_GATE_MODE mode);

extern BOOL SDHCLoadNKSecure(VOID);
//extern void PF0100Init();

//-----------------------------------------------------------------------------
// External Variables
extern PCSP_IOMUX_REGS g_pIOMUX;
extern BOOT_CFG   g_BootCFG;
extern PCSP_CCM_REGS g_pCCM;
extern UINT8 PicData[];

//-----------------------------------------------------------------------------
// Defines
#define RETRY_COUNT             100000

// SRC SBMR bit fields (left shift)
#define SRC_SBMR_BOOT_CFG1_LSH              0
#define SRC_SBMR_BOOT_CFG2_LSH              8
#define SRC_SBMR_BOOT_CFG3_LSH              16
#define SRC_SBMR_BMOD_LSH                   24
#define SRC_SBMR_BT_FUSE_SEL_LSH            26
#define SRC_SBMR_TEST_MODE_LSH              27

// SRC SBMR bit fields (width)
#define SRC_SBMR_BOOT_CFG1_WID              8
#define SRC_SBMR_BOOT_CFG2_WID              8
#define SRC_SBMR_BOOT_CFG3_WID              8
#define SRC_SBMR_BMOD_WID                   2
#define SRC_SBMR_BT_FUSE_SEL_WID            1
#define SRC_SBMR_TEST_MODE_WID              3

// SRC SBMR bit fields (values)
#define SRC_SBMR_BOOT_CFG1_LSH              0

//-----------------------------------------------------------------------------
// Types


//-----------------------------------------------------------------------------
// Global Variables
BSP_ARGS *g_pBSPArgs;
IMAGE_TYPE g_ImageType;
IMAGE_MEMORY g_ImageMemory;

BOOLEAN             g_SerialUSBDownload = FALSE;
BOOL g_DownloadImage = TRUE;
BOOL g_DownloadbyUSB = FALSE;
UCHAR *g_DefaultRamAddress;
BOOL g_bNandBootloader;
BOOL g_bNandExist;
BOOL g_bSDHCBootloader;
BOOL g_bSDHCExist;
BOOL g_bATABootloader;
BOOL g_bATAExist;
BOOL g_bSpiBootloader;
BOOL g_bSpiExist;
BOOL g_bSplashScreen = FALSE;

// Used to save information about downloaded DIO mage
BOOT_BINDIO_CONTEXT g_BinDIO;

// Board rev: on GPIO1_22
DWORD g_dwBoardID = 0;

//-----------------------------------------------------------------------------
// Local Variables


//------------------------------------------------------------------------------
// Local Functions
//
BOOL LoadBootCFG(BOOT_CFG *BootCFG);
BOOL StoreBootCFG(BOOT_CFG *BootCFG);
void ConfigBootCFG(BOOT_CFG *pBootCFG);
BOOL OEMVerifyMemory(DWORD dwStartAddr, DWORD dwLength);
BOOL OEMReportError (DWORD dwReason, DWORD dwReserved);
void OEMMultiBINNotify(const PMultiBINInfo pInfo);

#ifndef NAND_USED 
    // NULL implementation for pass building
    BOOL NANDFormatNK(void) { return TRUE;}
    BOOL NANDFormatAll(void){ return TRUE;}
#endif

volatile DWORD iomux_reg_ua_addr;
//------------------------------------------------------------------------------
//
//  Function:  main
//
//  Bootloader main routine.
//
//  Parameters:
//      None.
//
//  Returns:
//      None.
//
//------------------------------------------------------------------------------
void main(void)
{
	/*We shouldn't be able write debug information before the uart is initialized*/
	dpCurSettings.ulZoneMask =  ZONE_ERROR;
	
	/*Get the virtual address of the IOMUX register*/
	iomux_reg_ua_addr =(DWORD)OALPAtoUA(CSP_BASE_REG_PA_IOMUXC);
	
	/*Configure platform iomux before anything else.*/
	iomux_config();
		
	/* Configure platform Phase Fractional Dividers (PFD)*/
	pfd_init();

    /* Common boot loader (blcommon) main routine.*/
    BootloaderMain();

    /* Should never get here.*/
    SpinForever();

}
//------------------------------------------------------------------------------
//
//  Function:  AuxMain
//
//  The interrupt service function. This function only have 4k stack. And customer MUST
//  makesure all HW operation in this function will not impact the HW operation outside as 
//  this function maybe called in anytime. The function implementation here must be as short
//  as possible.
//
//  Parameters:
//      None.
//
//  Returns:
//      None.
//
//------------------------------------------------------------------------------

void AuxMain()
{
    // TODO:  Port to GIC
    
    PCSP_GIC_DIST_REGS pGIC_DIST;
    PCSP_EPIT_REG pEPIT;

    pGIC_DIST = (PCSP_GIC_DIST_REGS) OALPAtoUA(CSP_BASE_REG_PA_GIC_DIST);
    pEPIT = (PCSP_EPIT_REG) OALPAtoUA(CSP_BASE_REG_PA_EPIT2);

    //Mask interrupt.    
    OUTREG32(&pGIC_DIST->ICDICER[IRQ_EPIT2 >> 5], 1U << (IRQ_EPIT2 % 32));
    //Clear interrupt flag.
    OUTREG32(&pEPIT->SR, CSP_BITFMASK(EPIT_SR_OCIF));
    //Do the things in the interrupt.
    EbootDisplayUpdate();

    //Enable interrupt.
    OUTREG32(&pGIC_DIST->ICDISER[IRQ_EPIT2 >> 5], 1U << (IRQ_EPIT2 % 32));
    
}

//------------------------------------------------------------------------------
//
//  Function:  OEMDebugInit
//
//  This function is the first called by the BLCOMMON framework when a boot 
//  loader starts. This function initializes the debug transport, usually just 
//  initializing the debug serial universal asynchronous receiver-transmitter 
//  (UART).
//
//  Parameters:
//      None.
//
//  Returns:
//      TRUE indicates success. FALSE indicates failure.
//
//------------------------------------------------------------------------------
BOOL OEMDebugInit (void)
{
	// Configure platform Phase Fractional Dividers (PFD)
	
    OEMInitDebugSerial();
    return TRUE;
}




//------------------------------------------------------------------------------
//
//  Function:  OEMPlatformInit
//
//  This function initializes the platform and is called by the BLCOMMON 
//  framework.
//
//  Parameters:
//      None.
//
//  Returns:
//      TRUE indicates success. FALSE indicates failure.
//------------------------------------------------------------------------------
BOOL OEMPlatformInit (void)
{
    BOOL rc = FALSE;
    UINT32 pSBMR, SBMR;
    //PUINT32 pM4IF_MCR0 = OALPAtoUA(CSP_BASE_REG_PA_M4IF + 0x08C);
    //UINT32 mcr0;
    UINT8 *pFuse = NULL;

    g_pBSPArgs = (BSP_ARGS *) IMAGE_SHARE_ARGS_UA_START;

    // Initialize I2C interface connected to board ID and PMIC
    /*if (!OALI2cInit(NULL, NULL))
    {
        OALMSGS(OAL_ERROR, (_T("OEMPlatformInit: OALI2cInit failed!\r\n")));
        goto cleanUp;
    }*/
	
    OALClockSetGatingMode( DDK_CLOCK_GATE_INDEX_IIM, DDK_CLOCK_GATE_MODE_ENABLED_ALL );
    pFuse = (UINT8 *) OALPAtoUA( CSP_BASE_REG_PA_IIM );
    g_dwBoardID = INREG8(pFuse+0x0878);//GP[15:8]
    g_dwBoardID |= ((UINT32)INREG8(pFuse+0x087c))<<8;//GP[7:0]
    g_dwBoardID=0xffff; //temperarily use.
    OALClockSetGatingMode( DDK_CLOCK_GATE_INDEX_IIM, DDK_CLOCK_GATE_MODE_DISABLED );
    
    //OALBoard_ID_I2CRead((PUINT32)&g_dwBoardID);

    KITLOutputDebugString("INFO: BoardID = 0x%x.\r\n", g_dwBoardID);

    //Setup the bus arbitriation priority.
    //Enable IPU/VPU AXI RW cache.
    INSREG32(&g_pIOMUX->GPR[4],0xFF,0xFF);
    //Set IPU to bypass mode.
    OUTREG32(&g_pIOMUX->GPR[6],0xffffffff);
    //OUTREG32(&g_pIOMUX->GPR[7],0xffffffff);
    

// Open All CG clocks
#if CLOCK_HOTFIX 
#include "clock_hotfix.h"     
#endif

    // TODO : Create a real Init for PMIC
	//PF0100Init();

    OEMBootInit();

    // Initialize timer for stall function
    BootTimerInit();

    // TODO:  Port boot mode decode

    // Get boot mode from SRC SBMR
    pSBMR = (UINT32) OALPAtoUA(CSP_BASE_REG_PA_SRC+4);
    if (!pSBMR) 
    {
        OALMSGS(OAL_ERROR, (_T("OEMPlatformInit: SRC mapping failed!\r\n")));
        goto cleanUp;
    }

    SBMR = INREG32(pSBMR);

    KITLOutputDebugString("INFO: SBMR = 0x%x.\r\n", SBMR);

    //  Boot Device         BOOT_CFG1[7:3]
    //------------------------------------
    //  WEIM (NOR)              00000
    //  WEIM (OneNAND)          00001
    //  HD (SATA)               00100
    //  Serial-ROM (I2C)        00110             
    //  Serial-ROM (SPI)        00111
    //  SD/eSD (regular)        0100x
    //  SD/eSD (fast)           0101x
    //  MMC/eMMC (regular)      0110x
    //  MMC/eMMC (fast)         0111x
    //  NAND                    1xxxx    
    switch(CSP_BITFEXT(SBMR, SRC_SBMR_BOOT_CFG1) >> 3)
    {

    // WEIM
    case 0x0:
    case 0x1:
        break;

    // RESERVED
    case 0x2:
    case 0x3:
        break;

    // HD
    case 0x4:
        g_bATABootloader = TRUE;
        KITLOutputDebugString("INFO:  Bootloader launched from ATA.\r\n");
        break;

    // Serial-ROM
    case 0x6:
    case 0x7:
        g_bSDHCBootloader = TRUE;
        KITLOutputDebugString("INFO:  Bootloader launched from SPI but workaround boots eboot from SD.\r\n");
        break;


    // SD/eSD
    case 0x8:
    case 0x9:
    case 0xA:
    case 0xB:
    // MMC/eMMC    
    case 0xC:
    case 0xD:
    case 0xE:
    case 0xF:
        g_bSDHCBootloader = TRUE;
        KITLOutputDebugString("INFO:  Bootloader launched from SD.\r\n");
        break;

    // NAND
    default:            
        g_bNandBootloader = TRUE;
        KITLOutputDebugString("INFO:  Bootloader launched from NAND.\r\n");
        break;
    }


    // Initialize the BSP args structure.
    //
    if ((g_pBSPArgs->header.signature != OAL_ARGS_SIGNATURE) || 
        (g_pBSPArgs->header.oalVersion != OAL_ARGS_VERSION) || 
        (g_pBSPArgs->header.bspVersion != BSP_ARGS_VERSION))
    {
        // zero-out the unused protions of the BspArgs. Frequencies already setup in call to OalBspArgsInit()
        memset(g_pBSPArgs->deviceId, 0, sizeof(g_pBSPArgs->deviceId));
        memset(&g_pBSPArgs->kitl, 0, sizeof(g_pBSPArgs->kitl));
        g_pBSPArgs->header.signature  = OAL_ARGS_SIGNATURE;
        g_pBSPArgs->header.oalVersion = OAL_ARGS_VERSION;
        g_pBSPArgs->header.bspVersion = BSP_ARGS_VERSION;
        g_pBSPArgs->kitl.flags             =  OAL_KITL_FLAGS_VMINI;
        g_pBSPArgs->kitl.devLoc.IfcType    = Internal;
        g_pBSPArgs->kitl.devLoc.BusNumber  = 0;
        g_pBSPArgs->updateMode = FALSE;
		memset(&g_pBSPArgs->kitl, 0, sizeof(g_pBSPArgs->kitl));
		g_pBSPArgs->ebootVersion.major = EBOOT_VERSION_MAJOR;
		g_pBSPArgs->ebootVersion.minor = EBOOT_VERSION_MINOR;
		g_pBSPArgs->ebootVersion.incremental = EBOOT_VERSION_INCREMENTAL;
		KITLOutputDebugString("Eboot version = %d.%d.%d\r\n", g_pBSPArgs->ebootVersion.major, g_pBSPArgs->ebootVersion.minor, g_pBSPArgs->ebootVersion.incremental);
    }
    
    // Attempt to initialize the SD/MMC driver
#if 1
    if (!SDMMC_Init ())
    {
        KITLOutputDebugString("WARNING: OEMPlatformInit: Failed to initialize SDHC device.\r\n");
        g_bSDHCExist = FALSE;
    }
    else
    {
        g_bSDHCExist = TRUE;
    }
#endif

#if 0
	// BOOT from SATA seems OK but hard disk is not detected in kernel, some additional
	// steps may be needed to properly reset the SATA controller and disk
	KITLOutputDebugString("Initializing SATA\r\n");
    if (!ATA_Init ())
    {
        KITLOutputDebugString("WARNING: OEMPlatformInit: Failed to initialize ATA device.\r\n");
        g_bATAExist = FALSE;
    }
    else
    {
		KITLOutputDebugString("OEMPlatformInit: ATA device init OK\r\n");
        g_bATAExist = TRUE;
    }
#endif

#if defined(NAND_USED)

    // Attempt to initialize the NAND flash driver
    if (!FMD_Init(NULL, NULL, NULL) || !NANDBootInit())
    {
        KITLOutputDebugString("WARNING: OEMPlatformInit: Failed to initialize NAND flash device.\r\n");
        g_bNandExist = FALSE;
    }
    else
    {
        KITLOutputDebugString("INFO: OEMPlatformInit: Initialized NAND flash device.\r\n");
        g_bNandExist = TRUE;
        NANDBootReserved();        
    }
#endif
    // Attempt to initialize the SPI Flash driver
#if 0
    if (!SPIFMD_Init ())
    {
        KITLOutputDebugString("WARNING: OEMPlatformInit: Failed to initialize SPI device.\r\n");
        g_bSpiExist = FALSE;
    }
    else
    {
        g_bSpiExist = TRUE;
    }
#endif

    // Load eboot configuration
    //
    if (!LoadBootCFG(&g_BootCFG)) 
    {

        // Load default bootloader configuration settings.
        KITLOutputDebugString("ERROR: flash initialization failed - loading bootloader defaults...\r\n");
        ResetDefaultBootCFG(&g_BootCFG);
    }

    //OEMGetMACFromIIM( (UCHAR*)&g_BootCFG.mac[0]);
    //KITLOutputDebugString("INFO: MAC address set to: %x:%x:%x:%x:%x:%x\r\n",
    //          g_BootCFG.mac[0] & 0x00FF, g_BootCFG.mac[0] >> 8,
    //          g_BootCFG.mac[1] & 0x00FF, g_BootCFG.mac[1] >> 8,
    //          g_BootCFG.mac[2] & 0x00FF, g_BootCFG.mac[2] >> 8);

    //Config EbootCFG
    ConfigBootCFG(&g_BootCFG);

    // Set up optional bootloader function pointers.
    //
    g_pOEMMultiBINNotify = OEMMultiBINNotify;
    g_pOEMVerifyMemory = OEMVerifyMemory;
    g_pOEMReportError = OEMReportError;
        
    rc = TRUE;

cleanUp:
    return rc;
}


//------------------------------------------------------------------------------
//
//  Function:  OEMPreDownload
//
//  This function is called by the BLCOMMON framework prior to download and can
//  be customized to prompt for user feedback, such as obtaining a static IP 
//  address or skipping the download and jumping to a flash-resident run-time 
//  image.
//
//  Parameters:        
//      None.
//
//  Returns:
//      Possible return values for OEMPreDownload:
//      
//      Value               Description 
//      -----               -----------
//      BL_DOWNLOAD = 0     Download the OS image from the host machine. 
//      BL_JUMP = 1         Skip the download and jump to a resident OS image. 
//      BL_ERROR = -1       Image download is unsuccessful. 
//------------------------------------------------------------------------------
DWORD OEMPreDownload(void)
{
    UINT32 rc = (DWORD)BL_ERROR; 
    BOOL  fGotJumpImg = FALSE;
    const char * platformString = "IMX6Q";

    //Initialize panel display on eboot
    EbootDisplayInit();
    BootUpdateTimerInit(33);
    // User menu code...
    //
    if (!BLMenu())
    {
        return rc;
    }

    // Create device name based on Ethernet address (this is how Platform Builder identifies this device).
    //
    OALKitlCreateName(BSP_DEVICE_PREFIX, g_pBSPArgs->kitl.mac, (CHAR *)g_pBSPArgs->deviceId);
    KITLOutputDebugString("INFO: Using device name: '%s'\n", g_pBSPArgs->deviceId);

    if( g_SerialUSBDownload && g_DownloadImage)
    {
        // Send boot requests indefinitely
        do
        {
            KITLOutputDebugString("Sending boot request...\r\n");
            if(!SerialSendBootRequest(platformString))
            {
                KITLOutputDebugString("Failed to send boot request\r\n");
                return (DWORD)BL_ERROR;
            }
        }
        while(!SerialWaitForBootAck(&fGotJumpImg));
        
        // Ack block zero to start the download
        SerialSendBlockAck(0);

        if( fGotJumpImg )
        {
            KITLOutputDebugString("Received boot request ack... jumping to image\r\n");
        }
        else
        {
            KITLOutputDebugString("Received boot request ack... starting download\r\n");
        }
        
    }else
    {
        fGotJumpImg = GetPreDownloadInfo (&g_BootCFG);
    }
     
    if (!g_DownloadImage || // this gets set in the BLMenu() function
        fGotJumpImg)        // this gets set in EbootInitEtherTransport
    {
        switch(g_BootCFG.autoDownloadImage)
        {
        case BOOT_CFG_AUTODOWNLOAD_NK_NOR:
            rc = BL_JUMP;
            break;

#if defined(NAND_USED)
        case BOOT_CFG_AUTODOWNLOAD_NK_NAND:
            if (NANDLoadNK())
            {
                rc = BL_JUMP;
            }
            else
            {
                KITLOutputDebugString("ERROR: Failed to load OS image from NAND.\r\n");
            }
            break;
            
        case BOOT_CFG_AUTODOWNLOAD_IPL_NAND:
            if (NANDLoadIPL())
            {
                rc = BL_JUMP;
            }
            else
            {
                KITLOutputDebugString("ERROR: Failed to load IPL image from NAND.\r\n");
            }
            break;
#endif
        case BOOT_CFG_AUTODOWNLOAD_NK_SD:
#ifdef CRC_APPEND
			KITLOutputDebugString("INFO: Calling SDHCLoadNKSecure.\r\n");
            if (SDHCLoadNKSecure())
            {
                rc = BL_JUMP;
            }
#else
			KITLOutputDebugString("INFO: Calling SDHCLoadNK.\r\n");
			if (SDHCLoadNK())
            {
                rc = BL_JUMP;
            }
#endif
            else
            {
                KITLOutputDebugString("ERROR: Failed to load OS image from SD/MMC.\r\n");
            }
            break;

        case BOOT_CFG_AUTODOWNLOAD_NK_ATA:
            if (ATALoadNK())
            {
                rc = BL_JUMP;
            }
            else
            {
                KITLOutputDebugString("ERROR: Failed to load OS image from ATA.\r\n");
            }
            break;            
        }

        // Set the clean boot flag so that OAL will let the kernel know that
        // it needs a clean boot
        //
        // g_pBSPArgs->bCleanBootFlag = TRUE;
    }
    else if (g_DownloadImage)
    {
        rc = BL_DOWNLOAD;
    }


    return(rc);
}


//------------------------------------------------------------------------------
//
//  Function:  OEMLaunch
//
//  This function launches the run-time image. It is the last one called by 
//  the BLCOMMON framework.
//
//  Parameters:        
//      dwImageStart 
//          [in] Starting address of OS image. 
//
//      dwImageLength 
//          [in] Length, in bytes, of the OS image. 
//
//      dwLaunchAddr 
//          [in] First instruction of the OS image.
//
//      pRomHdr 
//          [out] Pointer to the ROM header structure.
//
//  Returns:
//      None.
//------------------------------------------------------------------------------
void OEMLaunch (DWORD dwImageStart, DWORD dwImageLength, DWORD dwLaunchAddr, const ROMHDR *pRomHdr)
{
    UINT32 PhysAddress;
    
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(pRomHdr);

    switch(g_BootCFG.autoDownloadImage)
    {
    case BOOT_CFG_AUTODOWNLOAD_NK_NOR:
        // Set launch address for NOR OS image.  OS executes-in-place from NOR.
        PhysAddress = IMAGE_BOOT_NKIMAGE_NOR_PA_START;
        break;

    case BOOT_CFG_AUTODOWNLOAD_NK_NAND:
    case BOOT_CFG_AUTODOWNLOAD_NK_SD:
    case BOOT_CFG_AUTODOWNLOAD_NK_ATA:
        // Set launch address for NAND/SDHC/ATA OS image.  OS is copied into RAM for execution.
        PhysAddress = IMAGE_BOOT_NKIMAGE_RAM_PA_START;
        break;

    case BOOT_CFG_AUTODOWNLOAD_IPL_NAND:
        // Set launch address for NAND IPL image.  IPL is copied into RAM for execution.
        PhysAddress = IMAGE_BOOT_IPLIMAGE_RAM_START;
        break;

    default:
        // If a launch address wasn't specified - use the last known good address.
        //
        if (!dwLaunchAddr)
        {
            dwLaunchAddr = g_BootCFG.dwLaunchAddr;
        }
        else
        {
            if (g_BootCFG.dwLaunchAddr != dwLaunchAddr ||
                g_BootCFG.dwPhysStart  != dwImageStart ||
                g_BootCFG.dwPhysLen    != dwImageLength)
            {
                g_BootCFG.dwLaunchAddr = dwLaunchAddr;
                g_BootCFG.dwPhysStart  = dwImageStart;
                g_BootCFG.dwPhysLen    = dwImageLength;

                StoreBootCFG(&g_BootCFG);
            }
        }

        // Translate the image start address (virtual address) to a physical address.
        //
        PhysAddress = (UINT32) OALVAtoPA((VOID *)dwLaunchAddr);
        break;
    }

    // disable boot partition in case there is eMMC or eSD in the system, so that OS file system does not overwrite boot partition
    if (g_bSDHCExist)
        SDHCDisableBP();
    
    KITLOutputDebugString("Download successful!  Jumping to image at 0x%x (physical 0x%x)...\r\n", dwLaunchAddr, PhysAddress);

    // Wait for PB connection...
    //
    if (g_DownloadImage)
    {
        if( g_SerialUSBDownload )
        {
            SerialWaitForJump();

        }else
        {
            GetLaunchInfo ();
        }
    }

    EbootDisplayOff();


    // Jump to the image we just downloaded.
    //
    KITLOutputDebugString("\r\n\r\n\r\n");
    OEMWriteDebugLED(0, 0x00FF);
    Launch(PhysAddress);

    // Should never get here...
    //
    SpinForever();

}


//------------------------------------------------------------------------------
//
//  Function:  OEMMultiBINNotify
//
//  Receives/processes download file manifest.
//
//  Parameters:
//      pInfo 
//          [in] Download manifiest provided by BLCOMMON framework.
//
//  Returns:
//             void
//------------------------------------------------------------------------------
void OEMMultiBINNotify(const PMultiBINInfo pInfo)
{
    CHAR szFileName[MAX_PATH];
    int i, key, fileExtPos;
    UINT8 * pAddr;

    if (!pInfo) return;

    KITLOutputDebugString("INFO: OEMMultiBINNotify (dwNumRegions = %d, dwRegionStart = 0x%x).\r\n", 
        pInfo->dwNumRegions, pInfo->Region[0].dwRegionStart);
    
    // Only inspect manifest if this is a monolithic .nb0 or diskimage file
    if (pInfo->dwNumRegions != 1) 
        return;

    //  NBO and DIO files will have start address of zero
    if (pInfo->Region[0].dwRegionStart == 0)
    {

        // Convert the file name to lower case
        i = 0;
        fileExtPos = 0;
        while ((pInfo->Region[0].szFileName[i] != '\0') && (i < MAX_PATH))
        {
            szFileName[i] = (CHAR)tolower(pInfo->Region[0].szFileName[i]);
            // Keep track of file extension position
            if (szFileName[i] == '.')
            {
                fileExtPos = i;
            }
            i++;
        }
        
        // Check for EBOOT/SBOOT update
        if  ( (strncmp(szFileName, EBOOT_NB0_FILE, EBOOT_NB0_FILE_LEN) == 0) ||
           (strncmp(szFileName, SBOOT_NB0_FILE, SBOOT_NB0_FILE_LEN) == 0) )
        {
            // Remap the start address to the region specified by the user
            KITLOutputDebugString("Specify destination for EBOOT/SBOOT NB0 [1 = NOR, 2 = NAND, 3 = SD/MMC, 4 = SPI Flash, 5 = ATA]: ");
            do {
                key = OEMReadDebugByte();
            } while ((key != '1') && (key != '2') && (key != '3') && (key != '4'));
            KITLOutputDebugString("\r\n");
            switch (key)
            {
            case '1':
                pInfo->Region[0].dwRegionStart = (DWORD) OALPAtoCA(IMAGE_BOOT_BOOTIMAGE_NOR_PA_START);
                break;
                
            case '2':
                pInfo->Region[0].dwRegionStart = (DWORD) OALPAtoCA(IMAGE_BOOT_BOOTIMAGE_NAND_PA_START);
                break;

            case '3':
                pInfo->Region[0].dwRegionStart = (DWORD) OALPAtoCA(IMAGE_BOOT_BOOTIMAGE_SD_PA_START);
                break;

            case '4':
                pInfo->Region[0].dwRegionStart = (DWORD) OALPAtoCA(IMAGE_BOOT_BOOTIMAGE_SPI_PA_START);
                break;

            case '5':
                pInfo->Region[0].dwRegionStart = (DWORD) OALPAtoCA(IMAGE_BOOT_BOOTIMAGE_ATA_PA_START);
                break;
                
            default:
                return;
            }

            KITLOutputDebugString("\r\nINFO: EBOOT/SBOOT NB0 remapped to 0x%x.\r\n", pInfo->Region[0].dwRegionStart);
        }
        // Check for splash screen update
        else if  (strncmp(szFileName, SPLASHSCREEN_NB0_FILE, SPLASHSCREEN_NB0_FILE_LEN) == 0) 
        {
            // Remap the start address to the region specified by the user
            KITLOutputDebugString("Specify destination for Splash screen NB0 \r\n[1 = NOR, 2 = NAND, 3 = SD/MMC, 4 = SPI Flash]: ");
            do {
                key = OEMReadDebugByte();
            } while ((key != '1') && (key != '2') && (key != '3') && (key != '4'));
            KITLOutputDebugString("\r\n");
            switch (key)
            {
            case '1':
                pInfo->Region[0].dwRegionStart = (DWORD) OALPAtoCA(IMAGE_BOOT_BOOTIMAGE_NOR_PA_START);
                break;
                
            case '2':
                pInfo->Region[0].dwRegionStart = (DWORD) OALPAtoCA(IMAGE_BOOT_BOOTIMAGE_NAND_PA_START);
                break;

            case '3':
                pInfo->Region[0].dwRegionStart = (DWORD) OALPAtoCA(IMAGE_BOOT_BOOTIMAGE_SD_PA_START);
                break;

            case '4':
                pInfo->Region[0].dwRegionStart = (DWORD) OALPAtoCA(IMAGE_BOOT_BOOTIMAGE_SPI_PA_START);
                break;

            default:
                return;
            }
            pAddr = (UINT8 *)BOOT_FLASHBLOCK_CACHE_START;
            memcpy(pAddr, (UINT8 *)((UINT32)OALPAtoUA(IMAGE_BOOT_BOOTIMAGE_RAM_PA_START)+0x1000), IMAGE_BOOT_BOOTIMAGE_RAM_SIZE);
            g_bSplashScreen = TRUE;
        }       
        // If  file to be downloaded has an NB0 extension, assume it is an NK NB0 image
        else if (fileExtPos && (strncmp(&szFileName[fileExtPos], ".nb0", 4) == 0))
        {
            // Remap the start address to the region specified by the user
            KITLOutputDebugString("Specify destination for NK NB0 [0 = RAM, 1 = NOR, 2 = NAND, 3 = SD/MMC], 5 = ATA]: ");
            do {
                key = OEMReadDebugByte();
            } while ((key != '0') && (key != '1') && (key != '2') && (key != '3') && (key != '5'));
            KITLOutputDebugString("\r\n");
            switch (key)
            {
            case '0':
                pInfo->Region[0].dwRegionStart = (DWORD) OALPAtoCA(IMAGE_BOOT_NKIMAGE_RAM_PA_START);
                break;

            case '1':
                pInfo->Region[0].dwRegionStart = (DWORD) OALPAtoCA(IMAGE_BOOT_NKIMAGE_NOR_PA_START);
                break;
                
            case '2':
                pInfo->Region[0].dwRegionStart = (DWORD) OALPAtoCA(IMAGE_BOOT_NKIMAGE_NAND_PA_START);
                break;

            case '3':
                pInfo->Region[0].dwRegionStart = (DWORD) OALPAtoCA(IMAGE_BOOT_NKIMAGE_SD_PA_START);
                break;

            case '5':
                pInfo->Region[0].dwRegionStart = (DWORD) OALPAtoCA(IMAGE_BOOT_NKIMAGE_ATA_PA_START);
                break;
                                
            default:
                return;
            }

            KITLOutputDebugString("\r\nINFO: NK NB0 remapped to 0x%x.\r\n", pInfo->Region[0].dwRegionStart);
        }

        // Notify user of unsupported format
        else 
        {
            KITLOutputDebugString("\r\nWARNING: Unsupported binary format.  Image not remapped.\r\n");
        }
    }

#if 0
    // If this is a Windows Mobile disk image BIN, then dwRegionStart will
    // be offset into NAND.
    else if (pInfo->Region[0].dwRegionStart < IMAGE_BOOT_RAMDEV_RAM_PA_START)
    {
        g_ImageType = IMAGE_TYPE_BINDIO;
        g_ImageMemory = IMAGE_MEMORY_NAND;
        KITLOutputDebugString("\r\nINFO: DIO image with starting address 0x%x.\r\n", pInfo->Region[0].dwRegionStart);
    }
#endif
    
    return;
}

//------------------------------------------------------------------------------
//
//  Function:  OEMReportError
//
//  Error reporting function provided by BLCOMMON framework.
//
//  Parameters:
//      dwReason 
//          [in] Reason for error
//
//      dwReserved 
//          [in] Reserved parameter
//
//  Returns:
//             void
//------------------------------------------------------------------------------
BOOL OEMReportError (DWORD dwReason, DWORD dwReserved)
{
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(dwReserved);

    KITLOutputDebugString("INFO: OEMReportError Reason 0x%x\r\n", dwReason);

    return TRUE;    
}

//-----------------------------------------------------------------------------
//
//  Function:  OEMVerifyMemory
//
//  This function verifies that the address provided is in valid memory,
//  and sets globals to describe the image region being updated.
//
//  Parameters:
//      dwStartAddr 
//          [in] Address to be verified. 
//
//      dwLength 
//          [in] Length of the address, in bytes.
//
//  Returns:
//      TRUE indicates success. FALSE indicates failure.
//
//-----------------------------------------------------------------------------
BOOL OEMVerifyMemory(DWORD dwStartAddr, DWORD dwLength)
{
    BOOL  rc = TRUE;
    DWORD dwPhysVerifyStart; 
    DWORD dwPhysVerifyEnd;

    // First check for DIO image flagged by OEMMultiBINNotify
    if (g_ImageType == IMAGE_TYPE_BINDIO)
    {
        KITLOutputDebugString("INFO: Downloading DIO NAND image.\r\n");
        return (TRUE);
    }

    dwPhysVerifyStart = (DWORD) OALVAtoPA((void *)dwStartAddr); 
    dwPhysVerifyEnd   = dwPhysVerifyStart + dwLength - 1;

    KITLOutputDebugString("INFO: OEMVerifyMemory (CA = 0x%x, PAStart = 0x%x, PAEnd = 0x%x, length = 0x%x)\r\n", 
        dwStartAddr, dwPhysVerifyStart, dwPhysVerifyEnd, dwLength);

    //Add addtional 64k boundary check for nand image, 
    //After VAtoPA() conversion, nand and sd and ata may has the same physical address range are overlapped, here need addtional check.
    if ((dwPhysVerifyStart == IMAGE_BOOT_BOOTIMAGE_NAND_PA_START) && (dwPhysVerifyEnd <= IMAGE_BOOT_BOOTIMAGE_NAND_PA_END))
    {
        KITLOutputDebugString("INFO: Downloading EBOOT/SBOOT NAND image.\r\n");
        g_ImageType = IMAGE_TYPE_BOOT;
        g_ImageMemory = IMAGE_MEMORY_NAND;
    }
    else if ((dwPhysVerifyStart == IMAGE_BOOT_IPLIMAGE_NAND_PA_START) && (dwPhysVerifyEnd <= IMAGE_BOOT_IPLIMAGE_NAND_PA_END))
    {
        KITLOutputDebugString("INFO: Downloading IPL NAND image.\r\n");
        g_ImageType = IMAGE_TYPE_IPL;
        g_ImageMemory = IMAGE_MEMORY_NAND;
    }
    // DIO and NK share this start address
    else if ((dwPhysVerifyStart == IMAGE_BOOT_NKIMAGE_NAND_PA_START) && (dwPhysVerifyEnd <= IMAGE_BOOT_NKIMAGE_NAND_PA_END))
    {
        KITLOutputDebugString("INFO: Downloading NK NAND image.\r\n");
        g_ImageType = IMAGE_TYPE_NK;
        g_ImageMemory = IMAGE_MEMORY_NAND;
    }
    else if ((dwPhysVerifyStart >= IMAGE_BOOT_BOOTIMAGE_NOR_PA_START) && (dwPhysVerifyEnd <= IMAGE_BOOT_BOOTIMAGE_NOR_PA_END))
    {
        KITLOutputDebugString("INFO: Downloading EBOOT/SBOOT NOR image.\r\n");
        g_ImageType = IMAGE_TYPE_BOOT;
        g_ImageMemory = IMAGE_MEMORY_NOR;
    }
    else if ((dwPhysVerifyStart >= IMAGE_BOOT_NKIMAGE_NOR_PA_START) && (dwPhysVerifyEnd <= IMAGE_BOOT_NKIMAGE_NOR_PA_END))
    {
        KITLOutputDebugString("INFO: Downloading NK NOR image.\r\n");
        g_ImageType = IMAGE_TYPE_NK;
        g_ImageMemory = IMAGE_MEMORY_NOR;
    }
    else if ((dwPhysVerifyStart >= IMAGE_BOOT_NORDEV_NOR_PA_START) && (dwPhysVerifyEnd <= IMAGE_BOOT_NORDEV_NOR_PA_END))
    {
        KITLOutputDebugString("INFO: Downloading NK NOR image.\r\n");
        KITLOutputDebugString("WARNING:  NK image will overwrite EBOOT reserved space \r\n");
        g_ImageType = IMAGE_TYPE_NK;
        g_ImageMemory = IMAGE_MEMORY_NOR;
    }
    else if ((dwPhysVerifyStart >= IMAGE_BOOT_RAMDEV_RAM_PA_START) && (dwPhysVerifyEnd <= IMAGE_BOOT_RAMDEV_RAM_PA_END))
    {
        KITLOutputDebugString("INFO: Downloading NK RAM image.\r\n");
        g_ImageType = IMAGE_TYPE_NK;
        g_ImageMemory = IMAGE_MEMORY_RAM;
    }
    else if ((dwPhysVerifyStart >= IMAGE_BOOT_BOOTIMAGE_SD_PA_START) && (dwPhysVerifyEnd <= IMAGE_BOOT_BOOTIMAGE_SD_PA_END))
    {
        KITLOutputDebugString("INFO: Downloading EBOOT/SBOOT for SD/MMC image.\r\n");
        g_ImageType = IMAGE_TYPE_BOOT;
        g_ImageMemory = IMAGE_MEMORY_SD;
    }
    // DIO and NK share this start address
    else if ((dwPhysVerifyStart >= IMAGE_BOOT_NKIMAGE_SD_PA_START) && (dwPhysVerifyEnd <= IMAGE_BOOT_NKIMAGE_SD_PA_END))
    {
        KITLOutputDebugString("INFO: Downloading NK SD/MMC image.\r\n");
        g_ImageType = IMAGE_TYPE_NK;
        g_ImageMemory = IMAGE_MEMORY_SD;
    }
    else if ((dwPhysVerifyStart >= IMAGE_BOOT_BOOTIMAGE_ATA_PA_START) && (dwPhysVerifyEnd <= IMAGE_BOOT_BOOTIMAGE_ATA_PA_END))
    {
        KITLOutputDebugString("INFO: Downloading EBOOT/SBOOT for ATA image.\r\n");
        g_ImageType = IMAGE_TYPE_BOOT;
        g_ImageMemory = IMAGE_MEMORY_ATA;
    }
    // DIO and NK share this start address
    else if ((dwPhysVerifyStart >= IMAGE_BOOT_NKIMAGE_ATA_PA_START) && (dwPhysVerifyEnd <= IMAGE_BOOT_NKIMAGE_ATA_PA_END))
    {
        KITLOutputDebugString("INFO: Downloading NK ATA image.\r\n");
        g_ImageType = IMAGE_TYPE_NK;
        g_ImageMemory = IMAGE_MEMORY_ATA;
    }    
    else if ((dwPhysVerifyStart >= IMAGE_BOOT_BOOTIMAGE_SPI_PA_START) && (dwPhysVerifyEnd <= IMAGE_BOOT_BOOTIMAGE_SPI_PA_END))
    {
        KITLOutputDebugString("INFO: Downloading EBOOT/SBOOT for SPI Flash image.\r\n");
        g_ImageType = IMAGE_TYPE_BOOT;
        g_ImageMemory = IMAGE_MEMORY_SPI;
    }
    else
    {
        KITLOutputDebugString("ERROR: Invalid address range.\r\n");
        rc = FALSE;
    }

    return(rc);

}


//------------------------------------------------------------------------------
//
//  Function:  StoreBootCFG
//
//  Stores bootloader configuration information (menu settings, etc.) in flash.
//
//  Parameters:
//      BootCfg 
//          [in] Points to bootloader configuration to be stored. 
//
//  Returns:
//      TRUE indicates success. FALSE indicates failure.
//
//-----------------------------------------------------------------------------
BOOL StoreBootCFG(BOOT_CFG *BootCfg)
{
    if (!FlashStoreBootCFG((BYTE*) BootCfg, sizeof(BOOT_CFG)))
    {
        KITLOutputDebugString("ERROR: StoreBootCFG: failed to write configuration.\r\n");
        return(FALSE);
    }

    return(TRUE);
}


//------------------------------------------------------------------------------
//
//  Function:  LoadBootCFG
//
//  Retrieves bootloader configuration information (menu settings, etc.) from 
//  flash.
//
//  Parameters:
//      BootCfg 
//          [out] Points to bootloader configuration that will be filled with
//          loaded data. 
//
//  Returns:
//      TRUE indicates success. FALSE indicates failure.
//
//-----------------------------------------------------------------------------
BOOL LoadBootCFG(BOOT_CFG *BootCfg)
{

    if (!FlashLoadBootCFG((BYTE*) BootCfg, sizeof(BOOT_CFG))) {
        KITLOutputDebugString("ERROR: LoadBootCFG: failed to load configuration.\r\n");
        return(FALSE);
    }

    // Is the CFG data valid?  Check for the magic number that was written the last time
    // the CFG block was updated.  If Eboot has never been run, there will be no configuration
    // information, so the magic number will likely not be found.  In this case, setup the 
    // factory defaults and store them into Flash.
    //
    if (BootCfg->ConfigMagicNumber != OEMGetMagicNumber())
    {
        KITLOutputDebugString("ERROR: LoadBootCFG: ConfigMagicNumber not correct. Expected = 0x%x ; Actual = 0x%x.\r\n", 
            OEMGetMagicNumber(), BootCfg->ConfigMagicNumber);
        ResetDefaultBootCFG(BootCfg);
    }

    g_DefaultRamAddress = (PUCHAR)g_BootCFG.dwLaunchAddr;

    return(TRUE);
}


//------------------------------------------------------------------------------
//
//  Function:  ConfigBootCFG
//
//  Update PCB ram and config system according eboot config
//
//  Parameters:
//      eBootCFG 
//          [in] Points to bootloader configuration
//
//  Returns:
//      None.
//
//-----------------------------------------------------------------------------
void ConfigBootCFG(BOOT_CFG *pBootCFG)
{
    if(pBootCFG->dwConfigFlags & CONFIG_FLAGS_KITL_ENABLE)
    {
        g_pBSPArgs->kitl.flags  |= OAL_KITL_FLAGS_ENABLED;        
    } 
    else 
    {
        g_pBSPArgs->kitl.flags  &= ~OAL_KITL_FLAGS_ENABLED;        
    }

    if(pBootCFG->dwConfigFlags & CONFIG_FLAGS_KITL_PASSIVE)
    {
        g_pBSPArgs->kitl.flags  |= OAL_KITL_FLAGS_PASSIVE;        
    } 
    else 
    {
        g_pBSPArgs->kitl.flags  &= ~OAL_KITL_FLAGS_PASSIVE;        
    }

    // update bsp args kitl flag
    if(pBootCFG->dwConfigFlags & CONFIG_FLAGS_KITL_INT)
    {
        g_pBSPArgs->kitl.flags  &= ~OAL_KITL_FLAGS_POLL;        
    }
    else
    {
        g_pBSPArgs->kitl.flags  |= OAL_KITL_FLAGS_POLL;        
    }

}


//------------------------------------------------------------------------------
//
//  Function:  OEMSerialSendRaw
//
//  The function transmits the buffer passed on the serial transport
//
//  Parameters:
//      pbFrame
//          [in] Frame to be transmitted.
//      cbFrame
//          [in] Frame length
//
//  Returns:
//      TRUE.
//
//-----------------------------------------------------------------------------
BOOL OEMSerialSendRaw(LPBYTE pbFrame, USHORT cbFrame)
{
    UINT16 byteSent;

    while (cbFrame) {
        byteSent = Serial_Send(pbFrame, cbFrame);
        cbFrame -= byteSent;
        pbFrame += byteSent;
    }
    return TRUE;
}


//------------------------------------------------------------------------------
//
//  Function:  OEMSerialRecvRaw
//
//  The function transmits the buffer passed on the serial transport
//
//  Parameters:
//      pbFrame
//          [out] Frame buffer for receiving data
//      cbFrame
//          [in] Frame length
//      bWaitInfinite
//          [in] Flag to inidicate infinite wait
//
//  Returns:
//      TRUE.
//
//-----------------------------------------------------------------------------
BOOL OEMSerialRecvRaw(LPBYTE pbFrame, PUSHORT pcbFrame, BOOLEAN bWaitInfinite)
{
    USHORT byteToRecv = *pcbFrame;
    UINT16 byteRecv;
    UINT Retries = 0;

    while (byteToRecv) {
        byteRecv = Serial_Recv(pbFrame, byteToRecv);

        if (!bWaitInfinite) {
            // check retry count is we don't want to wait infinite
            // we only return false if we have not receive any data and retry more than RETRY_COUNT times
            if ((byteRecv == 0) && (byteToRecv == *pcbFrame)) {
                Retries++;
                if (Retries > RETRY_COUNT) {
                    *pcbFrame = 0;
                    return FALSE;
                }
            }
        }
        byteToRecv -= byteRecv;
        pbFrame += byteRecv;
    }
    return TRUE;
}


