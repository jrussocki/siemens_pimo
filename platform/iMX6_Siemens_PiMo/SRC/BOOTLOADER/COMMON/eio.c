//------------------------------------------------------------------------------
//
//  Copyright (C) 2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  eio.c
//
//  Implementation of the expand IO control for eboot.(MX6Q CPU ddr3 board)
//
//------------------------------------------------------------------------------

#include "bsp.h"

//------------------------------------------------------------------------------
// External Functions
extern BOOL OALI2cInit(BYTE * pbySelfAddr,BYTE * pbyClkDiv);
extern void OALI2cEnable( BOOL bChangeAddr, BOOL bChangeClkDiv, BYTE bySelfAddr, BYTE byClkDiv );
extern void OALI2cDisable();
extern WORD OALI2cCalculateClkRateDiv( DWORD dwFrequency );
extern BOOL OALI2cGenerateStart( BYTE devAddr, BOOL bWrite );
extern BOOL OALI2cRepeatedStart( BYTE devAddr, BOOL bWrite, BOOL bRSTACycleComplete );
extern void OALI2cSetReceiveMode( BOOL bReceive );
extern void OALI2cSetTxAck( BOOL bTxAck );

 
extern BOOL OALI2cPutData( BYTE *pData, WORD nBufLen, 
                   BOOL bStopAhead, 
                   BOOL bRepeatedStartCycleAhead, 
                   BOOL *pbStopped, 
                   BOOL *pbRSTACycleCompleted );
 
extern BOOL OALI2cGetData( BYTE *pData, WORD nBufLen, 
                   BOOL bStopAhead, 
                   BOOL bRepeatedStartCycleAhead, 
                   BOOL *pbStopped, 
                   BOOL *pbRSTACycleCompleted );
 
extern BOOL OALI2cWriteData( BYTE *pData, WORD nBufLen, 
                     BOOL bStopAhead, 
                     BOOL bRepeatedStartCycleAhead, 
                     BOOL *pbStopped, 
                     BOOL *pbRSTACycleCompleted );
 
extern BOOL OALI2cReadData( BYTE *pData, WORD nBufLen, 
                    BOOL bStopAhead, 
                    BOOL bRepeatedStartCycleAhead, 
                    BOOL *pbStopped, 
                    BOOL *pbRSTACycleCompleted );
 
extern void OALI2cGenerateStop();


//------------------------------------------------------------------------------
// External Variables


//------------------------------------------------------------------------------
// Defines
#define EIO1_I2C_ADDRESS 0x1b
#define EIO2_I2C_ADDRESS 0x1f

#define EIO_IN      0
#define EIO_OUT     1
#define EIO_POLAR   2
#define EIO_CONF    3
//------------------------------------------------------------------------------
// Types


//------------------------------------------------------------------------------
// Global Variables


//------------------------------------------------------------------------------
// Local Variables


//------------------------------------------------------------------------------
// Local Functions

//-----------------------------------------------------------------------------
//
// Function: OALI2CWriteOneByte
//
// This function writes a single byte byData to the register stated in byReg.
//
// Parameters:
//      byAddr
//          [in] I2C Slave device address.
//
//      byReg
//          [in] Register Index.
//
//      byData
//          [in] Data to write to byReg.
//
//      lpiResult
//          [in] Pointer of the result. The I2C Bus will store the
//            result of the operation in location pointed to by
//            lpiResult.
//
// Returns:
//      None.
//
//-----------------------------------------------------------------------------
VOID OALI2CWriteOneByte(BYTE byAddr, BYTE byReg, BYTE byData, LPINT lpiResult)
{
    BYTE byWriteData[2];
   
    *lpiResult=0;
    OALI2cEnable(FALSE, FALSE, 0, 0);
 
    if (!OALI2cGenerateStart(byAddr,TRUE))
    {
        OALMSGS( OAL_ERROR, (_T("OALI2CWriteOneByte: OALI2cGenerateStart Error!\r\n")));
        OALI2cDisable();
        return; 

    }
 
    byWriteData[0]=byReg;
    byWriteData[1]=byData;
    
    if (!OALI2cWriteData(byWriteData,sizeof(byWriteData),FALSE, FALSE, NULL, NULL))
    {
        OALMSGS( OAL_ERROR, (_T("OALI2CWriteOneByte: OALI2cWriteData Set Reg Error!\r\n")));
        OALI2cDisable();
        return; 

    }

    OALI2cGenerateStop();
    
    *lpiResult=1;
    
    return; 
}
 
//-----------------------------------------------------------------------------
//
// Function: OALI2CReadOneByte
//
// This function read a single byte data from the register stated in byReg.
//
// Parameters:
//      byAddr
//          [in] I2C Slave device address.
//
//      byReg
//          [in] Register Index.
//
//      lpiResult
//          [in] Pointer of the result. The I2C Bus will store the
//            result of the operation in location pointed to by
//            lpiResult.
//
// Returns:
//      The single byte content stored in byReg.
//
//-----------------------------------------------------------------------------
BYTE OALI2CReadOneByte(BYTE byAddr, BYTE byReg, LPINT lpiResult)
{
    BYTE byData=0;
    
    *lpiResult=0;
    OALI2cEnable(FALSE, FALSE, 0, 0);
    
    if (!OALI2cGenerateStart(byAddr,TRUE))
    {
        OALMSGS( OAL_ERROR, (_T("OALI2CReadOneByte: OALI2cGenerateStart Error!\r\n")));
        goto error;
    }
    
    if (!OALI2cWriteData(&byReg,sizeof(byReg),FALSE, FALSE, NULL, NULL))
    {
        OALMSGS( OAL_ERROR, (_T("OALI2CReadOneByte: OALI2cWriteData Set Reg Error!\r\n")));
        goto error;
    }
    
    if (!OALI2cRepeatedStart(byAddr,FALSE, FALSE))
    {
        OALMSGS( OAL_ERROR, (_T("OALI2CReadOneByte: OALI2cGenerateStart Error!\r\n")));
        goto error;
    }
    
    if (!OALI2cReadData(&byData,sizeof(byData), TRUE, FALSE, NULL, NULL))
    {
        OALMSGS( OAL_ERROR, (_T("OALI2CReadOneByte: OALI2cReadData Set byData Error!\r\n")));
        goto error;
    }
            
    OALI2cGenerateStop();
    *lpiResult=1;
 
 error:   
    OALI2cDisable();
    return byData;
}


//-----------------------------------------------------------------------------
//
//  Function: InitExpandIO
//
//  Initializes  MAX7310 i2c interface for expand IO configuration and set all EIO to straight way.
//
//  Parameters:
//
//  Returns:
//      Returns TRUE if successful, otherwise returns FALSE.
//
//-----------------------------------------------------------------------------
BOOL InitExpandIO()
{
    INT iResult;
    OALMSGS(OAL_INFO, (_T("InitEIO: Trying to init max7310 I2C Interface\r\n")));
  
    // Initialize the device internal fields
    if (!OALI2cInit(NULL, NULL))
    {
        OALMSGS( OAL_ERROR, (_T("InitEIO: InitEIO Error!\r\n")));
        return FALSE;
    }
    OALI2CWriteOneByte(EIO1_I2C_ADDRESS, EIO_POLAR, 0, &iResult) ; //Clear the polarity inversion
    OALI2CWriteOneByte(EIO2_I2C_ADDRESS, EIO_POLAR, 0, &iResult) ; //Clear the polarity inversion
     
     if(1==iResult)
       return TRUE;
     else
       return FALSE;

}

//-----------------------------------------------------------------------------
//
// Function: ReadExpandIO
//
// This function read the request pin value from the expander IO.
//
// Parameters:
//      IOName
//          [in] the IOName.
//
//      pData
//          [out] the data output.
//
// Returns:
//      TRUE if successful, otherwise false.
//
//-----------------------------------------------------------------------------

BOOL ReadExpandIO(UINT32 IOName, BOOL *pData)
{
    BYTE content,I2CAddr;
    INT iResult;
    if(IOName > EIO_MAX) return FALSE;
    
    if (IOName > EIO_GPS_RST_B)
    {
        I2CAddr = EIO2_I2C_ADDRESS;
        IOName -=8;
    }
    else
    {
        I2CAddr = EIO1_I2C_ADDRESS;
    }
    
    content = OALI2CReadOneByte(I2CAddr, EIO_OUT,  &iResult);
    *pData = content & (1 <<IOName);

    if(1==iResult)
      return TRUE;
    else
      return FALSE;
    
}

//-----------------------------------------------------------------------------
//
// Function: ConfigureExpandIO
//
// This function configure the expander IO pin to certain value.
//
// Parameters:
//      IOName
//          [in] the IOName.
//
//      pData
//          [in] the data for configuration.
//
// Returns:
//      TRUE if successful, otherwise false.
//
//-----------------------------------------------------------------------------
BOOL ConfigureExpandIO(UINT32 IOName, BOOL bData)
{
    BYTE content,I2CAddr;
    INT iResult;
    if(IOName > EIO_MAX) return FALSE;
    
    if (IOName > EIO_GPS_RST_B)
    {
        I2CAddr = EIO2_I2C_ADDRESS;
        IOName -=8;
    }
    else
    {
        I2CAddr = EIO1_I2C_ADDRESS;
    }
    
    content = OALI2CReadOneByte(I2CAddr, EIO_CONF,  &iResult); 
    content &= ~(1 << IOName);
    OALI2CWriteOneByte(I2CAddr, EIO_CONF, content, &iResult) ; //Set to output

    content = OALI2CReadOneByte(I2CAddr, EIO_OUT,  &iResult); 
    if(bData)
        content |= (1 << IOName);
    else
        content &= ~(1 << IOName);
    OALI2CWriteOneByte(I2CAddr, EIO_OUT, content, &iResult) ; //Set to level
    
    if(1==iResult)
      return TRUE;
    else
      return FALSE;
}
