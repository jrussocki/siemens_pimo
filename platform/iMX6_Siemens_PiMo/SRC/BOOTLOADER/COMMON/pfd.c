//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//------------------------------------------------------------------------------
//
//  Copyright (C) 2007-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------

#include <bsp.h>
#include "dvfs.h"




//------------------------------------------------------------------------------
// Local Variables


//------------------------------------------------------------------------------
// Functions

//------------------------------------------------------------------------------
//
//  Function:   pfd_init
//
//  This function initializes Phase Fractional Dividers (PFD) based on PLLs.
//
//  Parameters:
//      none
//
//  Returns:
//      TRUE if boot successfully, otherwise returns FALSE.
//
//------------------------------------------------------------------------------
BOOL pfd_init()
{
	PCSP_PLL_BASIC_REGS g_pPLL_PFD_480MHZ = NULL;
	PCSP_PLL_BASIC_REGS g_pPLL_PFD_528MHZ = NULL;

    g_pPLL_PFD_480MHZ= (PCSP_PLL_BASIC_REGS) OALPAtoUA(CSP_BASE_REG_PA_PLL_PFD_480MHZ);
    if (g_pPLL_PFD_480MHZ == NULL)
    {
        goto cleanUp;
    } 

    g_pPLL_PFD_528MHZ = (PCSP_PLL_BASIC_REGS) OALPAtoUA(CSP_BASE_REG_PA_PLL_PFD_528MHZ);
    if (g_pPLL_PFD_528MHZ == NULL)
    {
        goto cleanUp;
    }     

	// CCM_ANALOG_PFD_480
	
	// Force PLL3 FD0 to 720 Mhz
	// Disable the PLL
	SETREG32(&g_pPLL_PFD_480MHZ->CTRL, CSP_BITFVAL(PLL_PFD_CTRL_PFD0_CLKGATE, 0x1));
	// Confgigure PLL divider
	INSREG32(&g_pPLL_PFD_480MHZ->CTRL, CSP_BITFVAL(PLL_PFD_CTRL_PFD0_FRAC, 0x3F), CSP_BITFVAL(PLL_PFD_CTRL_PFD0_FRAC, 0x0C));
	// Enable PLL divider
	CLRREG32(&g_pPLL_PFD_480MHZ->CTRL, CSP_BITFVAL(PLL_PFD_CTRL_PFD0_CLKGATE, 0x1));

	// Force PLL3 FD1 to 540 Mhz
	// Disable the PLL
	SETREG32(&g_pPLL_PFD_480MHZ->CTRL, CSP_BITFVAL(PLL_PFD_CTRL_PFD1_CLKGATE, 0x1));
	// Confgigure PLL divider
	INSREG32(&g_pPLL_PFD_480MHZ->CTRL, CSP_BITFVAL(PLL_PFD_CTRL_PFD1_FRAC, 0x3F), CSP_BITFVAL(PLL_PFD_CTRL_PFD1_FRAC, 0x10));
	// Enable PLL divider
	CLRREG32(&g_pPLL_PFD_480MHZ->CTRL, CSP_BITFVAL(PLL_PFD_CTRL_PFD1_CLKGATE, 0x1));

	// Force PLL3 FD2 to 508 Mhz
	// Disable the PLL
	SETREG32(&g_pPLL_PFD_480MHZ->CTRL, CSP_BITFVAL(PLL_PFD_CTRL_PFD2_CLKGATE, 0x1));
	// Confgigure PLL divider
	INSREG32(&g_pPLL_PFD_480MHZ->CTRL, CSP_BITFVAL(PLL_PFD_CTRL_PFD2_FRAC, 0x3F), CSP_BITFVAL(PLL_PFD_CTRL_PFD2_FRAC, 0x11));
	// Enable PLL divider
	CLRREG32(&g_pPLL_PFD_480MHZ->CTRL, CSP_BITFVAL(PLL_PFD_CTRL_PFD2_CLKGATE, 0x1));


	// Force PLL3 FD3 to 454 Mhz
	// Disable the PLL
	SETREG32(&g_pPLL_PFD_480MHZ->CTRL, CSP_BITFVAL(PLL_PFD_CTRL_PFD3_CLKGATE, 0x1));
	// Confgigure PLL divider
	INSREG32(&g_pPLL_PFD_480MHZ->CTRL, CSP_BITFVAL(PLL_PFD_CTRL_PFD3_FRAC, 0x3F), CSP_BITFVAL(PLL_PFD_CTRL_PFD3_FRAC, 0x13));
	// Enable PLL divider
	CLRREG32(&g_pPLL_PFD_480MHZ->CTRL, CSP_BITFVAL(PLL_PFD_CTRL_PFD3_CLKGATE, 0x1));

	
	// CCM_ANALOG_PFD_528

	// Force PLL2 FD0 to 352 Mhz
	// Disable the PLL
	SETREG32(&g_pPLL_PFD_528MHZ->CTRL, CSP_BITFVAL(PLL_PFD_CTRL_PFD0_CLKGATE, 0x1));
	// Confgigure PLL divider
	INSREG32(&g_pPLL_PFD_528MHZ->CTRL, CSP_BITFVAL(PLL_PFD_CTRL_PFD0_FRAC, 0x3F), CSP_BITFVAL(PLL_PFD_CTRL_PFD0_FRAC, 0x1B));
	// Enable PLL divider
	CLRREG32(&g_pPLL_PFD_528MHZ->CTRL, CSP_BITFVAL(PLL_PFD_CTRL_PFD0_CLKGATE, 0x1));

	
	// Force PLL2 FD1 to 594 Mhz
	// Disable the PLL
	SETREG32(&g_pPLL_PFD_528MHZ->CTRL, CSP_BITFVAL(PLL_PFD_CTRL_PFD1_CLKGATE, 0x1));
	// Confgigure PLL divider
	INSREG32(&g_pPLL_PFD_528MHZ->CTRL, CSP_BITFVAL(PLL_PFD_CTRL_PFD1_FRAC, 0x3F), CSP_BITFVAL(PLL_PFD_CTRL_PFD1_FRAC, 0x10));
	// Enable PLL divider
	CLRREG32(&g_pPLL_PFD_528MHZ->CTRL, CSP_BITFVAL(PLL_PFD_CTRL_PFD1_CLKGATE, 0x1));

/*
	This PLL is connected tothe DDR controller, so system hangs when disabled :-(
	The initialisation shuold be performed in the IVT table instead
	// Force PLL2 FD2 to 396 Mhz
	// Disable the PLL
	SETREG32(&g_pPLL_PFD_528MHZ->CTRL, CSP_BITFVAL(PLL_PFD_CTRL_PFD2_CLKGATE, 0x1));
    OALMSG(1, (L"PFD2 - 1\r\n"));
	// Confgigure PLL divider
	INSREG32(&g_pPLL_PFD_528MHZ->CTRL, CSP_BITFVAL(PLL_PFD_CTRL_PFD2_FRAC, 0x3F), CSP_BITFVAL(PLL_PFD_CTRL_PFD2_FRAC, 0x18));
    OALMSG(1, (L"PFD2 - 2\r\n"));
	// Enable PLL divider
	CLRREG32(&g_pPLL_PFD_528MHZ->CTRL, CSP_BITFVAL(PLL_PFD_CTRL_PFD2_CLKGATE, 0x1));
    OALMSG(1, (L"PFD2 - 3\r\n"));*/


	// Force PLL2 FD3 to 594 Mhz -> only available on Duo/Quad
	// Disable the PLL
	SETREG32(&g_pPLL_PFD_528MHZ->CTRL, CSP_BITFVAL(PLL_PFD_CTRL_PFD3_CLKGATE, 0x1));
	// Confgigure PLL divider
	INSREG32(&g_pPLL_PFD_528MHZ->CTRL, CSP_BITFVAL(PLL_PFD_CTRL_PFD3_FRAC, 0x3F), CSP_BITFVAL(PLL_PFD_CTRL_PFD3_FRAC, 0x10));
	// Enable PLL divider
	CLRREG32(&g_pPLL_PFD_528MHZ->CTRL, CSP_BITFVAL(PLL_PFD_CTRL_PFD3_CLKGATE, 0x1));
	return TRUE;

cleanUp:

	return FALSE;
}
