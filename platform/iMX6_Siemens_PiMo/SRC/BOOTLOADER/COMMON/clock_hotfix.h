    {
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_AIPS_TZ1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_AIPS_TZ1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_AIPS_TZ2, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_APBHDMA_HCLK, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_ASRC_CLK, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_CAAM_SECURE_MEM, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_CAAM_WRAPPER_ACLK, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_CAAM_WRAPPER_IPG, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_CAN1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_CAN1_SERIAL, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_CAN2, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_CAN2_SERIAL, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_CHEETAH_DBG, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_DCIC1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_DCIC2, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_DTCP_DTCP, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_ECSPI1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_ECSPI2, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_ECSPI3, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_ECSPI4, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_ECSPI5, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_ENET, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_EPIT1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_EPIT2, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_ESAI, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R2, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_GPT, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_GPT_SERIAL, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_GPU2D, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_GPU3D, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R3, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R4, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_HDMI_TX_IAHBCLK, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R5, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_HDMI_TX_ISFR, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_I2C1_SERIAL, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_I2C2_SERIAL, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_I2C3_SERIAL, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IIM, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IOMUX_IPT, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPMUX1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPMUX2, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPMUX3, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPSYNC_IP2APB_TZASC2_IPG_MASTER, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPSYNC_IP2APB_TZASC2_IPG, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPSYNC_VDOA_IPG_MASTER, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R6, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R7, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU_DI0, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU_DI1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU2_IPU, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU2_IPU_DI0, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU2_IPU_DI1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_LDB_DI0, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_LDB_DI1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_MIPI_CORE_CFG, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_MLB, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_MMDC_CORE_ACLK_FAST_CORE_P0, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_MMDC_CORE_ACLK_FAST_CORE_P1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_MMDC_CORE_IPG_P0, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_MMDC_CORE_IPG_P1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_OCRAM, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_OPENVGAXI_ROOT, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_PCIE_ROOT, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_PERFMON1_APB, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_PERFMON2_APB, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_PERFMON3_APB, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_PL301_FAST1_S133, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R8, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_PL301_PER1_BCH, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_PL301_PER2_MAIN, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_PWM1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_PWM2, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_PWM3, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_PWM4, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_RAWNAND_U_BCH_INPUT_APB, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_RAWNAND_U_GPMI_BCH_INPUT_BCH, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_RAWNAND_U_GPMI_BCH_INPUT_GPMI_IO, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_RAWNAND_U_GPMI_INPUT_APB, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_ROM, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R9, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_SATA, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_SDMA, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R10, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R11, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_SPBA, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_SPDIF, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R12, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_SSI1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_SSI2, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_SSI3, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_UART, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_UART_SERIAL, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R13, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R14, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_USBOH3, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_USDHC1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_USDHC2, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_USDHC3, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_USDHC4, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_EMI_SLOW, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_VDOAXI, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_VPU, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R15, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R16, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R17, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R18, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R19, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R20, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R21, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R22, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R23, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R24, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R25, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R26, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R27, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R28, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R29, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R30, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R31, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R32, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R33, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R34, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R35, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R36, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R37, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_R38, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
    }
