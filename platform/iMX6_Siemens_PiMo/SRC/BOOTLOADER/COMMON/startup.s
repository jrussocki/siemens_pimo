;
; Copyright (c) Microsoft Corporation.  All rights reserved.
;
;
; Use of this source code is subject to the terms of the Microsoft end-user
; license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
; If you did not accept the terms of the EULA, you are not authorized to use
; this source code. For a copy of the EULA, please see the LICENSE.RTF on your
; install media.
;
;------------------------------------------------------------------------------
;
;  Copyright (C) 2007-2011, Freescale Semiconductor, Inc. All Rights Reserved.
;  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
;  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
;
;------------------------------------------------------------------------------
;
;  File:  startup.s
;
;  Defines configuration parameters used to create the NK and Bootloader
;  program images.
;
;------------------------------------------------------------------------------

    INCLUDE ..\\..\\oal\\oallib\\startup.s

    INCLUDE image_cfg.inc

    IMPORT  main
    IMPORT  OALVAtoPA
    IMPORT  OALPAtoVA
    IMPORT  AuxMain

;-------------------------------------------------------------------------------
;
; KernelStart: OEM bootloader startup code.  This routine will:
;
; * Copy the image to RAM if it's not already running there.
;
; * Set up the MMU and Dcache for the bootloader.
;
; * Initialize the first-level page table based up the contents
;   of the MemoryMap array and enable the MMU and caches.
;
; Inputs: None.
; 
; On return: N/A.
;
; Register used:
;
;-------------------------------------------------------------------------------
;
    STARTUPTEXT
    ALIGN 4
    NESTED_ENTRY KernelStart, |.astart|

    PROLOG_END KernelStart

    ; Copy the bootloader image from flash to RAM.  The image is configured
    ; to run in RAM, but is stored in flash.  Absolute address references
    ; should be avoided until the image has been relocated and the MMU enabled.
    ;
    ; NOTE: The destination (RAM) address must match the address in the
    ; bootloader's .bib file.  The latter dictates the code fix-up addresses.
    ;

    ; Check if we are running from NOR flash
    mov     r0, pc
    ldr     r1, =IMAGE_BOOT_NORDEV_NOR_PA_START
    cmp     r0, r1
    blt     CODEINRAM

    ldr     r1, =IMAGE_BOOT_NORDEV_NOR_PA_END
    cmp     r0, r1
    bgt     CODEINRAM
    
    ldr     r8, =IMAGE_BOOT_BOOTIMAGE_NOR_PA_START
    ldr     r1, =IMAGE_BOOT_BOOTIMAGE_RAM_PA_START
    ldr     r2, =(IMAGE_BOOT_BOOTIMAGE_RAM_SIZE / 16) ; Bootloader image length (this must be <= the NK
                                                      ; length in the .bib file).  We are block-copying 
                                                      ; 16-bytes per iteration.

    ; Do 4x32-bit block copies from flash->RAM (corrupts r4-r7).
    ;
10  ldmia   r8!, {r4-r7}        ; Loads from flash (post increment).
    stmia   r1!, {r4-r7}        ; Stores to RAM (post increment).
    subs    r2, r2, #1          ;
    bne     %B10                ; Done?


    ; Now that we've copied ourselves to RAM, jump to the RAM image.  Use the "CodeInRAM" label
    ; to determine the RAM-based code address to which we should jump.
    ;
    ldr     r1, =IMAGE_BOOT_BOOTIMAGE_NOR_PA_START
    adr     r2, CODEINRAM
    sub     r2, r2, r1
    ldr     r0, =IMAGE_BOOT_BOOTIMAGE_RAM_PA_START
    add     r2, r0, r2
    mov     pc, r2
    nop
    nop
    nop

CODEINRAM
    ; Now that we're running out of RAM, construct the first-level Section descriptors
    ; to create 1MB mapped regions from the addresses defined in the OEMAddressTable.
    ; This will allow us to enable the MMU and use a virtual address space that matches
    ; the mapping used by the OS image.
    ;
    ; We'll create two different mappings from the addresses specified:
    ;     [8000 0000 --> 9FFF FFFF] = Cacheable, Bufferable
    ;     [A000 0000 --> BFFF FFFF] = NonCacheable, nonBufferable
    ;

BUILDTTB

    ADR R11, g_oalAddressTable ; Pointer to OEMAddressTable.
    
    ; Set the TTB.
    ;
    ldr     r9, =IMAGE_BOOT_BOOTPT_RAM_PA_START
    ldr     r0, =0xFFFFC000                   ;
    and     r9, r9, r0                        ; Mask off TTB to be on 16K boundary.
    mcr     p15, 0, r9, c2, c0, 0             ; Set the TTB.
    mov     r10, r9                           ; Save TTB address

    ; Zero TTB (if not done: unpredictable behaviour of MMU)
    ;
    stmfd   sp!, {r0-r7}
    mov     r0, r9
    mov     r1, #0x4000
    mov     r2, #0
    mov     r3, r2
    mov     ip, r2
    mov     lr, r2
Zero64Byte
    subs    r1, r1, #64
    STMGEM	r0, r2, r3, ip, lr
    STMGEM	r0, r2, r3, ip, lr
    STMGEM	r0, r2, r3, ip, lr
    STMGEM	r0, r2, r3, ip, lr
    bgt     Zero64Byte
    ldmfd   sp!, {r0-r3}


    ; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ; ~~~~~~~~~~ MAP CACHED and BUFFERED SECTION DESCRIPTORS ~~~~~~~~~~~~~~~~~~
    ; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    mov     r0, #0x0A                         ; Section (1MB) descriptor; (C=1 B=0: cached write-through).
    orr     r0, r0, #0x400                    ; Set AP.

20  mov     r1, r11                           ; Pointer to OEMAddressTable.

    ; Start Crunching through the OEMAddressTable[]:
    ;
    ; r2 temporarily holds OEMAddressTable[VA]
    ; r3 temporarily holds OEMAddressTable[PHY]
    ; r4 temporarily holds OEMAddressTable[#MB]
    ;
25  ldr     r2, [r1], #4                       ; Virtual (cached) address to map physical address to.
    ldr     r3, [r1], #4                       ; Physical address to map from.
    ldr     r4, [r1], #4                       ; Number of MB to map.

    cmp     r4, #0                             ; End of table?
    beq     %F29

    ; r2 holds the descriptor address (virtual address)
    ; r0 holds the actual section descriptor
    ;

    ; Create descriptor address.
    ;
    ldr     r6, =0xFFF00000
    and     r2, r2, r6             ; Only VA[31:20] are valid.
    orr     r2, r9, r2, LSR #18    ; Build the descriptor address:  r2 = (TTB[31:14} | VA[31:20] >> 18)

    ; Create the descriptor.
    ;
    ldr     r6, =0xFFF00000
    and     r3, r3, r6             ; Only PA[31:20] are valid for the descriptor and the rest will be static.
    orr     r0, r3, r0             ; Build the descriptor: r0 = (PA[31:20] | the rest of the descriptor)

    ; Store the descriptor at the proper (physical) address
    ;
28  str     r0, [r2], #4
    add     r0, r0, #0x00100000    ; Section descriptor for the next 1MB mapping (just add 1MB).
    sub     r4, r4, #1             ; Decrement number of MB left.
    cmp     r4, #0                 ; Done?
    bne     %B28                   ; No - map next MB.

    bic     r0, r0, #0xF0000000    ; Clear section base address field.
    bic     r0, r0, #0x0FF00000    ; Clear section base address field.
    b       %B25                   ; Get and process the next OEMAddressTable element.

    ; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ; ~~~~~~~~~~ MAP UNCACHED and UNBUFFERED SECTION DESCRIPTORS ~~~~~~~~~~~~~~
    ; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

29  tst     r0, #8                 ; Test for 'C' bit set (means we just used 
                                   ; above loop structure to map cached and buffered space).
    bic     r0, r0, #0x0C          ; Clear cached and buffered bits in the descriptor (clear C&B bits).
    add     r9, r9, #0x0800        ; Pointer to the first PTE for "unmapped uncached space" (0x2000 0000 + V_U_Adx).
    bne     %B20                   ; Repeat the descriptor setup for uncached space (map C=B=0 space).

    ; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ; ~~~~~~~~~~ MAP Execption Vector Table ADDRESS TO 0x00000000 for place exception vector  ~~~~~~~~~~~~~~
    ; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    ; Create the descriptor.
    ldr     r1, =IMAGE_SHARE_EVT_RAM_PA_START
    orr     r0, r1, #(2 << 0)       ; L1D[1:0) = 2 = 1MB Section
    orr     r0, r0, #(1 << 10)      ; L1D[11:10] = 1 = AP is R/W privileged;

    str     r0,[r10]

    ; Create descriptor for 1M identity mapping to allow MMU enable transition.
    ; NOTE:  code assume EBOOT does not span 1M boundary from current execution point.
    MOV     R1, PC
    AND     R1, R1, R6

    orr     r0, r1, #(2 << 0)       ; L1D[1:0) = 2 = 1MB Section
    orr     r0, r0, #(1 << 10)      ; L1D[11:10] = 1 = AP is R/W privileged

    orr     r2, r10, r1, LSR #18    ; &L1D = TTB + (VA[31:20] >> 18)
    ldr     r3, [r2]                ; Save existing mapping
    str     r0, [r2]                ; Create identity mapping

ACTIVATEMMU
    ; The 1st Level Section Descriptors are setup. Initialize the MMU and turn it on.
    ;
    mov     r1, #1
    mcr     p15, 0, r1, c3, c0, 0   ; Set up access to domain 0.

INVALIDATE_ALL_CACHES
    stmfd   sp!, {r0-r12}

    ; the following loops are taken out of
    ; ARM Architecture Reference Manual
    ; ARMv7-A and ARMv7-R edition
    MRC p15, 1, R0, c0, c0, 1  ; Read CLIDR into R0
    ANDS R3, R0, #0x07000000
    MOV R3, R3, LSR #23        ; Cache level value (naturally aligned)
    BEQ FINISHED
    MOV R10, #0
LOOP1
    ADD R2, R10, R10, LSR #1   ; Work out 3 x cachelevel
    MOV R1, R0, LSR R2         ; bottom 3 bits are the Cache type for this level
    AND R1, R1, #7             ; get those 3 bits alone
    CMP R1, #2
    BLT SKIP                   ; no cache or only instruction cache at this level
    MCR p15, 2, R10, c0, c0, 0 ; write CSSELR from R10
    ISB                        ; ISB to sync the change to the CCSIDR
    MRC p15, 1, R1, c0, c0, 0  ; read current CCSIDR to R1
    AND R2, R1, #7             ; extract the line length field
    ADD R2, R2, #4             ; add 4 for the line length offset (log2 16 bytes)
    LDR R4, =0x3FF
    ANDS R4, R4, R1, LSR #3    ; R4 is the max number on the way size (right aligned)
    CLZ R5, R4                 ; R5 is the bit position of the way size increment
    MOV R9, R4                 ; R9 working copy of the max way size (right aligned)
LOOP2
    LDR R7, =0x00007FFF
    ANDS R7, R7, R1, LSR #13   ; R7 is the max number of the index size (right aligned)
LOOP3
    ORRLSL R11, R10, R9, R5, R12   ; factor in the way number and cache number into R11
    ORRLSL R11, R11, R7, R2, R12   ; factor in the index number
    MCR p15, 0, R11, c7, c6, 2 ; DCISW, invalidate by set/way
    SUBS R7, R7, #1            ; decrement the index
    BGE LOOP3
    SUBS R9, R9, #1            ; decrement the way number
    BGE LOOP2
SKIP
    ADD R10, R10, #2           ; increment the cache number
    CMP R3, R10
    BGT LOOP1
    DSB
FINISHED
    ldmfd   sp!, {r0-r12}

    mcr p15, 0, r0, c7, c5, 0   ; invalidate all instruction caches
    mcr p15, 0, r0, c7, c5, 6   ; invalidate all branch predictors
    mcr p15, 0, r0, c8, c7, 0   ; invalidate all TLBs

    mov     r1, #1
    mcr     p15, 0, r1, c7, c10, 4  ; Drain the write and fill buffers.

    mrc     p15, 0, r1, c1, c0, 0   ; Read control register

    orr     R1, R1, #0x1            ; Enable MMU

    orr     R1, R1, #0x1000         ; Enable IC
    orr     R1, R1, #0x4            ; Enable DC

    ORR     R1, R1, #(1 :SHL: 22)   ; Enable unaligned access support (U bit)
    BIC     R1, R1, #(1 :SHL: 1)    ; Disable alignment faults (A bit)

    ldr     r4, =VirtualStart       ; Get virtual address of 'VirtualStart' label.

    SETTHUMBBIT R4                  ; Set the LDB to keep the branch in Thumb mode

    cmp     r4, #0                  ; Make sure no stall on "mov pc,r4" below.
    mcr     p15, 0, r1, c1, c0, 0   ; Write control register (turn on mmu and cache)

    BX      R4                      ; Jump to virtual address

    nop

    ; We are now running virtual address space.
VirtualStart
    str     r3, [r2]                ; Restore mapping overwritten by identity
                                    ; mapping
    mcr     p15, 0, r2, c8, c7, 0   ; Flush the I&D TLBs

    ;Setup irq stack
    mrs     r0, cpsr                            ; r0 = CPSR
    mov     r0, #ARM_CPSR_MODE_IRQ              ; enter irq mode
    orr     r0, r0, #ARM_CPSR_IRQDISABLE        ; disable normal IRQ
    orr     r0, r0, #ARM_CPSR_FIQDISABLE        ; disable fast IRQ
    msr     cpsr_c, r0                         ; update CPSR control bits
    LDR     R0, =(IMAGE_BOOT_STACK_RAM_CA_START-228*1024) ;reserve the last 4k for isr stack.
    MOV     SP, R0

    ; Set up EBOOT stack
    mrs     r0, cpsr                            ; r0 = CPSR
    mov     r0, #ARM_CPSR_MODE_SVC              ; enter supervisor mode
    orr     r0, r0, #ARM_CPSR_IRQDISABLE        ; disable normal IRQ
    orr     r0, r0, #ARM_CPSR_FIQDISABLE        ; disable fast IRQ
    orr     r0, r0, #ARM_CPSR_PRECISE           ; enable precise data aborts
    msr     cpsr_xc, r0                         ; update CPSR control bits
    LDR     R0, =(IMAGE_BOOT_STACK_RAM_CA_START)
    MOV     SP, R0

    ; Set up exception vector
    mov     r8, #0                           ; (r8) = ptr to exception vectors
    ADR     R7, VectorInstructions

    ldmia   r7!, {r0-r3}                    ; load 4 instructions
    stmia   r8!, {r0-r3}                    ; store the 4 vector instructions
    ldmia   r7!, {r0-r3}                    ; load 4 instructions
    stmia   r8!, {r0-r3}                    ; store the 4 vector instructions


    ldr     r7, =VectorTable                ; (r0) = VA of VectorTable
    add     r8, r8, #0x3E0-(8*4)            ; (r8) = target location of the vector table
    ldmia   r7!, {r0-r3}
    stmia   r8!, {r0-r3}
    ldmia   r7!, {r0-r3}
    stmia   r8!, {r0-r3}

    ;Enable Interrupt
    mrs     r0, cpsr                            ; r0 = CPSR
    mov     r0, #ARM_CPSR_MODE_SVC              ; enter supervisor mode
    bic     r0, r0, #ARM_CPSR_IRQDISABLE        ; enable fast IRQ
    msr     cpsr_c, r0                         ; update CPSR control bits


    ; Jump to the C entrypoint.
    ;    
    bl      main                              ; Jump to main.c::main(), never to return...
    nop
    nop
    nop

STALL2
    b      STALL2

    EPILOG_ENTRY KernelStart

    NESTED_END KernelStart

;-------------------------------------------------------------------------------
;
; void Launch(UINT32 pFunc): This function launches the program at pFunc (pFunc
;                            is a physical address).  The MMU is disabled just
;                            before jumping to specified address.
;
; Inputs: pFunc (r0) - Physical address of program to Launch.
; 
; On return: None - the launched program never returns.
;
; Register used:
;
;-------------------------------------------------------------------------------
;

    ARM

    STARTUPTEXT
    ALIGN 4
    NESTED_ENTRY Launch, |.astart|

    PROLOG_END Launch

    ARM2THUMB R4

    THUMB

    mov     r4, r0                  ; Save jump address

    mrc     p15, 0, r0, c2, c0, 0   ; Get the TTB into r0
    ldr     r1, =0xFFFFC000
    and     r0, r0, r1              ; Mask off TTB to be on 16K boundary.
    mov     r1, #0                  ; Cached parameter of OALPAtoVA = FALSE
    bl      OALPAtoVA               ; Translate TTB PA into UA
    mov     r5, r0                  ; Save TTB virtual address in r5
    
    ; Create descriptor for 1M identity mapping to allow MMU disable transition.
    ; NOTE:  code assume EBOOT does not span 1M boundary from current execution point.
    mov     r0, pc
    bl      OALVAtoPA               ; Translate PC VA into PA
    ldr     r1, =0xFFF00000
    and     r1, r0, r1
    orr     r0, r1, #(2 << 0)       ; L1D[1:0) = 2 = 1MB Section
    orr     r0, r0, #(1 << 10)      ; L1D[11:10] = 1 = AP is R/W privileged

    orr     r2, r5, r1, LSR #18     ; &L1D = TTB + (VA[31:20] >> 18)
    str     r0, [r2]                ; Create identity mapping
    mcr     p15, 0, r2, c8, c7, 0   ; Flush the I&D TLBs
    mcr     p15, 0, r1, c7, c10, 4  ; Drain the write and fill buffers.
    
    ldr     r0, =VirtualEnd
    bl      OALVAtoPA               ; r0 contains physical address of code
    mov     pc, r0                  ; Jump to physical before MMU disable
   
VirtualEnd

    ; Next, we disable the MMU, and I&D caches.
    ;
    mrc     p15, 0, r1, c1, c0, 0   ; Read control register
    bic     r1, r1, #0x1            ; Disable MMU.
    bic     r1, r1, #0x1000         ; Disable IC.
    bic     r1, r1, #0x4            ; Disable DC.
    mcr     p15, 0, r1, c1, c0, 0   ; MMU OFF:  All memory accesses are now physical.

    ; Flush the I&D TLBs.
    ;
    mcr     p15, 0, r2, c8, c7, 0   ; Flush the I&D TLBs

    ; Jump to the physical launch address.  This should never return...
    ;    
    BX R4

    nop
    nop
    nop
    nop
    nop
    nop

    EPILOG_ENTRY Launch

    NESTED_END Launch

;-------------------------------------------------------------------------------
;
; void IRQHandler(void): This function is the irq entry, it will jump to the real irq service function.
;
; Inputs: None.
; 
; On return: None - interrupt return.
;
; Register used:
;
;-------------------------------------------------------------------------------
;

    ARM

    STARTUPTEXT
    ALIGN 4
    NESTED_ENTRY IRQHandler, |.astart|

    SUB LR, LR, #4

    PUSHSTACK2LR R0, R12

    PROLOG_END IRQHandler

    ARM2THUMB R0

    THUMB

    MOV R0, LR

    BL AuxMain

    EPILOG_ENTRY IRQHandler

    POPSTACK2LR R0, R12

    ERET

    NESTED_END IRQHandler

    ARM

    STARTUPDATA
    ALIGN 4
VectorInstructions
    DCD 0xE59FF3D8 ; LDR PC, [PC, #0x3D8] (ARM encoding) ; reset
    DCD 0xE59FF3D8 ; LDR PC, [PC, #0x3D8] (ARM encoding) ; undefined instruction
    DCD 0xE59FF3D8 ; LDR PC, [PC, #0x3D8] (ARM encoding) ; SVC
    DCD 0xE59FF3D8 ; LDR PC, [PC, #0x3D8] (ARM encoding) ; Prefetch abort
    DCD 0xE59FF3D8 ; LDR PC, [PC, #0x3D8] (ARM encoding) ; data abort
    DCD 0xE59FF3D8 ; LDR PC, [PC, #0x3D8] (ARM encoding) ; unused vector location
    DCD 0xE59FF3D8 ; LDR PC, [PC, #0x3D8] (ARM encoding) ; IRQ
    DCD 0xE59FF3D8 ; LDR PC, [PC, #0x3D8] (ARM encoding) ; FIQ

    STARTUPDATA
    ALIGN 4
VectorTable
    DCD 0xFFFFFFFF ; reset
    DCD 0xFFFFFFFF ; undefined instruction
    DCD 0xFFFFFFFF ; SVC
    DCD 0xFFFFFFFF ; Prefetch abort
    DCD 0xFFFFFFFF ; data abort
    DCD 0xFFFFFFFF ; unused vector
    DCD IRQHandler ; IRQ
    DCD 0xFFFFFFFF ; FIQ

    END
