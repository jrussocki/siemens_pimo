//-----------------------------------------------------------------------------
//
//  Use of this source code is subject to the terms of the Microsoft end-user
//  license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
//  If you did not accept the terms of the EULA, you are not authorized to use
//  this source code. For a copy of the EULA, please see the LICENSE.RTF on
//  your install media.
//
//-----------------------------------------------------------------------------
//
//  Copyright (C) 2007-2010, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//-----------------------------------------------------------------------------
//
//  File:  nor.c
//
//  Contains BOOT NOR flash support functions.
//
//-----------------------------------------------------------------------------
#include "bsp.h"
#include "loader.h"
#pragma warning(push)
#pragma warning(disable: 4115)
#include <fmd.h>
#pragma warning(pop)

//-----------------------------------------------------------------------------
// External Functions


//-----------------------------------------------------------------------------
// External Variables


//-----------------------------------------------------------------------------
// Defines


//-----------------------------------------------------------------------------
// Types


//-----------------------------------------------------------------------------
// Global Variables


//-----------------------------------------------------------------------------
// Local Variables


//-----------------------------------------------------------------------------
// Local Functions


//-----------------------------------------------------------------------------
//
//  Function:  NORWrite
//
//  This function writes to NOR flash memory the OS image stored 
//  in the RAM file cache area.
//
//  Parameters:
//      dwStartAddr 
//          [in] Address in flash memory where the start of the downloaded 
//          OS image is to be written.
//
//      dwLength 
//          [in] Length of the OS image, in bytes, to be written to flash
//          memory.            
//
//  Returns:
//      TRUE indicates success. FALSE indicates failure.
//-----------------------------------------------------------------------------
BOOL NORWrite(DWORD dwStartAddr, DWORD dwLength)
{
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(dwStartAddr);
    UNREFERENCED_PARAMETER(dwLength);
#if 0
    BOOL rc = FALSE;
    int nBlocks, i;
    OAL_FLASH_INFO info;
    DWORD BlockSize, dwOffset;
    UINT32 percentComplete, lastPercentComplete;
    VOID *pFlashBase = (VOID *) OALPAtoUA(IMAGE_BOOT_NORDEV_NOR_PA_START);
    VOID *pBuffer = (VOID *) OEMMapMemAddr(dwStartAddr, dwStartAddr);

    // Read NOR CFI info
    if (!OALFlashInfo(pFlashBase, &info)) {
        KITLOutputDebugString("ERROR: OALFlashErase failed get flash memory info\r\n");
        goto cleanUp;
    }

    // Find the largest block size on the device.  We will use
    // this later for erase/write operations so that we can show
    // progress.
    BlockSize = 0;
    for (i=0; i<(int)info.regions; i++)
    {
        if (info.aBlockSize[i] * info.parallel > BlockSize)
            BlockSize = info.aBlockSize[i] * info.parallel;
    }
    if (BlockSize == 0)
    {
        goto cleanUp;
    }

    // Determine how many blocks to write
    // Note: This assumes we always start at the beginning of a block
    nBlocks = (dwLength + BlockSize - 1) / BlockSize;

    // Fill unused space with 0xFF
    memset((VOID *) ((DWORD) pBuffer + dwLength), 0xFF, (nBlocks * BlockSize) - dwLength);

    KITLOutputDebugString("INFO:  Programming image into NOR flash.\r\n");

    // Keep track of image offset (incremented by block size for each programmed block)
    dwOffset = 0;

    lastPercentComplete = 0;

    // Program (erase then write) the flash blocks
    for (i=0; i<nBlocks; i++)
    {
        if (!OALFlashErase(pFlashBase, (VOID *) (dwStartAddr+dwOffset), BlockSize))
        {
            KITLOutputDebugString("ERROR:  OEMWriteErase failed!\r\n");
            goto cleanUp;
        }

        if (!OALFlashWrite(pFlashBase, (VOID *) (dwStartAddr+dwOffset), BlockSize, (VOID *) ((DWORD) pBuffer + dwOffset)))
        {
            KITLOutputDebugString("ERROR:  OALFlashWrite failed!\r\n");
            goto cleanUp;
        }        

        percentComplete = 100 * (i+1) / (nBlocks);

        // If percentage complete has changed, show the progress
        if (lastPercentComplete != percentComplete)
        {
            lastPercentComplete = percentComplete;
            OEMWriteDebugByte('\r');
            KITLOutputDebugString("INFO: Program is %d%% complete.", percentComplete);
        }

        // Go to next block
        dwOffset += BlockSize;
    }

    rc = TRUE;

cleanUp:
    KITLOutputDebugString("\r\nINFO:  Flashing sequence complete.\r\n");
    return rc;
#else
    return TRUE;
#endif
}


//------------------------------------------------------------------------------
//
//  Function:  NORLoadBootCFG
//
//  Retrieves bootloader configuration information (menu settings, etc.) from 
//  the NOR flash.
//
//  Parameters:
//      eBootCFG 
//          [out] Points to bootloader configuration that will be filled with
//          loaded data. 
//
//      cbBootCfgSize
//          [in] Size in bytes of the bootloader configuration.
//
//  Returns:
//      TRUE indicates success. FALSE indicates failure.
//
//-----------------------------------------------------------------------------
BOOL NORLoadBootCFG(BYTE *pBootCfg, DWORD cbBootCfgSize)
{
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(pBootCfg);
    UNREFERENCED_PARAMETER(cbBootCfgSize);
#if 0
    VOID *pStart = (VOID *) OALPAtoUA(IMAGE_BOOT_BOOTCFG_NOR_PA_START);

    KITLOutputDebugString("INFO:  Reading boot configuaration in NOR flash (addr = 0x%x, size = 0x%x)\r\n", 
        pStart, cbBootCfgSize);

    if (!pBootCfg) return FALSE;

    memcpy((VOID *) pBootCfg, pStart, cbBootCfgSize);
#endif

    return TRUE;
}


//------------------------------------------------------------------------------
//
//  Function:  NORStoreBootCFG
//
//  Stores bootloader configuration information (menu settings, etc.) to 
//  the NOR flash.
//
//  Parameters:
//      eBootCFG 
//          [out] Points to bootloader configuration that will be stored.
//
//      cbBootCfgSize
//          [in] Size in bytes of the bootloader configuration.
//
//  Returns:
//      TRUE indicates success. FALSE indicates failure.
//
//-----------------------------------------------------------------------------
BOOL NORStoreBootCFG(BYTE *pBootCfg, DWORD cbBootCfgSize)
{
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(pBootCfg);
    UNREFERENCED_PARAMETER(cbBootCfgSize);
#if 0
    BOOL rc = FALSE;
    VOID *pFlashBase = (VOID *) OALPAtoUA(IMAGE_BOOT_NORDEV_NOR_PA_START);
    VOID *pStart = (VOID *) OALPAtoUA(IMAGE_BOOT_BOOTCFG_NOR_PA_START);

    KITLOutputDebugString("INFO:  Storing boot configuaration in NOR flash (addr = 0x%x, size = 0x%x)\r\n", 
        pStart, cbBootCfgSize);

    if (!OALFlashErase(pFlashBase, pStart, cbBootCfgSize))
    {
        KITLOutputDebugString("ERROR:  OEMWriteErase failed!\r\n");
        goto cleanUp;
    }

    if (!OALFlashWrite(pFlashBase, pStart, cbBootCfgSize, (VOID *) pBootCfg))
    {
        KITLOutputDebugString("ERROR:  OALFlashWrite failed!\r\n");
        goto cleanUp;
    }        

    KITLOutputDebugString("INFO:  Boot configuration successfully stored.\r\n");

    rc = TRUE;

cleanUp:
    return  rc;

#else
    return TRUE;
#endif
}

