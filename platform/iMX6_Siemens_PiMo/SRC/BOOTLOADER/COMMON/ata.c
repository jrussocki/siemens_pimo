//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//------------------------------------------------------------------------------
//
//  Copyright (C) 2007-2011 Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
#include "bsp.h"
#include "atafmd.h"

//-----------------------------------------------------------------------------
// External Functions
extern VOID OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX index, DDK_CLOCK_GATE_MODE mode);


//-----------------------------------------------------------------------------
// External Variables

extern DWORD g_dwBoardID;

//------------------------------------------------------------------------------
// Defines
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//
// Function: BSP_GetATAImageInfo
//
//    Get the image parameters
//
// Parameters:
//        pATAImageCfg[out] - image parameters
//
// Returns:
//        None
//
//------------------------------------------------------------------------------
void BSP_GetATAImageCfg(PATA_IMAGE_CFG pATAImageCfg)
{
    //In MX6Q ata boot, there is no xldr, we use eboot parameter to get correct offset
    pATAImageCfg->dwXldrOffset = IMAGE_BOOT_BOOTIMAGE_ATA_OFFSET;
    pATAImageCfg->dwXldrSize = IMAGE_BOOT_BOOTIMAGE_ATA_SIZE;

    pATAImageCfg->dwBootOffset = IMAGE_BOOT_BOOTIMAGE_ATA_OFFSET;
    pATAImageCfg->dwBootSize = IMAGE_BOOT_BOOTIMAGE_ATA_SIZE;

    pATAImageCfg->dwNkOffset = IMAGE_BOOT_NKIMAGE_ATA_OFFSET;
    pATAImageCfg->dwNkSize = IMAGE_BOOT_NKIMAGE_ATA_SIZE;

    pATAImageCfg->dwBoot2Offset = IMAGE_BOOT_BOOTIMAGE2_ATA_OFFSET;
    pATAImageCfg->dwBoot2Size = IMAGE_BOOT_BOOTIMAGE2_ATA_SIZE;
    
    pATAImageCfg->dwCfgOffset = IMAGE_BOOT_BOOTCFG_ATA_OFFSET;
    pATAImageCfg->dwCfgSize = IMAGE_BOOT_BOOTCFG_ATA_SIZE;

    pATAImageCfg->dwNkRAMOffset = IMAGE_BOOT_NKIMAGE_RAM_PA_START;
    pATAImageCfg->dwATASize = IMAGE_BOOT_ATADEV_ATA_SIZE;

    pATAImageCfg->dwSocId = 53;
}

ULONG BSP_GetATARAMBase()
{
    return IMAGE_SHARE_ATA_RAM_PA_START;
}

#if 0
void EnablePhySataPll()
{
    PCSP_IOMUX_REGS pIOMUX = OALPAtoUA(CSP_BASE_REG_PA_IOMUXC);

    UINT32 reg = INREG32(&pIOMUX->GPR[13]);

	// PHY configuration
	// rx_eq_val_0(iomuxc_gpr13[26:24])
	// los_lvl(iomuxc_gpr13[23:19])
	// rx_dpll_mode_0(iomuxc_gpr13[18:16])
	// sata_speed(iomuxc_gpr13[15])
	// mpll_ss_en(iomuxc_gpr13[14])
	// tx_atten_0(iomuxc_gpr13[13:11])
	// tx_boost_0(iomuxc_gpr13[10:7])
	// tx_lvl(iomuxc_gpr13[6:2])
	// mpll_ck_off(iomuxc_gpr13[1])
	// tx_edgerate_0(iomuxc_gpr13[0]), 

	reg &= ~0x7FFFFFFD;
	reg |=  0x0593A044;
	OUTREG32(&pIOMUX->GPR[13], reg);

	// Enable PLL
	reg = INREG32(&pIOMUX->GPR[13]);
	reg |= 0x02;
	OUTREG32(&pIOMUX->GPR[13], reg);
}

void EnableEnetSataPll()
{
	DWORD timeout;
	// Enable SATA PLL
	PCSP_PLL_BASIC_REGS pPLL_ENET = OALPAtoUA(CSP_BASE_REG_PA_PLL_ENET);
    volatile UINT32 value=INREG32(&pPLL_ENET->CTRL);
    value&=(~(0x3<<14)); // Select the 24MHz oscillator as source
    value|=(0x1<<20)|(0x1<<13)|(0x0<<14); // ENABLE_SATA/ENABLE/Select the 24MHz oscillator as source
    value&=(~((0x1<<16)|(0x1<<12))); // BYPASS/POWERDOWN
	OUTREG32(&pPLL_ENET->CTRL, value); // enable sata 100MHZ // 0x102001
    value=INREG32(&pPLL_ENET->CTRL);
    timeout=1000;
    while((value&(0x1<<31))==0) // must wait pll lock, otherwise timeout
    {
        timeout--;      
        if(timeout==0) {
			OALMSG(1, (TEXT("ATA pll timeout 0x%x !!\r\n"), timeout)); 
			break;
		}
        OALStall(1000);
        value=INREG32(&pPLL_ENET->CTRL);
    }
	if (timeout != 0) {
		RETAILMSG(1, (L"%S: ATA pll lock OK\r\n", __FUNCTION__));
	}
	else {
		RETAILMSG(1, (L"%S: ATA pll lock failed !!\r\n", __FUNCTION__));
	}
}

//------------------------------------------------------------------------------
//
// Function: BSPBoard
//
// This function init board setting // clk // pin // power
//
// Parameters:
//        none
//
// Returns:
//        none
//
//------------------------------------------------------------------------------

void BSPBoard(UINT32* pValue)
{
	// Enable SATA PLL (100MHz)
    OALClockSetGatingMode (DDK_CLOCK_GATE_INDEX_SATA, DDK_CLOCK_GATE_MODE_ENABLED_ALL);

	// Enable SATA PHY PLL from GPR13
	EnablePhySataPll();

	// Enable SATA PLL from ENET PLL
	EnableEnetSataPll();

	// SATA PHY was configured in EnablePhySataPll, don't use this
	*pValue = 0;
}
#endif

//------------------------------------------------------------------------------
//
//       Function:  ConfigureSataPhy
//
//             This function configures the SATA PHY by writting the GPR13 register
//				Note :  this function alsa enables the SATA PHY pll bby writting to
//						bit 2 of the GPR13 register. This feature is not documented
//						in the imx6 TRM rev D, but is used in WCE7 and linux init
//						code.
//
//      Parameters:
//             none
//
//      Returns:
//             none
//
//------------------------------------------------------------------------------
VOID ConfigureSataPhy()
{
	PCSP_IOMUX_REGS pIOMUX;
	pIOMUX = OALPAtoUA(CSP_BASE_REG_PA_IOMUXC);

	// PHY configuration
	// rx_eq_val_0		iomuxc_gpr13[26:24]
	// los_lvl			iomuxc_gpr13[23:19]
	// rx_dpll_mode_0	iomuxc_gpr13[18:16]
	// sata_speed		iomuxc_gpr13[15]
	// mpll_ss_en		iomuxc_gpr13[14]
	// tx_atten_0		iomuxc_gpr13[13:11]
	// tx_boost_0		iomuxc_gpr13[10:7]
	// tx_lvl			iomuxc_gpr13[6:2]
	// mpll_ck_off		iomuxc_gpr13[1]		** This is not ducumented in rev D of the imx6 TRM,  **
	//										** but appears in linux and CE7 initialization codes **
	// tx_edgerate_0	iomuxc_gpr13[0]

	// Configure PHY
	INSREG32(&pIOMUX->GPR[13], 0xFFFFFFFD, 0x0593A044);

	// Enable PHY PLL
	INSREG32(&pIOMUX->GPR[13], 0x00000003, 0x2);
}

//------------------------------------------------------------------------------
//
//       Function:  EnableSataRefClock
//
//             This function enables the SATA ref clock from ethernet PLL
//				Ethernet PLL is responsible for generating the 100MHz SATA
//				reference clock.
//			   Note: We have to do stuff by hands here since DDK does not really deal with this clock
//				(comment from DDK: )
//				 "The following PLL6~PLL8 are not explicitly in clock tree, we need"
//				 "to consider expose a interface to open them"
//				(PLL8 IS the ethernet PLL)
//
//      Parameters:
//             none
//
//      Returns:
//             none
//
//------------------------------------------------------------------------------
void EnableSataRefClock()
{
	PCSP_PLL_BASIC_REGS pPLL_ENET;

	// Found no way from DDK to enable SATA ref clock, do it by hands...
	pPLL_ENET = OALPAtoUA(CSP_BASE_REG_PA_PLL_ENET);

	// Enable SATA 100MHz output
	INSREG32BF(&pPLL_ENET->CTRL, PLL_ENET_CTRL_ENABLE_SATA, 0x1);
	// Clear PLL POWERDOWN
	INSREG32BF(&pPLL_ENET->CTRL, PLL_ENET_CTRL_POWERDOWN, 0x0);
	// Clear PLL BYPASS
	INSREG32BF(&pPLL_ENET->CTRL, PLL_ENET_CTRL_BYPASS, 0x0);
	// Wait until PLL is locked
	while (PLL_CTL_NO_LOCKED == EXTREG32BF(&pPLL_ENET->CTRL, PLL_ENET_CTRL_LOCK));

	// If we don't sleep there, device won't be detected (spin up detection timeout),
	// maybe PLL needs some time to propagate ?
	OALStall(1000);
}

//------------------------------------------------------------------------------
//
//       Function:  void BSPBoard(UINT32* pValue)
//
//             This function initializes SATA clocks and PHY.
//
//      Parameters:
//             pValue [out]
//					value of the register to configure PHY. Since we use GPR13 to
//					configure PHY we set in to 0 to indicate to the common part
//					that no additional configuration has to be done
//
//      Returns:
//             none
//
//------------------------------------------------------------------------------
void BSPBoard(UINT32* pValue)
{
	// Enable SATA PLL (100MHz)
    OALClockSetGatingMode (DDK_CLOCK_GATE_INDEX_SATA, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
	
	// Configure SATA PHY from GPR13
	ConfigureSataPhy();

	// Enable SATA ref clock (100MHz) from CCM_ANALOG_PLL_ENET
	EnableSataRefClock();

	// SATA PHY was configured in ConfigureSataPhy, don't use this
	*pValue = 0;
}

UINT32 BSPATAGetBaseRegAddr(void)
{
    return CSP_BASE_REG_PA_SATA;
}
