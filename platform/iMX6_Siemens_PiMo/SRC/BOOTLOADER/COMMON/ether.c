//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//-----------------------------------------------------------------------------
//
//  Copyright (C) 2007-2010, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//-----------------------------------------------------------------------------
//
//  File:  ether.c
//
//  EBOOT ethernet routines
//
//-----------------------------------------------------------------------------
#pragma warning(push)
#pragma warning(disable: 4028)
#include "bsp.h"
#include "loader.h"
#include "kitl_cfg.h"
#include <usbdbgser.h>
#include <usbdbgrndis.h>

//-----------------------------------------------------------------------------
// External Functions
extern BOOL USBSerialInit();
extern void EthMapIO(void);
extern BOOL RndisInit( BYTE *pbBaseAddress, DWORD dwMultiplier, USHORT MacAddr[3]);
extern void RndisEnableInts(void);
extern void RndisDisableInts(void);
//extern UINT16 RndisEDbgSendFrame(UINT8 * pData,UINT32 dwLength);
extern VOID OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX index, DDK_CLOCK_GATE_MODE mode);

//-----------------------------------------------------------------------------
// External Variables
extern BSP_ARGS *g_pBSPArgs;
extern BOOT_CFG g_BootCFG;


//-----------------------------------------------------------------------------
// Defines
#define NEW_USB_KITL_DWLD 1


//-----------------------------------------------------------------------------
// Types


//-----------------------------------------------------------------------------
// Global Variables

// Function pointers to the support library functions of the currently 
// installed debug ethernet controller.
PFN_EDBG_INIT               pfnEDbgInit;
PFN_EDBG_GET_FRAME          pfnEDbgGetFrame;
PFN_EDBG_SEND_FRAME         pfnEDbgSendFrame;
PFN_EDBG_ENABLE_INTS        pfnEDbgEnableInts;
PFN_EDBG_DISABLE_INTS       pfnEDbgDisableInts;
PFN_EDBG_INIT_DMABUFFER     pfnEDbgInitDMABuffer;

//PFN_EDBG_GET_PENDING_INTS   pfnEDbgGetPendingInts;
//PFN_EDBG_READ_EEPROM        pfnEDbgReadEEPROM;
//PFN_EDBG_WRITE_EEPROM       pfnEDbgWriteEEPROM;
//PFN_EDBG_SET_OPTIONS        pfnEDbgSetOptions;

static int InitUSBRNDISDevice(OAL_KITL_ARGS *pKITLArgs)
{
    KITLOutputDebugString("INFO: Trying to initialize USB RNDIS...\r\n"); 

    pKITLArgs = pKITLArgs;
#if 1
    // same as ethernet
    memcpy( pKITLArgs->mac, g_BootCFG.mac, 6);
    pKITLArgs->mac[0] |= 2;

    KITLOutputDebugString("INFO: MAC address: %x-%x-%x-%x-%x-%x\r\n",
        g_pBSPArgs->kitl.mac[0] & 0x00FF, g_pBSPArgs->kitl.mac[0] >> 8,
        g_pBSPArgs->kitl.mac[1] & 0x00FF, g_pBSPArgs->kitl.mac[1] >> 8,
        g_pBSPArgs->kitl.mac[2] & 0x00FF, g_pBSPArgs->kitl.mac[2] >> 8);

    // init USB registers
    if (HostMiniInit(NULL, 1, pKITLArgs->mac))
    {
        pfnEDbgInit           = RndisInit;
        pfnEDbgEnableInts     = RndisEnableInts;
        pfnEDbgDisableInts    = RndisDisableInts;    
        pfnEDbgGetFrame       = RndisEDbgGetFrame;       
        pfnEDbgSendFrame      = RndisEDbgSendFrame;      
        //pfnEDbgReadEEPROM     = NULL;     
        //pfnEDbgWriteEEPROM    = NULL;    

        // TODO: These two functons are provided in \oak\drivers\ethdbg\rne_mdd but not in \oak\drivers\netcard\rndismini

        //pfnEDbgGetPendingInts = RndisGetPendingInts;  // TODO: This is called in OEMEthISR - what to do
        //pfnEDbgSetOptions     = RndisSetOptions;   // TODO: This function does nothing

#ifdef IMGSHAREETH
        pfnCurrentPacketFilter = RndisCurrentPacketFilter;
        pfnMulticastList       = RndisMulticastList;
#endif

        pKITLArgs->flags |= OAL_KITL_FLAGS_POLL;

        // Save the device location information for later use.
        pKITLArgs->devLoc.IfcType     = Internal;
        pKITLArgs->devLoc.BusNumber   = 0;
        pKITLArgs->devLoc.PhysicalLoc = (PVOID) (CSP_BASE_REG_PA_USB);    // not a real Ethernet card
        pKITLArgs->devLoc.LogicalLoc  = (DWORD)pKITLArgs->devLoc.PhysicalLoc;

        return 0;
        //return (EDBG_USB_RNDIS);
    }
    else
    {
        KITLOutputDebugString("ERROR: Failed to initialize ARC Rndis USB Ethernet controller.\n");
        return -1;
    }
#endif
}


//-----------------------------------------------------------------------------
//
// Function: InitSpecifiedEthDevice
//
// Initializes the specified Ethernet device to be used for download and 
// KITL services.
//
// Parameters:
//      pKITLArgs
//          [in/out] Points to the KITL argument structure.
//
//      EthDevice
//          [in] Ethernet device to be initialized.
//
// Returns:
//      Returns Ethernet adapter type intialized on success, otherwise 
//      returns -1.
//
//-----------------------------------------------------------------------------
UINT32 InitSpecifiedEthDevice(OAL_KITL_ARGS *pKITLArgs, UINT32 EthDevice)
{
    UINT32 rc = (UINT32)-1;
    //PUINT32 pIIM_MAC_ADDR = NULL;
    PCSP_IOMUX_REGS pIOMUX;
    PCSP_GPIO_REGS pGPIO2;
    g_SerialUSBDownload = FALSE;

    KITLOutputDebugString("+InitSpecifiedEthDevice \r\n");
    
    switch(EthDevice)
    {
    case ETH_DEVICE_LAN911x:
        pIOMUX = (PCSP_IOMUX_REGS) OALPAtoUA(CSP_BASE_REG_PA_IOMUXC);
        pGPIO2 = (PCSP_GPIO_REGS) OALPAtoUA(CSP_BASE_REG_PA_GPIO2);
        
#if 1   // MX6Q_BRING_UP
        // IOMUX related, to rewrite
#else
        // GPIO2_31 (ETHERNET_INT_B) muxed on EIM_EB3 as ALT1
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_EB3, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
        INSREG32(&pGPIO2->GDIR, GPIO_PIN_MASK(31), GPIO_PIN_VAL(GPIO_GDIR_INPUT, 31));
        INSREG32(&pGPIO2->EDGE_SEL, GPIO_PIN_MASK(31), GPIO_PIN_VAL(GPIO_EDGE_SEL_DISABLE, 31));
        INSREG32(&pGPIO2->ICR2, GPIO_ICR_MASK((31 - 16)), GPIO_ICR_VAL(GPIO_ICR_LOW_LEVEL, (31 - 16)));
        INSREG32(&pGPIO2->IMR, GPIO_PIN_MASK(31), GPIO_PIN_VAL(GPIO_IMR_UNMASKED, 31));

        // WEIM_RW muxed on EIM_RW as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_RW, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);

        // WEIM_OE muxed on EIM_OE as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_OE, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);

        // WEIM_CS1 muxed on EIM_CS1 as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_CS1, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);

        // WEIM_D[16] muxed on EIM_D[16] as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_D16, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);
        OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_D16, DDK_IOMUX_PAD_SLEW_NULL, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_KEEPER, DDK_IOMUX_PAD_HYSTERESIS_DISABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);
        
        // WEIM_D[17] muxed on EIM_D[17] as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_D17, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);
        OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_D17, DDK_IOMUX_PAD_SLEW_NULL, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_KEEPER, DDK_IOMUX_PAD_HYSTERESIS_DISABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);

        // WEIM_D[18] muxed on EIM_D[18] as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_D18, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);
        OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_D18, DDK_IOMUX_PAD_SLEW_NULL, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_KEEPER, DDK_IOMUX_PAD_HYSTERESIS_DISABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);

        // WEIM_D[19] muxed on EIM_D[19] as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_D19, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);
        OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_D19, DDK_IOMUX_PAD_SLEW_NULL, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_KEEPER, DDK_IOMUX_PAD_HYSTERESIS_DISABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);

        // WEIM_D[20] muxed on EIM_D[20] as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_D20, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);
        OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_D20, DDK_IOMUX_PAD_SLEW_NULL, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_KEEPER, DDK_IOMUX_PAD_HYSTERESIS_DISABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);

        // WEIM_D[21] muxed on EIM_D[21] as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_D21, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);
        OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_D21, DDK_IOMUX_PAD_SLEW_NULL, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_KEEPER, DDK_IOMUX_PAD_HYSTERESIS_DISABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);

        // WEIM_D[22] muxed on EIM_D[22] as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_D22, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);
        OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_D22, DDK_IOMUX_PAD_SLEW_NULL, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_KEEPER, DDK_IOMUX_PAD_HYSTERESIS_DISABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);

        // WEIM_D[23] muxed on EIM_D[23] as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_D23, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);
        OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_D23, DDK_IOMUX_PAD_SLEW_NULL, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_KEEPER, DDK_IOMUX_PAD_HYSTERESIS_DISABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);

        // WEIM_D[24] muxed on EIM_D[24] as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_D24, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);
        OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_D24, DDK_IOMUX_PAD_SLEW_NULL, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_KEEPER, DDK_IOMUX_PAD_HYSTERESIS_DISABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);

        // WEIM_D[25] muxed on EIM_D[25] as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_D25, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);
        OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_D25, DDK_IOMUX_PAD_SLEW_NULL, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_KEEPER, DDK_IOMUX_PAD_HYSTERESIS_DISABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);

        // WEIM_D[26] muxed on EIM_D[26] as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_D26, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);
        OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_D26, DDK_IOMUX_PAD_SLEW_NULL, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_KEEPER, DDK_IOMUX_PAD_HYSTERESIS_DISABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);

        // WEIM_D[27] muxed on EIM_D[27] as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_D27, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);
        OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_D27, DDK_IOMUX_PAD_SLEW_NULL, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_KEEPER, DDK_IOMUX_PAD_HYSTERESIS_DISABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);

        // WEIM_D[28] muxed on EIM_D[28] as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_D28, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);
        OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_D28, DDK_IOMUX_PAD_SLEW_NULL, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_KEEPER, DDK_IOMUX_PAD_HYSTERESIS_DISABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);

        // WEIM_D[29] muxed on EIM_D[29] as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_D29, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);
        OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_D29, DDK_IOMUX_PAD_SLEW_NULL, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_KEEPER, DDK_IOMUX_PAD_HYSTERESIS_DISABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);

        // WEIM_D[30] muxed on EIM_D[30] as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_D30, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);
        OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_D30, DDK_IOMUX_PAD_SLEW_NULL, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_KEEPER, DDK_IOMUX_PAD_HYSTERESIS_DISABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);

        // WEIM_D[31] muxed on EIM_D[31] as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_D31, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);
        OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_D31, DDK_IOMUX_PAD_SLEW_NULL, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_KEEPER, DDK_IOMUX_PAD_HYSTERESIS_DISABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);

        // WEIM_DA[0] muxed on EIM_DA[0] as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_DA0, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);
        OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_DA0, DDK_IOMUX_PAD_SLEW_NULL, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_KEEPER, DDK_IOMUX_PAD_HYSTERESIS_DISABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);

        // WEIM_DA[1] muxed on EIM_DA[1] as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_DA1, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);
        OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_DA1, DDK_IOMUX_PAD_SLEW_NULL, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_KEEPER, DDK_IOMUX_PAD_HYSTERESIS_DISABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);

        // WEIM_DA[2] muxed on EIM_DA[2] as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_DA2, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);
        OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_DA2, DDK_IOMUX_PAD_SLEW_NULL, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_KEEPER, DDK_IOMUX_PAD_HYSTERESIS_DISABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);

        // WEIM_DA[3] muxed on EIM_DA[3] as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_DA3, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);
        OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_DA3, DDK_IOMUX_PAD_SLEW_NULL, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_KEEPER, DDK_IOMUX_PAD_HYSTERESIS_DISABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);

        // WEIM_DA[4] muxed on EIM_DA[4] as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_DA4, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);
        OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_DA4, DDK_IOMUX_PAD_SLEW_NULL, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_KEEPER, DDK_IOMUX_PAD_HYSTERESIS_DISABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);

        // WEIM_DA[5] muxed on EIM_DA[5] as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_DA5, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);
        OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_DA5, DDK_IOMUX_PAD_SLEW_NULL, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_KEEPER, DDK_IOMUX_PAD_HYSTERESIS_DISABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);

        // WEIM_DA[6] muxed on EIM_DA[6] as ALT0
        OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_DA6, DDK_IOMUX_PIN_MUXMODE_ALT0, DDK_IOMUX_PIN_SION_REGULAR);
        OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_DA6, DDK_IOMUX_PAD_SLEW_NULL, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_KEEPER, DDK_IOMUX_PAD_HYSTERESIS_DISABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);
#endif

        if (LAN911xInit((PBYTE) OALPAtoUA(BSP_BASE_REG_PA_LAN911x_IOBASE), 0, pKITLArgs->mac))
        {
            pfnEDbgInit         = (PFN_EDBG_INIT) LAN911xInit;
            pfnEDbgGetFrame     = (PFN_EDBG_GET_FRAME) LAN911xGetFrame;
            pfnEDbgSendFrame    = (PFN_EDBG_SEND_FRAME) LAN911xSendFrame;
            pfnEDbgEnableInts   = (PFN_EDBG_ENABLE_INTS) LAN911xEnableInts;     
            pfnEDbgDisableInts  = (PFN_EDBG_DISABLE_INTS)LAN911xDisableInts;    
            
            // Save the device location information for later use.
            //
            pKITLArgs->devLoc.IfcType     = Internal;
            pKITLArgs->devLoc.BusNumber   = 0;
            pKITLArgs->devLoc.PhysicalLoc = (PVOID)(BSP_BASE_REG_PA_LAN911x_IOBASE);
            pKITLArgs->devLoc.LogicalLoc  = (DWORD)pKITLArgs->devLoc.PhysicalLoc;

            KITLOutputDebugString("INFO: LAN911x Ethernet controller initialized.\r\n");
            rc = EDBG_ADAPTER_OEM;
        }
        else
        {
            KITLOutputDebugString("ERROR: Failed to initialize LAN911x Ethernet controller.\n");
        }
        break;
     case ETH_DEVICE_FEC:
        // Enable clocks used by controller.  Note that EMI_FAST clocks are already enabled.
#if 1   // MX6Q_BRING_UP
        // CCM related, to rewrite
#else
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_FEC, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_PL301_4x1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
#endif
#if 0
        // Initialize DMA buffer for FEC
        if (!FECInitDMABuffer((UINT32) OALPAtoUA(IMAGE_SHARE_FEC_RAM_PA_START), IMAGE_SHARE_FEC_RAM_SIZE))
        {
            KITLOutputDebugString("ERROR: Failed to initialize Fast Ethernet DMA buffer.\n");
            break;
        }

        // Check if caller wants use FEC MAC programmed into IIM
        if ((pKITLArgs->mac[0] == 0xFFFF) && (pKITLArgs->mac[1] == 0xFFFF) && (pKITLArgs->mac[2] == 0xFFFF))
        {
            pIIM_MAC_ADDR = (PUINT32) OALPAtoUA(CSP_BASE_REG_PA_IIM + 0xC24);
            if (pIIM_MAC_ADDR == NULL)
            {
                KITLOutputDebugString("ERROR: Failed to map IIM MAC locations.\n");
                break;
            }
            pKITLArgs->mac[0] = (UINT16) ((INREG32(&pIIM_MAC_ADDR[1]) << 8) | (INREG32(&pIIM_MAC_ADDR[0])));
            pKITLArgs->mac[1] = (UINT16) ((INREG32(&pIIM_MAC_ADDR[3]) << 8) | (INREG32(&pIIM_MAC_ADDR[2])));
            pKITLArgs->mac[2] = (UINT16) ((INREG32(&pIIM_MAC_ADDR[5]) << 8) | (INREG32(&pIIM_MAC_ADDR[4])));
        }

        // Initialize FEC module
        if (FECInit((PBYTE) OALPAtoUA(CSP_BASE_REG_PA_ENET), 0, pKITLArgs->mac))
        {
            pfnEDbgInit             = (PFN_EDBG_INIT) FECInit;
            pfnEDbgGetFrame         = (PFN_EDBG_GET_FRAME) FECGetFrame;
            pfnEDbgSendFrame        = (PFN_EDBG_SEND_FRAME) FECSendFrame;
            pfnEDbgEnableInts       = (PFN_EDBG_ENABLE_INTS) FECEnableInts;
            pfnEDbgDisableInts      = (PFN_EDBG_DISABLE_INTS) FECDisableInts;
           
            pKITLArgs->devLoc.IfcType     = Internal;
            pKITLArgs->devLoc.BusNumber   = 0;
            pKITLArgs->devLoc.PhysicalLoc = (PVOID)(CSP_BASE_REG_PA_ENET);
            pKITLArgs->devLoc.LogicalLoc  = (DWORD)pKITLArgs->devLoc.PhysicalLoc;

            KITLOutputDebugString("INFO: Fast Ethernet controller initialized.\r\n");
            rc = EDBG_ADAPTER_OEM;
        }
        else
        {
            KITLOutputDebugString("ERROR: Failed to initialize Fast Ethernet controller.\n");
        }
#endif
        break;
    

    case ETH_DEVICE_USB:
#if NEW_USB_KITL_DWLD
        {
            KITLOutputDebugString("INFO: BootDevice - USB RNDIS.\r\n");
            // Save the device location information for later use.
            //
            pKITLArgs->devLoc.IfcType    = (DWORD)Internal; // Using InterfaceTypeUndefined will differentiate between USB RNDIS and USB Serial
            pKITLArgs->devLoc.BusNumber  = 0;
            pKITLArgs->devLoc.PhysicalLoc = (PVOID)CSP_BASE_REG_PA_USB;
            pKITLArgs->devLoc.LogicalLoc = (DWORD)pKITLArgs->devLoc.PhysicalLoc;
            pKITLArgs->devLoc.Pin        = 0;
            pKITLArgs->flags |= (OAL_KITL_FLAGS_VMINI | OAL_KITL_FLAGS_POLL);

            if (!Rndis_Init((UINT8 *)CSP_BASE_REG_PA_USB, 0, pKITLArgs->mac))
            {
                KITLOutputDebugString("ERROR: ERROR: Failed to initialize USB RNDIS for Download.\r\n");
            }
            pfnEDbgInit           = (PFN_EDBG_INIT) Rndis_Init;
            pfnEDbgEnableInts     = (PFN_EDBG_ENABLE_INTS)Rndis_EnableInts;
            pfnEDbgDisableInts    = (PFN_EDBG_DISABLE_INTS)Rndis_DisableInts;    
            pfnEDbgGetFrame       = (PFN_EDBG_GET_FRAME)Rndis_RecvFrame;       
            pfnEDbgSendFrame      = (PFN_EDBG_SEND_FRAME)Rndis_SendFrame; 
            rc = EDBG_ADAPTER_OEM;
            KITLOutputDebugString("INFO: Initialized USB for RNDIS download.\r\n");
        }
#else
        if( InitUSBRNDISDevice( pKITLArgs) != -1)
        {
            rc = EDBG_ADAPTER_OEM;
        }
        else
        {
            KITLOutputDebugString("ERROR: Failed to initialize USB RNDIS Ethernet controller.\n");
        }
#endif
        break;

    case SER_DEVICE_USB:
#if NEW_USB_KITL_DWLD
        {
            // Configure Serial USB Download
            KITL_SERIAL_INFO SerInfo;
            SerInfo.pAddress = (UINT8 *)CSP_BASE_REG_PA_USB;

            pKITLArgs->devLoc.IfcType    = Internal;
            pKITLArgs->devLoc.BusNumber  = 0;
            pKITLArgs->devLoc.PhysicalLoc = (PVOID)(CSP_BASE_REG_PA_USB+1);
            pKITLArgs->devLoc.LogicalLoc = (DWORD)pKITLArgs->devLoc.PhysicalLoc;
            pKITLArgs->flags &= ~OAL_KITL_FLAGS_VMINI;
            pKITLArgs->flags |= OAL_KITL_FLAGS_POLL;

            KITLOutputDebugString("INFO: BootDevice - USB Serial.\r\n");
            KITLOutputDebugString("INFO: Waiting for Platform Builder to connect...\r\n");
            if (!Serial_Init(&SerInfo))
            {
                KITLOutputDebugString("ERROR: Failed to initialize USB for serial download.\r\n");
            }
            g_SerialUSBDownload = TRUE;
            rc = EDBG_ADAPTER_OEM;
            KITLOutputDebugString("INFO: Initialized USB for serial download.\r\n");
        }   
#else
        // Indicate to OEMPreDownload that serial will be used
       
        if(!USBSerialInit())
        {
            KITLOutputDebugString("ERROR: Failed to detect and initialize USB Function controller.\r\n");
            break;
            
        }
        g_SerialUSBDownload = TRUE;
        rc = SDBG_USB_SERIAL;
         // same as ethernet
        memcpy( pKITLArgs->mac, g_BootCFG.mac, 6);
        pKITLArgs->mac[0] |= 2;

        // Save the device location information for later use.
        pKITLArgs->devLoc.IfcType     = Internal;
        pKITLArgs->devLoc.BusNumber   = 0;
        pKITLArgs->devLoc.PhysicalLoc = (PVOID)(CSP_BASE_REG_PA_USB+0x1);    // Just has to be different than RNDIS
        pKITLArgs->devLoc.LogicalLoc  = (DWORD)pKITLArgs->devLoc.PhysicalLoc;
        pKITLArgs->flags &= ~OAL_KITL_FLAGS_VMINI;
        pKITLArgs->flags |= OAL_KITL_FLAGS_POLL;
#endif

        break;
    case ETH_DEVICE_ENET:
        // Initialize DMA buffer for ENET
        if (!ENETInitDMABuffer((UINT32) OALPAtoUA(IMAGE_SHARE_ENET_RAM_PA_START), IMAGE_SHARE_ENET_RAM_SIZE))
        {
            KITLOutputDebugString("ERROR: Failed to initialize ENET  DMA buffer.\n");
            break;
        }
        
        // Check if caller wants use ENET  MAC programmed into OTP
        if ((pKITLArgs->mac[0] == 0xFFFF) && (pKITLArgs->mac[1] == 0xFFFF) && (pKITLArgs->mac[2] == 0xFFFF))
        {
          
//            pIIM_MAC_ADDR = (PUINT32) OALPAtoUA(OTP_PHY_ADDRESS);
//            if (pIIM_MAC_ADDR == NULL)
//            {
//                KITLOutputDebugString("ERROR: Failed to map OTP MAC locations.\n");
//                break;
//            }
//            
//            SETREG32(pIIM_MAC_ADDR,1<<12);

//            OALStall(50);
//            
//            dwMAC=INREG32(pIIM_MAC_ADDR+MAC_OFFSET_ADD/sizeof(DWORD));
            
            pKITLArgs->mac[0] = (UINT16)0x0400;    
            pKITLArgs->mac[1] = (UINT16)0x9F12;
            pKITLArgs->mac[2] = (UINT16)0x3456;
//            pKITLArgs->mac[1] = htons((UINT16)((dwMAC&0xffff0000)>>16));
//            pKITLArgs->mac[2] = htons ((UINT16)(dwMAC));
          
        }

        // Initialize ENET module
        if (ENETInit((PBYTE) OALPAtoUA(CSP_BASE_REG_PA_ENET), 0, pKITLArgs->mac))
        {
            pfnEDbgInit             = (PFN_EDBG_INIT) ENETInit;
            pfnEDbgGetFrame         = (PFN_EDBG_GET_FRAME) ENETGetFrame;
            pfnEDbgSendFrame        = (PFN_EDBG_SEND_FRAME) ENETSendFrame;
            pfnEDbgEnableInts       = (PFN_EDBG_ENABLE_INTS) ENETEnableInts;
            pfnEDbgDisableInts      = (PFN_EDBG_DISABLE_INTS) ENETDisableInts;
            pfnEDbgInitDMABuffer    = (PFN_EDBG_INIT_DMABUFFER) ENETInitDMABuffer;

            // Save the device location information for later use.
            //
            pKITLArgs->devLoc.IfcType     = Internal;
            pKITLArgs->devLoc.BusNumber   = 0;
            pKITLArgs->devLoc.PhysicalLoc = (PVOID)(CSP_BASE_REG_PA_ENET);
            pKITLArgs->devLoc.LogicalLoc  = (DWORD)pKITLArgs->devLoc.PhysicalLoc;

            KITLOutputDebugString("INFO: ENET controller initialized.\r\n");
            rc = EDBG_ADAPTER_OEM;
        }
        else
        {
            KITLOutputDebugString("ERROR: Failed to initialize ENET controller.\n");
        }
        break;
   

   

    default:
        KITLOutputDebugString("ERROR: Failed to initialize unknown Ethernet controller. EthDevice = %d\n",EthDevice);
        break;
    }

    return rc;

}

