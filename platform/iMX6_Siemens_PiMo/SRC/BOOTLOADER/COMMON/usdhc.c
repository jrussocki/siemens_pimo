//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//------------------------------------------------------------------------------
//
//  Copyright (C) 2012, Adeneo Embedded All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//  Copyright (C) 2007-2011 Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
#include "bsp.h"
#include "sdfmd.h"
#include "loader.h"
  

//-----------------------------------------------------------------------------
// External Functions
extern VOID OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX index, DDK_CLOCK_GATE_MODE mode);


//-----------------------------------------------------------------------------
// External Variables
extern PCSP_IOMUX_REGS g_pIOMUX;
extern BOOL g_bSDHCBootloader;
extern BOOL g_fDDRMode;
//-----------------------------------------------------------------------------
// Defines
#define SD1_CARD_DETECT_PIN         (1)
#define SD1_CARD_WRITEPROTECT_PIN   (9)
#define SD2_CARD_DETECT_PIN         (4)
#define SD2_CARD_WRITEPROTECT_PIN   (2)

// SRC SBMR bit fields (left shift)
#define SRC_SBMR_BOOT_CFG1_LSH              0
#define SRC_SBMR_BOOT_CFG2_LSH              8
#define SRC_SBMR_BOOT_CFG3_LSH              16
#define SRC_SBMR_BMOD_LSH                   24
#define SRC_SBMR_BT_FUSE_SEL_LSH            26
#define SRC_SBMR_TEST_MODE_LSH              27

// SRC SBMR bit fields (width)
#define SRC_SBMR_BOOT_CFG1_WID              8
#define SRC_SBMR_BOOT_CFG2_WID              8
#define SRC_SBMR_BOOT_CFG3_WID              8
#define SRC_SBMR_BMOD_WID                   2
#define SRC_SBMR_BT_FUSE_SEL_WID            1
#define SRC_SBMR_TEST_MODE_WID              3

// SRC SBMR bit fields (values)
#define SRC_SBMR_BOOT_CFG1_LSH              0

// SRC SMBR CFG1
#define SRC_SMBR_CFG1_BOOT_DEV_LSH          (6)
#define SRC_SMBR_CFG1_SDMMC_SEL_LSH         (5)
#define SRC_SMBR_CFG1_FAST_BOOT_LSH         (4)
#define SRC_SMBR_CFG1_SPEED_MODE_LSH        (2)
#define SRC_SMBR_CFG1_SDLB_CLKSEL_LSH       (0)

#define SRC_SMBR_CFG1_BOOT_DEV_WID          (2)
#define SRC_SMBR_CFG1_SDMMC_SEL_WID         (1)
#define SRC_SMBR_CFG1_FAST_BOOT_WID         (1)
#define SRC_SMBR_CFG1_SPEED_MODE_WID        (2)
#define SRC_SMBR_CFG1_SDLB_CLKSEL_WID       (2)

// SRC SMBR CFG2
#define SRC_SMBR_CFG2_BUSWID_SDCALSTP_LSH   (5)
#define SRC_SMBR_CFG2_PORT_SEL_LSH          (3)
#define SRC_SMBR_CFG2_DLL_OR_LSH            (2)
#define SRC_SMBR_CFG2_BOOT_ACK_LSH          (1)
#define SRC_SMBR_CFG2_OR_PAD_SET_LSH        (0)

#define SRC_SMBR_CFG2_BUSWID_SDCALSTP_WID   (3)
#define SRC_SMBR_CFG2_PORT_SEL_WID          (2)
#define SRC_SMBR_CFG2_DLL_OR_WID            (1)
#define SRC_SMBR_CFG2_BOOT_ACK_WID          (1)
#define SRC_SMBR_CFG2_OR_PAD_SET_WID        (1)


//-----------------------------------------------------------------------------
// Types
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Global Variables
SDH_HARDWARE_CONTEXT    SDController;
PCSP_USDHC_REG          g_pUSDHCReg;    // SD/MMC controller registers
extern DWORD                   g_SDSlotIndex;
DWORD                   dwVVN;
BOOL                    bLastCmdRead = FALSE;

// All available SD slots
DWORD g_AvaiSDSlotIdx[] = {2, 3};
// TODO: move "g_AvaiSDSlotNum" to header file and define it instead of declaring as a variable.
DWORD g_AvaiSDSlotNum = sizeof(g_AvaiSDSlotIdx)/sizeof(DWORD);

//-----------------------------------------------------------------------------
// Local Variables


//-----------------------------------------------------------------------------
// Local Functions

//-----------------------------------------------------------------------------
//
//  Function: SDConfigPins
//
//  This function configures the pins and enables the SD interface.
//
//  Parameters:
//        None.
//
//  Returns:
//        None.
//
//-----------------------------------------------------------------------------
void SDConfigPins (void)
{
    UINT32 pSBMR = 0, SBMR = 0; 

    // Get SD slot selection.
    pSBMR = (UINT32) OALPAtoUA(CSP_BASE_REG_PA_SRC+4);
    if (!pSBMR) 
    {
        OALMSGS(OAL_ERROR, (_T("SDConfigPins: SRC mapping failed!\r\n")));
    }

    SBMR = INREG32(pSBMR);

    //  Boot Device         BOOT_CFG2[4:3]
    //------------------------------------
    //  eSDHC1              00
    //  eSDHC2              01: SD2 4-bit ÂµSD slot onboard
    //  eSDHC3              10: SD3 8-bit eMMC 8GB onboard
    //  eSDHC4              11: SD4 8-bit SDIO SD slot

    if (g_SDSlotIndex == 0xFFFFFFFF)
    {
        UINT32 SBMR = 0;
        UCHAR cfg1 = 0;
        UCHAR cfg2 = 0;

        //  This check should be done only at the beginning of boot
        g_SDSlotIndex = 2;

        //  Automatically detect the SD/eSD/MMC/eMMC Slot from which the Eboot has booted
        SBMR = INREG32(pSBMR);
        cfg1 = CSP_BITFEXT(SBMR, SRC_SBMR_BOOT_CFG1);
        cfg2 = CSP_BITFEXT(SBMR, SRC_SBMR_BOOT_CFG2);

        //  Auto-detect the boot SD/eSD/MMC/eMMC Slot only if the boot occured from SD/eSD/MMC/eMMC Card
        if ( (0x8 <= (cfg1>>3) ) && ( (cfg1>>3) <= 0xF) )
        {
            g_SDSlotIndex = CSP_BITFEXT(cfg2, SRC_SMBR_CFG2_PORT_SEL);
        }
    }

    OALMSGS(OAL_INFO, (L"USDHC[%d] is being activated...\r\n", g_SDSlotIndex + 1));


    switch(g_SDSlotIndex)
    {
    case 0x1: // SD2 4-bit ÂµSD slot onboard
		g_pUSDHCReg = (PCSP_USDHC_REG) OALPAtoUA(CSP_BASE_REG_PA_USDHC2);
        break;

    case 0x2: // SD3 sd slot
		g_pUSDHCReg = (PCSP_USDHC_REG) OALPAtoUA(CSP_BASE_REG_PA_USDHC3);
        break;

    case 0x3: // SD4 8-bit  eMMC 8GB onboard
        g_pUSDHCReg = (PCSP_USDHC_REG) OALPAtoUA(CSP_BASE_REG_PA_USDHC4);
		break;
    }
}

//-----------------------------------------------------------------------------
//
//  Function: SDInterface_Init
//
//  This function configures and resets the SD controller.
//
//  Parameters:
//        None.
//
//  Returns:
//        TRUE for success/FALSE for failure.
//
//-----------------------------------------------------------------------------
BOOL SDInterface_Init (void)
{
	
    DWORD dwProctlReg = 0;
    UINT32 pSBMR = 0, SBMR = 0; 
    UCHAR cfg1 = 0, cfg2 = 0;

    SDController.IsMMC = FALSE;
    SDController.BusWidthSetting = USDHC_DTW_1BIT;
    SDController.Words_in_fifo  = USDHC_MAX_DATA_BUFFER_SIZE >> 2;    
    SDController.Bytes_in_fifo  = USDHC_MAX_DATA_BUFFER_SIZE; 
    SDController.SendInitClocks = TRUE;
	//OALMSGS(1, (L"+ SDInterface_Init\r\n"));

    // Get SD slot selection.
    pSBMR = (UINT32) OALPAtoUA(CSP_BASE_REG_PA_SRC+4);
    if (!pSBMR) 
    {
        OALMSGS(OAL_ERROR, (_T("SDConfigPins: SRC mapping failed!\r\n")));
    }

    SBMR = INREG32(pSBMR);
    cfg1 = CSP_BITFEXT(SBMR, SRC_SBMR_BOOT_CFG1);
    cfg2 = CSP_BITFEXT(SBMR, SRC_SBMR_BOOT_CFG2);

    OALMSG(0, (L"SD/MMC boot info:\r\n"));
    OALMSG(0, (L"Boot device:    0x%x\r\n", CSP_BITFEXT(cfg1, SRC_SMBR_CFG1_BOOT_DEV)));
    OALMSG(0, (L"Boot from:      %s\r\n", CSP_BITFEXT(cfg1, SRC_SMBR_CFG1_SDMMC_SEL) ? L"MMC" : L"SD"));
    OALMSG(0, (L"Boot mode:      %s\r\n", CSP_BITFEXT(cfg1, SRC_SMBR_CFG1_FAST_BOOT) ? L"fast" : L"normal"));
    OALMSG(0, (L"Speed mode:     0x%x\r\n", CSP_BITFEXT(cfg1, SRC_SMBR_CFG1_SPEED_MODE)));
    OALMSG(0, (L"SD lpbk clksrc  0x%x\r\n", CSP_BITFEXT(cfg1, SRC_SMBR_CFG1_SDLB_CLKSEL)));
    OALMSG(0, (L"bus width:      0x%x\r\n", CSP_BITFEXT(cfg2, SRC_SMBR_CFG2_BUSWID_SDCALSTP)));
    OALMSG(0, (L"boot port:      0x%x\r\n", CSP_BITFEXT(cfg2, SRC_SMBR_CFG2_PORT_SEL)));
    OALMSG(0, (L"---------------------------\r\n"));

    switch(g_SDSlotIndex)
    {
        case 0:
            OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_USDHC1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
            break;
        case 1:
            OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_USDHC2, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
            break;
        case 2:
            OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_USDHC3, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
            break;
        case 3:
            OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_USDHC4, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
            break;
    }

    if (CSP_BITFEXT(cfg1, SRC_SMBR_CFG1_SDMMC_SEL) &&         // MMC memory card
            CSP_BITFEXT(cfg1, SRC_SMBR_CFG1_FAST_BOOT))       // fast boot
    {
        OALMSG(1, (L"SD/MMC: MMC @ fast boot, clear mmc boot enable bit\r\n"));
        INSREG32BF(&g_pUSDHCReg->MMCBOOT,USDHC_MMCBOOT_BOOT_EN, 0);
    }

    // Reset the controller
    INSREG32BF(&g_pUSDHCReg->SYSCTL,USDHC_SYSCTL_RSTA, 1);

    // wait for reset to complete
    while (INREG32(&g_pUSDHCReg->SYSCTL) & CSP_BITFMASK(USDHC_SYSCTL_RSTA));

    if(!SDHC_IsCardPresent ())
    {
        OALMSG(TRUE,(_T("Card is not present\r\n")));
        return FALSE; 
    }

    //clear DMAS bit in protocl control reg
    dwProctlReg = INREG32(&g_pUSDHCReg->PROCTL);
    OUTREG32(&g_pUSDHCReg->PROCTL, dwProctlReg & (~CSP_BITFMASK(USDHC_PROCTL_DMAS)));

    // Mask all SDHC interrupts
    OUTREG32(&g_pUSDHCReg->IRQSIGEN, 0);

    // Enable all status bits in the IRQSTAT register except CINS and CRM because we do not process changes in card status in eboot
    OUTREG32(&g_pUSDHCReg->IRQSTATEN, 0xFFFFFF3F);

    SDController.dwClockRate = ESDHC_INIT_CLOCK_RATE;
    SetClockRate(&(SDController.dwClockRate)); // Identification Frequency
    dwVVN = CSP_BITFEXT(INREG32(&g_pUSDHCReg->HOSTVER), USDHC_HOSTVER_VVN);

	//OALMSGS(1, (L"- SDInterface_Init\r\n"));
    return TRUE;
}

//-----------------------------------------------------------------------------
//
//  Function: SDHC_IsCardPresent
//
//  This function detects the presence of SD/MMC card.
//
//  Parameters:
//        None.
//
//  Returns:
//        TRUE for success/FALSE for failure.
//
//-----------------------------------------------------------------------------
BOOL SDHC_IsCardPresent (void)
{
    /*
    BOOL bCardPresent = FALSE;
    DWORD i = 0;

    // read the bit a 100 times to allow controller to debounce the SD1_CD line
    for (; i < 100; i++)
    {
        if(g_SDSlotIndex == 0)
        {
            // card is present if CINS bit of PRSSTAT reads back as 1. This bit is already debounced by the controller
            if (CSP_BITFEXT(INREG32(&g_pUSDHCReg->PRSSTAT), USDHC_PRSSTAT_CINS))        
            {
                bCardPresent = TRUE;
                break;
            }
        }
        else if(g_SDSlotIndex == 1)
        {
            // card is present if CINS bit of PRSSTAT reads back as 1. This bit is already debounced by the controller
            if (CSP_BITFEXT(INREG32(&g_pUSDHCReg->PRSSTAT), USDHC_PRSSTAT_CINS))        
            {
                bCardPresent = TRUE;
                break;
            }          
        }

    }
    
    return bCardPresent;
    */
    return TRUE;
}

//-----------------------------------------------------------------------------
//
//  Function: SDHC_IsCardWriteProtected
//
//  This function detects whether WP switch on card is on or off
//
//  Parameters:
//        None.
//
//  Returns:
//        TRUE for write protect set, FALSE for not set.
//
//-----------------------------------------------------------------------------
BOOL SDHC_IsCardWriteProtected(void)
{
    if (!CSP_BITFEXT(INREG32(&g_pUSDHCReg->PRSSTAT), USDHC_PRSSTAT_WPSPL))
    {
        return TRUE;
    }
    return FALSE;
}

//------------------------------------------------------------------------------
//
// Function: SetRate
//
// Sets the desired SD/MMC clock frequency. Note: The closest frequecy
//            to the desired setting is chosen.
//
// Parameters:
//          dwRate[in] - desired clock rate in Hz
//
// Returns:
//        None
//
//------------------------------------------------------------------------------
void SetClockRate(PDWORD pdwRate)
{
    BSP_ARGS *pBspArgs = (BSP_ARGS *)IMAGE_SHARE_ARGS_UA_START;
    ULONG ulClockIndex;
    ULONG ulMaxSDClk; 
    ULONG ulMinSDClk; 
    const ULONG ulMaxDivisor = 16;
    const ULONG ulMaxPrescaler = 256;
    ULONG ulDiv =1, ulPrescaler = 1, ulErr = 0xFFFFFFFF;
    ULONG ulCurrRate = 0, ulCurrErr = 0, i = 0, j = 0;
    
    DWORD rate = *pdwRate;
    
    switch(g_SDSlotIndex)
    {
        case 0:
            ulClockIndex = DDK_CLOCK_SIGNAL_USDHC1;
            break;
        case 1:
            ulClockIndex = DDK_CLOCK_SIGNAL_USDHC2;
            break;
        case 2:
            ulClockIndex = DDK_CLOCK_SIGNAL_USDHC3;
            break;
        case 3:
            ulClockIndex = DDK_CLOCK_SIGNAL_USDHC4;
            break;
        default:
            ulClockIndex = DDK_CLOCK_SIGNAL_USDHC3;
            break;
    }

    ulMaxSDClk = min(USDHC_MAX_CLOCK_RATE, pBspArgs->clockFreq[ulClockIndex]);
    ulMinSDClk = pBspArgs->clockFreq[ulClockIndex] >> 12;  // BaseClk / 4096 is the minimum SD clock    
    
    if( rate > ulMaxSDClk)
    {
        rate = ulMaxSDClk;
    }
    else if (rate < ulMinSDClk)
    {
        rate = ulMinSDClk;
    }

    // find the best values for Divisor and Prescalar 
    for (i = 1; i <= ulMaxDivisor; i++)
    {
        for (j = 1; j < ulMaxPrescaler; j <<= 1)
        {
            ulCurrRate = pBspArgs->clockFreq[ulClockIndex] / (i * j);

            if (ulCurrRate <= rate)
            {
                ulCurrErr = rate - ulCurrRate;
                if (ulCurrErr <= ulErr)
                {
                    ulDiv = i;
                    ulPrescaler = j;
                    ulErr = ulCurrErr;
                }

                // we are done if we can generate the exact rate asked for
                if (ulErr == 0)
                    break;

            }

        }

        // we are done if we can generate the exact rate asked for
        if (ulErr == 0)
            break;

    }

    // Actual rate to be generated
    rate = pBspArgs->clockFreq[ulClockIndex] / (ulDiv * ulPrescaler);

    // Map the divisors to their respective register bit values
    ulDiv--;
    ulPrescaler >>= 1;

    OALMSG(OAL_FUNC, (TEXT("SetClockRate - Requested Rate: %d Hz, Setting clock rate to %d Hz, ulDiv %d, ulPrescaler %d\r\n"), 
                *pdwRate, rate, ulDiv, ulPrescaler ));
    // clear SDCLKEN bit first before changing frequency
    CLRREG32(&g_pUSDHCReg->SYSCTL, CSP_BITFMASK(USDHC_SYSCTL_SDCLKEN));
    
    // set the clock rate
    INSREG32BF(&g_pUSDHCReg->SYSCTL, USDHC_SYSCTL_DVS, ulDiv);
    INSREG32BF(&g_pUSDHCReg->SYSCTL, USDHC_SYSCTL_SDCLKFS, ulPrescaler);

    // set the data timeout value to be maximum
    INSREG32BF(&g_pUSDHCReg->SYSCTL, USDHC_SYSCTL_DTOCV, USDHC_MAX_DATA_TIMEOUT_COUNTER_VAL);

    // wait until new frequency is stabilized (SDSTB bit is only avaliable on VVN = 0x12 or later)
    //if ( CSP_BITFEXT(INREG32(&g_pUSDHCReg->HOSTVER), USDHC_HOSTVER_VVN) >= ESDHC_VVN_FSL22 )
    //OALMSG(1,(L"%d, %d\r\n", CSP_BITFEXT(INREG32(&g_pUSDHCReg->HOSTVER), USDHC_HOSTVER_VVN), ESDHC_VVN_FSL22));
    while(! CSP_BITFEXT(INREG32(&g_pUSDHCReg->PRSSTAT), USDHC_PRSSTAT_SDSTB));

    // Enable clocks (including SDCLKEN)
    INSREG32(&g_pUSDHCReg->SYSCTL, 0xF, 0xF);
    
    // return the actual frequency
    *pdwRate = rate;

    OALMSG(OAL_FUNC, (_T("- SetClockRate\r\n")));
}

//------------------------------------------------------------------------------
//
// Function: SDHCCmdConfig
//
// Configure USDHC registers for sending a command to MMC/SD.
//
// Parameters:
//          pReq[in] - structure containing necesary fields to issue a command
//
// Returns:
//        None
//
//------------------------------------------------------------------------------
static void SDHCCmdConfig(PSD_BUS_REQUEST pReq)                                                           
{
    DWORD dwXferTypReg = 0;
    DWORD dwMixCtrlReg = 0;

    UCHAR ucCmd = pReq->CommandCode;
    UINT32 Arg = pReq->CommandArgument;
    UINT16 respType = pReq->CommandResponse.ResponseType;
    UINT16 TransferClass = pReq->TransferClass;
    DWORD dwPresStatReg = INREG32(&g_pUSDHCReg->PRSSTAT);

    // Can't write to XFERTYP register while CIHB is set
    if (dwPresStatReg & CSP_BITFMASK(USDHC_PRSSTAT_CIHB))
    {
        OALMSG(1, (L"SDHCCmdConfig: CIHB set, can't Send Cmd!! CMD[%d]\r\n", ucCmd));
        return;
    }
    
    // For commands with data, can't write to XFERTYP while CDIHB is set
    if (TransferClass != SD_COMMAND && (dwPresStatReg & CSP_BITFMASK(USDHC_PRSSTAT_CDIHB)))
    {
        OALMSG(1, (L"SDHCCmdConfig: CDIHB set, can't Send Data Cmd!!\r\n"));
        return;
    }
    OALMSG(OAL_FUNC, (L"SDHCCmdConfig: Send Cmd[%d]!!\r\n", ucCmd));

    if (SDController.SendInitClocks) 
    {
        SDController.SendInitClocks = FALSE;

        // send the initialization clocks (80) before first command is sent
        SETREG32(&g_pUSDHCReg->SYSCTL, CSP_BITFMASK(USDHC_SYSCTL_INITA));
        // wait until done sending init clocks
        while(INREG32(&g_pUSDHCReg->SYSCTL) & CSP_BITFMASK(USDHC_SYSCTL_INITA));
    }

    // Set the command index
    dwXferTypReg = CSP_BITFVAL(USDHC_XFERTYP_CMDINX, ucCmd);
    
    dwXferTypReg |= CSP_BITFVAL(USDHC_XFERTYP_CMDTYP, USDHC_CMD_NORMAL);    

    // set DPSEL and block size for transfers that are not command-only
    if (TransferClass != SD_COMMAND)
    {

        // workaround for hw errata for SDHC versions < 2.2 that writes following reads will result in DCE error
        //if (dwVVN < ESDHC_VVN_FSL22 && TransferClass == SD_WRITE && bLastCmdRead)
        //    SETREG32(&g_pUSDHCReg->SYSCTL, CSP_BITFMASK(USDHC_SYSCTL_RSTD));
    
        dwXferTypReg |= CSP_BITFVAL(USDHC_XFERTYP_DPSEL, 1);

        // setup blocksize and number of blocks to transfer (= 1 for single transfer)
        OUTREG32(&g_pUSDHCReg->BLKATTR, (CSP_BITFVAL(USDHC_BLKATTR_BLKSIZE, pReq->BlockSize) | CSP_BITFVAL(USDHC_BLKATTR_BLKCNT, pReq->NumBlocks)) );
        // setup data bus width
        if (SDController.BusWidthSetting == SD_INTERFACE_SD_MMC_1BIT)
        {
            INSREG32BF(&g_pUSDHCReg->PROCTL, USDHC_PROCTL_DTW, USDHC_DTW_1BIT);
        }
        else if (SDController.BusWidthSetting == SD_INTERFACE_SD_4BIT)
        {
            INSREG32BF(&g_pUSDHCReg->PROCTL, USDHC_PROCTL_DTW, USDHC_DTW_4BIT);
        }
        else
        {
            INSREG32BF(&g_pUSDHCReg->PROCTL, USDHC_PROCTL_DTW, USDHC_DTW_8BIT);
        }

        // set watermark level (WML register units are 4-byte words, not bytes
        SDController.Bytes_in_fifo = min(pReq->BlockSize, USDHC_MAX_DATA_BUFFER_SIZE);
        SDController.Words_in_fifo = SDController.Bytes_in_fifo >> 2;
        OUTREG32(&g_pUSDHCReg->WML, CSP_BITFVAL(USDHC_WML_RDWML, SDController.Words_in_fifo)
                                                      | CSP_BITFVAL(USDHC_WML_WRWML, SDController.Words_in_fifo) );

        // Multi-block transfer? Set BCEN, MSBSEL bits
        if (pReq->NumBlocks > 1)
        {
            dwMixCtrlReg |= CSP_BITFVAL(USDHC_MIXCTL_MSBSEL, 1);
            dwMixCtrlReg |= CSP_BITFVAL(USDHC_MIXCTL_BCEN, 1);

            // Set AC12EN bit to have controller auto-send the CMD12 at the end of multi-block transaction
            dwMixCtrlReg |= CSP_BITFVAL(USDHC_MIXCTL_AC12EN, 1);

        }

        // set DTDSEL for reads from card
        if (TransferClass == SD_READ)
        {
            dwMixCtrlReg |= CSP_BITFVAL(USDHC_MIXCTL_DTDSEL, 1);
            bLastCmdRead = TRUE;
        }
        else
        {
            bLastCmdRead = FALSE;
        }
    }

    switch( respType )
    {
    case NoResponse:
        // RSPTYP, CICEN, and CCCEN remain 0, nothing to do
        break;

    case ResponseR1:
    case ResponseR5:
    case ResponseR6:        
        dwXferTypReg |= CSP_BITFVAL(USDHC_XFERTYP_RSPTYP, USDHC_RSPLEN_48);
        dwXferTypReg |= CSP_BITFVAL(USDHC_XFERTYP_CICEN, 1);
        dwXferTypReg |= CSP_BITFVAL(USDHC_XFERTYP_CCCEN, 1);
        break;

    case ResponseR2:
        dwXferTypReg |= CSP_BITFVAL(USDHC_XFERTYP_RSPTYP, USDHC_RSPLEN_136);
        dwXferTypReg |= CSP_BITFVAL(USDHC_XFERTYP_CCCEN, 1);
        break;

    case ResponseR3:
    case ResponseR4:
        dwXferTypReg |= CSP_BITFVAL(USDHC_XFERTYP_RSPTYP, USDHC_RSPLEN_48);
        break;

    // Some R5 CMDs will be treated by USDHC as R5b (need to define here), for eg. CMD52 for I/O Abort ...
    case ResponseR1b:                
        dwXferTypReg |= CSP_BITFVAL(USDHC_XFERTYP_RSPTYP, USDHC_RSPLEN_48B);
        dwXferTypReg |= CSP_BITFVAL(USDHC_XFERTYP_CICEN, 1);
        dwXferTypReg |= CSP_BITFVAL(USDHC_XFERTYP_CCCEN, 1);
        break;

    default:
        break;
    }
    
    // Clear Interrupt Status Register (all bits) before sending the command
    OUTREG32(&g_pUSDHCReg->IRQSTAT, 0xFFFFFFFF);
    
    // Write the command argument, and send the command
    OUTREG32(&g_pUSDHCReg->CMDARG, Arg);
    if (SDController.IsMMC && g_fDDRMode)
    {
        dwMixCtrlReg |= CSP_BITFVAL(USDHC_MIXCTL_DDREN, 1);
    }
    OUTREG32(&g_pUSDHCReg->MIXCTL, dwMixCtrlReg);

    OALMSG(0, (L"before\r\n"));
    OALMSG(0, (L"DSADDR: 0x%08X\r\n", INREG32(&g_pUSDHCReg->DSADDR)));
    OALMSG(0, (L"BLKATTR: 0x%08X\r\n", INREG32(&g_pUSDHCReg->BLKATTR)));
    OALMSG(0, (L"CMDARG: 0x%08X\r\n", INREG32(&g_pUSDHCReg->CMDARG)));
    OALMSG(0, (L"XFERTYP: 0x%08X\r\n", INREG32(&g_pUSDHCReg->XFERTYP)));
    OALMSG(0, (L"CMDRSP0: 0x%08X\r\n", INREG32(&g_pUSDHCReg->CMDRSP0)));
    OALMSG(0, (L"CMDRSP1: 0x%08X\r\n", INREG32(&g_pUSDHCReg->CMDRSP1)));
    OALMSG(0, (L"CMDRSP2: 0x%08X\r\n", INREG32(&g_pUSDHCReg->CMDRSP2)));
    OALMSG(0, (L"CMDRSP3: 0x%08X\r\n", INREG32(&g_pUSDHCReg->CMDRSP3)));
    OALMSG(0, (L"DATPORT: 0x%08X\r\n", INREG32(&g_pUSDHCReg->DATPORT)));
    OALMSG(0, (L"PRSSTAT: 0x%08X\r\n", INREG32(&g_pUSDHCReg->PRSSTAT)));
    OALMSG(0, (L"PROCTL: 0x%08X\r\n", INREG32(&g_pUSDHCReg->PROCTL)));
    OALMSG(0, (L"SYSCTL: 0x%08X\r\n", INREG32(&g_pUSDHCReg->SYSCTL)));
    OALMSG(0, (L"IRQSTAT: 0x%08X\r\n", INREG32(&g_pUSDHCReg->IRQSTAT)));
    OALMSG(0, (L"IRQSTATEN: 0x%08X\r\n", INREG32(&g_pUSDHCReg->IRQSTATEN)));
    OALMSG(0, (L"IRQSIGEN: 0x%08X\r\n", INREG32(&g_pUSDHCReg->IRQSIGEN)));
    OALMSG(0, (L"AUTOC12ERR: 0x%08X\r\n", INREG32(&g_pUSDHCReg->AUTOC12ERR)));
    OALMSG(0, (L"HOSTCAPBLT: 0x%08X\r\n", INREG32(&g_pUSDHCReg->HOSTCAPBLT)));
    OALMSG(0, (L"WML: 0x%08X\r\n", INREG32(&g_pUSDHCReg->WML)));
    OALMSG(0, (L"MIXCTL: 0x%08X\r\n", INREG32(&g_pUSDHCReg->MIXCTL)));
    OALMSG(0, (L"FEVT: 0x%08X\r\n", INREG32(&g_pUSDHCReg->FEVT)));
    OALMSG(0, (L"DLLCTL: 0x%08X\r\n", INREG32(&g_pUSDHCReg->DLLCTL)));
    OALMSG(0, (L"DLLSTATUS: 0x%08X\r\n", INREG32(&g_pUSDHCReg->DLLSTATUS)));
    OALMSG(0, (L"VENDOR: 0x%08X\r\n", INREG32(&g_pUSDHCReg->VENDOR)));
    OALMSG(0, (L"MMCBOOT: 0x%08X\r\n", INREG32(&g_pUSDHCReg->MMCBOOT)));
    OALMSG(0, (L"HOSTVER: 0x%08X\r\n", INREG32(&g_pUSDHCReg->HOSTVER)));

    OUTREG32(&g_pUSDHCReg->XFERTYP, dwXferTypReg);
    
    OALMSG(0, (L"after\r\n"));
    OALMSG(0, (L"DSADDR: 0x%08X\r\n", INREG32(&g_pUSDHCReg->DSADDR)));
    OALMSG(0, (L"BLKATTR: 0x%08X\r\n", INREG32(&g_pUSDHCReg->BLKATTR)));
    OALMSG(0, (L"CMDARG: 0x%08X\r\n", INREG32(&g_pUSDHCReg->CMDARG)));
    OALMSG(0, (L"XFERTYP: 0x%08X\r\n", INREG32(&g_pUSDHCReg->XFERTYP)));
    OALMSG(0, (L"CMDRSP0: 0x%08X\r\n", INREG32(&g_pUSDHCReg->CMDRSP0)));
    OALMSG(0, (L"CMDRSP1: 0x%08X\r\n", INREG32(&g_pUSDHCReg->CMDRSP1)));
    OALMSG(0, (L"CMDRSP2: 0x%08X\r\n", INREG32(&g_pUSDHCReg->CMDRSP2)));
    OALMSG(0, (L"CMDRSP3: 0x%08X\r\n", INREG32(&g_pUSDHCReg->CMDRSP3)));
    OALMSG(0, (L"DATPORT: 0x%08X\r\n", INREG32(&g_pUSDHCReg->DATPORT)));
    OALMSG(0, (L"PRSSTAT: 0x%08X\r\n", INREG32(&g_pUSDHCReg->PRSSTAT)));
    OALMSG(0, (L"PROCTL: 0x%08X\r\n", INREG32(&g_pUSDHCReg->PROCTL)));
    OALMSG(0, (L"SYSCTL: 0x%08X\r\n", INREG32(&g_pUSDHCReg->SYSCTL)));
    OALMSG(0, (L"IRQSTAT: 0x%08X\r\n", INREG32(&g_pUSDHCReg->IRQSTAT)));
    OALMSG(0, (L"IRQSTATEN: 0x%08X\r\n", INREG32(&g_pUSDHCReg->IRQSTATEN)));
    OALMSG(0, (L"IRQSIGEN: 0x%08X\r\n", INREG32(&g_pUSDHCReg->IRQSIGEN)));
    OALMSG(0, (L"AUTOC12ERR: 0x%08X\r\n", INREG32(&g_pUSDHCReg->AUTOC12ERR)));
    OALMSG(0, (L"HOSTCAPBLT: 0x%08X\r\n", INREG32(&g_pUSDHCReg->HOSTCAPBLT)));
    OALMSG(0, (L"WML: 0x%08X\r\n", INREG32(&g_pUSDHCReg->WML)));
    OALMSG(0, (L"MIXCTL: 0x%08X\r\n", INREG32(&g_pUSDHCReg->MIXCTL)));
    OALMSG(0, (L"FEVT: 0x%08X\r\n", INREG32(&g_pUSDHCReg->FEVT)));
    OALMSG(0, (L"DLLCTL: 0x%08X\r\n", INREG32(&g_pUSDHCReg->DLLCTL)));
    OALMSG(0, (L"DLLSTATUS: 0x%08X\r\n", INREG32(&g_pUSDHCReg->DLLSTATUS)));
    OALMSG(0, (L"VENDOR: 0x%08X\r\n", INREG32(&g_pUSDHCReg->VENDOR)));
    OALMSG(0, (L"MMCBOOT: 0x%08X\r\n", INREG32(&g_pUSDHCReg->MMCBOOT)));
    OALMSG(0, (L"HOSTVER: 0x%08X\r\n", INREG32(&g_pUSDHCReg->HOSTVER)));

    return; 
}

//------------------------------------------------------------------------------
//
// Function: SDHCWaitEndCmdRespIntr
//
//    Wait a END_CMD_RESP interrupt by polling status register. The fields are checked 
//    to determine if an error has occurred.
//
// Parameters:
//        None
//
// Returns:
//        USDHC_STATUS_PASS for success/USDHC_STATUS_FAILURE for failure
//
//------------------------------------------------------------------------------
static UINT32 SDHCWaitEndCmdRespIntr()
{
    UINT32 sdhc_status = ESDHC_STATUS_FAILURE;
    DWORD status = 0;
    
    OALMSG(OAL_FUNC, (_T("+ SDHCWaitEndCmdRespIntr\r\n")));

    while (!( (status = INREG32(&g_pUSDHCReg->IRQSTAT)) &  (CSP_BITFMASK(USDHC_IRQSTAT_CC) | USDHC_CMD_ERROR_BITS)));

    OALMSG(OAL_FUNC, (_T("SDHCWaitEndCmdRespIntr Status: %x\r\n"), status));
    /* Check whether the interrupt is an END_CMD_RESP  
     * or an error 
     */         
    if((status & CSP_BITFMASK(USDHC_IRQSTAT_CC)) &&  !(status & USDHC_CMD_ERROR_BITS) )
    {
         sdhc_status = ESDHC_STATUS_PASS;
    }
    else if (CSP_BITFEXT(INREG32(&g_pUSDHCReg->PRSSTAT), USDHC_PRSSTAT_CIHB) || CSP_BITFEXT(INREG32(&g_pUSDHCReg->PRSSTAT), USDHC_PRSSTAT_CDIHB))
    {
        // Reset the controller
        INSREG32BF(&g_pUSDHCReg->SYSCTL,USDHC_SYSCTL_RSTA, 1);
        // wait for reset to complete
        while (INREG32(&g_pUSDHCReg->SYSCTL) & CSP_BITFMASK(USDHC_SYSCTL_RSTA));
    }


    OALMSG(OAL_FUNC, (_T("- SDHCWaitEndCmdRespIntr\r\n")));
     
    return sdhc_status;
}

//------------------------------------------------------------------------------
//
// Function: SDHCCheckDataStatus
//
// Parameters:
//        None
//
// Returns:
//        USDHC_STATUS_PASS for success/USDHC_STATUS_FAILURE for failure
//
//------------------------------------------------------------------------------
static UINT32 SDHCCheckDataStatus ()
{
    UINT32 sdhc_status = INREG32 (&g_pUSDHCReg->IRQSTAT);

    OALMSG(OAL_FUNC, (_T("+ CheckDataStatus\r\n")));

    /* Check whether there is a data time out error, a data end bit error, or a CRC error  */
    if(sdhc_status & ESDHC_DAT_ERROR_BITS)
    {
        sdhc_status = ESDHC_STATUS_FAILURE;
    }
    else
    {
        sdhc_status = ESDHC_STATUS_PASS;
    }

    OALMSG(OAL_FUNC, (_T("- CheckDataStatus\r\n")));
    return sdhc_status;
}

//------------------------------------------------------------------------------
//
// Function: SDHCSendCmdWaitResp
//
//    Execute a command and wait for the response
//
// Parameters:
//        pReq[in] - SD structure
//
// Returns:
//        USDHC_STATUS_PASS for success/USDHC_STATUS_FAILURE for failure
//
//------------------------------------------------------------------------------
UINT32 SDHCSendCmdWaitResp(PSD_BUS_REQUEST pReq)
{
    UINT32 sdhc_status = ESDHC_STATUS_FAILURE;

    /* Configure XFERTYP register to send the command */
    SDHCCmdConfig(pReq);

    /* Wait for interrupt end_command_resp */
    sdhc_status = SDHCWaitEndCmdRespIntr();

    /* Check if an error occured */
    return sdhc_status;

}

//------------------------------------------------------------------------------
//
// Function: SDHCReadResponse
//
//    Read the response returned by the card after a command 
//
// Parameters:
//        pResp[in] - structure to fill the response
//
// Returns:
//        USDHC_STATUS_PASS for success/USDHC_STATUS_FAILURE for failure
//
//------------------------------------------------------------------------------
UINT32 SDHCReadResponse (PSD_COMMAND_RESPONSE pResp)
{
    UINT32 status = ESDHC_STATUS_FAILURE;
 
    // skip the CRC
    UNALIGNED PDWORD respBuff = (PDWORD)(pResp->ResponseBuffer + 1);
    DWORD dwRespTyp = pResp->ResponseType;
    
    if (dwRespTyp != NoResponse)
    {
         respBuff[0] = INREG32(&g_pUSDHCReg->CMDRSP0);

          if (dwRespTyp == ResponseR2)
          {
              respBuff[1] = INREG32(&g_pUSDHCReg->CMDRSP1);
              respBuff[2] = INREG32(&g_pUSDHCReg->CMDRSP2);
              respBuff[3] = INREG32(&g_pUSDHCReg->CMDRSP3);   
          }
    
          status = ESDHC_STATUS_PASS;
    }
    
    return status;

}

//------------------------------------------------------------------------------
//
// Function: SDHCDataRead
//
//    Read the data from the card.
//
// Parameters:
//        dest_ptr[out] - Destination memory address
//        transferSize[in] - number of bytes to transfer
//
// Returns:
//        USDHC_STATUS_PASS for success/USDHC_STATUS_FAILURE for failure
//
//------------------------------------------------------------------------------
UINT32 SDHCDataRead (UINT32* dest_ptr, UINT32 transferSize) 
{

    // transferSize MUST be multiple of read watermark level   
    DWORD dwBytesRead = 0;
    DWORD dwBurstLen = EXTREG32BF(&g_pUSDHCReg->WML, USDHC_WML_RDWML); 
    
    DWORD dwDwordsRemaining = 0;
    UINT32 status = ESDHC_STATUS_FAILURE;

    OALMSG(OAL_FUNC, (_T("+ SDHCDataRead\r\n")));

    // we will read out burst length of data until we read out transferSize number of bytes
    while(dwBytesRead < transferSize)
    {
        // wait for Data Buffer to be ready
        while(! (INREG32(&g_pUSDHCReg->IRQSTAT) & CSP_BITFMASK(USDHC_IRQSTAT_BRR)))
        {
            ;
        }

        dwDwordsRemaining = dwBurstLen;
        while(dwDwordsRemaining--)
        {
            *(dest_ptr++) = INREG32(&g_pUSDHCReg->DATPORT);
        }

        dwBytesRead += (dwBurstLen << 2);

        // clear the BRR bit
        OUTREG32(&g_pUSDHCReg->IRQSTAT, CSP_BITFMASK(USDHC_IRQSTAT_BRR));
    }

    // Wait for transfer complete operation interrupt
    while(! (INREG32(&g_pUSDHCReg->IRQSTAT) & CSP_BITFMASK(USDHC_IRQSTAT_TC)) );
        
    /* Check for status errors */    
    status = SDHCCheckDataStatus();

    OALMSG(OAL_FUNC, (_T("- SDHCDataRead\r\n")));
    
    return status;
    
}

//------------------------------------------------------------------------------
//
// Function: SDHCReset
//
//    Reset SD host controller both data line and command line
//
// Parameters:
//
// Returns:
//
//------------------------------------------------------------------------------
void SDHCReset()
{
    DWORD dwProctlReg = INREG32(&g_pUSDHCReg->PROCTL);
    DWORD dwIrqStatEnReg = INREG32(&g_pUSDHCReg->IRQSTATEN);
    DWORD dwIrqSigEnReg = INREG32(&g_pUSDHCReg->IRQSIGEN);

    INSREG32BF(&g_pUSDHCReg->SYSCTL, USDHC_SYSCTL_RSTC, 1);
    INSREG32BF(&g_pUSDHCReg->SYSCTL, USDHC_SYSCTL_RSTD, 1);

    OUTREG32(&g_pUSDHCReg->PROCTL, dwProctlReg & (~CSP_BITFMASK(USDHC_PROCTL_DMAS)));
    OUTREG32(&g_pUSDHCReg->IRQSTATEN, dwIrqStatEnReg);
    OUTREG32(&g_pUSDHCReg->IRQSIGEN, dwIrqSigEnReg);
}

//------------------------------------------------------------------------------
//
// Function: SDHCDataWrite
//
//    Read the data from the card
//
// Parameters:
//        src_ptr[out] - Source memory address
//        transferSize[in] - number of bytes to transfer
//
// Returns:
//        USDHC_STATUS_PASS for success/USDHC_STATUS_FAILURE for failure
//
//------------------------------------------------------------------------------
UINT32 SDHCDataWrite (UINT32* src_ptr, UINT32 transferSize) 
{
    // transferSize MUST be multiple of write watermark level   
    DWORD dwBytesWritten = 0;
    DWORD dwBurstLen = EXTREG32BF(&g_pUSDHCReg->WML, USDHC_WML_WRWML);

    DWORD dwDwordsRemaining = 0;
    UINT32 status = ESDHC_STATUS_FAILURE;

    OALMSG(OAL_FUNC, (_T("+ SDHCDataWrite\r\n")));

    // we will read out burst length of data until we write out transferSize number of bytes
    while(dwBytesWritten < transferSize)
    {
        // wait for Data Buffer to be ready
        while(!(INREG32(&g_pUSDHCReg->IRQSTAT) & CSP_BITFMASK(USDHC_IRQSTAT_BWR)));

        dwDwordsRemaining = dwBurstLen;
        while(dwDwordsRemaining--)
        {
            OUTREG32(&g_pUSDHCReg->DATPORT, *(src_ptr++));
        }

        dwBytesWritten += (dwBurstLen << 2);

        // clear the BWR bit
        OUTREG32(&g_pUSDHCReg->IRQSTAT, CSP_BITFMASK(USDHC_IRQSTAT_BWR));
    }

    // Wait for transfer complete operation interrupt
    while(!(INREG32(&g_pUSDHCReg->IRQSTAT) & CSP_BITFMASK(USDHC_IRQSTAT_TC)));
        
    /* Check for status errors */    
    status = SDHCCheckDataStatus();
    
    OALMSG(OAL_FUNC, (_T("- SDHCDataWrite\r\n")));
    
    return status;
}

//------------------------------------------------------------------------------
//
// Function: BSP_MMC8BitSupported
//
//      Whether the MMC 8bit mode supported or not
//
// Parameters:
//      None.
//
// Returns:
//      TRUE for MMC 8bit supported, FALSE for MMC 8bit not supported
//
//------------------------------------------------------------------------------
BOOL BSP_MMC8BitSupported()
{
    switch(g_SDSlotIndex)
    {
        case 3:
			return TRUE;
        default:
            break;
    }
    return FALSE;
}

//------------------------------------------------------------------------------
//
// Function: BSP_MMCDDRSupported
//
//      Whether the MMC DDR mode supported or not
//
// Parameters:
//      None.
//
// Returns:
//      TRUE for MMC DDR supported, FALSE for MMC DDR not supported
//
//------------------------------------------------------------------------------
BOOL BSP_MMCDDRSupported()
{
    switch(g_SDSlotIndex)
    {
        default:
            break;
    }
    return FALSE;
}

//------------------------------------------------------------------------------
//
// Function: BSP_MMC4BitSupported
//
//      Whether the MMC 4bit mode supported or not
//
// Parameters:
//      None.
//
// Returns:
//      TRUE for MMC 4bit supported, FALSE for MMC 4bit not supported
//
//------------------------------------------------------------------------------
BOOL BSP_MMC4BitSupported()
{
    return TRUE;
}


//------------------------------------------------------------------------------
//
// Function: BSP_GetSDImageInfo
//
//    Get the image parameters
//
// Parameters:
//        pSDImageContext[out] - image parameters
//
// Returns:
//        the image parameters
//
//------------------------------------------------------------------------------
void BSP_GetSDImageCfg(PSD_IMAGE_CFG pSDImageCfg)
{
    //In MX6Q sd boot, there is no xldr, we use eboot parameter to get correct offset
    pSDImageCfg->dwXldrOffset = IMAGE_BOOT_BOOTIMAGE_SD_OFFSET;
    pSDImageCfg->dwXldrSize = IMAGE_BOOT_BOOTIMAGE_SD_SIZE;

    pSDImageCfg->dwBootOffset = IMAGE_BOOT_BOOTIMAGE_SD_OFFSET;
    pSDImageCfg->dwBootSize = IMAGE_BOOT_BOOTIMAGE_SD_SIZE;

    pSDImageCfg->dwNkOffset = IMAGE_BOOT_NKIMAGE_SD_OFFSET;
    pSDImageCfg->dwNkSize = IMAGE_BOOT_NKIMAGE_SD_SIZE;

    pSDImageCfg->dwBoot2Offset = IMAGE_BOOT_BOOTIMAGE2_SD_OFFSET;
    pSDImageCfg->dwBoot2Size = IMAGE_BOOT_BOOTIMAGE2_SD_SIZE;
    
    pSDImageCfg->dwCfgOffset = IMAGE_BOOT_BOOTCFG_SD_OFFSET;
    pSDImageCfg->dwCfgSize = IMAGE_BOOT_BOOTCFG_SD_SIZE;

    pSDImageCfg->dwNkRAMOffset = IMAGE_BOOT_NKIMAGE_RAM_PA_START;
    pSDImageCfg->dwSdSize = IMAGE_BOOT_SDHCDEV_SD_SIZE;

    pSDImageCfg->dwSocId = 63;
}

//------------------------------------------------------------------------------
//
// Function: BSP_SDHCSupportHSMode
//
//      Whether the SDHC controller supports HS mode
//
// Parameters:
//      None.
//
// Returns:
//      TRUE for HS mode supported, FALSE for HS mode not supported
//
//------------------------------------------------------------------------------
BOOL BSP_SDHCSupportHSMode()
{
    DWORD dwHSS = CSP_BITFEXT(INREG32(&g_pUSDHCReg->HOSTCAPBLT), USDHC_HOSTCAPBLT_HSS);
    OALMSG(OAL_FUNC, (L"HSS: %d\r\n", dwHSS));
    return dwHSS?TRUE:FALSE;
}
