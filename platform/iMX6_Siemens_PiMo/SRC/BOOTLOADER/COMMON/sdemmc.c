//------------------------------------------------------------------------------
//
//  Copyright (C) 2012, Adeneo Embedded All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//  Copyright (C) 2007-2011 Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//
//  File:  sdemmc.c
//
//  Additional "secure image load from SD/eMMC" routine for bootloader
//
//-----------------------------------------------------------------------------

#include "sdfmd.h"
#include "bsp.h"

//-----------------------------------------------------------------------------
// External Functions

extern SD_IMAGE_CFG SDImageCfg;
extern BOOL SDHCLoadNK(VOID);
extern UINT32 CRC32(UINT32 crc, const BYTE* buf, size_t len);
extern void BSP_GetSDImageCfg(PSD_IMAGE_CFG pSDImageCfg);



#include <oal_cache.h>
#include <args.h>
#include <bsp_bootline.h>

extern VOID OALCachePL310GlobalsInit(UINT32 baseAddr);
extern VOID OALCacheInfoDisplay(BOOL bEnable);

void SetBootMode(BOOL bIsRecovery)
{
	BSP_ARGS * pArgs;
	pArgs = (BSP_ARGS*) IMAGE_SHARE_ARGS_UA_START;
	if(bIsRecovery)
	{
		pArgs->recoveryMode = BOOTLINE_RECOVERY;
	}
	else
	{
		pArgs->recoveryMode = BOOTLINE_OPERATIVE;
	}
}

//------------------------------------------------------------------------------
//
//  Function:  SDHCLoadNKSecure
//
//  This function load NK.nb0 image from SD card and check for image corruption.
//	It computes a CRC of NK.nb0 and compairs it with CRC appended at the end of 
//	image at build time.
//
//	The first image to be loaded is the "operating image" at address
//	If corruption check fails, a second, "recovery image" is loaded and checked.
//	If both images are corrupt, no NK.nb0 is loaded.
//  This function is called by OEMPreDownload
//
//  Parameters:        
//      None.
//
//  Returns:
//		TRUE if either operating or recovery kernel has been loaded and checked.
//		FALSE otherwise. 
//------------------------------------------------------------------------------

BOOL SDHCLoadNKSecure(VOID)
{
	LPBYTE	pSectorBuf;
	DWORD	dwImageSize;
	DWORD	dwComputedCRC = 0;
	PDWORD pdwStoredCRC;

	/*Load operating NK*/
	BSP_GetSDImageCfg(&SDImageCfg);
	pSectorBuf				= (LPBYTE)OALPAtoCA(SDImageCfg.dwNkRAMOffset);
	dwImageSize				= (DWORD)SDImageCfg.dwNkSize-4;
	pdwStoredCRC			= (DWORD*)(pSectorBuf+dwImageSize);
	SDImageCfg.dwNkOffset	= IMAGE_BOOT_NKIMAGE_OPE_SD_OFFSET;

	OALCacheGlobalsInit();
	OALCachePL310GlobalsInit(CSP_BASE_REG_PA_L2CC);
	OALCacheInfoDisplay(TRUE);
	

	if(SDHCLoadNK()==TRUE)
	{
		OALMSG(1, (_T("NK Loaded. at 0x%08x Now computing CRC...\r\n"), pSectorBuf));
		dwComputedCRC = CRC32(0,pSectorBuf, dwImageSize);
		if(dwComputedCRC == (*pdwStoredCRC))
		{
			
			OALMSG(1, (_T("INFO: Operating Kernel CRC OK\r\n")));
			OALMSG(1, (_T("INFO: Computed : %08x Stored:%08x\r\n"), dwComputedCRC, *pdwStoredCRC));
			*pdwStoredCRC=0;
			/* WARNING : Set boot path variable to "operating kernel"*/
			SetBootMode(FALSE);
			return TRUE;
		}
	}

	/*If operating Kernel CRC fail, load recovery NK image*/
	OALMSG(1, (_T("INFO: Operational Kernel CRC error. Loading Recovery Kernel\r\n")));
	SDImageCfg.dwNkOffset = IMAGE_BOOT_NKIMAGE_REC_SD_OFFSET;
	if(SDHCLoadNK()==TRUE)
	{
		OALMSG(1, (_T("NK Loaded. Now computing CRC...\r\n")));
		dwComputedCRC = CRC32(0,pSectorBuf, dwImageSize);
		if(dwComputedCRC == (*pdwStoredCRC))
		{
			
			OALMSG(1, (_T("INFO: Recovery Kernel CRC OK\r\n")));
			OALMSG(1, (_T("INFO: Computed : %08x Stored:%08x\r\n"), dwComputedCRC, *pdwStoredCRC));
			*pdwStoredCRC=0;
			/* WARNING : Set boot path variable to "recovery"*/
			SetBootMode(TRUE);
			return TRUE;
		}
	}

	OALMSG(1, (_T("ERROR: No valild NK.nb0 image present on device. Computed : %08x Stored : %08x\r\n"),
		dwComputedCRC, *pdwStoredCRC));
	return FALSE;
}