//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2010, Freescale Semiconductor, Inc. All Rights Reserved.
// THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
// AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
 
//------------------------------------------------------------------------------
// File:      
//     USBClock.c
// Purpose:   
//     Maintains shared access to USB peripheral block clock gating
// Functions: 
//     USBClockDisable(BOOL fEnabled)
//         Either enable or disable the USB clock, for this user.  Only if no 
//         other user requires the clock will it actually be gated off.
//
//------------------------------------------------------------------------------
#pragma warning(push)
#pragma warning(disable: 4115 4201 4204 4214)
#include <windows.h>
#include <Winbase.h>
#include <ceddk.h>
#pragma warning(pop)

#include "bsp.h"
#include "common_usbname.h"
#include "common_usbcommon.h"
#include "regsusbphy.h"


HANDLE m_hUSBClockGatingHandle = NULL;
BSP_USB_CLOCK_GATING  *pUSBClkGating = NULL;
BOOL BSPUSBClockSwitch(BOOL fOn);
extern WORD BSPGetUSBControllerType(void);

#define USBOH3_CLOCK_SIGNAL   DDK_CLOCK_SIGNAL_PLL2_BUS
#define USBOH3_CLOCK_SOURCE   DDK_CLOCK_BAUD_SOURCE_PLL2_BUS
#define USBPHY_CLOCK_SOURCE   DDK_CLOCK_BAUD_SOURCE_OSC
#define USBPHY_PREVID         1
#define USBPHY_POSTDIV        1

//------------------------------------------------------------------------------
// Function: USBClockSet
//
// Description: This function enables or disables the USB clock by calling the
//              DDK clock related interfaces. Configure the clock source for USB
//              as PER CLK and prediv = 0, postdiv = 4(The actual divider will 
//              be (0+1)*(4+1)= 5 ).
//
// Parameters: 
//     fEnabled
//         [IN] If TRUE, configure the boud rate for USB module and enable the 
//              clock for USB. If FALSE, disable the clock for USB module.
// Returns: 
//    TRUE - operation success 
//    FALSE - operation failed
//
//------------------------------------------------------------------------------
static BOOL USBClockSet(BOOL fEnabled)
{
    BOOL rc = FALSE;
    DDK_CLOCK_GATE_MODE curMode;
   

    if (fEnabled)
    {
  
        DDKClockGetGatingMode(DDK_CLOCK_GATE_INDEX_USBOH3, &curMode);
        if(curMode != DDK_CLOCK_GATE_MODE_ENABLED_ALL)
        {
            // for USBOH3
            if (!DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_USBOH3, DDK_CLOCK_GATE_MODE_ENABLED_ALL))
            {
                DEBUGMSG (ZONE_ERROR, (L"USBClockSet: DDKClockSetGatingMode for DDK_CLOCK_GATE_INDEX_USBOH3 failed\r\n"));
                goto cleanUp;
            }
        }
     
    }
    else
    {
        DDKClockGetGatingMode(DDK_CLOCK_GATE_INDEX_USBOH3, &curMode);
        if(curMode != DDK_CLOCK_GATE_MODE_DISABLED)
        {
            if (!DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_USBOH3, DDK_CLOCK_GATE_MODE_DISABLED))
            {
                DEBUGMSG (ZONE_ERROR, (L"USBClockSet: DDKClockSetGatingMode for DDK_CLOCK_GATE_INDEX_USBOH3 failed\r\n"));
                goto cleanUp;
            }
        }
  
    }

    rc = TRUE;
    DEBUGMSG(ZONE_FUNCTION, (TEXT("USBClock %s return %d\r\n"),
                    (fEnabled? TEXT("TRUE"): TEXT("FALSE")), rc));    

cleanUp:


    return rc;
}

//------------------------------------------------------------------------------
// Function: USBClockInit
//
// Description: This funtion initializes and starts the USB Core Clock
//
// Parameter:
//     NULL.
//
// Return: 
//     TRUE - success to start the clock 
//     FALSE - fail to start the clock
//
//------------------------------------------------------------------------------
BOOL USBClockInit(void)
{    
    return BSPUSBClockSwitch(TRUE);
    // USBClockSet(TRUE);
}

//------------------------------------------------------------------------------
// Function: USBClockGatingLock
//
// Descriptions: Use the parameter ClockGatingLock as a critical section
//               in controlling the access within the Lock/Unlock for multiple
//               USB drivers
//
// Parameters: 
//     NULL
//
// Returns:
//     NULL
//
//------------------------------------------------------------------------------
void USBClockGatingLock(void)
{
    do
    {
        // Wait until lock is released
        while (*((volatile UINT32 *)(&pUSBClkGating->ClockGatingLock)))
        {
            Sleep(1);
        }
    } while (InterlockedTestExchange((LPLONG)&pUSBClkGating->ClockGatingLock, FALSE, TRUE) != FALSE);

}

//------------------------------------------------------------------------------
// Function: USBClockGatingUnlock
//
// Description: This function uses the parameter ClockGatingLock as a critical 
//              section in controlling the access within the Lock/Unlock for 
//              multiple USB drivers
//
// Parameter: 
//     NULL
//
// Returns:
//     NULL
//
//------------------------------------------------------------------------------
void USBClockGatingUnlock(void)
{
    pUSBClkGating->ClockGatingLock = FALSE;
}

//------------------------------------------------------------------------------
// Function: BSPUSBClockCreateFileMapping
//
// Description: This fuction is used to create the shared memory to be used
//              for controlling the stop/start of USB clock when multiple
//              USB controllers are running
// 
// Parameter: 
//     NULL
//
// Returns: 
//     NULL
//
//------------------------------------------------------------------------------
BOOL BSPUSBClockCreateFileMapping(void)
{
#ifdef DEBUG // Remove-W4: Warning C4189 workaround
    WORD sel = BSPGetUSBControllerType();
#else
    BSPGetUSBControllerType();
#endif
    DEBUGMSG(ZONE_FUNCTION, (TEXT("Port(%d):BSPUSBCreateFileMapping\r\n"), sel));
    if (m_hUSBClockGatingHandle == NULL)
    {
        m_hUSBClockGatingHandle = CreateFileMapping(INVALID_HANDLE_VALUE, NULL,
            PAGE_READWRITE, 0, sizeof(BSP_USB_CLOCK_GATING), USBClockGatingName);

        if (m_hUSBClockGatingHandle == NULL)
        {
            DEBUGMSG (ZONE_ERROR, (TEXT("Failure to Create File Mapping for USB\r\n")));
            return FALSE;
        }

        if (GetLastError() != ERROR_ALREADY_EXISTS) // you are the first one to create it
        {
            DEBUGMSG(ZONE_FUNCTION, (TEXT("Port(%d):First to create USB_CLOCK_GATING\r\n"), sel));
            pUSBClkGating = (BSP_USB_CLOCK_GATING *) MapViewOfFile(m_hUSBClockGatingHandle, 
                    FILE_MAP_ALL_ACCESS, 0, 0, 0);
            pUSBClkGating->ClockGatingMask = 0;
            pUSBClkGating->ClockGatingLock = FALSE;
        }
        else
        {
            DEBUGMSG(ZONE_FUNCTION, (TEXT("Port(%d) Open existing USB_CLOCK_GATING\r\n"), sel));
            pUSBClkGating = (BSP_USB_CLOCK_GATING *) MapViewOfFile(m_hUSBClockGatingHandle, 
                    FILE_MAP_ALL_ACCESS, 0, 0, 0);
        }
        
        DEBUGMSG(ZONE_FUNCTION, (TEXT("Port(%d):BSPUSBClockCreateFileMapping create success\r\n"), sel));
        return TRUE;
    }

    DEBUGMSG(ZONE_FUNCTION, (TEXT("-Port(%d):BSPUSBClockCreateFileMapping handle exist\r\n"), sel));
    return TRUE;
}

//------------------------------------------------------------------------------
// Function: BSPUSBClockDeleteFileMapping
//
// Description: This function is used to delete the shared memory to be used
//              for controlling the stop/start of USB clock when multiple
//              USB controllers are running
//
// Parameters: 
//     NULL
//
// Returns: 
//     NULL
//
//------------------------------------------------------------------------------
void BSPUSBClockDeleteFileMapping(void)
{
    if (pUSBClkGating)
    {
        UnmapViewOfFile(pUSBClkGating);
        pUSBClkGating = NULL;
    }

    if (m_hUSBClockGatingHandle)
    {
        CloseHandle(m_hUSBClockGatingHandle);
        m_hUSBClockGatingHandle = NULL;
    }

}


//------------------------------------------------------------------------------
// Function: BSPUSBClockSwitch
//
// Description: This function is used to control multiple USB controllers from 
//              accessing the same USB clock. ClockGatingMask is used to control
//              2 USB controllers (OTG and Host 2). If stop is request, it would
//              check against the ClockGatingMask and make sure it is 0 before 
//              it actually stop the clock.
//              On other hands, whenever a start is request, it would start
//              the USB clock right away.
//              ClockGatingMask: Bit 0 => OTG, Bit 1 => Host 2
//
// Parameters: 
//     fOn
//         [IN] TRUE means to start the USB clock, FALSE means to stop the USB
//              clock
//
// Returns:
//    TRUE: Success
//    FALSE: FAilure
//
//------------------------------------------------------------------------------
BOOL BSPUSBClockSwitch(BOOL fOn)
{ 
    DWORD dwMask = 0x0;
    BOOL fOK = FALSE;

    WORD sel = BSPGetUSBControllerType();

    {

    PHYSICAL_ADDRESS phyAddr;
    PVOID pv_HWregUSBPhy0;
    PVOID pv_HWregUSBPhy1;
    
    phyAddr.QuadPart =CSP_BASE_REG_PA_OTGPHY;
    pv_HWregUSBPhy0 = (PVOID) MmMapIoSpace(phyAddr, 0x1000, FALSE);
    phyAddr.QuadPart = CSP_BASE_REG_PA_UH1PHY;
    pv_HWregUSBPhy1 = (PVOID) MmMapIoSpace(phyAddr, 0x1000, FALSE);

    if (fOn)
    { 
        if(sel==USB_SEL_OTG)
            HW_USBPHY_CTRL_CLR(0,BM_USBPHY_CTRL_CLKGATE);
        else if(sel==USB_SEL_H1)
            HW_USBPHY_CTRL_CLR(1,BM_USBPHY_CTRL_CLKGATE);
    }else
    {
        if(sel==USB_SEL_OTG)
            HW_USBPHY_CTRL_SET(0,BM_USBPHY_CTRL_CLKGATE);
        else if(sel==USB_SEL_H1)
            HW_USBPHY_CTRL_SET(1,BM_USBPHY_CTRL_CLKGATE);
    }

    MmUnmapIoSpace(pv_HWregUSBPhy0,0x1000);
    MmUnmapIoSpace(pv_HWregUSBPhy1,0x1000);
    }

    // Since only OTG port support client mode.
    dwMask = 0x1 << sel;

    BSPUSBClockCreateFileMapping();

    USBClockGatingLock();
    if (fOn == FALSE) // close the clock
    {
        pUSBClkGating->ClockGatingMask &= ~dwMask;       
        fOK = ((pUSBClkGating->ClockGatingMask == 0)? TRUE: FALSE);
        if (fOK)
            fOK = USBClockSet(FALSE);
    }
    else // open the clock
    {
        pUSBClkGating->ClockGatingMask |= dwMask;
        fOK = TRUE;
        fOK = USBClockSet(TRUE);
    }
    USBClockGatingUnlock();

    RETAILMSG(1, (TEXT("Port(%d) - USBClockCanClockGating(%d), return 0x%x, value 0x%x\r\n"),
                     sel, fOn, fOK, pUSBClkGating->ClockGatingMask));
    return fOK;    
}
