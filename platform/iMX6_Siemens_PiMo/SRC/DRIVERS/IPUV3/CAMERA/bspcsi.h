//
//------------------------------------------------------------------------------
//
//  Copyright (C) 2008-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  bspcsi.h
//
//  Provides prototypes for BSP-specific CSI routines to communicate via I2C.
//
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// Defines
#define CAMAPP_FOR_RITA 1

#define CSI_DEBUG_MSG 0
//
// Supported camera
//     
#define CSI_SENSOR_NUMBER_SUPPORTED      9 // 0~3 for reserved sensor, 4~5 for reserved tvin


// CSI Registry Path
#define CSI_CAMERA1_REG_PATH                        "Drivers\\BuiltIn\\Camera1"
#define CSI_CAMERA2_REG_PATH                        "Drivers\\BuiltIn\\Camera2"
#define CSI_REG_CAMERAID_KEYWORD                    "CameraId"

#define FSL_FILTER_EVENT                            L"Freescale codec filter"
//------------------------------------------------------------------------------
// Types
// CAPTURE and STILL are required pin types. PREVIEW is optional, so list it last.
enum
{
    ADAPTER = -1,
    CAPTURE = 0,
    STILL   = 1,
    PREVIEW = 2
};

typedef enum csiSensorId {
    csiSensorId_OV3640 = 0,
	csiSensorId_OV5640 = 1,
    csiSensorId_OV5642 = 2,
    csiTVinId_ADV7180 = 4,
    csiTestMode = 9
}csiSensorId_c;

typedef enum SensorOutPutFrameRateEnum
{
    SensorOutputFrameRate15fps = 15,
    SensorOutputFrameRate30fps = 30,
} SensorOutputFrameRate;

struct reg_value {
	UINT16 u16RegAddr;
	BYTE u8Val;
	BYTE u8Mask;
	UINT32 u32Delay_ms;
};


//------------------------------------------------------------------------------
// Functions

BOOL I2CWriteOneByte(HANDLE hI2C, BYTE byAddr, WORD byReg, BYTE byData, LPINT lpiResult);
BOOL I2CWriteOneByte8bitAddr(HANDLE hI2C, BYTE byAddr, BYTE byReg, BYTE byData, LPINT lpiResult);
BYTE I2CReadOneByte(HANDLE hI2C, BYTE byAddr, WORD byReg, LPINT lpiResult);
BYTE I2CReadOneByte8bitAddr(HANDLE hI2C, BYTE byAddr, BYTE byReg, LPINT lpiResult);

int I2CWriteRegs(HANDLE hI2C, BYTE slaveAddr, struct reg_value *pModeSetting, int ArySize);
