//------------------------------------------------------------------------------
//
// Copyright (C) 2008-2011 Freescale Semiconductor, Inc. All Rights Reserved.
// THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
// AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT 
//
//------------------------------------------------------------------------------
//
//  File:  Camera OV5642.cpp    
//
//  Definitions for OminVisionChip camera module specific
//
//------------------------------------------------------------------------------
#pragma warning(disable: 4201 6244)

#include <windows.h>
#include <winddi.h>
#include <ddgpe.h>
#define __DVP_INCLUDED__

#include "Cs.h"
#include "Csmedia.h"
#include "SensorFormats.h"

#include "common_macros.h"
#include "common_ipuv3h.h"
#include "ipu_common.h"

#include "csp.h"
#include "i2cbus.h"
#include "Csi.h"
#include "CsiClass.h"
#include "bspcsi.h"
#include "CameraOV5642.h"
#include "cameradbg.h"


//------------------------------------------------------------------------------
// External Functions


//------------------------------------------------------------------------------
// External Variables
extern HANDLE      g_hI2C;


//------------------------------------------------------------------------------
// Defines
#define CSI_I2C_ADDRESS   0x20

#define OV5642_I2C_ADDRESS        0x3c
#define OV5642_I2C_SPEED          400000

#define CAMERA_FUNCTION_ENTRY() \
    DEBUGMSG(ZONE_FUNCTION, (TEXT("++%s\r\n"), __WFUNCTION__))
#define CAMERA_FUNCTION_EXIT() \
    DEBUGMSG(ZONE_FUNCTION, (TEXT("--%s\r\n"), __WFUNCTION__)) 

//------------------------------------------------------------------------------
// Types


//------------------------------------------------------------------------------
// Global Variables


//------------------------------------------------------------------------------
// Local Variables
MAKE_STREAM_MODE_RGB565(DCAM_OV5642_StreamMode_RGB565_QVGA, 320, -240, 16, 15);  //QVGA
MAKE_STREAM_MODE_RGB565(DCAM_OV5642_StreamMode_RGB565_VGA, 640, -480, 16, 15);  //VGA
MAKE_STREAM_MODE_RGB565(DCAM_OV5642_StreamMode_RGB565_D1_PAL, 720, -576, 16, 15); //PAL
MAKE_STREAM_MODE_RGB565(DCAM_OV5642_StreamMode_RGB565_D1_NTSC, 720, -480, 16, 15);  //NTSC
MAKE_STREAM_MODE_RGB565(DCAM_OV5642_StreamMode_RGB565_720P, 1280, -720, 16, 15);  //720P
MAKE_STREAM_MODE_RGB565(DCAM_OV5642_StreamMode_RGB565_1080P, 1920, -1080, 16, 15);  //1080P
MAKE_STREAM_MODE_RGB565(DCAM_OV5642_StreamMode_RGB565_QSXGA, 2592, -1944, 16, 15);  //QSXGA

MAKE_STREAM_MODE_UYVY(DCAM_OV5642_StreamMode_UYVY_QVGA, 320, -240, 16, 15); //QVGA
MAKE_STREAM_MODE_UYVY(DCAM_OV5642_StreamMode_UYVY_VGA, 640, -480, 16, 15);  //VGA
MAKE_STREAM_MODE_UYVY(DCAM_OV5642_StreamMode_UYVY_D1_PAL, 720, -576, 16, 15); //PAL
MAKE_STREAM_MODE_UYVY(DCAM_OV5642_StreamMode_UYVY_D1_NTSC, 720, -480, 16, 15);  //NTSC
MAKE_STREAM_MODE_UYVY(DCAM_OV5642_StreamMode_UYVY_720P, 1280, -720, 16, 15);  //720P
MAKE_STREAM_MODE_UYVY(DCAM_OV5642_StreamMode_UYVY_1080P, 1920, -1080, 16, 15);  //1080P
MAKE_STREAM_MODE_UYVY(DCAM_OV5642_StreamMode_UYVY_QSXGA, 2592, -1944, 16, 15);  //QSXGA

MAKE_STREAM_MODE_YV12(DCAM_OV5642_StreamMode_YV12_QVGA, 320, -240, 12, 15);  //QVGA
MAKE_STREAM_MODE_YV12(DCAM_OV5642_StreamMode_YV12_VGA, 640, -480, 12, 15);  //VGA
MAKE_STREAM_MODE_YV12(DCAM_OV5642_StreamMode_YV12_D1_PAL, 720, -576, 12, 15); //PAL
MAKE_STREAM_MODE_YV12(DCAM_OV5642_StreamMode_YV12_D1_NTSC, 720, -480, 12, 15);  //NTSC
MAKE_STREAM_MODE_YV12(DCAM_OV5642_StreamMode_YV12_720P, 1280, -720, 12, 15);  //720P
MAKE_STREAM_MODE_YV12(DCAM_OV5642_StreamMode_YV12_1080P, 1920, -1080, 12, 15);  //1080P
MAKE_STREAM_MODE_YV12(DCAM_OV5642_StreamMode_YV12_QSXGA, 2592, -1944, 12, 15);  //QSXGA

MAKE_STREAM_MODE_NV12(DCAM_OV5642_StreamMode_NV12_QVGA, 320, -240, 12, 15);  //QVGA
MAKE_STREAM_MODE_NV12(DCAM_OV5642_StreamMode_NV12_VGA, 640, -480, 12, 15);  //VGA
MAKE_STREAM_MODE_NV12(DCAM_OV5642_StreamMode_NV12_D1_PAL, 720, -576, 12, 15); //PAL
MAKE_STREAM_MODE_NV12(DCAM_OV5642_StreamMode_NV12_D1_NTSC, 720, -480, 12, 15);  //NTSC
MAKE_STREAM_MODE_NV12(DCAM_OV5642_StreamMode_NV12_720P, 1280, -720, 12, 15);  //720P
MAKE_STREAM_MODE_NV12(DCAM_OV5642_StreamMode_NV12_1080P, 1920, -1080, 12, 15);  //1080P
MAKE_STREAM_MODE_NV12(DCAM_OV5642_StreamMode_NV12_QSXGA, 2592, -1944, 12, 15);  //QSXGA


//------------------------------------------------------------------------------
// Local Functions


//------------------------------------------------------------------------------
//
// Function: CameraOV5642Init
//
// Initializes the OV5642 camera sensor.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//      SensorFramerate
//          [in] Sensor frame rate
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL CameraOV5642Init(CSI_SELECT csi_sel, SensorOutputFrameRate SensorFramerate)
{
    DWORD dwFrequency;
    BYTE bySlaveAddr, byCsiAddr;
    INT  iResult;
    BYTE byRead;
    UINT uSensorVer;
    BOOL retVal = TRUE;
    
    CAMERA_FUNCTION_ENTRY();

	// Clear PWDN pin (power down)
    DDKGpioSetConfig(DDK_GPIO_PORT1, 16, DDK_GPIO_DIR_OUT, DDK_GPIO_INTR_NONE);
    DDKGpioWriteDataPin(DDK_GPIO_PORT1, 16, 0);

	// Set RESET_B (/reset)
    DDKGpioSetConfig(DDK_GPIO_PORT1, 17, DDK_GPIO_DIR_OUT, DDK_GPIO_INTR_NONE);
    DDKGpioWriteDataPin(DDK_GPIO_PORT1, 17, 1);
	Sleep(1); // Need to sleep after reset

    byCsiAddr = CSI_I2C_ADDRESS;

    switch (csi_sel)
    {
        case CSI_SELECT_CSI0:
			dwFrequency = OV5642_I2C_SPEED;
            bySlaveAddr = OV5642_I2C_ADDRESS;
            break;    

        default:
             DEBUGMSG(ZONE_ERROR, (TEXT("%s: Don't support!\r\n"), __WFUNCTION__));
             return FALSE;
    }        

    if(g_hI2C == NULL)
    {
        g_hI2C = I2COpenHandle(I2C1_FID);   
		
		if (g_hI2C == INVALID_HANDLE_VALUE)
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: CreateFile for I2C failed!\r\n"), __WFUNCTION__));
            return FALSE;
        }

        if (!I2CSetMasterMode(g_hI2C))
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: Set I2C Master mode failed!\r\n"), __WFUNCTION__));
            return FALSE;
        }

        // Initialize the device internal fields
        if (!I2CSetFrequency(g_hI2C, dwFrequency))
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: Set HSI2C frequency failed!\r\n"), __WFUNCTION__));
            return FALSE;
        }

        if (!I2CSetSelfAddr(g_hI2C, byCsiAddr))
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: Set I2C self address failed!\r\n"), __WFUNCTION__));
            return FALSE;
        }
    }

    // Check connected sensor type
    byRead = I2CReadOneByte(g_hI2C, bySlaveAddr, 0x300a, &iResult);
    uSensorVer = byRead << 8;
    byRead = I2CReadOneByte(g_hI2C, bySlaveAddr, 0x300b, &iResult);
    uSensorVer |= byRead;

    if(uSensorVer == 0x5642)
    {
        DEBUGMSG(TRUE,(TEXT("%s: Ov5642 sensor detected! \r\n"), __WFUNCTION__));
    }  
    else
    {
        ERRORMSG(TRUE,(TEXT("%s: The sensor type doesn't match! \r\n"), __WFUNCTION__));
        return FALSE;
    }

    switch(SensorFramerate)
    {
        case SensorOutputFrameRate30fps:
            retVal = CameraOV5642Set30fps(csi_sel);
            break;
        case SensorOutputFrameRate15fps:
            retVal = CameraOV5642Set15fps(csi_sel);
            break;

        default:
            ERRORMSG(TRUE,(TEXT("%s: Set error sensor output framerate!\r\n"), __WFUNCTION__));
            retVal = FALSE;
            break;
    }

    return retVal;
}


//------------------------------------------------------------------------------
//
// Function: CameraOV5642Deinit
//
// Deinitializes the OV5642 camera sensor.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL CameraOV5642Deinit(CSI_SELECT csi_sel)
{
    BOOL retVal = TRUE;

    CAMERA_FUNCTION_ENTRY();

    UNREFERENCED_PARAMETER(csi_sel);

    if(g_hI2C)
    {
        CloseHandle(g_hI2C);
        g_hI2C = NULL;
    }

    CAMERA_FUNCTION_EXIT();

    return retVal;
}


//------------------------------------------------------------------------------
//
// Function: CameraOV5642Set15fps
//
// Set sensor output frame rate is 15fps.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL CameraOV5642Set15fps(CSI_SELECT csi_sel)
{
    BYTE bySlaveAddr;
    BOOL retVal = TRUE;

    CAMERA_FUNCTION_ENTRY();
    switch (csi_sel)
    {
        case CSI_SELECT_CSI0:
             bySlaveAddr = OV5642_I2C_ADDRESS;
             break;               

        default:
             DEBUGMSG(ZONE_ERROR, (TEXT("%s: Don't support!\r\n"), __WFUNCTION__));
             return FALSE;
    }   
    
    // OV5642 15fps

    CAMERA_FUNCTION_EXIT();

    return retVal;
}

//------------------------------------------------------------------------------
//
// Function: CameraOV5642Set30fps
//
// Set sensor output frame rate is 30fps.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL CameraOV5642Set30fps(CSI_SELECT csi_sel)
{
    BYTE bySlaveAddr;
    BOOL retVal = TRUE;

    CAMERA_FUNCTION_ENTRY();

    switch (csi_sel)
    {
        case CSI_SELECT_CSI0:
             bySlaveAddr = OV5642_I2C_ADDRESS;
             break;               

        default:
             DEBUGMSG(ZONE_ERROR, (TEXT("%s: Don't support!\r\n"), __WFUNCTION__));
             return FALSE;
    }  
    
    CAMERA_FUNCTION_EXIT();

    return retVal;
}

//------------------------------------------------------------------------------
//
// Function: CameraOV5642SetOutputFormat
//
// Set interface to configure camera output format.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//      SensorFramerate
//          [in] Sensor frame rate
//      outputFormat
//          [in] structure indicating the sensor output format:
//              csiSensorOutputFormat_RGB888 - RGB888
//              csiSensorOutputFormat_RGB565 - RGB565
//              csiSensorOutputFormat_YUV422_UYVY - UYVY
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL CameraOV5642SetOutputFormat(CSI_SELECT csi_sel, SensorOutputFrameRate SensorFramerate, csiSensorOutputFormat outputFormat)
{
    INT iResult;
    BYTE bySlaveAddr;
    BYTE byRead;
    BOOL retVal = TRUE;

    CAMERA_FUNCTION_ENTRY();

    switch (csi_sel)
    {
        case CSI_SELECT_CSI0:
             bySlaveAddr = OV5642_I2C_ADDRESS;
             break;              

        default:
             DEBUGMSG(ZONE_ERROR, (TEXT("%s: Don't support!\r\n"), __WFUNCTION__));
             return FALSE;
    }      

    switch (outputFormat)
    {
        case csiSensorOutputFormat_RGB888:
        {
            switch(SensorFramerate)
            {
                case SensorOutputFrameRate30fps:
                    retVal = FALSE;
                break;

                case SensorOutputFrameRate15fps:
                    retVal = FALSE;
                break;
            }
        }
        break;

        case csiSensorOutputFormat_RGB565:  
        {
            switch(SensorFramerate)
            {
                case SensorOutputFrameRate30fps:
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460c, 0x20,&iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460b, 0x37,&iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471c, 0xd0,&iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3815, 0x01,&iResult);
                    byRead = I2CReadOneByte(g_hI2C, bySlaveAddr, 0x3818, &iResult);
                    byRead = byRead & ((BYTE)(~0x08)) | 0x00;
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3818, byRead, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x501f, 0x01,&iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4300, 0x61,&iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3002, 0x1c,&iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3819, 0x80,&iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5002, 0xf8,&iResult);
                    break;

                case SensorOutputFrameRate15fps:
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460c, 0x20,&iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460b, 0x37,&iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471c, 0xd0,&iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3815, 0x01,&iResult);
                    byRead = I2CReadOneByte(g_hI2C, bySlaveAddr, 0x3818, &iResult);
                    byRead = byRead & ((BYTE)(~0x08)) | 0x00;
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3818, byRead, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x501f, 0x01,&iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4300, 0x61,&iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3002, 0x1c,&iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3819, 0x80,&iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5002, 0xf8,&iResult);
                    break;
            }
        }
        break;

        case csiSensorOutputFormat_YUV444:
        {
            switch(SensorFramerate)
            {
                case SensorOutputFrameRate30fps:
                    retVal = FALSE;
                break;

                case SensorOutputFrameRate15fps:
                    retVal = FALSE;
                break;
            }
        }
        break;

        case csiSensorOutputFormat_YUV422_UYVY:
        {
            switch(SensorFramerate)
            {
                case SensorOutputFrameRate30fps:
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460c, 0x20, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460b, 0x37, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471c, 0xd0, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471d, 0x05, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3815, 0x01, &iResult);
                    byRead = I2CReadOneByte(g_hI2C, bySlaveAddr, 0x3818, &iResult);
                    byRead = byRead & ((BYTE)(~0x08)) | 0x00;
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3818, byRead, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x501f, 0x00, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4300, 0x32, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3002, 0x1c, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3819, 0x80, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5002, 0xe0, &iResult);
                    break;

                case SensorOutputFrameRate15fps:
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460c, 0x20, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460b, 0x37, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471c, 0xd0, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471d, 0x05, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3815, 0x01, &iResult);
                    byRead = I2CReadOneByte(g_hI2C, bySlaveAddr, 0x3818, &iResult);
                    byRead = byRead & ((BYTE)(~0x08)) | 0x00;
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3818, byRead, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x501f, 0x00, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4300, 0x32, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3002, 0x1c, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3819, 0x80, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5002, 0xe0, &iResult);
                    break;
            }
        }
        break;

        case csiSensorOutputFormat_YUV422_YUYV:
        {
            switch(SensorFramerate)
            {
                case SensorOutputFrameRate30fps:
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460c, 0x20, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460b, 0x37, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471c, 0xd0, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471d, 0x05, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3815, 0x01, &iResult);
                    byRead = I2CReadOneByte(g_hI2C, bySlaveAddr, 0x3818, &iResult);
                    byRead = byRead & ((BYTE)(~0x08)) | 0x00;
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3818, byRead, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x501f, 0x00, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4300, 0x30, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3002, 0x1c, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3819, 0x80, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5002, 0xe0, &iResult);
                    break;

                case SensorOutputFrameRate15fps:
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460c, 0x20, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460b, 0x37, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471c, 0xd0, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471d, 0x05, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3815, 0x01, &iResult);
                    byRead = I2CReadOneByte(g_hI2C, bySlaveAddr, 0x3818, &iResult);
                    byRead = byRead & ((BYTE)(~0x08)) | 0x00;
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3818, byRead, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x501f, 0x00, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4300, 0x30, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3002, 0x1c, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3819, 0x80, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5002, 0xe0, &iResult);
                    break;
            }
        }
        break;

     default:
        DEBUGMSG(ZONE_ERROR, (TEXT("%s: We don't support this kind of format!\r\n"), __WFUNCTION__));
        retVal = FALSE;
        break;
    }

    CAMERA_FUNCTION_EXIT();

    return retVal;
}


//------------------------------------------------------------------------------
//
// Function: CameraOV5642SetOutputResolution
//
// Set interface to configure camera output resolution.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//      SensorFramerate
//          [in] Sensor frame rate
//      outputResolution
//          [in]  Structure indicating the camera output resolution:
//              csiSensorOutputResolution outputResolution
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL CameraOV5642SetOutputResolution(CSI_SELECT csi_sel, SensorOutputFrameRate SensorFramerate, csiSensorOutputResolution outputResolution)
{
    INT iResult;
    BYTE bySlaveAddr;
    BYTE byRead;
    BOOL retVal = TRUE;

    CAMERA_FUNCTION_ENTRY();

    switch (csi_sel)
    {
        case CSI_SELECT_CSI0:
             bySlaveAddr = OV5642_I2C_ADDRESS;
             break;               

        default:
             DEBUGMSG(ZONE_ERROR, (TEXT("%s: Don't support!\r\n"), __WFUNCTION__));
             return FALSE;
    }        

    switch (outputResolution)
    {
        
        case csiSensorOutputResolution_QSXGA:
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3103, 0x93, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3008, 0x82, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3017, 0x7f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3018, 0xfc, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3810, 0xc2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3615, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3000, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3001, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3002, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3003, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3004, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3030, 0x2b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3011, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3010, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3604, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3622, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3621, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3709, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4000, 0x21, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x401d, 0x22, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600, 0x54, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3605, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3606, 0x3f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3c01, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x300d, 0x22, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3623, 0x22, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5000, 0x4f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5020, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5181, 0x79, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5182, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5185, 0x22, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5197, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5500, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5504, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5505, 0x7f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5080, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x300e, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4610, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471d, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4708, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370c, 0xa0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3808, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3809, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380a, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380b, 0x98, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380c, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380d, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380e, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380f, 0xd0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5687, 0x94, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x501f, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5000, 0x4f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5001, 0xcf, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4300, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4300, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460b, 0x35, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471d, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3002, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3002, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4713, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471c, 0x50, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4721, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4402, 0x90, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460c, 0x22, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3815, 0x44, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3503, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3501, 0x73, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3502, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3818, 0xc8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3801, 0x88, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3824, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a00, 0x78, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1a, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a13, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a18, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a19, 0x7c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a08, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a09, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0a, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0b, 0xa0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350c, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350d, 0xd0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0d, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0e, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3500, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3501, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3502, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3503, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0f, 0x3c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a10, 0x32, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1b, 0x3c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1e, 0x32, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a11, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1f, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3030, 0x2b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a02, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a03, 0x7d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a04, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a14, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a15, 0x7d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a16, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a00, 0x78, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a08, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a09, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0a, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0b, 0xd0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0d, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0e, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4407, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5193, 0x70, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x589b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x589a, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x401e, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4001, 0x42, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x401c, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3825, 0xac, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3827, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528a, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528b, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528c, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528d, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528e, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528f, 0x28, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5290, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5292, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5293, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5294, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5295, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5296, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5297, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5298, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5299, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529b, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529d, 0x28, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529f, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5282, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5300, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5301, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5302, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5303, 0x7c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530d, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530e, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530f, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5310, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5311, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5308, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5309, 0x40, &iResult); 
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5304, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5305, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5306, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5307, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5314, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5315, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5319, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5316, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5317, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5318, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5380, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5381, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5382, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5383, 0x4e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5384, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5385, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5386, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5387, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5388, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5389, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538b, 0x31, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538d, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538f, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5390, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5391, 0xab, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5392, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5393, 0xa2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5394, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5480, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5481, 0x21, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5482, 0x36, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5483, 0x57, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5484, 0x65, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5485, 0x71, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5486, 0x7d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5487, 0x87, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5488, 0x91, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5489, 0x9a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548a, 0xaa, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548b, 0xb8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548c, 0xcd, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548d, 0xdd, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548e, 0xea, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548f, 0x1d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5490, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5491, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5492, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5493, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5494, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5495, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5496, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5497, 0xb8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5498, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5499, 0x86, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549a, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549b, 0x5b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549c, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549d, 0x3b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549e, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549f, 0x1c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a0, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a1, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a2, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a3, 0xed, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a4, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a5, 0xc5, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a6, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a7, 0xa5, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a8, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a9, 0x6c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54aa, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ab, 0x41, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ac, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ad, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ae, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54af, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b0, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b1, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b2, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b3, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b4, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b5, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b6, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b7, 0xdf, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5402, 0x3f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5403, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3406, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5180, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5181, 0x52, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5182, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5183, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5184, 0x25, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5185, 0x24, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5186, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5187, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5188, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5189, 0x7c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518a, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518b, 0xb2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518c, 0xb2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518d, 0x44, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518e, 0x3d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518f, 0x58, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5190, 0x46, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5191, 0xf8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5192, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5193, 0x70, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5194, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5195, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5196, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5197, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5198, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5199, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519a, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519c, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519d, 0x82, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5025, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0f, 0x38, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a10, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1b, 0x3a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1e, 0x2e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a11, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1f, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5688, 0xa6, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5689, 0x6a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568a, 0xea, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568b, 0xae, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568c, 0xa6, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568d, 0x6a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568e, 0x62, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568f, 0x26, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5583, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5584, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5580, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5000, 0xcf, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5800, 0x27, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5801, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5802, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5803, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5804, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5805, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5806, 0x1e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5807, 0x2f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5808, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5809, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580a, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580b, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580c, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580d, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580e, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580f, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5810, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5811, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5812, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5813, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5814, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5815, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5816, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5817, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5818, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5819, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581a, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581d, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581e, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581f, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5820, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5821, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5822, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5823, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5824, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5825, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5826, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5827, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5828, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5829, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582a, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582b, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582c, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582d, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582e, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582f, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5830, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5831, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5832, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5833, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5834, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5835, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5836, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5837, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5838, 0x32, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5839, 0x1f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583a, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583b, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583c, 0x17, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583d, 0x1e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583e, 0x26, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583f, 0x53, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5840, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5841, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5842, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5843, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5844, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5845, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5846, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5847, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5848, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5849, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584a, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584b, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584c, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584d, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584e, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584f, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5850, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5851, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5852, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5853, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5854, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5855, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5856, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5857, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5858, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5859, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585a, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585b, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585c, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585d, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585e, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585f, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5860, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5861, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5862, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5863, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5864, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5865, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5866, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5867, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5868, 0x17, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5869, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586a, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586b, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586c, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586d, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586e, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586f, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5870, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5871, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5872, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5873, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5874, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5875, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5876, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5877, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5878, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5879, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587a, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587b, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587c, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587d, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587e, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587f, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5880, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5881, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5882, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5883, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5884, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5885, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5886, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5887, 0x17, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3710, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3632, 0x51, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3702, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3703, 0xb2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3704, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370b, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370d, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3631, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3632, 0x52, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3606, 0x24, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3620, 0x96, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5785, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a13, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600, 0x52, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3604, 0x48, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3606, 0x1b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370d, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370f, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3709, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3823, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5007, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5009, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5011, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5013, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5086, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5087, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5088, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5089, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x302b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460c, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460b, 0x37, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471c, 0xd0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471d, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3815, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3818, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x501f, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4300, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3002, 0x1c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3819, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5002, 0xe0, &iResult);
            break;

        case csiSensorOutputResolution_1080P:
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3103, 0x93, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3008, 0x82, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3017, 0x7f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3018, 0xfc, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3810, 0xc2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3615, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3000, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3001, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3002, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3003, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3004, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3030, 0x2b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3011, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3010, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3604, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3622, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3621, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3709, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4000, 0x21, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x401d, 0x22, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600, 0x54, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3605, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3606, 0x3f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3c01, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x300d, 0x22, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3623, 0x22, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5000, 0x4f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5020, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5181, 0x79, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5182, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5185, 0x22, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5197, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5500, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5504, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5505, 0x7f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5080, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x300e, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4610, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471d, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4708, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370c, 0xa0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3808, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3809, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380a, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380b, 0x98, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380c, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380d, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380e, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380f, 0xd0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5687, 0x94, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x501f, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5000, 0x4f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5001, 0xcf, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4300, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4300, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460b, 0x35, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471d, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3002, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3002, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4713, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471c, 0x50, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4721, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4402, 0x90, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460c, 0x22, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3815, 0x44, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3503, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3501, 0x73, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3502, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3818, 0xc8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3801, 0x88, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3824, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a00, 0x78, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1a, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a13, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a18, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a19, 0x7c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a08, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a09, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0a, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0b, 0xa0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350c, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350d, 0xd0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0d, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0e, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3500, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3501, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3502, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3503, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0f, 0x3c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a10, 0x32, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1b, 0x3c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1e, 0x32, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a11, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1f, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3030, 0x2b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a02, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a03, 0x7d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a04, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a14, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a15, 0x7d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a16, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a00, 0x78, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a08, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a09, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0a, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0b, 0xd0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0d, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0e, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4407, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5193, 0x70, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x589b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x589a, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x401e, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4001, 0x42, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x401c, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3825, 0xac, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3827, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528a, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528b, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528c, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528d, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528e, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528f, 0x28, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5290, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5292, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5293, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5294, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5295, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5296, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5297, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5298, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5299, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529b, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529d, 0x28, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529f, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5282, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5300, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5301, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5302, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5303, 0x7c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530d, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530e, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530f, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5310, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5311, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5308, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5309, 0x40, &iResult); 
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5304, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5305, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5306, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5307, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5314, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5315, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5319, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5316, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5317, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5318, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5380, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5381, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5382, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5383, 0x4e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5384, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5385, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5386, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5387, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5388, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5389, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538b, 0x31, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538d, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538f, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5390, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5391, 0xab, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5392, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5393, 0xa2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5394, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5480, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5481, 0x21, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5482, 0x36, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5483, 0x57, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5484, 0x65, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5485, 0x71, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5486, 0x7d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5487, 0x87, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5488, 0x91, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5489, 0x9a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548a, 0xaa, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548b, 0xb8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548c, 0xcd, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548d, 0xdd, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548e, 0xea, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548f, 0x1d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5490, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5491, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5492, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5493, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5494, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5495, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5496, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5497, 0xb8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5498, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5499, 0x86, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549a, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549b, 0x5b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549c, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549d, 0x3b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549e, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549f, 0x1c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a0, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a1, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a2, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a3, 0xed, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a4, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a5, 0xc5, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a6, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a7, 0xa5, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a8, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a9, 0x6c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54aa, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ab, 0x41, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ac, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ad, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ae, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54af, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b0, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b1, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b2, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b3, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b4, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b5, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b6, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b7, 0xdf, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5402, 0x3f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5403, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3406, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5180, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5181, 0x52, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5182, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5183, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5184, 0x25, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5185, 0x24, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5186, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5187, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5188, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5189, 0x7c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518a, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518b, 0xb2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518c, 0xb2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518d, 0x44, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518e, 0x3d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518f, 0x58, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5190, 0x46, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5191, 0xf8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5192, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5193, 0x70, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5194, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5195, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5196, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5197, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5198, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5199, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519a, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519c, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519d, 0x82, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5025, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0f, 0x38, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a10, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1b, 0x3a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1e, 0x2e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a11, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1f, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5688, 0xa6, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5689, 0x6a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568a, 0xea, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568b, 0xae, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568c, 0xa6, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568d, 0x6a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568e, 0x62, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568f, 0x26, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5583, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5584, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5580, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5000, 0xcf, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5800, 0x27, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5801, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5802, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5803, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5804, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5805, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5806, 0x1e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5807, 0x2f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5808, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5809, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580a, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580b, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580c, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580d, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580e, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580f, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5810, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5811, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5812, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5813, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5814, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5815, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5816, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5817, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5818, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5819, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581a, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581d, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581e, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581f, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5820, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5821, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5822, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5823, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5824, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5825, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5826, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5827, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5828, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5829, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582a, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582b, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582c, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582d, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582e, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582f, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5830, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5831, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5832, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5833, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5834, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5835, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5836, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5837, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5838, 0x32, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5839, 0x1f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583a, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583b, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583c, 0x17, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583d, 0x1e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583e, 0x26, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583f, 0x53, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5840, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5841, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5842, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5843, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5844, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5845, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5846, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5847, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5848, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5849, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584a, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584b, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584c, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584d, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584e, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584f, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5850, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5851, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5852, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5853, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5854, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5855, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5856, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5857, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5858, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5859, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585a, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585b, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585c, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585d, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585e, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585f, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5860, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5861, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5862, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5863, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5864, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5865, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5866, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5867, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5868, 0x17, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5869, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586a, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586b, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586c, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586d, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586e, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586f, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5870, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5871, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5872, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5873, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5874, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5875, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5876, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5877, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5878, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5879, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587a, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587b, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587c, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587d, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587e, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587f, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5880, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5881, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5882, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5883, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5884, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5885, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5886, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5887, 0x17, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3710, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3632, 0x51, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3702, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3703, 0xb2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3704, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370b, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370d, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3631, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3632, 0x52, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3606, 0x24, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3620, 0x96, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5785, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a13, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600, 0x52, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3604, 0x48, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3606, 0x1b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370d, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370f, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3709, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3823, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5007, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5009, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5011, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5013, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5086, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5087, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5088, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5089, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x302b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3503, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3011, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350c, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350d, 0x58, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3801, 0x8a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3803, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3804, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3805, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3806, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3807, 0x39, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3808, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3809, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380a, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380b, 0x38, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380c, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380d, 0xd6, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380e, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380f, 0x58, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x381c, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x381d, 0xba, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x381e, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x381f, 0x48, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3820, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3821, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a08, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a09, 0xe0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0a, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0b, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0d, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0e, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5682, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5683, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5686, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5687, 0x1c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5001, 0x7f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3503, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3010, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460c, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460b, 0x37, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471c, 0xd0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471d, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3815, 0x01, &iResult);
            byRead = I2CReadOneByte(g_hI2C, bySlaveAddr, 0x3818, &iResult);
            byRead = byRead & ((BYTE)(~0x08)) | 0x00;
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3818, byRead, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x501f, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4300, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3002, 0x1c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3819, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5002, 0xe0, &iResult);
            break;

        case csiSensorOutputResolution_720P:
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3103, 0x93, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3008, 0x82, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3017, 0x7f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3018, 0xfc, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3810, 0xc2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3615, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3000, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3001, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3002, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3003, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3004, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3030, 0x2b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3011, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3010, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3604, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3622, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3621, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3709, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4000, 0x21, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x401d, 0x22, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600, 0x54, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3605, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3606, 0x3f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3c01, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x300d, 0x22, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3623, 0x22, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5000, 0x4f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5020, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5181, 0x79, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5182, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5185, 0x22, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5197, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5500, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5504, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5505, 0x7f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5080, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x300e, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4610, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471d, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4708, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370c, 0xa0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3808, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3809, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380a, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380b, 0x98, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380c, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380d, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380e, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380f, 0xd0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5687, 0x94, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x501f, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5000, 0x4f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5001, 0xcf, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4300, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4300, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460b, 0x35, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471d, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3002, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3002, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4713, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471c, 0x50, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4721, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4402, 0x90, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460c, 0x22, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3815, 0x44, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3503, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3501, 0x73, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3502, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3818, 0xc8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3801, 0x88, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3824, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a00, 0x78, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1a, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a13, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a18, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a19, 0x7c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a08, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a09, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0a, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0b, 0xa0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350c, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350d, 0xd0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0d, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0e, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3500, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3501, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3502, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3503, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0f, 0x3c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a10, 0x32, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1b, 0x3c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1e, 0x32, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a11, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1f, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3030, 0x2b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a02, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a03, 0x7d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a04, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a14, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a15, 0x7d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a16, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a00, 0x78, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a08, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a09, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0a, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0b, 0xd0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0d, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0e, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4407, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5193, 0x70, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x589b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x589a, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x401e, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4001, 0x42, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x401c, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3825, 0xac, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3827, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528a, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528b, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528c, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528d, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528e, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528f, 0x28, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5290, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5292, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5293, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5294, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5295, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5296, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5297, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5298, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5299, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529b, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529d, 0x28, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529f, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5282, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5300, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5301, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5302, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5303, 0x7c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530d, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530e, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530f, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5310, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5311, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5308, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5309, 0x40, &iResult); 
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5304, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5305, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5306, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5307, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5314, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5315, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5319, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5316, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5317, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5318, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5380, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5381, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5382, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5383, 0x4e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5384, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5385, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5386, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5387, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5388, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5389, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538b, 0x31, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538d, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538f, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5390, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5391, 0xab, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5392, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5393, 0xa2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5394, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5480, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5481, 0x21, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5482, 0x36, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5483, 0x57, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5484, 0x65, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5485, 0x71, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5486, 0x7d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5487, 0x87, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5488, 0x91, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5489, 0x9a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548a, 0xaa, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548b, 0xb8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548c, 0xcd, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548d, 0xdd, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548e, 0xea, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548f, 0x1d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5490, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5491, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5492, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5493, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5494, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5495, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5496, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5497, 0xb8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5498, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5499, 0x86, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549a, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549b, 0x5b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549c, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549d, 0x3b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549e, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549f, 0x1c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a0, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a1, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a2, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a3, 0xed, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a4, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a5, 0xc5, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a6, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a7, 0xa5, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a8, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a9, 0x6c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54aa, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ab, 0x41, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ac, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ad, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ae, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54af, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b0, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b1, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b2, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b3, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b4, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b5, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b6, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b7, 0xdf, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5402, 0x3f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5403, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3406, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5180, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5181, 0x52, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5182, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5183, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5184, 0x25, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5185, 0x24, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5186, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5187, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5188, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5189, 0x7c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518a, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518b, 0xb2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518c, 0xb2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518d, 0x44, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518e, 0x3d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518f, 0x58, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5190, 0x46, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5191, 0xf8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5192, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5193, 0x70, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5194, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5195, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5196, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5197, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5198, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5199, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519a, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519c, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519d, 0x82, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5025, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0f, 0x38, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a10, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1b, 0x3a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1e, 0x2e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a11, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1f, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5688, 0xa6, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5689, 0x6a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568a, 0xea, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568b, 0xae, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568c, 0xa6, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568d, 0x6a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568e, 0x62, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568f, 0x26, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5583, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5584, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5580, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5000, 0xcf, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5800, 0x27, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5801, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5802, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5803, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5804, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5805, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5806, 0x1e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5807, 0x2f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5808, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5809, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580a, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580b, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580c, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580d, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580e, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580f, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5810, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5811, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5812, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5813, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5814, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5815, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5816, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5817, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5818, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5819, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581a, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581d, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581e, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581f, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5820, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5821, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5822, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5823, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5824, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5825, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5826, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5827, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5828, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5829, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582a, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582b, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582c, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582d, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582e, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582f, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5830, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5831, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5832, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5833, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5834, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5835, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5836, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5837, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5838, 0x32, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5839, 0x1f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583a, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583b, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583c, 0x17, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583d, 0x1e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583e, 0x26, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583f, 0x53, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5840, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5841, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5842, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5843, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5844, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5845, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5846, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5847, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5848, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5849, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584a, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584b, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584c, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584d, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584e, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584f, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5850, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5851, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5852, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5853, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5854, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5855, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5856, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5857, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5858, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5859, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585a, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585b, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585c, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585d, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585e, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585f, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5860, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5861, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5862, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5863, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5864, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5865, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5866, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5867, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5868, 0x17, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5869, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586a, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586b, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586c, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586d, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586e, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586f, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5870, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5871, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5872, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5873, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5874, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5875, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5876, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5877, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5878, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5879, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587a, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587b, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587c, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587d, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587e, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587f, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5880, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5881, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5882, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5883, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5884, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5885, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5886, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5887, 0x17, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3710, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3632, 0x51, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3702, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3703, 0xb2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3704, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370b, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370d, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3631, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3632, 0x52, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3606, 0x24, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3620, 0x96, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5785, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a13, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600, 0x52, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3604, 0x48, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3606, 0x1b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370d, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370f, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3709, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3823, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5007, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5009, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5011, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5013, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5086, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5087, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5088, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5089, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x302b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3503, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3011, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350c, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350d, 0xe4, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3621, 0xc9, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370a, 0x81, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3803, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3804, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3805, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3806, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3807, 0xd0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3808, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3809, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380a, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380b, 0xd0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380c, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380d, 0x72, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380e, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380f, 0xe4, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3810, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3818, 0xc9, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x381c, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x381d, 0xa0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x381e, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x381f, 0xb0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3820, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3821, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3824, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a08, 0x1b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a09, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0a, 0x17, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0b, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0d, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0e, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x401c, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5682, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5683, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5686, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5687, 0xcc, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5001, 0x7f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x589b, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x589a, 0xc5, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3503, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3010, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460c, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460b, 0x37, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471c, 0xd0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471d, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3815, 0x01, &iResult);
            byRead = I2CReadOneByte(g_hI2C, bySlaveAddr, 0x3818, &iResult);
            byRead = byRead & ((BYTE)(~0x08)) | 0x00;
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3818, byRead, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x501f, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4300, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3002, 0x1c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3819, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5002, 0xe0, &iResult);

            if( SensorFramerate == SensorOutputFrameRate15fps )
                I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3010, 0x30,&iResult);

            break;

        case csiSensorOutputResolution_D1_PAL:
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3103, 0x93, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3008, 0x82, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3017, 0x7f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3018, 0xfc, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3615, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3000, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3001, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3002, 0x5c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3003, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3004, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3005, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3006, 0x43, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3007, 0x37, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3011, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3012, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3010, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460c, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3815, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370c, 0xa0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3602, 0xfc, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3612, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3634, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3613, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3605, 0x7c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3621, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3622, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3604, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3603, 0xa7, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3603, 0x27, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4000, 0x21, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x401d, 0x22, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600, 0x54, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3605, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3606, 0x3f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3c01, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5000, 0x4f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5020, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5181, 0x79, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5182, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5185, 0x22, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5197, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5001, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5500, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5504, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5505, 0x7f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5080, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x300e, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4610, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471d, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4708, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3808, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3809, 0xd0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380a, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380b, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380e, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380f, 0xd0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x501f, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5000, 0x4f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4300, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3503, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3501, 0x73, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3502, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3503, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3824, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3825, 0xd8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3501, 0x1e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3502, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350b, 0x7f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380c, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380d, 0x2a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380e, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380f, 0xe8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0d, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0e, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3818, 0xc1, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3705, 0xdb, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370a, 0x81, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3801, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3621, 0xc7, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3801, 0x50, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3803, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3827, 0x3c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3810, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3804, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3805, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5682, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5683, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3806, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3807, 0x58, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5686, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5687, 0x58, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a00, 0x78, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1a, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a13, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a18, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a19, 0x7c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a08, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a09, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0a, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0b, 0xa0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350c, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350d, 0xd0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3500, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3501, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3502, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3503, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528a, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528b, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528c, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528d, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528e, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528f, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5290, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5292, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5293, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5294, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5295, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5296, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5297, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5298, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5299, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529b, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529d, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529f, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0f, 0x3c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a10, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1b, 0x3c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1e, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a11, 0x70, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1f, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3030, 0x2b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a02, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a03, 0x7d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a04, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a14, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a15, 0x7d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a16, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a00, 0x78, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a08, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a09, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0a, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0b, 0xa0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0d, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0e, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5193, 0x70, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x589b, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x589a, 0xc5, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x401e, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4001, 0x42, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x401c, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528a, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528b, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528c, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528d, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528e, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528f, 0x28, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5290, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5292, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5293, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5294, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5295, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5296, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5297, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5298, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5299, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529b, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529d, 0x28, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529f, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5282, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5300, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5301, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5302, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5303, 0x7c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530d, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530e, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530f, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5310, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5311, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5308, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5309, 0x40, &iResult); 
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5304, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5305, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5306, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5307, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5314, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5315, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5319, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5316, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5317, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5318, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5380, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5381, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5382, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5383, 0x4e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5384, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5385, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5386, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5387, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5388, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5389, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538b, 0x31, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538d, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538f, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5390, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5391, 0xab, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5392, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5393, 0xa2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5394, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5480, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5481, 0x21, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5482, 0x36, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5483, 0x57, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5484, 0x65, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5485, 0x71, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5486, 0x7d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5487, 0x87, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5488, 0x91, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5489, 0x9a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548a, 0xaa, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548b, 0xb8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548c, 0xcd, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548d, 0xdd, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548e, 0xea, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548f, 0x1d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5490, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5491, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5492, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5493, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5494, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5495, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5496, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5497, 0xb8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5498, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5499, 0x86, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549a, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549b, 0x5b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549c, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549d, 0x3b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549e, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549f, 0x1c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a0, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a1, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a2, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a3, 0xed, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a4, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a5, 0xc5, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a6, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a7, 0xa5, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a8, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a9, 0x6c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54aa, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ab, 0x41, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ac, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ad, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ae, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54af, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b0, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b1, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b2, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b3, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b4, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b5, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b6, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b7, 0xdf, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5402, 0x3f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5403, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3406, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5180, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5181, 0x52, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5182, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5183, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5184, 0x25, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5185, 0x24, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5186, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5187, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5188, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5189, 0x7c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518a, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518b, 0xb2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518c, 0xb2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518d, 0x44, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518e, 0x3d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518f, 0x58, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5190, 0x46, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5191, 0xf8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5192, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5193, 0x70, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5194, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5195, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5196, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5197, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5198, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5199, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519a, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519c, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519d, 0x82, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5025, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0f, 0x38, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a10, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1b, 0x3a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1e, 0x2e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a11, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1f, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5688, 0xa6, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5689, 0x6a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568a, 0xea, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568b, 0xae, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568c, 0xa6, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568d, 0x6a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568e, 0x62, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568f, 0x26, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5583, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5584, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5580, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5000, 0xcf, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5800, 0x27, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5801, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5802, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5803, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5804, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5805, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5806, 0x1e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5807, 0x2f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5808, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5809, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580a, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580b, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580c, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580d, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580e, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580f, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5810, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5811, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5812, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5813, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5814, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5815, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5816, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5817, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5818, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5819, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581a, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581d, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581e, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581f, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5820, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5821, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5822, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5823, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5824, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5825, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5826, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5827, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5828, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5829, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582a, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582b, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582c, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582d, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582e, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582f, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5830, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5831, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5832, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5833, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5834, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5835, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5836, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5837, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5838, 0x32, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5839, 0x1f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583a, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583b, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583c, 0x17, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583d, 0x1e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583e, 0x26, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583f, 0x53, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5840, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5841, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5842, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5843, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5844, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5845, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5846, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5847, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5848, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5849, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584a, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584b, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584c, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584d, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584e, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584f, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5850, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5851, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5852, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5853, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5854, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5855, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5856, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5857, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5858, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5859, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585a, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585b, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585c, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585d, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585e, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585f, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5860, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5861, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5862, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5863, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5864, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5865, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5866, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5867, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5868, 0x17, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5869, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586a, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586b, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586c, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586d, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586e, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586f, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5870, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5871, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5872, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5873, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5874, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5875, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5876, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5877, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5878, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5879, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587a, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587b, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587c, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587d, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587e, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587f, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5880, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5881, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5882, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5883, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5884, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5885, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5886, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5887, 0x17, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3710, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3632, 0x51, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3702, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3703, 0xb2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3704, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370b, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370d, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3631, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3632, 0x52, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3606, 0x24, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3620, 0x96, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5785, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a13, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600, 0x52, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3604, 0x48, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3606, 0x1b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370d, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370f, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3709, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3823, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5007, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5009, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5011, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5013, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5086, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5087, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5088, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5089, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x302b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3621, 0x87, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a00, 0x78, &iResult);

            if( SensorFramerate == SensorOutputFrameRate15fps )
                I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3010, 0x10, &iResult);

            break;

        case csiSensorOutputResolution_D1_NTSC:
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3103, 0x93, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3008, 0x82, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3017, 0x7f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3018, 0xfc, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3615, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3000, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3001, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3002, 0x5c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3003, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3004, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3005, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3006, 0x43, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3007, 0x37, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3011, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3012, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3010, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460c, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3815, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370c, 0xa0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3602, 0xfc, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3612, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3634, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3613, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3605, 0x7c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3621, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3622, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3604, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3603, 0xa7, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3603, 0x27, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4000, 0x21, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x401d, 0x22, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600, 0x54, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3605, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3606, 0x3f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3c01, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5000, 0x4f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5020, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5181, 0x79, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5182, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5185, 0x22, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5197, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5001, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5500, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5504, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5505, 0x7f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5080, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x300e, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4610, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471d, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4708, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3808, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3809, 0xd0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380a, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380b, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380e, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380f, 0xd0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x501f, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5000, 0x4f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4300, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3503, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3501, 0x73, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3502, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3503, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3824, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3825, 0xd8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3501, 0x1e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3502, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350b, 0x7f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380c, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380d, 0x2a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380e, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380f, 0xe8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0d, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0e, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3818, 0xc1, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3705, 0xdb, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370a, 0x81, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3801, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3621, 0xc7, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3801, 0x50, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3803, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3827, 0x3c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3810, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3804, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3805, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5682, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5683, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3806, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3807, 0x58, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5686, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5687, 0x58, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a00, 0x78, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1a, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a13, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a18, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a19, 0x7c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a08, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a09, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0a, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0b, 0xa0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350c, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350d, 0xd0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3500, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3501, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3502, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3503, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528a, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528b, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528c, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528d, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528e, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528f, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5290, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5292, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5293, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5294, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5295, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5296, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5297, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5298, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5299, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529b, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529d, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529f, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0f, 0x3c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a10, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1b, 0x3c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1e, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a11, 0x70, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1f, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3030, 0x2b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a02, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a03, 0x7d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a04, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a14, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a15, 0x7d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a16, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a00, 0x78, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a08, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a09, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0a, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0b, 0xa0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0d, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0e, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5193, 0x70, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x589b, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x589a, 0xc5, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x401e, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4001, 0x42, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x401c, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528a, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528b, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528c, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528d, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528e, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528f, 0x28, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5290, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5292, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5293, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5294, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5295, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5296, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5297, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5298, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5299, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529b, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529d, 0x28, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529f, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5282, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5300, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5301, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5302, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5303, 0x7c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530d, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530e, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530f, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5310, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5311, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5308, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5309, 0x40, &iResult); 
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5304, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5305, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5306, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5307, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5314, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5315, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5319, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5316, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5317, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5318, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5380, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5381, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5382, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5383, 0x4e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5384, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5385, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5386, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5387, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5388, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5389, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538b, 0x31, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538d, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538f, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5390, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5391, 0xab, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5392, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5393, 0xa2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5394, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5480, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5481, 0x21, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5482, 0x36, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5483, 0x57, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5484, 0x65, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5485, 0x71, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5486, 0x7d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5487, 0x87, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5488, 0x91, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5489, 0x9a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548a, 0xaa, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548b, 0xb8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548c, 0xcd, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548d, 0xdd, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548e, 0xea, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548f, 0x1d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5490, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5491, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5492, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5493, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5494, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5495, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5496, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5497, 0xb8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5498, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5499, 0x86, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549a, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549b, 0x5b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549c, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549d, 0x3b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549e, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549f, 0x1c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a0, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a1, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a2, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a3, 0xed, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a4, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a5, 0xc5, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a6, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a7, 0xa5, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a8, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a9, 0x6c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54aa, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ab, 0x41, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ac, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ad, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ae, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54af, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b0, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b1, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b2, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b3, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b4, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b5, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b6, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b7, 0xdf, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5402, 0x3f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5403, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3406, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5180, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5181, 0x52, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5182, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5183, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5184, 0x25, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5185, 0x24, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5186, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5187, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5188, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5189, 0x7c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518a, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518b, 0xb2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518c, 0xb2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518d, 0x44, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518e, 0x3d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518f, 0x58, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5190, 0x46, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5191, 0xf8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5192, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5193, 0x70, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5194, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5195, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5196, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5197, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5198, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5199, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519a, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519c, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519d, 0x82, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5025, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0f, 0x38, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a10, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1b, 0x3a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1e, 0x2e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a11, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1f, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5688, 0xa6, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5689, 0x6a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568a, 0xea, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568b, 0xae, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568c, 0xa6, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568d, 0x6a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568e, 0x62, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568f, 0x26, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5583, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5584, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5580, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5000, 0xcf, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5800, 0x27, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5801, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5802, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5803, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5804, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5805, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5806, 0x1e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5807, 0x2f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5808, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5809, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580a, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580b, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580c, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580d, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580e, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580f, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5810, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5811, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5812, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5813, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5814, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5815, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5816, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5817, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5818, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5819, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581a, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581d, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581e, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581f, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5820, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5821, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5822, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5823, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5824, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5825, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5826, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5827, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5828, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5829, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582a, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582b, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582c, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582d, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582e, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582f, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5830, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5831, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5832, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5833, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5834, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5835, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5836, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5837, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5838, 0x32, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5839, 0x1f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583a, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583b, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583c, 0x17, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583d, 0x1e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583e, 0x26, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583f, 0x53, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5840, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5841, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5842, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5843, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5844, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5845, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5846, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5847, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5848, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5849, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584a, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584b, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584c, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584d, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584e, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584f, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5850, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5851, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5852, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5853, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5854, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5855, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5856, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5857, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5858, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5859, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585a, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585b, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585c, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585d, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585e, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585f, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5860, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5861, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5862, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5863, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5864, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5865, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5866, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5867, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5868, 0x17, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5869, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586a, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586b, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586c, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586d, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586e, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586f, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5870, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5871, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5872, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5873, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5874, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5875, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5876, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5877, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5878, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5879, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587a, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587b, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587c, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587d, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587e, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587f, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5880, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5881, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5882, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5883, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5884, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5885, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5886, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5887, 0x17, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3710, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3632, 0x51, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3702, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3703, 0xb2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3704, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370b, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370d, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3631, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3632, 0x52, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3606, 0x24, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3620, 0x96, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5785, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a13, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600, 0x52, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3604, 0x48, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3606, 0x1b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370d, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370f, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3709, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3823, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5007, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5009, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5011, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5013, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5086, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5087, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5088, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5089, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x302b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3621, 0x87, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a00, 0x78, &iResult);

            if( SensorFramerate == SensorOutputFrameRate15fps )
                I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3010, 0x10, &iResult);

            break;

        case csiSensorOutputResolution_VGA:
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3103, 0x93, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3008, 0x82, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3017, 0x7f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3018, 0xfc, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3615, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3000, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3001, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3002, 0x5c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3003, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3004, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3005, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3006, 0x43, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3007, 0x37, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3011, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3012, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3010, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460c, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3815, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370c, 0xa0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3602, 0xfc, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3612, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3634, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3613, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3605, 0x7c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3621, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3622, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3604, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3603, 0xa7, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3603, 0x27, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4000, 0x21, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x401d, 0x22, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600, 0x54, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3605, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3606, 0x3f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3c01, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5000, 0x4f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5020, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5181, 0x79, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5182, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5185, 0x22, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5197, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5001, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5500, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5504, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5505, 0x7f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5080, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x300e, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4610, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471d, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4708, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3808, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3809, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380a, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380b, 0xe0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380e, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380f, 0xd0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x501f, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5000, 0x4f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4300, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3503, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3501, 0x73, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3502, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3503, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3824, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3825, 0xb0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3501, 0x1e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3502, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350b, 0x7f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380c, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380d, 0x2a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380e, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380f, 0xe8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0d, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0e, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3818, 0xc1, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3705, 0xdb, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370a, 0x81, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3801, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3621, 0xc7, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3801, 0x50, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3803, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3827, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3810, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3804, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3805, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5682, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5683, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3806, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3807, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5686, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5687, 0xbc, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a00, 0x78, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1a, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a13, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a18, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a19, 0x7c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a08, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a09, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0a, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0b, 0xa0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350c, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350d, 0xd0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3500, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3501, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3502, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3503, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528a, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528b, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528c, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528d, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528e, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528f, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5290, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5292, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5293, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5294, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5295, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5296, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5297, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5298, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5299, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529b, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529d, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529f, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0f, 0x3c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a10, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1b, 0x3c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1e, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a11, 0x70, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1f, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3030, 0x2b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a02, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a03, 0x7d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a04, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a14, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a15, 0x7d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a16, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a00, 0x78, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a08, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a09, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0a, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0b, 0xa0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0d, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0e, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5193, 0x70, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x589b, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x589a, 0xc5, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x401e, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4001, 0x42, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x401c, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528a, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528b, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528c, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528d, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528e, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528f, 0x28, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5290, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5292, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5293, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5294, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5295, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5296, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5297, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5298, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5299, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529b, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529d, 0x28, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529f, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5282, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5300, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5301, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5302, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5303, 0x7c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530d, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530e, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530f, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5310, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5311, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5308, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5309, 0x40, &iResult); 
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5304, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5305, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5306, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5307, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5314, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5315, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5319, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5316, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5317, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5318, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5380, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5381, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5382, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5383, 0x4e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5384, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5385, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5386, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5387, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5388, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5389, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538b, 0x31, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538d, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538f, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5390, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5391, 0xab, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5392, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5393, 0xa2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5394, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5480, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5481, 0x21, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5482, 0x36, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5483, 0x57, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5484, 0x65, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5485, 0x71, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5486, 0x7d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5487, 0x87, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5488, 0x91, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5489, 0x9a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548a, 0xaa, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548b, 0xb8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548c, 0xcd, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548d, 0xdd, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548e, 0xea, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548f, 0x1d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5490, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5491, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5492, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5493, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5494, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5495, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5496, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5497, 0xb8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5498, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5499, 0x86, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549a, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549b, 0x5b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549c, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549d, 0x3b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549e, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549f, 0x1c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a0, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a1, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a2, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a3, 0xed, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a4, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a5, 0xc5, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a6, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a7, 0xa5, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a8, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a9, 0x6c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54aa, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ab, 0x41, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ac, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ad, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ae, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54af, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b0, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b1, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b2, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b3, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b4, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b5, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b6, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b7, 0xdf, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5402, 0x3f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5403, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3406, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5180, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5181, 0x52, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5182, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5183, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5184, 0x25, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5185, 0x24, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5186, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5187, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5188, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5189, 0x7c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518a, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518b, 0xb2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518c, 0xb2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518d, 0x44, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518e, 0x3d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518f, 0x58, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5190, 0x46, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5191, 0xf8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5192, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5193, 0x70, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5194, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5195, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5196, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5197, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5198, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5199, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519a, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519c, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519d, 0x82, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5025, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0f, 0x38, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a10, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1b, 0x3a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1e, 0x2e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a11, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1f, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5688, 0xa6, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5689, 0x6a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568a, 0xea, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568b, 0xae, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568c, 0xa6, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568d, 0x6a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568e, 0x62, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568f, 0x26, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5583, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5584, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5580, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5000, 0xcf, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5800, 0x27, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5801, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5802, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5803, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5804, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5805, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5806, 0x1e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5807, 0x2f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5808, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5809, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580a, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580b, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580c, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580d, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580e, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580f, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5810, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5811, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5812, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5813, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5814, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5815, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5816, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5817, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5818, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5819, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581a, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581d, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581e, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581f, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5820, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5821, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5822, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5823, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5824, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5825, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5826, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5827, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5828, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5829, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582a, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582b, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582c, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582d, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582e, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582f, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5830, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5831, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5832, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5833, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5834, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5835, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5836, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5837, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5838, 0x32, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5839, 0x1f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583a, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583b, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583c, 0x17, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583d, 0x1e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583e, 0x26, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583f, 0x53, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5840, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5841, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5842, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5843, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5844, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5845, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5846, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5847, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5848, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5849, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584a, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584b, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584c, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584d, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584e, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584f, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5850, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5851, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5852, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5853, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5854, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5855, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5856, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5857, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5858, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5859, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585a, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585b, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585c, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585d, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585e, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585f, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5860, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5861, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5862, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5863, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5864, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5865, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5866, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5867, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5868, 0x17, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5869, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586a, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586b, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586c, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586d, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586e, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586f, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5870, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5871, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5872, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5873, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5874, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5875, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5876, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5877, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5878, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5879, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587a, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587b, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587c, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587d, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587e, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587f, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5880, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5881, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5882, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5883, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5884, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5885, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5886, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5887, 0x17, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3710, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3632, 0x51, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3702, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3703, 0xb2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3704, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370b, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370d, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3631, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3632, 0x52, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3606, 0x24, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3620, 0x96, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5785, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a13, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600, 0x52, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3604, 0x48, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3606, 0x1b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370d, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370f, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3709, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3823, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5007, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5009, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5011, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5013, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5086, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5087, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5088, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5089, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x302b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3621, 0x87, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a00, 0x78, &iResult);

            if( SensorFramerate == SensorOutputFrameRate15fps )
                I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3010, 0x10, &iResult);


            break;

        case csiSensorOutputResolution_QVGA:
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3103, 0x93, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3008, 0x82, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3017, 0x7f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3018, 0xfc, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3615, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3000, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3001, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3002, 0x5c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3003, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3004, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3005, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3006, 0x43, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3007, 0x37, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3011, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3012, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3010, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x460c, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3815, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370c, 0xa0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3602, 0xfc, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3612, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3634, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3613, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3605, 0x7c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3621, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3622, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3604, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3603, 0xa7, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3603, 0x27, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4000, 0x21, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x401d, 0x22, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600, 0x54, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3605, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3606, 0x3f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3c01, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5000, 0x4f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5020, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5181, 0x79, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5182, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5185, 0x22, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5197, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5001, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5500, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5504, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5505, 0x7f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5080, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x300e, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4610, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x471d, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4708, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3808, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3809, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380a, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380b, 0xe0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380e, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380f, 0xd0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x501f, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5000, 0x4f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4300, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3503, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3501, 0x73, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3502, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3503, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3824, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3825, 0xb0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3501, 0x1e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3502, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350b, 0x7f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380c, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380d, 0x2a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380e, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380f, 0xe8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0d, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0e, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3818, 0xc1, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3705, 0xdb, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370a, 0x81, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3801, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3621, 0xc7, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3801, 0x50, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3803, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3827, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3810, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3804, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3805, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5682, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5683, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3806, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3807, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5686, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5687, 0xbc, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a00, 0x78, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1a, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a13, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a18, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a19, 0x7c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a08, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a09, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0a, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0b, 0xa0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350c, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350d, 0xd0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3500, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3501, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3502, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3503, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528a, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528b, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528c, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528d, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528e, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528f, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5290, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5292, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5293, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5294, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5295, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5296, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5297, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5298, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5299, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529b, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529d, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529f, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0f, 0x3c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a10, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1b, 0x3c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1e, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a11, 0x70, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1f, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3030, 0x2b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a02, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a03, 0x7d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a04, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a14, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a15, 0x7d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a16, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a00, 0x78, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a08, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a09, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0a, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0b, 0xa0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0d, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0e, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5193, 0x70, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x589b, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x589a, 0xc5, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x401e, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x4001, 0x42, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x401c, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528a, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528b, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528c, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528d, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528e, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x528f, 0x28, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5290, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5292, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5293, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5294, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5295, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5296, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5297, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5298, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5299, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529b, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529d, 0x28, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x529f, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5282, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5300, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5301, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5302, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5303, 0x7c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530d, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530e, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x530f, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5310, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5311, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5308, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5309, 0x40, &iResult); 
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5304, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5305, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5306, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5307, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5314, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5315, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5319, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5316, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5317, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5318, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5380, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5381, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5382, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5383, 0x4e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5384, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5385, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5386, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5387, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5388, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5389, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538b, 0x31, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538d, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x538f, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5390, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5391, 0xab, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5392, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5393, 0xa2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5394, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5480, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5481, 0x21, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5482, 0x36, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5483, 0x57, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5484, 0x65, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5485, 0x71, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5486, 0x7d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5487, 0x87, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5488, 0x91, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5489, 0x9a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548a, 0xaa, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548b, 0xb8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548c, 0xcd, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548d, 0xdd, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548e, 0xea, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x548f, 0x1d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5490, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5491, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5492, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5493, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5494, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5495, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5496, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5497, 0xb8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5498, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5499, 0x86, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549a, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549b, 0x5b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549c, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549d, 0x3b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549e, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x549f, 0x1c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a0, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a1, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a2, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a3, 0xed, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a4, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a5, 0xc5, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a6, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a7, 0xa5, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a8, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54a9, 0x6c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54aa, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ab, 0x41, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ac, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ad, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54ae, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54af, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b0, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b1, 0x20, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b2, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b3, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b4, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b5, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b6, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x54b7, 0xdf, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5402, 0x3f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5403, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3406, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5180, 0xff, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5181, 0x52, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5182, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5183, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5184, 0x25, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5185, 0x24, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5186, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5187, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5188, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5189, 0x7c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518a, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518b, 0xb2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518c, 0xb2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518d, 0x44, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518e, 0x3d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x518f, 0x58, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5190, 0x46, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5191, 0xf8, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5192, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5193, 0x70, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5194, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5195, 0xf0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5196, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5197, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5198, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5199, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519a, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519c, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519d, 0x82, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5025, 0x80, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a0f, 0x38, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a10, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1b, 0x3a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1e, 0x2e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a11, 0x60, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a1f, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5688, 0xa6, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5689, 0x6a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568a, 0xea, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568b, 0xae, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568c, 0xa6, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568d, 0x6a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568e, 0x62, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x568f, 0x26, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5583, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5584, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5580, 0x02, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5000, 0xcf, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5800, 0x27, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5801, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5802, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5803, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5804, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5805, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5806, 0x1e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5807, 0x2f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5808, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5809, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580a, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580b, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580c, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580d, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580e, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x580f, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5810, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5811, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5812, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5813, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5814, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5815, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5816, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5817, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5818, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5819, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581a, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581c, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581d, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581e, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x581f, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5820, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5821, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5822, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5823, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5824, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5825, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5826, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5827, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5828, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5829, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582a, 0x06, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582b, 0x04, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582c, 0x05, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582d, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582e, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x582f, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5830, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5831, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5832, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5833, 0x0a, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5834, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5835, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5836, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5837, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5838, 0x32, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5839, 0x1f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583a, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583b, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583c, 0x17, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583d, 0x1e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583e, 0x26, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x583f, 0x53, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5840, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5841, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5842, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5843, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5844, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5845, 0x09, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5846, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5847, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5848, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5849, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584a, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584b, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584c, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584d, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584e, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x584f, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5850, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5851, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5852, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5853, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5854, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5855, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5856, 0x0e, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5857, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5858, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5859, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585a, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585b, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585c, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585d, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585e, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x585f, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5860, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5861, 0x0c, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5862, 0x0d, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5863, 0x08, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5864, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5865, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5866, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5867, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5868, 0x17, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5869, 0x19, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586a, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586b, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586c, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586d, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586e, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x586f, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5870, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5871, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5872, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5873, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5874, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5875, 0x16, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5876, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5877, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5878, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5879, 0x0f, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587a, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587b, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587c, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587d, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587e, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x587f, 0x11, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5880, 0x12, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5881, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5882, 0x14, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5883, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5884, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5885, 0x15, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5886, 0x13, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5887, 0x17, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3710, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3632, 0x51, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3702, 0x10, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3703, 0xb2, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3704, 0x18, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370b, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370d, 0x03, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3631, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3632, 0x52, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3606, 0x24, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3620, 0x96, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5785, 0x07, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3a13, 0x30, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600, 0x52, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3604, 0x48, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3606, 0x1b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370d, 0x0b, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x370f, 0xc0, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3709, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3823, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5007, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5009, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5011, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5013, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x519e, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5086, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5087, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5088, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x5089, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x302b, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3621, 0x87, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3808, 0x01, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3809, 0x40, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380a, 0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x380b, 0xf0, &iResult);

            if( SensorFramerate == SensorOutputFrameRate15fps )
                I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3010, 0x10, &iResult);

            break;

        case csiSensorOutputResolution_QXGA:
        case csiSensorOutputResolution_XGA:
        case csiSensorOutputResolution_CIF: 
        case csiSensorOutputResolution_QCIF:
        case csiSensorOutputResolution_QQVGA:
        default:  
             DEBUGMSG(ZONE_ERROR, (TEXT("%s: We don't support this kind of resolution for the sensor!\r\n"), __WFUNCTION__));
             retVal = FALSE;
             break;
    }

    CAMERA_FUNCTION_EXIT();

    return retVal;
}


//----------------------------------------------------------------------------
//
// Function: CameraOV5642SetDigitalZoom
//
// Sets the camera digital zoom level .
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//      zoom
//          [in] Camera zoom value that will be set
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL CameraOV5642SetDigitalZoom(CSI_SELECT csi_sel, DWORD ZoomLevel)
{  
    BOOL retVal = TRUE;

    CAMERA_FUNCTION_ENTRY();

    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(ZoomLevel);
    UNREFERENCED_PARAMETER(csi_sel);

    CAMERA_FUNCTION_EXIT();

    return retVal;
}

//----------------------------------------------------------------------------
//
// Function: CameraOV5642MirFlip
//
// Sets the camera Mirroring and Flipping features.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//      doRot
//          [in] Camera Horizontal Mirroring will be set or not
//      doFlip
//          [in] Camera Vertical flipping will be set or not
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL CameraOV5642MirFlip(CSI_SELECT csi_sel, BOOL doMirror, BOOL doFlip)
{    
    INT iResult;
    BYTE bySlaveAddr;
    BYTE byRead;
    BOOL retVal = TRUE;

    CAMERA_FUNCTION_ENTRY();

    switch (csi_sel)
    {
        case CSI_SELECT_CSI0:
             bySlaveAddr = OV5642_I2C_ADDRESS;
             break;               

        default:
             DEBUGMSG(ZONE_ERROR, (TEXT("%s: Don't support!\r\n"), __WFUNCTION__));
             return FALSE;
    }   
    
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(doMirror);
    UNREFERENCED_PARAMETER(doFlip);

    if(doMirror)
    {
        if(doFlip)
        {
            byRead = I2CReadOneByte(g_hI2C, bySlaveAddr, 0x3818, &iResult);
            byRead = byRead & ((BYTE)(~0x60)) | 0x60;
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3818, byRead, &iResult);
        }
        else
        {
            byRead = I2CReadOneByte(g_hI2C, bySlaveAddr, 0x3818, &iResult);
            byRead = byRead & ((BYTE)(~0x60)) | 0x40;
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3818, byRead, &iResult);
        }

        byRead = I2CReadOneByte(g_hI2C, bySlaveAddr, 0x3621, &iResult);
        byRead = byRead & ((BYTE)(~0x20)) | 0x00;
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3621, byRead, &iResult);
    }
    else
    {
        if(doFlip)
        {
            byRead = I2CReadOneByte(g_hI2C, bySlaveAddr, 0x3818, &iResult);
            byRead = byRead & ((BYTE)(~0x60)) | 0x20;
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3818, byRead, &iResult);
        }
        else
        {
            byRead = I2CReadOneByte(g_hI2C, bySlaveAddr, 0x3818, &iResult);
            byRead = byRead & ((BYTE)(~0x60)) | 0x00;
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3818, byRead, &iResult);
        }

        byRead = I2CReadOneByte(g_hI2C, bySlaveAddr, 0x3621, &iResult);
        byRead = byRead & ((BYTE)(~0x20)) | 0x20;
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3621, byRead, &iResult);
    }
    
    CAMERA_FUNCTION_EXIT();

    return retVal;
}


//----------------------------------------------------------------------------
//
// Function: CameraOV5642GetFormats
//
// Get the sensor supported formats for specific pin
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//      type
//          [in] Camera pin number, such as capture, still, preview
//      formats
//          [out] supported formats
//
// Returns:
//      Number of supported formats
//
//------------------------------------------------------------------------------
DWORD CameraOV5642GetFormats(CSI_SELECT csi_sel, DWORD type, PCS_DATARANGE_VIDEO** formats)
{
    DWORD dFormats = 0;
    HANDLE hFslFilter = NULL;
    DWORD result;

    UNREFERENCED_PARAMETER(csi_sel);

    // Get counts and all formats of  current sensor supported
    if(formats != NULL)
    {
        switch(type)
        {
            case CAPTURE:
                {
                    dFormats = 18;
    
                    *formats = new PCS_DATARANGE_VIDEO[dFormats];
    
                    if( NULL == *formats )
                    {
                        return 0;
                    }
    
                    // Set sensor supported formats
                    (* formats)[0] = &DCAM_OV5642_StreamMode_UYVY_QVGA;
                    (* formats)[1] = &DCAM_OV5642_StreamMode_UYVY_VGA;
                    (* formats)[2] = &DCAM_OV5642_StreamMode_UYVY_D1_PAL;
                    (* formats)[3] = &DCAM_OV5642_StreamMode_UYVY_D1_NTSC;
                    (* formats)[4] = &DCAM_OV5642_StreamMode_UYVY_720P;
                    (* formats)[5] = &DCAM_OV5642_StreamMode_UYVY_1080P;
                    (* formats)[6] = &DCAM_OV5642_StreamMode_YV12_QVGA;
                    (* formats)[7] = &DCAM_OV5642_StreamMode_YV12_VGA;
                    (* formats)[8] = &DCAM_OV5642_StreamMode_YV12_D1_PAL;
                    (* formats)[9] = &DCAM_OV5642_StreamMode_YV12_D1_NTSC;
                    (* formats)[10] = &DCAM_OV5642_StreamMode_YV12_720P;
                    (* formats)[11] = &DCAM_OV5642_StreamMode_YV12_1080P;
                    (* formats)[12] = &DCAM_OV5642_StreamMode_NV12_QVGA;
                    (* formats)[13] = &DCAM_OV5642_StreamMode_NV12_VGA;
                    (* formats)[14] = &DCAM_OV5642_StreamMode_NV12_D1_PAL;
                    (* formats)[15] = &DCAM_OV5642_StreamMode_NV12_D1_NTSC;
                    (* formats)[16] = &DCAM_OV5642_StreamMode_NV12_720P;
                    (* formats)[17] = &DCAM_OV5642_StreamMode_NV12_1080P;
                }
                break;
    
            case STILL:
                {
                    dFormats = 7;
    
                    *formats = new PCS_DATARANGE_VIDEO[dFormats];
    
                    if( NULL == *formats )
                    {
                        return 0;
                    }
    
                    // Set sensor supported formats
                    (* formats)[0] = &DCAM_OV5642_StreamMode_RGB565_QVGA;
                    (* formats)[1] = &DCAM_OV5642_StreamMode_RGB565_VGA;
                    (* formats)[2] = &DCAM_OV5642_StreamMode_RGB565_D1_PAL;
                    (* formats)[3] = &DCAM_OV5642_StreamMode_RGB565_D1_NTSC;
                    (* formats)[4] = &DCAM_OV5642_StreamMode_RGB565_720P;
                    (* formats)[5] = &DCAM_OV5642_StreamMode_RGB565_1080P;
                    (* formats)[6] = &DCAM_OV5642_StreamMode_RGB565_QSXGA;
                }
                break;
    
            case PREVIEW:
                {
                    dFormats = 6;
    
                    *formats = new PCS_DATARANGE_VIDEO[dFormats];
    
                    if( NULL == *formats )
                    {
                        return 0;
                    }
    
                    // Set sensor supported formats
                    (* formats)[0] = &DCAM_OV5642_StreamMode_RGB565_QVGA;
                    (* formats)[1] = &DCAM_OV5642_StreamMode_RGB565_VGA;
                    (* formats)[2] = &DCAM_OV5642_StreamMode_YV12_QVGA;
                    (* formats)[3] = &DCAM_OV5642_StreamMode_YV12_VGA;
                    (* formats)[4] = &DCAM_OV5642_StreamMode_NV12_QVGA;
                    (* formats)[5] = &DCAM_OV5642_StreamMode_NV12_VGA;
                }
                break;
    
            default:
                ERRORMSG(TRUE, (TEXT("%s: Invalid type parameter!\r\n"),__WFUNCTION__));
                break;
        }
    }
    // Only get the counts of supported
    else
    {
        //Create an event to flag filter's factory
        //This Event using to distermin the connected filter is freescale filter or not
        //When Event is set, connected filter is freescale filter, which can support all of the format
        //When Event is reset, the filter isn't freescale's, so we just report limited format
        hFslFilter = CreateEvent(NULL, TRUE, FALSE, FSL_FILTER_EVENT);
        
        if(hFslFilter == NULL)
        {
            ERRORMSG(TRUE, (TEXT("%s: Failed creating Event for hFslFilter\r\n"),__WFUNCTION__));
            return 0;
        }
        
        result = WaitForSingleObject(hFslFilter,1);
        
        if(result == WAIT_OBJECT_0)
        {
            //if connected filter is  freescale filter, we will support all format
            switch(type)
            {
                case CAPTURE:
                    dFormats = 18;
                    break;
            
                case STILL:
                    dFormats = 7;
                    break;
            
                case PREVIEW:
                    dFormats = 6;
                    break;
            
                default:
                    ERRORMSG(TRUE, (TEXT("%s: Invalid type parameter!\r\n"),__WFUNCTION__));
                    break;
            }
        }
        else
        {
            //if connected filter isn't  freescale filter, we only support limited format
            switch(type)
            {
                case CAPTURE:
                    dFormats = 2;
                    break;
            
                case STILL:
                    dFormats = 2;
                    break;
            
                case PREVIEW:
                    dFormats = 4;
                    break;
            
                default:
                    ERRORMSG(TRUE, (TEXT("%s: Invalid type parameter!\r\n"),__WFUNCTION__));
                    break;
            }
        }  

        CloseHandle(hFslFilter);
    }

    return dFormats;

}


