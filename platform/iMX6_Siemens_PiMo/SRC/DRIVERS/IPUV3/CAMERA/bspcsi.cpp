//------------------------------------------------------------------------------
//
//  Copyright (C) 2008-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  bspcsi.c
//
//  Provides BSP-specific configuration routines for the CSI peripheral.
//
//------------------------------------------------------------------------------
#pragma warning(disable: 4005 6244)

#include <windows.h>
#include <ceddk.h>
#include <devload.h>
#include <NKIntr.h>
#include <winddi.h>
#include <ddgpe.h>
#define __DVP_INCLUDED__

#include "Cs.h"
#include "Csmedia.h"
#include "SensorFormats.h"

#include "common_ipuv3h.h"
#include "ipu_common.h"
#include "cameradbg.h"

#include "bsp.h"
#include "i2cbus.h"
#include "bspcsi.h"
#include "csi.h"
#include "CsiClass.h"
#include "CameraOV3640.h"
#include "CameraOV5640.h"
#include "CameraOV5642.h"
#include "TVinadv7180.h"
//#include "pmic.h"

//------------------------------------------------------------------------------
// External Functions
//------------------------------------------------------------------------------

// External Variables
//------------------------------------------------------------------------------

// Defines
#define BSP_CSI_FUNCTION_ENTRY() \
    DEBUGMSG(ZONE_FUNCTION, (TEXT("++%s\r\n"), __WFUNCTION__))

#define BSP_CSI_FUNCTION_EXIT() \
    DEBUGMSG(ZONE_FUNCTION, (TEXT("--%s\r\n"), __WFUNCTION__))

#define CAMERA0_SENSOR_MODULE    csiSensorId_OV3640 // OmniVision as default Sensor
#define CAMERA1_SENSOR_MODULE    csiTestMode        // CSi test mode

//------------------------------------------------------------------------------
// Types

//------------------------------------------------------------------------------
// Global Variables



// Camera in Used - Default Ov3640
csiSensorId gSensorInUse0 = CAMERA0_SENSOR_MODULE;
csiSensorId gSensorInUse1 = CAMERA1_SENSOR_MODULE;
HANDLE g_hI2C;
extern BYTE gLastTVinType;

//------------------------------------------------------------------------------
// Local Variables



//------------------------------------------------------------------------------
// Local Functions

BOOL BSPSensorSetClockGatingMode(CSI_SELECT csi_sel,BOOL);
BOOL BSPEnableCamera(CSI_SELECT csi_sel);
BOOL BSPSetupCamera(CSI_SELECT csi_sel, DWORD dwFramerate);

//-----------------------------------------------------------------------------
//
// Function:  BSPInitializeHSC
//
// This function configures HSC module to not use MIPI mode.
//
// Parameters:
//      None.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
BOOL BSPInitializeHSC()
{
    BOOL retVal = TRUE;

    BSP_CSI_FUNCTION_ENTRY();

    BSP_CSI_FUNCTION_EXIT();    

    return retVal;

}


//-----------------------------------------------------------------------------
//
// Function:  BSPCSI0IOMUXConfig
//
// This function makes the DDK call to configure the IOMUX
// pins required for the CSI0.
//
// Parameters:
//      None.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
BOOL BSPCSI0IOMUXConfig()
{
    BSP_CSI_FUNCTION_ENTRY();

    BOOL retVal = TRUE;

	OAL_PLAT_INFO pInfo;

	//Get Processor type
	KernelIoControl(IOCTL_PLATFORM_INFO, NULL, 0, &pInfo, sizeof(OAL_PLAT_INFO), NULL);

	if(pInfo.ChipType == CHIP_MX6DQ)
	{
		UINT32 ConfData;

		ConfData = DDKIomuxGetGpr(1);
		ConfData |=   (1 << 19); // Enable iomux on IPU1
		ConfData |=   (1 << 20); // Enable iomux on IPU2
		ConfData &= ~ (1 << 24); // Disable MIPI DPI off
		ConfData &= ~ (1 << 25); // DIsable MIPI color switch
		DDKIomuxSetGpr(1, ConfData);

		RETAILMSG(1,(L"Processor Type IMX6QD\r\n"));

	} else if (pInfo.ChipType == CHIP_MX6DL) 
	{
		UINT32 ConfData;

		ConfData = DDKIomuxGetGpr(13);

		ConfData |=   (1 << 2); // Enable iomux on IPU CSI0

		DDKIomuxSetGpr(13, ConfData);

		RETAILMSG(1,(L"Processor Type IMX6DSL\r\n"));

	} else 
	{
		RETAILMSG(1,((L"ERROR: CSI0 UNKNOWN Processor [0x%x]\r\n"),pInfo.ChipType));
		return FALSE;
	}

	// CSI_PWN

    DDKGpioSetConfig(DDK_GPIO_PORT1,19,DDK_GPIO_DIR_OUT,DDK_GPIO_INTR_NONE);   //GPIO2_2 output
    DDKGpioWriteDataPin(DDK_GPIO_PORT1,19,0); //output high for normal mode, low for powerdown

	// CSI0_PWN

    DDKGpioSetConfig(DDK_GPIO_PORT1,16,DDK_GPIO_DIR_OUT,DDK_GPIO_INTR_NONE);   //GPIO2_2 output
    DDKGpioWriteDataPin(DDK_GPIO_PORT1,16,0); //output high for normal mode, low for powerdown

	// OV5642 GPIO init is done in cameraov5642.cpp

	// Enable clocks
	BSPSensorSetClockGatingMode(CSI_SELECT_CSI0, TRUE);

    return retVal;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPCSI1IOMUXConfig
//
// This function makes the DDK call to configure the IOMUX
// pins required for the CSI1.
//
// Parameters:
//      None.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
BOOL BSPCSI1IOMUXConfig()
{
    BSP_CSI_FUNCTION_ENTRY();

    BOOL retVal = TRUE;

	// CSI MCLK

    // Enable master clock
    DDKClockSetCKO1(true, DDK_CLOCK_CKO1_SRC_VIDEO_27M, DDK_CLOCK_CKO_DIV_1);

	// CSI_PWN (GPIO1_19)


	// GPIO1_19 output
    DDKGpioSetConfig(DDK_GPIO_PORT1,19,DDK_GPIO_DIR_OUT,DDK_GPIO_INTR_NONE);   
    DDKGpioWriteDataPin(DDK_GPIO_PORT1,19,0); //output high for normal mode, low for powerdown

	// GPIO1_16

	//GPIO1_16 output
    DDKGpioSetConfig(DDK_GPIO_PORT1,16,DDK_GPIO_DIR_OUT,DDK_GPIO_INTR_NONE);
    DDKGpioWriteDataPin(DDK_GPIO_PORT1,16,0); //output high for normal mode, low for powerdown

	// Enable clocks
	BSPSensorSetClockGatingMode(CSI_SELECT_CSI1, TRUE);
	
	BSP_CSI_FUNCTION_EXIT();    

    return retVal;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPSetSensorPowerDown
//
// This function set sensor Power mode
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to set power.
//      bPowerDown
//          [in] True: sensor enter into low power mode
//                False: sensor out from low power mode
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
BOOL BSPSetSensorPowerDown(CSI_SELECT csi_sel, BOOL bPowerDown)
{
    BSP_CSI_FUNCTION_ENTRY();

    BOOL retVal = TRUE;

    if(csi_sel == CSI_SELECT_CSI0)
    {
        DEBUGMSG(ZONE_INIT,(TEXT("%s: CSI0 sensor set power\r\n"),__WFUNCTION__));
        switch (gSensorInUse0)
        {
            case csiTVinId_ADV7180:
                 if(bPowerDown)
                 {
                     DDKGpioWriteDataPin(DDK_GPIO_PORT2,2,0); 
                 }
                 else
                 {
                     DDKGpioWriteDataPin(DDK_GPIO_PORT2,2,1); 
                 }
                 break;

            case csiSensorId_OV3640:
            case csiSensorId_OV5642:
            case csiSensorId_OV5640:
            default:
                break;        
        }
    }
    else
    {
    }

    BSP_CSI_FUNCTION_EXIT();    

    return retVal;
}


//------------------------------------------------------------------------------
//
// Function: BSPSetupCamera
//
// This function initializes the camera sensor module.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1 to initialize.
//      dwFramerate
//          [in] Sensor frame rate
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL BSPSetupCamera(CSI_SELECT csi_sel, DWORD dwFramerate)
{
    BSP_CSI_FUNCTION_ENTRY();

    BOOL retVal = TRUE;

    if(csi_sel == CSI_SELECT_CSI0)
    {
        DEBUGMSG(ZONE_INIT,(TEXT("%s: CSI0 sensor setup\r\n"),__WFUNCTION__));
        switch (gSensorInUse0)
        {
            case csiTVinId_ADV7180:
                 DEBUGMSG(ZONE_INIT, (TEXT("setupTvin: ADV7180 initialization\r\n")));
                 retVal = TVInADV7180Init(csi_sel);        
                 break;

            case csiSensorId_OV5640:
                 DEBUGMSG(ZONE_INIT, (TEXT("setupCamera: OmniVision 5640 sensor initialization\r\n")));
                 retVal = CameraOV5640Init(csi_sel, (SensorOutputFrameRate)dwFramerate);        
                 break;

			case csiSensorId_OV5642:
                 DEBUGMSG(ZONE_INIT, (TEXT("setupCamera: OmniVision 5642 sensor initialization\r\n")));
                 retVal = CameraOV5642Init(csi_sel, (SensorOutputFrameRate)dwFramerate);        
                 break;

            case csiSensorId_OV3640:
                 DEBUGMSG(ZONE_INIT, (TEXT("setupCamera: OmniVision 3640 sensor initialization\r\n")));
                 retVal = CameraOV3640Init(csi_sel, (SensorOutputFrameRate)dwFramerate);        
                 break; 

            case csiTestMode:
            default:
                 break; 
        }
    }
    else
    {
        DEBUGMSG(ZONE_INIT,(TEXT("%s: CSI1 sensor setup \r\n"),__WFUNCTION__));
        switch (gSensorInUse1)
        {
            case csiSensorId_OV3640:
                 DEBUGMSG(ZONE_INIT, (TEXT("setupCamera: OmniVision 3640 sensor initialization\r\n")));
                 retVal = CameraOV3640Init(csi_sel, (SensorOutputFrameRate)dwFramerate);        
                 break; 

			case csiSensorId_OV5640:
                 DEBUGMSG(ZONE_INIT, (TEXT("setupCamera: OmniVision 5640 sensor initialization\r\n")));
                 retVal = CameraOV5640Init(csi_sel, (SensorOutputFrameRate)dwFramerate);        
                 break;

			case csiSensorId_OV5642:
                 DEBUGMSG(ZONE_INIT, (TEXT("setupCamera: OmniVision 5642 sensor initialization\r\n")));
                 retVal = CameraOV5642Init(csi_sel, (SensorOutputFrameRate)dwFramerate);        
                 break;

            case csiTestMode:
            default:
                 break; 
        }
    }

    BSP_CSI_FUNCTION_EXIT();

    return retVal;
}


//------------------------------------------------------------------------------
//
// Function: BSPGetTVinType
//
// This function initializes the camera sensor module.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//
// Returns:
//      TVinType.
//
//------------------------------------------------------------------------------
BYTE BSPGetTVinType(CSI_SELECT csi_sel)
{
    BSP_CSI_FUNCTION_ENTRY();

    BYTE byTVType = 0;

    if(csi_sel == CSI_SELECT_CSI0)
    {
        DEBUGMSG(ZONE_INIT,(TEXT("%s: CSI0 sensor\r\n"),__WFUNCTION__));
        switch (gSensorInUse0)
        {
            case csiTVinId_ADV7180:  
                 byTVType = TVInGetTVinType(csi_sel);
                 break;

            case csiSensorId_OV5640:       
                 break;

			case csiSensorId_OV5642:       
                 break;

            case csiSensorId_OV3640:       
                 break; 

            case csiTestMode:
            default:
                 break; 
        }
    }
    else
    {
        DEBUGMSG(ZONE_INIT,(TEXT("%s: CSI1 sensor\r\n"),__WFUNCTION__));
        switch (gSensorInUse1)
        {
            case csiSensorId_OV3640:      
                 break; 

            case csiTestMode:
            default:
                 break; 
        }
    }

    BSP_CSI_FUNCTION_EXIT();

    return byTVType;
}



//------------------------------------------------------------------------------
//
// Function: BSPDeleteCamera
//
// This function deinitializes the camera sensor module.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1 to initialize.
//
//Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL BSPDeleteCamera(CSI_SELECT csi_sel)
{
    BOOL retVal = TRUE;
    BSP_CSI_FUNCTION_ENTRY();

    if(csi_sel == CSI_SELECT_CSI0)
    {
        switch (gSensorInUse0)
        {
            case csiTVinId_ADV7180:
                retVal = TVInADV7180Deinit(csi_sel);
                
                DEBUGMSG(ZONE_INIT,(TEXT("DeleteTvin: ADV7180 deinitialization\r\n")));
                break;

            case csiSensorId_OV5640:
                retVal = CameraOV5640Deinit(csi_sel);
                break;

			case csiSensorId_OV5642:
                retVal = CameraOV5642Deinit(csi_sel);
                break;

            case csiSensorId_OV3640:
                retVal = CameraOV3640Deinit(csi_sel);
                DEBUGMSG(ZONE_INIT,(TEXT("DeleteCamera: OmniVision sensor deinitialization\r\n")));
                break;
            case csiTestMode:
            default:
                break; 
        }
    }
    else
    {
        switch (gSensorInUse1)
        {
            case csiSensorId_OV3640:
                retVal = CameraOV3640Deinit(csi_sel); 
                DEBUGMSG(ZONE_INIT,(TEXT("DeleteCamera: OmniVision sensor deinitialization\r\n")));
                break; 

            case csiTestMode:
            default:
                break; 
        }
    }

    BSP_CSI_FUNCTION_EXIT();

    return retVal;
}



//------------------------------------------------------------------------------
//
// Function: BSPEnableCamera
//
// This function enables the camera sensor module.
//
// Parameters:
//      csi_sel
//          [in] Selects CSI0 or CSI1 to enable the connected sensor.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL BSPEnableCamera(CSI_SELECT csi_sel)
{
    BOOL   retVal = TRUE;
        
    BSP_CSI_FUNCTION_ENTRY();

    // Sensor not in low power mode
    BSPSetSensorPowerDown(csi_sel, FALSE);

    if(csi_sel == CSI_SELECT_CSI0)
    {
        switch (gSensorInUse0)
        {
            case csiTVinId_ADV7180:
                break;

            case csiSensorId_OV3640:
            case csiSensorId_OV5640:
            case csiSensorId_OV5642:
            default:
                break;
        }
    }
    else
    {
    }

    Sleep(100);

    BSP_CSI_FUNCTION_EXIT();

    return retVal;
}



//------------------------------------------------------------------------------
//
// Function: BSPDisableCamera
//
// This function disables the camera sensor module.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1 to disable clk.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL BSPDisableCamera(CSI_SELECT csi_sel)
{
    BOOL   retVal = TRUE;

    BSP_CSI_FUNCTION_ENTRY();

    // Sensor enter into low power mode
    BSPSetSensorPowerDown(csi_sel, TRUE);

    if(csi_sel == CSI_SELECT_CSI0)
    {
        switch (gSensorInUse0)
        {
            case csiTVinId_ADV7180:
                break;

            case csiSensorId_OV3640:
            case csiSensorId_OV5640:
            case csiSensorId_OV5642:
            default:
                break;
        }
    }
    else
    {
    }

    BSP_CSI_FUNCTION_EXIT();

    return retVal;
}

//------------------------------------------------------------------------------
//
// Function: BSPGetSensorClockRatio
//
// This function calculates the divider value to program into
// the CSI to use in test mode.
//
// Parameters:
//      None.
//
// Returns:
//      Sensor clock divider value.
//
//------------------------------------------------------------------------------
UINT32 BSPGetSensorClockRatio()
{
    UINT32 freq;

    BSP_CSI_FUNCTION_ENTRY();

//    DDKClockGetFreq(DDK_CLOCK_SIGNAL_IPU_HSP, &freq);
    DDKClockGetFreq(DDK_CLOCK_SIGNAL_IPU1_HSP, &freq);
    RETAILMSG(CSI_DEBUG_MSG,(TEXT("%s:DDK_CLOCK_SIGNAL_IPU_HSP = %d\r\n"),__WFUNCTION__,freq));

    BSP_CSI_FUNCTION_EXIT();

    // For a target frame rate of 30 FPS, we want a sensor clock
    // in the 6 -27MHz range.

    return (freq/24000000 - 1);
}


//------------------------------------------------------------------------------
//
// Function: BSPGetDefaultCameraFromRegistry
//
// This function reads the default camera sensor from the
// registry.
//
// Parameters:
//      None.
//
// Returns:
//      TRUE if success
//      FALSE if failure
//
//------------------------------------------------------------------------------
BOOL BSPGetDefaultCameraFromRegistry(UINT32 camer_num, CSI_SELECT csi_sel)
{
    UINT32 error;
    HKEY hKey;
    UINT32 dwSize;
    csiSensorId sensorId;

    BSP_CSI_FUNCTION_ENTRY();

    // Open Camera registry path
    if( camer_num == 1 )
    {
        error = RegOpenKeyEx(HKEY_LOCAL_MACHINE, TEXT(CSI_CAMERA1_REG_PATH), 0 , 0, &hKey);
        if (error != ERROR_SUCCESS) {
            ERRORMSG(TRUE,(TEXT("Failed to open camera reg path:%s [Error:0x%x]\r\n"),CSI_CAMERA1_REG_PATH,error));
            return FALSE;
        }
    }
    else if( camer_num == 2 )
    {
        error = RegOpenKeyEx(HKEY_LOCAL_MACHINE, TEXT(CSI_CAMERA2_REG_PATH), 0 , 0, &hKey);
        if (error != ERROR_SUCCESS) {
            ERRORMSG(TRUE,(TEXT("Failed to open camera reg path:%s [Error:0x%x]\r\n"),CSI_CAMERA2_REG_PATH,error));
            return FALSE;
        }
    }
    else
    {
        ERRORMSG(TRUE,(TEXT("Can not support Camera NO.%d \r\n"),camer_num));
        return FALSE;
    }

    // Get Default Camera
    dwSize = sizeof(csiSensorId);
    error = RegQueryValueEx(hKey, TEXT(CSI_REG_CAMERAID_KEYWORD), NULL, NULL,(LPBYTE)&sensorId, (LPDWORD)&dwSize);
    if (error == ERROR_SUCCESS)
    {
        // Make sure it's valid CameraID
        if(sensorId >= 0 && sensorId <= CSI_SENSOR_NUMBER_SUPPORTED)
        {
            if(csi_sel == CSI_SELECT_CSI0)  
            {
                gSensorInUse0 = sensorId;
            }
            else if(csi_sel == CSI_SELECT_CSI1)
            {
                gSensorInUse1 = sensorId;
            }
            else
            {
                ERRORMSG(TRUE,(TEXT("Invalid CSI Interface\r\n")));
            }
                
        }
        else
        {
            ERRORMSG(TRUE,(TEXT("Invalid Camera ID, set to default:%d\r\n"),CAMERA1_SENSOR_MODULE));
        }
    }
    else
    {
        ERRORMSG(TRUE,(TEXT("Failed to get the default CameraID [Error:0x%x]\r\n"),error));
    }

    // Close registry key
    RegCloseKey(hKey);

    BSP_CSI_FUNCTION_EXIT();

    return TRUE;

}

//------------------------------------------------------------------------------
//
// Function: BSPGetSensorFormat
//
// This function returns the sensor data format.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//      pSensorFormat
//          [out] Pointer to DWORD to hold return value of sensor format.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL BSPGetSensorFormat(CSI_SELECT csi_sel, DWORD *pSensorFormat)
{
    BOOL retVal = TRUE;

    BSP_CSI_FUNCTION_ENTRY();

    if(csi_sel == CSI_SELECT_CSI0)
    {
        DEBUGMSG(ZONE_INIT,(TEXT("%s: CSI0 sensor\r\n"),__WFUNCTION__));
        switch (gSensorInUse0)
        {
            case csiTVinId_ADV7180:   
                 *pSensorFormat = csiSensorOutputFormat_YUV422_UYVY;
                 break;

            case csiSensorId_OV5640:   
                 *pSensorFormat = csiSensorOutputFormat_YUV422_UYVY;
                 break;

			case csiSensorId_OV5642:   
                 *pSensorFormat = csiSensorOutputFormat_YUV422_UYVY;
                 break;

            case csiSensorId_OV3640:  
                 *pSensorFormat = csiSensorOutputFormat_YUV422_UYVY;
                 break; 

            case csiTestMode:
            default:
                 *pSensorFormat = csiSensorOutputFormat_YUV422_UYVY;
                 break; 
        }
    }
    else
    {
        DEBUGMSG(ZONE_INIT,(TEXT("%s: CSI1 sensor\r\n"),__WFUNCTION__));
        switch (gSensorInUse1)
        {
            case csiSensorId_OV3640:  
                 *pSensorFormat = csiSensorOutputFormat_YUV422_UYVY;
                 break; 

			case csiSensorId_OV5640:  
                 *pSensorFormat = csiSensorOutputFormat_YUV422_UYVY;
                 break; 

			case csiSensorId_OV5642:  
                 *pSensorFormat = csiSensorOutputFormat_YUV422_UYVY;
                 break; 

            case csiTestMode:
            default:
                 *pSensorFormat = csiSensorOutputFormat_YUV422_UYVY;
                 break; 
        }
    } 

    switch(*pSensorFormat)
    {
        case csiSensorOutputFormat_YUV422_UYVY:
            RETAILMSG(CSI_DEBUG_MSG,(TEXT("%s:Set Sensor output format csiSensorOutputFormat_YUV422_UYVY"),__WFUNCTION__));
            break;
        case csiSensorOutputFormat_YUV422_YUYV:
            RETAILMSG(CSI_DEBUG_MSG,(TEXT("%s:Set Sensor output format csiSensorOutputFormat_YUV422_UYVY"),__WFUNCTION__));
            break;
        case csiSensorOutputFormat_RGB565:
            RETAILMSG(CSI_DEBUG_MSG,(TEXT("%s:Set Sensor output format csiSensorOutputFormat_RGB565"),__WFUNCTION__));
            break;
        case csiSensorOutputFormat_RGB888:
            RETAILMSG(CSI_DEBUG_MSG,(TEXT("%s:Set Sensor output format csiSensorOutputFormat_RGB888"),__WFUNCTION__));
            break;
        case csiSensorOutputFormat_YUV444:
            RETAILMSG(CSI_DEBUG_MSG,(TEXT("%s:Set Sensor output format csiSensorOutputFormat_YUV444"),__WFUNCTION__));
            break;
    }

    BSP_CSI_FUNCTION_EXIT();

    return retVal;
}


//------------------------------------------------------------------------------
//
// Function: BSPGetSensorResolution
//
// This function returns the sensor output resolution.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//      pSensorFormat
//          [out] Pointer to DWORD to hold return value of sensor resolution.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL BSPGetSensorResolution(CSI_SELECT csi_sel, DWORD *pSensorResolution)
{
    BOOL retVal = TRUE;

    BSP_CSI_FUNCTION_ENTRY();

    if(csi_sel == CSI_SELECT_CSI0)
    {
        DEBUGMSG(ZONE_INIT,(TEXT("%s: CSI0 sensor\r\n"),__WFUNCTION__));
        switch (gSensorInUse0)
        {
            case csiTVinId_ADV7180: 
                 gLastTVinType = BSPGetTVinType(csi_sel);            
                 switch((TVinType)gLastTVinType)
                 {
                     case TVin_NTSC_MJ:
                         *pSensorResolution = csiSensorOutputResolution_D1_NTSC;
                         gLastTVinType = (BYTE)TVin_NTSC_MJ;
                         break;                
                     case TVin_PAL_BGHID:
                     default:
                         *pSensorResolution = csiSensorOutputResolution_D1_PAL;
                         gLastTVinType = (BYTE)TVin_PAL_BGHID;
                         break;
                 }
                 break;

            case csiSensorId_OV5640:   
                 *pSensorResolution = csiSensorOutputResolution_VGA; 
                 break;

			case csiSensorId_OV5642:   
                 *pSensorResolution = csiSensorOutputResolution_VGA; 
                 break;

            case csiSensorId_OV3640:    
                 *pSensorResolution = csiSensorOutputResolution_VGA; 
                 break; 

            case csiTestMode:
            default:
                 *pSensorResolution = csiSensorOutputResolution_VGA; 
                 break; 
        }
    }
    else
    {
        DEBUGMSG(ZONE_INIT,(TEXT("%s: CSI1 sensor\r\n"),__WFUNCTION__));
        switch (gSensorInUse1)
        {
            case csiSensorId_OV3640:   
                 *pSensorResolution = csiSensorOutputResolution_VGA; 
                 break; 

            case csiTestMode:
            default:
                 *pSensorResolution = csiSensorOutputResolution_VGA; 
                 break; 
        }
    }

    BSP_CSI_FUNCTION_EXIT(); 

    return retVal;
}

//------------------------------------------------------------------------------
//
// Function: BSPSetDigitalZoom
//
// This function sets the zoom value on the camera sensor.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//      zoom
//          [in] If 2, zoom set to 2x; if 1, zoom set to 1x
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL BSPSetDigitalZoom (CSI_SELECT csi_sel, DWORD zoom)
{
    BOOL retVal = TRUE;

    BSP_CSI_FUNCTION_ENTRY();

    if(csi_sel == CSI_SELECT_CSI0)
    {
        DEBUGMSG(ZONE_INIT,(TEXT("%s: CSI0 sensor\r\n"),__WFUNCTION__));
        switch (gSensorInUse0)
        {
            case csiTVinId_ADV7180:      
                 break;

            case csiSensorId_OV5640: 
                 retVal = CameraOV5642SetDigitalZoom(csi_sel, zoom);
                 break;

			case csiSensorId_OV5642: 
                 retVal = CameraOV5642SetDigitalZoom(csi_sel, zoom);
                 break;

            case csiSensorId_OV3640:  
                 retVal = CameraOV3640SetDigitalZoom(csi_sel, zoom);
                 break; 

            case csiTestMode:
            default:
                 break; 
        }
    }
    else
    {
        DEBUGMSG(ZONE_INIT,(TEXT("%s: CSI1 sensor\r\n"),__WFUNCTION__));
        switch (gSensorInUse1)
        {
            case csiSensorId_OV3640:  
                 retVal = CameraOV3640SetDigitalZoom(csi_sel, zoom);
                 break; 

            case csiTestMode:
            default:
                 break; 
        }
    }
       
    BSP_CSI_FUNCTION_EXIT();

    return retVal;
}


//------------------------------------------------------------------------------
//
// Function: BSPCameraSetOutputResolution
//
// This function sets the output resolution on the camera sensor.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//      dwFramerate
//          [in] Sensor frame rate
//      outputMode
//          [in] Resolution type.
//
// Returns:
//      TRUE if success
//      FALSE if failure
//
//------------------------------------------------------------------------------
BOOL BSPCameraSetOutputResolution(CSI_SELECT csi_sel, DWORD dwFramerate, csiSensorOutputResolution outputResolution)
{
    BOOL retVal = TRUE;

    BSP_CSI_FUNCTION_ENTRY();

    if(csi_sel == CSI_SELECT_CSI0)
    {
        DEBUGMSG(ZONE_INIT,(TEXT("%s: CSI0 sensor\r\n"),__WFUNCTION__));
        switch (gSensorInUse0)
        {
            case csiTVinId_ADV7180:      
                 break;

            case csiSensorId_OV5640:  
                 retVal = CameraOV5640SetOutputResolution(csi_sel,(SensorOutputFrameRate)dwFramerate,outputResolution);
                 break;

			case csiSensorId_OV5642:  
                 retVal = CameraOV5642SetOutputResolution(csi_sel,(SensorOutputFrameRate)dwFramerate,outputResolution);
                 break;

            case csiSensorId_OV3640:  
                 retVal = CameraOV3640SetOutputResolution(csi_sel,(SensorOutputFrameRate)dwFramerate,outputResolution);
                 break; 

            case csiTestMode:
            default:
                 break; 
        }
    }
    else
    {
        DEBUGMSG(ZONE_INIT,(TEXT("%s: CSI1 sensor\r\n"),__WFUNCTION__));
        switch (gSensorInUse1)
        {
            case csiSensorId_OV3640:   
                 retVal = CameraOV3640SetOutputResolution(csi_sel,(SensorOutputFrameRate)dwFramerate,outputResolution);
                 break; 
			case csiSensorId_OV5640:  
                 retVal = CameraOV5640SetOutputResolution(csi_sel,(SensorOutputFrameRate)dwFramerate,outputResolution);
                 break;
			case csiSensorId_OV5642:  
                 retVal = CameraOV5642SetOutputResolution(csi_sel,(SensorOutputFrameRate)dwFramerate,outputResolution);
                 break;

            case csiTestMode:
            default:
                 break; 
        }
    }

    BSP_CSI_FUNCTION_EXIT();

    return retVal;
}


//------------------------------------------------------------------------------
//
// Function: BSPCameraSetOutputFormat
//
// This function sets the output format on the camera sensor.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//      dwFramerate
//          [in] Sensor frame rate
//      outputFormat
//          [in] Format specified to output sensor data.
//
// Returns:
//      TRUE if success
//      FALSE if failure
//
//------------------------------------------------------------------------------
BOOL BSPCameraSetOutputFormat(CSI_SELECT csi_sel, DWORD dwFramerate, csiSensorOutputFormat outputFormat)
{
    BOOL retVal = TRUE;

    BSP_CSI_FUNCTION_ENTRY();

    if(csi_sel == CSI_SELECT_CSI0)
    {
        DEBUGMSG(ZONE_INIT,(TEXT("%s: CSI0 sensor\r\n"),__WFUNCTION__));
        switch (gSensorInUse0)
        {
            case csiTVinId_ADV7180:      
                 break;

			case csiSensorId_OV5642:
                 retVal = CameraOV5642SetOutputFormat(csi_sel,(SensorOutputFrameRate)dwFramerate,outputFormat);
                 break;

            case csiSensorId_OV3640:   
                 retVal = CameraOV3640SetOutputFormat(csi_sel,(SensorOutputFrameRate)dwFramerate,outputFormat);
                 break; 

            case csiTestMode:
            default:
                 break; 
        }
    }
    else
    {
        DEBUGMSG(ZONE_INIT,(TEXT("%s: CSI1 sensor\r\n"),__WFUNCTION__));
        switch (gSensorInUse1)
        {
            case csiSensorId_OV3640: 
                 retVal = CameraOV3640SetOutputFormat(csi_sel,(SensorOutputFrameRate)dwFramerate,outputFormat);
                 break; 

            case csiSensorId_OV5640:
                 retVal = CameraOV5640SetOutputFormat(csi_sel,(SensorOutputFrameRate)dwFramerate,outputFormat);
                 break;

            case csiTestMode:
            default:
                 break; 
        }
    }

    BSP_CSI_FUNCTION_EXIT();

    return retVal;
}


//------------------------------------------------------------------------------
//
// Function: BSPSensorSetClockGatingMode
//
// This function calls to the CRM module to
// set the clock gating mode, turning on or off
// clocks to the Camera Sesnor.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1 to set clk.
//      startClocks
//          [in] If TRUE, turn clocks to GPT on.
//                If FALSE, turn clocks to GPT off
//
// Returns:
//      TRUE if success.
//      FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL BSPSensorSetClockGatingMode(CSI_SELECT csi_sel, BOOL startClocks)
{
    BSP_CSI_FUNCTION_ENTRY();

    if (startClocks)
    {
		DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU_DI0 , 
							  DDK_CLOCK_GATE_MODE_ENABLED_ALL);

		DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU2_IPU_DI0 , 
							  DDK_CLOCK_GATE_MODE_ENABLED_ALL);

		// Enable Master Clock
	    DDKClockSetCKO1(true, DDK_CLOCK_CKO1_SRC_VIDEO_27M, DDK_CLOCK_CKO_DIV_1);
    }
    else
    {
		DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU_DI0 , 
							  DDK_CLOCK_GATE_MODE_DISABLED);

		DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU2_IPU_DI0 , 
							  DDK_CLOCK_GATE_MODE_DISABLED);

		// Disable Master Clock
	    DDKClockSetCKO1(false, DDK_CLOCK_CKO1_SRC_VIDEO_27M, DDK_CLOCK_CKO_DIV_1);
    }

    BSP_CSI_FUNCTION_EXIT();

    return TRUE;
}


//----------------------------------------------------------------------------
//
// Function: BSPSensorFlip
//
// Sets the Sensor Mirror/Flipping features.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//      doMirror
//          [in] Sensor Mirror will be set or not
//      doFlip
//          [in] Sensor Vertical flipping will be set or not
//      Default
//          [in] Sensor Mirror/Flip will be set to default for special board
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL BSPSensorMirrorFlip(CSI_SELECT csi_sel, BOOL doMirror, BOOL doFlip, BOOL Default)
{
    BOOL retVal = TRUE;

    BSP_CSI_FUNCTION_ENTRY();
    
    UNREFERENCED_PARAMETER(Default);

    if(csi_sel == CSI_SELECT_CSI0)
    {
        DEBUGMSG(ZONE_INIT,(TEXT("%s: CSI0 sensor\r\n"),__WFUNCTION__));
        switch (gSensorInUse0)
        {
            case csiTVinId_ADV7180:      
                 break;

            case csiSensorId_OV5640: 
                 retVal = CameraOV5640MirFlip(csi_sel, doMirror, doFlip);
                 break;

			case csiSensorId_OV5642: 
                 retVal = CameraOV5642MirFlip(csi_sel, doMirror, doFlip);
                 break;

            case csiSensorId_OV3640:  
                 retVal = CameraOV3640MirFlip(csi_sel, doMirror, doFlip);
                 break; 

            case csiTestMode:
            default:
                 break; 
        }
    }
    else
    {
        DEBUGMSG(ZONE_INIT,(TEXT("%s: CSI1 sensor\r\n"),__WFUNCTION__));
        switch (gSensorInUse1)
        {
            case csiSensorId_OV3640: 
                 retVal = CameraOV3640MirFlip(csi_sel, doMirror, doFlip);
                 break; 

            case csiTestMode:
            default:
                 break; 
        }
    }

    BSP_CSI_FUNCTION_EXIT();

    return retVal;
}

//----------------------------------------------------------------------------
//
// Function: BSPGetSiVer
//
// Gets the Chip Silicon Version.
//
// Parameters:
//
// Returns:
//      The Chip Silicon Version.
//------------------------------------------------------------------------------
DWORD BSPGetSiVer()
{
    DWORD dwSiVer = 0;

    if (!KernelIoControl(IOCTL_HAL_QUERY_SI_VERSION, NULL, 0,
        &dwSiVer, sizeof(dwSiVer), NULL))
    {
        ERRORMSG(1, (_T("Cannot obtain the silicon version!\r\n")));
    }

    return dwSiVer;
}

//----------------------------------------------------------------------------
//
// Function: BSPGetChip
//
// Gets the Chip .
//
// Parameters:
//
// Returns:
//      The Chip .
//------------------------------------------------------------------------------
DWORD BSPGetChip()
{

    return 2;
}

//----------------------------------------------------------------------------
//
// Function: BspGetFormats
//
// Gets the current sensor supported formats .
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//      type
//          [in] Pin number
//      formats
//          [out] Current sensor supported formats
//
// Returns:
//      The Chip .
//------------------------------------------------------------------------------
DWORD BspGetFormats(CSI_SELECT csi_sel, DWORD type, PCS_DATARANGE_VIDEO** formats)
{
    DWORD dFormats = 0;

    BSP_CSI_FUNCTION_ENTRY();

    if(csi_sel == CSI_SELECT_CSI0)
    {
        DEBUGMSG(ZONE_INIT,(TEXT("%s: CSI0 sensor\r\n"),__WFUNCTION__));
        switch (gSensorInUse0)
        {
            case csiTVinId_ADV7180:  
                 dFormats = TVInADV7180GetFormats(csi_sel, type, formats);
                 break;

            case csiSensorId_OV5640: 
                 dFormats = CameraOV5640GetFormats(csi_sel, type, formats);
                 break;

			case csiSensorId_OV5642: 
                 dFormats = CameraOV5642GetFormats(csi_sel, type, formats);
                 break;

            case csiSensorId_OV3640:
            case csiTestMode:
            default:
                 dFormats = CameraOV3640GetFormats(csi_sel, type, formats);
                 break; 
        }
    }
    else
    {
        DEBUGMSG(ZONE_INIT,(TEXT("%s: CSI1 sensor\r\n"),__WFUNCTION__));
        switch (gSensorInUse1)
        {
			case csiSensorId_OV5640: 
                 dFormats = CameraOV5640GetFormats(csi_sel, type, formats);
                 break;

			case csiSensorId_OV5642: 
                 dFormats = CameraOV5642GetFormats(csi_sel, type, formats);
                 break;

            case csiSensorId_OV3640:      
            case csiTestMode:
            default:
                 dFormats = CameraOV3640GetFormats(csi_sel, type, formats);
                 break; 
        }
    }

    BSP_CSI_FUNCTION_EXIT();

    return dFormats;
}

//------------------------------------------------------------------------------
//
// Function: BspGetSensorMode
//
// This function get  the camera sensor mode.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//
//Returns:
//      csiMode:  CSI mode
//
//------------------------------------------------------------------------------
csiMode BspGetSensorMode(CSI_SELECT csi_sel)
{
    csiMode csi_mode = CSI_GATED_CLOCK_MODE;

    BSP_CSI_FUNCTION_ENTRY();
    
    if(csi_sel == CSI_SELECT_CSI0)
    {
        switch (gSensorInUse0)
        {
            case csiTVinId_ADV7180:
                csi_mode = CSI_CCIR_INTERLACE_BT656_MODE;
                break;
    
            case csiSensorId_OV3640:
            case csiSensorId_OV5640:
            case csiSensorId_OV5642:
                csi_mode = CSI_GATED_CLOCK_MODE;
                break;

            case csiTestMode:
            default:
                // For test mode
                csi_mode = CSI_NONGATED_CLOCK_MODE;
                break;
        }
    }
    else
    {
        switch (gSensorInUse1)
        {
            case csiTVinId_ADV7180:
                csi_mode = CSI_CCIR_INTERLACE_BT656_MODE;
                break;
    
            case csiSensorId_OV3640:
            case csiSensorId_OV5640:
			case csiSensorId_OV5642:
                csi_mode = CSI_GATED_CLOCK_MODE;
                break;

            case csiTestMode:
            default:
                // For test mode
                csi_mode = CSI_NONGATED_CLOCK_MODE;
                break;
        }
    }

    BSP_CSI_FUNCTION_EXIT();

    return csi_mode;
}

//------------------------------------------------------------------------------
//
// Function: BspGetSensorDataSource
//
// This function gets the camera sensor interface.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//
//Returns:
//      csiConnectedDataSource:  CSI data source (MIPI or parallel)
//
//------------------------------------------------------------------------------
csiConnectedDataSource BspGetSensorDataSource(CSI_SELECT csi_sel)
{
	csiConnectedDataSource dataSource = csiConnectedDataSource_PARALLEL;
	csiSensorId sensorId;

	sensorId = ( (csi_sel == CSI_SELECT_CSI0) ? gSensorInUse0 : gSensorInUse1);

	BSP_CSI_FUNCTION_ENTRY();

	switch (sensorId)
	{
	// Add here other sensors using the MIPI interface
	case csiSensorId_OV5640 :
		dataSource = csiConnectedDataSource_MIPI;
		break;

	default :
		dataSource = csiConnectedDataSource_PARALLEL;
		break;
	}

    BSP_CSI_FUNCTION_EXIT();

    return dataSource;
}

//-----------------------------------------------------------------------------
//
// Function: I2CWriteOneByte
//
// This function writes a single byte byData to the register stated in byReg.
//
// Parameters:
//      hI2C
//          [in] File handle to I2C Bus Interface.
//
//      byAddr
//          [in] I2C Slave device address.
//
//      byReg
//          [in] Register Index.
//
//      byData
//          [in] Data to write to byReg.
//
//      lpiResult
//          [in] Pointer of the result. The I2C Bus will store the
//            result of the operation in location pointed to by
//            lpiResult.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
BOOL I2CWriteOneByte(HANDLE hI2C, BYTE byAddr, WORD byReg, BYTE byData, LPINT lpiResult)
{
    I2C_TRANSFER_BLOCK I2CXferBlock;
    I2C_PACKET I2CPacket;
    BYTE byOutData[3];

    //BSP_CSI_FUNCTION_ENTRY();

    byOutData[0] = (BYTE)((byReg & 0xff00) >> 8);
    byOutData[1] = (BYTE)(byReg & 0xff);
    byOutData[2] = byData;
    I2CPacket.wLen = sizeof(byOutData);
    I2CPacket.pbyBuf = (PBYTE) &byOutData;
    I2CPacket.byRW = I2C_RW_WRITE;
    I2CPacket.byAddr = byAddr;
    I2CPacket.lpiResult = lpiResult;
    I2CXferBlock.pI2CPackets = &I2CPacket;
    I2CXferBlock.iNumPackets = 1;

    if (!I2CTransfer(hI2C, &I2CXferBlock))
    {
        ERRORMSG(TRUE,(TEXT("%s:I2C Write Sensor register fail! - ERRORCode:%d \r\n"),__WFUNCTION__,*lpiResult));
        return FALSE;
    }

    //BSP_CSI_FUNCTION_EXIT();

    return TRUE;
}

// The register address in slave chip is 8bit
BOOL I2CWriteOneByte8bitAddr(HANDLE hI2C, BYTE byAddr, BYTE byReg, BYTE byData, LPINT lpiResult)
{
    I2C_TRANSFER_BLOCK I2CXferBlock;
    I2C_PACKET I2CPacket;
    BYTE byOutData[2];

    BSP_CSI_FUNCTION_ENTRY();

    byOutData[0] = byReg;
    byOutData[1] = byData;
    I2CPacket.wLen = sizeof(byOutData);
    I2CPacket.pbyBuf = (PBYTE) &byOutData;
    I2CPacket.byRW = I2C_RW_WRITE;
    I2CPacket.byAddr = byAddr;
    I2CPacket.lpiResult = lpiResult;
    I2CXferBlock.pI2CPackets = &I2CPacket;
    I2CXferBlock.iNumPackets = 1;

    if (!I2CTransfer(hI2C, &I2CXferBlock))
    {
        ERRORMSG(TRUE,(TEXT("%s:I2C Write TVCodec register fail! - ERRORCode:%d \r\n"),__WFUNCTION__,*lpiResult));
        return FALSE;
    }

    BSP_CSI_FUNCTION_EXIT();

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function: I2CReadOneByte
//
// This function read a single byte data from the register stated in byReg.
//
// Parameters:
//      hI2C
//          [in] File handle to I2C Bus Interface.
//
//      byAddr
//          [in] I2C Slave device address.
//
//      byReg
//          [in] Register Index.
//
//      lpiResult
//          [in] Pointer of the result. The I2C Bus will store the
//            result of the operation in location pointed to by
//            lpiResult.
//
// Returns:
//      The single byte content stored in byReg.
//
//-----------------------------------------------------------------------------
BYTE I2CReadOneByte(HANDLE hI2C, BYTE byAddr, WORD byReg, LPINT lpiResult)
{
    I2C_TRANSFER_BLOCK I2CXferBlock;
    I2C_PACKET I2CPacket[2];
    BYTE byOutData[2];
    BYTE byInData = 0;

    //BSP_CSI_FUNCTION_ENTRY();

    byOutData[0] = (BYTE)((byReg & 0xff00) >> 8);
    byOutData[1] = (BYTE)(byReg & 0xff);
    I2CPacket[0].pbyBuf = (PBYTE) &byOutData;
    I2CPacket[0].wLen = sizeof(byOutData);
    I2CPacket[0].byRW = I2C_RW_WRITE;
    I2CPacket[0].byAddr = byAddr;
    I2CPacket[0].lpiResult = lpiResult;

    I2CPacket[1].pbyBuf = (PBYTE) &byInData;
    I2CPacket[1].wLen = sizeof(byInData);
    I2CPacket[1].byRW = I2C_RW_READ;
    I2CPacket[1].byAddr = byAddr;
    I2CPacket[1].lpiResult = lpiResult;

    I2CXferBlock.pI2CPackets = I2CPacket;
    I2CXferBlock.iNumPackets = 2;

    if(!I2CTransfer(hI2C, &I2CXferBlock))
        ERRORMSG(TRUE,(TEXT("%s:I2C Read Sensor Register Fail\r\n"),__WFUNCTION__));

    //BSP_CSI_FUNCTION_EXIT();

    return byInData;
}

// The register address in slave chip is 8bit
BYTE I2CReadOneByte8bitAddr(HANDLE hI2C, BYTE byAddr, BYTE byReg, LPINT lpiResult)
{
    I2C_TRANSFER_BLOCK I2CXferBlock;
    I2C_PACKET I2CPacket[2];
    BYTE byOutData = 0;
    BYTE byInData = 0;

    BSP_CSI_FUNCTION_ENTRY();

    byOutData = byReg;
    I2CPacket[0].pbyBuf = (PBYTE) &byOutData;
    I2CPacket[0].wLen = sizeof(byOutData);
    I2CPacket[0].byRW = I2C_RW_WRITE;
    I2CPacket[0].byAddr = byAddr;
    I2CPacket[0].lpiResult = lpiResult;

    I2CPacket[1].pbyBuf = (PBYTE) &byInData;
    I2CPacket[1].wLen = sizeof(byInData);
    I2CPacket[1].byRW = I2C_RW_READ;
    I2CPacket[1].byAddr = byAddr;
    I2CPacket[1].lpiResult = lpiResult;

    I2CXferBlock.pI2CPackets = I2CPacket;
    I2CXferBlock.iNumPackets = 2;

    if(!I2CTransfer(hI2C, &I2CXferBlock))
        ERRORMSG(TRUE,(TEXT("%s:I2C Read TVCodec Register Fail\r\n"),__WFUNCTION__));

    BSP_CSI_FUNCTION_EXIT();

    return byInData;
}

int I2CWriteRegs(HANDLE hI2C, BYTE slaveAddr, struct reg_value *pModeSetting, int ArySize)
{
	UINT32 Delay_ms = 0;
	UINT16 RegAddr = 0;
	UINT8 Mask = 0;
	UINT8 Val = 0;
	UINT8 RegVal = 0;
	int i, retval = 0;

	for (i = 0; i < ArySize; ++i, ++pModeSetting) {
		Delay_ms = pModeSetting->u32Delay_ms;
		RegAddr = pModeSetting->u16RegAddr;
		Val = pModeSetting->u8Val;
		Mask = pModeSetting->u8Mask;

		if (Mask) {
			RegVal = I2CReadOneByte(hI2C, slaveAddr, RegAddr, &retval);

			if (retval != 0) {
				RETAILMSG(1, (L"%S: Failed in I2CReadOneByte!!\r\n", __FUNCTION__));
			}

			RegVal &= ~(BYTE)Mask;
			Val &= Mask;
			Val |= RegVal;
		}

		I2CWriteOneByte(hI2C, slaveAddr, RegAddr, Val, &retval);

		if (retval != 0) {
			RETAILMSG(1, (L"%S: Failed in I2CWriteOneByte!!\r\n", __FUNCTION__));
		}

		if (Delay_ms)
			Sleep(Delay_ms);
	}

	return retval;
}
