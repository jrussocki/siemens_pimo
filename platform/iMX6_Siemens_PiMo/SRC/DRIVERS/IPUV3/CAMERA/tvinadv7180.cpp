//
//------------------------------------------------------------------------------
//
//  Copyright (C) 2008-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//

//  File:  tvinadv7180.cpp
//
//  Definitions for TVin adv7180 module specific
//
//------------------------------------------------------------------------------
#pragma warning(disable: 4201 6244)

#include <windows.h>
#include <winddi.h>
#include <ddgpe.h>
#define __DVP_INCLUDED__

#include "Cs.h"
#include "Csmedia.h"
#include "SensorFormats.h"

#include "common_macros.h"
#include "common_ipuv3h.h"
#include "ipu_common.h"

#include "csp.h"
#include "i2cbus.h"
#include "Csi.h"
#include "CsiClass.h"
#include "bspcsi.h"
#include "tvinadv7180.h"
#include "cameradbg.h"


//------------------------------------------------------------------------------
// External Functions


//------------------------------------------------------------------------------
// External Variables
extern HANDLE      g_hI2C;


//------------------------------------------------------------------------------
// Defines
#define CSI_I2C_ADDRESS        0x20

#define ADV7180_I2C_ADDRESS    0x21  //adv7180, default 21; 
#define ADV7180_I2C_SPEED      81000
#define ADV7180_I2C_TEST       0

#define TVIN_FUNCTION_ENTRY() \
    DEBUGMSG(ZONE_FUNCTION, (TEXT("++%s\r\n"), __WFUNCTION__))
#define TVIN_FUNCTION_EXIT() \
    DEBUGMSG(ZONE_FUNCTION, (TEXT("--%s\r\n"), __WFUNCTION__))
//------------------------------------------------------------------------------
// Types


//------------------------------------------------------------------------------
// Global Variables
BYTE gLastTVinType = 0;//0-NTSC,4-PAL

//------------------------------------------------------------------------------
// Local Variables
MAKE_STREAM_MODE_RGB565(DCAM_ADV7180_StreamMode_RGB565_QQCIF, 88, -72, 16, 15);  //QQCIF
MAKE_STREAM_MODE_RGB565(DCAM_ADV7180_StreamMode_RGB565_QQVGA, 160, -120, 16, 15);  //QQVGA
MAKE_STREAM_MODE_RGB565(DCAM_ADV7180_StreamMode_RGB565_QCIF, 176, -144, 16, 15);  //QCIF
MAKE_STREAM_MODE_RGB565(DCAM_ADV7180_StreamMode_RGB565_QVGA, 320, -240, 16, 15);  //QVGA
MAKE_STREAM_MODE_RGB565(DCAM_ADV7180_StreamMode_RGB565_CIF, 352, -288, 16, 15); //CIF
MAKE_STREAM_MODE_RGB565(DCAM_ADV7180_StreamMode_RGB565_VGA, 640, -480, 16, 15);  //VGA

MAKE_STREAM_MODE_UYVY(DCAM_ADV7180_StreamMode_UYVY_D1_PAL, 720, -576, 16, 15); //PAL
MAKE_STREAM_MODE_UYVY(DCAM_ADV7180_StreamMode_UYVY_D1_NTSC, 720, -480, 16, 15);  //NTSC

//The following function for TV in module.

//------------------------------------------------------------------------------
//
// Function: TVInGetTVinType
//
// Get the DVD output type:PAL or NTSC.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//
// Returns:
//      TVin type.
//
//------------------------------------------------------------------------------
BYTE TVInGetTVinType(CSI_SELECT csi_sel)
{
    BYTE rData, temp;
    BYTE bySlaveAddr;
    INT iResult;

    TVIN_FUNCTION_ENTRY();

    switch (csi_sel)
    {
        case CSI_SELECT_CSI0:
            bySlaveAddr = ADV7180_I2C_ADDRESS;
            break;               

        default:
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: Don't support!\r\n"), __WFUNCTION__));
            return FALSE;
    } 
    
    //STATUS_1:auto dectection
    // Check the TVin-type
    //STATUS_1
    rData = I2CReadOneByte8bitAddr(g_hI2C,bySlaveAddr,0x10, &iResult);
    RETAILMSG(ADV7180_I2C_TEST,(TEXT("Status_1 is %x \n"),rData));
    
    //autodetection
    temp = (BYTE)((rData & 0x70) >> 4);    
    switch(temp)
    {
        case 0://TVin_NTSC_MJ;
            RETAILMSG(ADV7180_I2C_TEST,(TEXT("AduoDetection type is NTSC  M/J \n")));
            break;
        case 1://TVin_NTSC_443;
            RETAILMSG(ADV7180_I2C_TEST,(TEXT("AduoDetection type is NTSC  4.43 \n")));
            break;
        case 2://TVin_PAL_M;
            RETAILMSG(ADV7180_I2C_TEST,(TEXT("AduoDetection type is PAL  M \n")));      
        case 3://TVin_PAL_60;
            RETAILMSG(ADV7180_I2C_TEST,(TEXT("AduoDetection type is PAL  60 \n")));
            break;            
        case 4://TVin_PAL_BGHID;
            RETAILMSG(ADV7180_I2C_TEST,(TEXT("AduoDetection type is PAL  B/G/H/I/D \n")));
            break;           
        case 5://TVin_SECAM;
            RETAILMSG(ADV7180_I2C_TEST,(TEXT("AduoDetection type is SECAM \n")));
            break;
        case 6://TVin_PAL_CombinationN;
            RETAILMSG(ADV7180_I2C_TEST,(TEXT("AduoDetection type is PAL Combination N \n")));
            break;
        case 7://TVin_SECAM_525;
            RETAILMSG(ADV7180_I2C_TEST,(TEXT("AduoDetection type is SECAM  525 \n")));
            break;
    }
    
    TVIN_FUNCTION_EXIT();

    return temp;
}


//------------------------------------------------------------------------------
//
// Function: TVInADV7180Init
//
// Initializes the TV in sensor.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL TVInADV7180Init(CSI_SELECT csi_sel)
{
    INT iResult;
    DWORD dwFrequency;
    BYTE bySlaveAddr, byCsiAddr;
    UINT uSensorVer;
    BOOL retVal = TRUE;
    
    TVIN_FUNCTION_ENTRY();

    byCsiAddr = CSI_I2C_ADDRESS;

    switch (csi_sel)
    {
        case CSI_SELECT_CSI0:
            dwFrequency = ADV7180_I2C_SPEED;
            bySlaveAddr = ADV7180_I2C_ADDRESS;
            break;               

        default:
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: Don't support!\r\n"), __WFUNCTION__));
            return FALSE;
    }        

    if(g_hI2C == NULL)
    {
        g_hI2C = I2COpenHandle(I2C3_FID);   
        if (g_hI2C == INVALID_HANDLE_VALUE)
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: CreateFile for I2C failed!\r\n"), __WFUNCTION__));
            return FALSE;
        }

        if (!I2CSetMasterMode(g_hI2C))
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: Set I2C Master mode failed!\r\n"), __WFUNCTION__));
            return FALSE;
        }

        // Initialize the device internal fields
        if (!I2CSetFrequency(g_hI2C, dwFrequency))
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: Set HSI2C frequency failed!\r\n"), __WFUNCTION__));
            return FALSE;
        }

        if (!I2CSetSelfAddr(g_hI2C, byCsiAddr))
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: Set I2C self address failed!\r\n"), __WFUNCTION__));
            return FALSE;
        }
    }

    // Check connected sensor type
    uSensorVer = I2CReadOneByte8bitAddr(g_hI2C,bySlaveAddr,0x11, &iResult);

    if((uSensorVer == 0x18) || (uSensorVer == 0x1B) || (uSensorVer == 0x1C) || (uSensorVer == 0x1E))
    {
        DEBUGMSG(TRUE,(TEXT("%s: ADV7180 sensor detected! \r\n"), __WFUNCTION__));
    }  
    else
    {
        ERRORMSG(TRUE,(TEXT("%s: The sensor type doesn't match! \r\n"), __WFUNCTION__));
        return FALSE;
    }
    
    // Set ADV7180 registers
    // Analog Front End
    // select the input format: (YPbPr) - INSEL[3:0], INPUT SELECTION, ADDRESS 0x00 [3:0]
    I2CWriteOneByte8bitAddr(g_hI2C, bySlaveAddr, 0x00, 0x00, &iResult); 
    I2CWriteOneByte8bitAddr(g_hI2C, bySlaveAddr, 0x31, 0x02, &iResult);
    I2CWriteOneByte8bitAddr(g_hI2C, bySlaveAddr, 0x3D, 0xA2, &iResult);
    I2CWriteOneByte8bitAddr(g_hI2C, bySlaveAddr, 0x3E, 0x6A, &iResult);
    I2CWriteOneByte8bitAddr(g_hI2C, bySlaveAddr, 0x3F, 0xA0, &iResult);
    I2CWriteOneByte8bitAddr(g_hI2C, bySlaveAddr, 0x0E, 0x80, &iResult);
    I2CWriteOneByte8bitAddr(g_hI2C, bySlaveAddr, 0x55, 0x81, &iResult);
    I2CWriteOneByte8bitAddr(g_hI2C, bySlaveAddr, 0x0e, 0x00, &iResult);
    // Auto detect enable
    //I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x07, 0xff, &iResult);
    Sleep(100);

    //Get TVintype:only two type ->0-NTSC,4-PAL
    gLastTVinType = TVInGetTVinType(csi_sel);
    if (gLastTVinType == 0)
        RETAILMSG(0, (TEXT("%s: Get the TVIN type is NTSC !\r\n"), __WFUNCTION__));
    else if(gLastTVinType == 4)
        RETAILMSG(0, (TEXT("%s: Get the TVIN type is PAL !\r\n"), __WFUNCTION__));
            
    TVIN_FUNCTION_EXIT();

    return retVal;
}


//------------------------------------------------------------------------------
//
// Function: TVInADV7180Test
//
// ADV7180 chip tests itself.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//
// Returns:
//      TRUE for successful, FALSE for failed.
//
//------------------------------------------------------------------------------
BOOL TVInADV7180Test(CSI_SELECT csi_sel)
{
    CHAR Chip_Ver;    //silicon ID
    BYTE rData, temp;
    BYTE byVid_sel;
    INT iResult;
    BYTE bySlaveAddr;

    switch (csi_sel)
    {
        case CSI_SELECT_CSI0:
            bySlaveAddr = ADV7180_I2C_ADDRESS;
            break;               

        default:
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: Don't support!\r\n"), __WFUNCTION__));
            return FALSE;
    }
    
    //IDENTIFICATION
    Chip_Ver = I2CReadOneByte8bitAddr(g_hI2C,bySlaveAddr,0x11, &iResult);
    if ( Chip_Ver != 0x1B)
    {
        RETAILMSG(ADV7180_I2C_TEST,(TEXT("power-up ADV7180 should be 0x1B,Chip version = %02X\n"), Chip_Ver));
        return FALSE;
    }
    
    // VID_SEL Function
    byVid_sel = I2CReadOneByte8bitAddr(g_hI2C,bySlaveAddr,0x00, &iResult);
    byVid_sel = (BYTE)((byVid_sel & 0xf0) >> 4);
    RETAILMSG(ADV7180_I2C_TEST,(TEXT("Video selection,Vid_Sel = %02X\n"), byVid_sel));
           
    //STATUS_2
    rData = I2CReadOneByte8bitAddr(g_hI2C,bySlaveAddr,0x12, &iResult);
    RETAILMSG(ADV7180_I2C_TEST,(TEXT("Status_2 is %x \n"),rData));
    //STATUS_3
    rData = I2CReadOneByte8bitAddr(g_hI2C,bySlaveAddr,0x13, &iResult);
    temp = (rData & 0x40) << 6;
    RETAILMSG(ADV7180_I2C_TEST,(TEXT("Status_3_interlaced is %x \n"),temp));

    //AutoDetect Enable
    rData = I2CReadOneByte8bitAddr(g_hI2C,bySlaveAddr,0x07, &iResult);
    RETAILMSG(ADV7180_I2C_TEST,(TEXT("AutoDetection enable %x\n"),rData));

    return TRUE;
}

//------------------------------------------------------------------------------
//
// Function: TVInADV7180Deinit
//
// Deinitialize the TV in Sensor.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL TVInADV7180Deinit(CSI_SELECT csi_sel)
{
    BOOL retVal = TRUE;

    TVIN_FUNCTION_ENTRY();

    UNREFERENCED_PARAMETER(csi_sel);

    if(g_hI2C)
    {
        CloseHandle(g_hI2C);
        g_hI2C = NULL;
    }

    TVIN_FUNCTION_EXIT();

    return retVal;
}

//------------------------------------------------------------------------------
//
// Function: TVInADV7180SetPower
//
// Set TVin power status.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//      [In]: TRUE,Power up
//              FALSE,Power down 
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL TVInADV7180SetPower(CSI_SELECT csi_sel, BOOL powerstate)
{
    TVIN_FUNCTION_ENTRY();
    BYTE bySlaveAddr;
    INT iResult;
    BOOL retVal = TRUE;

    if (g_hI2C == NULL)
        return FALSE;

    switch (csi_sel)
    {
        case CSI_SELECT_CSI0:
            bySlaveAddr = ADV7180_I2C_ADDRESS;
            break;               

        default:
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: Don't support!\r\n"), __WFUNCTION__));
            return FALSE;
    }
    
    //PWRDWN,Address ox0F[2][5]
    if (powerstate)
    {
        //Power up
        I2CWriteOneByte8bitAddr(g_hI2C, bySlaveAddr, 0x0F, 0x00, &iResult);
        Sleep(100);
        RETAILMSG (0,(TEXT("%s: Power up 10 ms : %x\r\n"),__WFUNCTION__,powerstate));
    }
    else
    {
        //Power down
        I2CWriteOneByte8bitAddr(g_hI2C, bySlaveAddr, 0x0F, 0x24, &iResult);
        RETAILMSG (0,(TEXT("%s: Power down 0 ms : %x\r\n"),__WFUNCTION__,powerstate));
    }

    TVIN_FUNCTION_EXIT();

    return retVal;
}


//----------------------------------------------------------------------------
//
// Function: TVInADV7180GetFormats
//
// Get the sensor supported formats for specific pin
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//      type
//          [in] Camera pin number, such as capture, still, preview
//      formats
//          [out] supported formats
//
// Returns:
//      Number of supported formats
//
//------------------------------------------------------------------------------
DWORD TVInADV7180GetFormats(CSI_SELECT csi_sel, DWORD type, PCS_DATARANGE_VIDEO** formats)
{
    DWORD dFormats = 0;

    UNREFERENCED_PARAMETER(csi_sel);

    // Get counts and all formats of  current sensor supported
    if(formats != NULL)
    {
        switch(type)
        {
            case CAPTURE:
                {
                    dFormats = 2;
    
                    *formats = new PCS_DATARANGE_VIDEO[dFormats];
    
                    if( NULL == *formats )
                    {
                        return 0;
                    }
    
                    // Set sensor supported formats
                    (* formats)[0] = &DCAM_ADV7180_StreamMode_UYVY_D1_PAL;
                    (* formats)[1] = &DCAM_ADV7180_StreamMode_UYVY_D1_NTSC;
                }
                break;
    
            case STILL:
                {
                    dFormats = 2;
    
                    *formats = new PCS_DATARANGE_VIDEO[dFormats];
    
                    if( NULL == *formats )
                    {
                        return 0;
                    }
    
                    // Set sensor supported formats
                    (* formats)[0] = &DCAM_ADV7180_StreamMode_UYVY_D1_PAL;
                    (* formats)[1] = &DCAM_ADV7180_StreamMode_UYVY_D1_NTSC;
                }
                break;
    
            case PREVIEW:
                {
                    dFormats = 5;
    
                    *formats = new PCS_DATARANGE_VIDEO[dFormats];
    
                    if( NULL == *formats )
                    {
                        return 0;
                    }
    
                    // Set sensor supported formats
                    (* formats)[0] = &DCAM_ADV7180_StreamMode_RGB565_QQVGA;
                    (* formats)[1] = &DCAM_ADV7180_StreamMode_RGB565_QCIF;
                    (* formats)[2] = &DCAM_ADV7180_StreamMode_RGB565_QVGA;
                    (* formats)[3] = &DCAM_ADV7180_StreamMode_RGB565_CIF;
                    (* formats)[4] = &DCAM_ADV7180_StreamMode_RGB565_VGA;  
                }
                break;
    
            default:
                ERRORMSG(TRUE, (TEXT("%s: Invalid type parameter!\r\n"),__WFUNCTION__));
                break;
        }
    }
    // Only get the counts of supported
    else
    {
        switch(type)
        {
            case CAPTURE:
                dFormats = 2;
                break;
        
            case STILL:
                dFormats = 2;
                break;
        
            case PREVIEW:
                dFormats = 5;
                break;
        
            default:
                ERRORMSG(TRUE, (TEXT("%s: Invalid type parameter!\r\n"),__WFUNCTION__));
                break;
        }

    }

    return dFormats;
}

