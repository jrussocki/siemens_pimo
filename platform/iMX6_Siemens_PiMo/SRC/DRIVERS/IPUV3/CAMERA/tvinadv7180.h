//
//------------------------------------------------------------------------------
//
//  Copyright (C) 2008-2011 Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//

//  File:  tvinadv7180.h
//
//  Definitions for TVin adv7180.
//
//------------------------------------------------------------------------------

#ifndef __TVINADV7180_H__
#define __TVINADV7180_H__

//------------------------------------------------------------------------------
// Defines


//------------------------------------------------------------------------------
// Types
// The TV type
typedef enum TVinTypeEnum
{
    TVin_NTSC_MJ = 0 ,
    TVin_NTSC_443,
    TVin_PAL_M,
    TVin_PAL_60,
    TVin_PAL_BGHID,
    TVin_SECAM,
    TVin_PAL_CombinationN,
    TVin_SECAM_525
} TVinType;  

//------------------------------------------------------------------------------
// Functions

//The following for TVin Card.
//------------------------------------------------------------------------------
// Defines



//------------------------------------------------------------------------------
// Functions
BOOL TVInADV7180Init(CSI_SELECT csi_sel);
BOOL TVInADV7180Test(CSI_SELECT csi_sel);
BOOL TVInADV7180Deinit(CSI_SELECT csi_sel);
BYTE TVInGetTVinType(CSI_SELECT csi_sel);
BOOL TVInADV7180SetPower(CSI_SELECT csi_sel, BOOL powerstate);
DWORD TVInADV7180GetFormats(CSI_SELECT csi_sel, DWORD type, PCS_DATARANGE_VIDEO** formats);
#endif   // __TVINADV7180_H__



