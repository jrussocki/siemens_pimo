//------------------------------------------------------------------------------
//
// Copyright (C) 2008-2011 Freescale Semiconductor, Inc. All Rights Reserved.
// THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
// AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT 
//
//------------------------------------------------------------------------------
//
//  File:  CameraOV5640.h
//
//  Definitions for OV5640 Chip Camera Sensor.
//
//------------------------------------------------------------------------------


#ifndef __CAMERAOV5640_H__
#define __CAMERAOV5640_H__

#define OV5640_CSI_SELECT CSI_SELECT_CSI1

//------------------------------------------------------------------------------
// Defines

//------------------------------------------------------------------------------
// Types

//------------------------------------------------------------------------------
// Functions

BOOL CameraOV5640Init(CSI_SELECT csi_sel, SensorOutputFrameRate SensorFramerate);
BOOL CameraOV5640Deinit(CSI_SELECT csi_sel);
BOOL CameraOV5640Set15fps(CSI_SELECT csi_sel);
BOOL CameraOV5640Set30fps(CSI_SELECT csi_sel);
BOOL CameraOV5640SetOutputResolution(CSI_SELECT csi_sel, SensorOutputFrameRate SensorFramerate, csiSensorOutputResolution outputResolution);
BOOL CameraOV5640SetOutputFormat(CSI_SELECT csi_sel, SensorOutputFrameRate SensorFramerate, csiSensorOutputFormat outputFormat);
BOOL CameraOV5640SetDigitalZoom(CSI_SELECT csi_sel, DWORD ZoomLevel);
BOOL CameraOV5640MirFlip(CSI_SELECT csi_sel, BOOL doMirror, BOOL doFlip);
DWORD CameraOV5640GetFormats(CSI_SELECT csi_sel, DWORD type, PCS_DATARANGE_VIDEO** formats);
#endif   // __CAMERAOV5640_H__

