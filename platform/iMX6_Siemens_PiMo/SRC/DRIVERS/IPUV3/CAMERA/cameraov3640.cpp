//------------------------------------------------------------------------------
//
// Copyright (C) 2008-2011 Freescale Semiconductor, Inc. All Rights Reserved.
// THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
// AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT 
//
//------------------------------------------------------------------------------
//
//  File:  CameraOV3640.cpp    
//
//  Definitions for OminVisionChip camera module specific
//
//------------------------------------------------------------------------------
#pragma warning(disable: 4201 6244)

#include <windows.h>
#include <winddi.h>
#include <ddgpe.h>
#define __DVP_INCLUDED__

#include "Cs.h"
#include "Csmedia.h"
#include "SensorFormats.h"

#include "common_macros.h"
#include "common_ipuv3h.h"
#include "ipu_common.h"

#include "csp.h"
#include "i2cbus.h"
#include "Csi.h"
#include "CsiClass.h"
#include "bspcsi.h"
#include "CameraOV3640.h"
#include "cameradbg.h"


//------------------------------------------------------------------------------
// External Functions


//------------------------------------------------------------------------------
// External Variables
extern HANDLE      g_hI2C;


//------------------------------------------------------------------------------
// Defines
#define CSI_I2C_ADDRESS   0x20

#define OV3640_I2C_ADDRESS        0x3c
#define OV3640_I2C_SPEED          400000

#define CAMERA_FUNCTION_ENTRY() \
    DEBUGMSG(ZONE_FUNCTION, (TEXT("++%s\r\n"), __WFUNCTION__))
#define CAMERA_FUNCTION_EXIT() \
    DEBUGMSG(ZONE_FUNCTION, (TEXT("--%s\r\n"), __WFUNCTION__)) 

//------------------------------------------------------------------------------
// Types


//------------------------------------------------------------------------------
// Global Variables


//------------------------------------------------------------------------------
// Local Variables
MAKE_STREAM_MODE_RGB565(DCAM_OV3640_StreamMode_RGB565_QQCIF, 88, -72, 16, 15);  //QQCIF
MAKE_STREAM_MODE_RGB565(DCAM_OV3640_StreamMode_RGB565_QQVGA, 160, -120, 16, 15);  //QQVGA
MAKE_STREAM_MODE_RGB565(DCAM_OV3640_StreamMode_RGB565_QCIF, 176, -144, 16, 15);  //QCIF
MAKE_STREAM_MODE_RGB565(DCAM_OV3640_StreamMode_RGB565_QVGA, 320, -240, 16, 15);  //QVGA
MAKE_STREAM_MODE_RGB565(DCAM_OV3640_StreamMode_RGB565_CIF, 352, -288, 16, 15); //CIF
MAKE_STREAM_MODE_RGB565(DCAM_OV3640_StreamMode_RGB565_VGA, 640, -480, 16, 15);  //VGA
MAKE_STREAM_MODE_RGB565(DCAM_OV3640_StreamMode_RGB565_D1_PAL, 720, -576, 16, 15); //PAL
MAKE_STREAM_MODE_RGB565(DCAM_OV3640_StreamMode_RGB565_D1_NTSC, 720, -480, 16, 15);  //NTSC

MAKE_STREAM_MODE_YV12(DCAM_OV3640_StreamMode_YV12_QQCIF, 88, -72, 12, 15); //QQCIF
MAKE_STREAM_MODE_YV12(DCAM_OV3640_StreamMode_YV12_QQVGA, 160, -120, 12, 15); //QQVGA
MAKE_STREAM_MODE_YV12(DCAM_OV3640_StreamMode_YV12_QCIF, 176, -144, 12, 15);  //QCIF  
MAKE_STREAM_MODE_YV12(DCAM_OV3640_StreamMode_YV12_QVGA, 320, -240, 12, 15);  //QVGA
MAKE_STREAM_MODE_YV12(DCAM_OV3640_StreamMode_YV12_CIF, 352, -288, 12, 15);  //CIF
MAKE_STREAM_MODE_YV12(DCAM_OV3640_StreamMode_YV12_VGA, 640, -480, 12, 15);  //VGA
MAKE_STREAM_MODE_YV12(DCAM_OV3640_StreamMode_YV12_D1_PAL, 720, -576, 12, 15); //PAL
MAKE_STREAM_MODE_YV12(DCAM_OV3640_StreamMode_YV12_D1_NTSC, 720, -480, 12, 15);  //NTSC

MAKE_STREAM_MODE_NV12(DCAM_OV3640_StreamMode_NV12_QQCIF, 88, -72, 12, 15); //QQCIF
MAKE_STREAM_MODE_NV12(DCAM_OV3640_StreamMode_NV12_QQVGA, 160, -120, 12, 15); //QQVGA
MAKE_STREAM_MODE_NV12(DCAM_OV3640_StreamMode_NV12_QCIF, 176, -144, 12, 15);  //QCIF  
MAKE_STREAM_MODE_NV12(DCAM_OV3640_StreamMode_NV12_QVGA, 320, -240, 12, 15);  //QVGA
MAKE_STREAM_MODE_NV12(DCAM_OV3640_StreamMode_NV12_CIF, 352, -288, 12, 15);  //CIF
MAKE_STREAM_MODE_NV12(DCAM_OV3640_StreamMode_NV12_VGA, 640, -480, 12, 15);  //VGA
MAKE_STREAM_MODE_NV12(DCAM_OV3640_StreamMode_NV12_D1_PAL, 720, -576, 12, 15); //PAL
MAKE_STREAM_MODE_NV12(DCAM_OV3640_StreamMode_NV12_D1_NTSC, 720, -480, 12, 15);  //NTSC

MAKE_STREAM_MODE_UYVY(DCAM_OV3640_StreamMode_UYVY_QQCIF, 88, -72, 16, 15); //QQCIF
MAKE_STREAM_MODE_UYVY(DCAM_OV3640_StreamMode_UYVY_QQVGA, 160, -120, 16, 15); //QQVGA
MAKE_STREAM_MODE_UYVY(DCAM_OV3640_StreamMode_UYVY_QCIF, 176, -144, 16, 15);  //QCIF  
MAKE_STREAM_MODE_UYVY(DCAM_OV3640_StreamMode_UYVY_QVGA, 320, -240, 16, 15);  //QVGA
MAKE_STREAM_MODE_UYVY(DCAM_OV3640_StreamMode_UYVY_CIF, 352, -288, 16, 15);  //CIF
MAKE_STREAM_MODE_UYVY(DCAM_OV3640_StreamMode_UYVY_VGA, 640, -480, 16, 15);  //VGA
MAKE_STREAM_MODE_UYVY(DCAM_OV3640_StreamMode_UYVY_D1_PAL, 720, -576, 16, 15); //PAL
MAKE_STREAM_MODE_UYVY(DCAM_OV3640_StreamMode_UYVY_D1_NTSC, 720, -480, 16, 15);  //NTSC

MAKE_STREAM_MODE_YUY2(DCAM_OV3640_StreamMode_YUY2_QQCIF, 88, -72, 16, 15); //QQCIF
MAKE_STREAM_MODE_YUY2(DCAM_OV3640_StreamMode_YUY2_QQVGA, 160, -120, 16, 15); //QQVGA
MAKE_STREAM_MODE_YUY2(DCAM_OV3640_StreamMode_YUY2_QCIF, 176, -144, 16, 15);  //QCIF  
MAKE_STREAM_MODE_YUY2(DCAM_OV3640_StreamMode_YUY2_QVGA, 320, -240, 16, 15);  //QVGA
MAKE_STREAM_MODE_YUY2(DCAM_OV3640_StreamMode_YUY2_CIF, 352, -288, 16, 15);  //CIF
MAKE_STREAM_MODE_YUY2(DCAM_OV3640_StreamMode_YUY2_VGA, 640, -480, 16, 15);  //VGA
MAKE_STREAM_MODE_YUY2(DCAM_OV3640_StreamMode_YUY2_D1_PAL, 720, -576, 16, 15); //PAL
MAKE_STREAM_MODE_YUY2(DCAM_OV3640_StreamMode_YUY2_D1_NTSC, 720, -480, 16, 15);  //NTSC

//------------------------------------------------------------------------------
// Local Functions


//------------------------------------------------------------------------------
//
// Function: CameraOV3640Init
//
// Initializes the OV3640 camera sensor.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//      SensorFramerate
//          [in] Sensor frame rate
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL CameraOV3640Init(CSI_SELECT csi_sel, SensorOutputFrameRate SensorFramerate)
{
    DWORD dwFrequency;
    BYTE bySlaveAddr, byCsiAddr;
    INT  iResult;
    BYTE byRead;
    UINT uSensorVer;
    BOOL retVal = TRUE;
    
    CAMERA_FUNCTION_ENTRY();

    byCsiAddr = CSI_I2C_ADDRESS;

    switch (csi_sel)
    {
        case CSI_SELECT_CSI0:
             dwFrequency = OV3640_I2C_SPEED;
             bySlaveAddr = OV3640_I2C_ADDRESS;
             break;  

        case CSI_SELECT_CSI1:
             dwFrequency = OV3640_I2C_SPEED;
             bySlaveAddr = OV3640_I2C_ADDRESS;
             break;  

        default:
             DEBUGMSG(ZONE_ERROR, (TEXT("%s: Don't support!\r\n"), __WFUNCTION__));
             return FALSE;
    }        

    if(g_hI2C == NULL)
    {
        g_hI2C = I2COpenHandle(I2C3_FID);   
        if (g_hI2C == INVALID_HANDLE_VALUE)
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: CreateFile for I2C failed!\r\n"), __WFUNCTION__));
            return FALSE;
        }

        if (!I2CSetMasterMode(g_hI2C))
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: Set I2C Master mode failed!\r\n"), __WFUNCTION__));
            return FALSE;
        }

        // Initialize the device internal fields
        if (!I2CSetFrequency(g_hI2C, dwFrequency))
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: Set HSI2C frequency failed!\r\n"), __WFUNCTION__));
            return FALSE;
        }

        if (!I2CSetSelfAddr(g_hI2C, byCsiAddr))
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: Set I2C self address failed!\r\n"), __WFUNCTION__));
            return FALSE;
        }
    }

    // Check connected sensor type
    byRead = I2CReadOneByte(g_hI2C, bySlaveAddr, 0x300a, &iResult);
    uSensorVer = byRead << 8;
    byRead = I2CReadOneByte(g_hI2C, bySlaveAddr, 0x300b, &iResult);
    uSensorVer |= byRead;

    if((uSensorVer == 0x3640) || (uSensorVer == 0x3641) || (uSensorVer == 0x364c))
    {
        DEBUGMSG(TRUE,(TEXT("%s: Ov3640 sensor detected! \r\n"), __WFUNCTION__));
    }  
    else
    {
        ERRORMSG(TRUE,(TEXT("%s: The sensor type doesn't match! \r\n"), __WFUNCTION__));
        return FALSE;
    }

    switch(SensorFramerate)
    {
        case SensorOutputFrameRate30fps:
            retVal = CameraOV3640Set30fps(csi_sel);
            break;
        case SensorOutputFrameRate15fps:
            retVal = CameraOV3640Set15fps(csi_sel);
            break;

        default:
            ERRORMSG(TRUE,(TEXT("%s: Set error sensor output framerate!\r\n"), __WFUNCTION__));
            retVal = FALSE;
            break;
    }

    return retVal;
}


//------------------------------------------------------------------------------
//
// Function: CameraOV3640Deinit
//
// Deinitializes the OV3640 camera sensor.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL CameraOV3640Deinit(CSI_SELECT csi_sel)
{
    BOOL retVal = TRUE;

    CAMERA_FUNCTION_ENTRY();

    UNREFERENCED_PARAMETER(csi_sel);

    if(g_hI2C)
    {
        CloseHandle(g_hI2C);
        g_hI2C = NULL;
    }

    CAMERA_FUNCTION_EXIT();

    return retVal;
}


//------------------------------------------------------------------------------
//
// Function: CameraOV3640Set15fps
//
// Set sensor output frame rate is 15fps.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL CameraOV3640Set15fps(CSI_SELECT csi_sel)
{
    INT iResult;
    BYTE bySlaveAddr;
    BOOL retVal = TRUE;

    CAMERA_FUNCTION_ENTRY();
    switch (csi_sel)
    {
        case CSI_SELECT_CSI0:
             bySlaveAddr = OV3640_I2C_ADDRESS;
             break;

        case CSI_SELECT_CSI1:
             bySlaveAddr = OV3640_I2C_ADDRESS;
             break;

        default:
             DEBUGMSG(ZONE_ERROR, (TEXT("%s: Don't support!\r\n"), __WFUNCTION__));
             return FALSE;
    }   
    
    // ;3640 QXGA 15fps RGB888
    {   
        //Reset
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3012,0x80, &iResult);
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x304d,0x45, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30a7,0x5e, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3087,0x16, &iResult);
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x309c,0x1a, &iResult);  
        
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30a2,0xe4, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30aa,0x42, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30b0,0xff, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30b1,0xff, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30b2,0x10, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x300e,0x32, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x300f,0x21, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3010,0x20, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3011,0x00, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x304c,0x81, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30d7,0x10, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30d9,0x0d, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30db,0x08, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3016,0x82, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3018,0x38, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3019,0x30, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x301a,0x61, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x307d,0x00, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3087,0x02, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3082,0x20, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3015,0x12, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3014,0x04, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3013,0xf7, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x303c,0x08, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x303d,0x18, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x303e,0x06, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x303f,0x0c, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3030,0x62, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3031,0x26, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3032,0xe6, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3033,0x6e, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3034,0xea, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3035,0xae, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3036,0xa6, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3037,0x6a, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3104,0x02, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3105,0xfd, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3106,0x00, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3107,0xff, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3300,0x12, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3301,0xde, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3302,0xcf, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3312,0x26, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3314,0x42, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3313,0x2b, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3315,0x42, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3310,0xd0, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3311,0xbd, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x330c,0x18, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x330d,0x18, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x330e,0x56, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x330f,0x5c, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x330b,0x1c, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3306,0x5c, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3307,0x11, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x336a,0x52, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3370,0x46, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3376,0x38, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30b8,0x20, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30b9,0x17, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30ba,0x04, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30bb,0x08, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3507,0x06, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x350a,0x4f, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3100,0x02, &iResult); 
        
        // RGB_888
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3301,0xde, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3304,0x00, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3400,0x01, &iResult);
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3404,0x1d, &iResult);          
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600,0xc4, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3302,0xef, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3020,0x01, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3021,0x1d, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3022,0x00, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3023,0x0a, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3024,0x08, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3025,0x18, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3026,0x06, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3027,0x0c, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x335f,0x68, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3360,0x18, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3361,0x0c, &iResult);

        //QXGA
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3362,0x68, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3363,0x08, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3364,0x04, &iResult);  
        
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3403,0x42, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3088,0x08, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3089,0x00, &iResult);
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308a,0x06, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308b,0x00, &iResult);
        
        //default value:Horizotal Mirror Disable,Vertical Flip Disable
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x307c,0x10, &iResult);
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3090,0xc0, &iResult);
        
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x304c,0x84, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308d,0x04, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3086,0x03, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3086,0x00, &iResult);
    }   

    CAMERA_FUNCTION_EXIT();

    return retVal;
}

//------------------------------------------------------------------------------
//
// Function: CameraOV3640Set30fps
//
// Set sensor output frame rate is 30fps.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL CameraOV3640Set30fps(CSI_SELECT csi_sel)
{
    INT iResult = 0;
    BYTE bySlaveAddr;
    BYTE byRead;
    BOOL retVal = TRUE;

    CAMERA_FUNCTION_ENTRY();

    switch (csi_sel)
    {
        case CSI_SELECT_CSI0:
             bySlaveAddr = OV3640_I2C_ADDRESS;
             break; 

        case CSI_SELECT_CSI1:
             bySlaveAddr = OV3640_I2C_ADDRESS;
             break;

        default:
             DEBUGMSG(ZONE_ERROR, (TEXT("%s: Don't support!\r\n"), __WFUNCTION__));
             return FALSE;
    }   
    
    // ;3640 QXGA-->XGA--> VGA30fps RGB565
    {   
        //Reset
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3012,0x80, &iResult);
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3012,0x10, &iResult);
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x304d,0x45, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30a7,0x5e, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3087,0x16, &iResult);
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x309c,0x1a, &iResult);  

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30a2,0xe4, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30aa,0x42, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30b0,0xff, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30b1,0xff, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30b2,0x10, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x300e,0x32, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x300f,0x21, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3010,0x20, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3011,0x01, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x304c,0x82, &iResult);

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30d7,0x10, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30d9,0x0d, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30db,0x08, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3016,0x82, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3018,0x38, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3019,0x30, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x301a,0x61, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x307d,0x00, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3087,0x02, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3082,0x20, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3015,0x12, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3014,0x0c, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3013,0xf7, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x303c,0x08, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x303d,0x18, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x303e,0x06, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x303f,0x0c, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3030,0x62, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3031,0x26, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3032,0xe6, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3033,0x6e, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3034,0xea, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3035,0xae, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3036,0xa6, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3037,0x6a, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3104,0x02, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3105,0xfd, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3106,0x00, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3107,0xff, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3300,0x12, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3301,0xde, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3302,0xcf, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3312,0x26, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3314,0x42, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3313,0x2b, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3315,0x42, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3310,0xd0, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3311,0xbd, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x330c,0x18, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x330d,0x18, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x330e,0x56, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x330f,0x5c, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x330b,0x1c, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3306,0x5c, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3307,0x11, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x336a,0x52, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3370,0x46, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3376,0x38, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3300,0x13, &iResult);
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30b8,0x20, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30b9,0x17, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30ba,0x04, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30bb,0x08, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3100,0x02, &iResult); 
        
        // RGB_888
        byRead = I2CReadOneByte(g_hI2C,bySlaveAddr,0x3301,&iResult);
        byRead = byRead & ((BYTE)(~0x30)) | 0x10;
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3301,byRead, &iResult);
        byRead = I2CReadOneByte(g_hI2C,bySlaveAddr,0x3304,&iResult);
        byRead = byRead & ((BYTE)(~0x03)) | 0x00 ;
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3304,byRead, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3400,0x01, &iResult);
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3404,0x1d, &iResult);          
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600,0xc0, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308d,0x04, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3086,0x03, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3086,0x00, &iResult); 

        // 3640->XGA@30fps 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3012,0x10, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3023,0x06, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3026,0x03, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3027,0x04, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x302a,0x03, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x302b,0x10, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3075,0x24, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x300d,0x01, &iResult); 
        byRead = I2CReadOneByte(g_hI2C,bySlaveAddr,0x30d7,&iResult);
        byRead = byRead & ((BYTE)(~0x80)) | 0x80;
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x30d7,byRead, &iResult);
        byRead = I2CReadOneByte(g_hI2C,bySlaveAddr,0x3069,&iResult);
        byRead = byRead & ((BYTE)(~0x40)) | 0x00 ;
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3069,byRead, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x303e,0x00, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x303f,0xc0, &iResult);

        byRead = I2CReadOneByte(g_hI2C,bySlaveAddr,0x3302,&iResult);
        byRead = byRead & ((BYTE)(~0x20)) | 0x20;
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3302,byRead, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x335f,0x34, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3360,0x0c, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3361,0x04, &iResult); 

        //XGA
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3362,0x34, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3363,0x08, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3364,0x04, &iResult);  
        
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3403,0x42, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3088,0x04, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3089,0x00, &iResult);
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308a,0x03, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308b,0x00, &iResult);

        //VGA
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3362,0x12, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3363,0x88, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3364,0xe4, &iResult);  
        
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3403,0x42, &iResult); 

        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3088,0x02, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3089,0x80, &iResult);
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308a,0x01, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308b,0xe0, &iResult);
        
        //default value:Horizotal Mirror Disable,Vertical Flip Disable
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x307c,0x10, &iResult);
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3090,0xc0, &iResult);

        //clock
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x300e,0x37, &iResult); 
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x300f,0xe1, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3010,0x22, &iResult);  
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3011,0x01, &iResult);
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x304c,0x84, &iResult);

        //switch off auto frame rate
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3014,0x04, &iResult);
        //IF auto frame rate is enabled do not allow any dummy frames
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3015,0x02, &iResult);
        //reset dummy lines
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x302e,0x00, &iResult);
        //reset dummy lines
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x302d,0x00, &iResult);
    }

    CAMERA_FUNCTION_EXIT();

    return retVal;
}

//------------------------------------------------------------------------------
//
// Function: CameraOV3640SetOutputFormat
//
// Set interface to configure camera output format.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//      SensorFramerate
//          [in] Sensor frame rate
//      outputFormat
//          [in] structure indicating the sensor output format:
//              csiSensorOutputFormat_RGB888 - RGB888
//              csiSensorOutputFormat_RGB565 - RGB565
//              csiSensorOutputFormat_YUV422_UYVY - UYVY
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL CameraOV3640SetOutputFormat(CSI_SELECT csi_sel, SensorOutputFrameRate SensorFramerate, csiSensorOutputFormat outputFormat)
{
    INT iResult;
    BYTE bySlaveAddr;
    BYTE byRead;
    BOOL retVal = TRUE;

    CAMERA_FUNCTION_ENTRY();

    switch (csi_sel)
    {
        case CSI_SELECT_CSI0:
             bySlaveAddr = OV3640_I2C_ADDRESS;
             break; 

        case CSI_SELECT_CSI1:
             bySlaveAddr = OV3640_I2C_ADDRESS;
             break;

        default:
             DEBUGMSG(ZONE_ERROR, (TEXT("%s: Don't support!\r\n"), __WFUNCTION__));
             return FALSE;
    }      

    switch (outputFormat)
    {
        case csiSensorOutputFormat_RGB888:
        {
            switch(SensorFramerate)
            {
                case SensorOutputFrameRate30fps:
                {
                    byRead = I2CReadOneByte(g_hI2C,bySlaveAddr,0x3301,&iResult);
                    byRead = byRead & ((BYTE)(~0x30)) | 0x10;
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3301,byRead, &iResult);
                    byRead = I2CReadOneByte(g_hI2C,bySlaveAddr,0x3304,&iResult);
                    byRead = byRead & ((BYTE)(~0x03)) | 0x00 ;
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3304,byRead, &iResult); 
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3400,0x01, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3404,0x1d, &iResult);          
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600,0xc0, &iResult); 
                }
                break;

                case SensorOutputFrameRate15fps:
                {
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3301,0xde, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3304,0x00, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3400,0x01, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3404,0x1d, &iResult); 
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600,0xc4, &iResult);            
                }
                break;
            }
        }
        break;

        case csiSensorOutputFormat_RGB565:  
        {
            switch(SensorFramerate)
            {
                case SensorOutputFrameRate30fps:
                {
                    byRead = I2CReadOneByte(g_hI2C,bySlaveAddr,0x3301,&iResult);
                    byRead = byRead & ((BYTE)(~0x30)) | 0x10;
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3301,byRead, &iResult);
                    byRead = I2CReadOneByte(g_hI2C,bySlaveAddr,0x3304,&iResult);
                    byRead = byRead & ((BYTE)(~0x03)) | 0x00 ; 
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3304,byRead, &iResult); 
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3400,0x01, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3404,0x11, &iResult);          
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600,0xc0, &iResult); 
                }
                break;

                case SensorOutputFrameRate15fps:
                {
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3301,0xde, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3304,0x00, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3400,0x01, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3404,0x11, &iResult); 
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600,0xc4, &iResult);
                }
                break;
            }
        }
        break;

        case csiSensorOutputFormat_YUV444:
        {
            switch(SensorFramerate)
            {
                case SensorOutputFrameRate30fps:
                {
                    byRead = I2CReadOneByte(g_hI2C,bySlaveAddr,0x3301,&iResult);
                    byRead = byRead & ((BYTE)(~0x30)) | 0x10;
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3301,byRead, &iResult);
                    byRead = I2CReadOneByte(g_hI2C,bySlaveAddr,0x3304,&iResult);
                    byRead = byRead & ((BYTE)(~0x03)) | 0x00 ; 
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3304,byRead, &iResult); 
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3400,0x00, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3404,0x0e, &iResult);          
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600,0xc0, &iResult); 
                }
                break;

                case SensorOutputFrameRate15fps:
                {
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3301,0xde, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3304,0x00, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3400,0x00, &iResult); 
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3404,0x0e, &iResult);   
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600,0xc4, &iResult);
                }
                break;
            }
        }
        break;

        case csiSensorOutputFormat_YUV422_UYVY:
        {
            switch(SensorFramerate)
            {
                case SensorOutputFrameRate30fps:
                {
                    byRead = I2CReadOneByte(g_hI2C,bySlaveAddr,0x3301,&iResult);
                    byRead = byRead & ((BYTE)(~0x30)) | 0x10;
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3301,byRead, &iResult);
                    byRead = I2CReadOneByte(g_hI2C,bySlaveAddr,0x3304,&iResult);
                    byRead = byRead & ((BYTE)(~0x03)) | 0x00 ;
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3304,byRead, &iResult); 
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3400,0x00, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3404,0x42, &iResult);          
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600,0xc0, &iResult); 
                }
                break;

                case SensorOutputFrameRate15fps:
                {
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3301,0xde, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3304,0x00, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3400,0x00, &iResult); 
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3404,0x42, &iResult);   
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600,0xc4, &iResult);
                }
                break;
            }
        }
        break;

        case csiSensorOutputFormat_YUV422_YUYV:
        {
            switch(SensorFramerate)
            {
                case SensorOutputFrameRate30fps:
                {
                    byRead = I2CReadOneByte(g_hI2C,bySlaveAddr,0x3301,&iResult);
                    byRead = byRead & ((BYTE)(~0x30)) | 0x10;
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3301,byRead, &iResult);
                    byRead = I2CReadOneByte(g_hI2C,bySlaveAddr,0x3304,&iResult);
                    byRead = byRead & ((BYTE)(~0x03)) | 0x00 ;
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3304,byRead, &iResult);  
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3400,0x00, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3404,0x40, &iResult);          
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600,0xc0, &iResult); 
                }
                break;

                case SensorOutputFrameRate15fps:
                {
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3301,0xde, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3304,0x00, &iResult);
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3400,0x00, &iResult); 
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3404,0x40, &iResult);   
                    I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3600,0xc4, &iResult);
                }
                break;
            }
        }
        break;

     default:
        DEBUGMSG(ZONE_ERROR, (TEXT("%s: We don't support this kind of format!\r\n"), __WFUNCTION__));
        retVal = FALSE;
        break;
    }

    CAMERA_FUNCTION_EXIT();

    return retVal;
}


//------------------------------------------------------------------------------
//
// Function: CameraOV3640SetOutputResolution
//
// Set interface to configure camera output resolution.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//      SensorFramerate
//          [in] Sensor frame rate
//      outputResolution
//          [in]  Structure indicating the camera output resolution:
//              csiSensorOutputResolution outputResolution :
//              csiSensorOutputResolution_VGA - VGA 640x480
//              csiSensorOutputResolution_XGA - XGA 1280x960
//              csiSensorOutputResolution_SXGA - SXGA 1280x1024
//              csiSensorOutputResolution_QXGA - QXGA 2048*1536
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL CameraOV3640SetOutputResolution(CSI_SELECT csi_sel, SensorOutputFrameRate SensorFramerate, csiSensorOutputResolution outputResolution)
{
    INT iResult;
    BYTE bySlaveAddr;
    BOOL retVal = TRUE;

    CAMERA_FUNCTION_ENTRY();

    UNREFERENCED_PARAMETER(SensorFramerate);

    switch (csi_sel)
    {
        case CSI_SELECT_CSI0:
             bySlaveAddr = OV3640_I2C_ADDRESS;
             break;

        case CSI_SELECT_CSI1:
             bySlaveAddr = OV3640_I2C_ADDRESS;
             break;

        default:
             DEBUGMSG(ZONE_ERROR, (TEXT("%s: Don't support!\r\n"), __WFUNCTION__));
             return FALSE;
    }        

    switch (outputResolution)
    {
        
        case csiSensorOutputResolution_QXGA:

            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3362,0x68, &iResult); 
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3363,0x08, &iResult); 
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3364,0x04, &iResult);  
            
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3403,0x42, &iResult); 

            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3088,0x08, &iResult);  
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3089,0x00, &iResult);
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308a,0x06, &iResult);  
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308b,0x00, &iResult);

            break;

        case csiSensorOutputResolution_XGA:
             
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3362,0x34, &iResult); 
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3363,0x08, &iResult); 
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3364,0x04, &iResult);  
             
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3403,0x42, &iResult); 
             
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3088,0x04, &iResult);  
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3089,0x00, &iResult);
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308a,0x03, &iResult);  
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308b,0x00, &iResult);
             break;

        case csiSensorOutputResolution_VGA:
             
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3362,0x12, &iResult); 
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3363,0x88, &iResult); 
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3364,0xe4, &iResult);  
             
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3403,0x42, &iResult); 
             
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3088,0x02, &iResult);  
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3089,0x80, &iResult);
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308a,0x01, &iResult);  
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308b,0xe0, &iResult);

             break;

        case csiSensorOutputResolution_QVGA:
             
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3362,0x01, &iResult); 
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3363,0x48, &iResult); 
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3364,0xf4, &iResult);  
             
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3403,0x42, &iResult); 
             
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3088,0x01, &iResult);  
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3089,0x40, &iResult);
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308a,0x00, &iResult);  
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308b,0xf0, &iResult);

             break;

        case csiSensorOutputResolution_D1_PAL:
             
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3362,0x22, &iResult); 
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3363,0xd8, &iResult); 
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3364,0x44, &iResult);  
             
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3403,0x42, &iResult); 
             
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3088,0x02, &iResult);  
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3089,0xd0, &iResult);
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308a,0x02, &iResult);  
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308b,0x40, &iResult);

             break;

        case csiSensorOutputResolution_D1_NTSC:
             
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3362,0x12, &iResult); 
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3363,0xD8, &iResult); 
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3364,0xe4, &iResult);  
             
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3403,0x42, &iResult); 
             
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3088,0x02, &iResult);  
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3089,0xd0, &iResult);
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308a,0x01, &iResult);  
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308b,0xe0, &iResult);

             break;

        case csiSensorOutputResolution_CIF:
             
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3362,0x11, &iResult); 
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3363,0x68, &iResult); 
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3364,0x24, &iResult);  
             
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3403,0x42, &iResult); 
             
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3088,0x01, &iResult);  
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3089,0x60, &iResult);
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308a,0x01, &iResult);  
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308b,0x20, &iResult);

             break;  

         case csiSensorOutputResolution_QCIF:
             
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3362,0x00, &iResult); 
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3363,0xB8, &iResult); 
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3364,0x94, &iResult);  
             
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3403,0x42, &iResult); 
             
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3088,0x00, &iResult);  
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3089,0xb0, &iResult);
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308a,0x00, &iResult);  
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308b,0x90, &iResult);

             break;

          case csiSensorOutputResolution_QQVGA:
             
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3362,0x00, &iResult); 
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3363,0xA8, &iResult); 
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3364,0x7C, &iResult);  
             
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3403,0x42, &iResult); 
             
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3088,0x00, &iResult);  
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3089,0xa0, &iResult);
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308a,0x00, &iResult);  
             I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x308b,0x78, &iResult);

             break;
             
        default:  
             DEBUGMSG(ZONE_ERROR, (TEXT("%s: We don't support this kind of resolution for the sensor!\r\n"), __WFUNCTION__));
             retVal = FALSE;
             break;
    }

    CAMERA_FUNCTION_EXIT();

    return retVal;
}


//----------------------------------------------------------------------------
//
// Function: CameraOV3640SetDigitalZoom
//
// Sets the camera digital zoom level .
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//      zoom
//          [in] Camera zoom value that will be set
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL CameraOV3640SetDigitalZoom(CSI_SELECT csi_sel, DWORD ZoomLevel)
{    
    BOOL retVal = TRUE;

    CAMERA_FUNCTION_ENTRY();

    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(ZoomLevel);
    UNREFERENCED_PARAMETER(csi_sel);

    //Currently, We set OV3640 Output as XGA 1280*960; for this resolution, OV3640 don't support digital zoom.

    //OV3640 digital zoom only support the ouput resolution less than SVGA.

    CAMERA_FUNCTION_EXIT();

    return retVal;
}

//----------------------------------------------------------------------------
//
// Function: CameraOV3640MirFlip
//
// Sets the camera Mirroring and Flipping features.
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//      doRot
//          [in] Camera Horizontal Mirroring will be set or not
//      doFlip
//          [in] Camera Vertical flipping will be set or not
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL CameraOV3640MirFlip(CSI_SELECT csi_sel, BOOL doMirror, BOOL doFlip)
{    
    INT iResult;
    BYTE bySlaveAddr;
    BOOL retVal = TRUE;

    CAMERA_FUNCTION_ENTRY();

    switch (csi_sel)
    {
         case CSI_SELECT_CSI0:
              bySlaveAddr = OV3640_I2C_ADDRESS;
              break;

         case CSI_SELECT_CSI1:
             bySlaveAddr = OV3640_I2C_ADDRESS;
             break;

         default:
              DEBUGMSG(ZONE_ERROR, (TEXT("%s: Don't support!\r\n"), __WFUNCTION__));
              return FALSE;
    }   
    
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(doMirror);
    UNREFERENCED_PARAMETER(doFlip);

    if(doMirror)
    {
        if (doFlip)
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x307c,0x13, &iResult); 
        else
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x307c,0x12, &iResult);
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3090,0xc8, &iResult);
    }
    else
    {
        if (doFlip)
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x307c,0x11, &iResult);
        else
            I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x307c,0x10, &iResult); //default value
        I2CWriteOneByte(g_hI2C, bySlaveAddr, 0x3090,0xc0, &iResult);//default value
    }
    
    CAMERA_FUNCTION_EXIT();

    return retVal;
}


//----------------------------------------------------------------------------
//
// Function: CameraOV3640GetFormats
//
// Get the sensor supported formats for specific pin
//
// Parameters:
//      csi_sel
//          [in] Selects the related sensor to CSI0 or CSI1.
//      type
//          [in] Camera pin number, such as capture, still, preview
//      formats
//          [out] supported formats
//
// Returns:
//      Number of supported formats
//
//------------------------------------------------------------------------------
DWORD CameraOV3640GetFormats(CSI_SELECT csi_sel, DWORD type, PCS_DATARANGE_VIDEO** formats)
{
    DWORD dFormats = 0;
    HANDLE hFslFilter = NULL;
    DWORD result;

    UNREFERENCED_PARAMETER(csi_sel);

    // Get counts and all formats of  current sensor supported
    if(formats != NULL)
    {
        switch(type)
        {
            case CAPTURE:
                {
                    dFormats = 28;
    
                    *formats = new PCS_DATARANGE_VIDEO[dFormats];
    
                    if( NULL == *formats )
                    {
                        return 0;
                    }
    
                    // Set sensor supported formats
                    (* formats)[0] = &DCAM_OV3640_StreamMode_RGB565_QQVGA;
                    (* formats)[1] = &DCAM_OV3640_StreamMode_RGB565_QCIF;
                    (* formats)[2] = &DCAM_OV3640_StreamMode_RGB565_QVGA;
                    (* formats)[3] = &DCAM_OV3640_StreamMode_RGB565_CIF;
                    (* formats)[4] = &DCAM_OV3640_StreamMode_RGB565_VGA;
                    (* formats)[5] = &DCAM_OV3640_StreamMode_RGB565_D1_PAL;
                    (* formats)[6] = &DCAM_OV3640_StreamMode_RGB565_D1_NTSC;
                    (* formats)[7] = &DCAM_OV3640_StreamMode_YV12_QQVGA;
                    (* formats)[8] = &DCAM_OV3640_StreamMode_YV12_QCIF;
                    (* formats)[9] = &DCAM_OV3640_StreamMode_YV12_QVGA;
                    (* formats)[10] = &DCAM_OV3640_StreamMode_YV12_CIF;
                    (* formats)[11] = &DCAM_OV3640_StreamMode_YV12_VGA;
                    (* formats)[12] = &DCAM_OV3640_StreamMode_YV12_D1_PAL;
                    (* formats)[13] = &DCAM_OV3640_StreamMode_YV12_D1_NTSC;    
                    (* formats)[14] = &DCAM_OV3640_StreamMode_UYVY_QQVGA;
                    (* formats)[15] = &DCAM_OV3640_StreamMode_UYVY_QCIF;
                    (* formats)[16] = &DCAM_OV3640_StreamMode_UYVY_QVGA;
                    (* formats)[17] = &DCAM_OV3640_StreamMode_UYVY_CIF;
                    (* formats)[18] = &DCAM_OV3640_StreamMode_UYVY_VGA;
                    (* formats)[19] = &DCAM_OV3640_StreamMode_UYVY_D1_PAL;
                    (* formats)[20] = &DCAM_OV3640_StreamMode_UYVY_D1_NTSC;
                    (* formats)[21] = &DCAM_OV3640_StreamMode_NV12_QQVGA;
                    (* formats)[22] = &DCAM_OV3640_StreamMode_NV12_QCIF;
                    (* formats)[23] = &DCAM_OV3640_StreamMode_NV12_QVGA;
                    (* formats)[24] = &DCAM_OV3640_StreamMode_NV12_CIF;
                    (* formats)[25] = &DCAM_OV3640_StreamMode_NV12_VGA;
                    (* formats)[26] = &DCAM_OV3640_StreamMode_NV12_D1_PAL;
                    (* formats)[27] = &DCAM_OV3640_StreamMode_NV12_D1_NTSC;
                }
                break;
    
            case STILL:
                {
                    dFormats = 15;
    
                    *formats = new PCS_DATARANGE_VIDEO[dFormats];
    
                    if( NULL == *formats )
                    {
                        return 0;
                    }
    
                    // Set sensor supported formats
                    (* formats)[0] = &DCAM_OV3640_StreamMode_RGB565_QQVGA;
                    (* formats)[1] = &DCAM_OV3640_StreamMode_RGB565_QCIF;
                    (* formats)[2] = &DCAM_OV3640_StreamMode_RGB565_QVGA;
                    (* formats)[3] = &DCAM_OV3640_StreamMode_RGB565_CIF;
                    (* formats)[4] = &DCAM_OV3640_StreamMode_RGB565_VGA;
                    (* formats)[5] = &DCAM_OV3640_StreamMode_YV12_QQVGA;
                    (* formats)[6] = &DCAM_OV3640_StreamMode_YV12_QCIF;
                    (* formats)[7] = &DCAM_OV3640_StreamMode_YV12_QVGA;
                    (* formats)[8] = &DCAM_OV3640_StreamMode_YV12_CIF;
                    (* formats)[9] = &DCAM_OV3640_StreamMode_YV12_VGA;
                    (* formats)[10] = &DCAM_OV3640_StreamMode_UYVY_QQVGA;
                    (* formats)[11] = &DCAM_OV3640_StreamMode_UYVY_QCIF;
                    (* formats)[12] = &DCAM_OV3640_StreamMode_UYVY_QVGA;
                    (* formats)[13] = &DCAM_OV3640_StreamMode_UYVY_CIF;
                    (* formats)[14] = &DCAM_OV3640_StreamMode_UYVY_VGA;
                }
                break;
    
            case PREVIEW:
                {
                    dFormats = 15;
    
                    *formats = new PCS_DATARANGE_VIDEO[dFormats];
    
                    if( NULL == *formats )
                    {
                        return 0;
                    }
    
                    // Set sensor supported formats
                    (* formats)[0] = &DCAM_OV3640_StreamMode_RGB565_QQVGA;
                    (* formats)[1] = &DCAM_OV3640_StreamMode_RGB565_QCIF;
                    (* formats)[2] = &DCAM_OV3640_StreamMode_RGB565_QVGA;
                    (* formats)[3] = &DCAM_OV3640_StreamMode_RGB565_CIF;
                    (* formats)[4] = &DCAM_OV3640_StreamMode_RGB565_VGA;  
                    (* formats)[5] = &DCAM_OV3640_StreamMode_YV12_QQVGA;
                    (* formats)[6] = &DCAM_OV3640_StreamMode_YV12_QCIF;
                    (* formats)[7] = &DCAM_OV3640_StreamMode_YV12_QVGA;
                    (* formats)[8] = &DCAM_OV3640_StreamMode_YV12_CIF;
                    (* formats)[9] = &DCAM_OV3640_StreamMode_YV12_VGA;
                    (* formats)[10] = &DCAM_OV3640_StreamMode_UYVY_QQVGA;
                    (* formats)[11] = &DCAM_OV3640_StreamMode_UYVY_QCIF;
                    (* formats)[12] = &DCAM_OV3640_StreamMode_UYVY_QVGA;
                    (* formats)[13] = &DCAM_OV3640_StreamMode_UYVY_CIF;
                    (* formats)[14] = &DCAM_OV3640_StreamMode_UYVY_VGA;
                }
                break;
    
            default:
                ERRORMSG(TRUE, (TEXT("%s: Invalid type parameter!\r\n"),__WFUNCTION__));
                break;
        }
    }
    // Only get the counts of supported
    else
    {
        //Create an event to flag filter's factory
        //This Event using to determine the connected filter is freescale filter or not
        //When Event is set, connected filter is freescale filter, which can support all of the format
        //When Event is reset, the filter isn't freescale's, so we just report limited format
        hFslFilter = CreateEvent(NULL, TRUE, FALSE, FSL_FILTER_EVENT);
        
        if(hFslFilter == NULL)
        {
            ERRORMSG(TRUE, (TEXT("%s: Failed creating Event for hFslFilter\r\n"),__WFUNCTION__));
            return 0;
        }
        
        result = WaitForSingleObject(hFslFilter,1);
        
        if(result == WAIT_OBJECT_0)
        {
            //if connected filter is  freescale filter, we will support all format
            switch(type)
            {
                case CAPTURE:
                    dFormats = 28;
                    break;
            
                case STILL:
                    dFormats = 15;
                    break;
            
                case PREVIEW:
                    dFormats = 15;
                    break;
            
                default:
                    ERRORMSG(TRUE, (TEXT("%s: Invalid type parameter!\r\n"),__WFUNCTION__));
                    break;
            }
        }
        else
        {
            //if connected filter isn't  freescale filter, we only support limited format
            switch(type)
            {
                case CAPTURE:
                    dFormats = 14;
                    break;
            
                case STILL:
                    dFormats = 5;
                    break;
            
                case PREVIEW:
                    dFormats = 15;
                    break;
            
                default:
                    ERRORMSG(TRUE, (TEXT("%s: Invalid type parameter!\r\n"),__WFUNCTION__));
                    break;
            }
        }

        CloseHandle(hFslFilter);
    }

    return dFormats;
}

