//------------------------------------------------------------------------------
//
// Copyright (C) 2008-2011 Freescale Semiconductor, Inc. All Rights Reserved.
// THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
// AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT 
//
//------------------------------------------------------------------------------
//
//  File:  CameraOV5642.h
//
//  Definitions for OV5642 Chip Camera Sensor.
//
//------------------------------------------------------------------------------


#ifndef __CAMERAOV5642_H__
#define __CAMERAOV5642_H__


//------------------------------------------------------------------------------
// Defines

//------------------------------------------------------------------------------
// Types

//------------------------------------------------------------------------------
// Functions

BOOL CameraOV5642Init(CSI_SELECT csi_sel, SensorOutputFrameRate SensorFramerate);
BOOL CameraOV5642Deinit(CSI_SELECT csi_sel);
BOOL CameraOV5642Set15fps(CSI_SELECT csi_sel);
BOOL CameraOV5642Set30fps(CSI_SELECT csi_sel);
BOOL CameraOV5642SetOutputResolution(CSI_SELECT csi_sel, SensorOutputFrameRate SensorFramerate, csiSensorOutputResolution outputResolution);
BOOL CameraOV5642SetOutputFormat(CSI_SELECT csi_sel, SensorOutputFrameRate SensorFramerate, csiSensorOutputFormat outputFormat);
BOOL CameraOV5642SetDigitalZoom(CSI_SELECT csi_sel, DWORD ZoomLevel);
BOOL CameraOV5642MirFlip(CSI_SELECT csi_sel, BOOL doMirror, BOOL doFlip);
DWORD CameraOV5642GetFormats(CSI_SELECT csi_sel, DWORD type, PCS_DATARANGE_VIDEO** formats);
#endif   // __CAMERAOV5642_H__

