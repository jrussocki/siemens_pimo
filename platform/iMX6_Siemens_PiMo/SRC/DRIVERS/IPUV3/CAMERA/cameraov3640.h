//------------------------------------------------------------------------------
//
// Copyright (C) 2008-2011 Freescale Semiconductor, Inc. All Rights Reserved.
// THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
// AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT 
//
//------------------------------------------------------------------------------
//
//  File:  CameraOV3640.h
//
//  Definitions for OV3640Chip Camera Sensor.
//
//------------------------------------------------------------------------------


#ifndef __CAMERAOV3640_H__
#define __CAMERAOV3640_H__


//------------------------------------------------------------------------------
// Defines

//------------------------------------------------------------------------------
// Types

//------------------------------------------------------------------------------
// Functions

BOOL CameraOV3640Init(CSI_SELECT csi_sel, SensorOutputFrameRate SensorFramerate);
BOOL CameraOV3640Deinit(CSI_SELECT csi_sel);
BOOL CameraOV3640Set15fps(CSI_SELECT csi_sel);
BOOL CameraOV3640Set30fps(CSI_SELECT csi_sel);
BOOL CameraOV3640SetOutputResolution(CSI_SELECT csi_sel, SensorOutputFrameRate SensorFramerate, csiSensorOutputResolution outputResolution);
BOOL CameraOV3640SetOutputFormat(CSI_SELECT csi_sel, SensorOutputFrameRate SensorFramerate, csiSensorOutputFormat outputFormat);
BOOL CameraOV3640SetDigitalZoom(CSI_SELECT csi_sel, DWORD ZoomLevel);
BOOL CameraOV3640MirFlip(CSI_SELECT csi_sel, BOOL doMirror, BOOL doFlip);
DWORD CameraOV3640GetFormats(CSI_SELECT csi_sel, DWORD type, PCS_DATARANGE_VIDEO** formats);
#endif   // __CAMERAOV3640_H__

