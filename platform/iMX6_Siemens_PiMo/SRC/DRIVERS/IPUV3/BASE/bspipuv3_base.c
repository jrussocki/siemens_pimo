//------------------------------------------------------------------------------
//
//  Copyright (C) 2007-2010, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  bspipuv3_base.c
//
//  Provides BSP-specific routines for use by the IPU Base.
//
//------------------------------------------------------------------------------

#pragma warning(push)
#pragma warning(disable: 4115 4201 4204 4214)
#include <windows.h>
#pragma warning(pop)

#include "bsp.h"
#include "bspdisplay.h"


//------------------------------------------------------------------------------
// External Functions


//------------------------------------------------------------------------------
// External Variables


//------------------------------------------------------------------------------
// Defines
#define VIDEO_REG_PATH                        TEXT("Drivers\\Display\\DDIPU")
#define VIDEO_MEM_SIZE                        TEXT("VideoMemSize")

//------------------------------------------------------------------------------
// Types


//------------------------------------------------------------------------------
// Global Variables


//------------------------------------------------------------------------------
// Local Variables
static DDK_DVFC_SETPOINT g_IpuSetpoint = DDK_DVFC_SETPOINT_LOW;
static BOOL g_bIpuClkEnable = FALSE;


//------------------------------------------------------------------------------
// Local Functions

//------------------------------------------------------------------------------
//
// Function: GetVMemSizeFromRegistry
//
// Get the size of video RAM requested by user from registry.
//
// Parameters:
//      None.
//
// Returns:
//      Video Memory size read from registry.
//
//------------------------------------------------------------------------------
DWORD BSPGetVMemSizeFromRegistry(VOID)
{
    BOOL result = TRUE;
    LONG  error;
    HKEY  hKey;
    DWORD dwSize;
    ULONG iAlignment;
    UINT32 nVideoMemorySize = 0;

    // Open registry key for display driver
    error = RegOpenKeyEx(HKEY_LOCAL_MACHINE, VIDEO_REG_PATH, 0 , 0, &hKey);
    if (error != ERROR_SUCCESS)
    {
        DEBUGMSG(1, (TEXT("%s: Failed to open reg path:%s [Error:0x%x]\r\n"), __WFUNCTION__, VIDEO_REG_PATH, error));
        result = FALSE;
        goto _doneVMem;
    }

    // Retrieve screen width from registry
    dwSize = sizeof(int);
    error = RegQueryValueEx(hKey, VIDEO_MEM_SIZE, NULL, NULL,(LPBYTE)&nVideoMemorySize, (LPDWORD)&dwSize);
    if (error != ERROR_SUCCESS)
    {
        DEBUGMSG(1, (TEXT("DDIPU GetVMemSizeFromRegistry: Failed to get the video RAM size [Error:0x%x].  Setting to default.\r\n"),error));
        result = FALSE;
        nVideoMemorySize = DEFAULT_VIDEO_MEM_SIZE;
        goto _doneVMem;
    }

    // Aligned to 64k bytes
    iAlignment = (1UL << 0xF) - 1;
    nVideoMemorySize = (nVideoMemorySize + iAlignment) & (~iAlignment);

_doneVMem:
    // Close registry key
    RegCloseKey(hKey);

    DEBUGMSG(1, (TEXT("GetVMemSizeFromRegistry: %s, size is %d!\r\n"), result ? L"succeeds" : L"fails", nVideoMemorySize));

    return nVideoMemorySize;
}


//------------------------------------------------------------------------------
//
// Function: BSPGetCacheMode
//
// Return whether the Cache mode is cached, write-through
// or non-cached, bufferable.
//
// Parameters:
//      None.
//
// Returns:
//      TRUE if cache mode is cached, write-through.
//      FALSE if non-cached, bufferable.
//
//------------------------------------------------------------------------------
BOOL BSPGetCacheMode()
{
#if defined(USE_C2D_ROUTINES)
    // turn off caching for video memory as it'll be accessed by ARM & C2D,
    // and the overheads of explict call to fluash d cache before every c2d
    // access would drop the performance heavily.
    
    return FALSE; 

#else

    return BSP_VID_MEM_CACHE_WRITETHROUGH;

#endif //#if defined(USE_C2D_ROUTINES)
}

//------------------------------------------------------------------------------
//
// Function: BSPGetIPUIRAMStartAddr
//
// Get the start address of the IRAM IPU reserved region.
//
// Parameters:
//      None.
//
// Returns:
//      The start address in IRAM.
//
//------------------------------------------------------------------------------
DWORD BSPGetIPUIRAMStartAddr()
{
    return IMAGE_WINCE_IPU_IRAM_PA_START;
}

//------------------------------------------------------------------------------
//
// Function: BSPGetIPUIRAMSize
//
// Get the size of the IRAM IPU reserved region.
//
// Parameters:
//      None.
//
// Returns:
//      The size of the IRAM region.
//
//------------------------------------------------------------------------------
DWORD BSPGetIPUIRAMSize(VOID)
{
    return IMAGE_WINCE_IPU_IRAM_SIZE;
}

//------------------------------------------------------------------------------
//
// Function: BSPGetReservedVMem
//
// Get the size and start hardware address of the Reserved video memory.
//
// Parameters:
//      pPhysicalMemAddr
//          [in] The pointer for start physical address of reserved  memory
//
//      pVMemSize
//          [in] The pointer for reserved  memory size
//
// Returns:
//      Return TRUE if the BSP contains reserved video memory, otherwise return FALSE.
//
//------------------------------------------------------------------------------
BOOL  BSPGetReservedVMem(ULONG * pPhysicalMemAddr, ULONG * pVMemSize)
{
#ifdef IMAGE_WINCE_VIDEO_RAM_PA_START
    (* pPhysicalMemAddr) = IMAGE_WINCE_VIDEO_RAM_PA_START;
    (* pVMemSize) = IMAGE_WINCE_VIDEO_RAM_SIZE;
    return TRUE;
#else
    (* pPhysicalMemAddr) = 0;
    (* pVMemSize) = 0;
    return FALSE;
#endif
}

//------------------------------------------------------------------------------
//
// Function: BSPIPUSetClockGatingMode
//
// This function calls to the CRM module to
// set the clock gating mode, turning on or off
// clocks to the IPU.
//
// Parameters:
//      dwIPUEnableMask
//          [in] Mask of bits in the IPU_CONF register, providing
//          information about which IPU submodules are enabled.
//
// Returns:
//      TRUE if success.
//      FALSE if failure.
//
//------------------------------------------------------------------------------
BOOL BSPIPUSetClockGatingMode(DWORD dwIPUEnableMask)
{
    static BOOL bClocksEnabled = FALSE;
    BOOL rc = FALSE;

    //TODO: Since DDKClockSetGatingMode can't work properly, comment it out to keep clock on.
    if (dwIPUEnableMask != 0)
    {
        if (!bClocksEnabled)
        {
            DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
            bClocksEnabled = TRUE;
        }
    }
    else
    {
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU, DDK_CLOCK_GATE_MODE_DISABLED);
        bClocksEnabled = FALSE;
    }

    rc = TRUE;
    return rc;
}
