//------------------------------------------------------------------------------
//
//  Copyright (C) 2009-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  panels.h
//
//  Provides a list of supported display panels for the BSP
//
//------------------------------------------------------------------------------

#ifndef __PANELS_H__
#define __PANELS_H__

#include "ipu_common.h"

//------------------------------------------------------------------------------
// Defines

#define ASYNC_PANEL_SET_X_POSITION_CMD        0x20
#define ASYNC_PANEL_SET_Y_POSITION_CMD        0x21
#define ASYNC_PANEL_SEND_DATA_CMD             0x22

#define ALTERNATE_SETTING                     0

// Primary LVDS panel is AUO XGA panel, but driver was also
// developed using CHIMEI 1920x1080 panel, so we include this flag
// to use this panel.
//------------------------------------------------------------------------------
// Types

//RGB888
BusMappingData rgb24Mapping =
{
    23,     // R component Offset (component 2)
    0xFF,   // R component Mask
    15,     // G component Offset (component 1)
    0xFF,   // G component Mask
    7,      // B component Offset (component 0)
    0xFF    // B component Mask
};

//RGB666
BusMappingData rgb18Mapping =
{
    21,     // R component Offset (component 2)
    0xFC,   // R component Mask
    13,     // G component Offset (component 1)
    0xFC,   // G component Mask
    5,      // B component Offset (component 0)
    0xFC    // B component Mask
};

//RGB565
BusMappingData rgb16Mapping =
{
    15,     // R component Offset (component 2)
    0xF8,   // R component Mask
    10,     // G component Offset (component 1)
    0xFC,   // G component Mask
    4,      // B component Offset (component 0)
    0xF8    // B component Mask
};


//#pragma pack (push, 1)
PANEL_INFO g_PanelArray[NUM_SUPPORTED_PANELS] =
{
	// HDMI - DEFAULT CONFIGURATION - 1280x720
    {
        (PUCHAR) "HDMI 1280x720 Panel",   // Name
        IPU_PANEL_SYNC_TYPE_SYNCHRONOUS, // Panel Sync Type
        DI_SELECT_DI0,         // Display Interface Number
        DISPLAY_PANEL_HDMI,       // Panel ID
        DISPLAY_PORT_HDMI,       // Port type
        DISPLAY_CLOCK_EXTERNAL,    // di clk source
        IPU_PIX_FMT_RGB24,       // Pixel Format
        1280,             // width
        720,             // height
        60,              // frequency
        1,               // transfer cycles
        &rgb24Mapping,         // Mapping for LVDS RGB data
   
      // Synchronous Panel Info
        5,                                 // Vertical Sync width   
        5,                              // Vertical Start Width
        20,                                 // Vertical End Width
        40,                                 // Horizontal Sync Width 
        110,                             // Horizontal Start Width
        220,                             // Horizontal End Width
        74250000,           // Pixel Clock Cycle Frequency: IPU clock 133Mhz, 133/44.3=3 so that we choose 3 as dividor.
        0,               // Pixel Data Offset Position
        0,               // Pixel Clock Up, in ns
        7,               // Pixel Clock Down, in ns
        {    // Display Interface signal polarities
            TRUE,             // Data mask enable
            FALSE,             // Clock Idle enable
            FALSE,             // Clock Select Enable
            TRUE,             // Vertical Sync Polarity
            TRUE,             // Output enable polarity(straight polarity for sharp signals)
            FALSE,             // Data Pol   Inverse polarity.
            FALSE,             // Clock Pol  Inverse polarity.
            TRUE,             // HSync Pol
        },
      // TV only sync panel info
        0,                // Field0 VStart width in lines
        0,                // Field0 VEnd width in lines
        0,                // Field1 VStart width in lines
        0,                // Field1 VEnd width in lines
        FALSE,              // TRUE if TV output data is interlaced; FALSE if progressive
   
      // Asynchronous Panel Info
        0,               // Read Cycle Period
        0,               // Read Up Position
        0,               // Read Down Position
        0,               // Write Cycle Period
        0,               // Write Up Position
        0,               // Write Down Position
        {    // ADC Display Interface signal polarities
            0,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
        },
   
        // Serial Mapping Data
        FALSE,             // Use IPU serial communication with panel?
        0,               // Number of serial data transfer cycles
        NULL,             // Pointer to serial bus mappings
       {
            0,             // Serial Clk Period
            0,             // CS Up Position
            0,             // CS Down Position
            0,             // Clk Up Position
            0,             // Clk Down Position
            0,             // RS Up Position
            0,             // RS Down Position
        },
   
      // Dynamic panel data
        DISPLAY_PANEL_HDMI,      // Starting panel mode corresponding to array index.
                     // Mode may change if array is rearranged during driver init.
        FALSE,             // Has panel been initialized yet?  Must be set to FALSE here.
        FALSE,             // Is panel active (always start out off)
        DC_DISPLAY_0,         // IPU Display number (initialized to 0)
    },

	// LVDS1 - DEFAULT CONFIGURATION - 1024x768
    {
        (PUCHAR) "LVDS1 1024x768 Panel",	// Name
        IPU_PANEL_SYNC_TYPE_SYNCHRONOUS,	// Panel Sync Type
        DI_SELECT_DI0,						// Display Interface Number
        DISPLAY_PANEL_LVDS1,				// Panel ID
        DISPLAY_PORT_LVDS1,					// Port type
        DISPLAY_CLOCK_EXTERNAL,				// Di clk source
        IPU_PIX_FMT_RGB24,					// Pixel Format
        1024,								// Width
        768,								// Height
        62,									// Frequency
        1,									// Transfer cycles
        &rgb18Mapping,						// Mapping for LVDS RGB data
   
      // Synchronous Panel Info
        9,									// Vertical Sync width (in vertical lines)
        24,									// Vertical Start Width (in vertical lines)
        5,									// Vertical End Width (in vertical lines)
        58,									// Horizontal Sync Width (in pixel clock cyles)
        174,								// Horizontal Start Width (in pixel clock cyles)
        88,									// Horizontal End Width (in pixel clock cyles)
        65000000,							// Display clock range from 38Mhz to 113Mhz
        0,									// Pixel Data Offset Position
        0,									// Pixel Clock Up, in ns
        8,									// Pixel Clock Down, in ns
        {    // Display Interface signal polarities
            TRUE,							// Data mask enable
            FALSE,							// Clock Idle enable
            FALSE,							// Clock Select Enable
            FALSE,							// Vertical Sync Polarity
            TRUE,							// Output enable polarity(straight polarity for sharp signals)
            FALSE,							// Data Pol   Inverse polarity.
            FALSE,							// Clock Pol  Inverse polarity.
            FALSE,							// HSync Pol
        },
      // TV only sync panel info
        0,									// Field0 VStart width in lines
        0,									// Field0 VEnd width in lines
        0,									// Field1 VStart width in lines
        0,									// Field1 VEnd width in lines
        FALSE,								// TRUE if TV output data is interlaced; FALSE if progressive
   
      // Asynchronous Panel Info
        0,									// Read Cycle Period
        0,									// Read Up Position
        0,									// Read Down Position
        0,									// Write Cycle Period
        0,									// Write Up Position
        0,									// Write Down Position
        {    // ADC Display Interface signal polarities
            0,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
        },
   
        // Serial Mapping Data
        FALSE,								// Use IPU serial communication with panel?
        0,									// Number of serial data transfer cycles
        NULL,								// Pointer to serial bus mappings
       {
            0,								// Serial Clk Period
            0,								// CS Up Position
            0,								// CS Down Position
            0,								// Clk Up Position
            0,								// Clk Down Position
            0,								// RS Up Position
            0,								// RS Down Position
        },
   
      // Dynamic panel data
        DISPLAY_PANEL_LVDS1,				// Starting panel mode corresponding to array index.
											// Mode may change if array is rearranged during driver init.
        FALSE,								// Has panel been initialized yet?  Must be set to FALSE here.
        FALSE,								// Is panel active (always start out off)
        DC_DISPLAY_0,						// IPU Display number (initialized to 0)
    },
	
	// LVDS2 - DEFAULT CONFIGURATION - 1024x768
    {
        (PUCHAR) "LVDS2 1024x768 Panel",	// Name
        IPU_PANEL_SYNC_TYPE_SYNCHRONOUS,	// Panel Sync Type
        DI_SELECT_DI0,						// Display Interface Number
        DISPLAY_PANEL_LVDS2,				// Panel ID
        DISPLAY_PORT_LVDS2,					// Port type
        DISPLAY_CLOCK_EXTERNAL,				// di clk source
        IPU_PIX_FMT_RGB666,					// Pixel Format
        1024,								// Width
        768,								// Height
        63,									// Frequency
        1,									// Transfer cycles
        &rgb18Mapping,						// Mapping for LVDS RGB data
   
      // Synchronous Panel Info
        9,									// Vertical Sync width (in vertical lines)
        24,									// Vertical Start Width (in vertical lines)
        5,									// Vertical End Width (in vertical lines)
        58,									// Horizontal Sync Width (in pixel clock cyles)
        174,								// Horizontal Start Width (in pixel clock cyles)
        88,									// Horizontal End Width (in pixel clock cyles)
        65000000,							// Pixel Clock Cycle Frequency: IPU clock 133Mhz, 133/44.3=3 so that we choose 3 as dividor.
        0,									// Pixel Data Offset Position
        0,									// Pixel Clock Up, in ns
        8,									// Pixel Clock Down, in ns
        {    // Display Interface signal polarities
            TRUE,							// Data mask enable
            FALSE,							// Clock Idle enable
            FALSE,							// Clock Select Enable
            FALSE,							// Vertical Sync Polarity
            TRUE,							// Output enable polarity(straight polarity for sharp signals)
            FALSE,							// Data Pol Inverse polarity.
            FALSE,							// Clock Pol Inverse polarity.
            FALSE,							// HSync Pol
        },
      // TV only sync panel info
        0,									// Field0 VStart width in lines
        0,									// Field0 VEnd width in lines
        0,									// Field1 VStart width in lines
        0,									// Field1 VEnd width in lines
        FALSE,								// TRUE if TV output data is interlaced; FALSE if progressive
   
      // Asynchronous Panel Info
        0,									// Read Cycle Period
        0,									// Read Up Position
        0,									// Read Down Position
        0,									// Write Cycle Period
        0,									// Write Up Position
        0,									// Write Down Position
        {    // ADC Display Interface signal polarities
            0,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
        },
   
        // Serial Mapping Data
        FALSE,								// Use IPU serial communication with panel?
        0,									// Number of serial data transfer cycles
        NULL,								// Pointer to serial bus mappings
       {
            0,								// Serial Clk Period
            0,								// CS Up Position
            0,								// CS Down Position
            0,								// Clk Up Position
            0,								// Clk Down Position
            0,								// RS Up Position
            0,								// RS Down Position
        },
   
      // Dynamic panel data
        DISPLAY_PANEL_LVDS2,				// Starting panel mode corresponding to array index.
											// Mode may change if array is rearranged during driver init.
        FALSE,								// Has panel been initialized yet?  Must be set to FALSE here.
        FALSE,								// Is panel active (always start out off)
        DC_DISPLAY_0,						// IPU Display number (initialized to 0)
    },
// TC : Commented because we are using only one default configuration for each type of display.
// The display configuration has to be made in the registry
#if 0
#ifdef BSP_DISPLAY_HDMI

    // HDMI 640X480 Definitions
    {
        (PUCHAR) "HDMI 640x480 Panel",   // Name
        IPU_PANEL_SYNC_TYPE_SYNCHRONOUS, // Panel Sync Type
        DI_SELECT_DI0,         // Display Interface Number
        DISPLAY_PANEL_HDMI_480P60,       // Panel ID
        DISPLAY_PORT_HDMI,       // Port type
        DISPLAY_CLOCK_INTERNAL,    // di clk source
        IPU_PIX_FMT_RGB24,       // Pixel Format
        640,             // width
        480,             // height
        60,              // frequency
        1,               // transfer cycles
        &HDMIMapping[0],         // Mapping for LVDS RGB data
   
      // Synchronous Panel Info
        2,                                 // Vertical Sync width   
        10,                              // Vertical Start Width
        33,                                 // Vertical End Width
        96,                                 // Horizontal Sync Width 
        16,                             // Horizontal Start Width
        48,                             // Horizontal End Width
        25200000,           // Pixel Clock Cycle Frequency: IPU clock 133Mhz, 133/44.3=3 so that we choose 3 as dividor.
        0,               // Pixel Data Offset Position
        0,               // Pixel Clock Up, in ns
        20,               // Pixel Clock Down, in ns
        {    // Display Interface signal polarities
            TRUE,             // Data mask enable
            FALSE,             // Clock Idle enable
            FALSE,             // Clock Select Enable
            TRUE,             // Vertical Sync Polarity
            TRUE,             // Output enable polarity(straight polarity for sharp signals)
            FALSE,             // Data Pol   Inverse polarity.
            FALSE,             // Clock Pol  Inverse polarity.
            TRUE,             // HSync Pol
        },
      // TV only sync panel info
        0,                // Field0 VStart width in lines
        0,                // Field0 VEnd width in lines
        0,                // Field1 VStart width in lines
        0,                // Field1 VEnd width in lines
        FALSE,              // TRUE if TV output data is interlaced; FALSE if progressive
   
      // Asynchronous Panel Info
        0,               // Read Cycle Period
        0,               // Read Up Position
        0,               // Read Down Position
        0,               // Write Cycle Period
        0,               // Write Up Position
        0,               // Write Down Position
        {    // ADC Display Interface signal polarities
            0,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
        },
   
        // Serial Mapping Data
        FALSE,             // Use IPU serial communication with panel?
        0,               // Number of serial data transfer cycles
        NULL,             // Pointer to serial bus mappings
       {
            0,             // Serial Clk Period
            0,             // CS Up Position
            0,             // CS Down Position
            0,             // Clk Up Position
            0,             // Clk Down Position
            0,             // RS Up Position
            0,             // RS Down Position
        },
   
      // Dynamic panel data
        DISPLAY_PANEL_HDMI_480P60,      // Starting panel mode corresponding to array index.
                     // Mode may change if array is rearranged during driver init.
        FALSE,             // Has panel been initialized yet?  Must be set to FALSE here.
        FALSE,             // Is panel active (always start out off)
        DC_DISPLAY_0,         // IPU Display number (initialized to 0)
    },
    
    // HDMI 1024x768 Definitions
    {
        (PUCHAR) "HDMI XGA Panel",    // Name
        IPU_PANEL_SYNC_TYPE_SYNCHRONOUS, // Panel Sync Type
        DI_SELECT_DI0,               // Display Interface Number
        DISPLAY_PANEL_HDMI_XGA,       // Panel ID
        DISPLAY_PORT_HDMI,            // Port type
        DISPLAY_CLOCK_INTERNAL,      // di clk source
        IPU_PIX_FMT_RGB24,           // Pixel Format
        1024,                        // width
        768,                         // height
        60,                          // frequency
        1,                           // transfer cycles
        &HDMIMapping[0],                 // Mapping for HDMI RGB data

        // Synchronous Panel Info
#if ALTERNATE_SETTING //(0)
        //LLLLL's configure
        6,                           // Vertical Sync width (in vertical lines)
        22,                          // Vertical Start Width (in vertical lines)
        4,                           // Vertical End Width (in vertical lines)
        100,                         // Horizontal Sync Width (in pixel clock cyles)
        60,                          // Horizontal Start Width (in pixel clock cyles)
        40,                          // Horizontal End Width (in pixel clock cyles)
        58800000,                    // Pixel Clock Cycle Frequency: IPU clock 133Mhz, 133/24=5.54 so that we choose 6 as dividor.
#else
        3,                           // Vertical Sync width (in vertical lines)
        0,                          // Vertical Start Width (in vertical lines)
        19,                           // Vertical End Width (in vertical lines)
        32,                         // Horizontal Sync Width (in pixel clock cyles)
        80,                         // Horizontal Start Width (in pixel clock cyles)
        48,                          // Horizontal End Width (in pixel clock cyles)
        56000000,                    // Pixel Clock Cycle Frequency: IPU clock 133Mhz, 133/24=5.54 so that we choose 6 as dividor.
#endif
        0,                           // Pixel Data Offset Position
        0,                           // Pixel Clock Up, in ns (if tick is 7.52ns(133Mhz), 2 HSP cycles)
        8,                           // Pixel Clock Down, in ns (if tick is 7.52ns(133Mhz), 4.5 HSP cycles)
        {       // Display Interface signal polarities
            TRUE,                        // Data mask enable
            FALSE,                       // Clock Idle enable
            FALSE,                       // Clock Select Enable
            FALSE,                       // Vertical Sync Polarity
            TRUE,                        // Output enable polarity(straight polarity for sharp signals)
            FALSE,                       // Data Pol   Inverse polarity.
            FALSE,                       // Clock Pol  Inverse polarity.
            FALSE,                       // HSync Pol
        },
        // TV only sync panel info
        0,                            // Field0 VStart width in lines
        0,                            // Field0 VEnd width in lines
        0,                            // Field1 VStart width in lines
        0,                            // Field1 VEnd width in lines
        FALSE,                        // TRUE if TV output data is interlaced; FALSE if progressive

        // Asynchronous Panel Info
        0,                           // Read Cycle Period
        0,                           // Read Up Position
        0,                           // Read Down Position
        0,                           // Write Cycle Period
        0,                           // Write Up Position
        0,                           // Write Down Position
        {       // ADC Display Interface signal polarities
            0,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
        },

        // Serial Mapping Data
        FALSE,                       // Use IPU serial communication with panel?
        0,                           // Number of serial data transfer cycles
        NULL,                        // Pointer to serial bus mappings
        {
            0,                       // Serial Clk Period
            0,                       // CS Up Position
            0,                       // CS Down Position
            0,                       // Clk Up Position
            0,                       // Clk Down Position
            0,                       // RS Up Position
            0,                       // RS Down Position
        },

        // Dynamic panel data
        DISPLAY_PANEL_HDMI_XGA,       // Starting panel mode corresponding to array index.
                                     // Mode may change if array is rearranged during driver init.
        FALSE,                       // Has panel been initialized yet?  Must be set to FALSE here.
        FALSE,                       // Is panel active (always start out off)
        DC_DISPLAY_0,                // IPU Display number (initialized to 0)
    },

    // HDMI 1280x1024 Definitions
    {
        (PUCHAR) "HDMI SXGA Panel",   // Name
        IPU_PANEL_SYNC_TYPE_SYNCHRONOUS, // Panel Sync Type
        DI_SELECT_DI0,               // Display Interface Number
        DISPLAY_PANEL_HDMI_SXGA,      // Panel ID
        DISPLAY_PORT_HDMI,            // Port type
        DISPLAY_CLOCK_EXTERNAL,      // di clk source
        IPU_PIX_FMT_RGB24,           // Pixel Format
        1280,                        // width
        1024,                        // height
        60,                          // frequency
        1,                           // transfer cycles
        &HDMIMapping[0],                 // Mapping for HDMI RGB data

        // Synchronous Panel Info
#if ALTERNATE_SETTING //(0)
        //LLLLL's configure
        6,                           // Vertical Sync width (in vertical lines)
        10,                          // Vertical Start Width (in vertical lines)
        4,                           // Vertical End Width (in vertical lines)
        100,                         // Horizontal Sync Width (in pixel clock cyles)
        69,                           // Horizontal Start Width (in pixel clock cyles)
        20,                          // Horizontal End Width (in pixel clock cyles)
        90900000,                    // Pixel Clock Cycle Frequency: IPU clock 133Mhz, 133/24=5.54 so that we choose 6 as dividor.
#else
        3,                           // Vertical Sync width (in vertical lines)
        1,//35,                          // Vertical Start Width (in vertical lines)
        38,//4,                           // Vertical End Width (in vertical lines)
        112,                         // Horizontal Sync Width (in pixel clock cyles)
        48,                         // Horizontal Start Width (in pixel clock cyles)
        248,//148,//                          // Horizontal End Width (in pixel clock cyles)
        108000000,//100000000,//                   // Pixel Clock Cycle Frequency: IPU clock 133Mhz, 133/24=5.54 so that we choose 6 as dividor.
#endif
        0,                           // Pixel Data Offset Position
        0,                           // Pixel Clock Up, in ns (if tick is 7.52ns(133Mhz), 2 HSP cycles)
        5,                           // Pixel Clock Down, in ns (if tick is 7.52ns(133Mhz), 4.5 HSP cycles)
        {       // Display Interface signal polarities
            TRUE,                        // Data mask enable
            FALSE,                       // Clock Idle enable
            FALSE,                       // Clock Select Enable
            TRUE,                       // Vertical Sync Polarity
            TRUE,                        // Output enable polarity(straight polarity for sharp signals)
            FALSE,                       // Data Pol   Inverse polarity.
            FALSE,                       // Clock Pol  Inverse polarity.
            TRUE,                       // HSync Pol
        },
        // TV only sync panel info
        0,                            // Field0 VStart width in lines
        0,                            // Field0 VEnd width in lines
        0,                            // Field1 VStart width in lines
        0,                            // Field1 VEnd width in lines
        FALSE,                        // TRUE if TV output data is interlaced; FALSE if progressive

        // Asynchronous Panel Info
        0,                           // Read Cycle Period
        0,                           // Read Up Position
        0,                           // Read Down Position
        0,                           // Write Cycle Period
        0,                           // Write Up Position
        0,                           // Write Down Position
        {       // ADC Display Interface signal polarities
            0,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
        },

        // Serial Mapping Data
        FALSE,                       // Use IPU serial communication with panel?
        0,                           // Number of serial data transfer cycles
        NULL,                        // Pointer to serial bus mappings
        {
            0,                       // Serial Clk Period
            0,                       // CS Up Position
            0,                       // CS Down Position
            0,                       // Clk Up Position
            0,                       // Clk Down Position
            0,                       // RS Up Position
            0,                       // RS Down Position
        },

        // Dynamic panel data
        DISPLAY_PANEL_HDMI_SXGA,      // Starting panel mode corresponding to array index.
                                     // Mode may change if array is rearranged during driver init.
        FALSE,                       // Has panel been initialized yet?  Must be set to FALSE here.
        FALSE,                       // Is panel active (always start out off)
        DC_DISPLAY_0,                // IPU Display number (initialized to 0)
    },

    // HDMI 1600x1200 Definitions
    {
        (PUCHAR) "HDMI UXGA Panel",   // Name
        IPU_PANEL_SYNC_TYPE_SYNCHRONOUS, // Panel Sync Type
        DI_SELECT_DI0,               // Display Interface Number
        DISPLAY_PANEL_HDMI_UXGA,      // Panel ID
        DISPLAY_PORT_HDMI,            // Port type
        DISPLAY_CLOCK_EXTERNAL,      // di clk source
        IPU_PIX_FMT_RGB24,           // Pixel Format
        1600,                        // width
        1200,                        // height
        60,                          // frequency
        1,                           // transfer cycles
        &HDMIMapping[0],                 // Mapping for HDMI RGB data
    
        // Synchronous Panel Info
        3,                           // Vertical Sync width (in vertical lines)
        1,                           // Vertical Start Width (in vertical lines)
        46,                          // Vertical End Width (in vertical lines)
        192,                         // Horizontal Sync Width (in pixel clock cyles)
        64,                          // Horizontal Start Width (in pixel clock cyles)
        304,//150,//                         // Horizontal End Width (in pixel clock cyles)
        162000000,//150000000,//                   // Pixel Clock Cycle Frequency: IPU clock 133Mhz, 133/24=5.54 so that we choose 6 as dividor.
        0,                           // Pixel Data Offset Position
        0,                           // Pixel Clock Up, in ns (if tick is 7.52ns(133Mhz), 2 HSP cycles)
        4,                           // Pixel Clock Down, in ns (if tick is 7.52ns(133Mhz), 4.5 HSP cycles)
        {       // Display Interface signal polarities
            TRUE,                        // Data mask enable
            FALSE,                       // Clock Idle enable
            FALSE,                       // Clock Select Enable
            FALSE,                       // Vertical Sync Polarity
            TRUE,                        // Output enable polarity(straight polarity for sharp signals)
            FALSE,                       // Data Pol   Inverse polarity.
            FALSE,                       // Clock Pol  Inverse polarity.
            FALSE,                       // HSync Pol
        },
        // TV only sync panel info
        0,                            // Field0 VStart width in lines
        0,                            // Field0 VEnd width in lines
        0,                            // Field1 VStart width in lines
        0,                            // Field1 VEnd width in lines
        FALSE,                        // TRUE if TV output data is interlaced; FALSE if progressive
    
        // Asynchronous Panel Info
        0,                           // Read Cycle Period
        0,                           // Read Up Position
        0,                           // Read Down Position
        0,                           // Write Cycle Period
        0,                           // Write Up Position
        0,                           // Write Down Position
        {       // ADC Display Interface signal polarities
            0,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
        },
    
        // Serial Mapping Data
        FALSE,                       // Use IPU serial communication with panel?
        0,                           // Number of serial data transfer cycles
        NULL,                        // Pointer to serial bus mappings
        {
            0,                       // Serial Clk Period
            0,                       // CS Up Position
            0,                       // CS Down Position
            0,                       // Clk Up Position
            0,                       // Clk Down Position
            0,                       // RS Up Position
            0,                       // RS Down Position
        },
    
        // Dynamic panel data
        DISPLAY_PANEL_HDMI_UXGA,      // Starting panel mode corresponding to array index.
                                     // Mode may change if array is rearranged during driver init.
        FALSE,                       // Has panel been initialized yet?  Must be set to FALSE here.
        FALSE,                       // Is panel active (always start out off)
        DC_DISPLAY_0,                // IPU Display number (initialized to 0)
    },

    // HDMI 1280x720 Definitions
    {
        (PUCHAR) "HDMI 1280x720 Panel",   // Name
        IPU_PANEL_SYNC_TYPE_SYNCHRONOUS, // Panel Sync Type
        DI_SELECT_DI0,         // Display Interface Number
        DISPLAY_PANEL_HDMI_720P60,       // Panel ID
        DISPLAY_PORT_HDMI,       // Port type
        DISPLAY_CLOCK_EXTERNAL,    // di clk source
        IPU_PIX_FMT_RGB24,       // Pixel Format
        1280,             // width
        720,             // height
        60,              // frequency
        1,               // transfer cycles
        &HDMIMapping[0],         // Mapping for LVDS RGB data
   
      // Synchronous Panel Info
        5,                                 // Vertical Sync width   
        5,                              // Vertical Start Width
        20,                                 // Vertical End Width
        40,                                 // Horizontal Sync Width 
        110,                             // Horizontal Start Width
        220,                             // Horizontal End Width
        74250000,           // Pixel Clock Cycle Frequency: IPU clock 133Mhz, 133/44.3=3 so that we choose 3 as dividor.
        0,               // Pixel Data Offset Position
        0,               // Pixel Clock Up, in ns
        7,               // Pixel Clock Down, in ns
        {    // Display Interface signal polarities
            TRUE,             // Data mask enable
            FALSE,             // Clock Idle enable
            FALSE,             // Clock Select Enable
            TRUE,             // Vertical Sync Polarity
            TRUE,             // Output enable polarity(straight polarity for sharp signals)
            FALSE,             // Data Pol   Inverse polarity.
            FALSE,             // Clock Pol  Inverse polarity.
            TRUE,             // HSync Pol
        },
      // TV only sync panel info
        0,                // Field0 VStart width in lines
        0,                // Field0 VEnd width in lines
        0,                // Field1 VStart width in lines
        0,                // Field1 VEnd width in lines
        FALSE,              // TRUE if TV output data is interlaced; FALSE if progressive
   
      // Asynchronous Panel Info
        0,               // Read Cycle Period
        0,               // Read Up Position
        0,               // Read Down Position
        0,               // Write Cycle Period
        0,               // Write Up Position
        0,               // Write Down Position
        {    // ADC Display Interface signal polarities
            0,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
        },
   
        // Serial Mapping Data
        FALSE,             // Use IPU serial communication with panel?
        0,               // Number of serial data transfer cycles
        NULL,             // Pointer to serial bus mappings
       {
            0,             // Serial Clk Period
            0,             // CS Up Position
            0,             // CS Down Position
            0,             // Clk Up Position
            0,             // Clk Down Position
            0,             // RS Up Position
            0,             // RS Down Position
        },
   
      // Dynamic panel data
        DISPLAY_PANEL_HDMI_720P60,      // Starting panel mode corresponding to array index.
                     // Mode may change if array is rearranged during driver init.
        FALSE,             // Has panel been initialized yet?  Must be set to FALSE here.
        FALSE,             // Is panel active (always start out off)
        DC_DISPLAY_0,         // IPU Display number (initialized to 0)
    },
        
    // HDMI 1920x1080 Definitions
    {
        (PUCHAR) "HDMI 1080p60 Panel",   // Name
        IPU_PANEL_SYNC_TYPE_SYNCHRONOUS, // Panel Sync Type
        DI_SELECT_DI0,               // Display Interface Number
        DISPLAY_PANEL_HDMI_1080P60,      // Panel ID
        DISPLAY_PORT_HDMI,            // Port type
        DISPLAY_CLOCK_EXTERNAL,      // di clk source
        IPU_PIX_FMT_RGB24,           // Pixel Format
        1918,                        // width - temporary workaround for 1080p
        1080,                        // height
        60,                          // frequency
        1,                           // transfer cycles
        &HDMIMapping[0],                 // Mapping for HDMI RGB data
    
        // Synchronous Panel Info
        5,                               // Vertical Sync width   
        4,                               // Vertical Start Width
        36,                              // Vertical End Width
        44,                              // Horizontal Sync Width 
        88,                              // Horizontal Start Width
        148,                             // Horizontal End Width
        148500000,                // Pixel Clock Cycle Frequency: 
        0,                           // Pixel Data Offset Position
        0,                           // Pixel Clock Up, in ns
        4,                           // Pixel Clock Down, in ns
        {       // Display Interface signal polarities
            TRUE,                        // Data mask enable
            FALSE,                       // Clock Idle enable
            FALSE,                       // Clock Select Enable
            TRUE,                       // Vertical Sync Polarity
            TRUE,                        // Output enable polarity(straight polarity for sharp signals)
            FALSE,                       // Data Pol   Inverse polarity.
            FALSE,                       // Clock Pol  Inverse polarity.
            TRUE,                       // HSync Pol
        },
        // TV only sync panel info
        0,                            // Field0 VStart width in lines
        0,                            // Field0 VEnd width in lines
        0,                            // Field1 VStart width in lines
        0,                            // Field1 VEnd width in lines
        FALSE,                        // TRUE if TV output data is interlaced; FALSE if progressive
    
        // Asynchronous Panel Info
        0,                           // Read Cycle Period
        0,                           // Read Up Position
        0,                           // Read Down Position
        0,                           // Write Cycle Period
        0,                           // Write Up Position
        0,                           // Write Down Position
        {       // ADC Display Interface signal polarities
            0,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
        },
    
        // Serial Mapping Data
        FALSE,                       // Use IPU serial communication with panel?
        0,                           // Number of serial data transfer cycles
        NULL,                        // Pointer to serial bus mappings
        {
            0,                       // Serial Clk Period
            0,                       // CS Up Position
            0,                       // CS Down Position
            0,                       // Clk Up Position
            0,                       // Clk Down Position
            0,                       // RS Up Position
            0,                       // RS Down Position
        },
    
        // Dynamic panel data
        DISPLAY_PANEL_HDMI_1080P60,     // Starting panel mode corresponding to array index.
                                     // Mode may change if array is rearranged during driver init.
        FALSE,                       // Has panel been initialized yet?  Must be set to FALSE here.
        FALSE,                       // Is panel active (always start out off)
        DC_DISPLAY_0,                // IPU Display number (initialized to 0)
    },

    // HDMI 1920x1200 Definitions
    {
        (PUCHAR) "HDMI WUXGA Panel",   // Name
        IPU_PANEL_SYNC_TYPE_SYNCHRONOUS, // Panel Sync Type
        DI_SELECT_DI0,               // Display Interface Number
        DISPLAY_PANEL_HDMI_WUXGA,      // Panel ID
        DISPLAY_PORT_HDMI,            // Port type
        DISPLAY_CLOCK_EXTERNAL,      // di clk source
        IPU_PIX_FMT_RGB24,           // Pixel Format
        1920,                        // width
        1200,                        // height
        60,                          // frequency
        1,                           // transfer cycles
        &HDMIMapping[0],                 // Mapping for HDMI RGB data
    
        // Synchronous Panel Info
        6,                               // Vertical Sync width   
        3,                               // Vertical Start Width
        26,                              // Vertical End Width
        32,                              // Horizontal Sync Width 
        48,                              // Horizontal Start Width
        80,                             // Horizontal End Width
        154000000,                // Pixel Clock Cycle Frequency: 
        0,                           // Pixel Data Offset Position
        0,                           // Pixel Clock Up, in ns
        3,                           // Pixel Clock Down, in ns
        {       // Display Interface signal polarities
            TRUE,                        // Data mask enable
            FALSE,                       // Clock Idle enable
            FALSE,                       // Clock Select Enable
            FALSE,                       // Vertical Sync Polarity
            TRUE,                        // Output enable polarity(straight polarity for sharp signals)
            FALSE,                       // Data Pol   Inverse polarity.
            FALSE,                       // Clock Pol  Inverse polarity.
            FALSE,                       // HSync Pol
        },
        // TV only sync panel info
        0,                            // Field0 VStart width in lines
        0,                            // Field0 VEnd width in lines
        0,                            // Field1 VStart width in lines
        0,                            // Field1 VEnd width in lines
        FALSE,                        // TRUE if TV output data is interlaced; FALSE if progressive
    
        // Asynchronous Panel Info
        0,                           // Read Cycle Period
        0,                           // Read Up Position
        0,                           // Read Down Position
        0,                           // Write Cycle Period
        0,                           // Write Up Position
        0,                           // Write Down Position
        {       // ADC Display Interface signal polarities
            0,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
        },
    
        // Serial Mapping Data
        FALSE,                       // Use IPU serial communication with panel?
        0,                           // Number of serial data transfer cycles
        NULL,                        // Pointer to serial bus mappings
        {
            0,                       // Serial Clk Period
            0,                       // CS Up Position
            0,                       // CS Down Position
            0,                       // Clk Up Position
            0,                       // Clk Down Position
            0,                       // RS Up Position
            0,                       // RS Down Position
        },
    
        // Dynamic panel data
        DISPLAY_PANEL_HDMI_WUXGA,     // Starting panel mode corresponding to array index.
                                     // Mode may change if array is rearranged during driver init.
        FALSE,                       // Has panel been initialized yet?  Must be set to FALSE here.
        FALSE,                       // Is panel active (always start out off)
        DC_DISPLAY_0,                // IPU Display number (initialized to 0)
    },    
#endif

#ifdef BSP_DISPLAY_DVI
    // DVI 800x600 Definitions
    {
        (PUCHAR) "DVI SVGA Panel",   // Name
        IPU_PANEL_SYNC_TYPE_SYNCHRONOUS, // Panel Sync Type
        DI_SELECT_DI0,               // Display Interface Number
        DISPLAY_PANEL_DVI_SVGA,      // Panel ID
        DISPLAY_PORT_DVI,            // type
        DISPLAY_CLOCK_INTERNAL,      // di clk source
        IPU_PIX_FMT_RGB24,           // Pixel Format
        800,                         // width
        600,                         // height
        60,                          // frequency
        1,                           // transfer cycles
        &DVIMapping,                 // Mapping for DVI RGB data

        // Synchronous Panel Info
#if ALTERNATE_SETTING //(0)
        //LLLLL's configure
        4,                           // Vertical Sync width (in vertical lines)
        18,                          // Vertical Start Width (in vertical lines)
        3,                           // Vertical End Width (in vertical lines)
        40,                          // Horizontal Sync Width (in pixel clock cyles)
        20,                          // Horizontal Start Width (in pixel clock cyles)
        27,                          // Horizontal End Width (in pixel clock cyles)
        33250000,                    // Pixel Clock Cycle Frequency: IPU clock 133Mhz, 133/40=3.325 so that we choose 4 as  dividor.
#else
        4,                           // Vertical Sync width (in vertical lines)
        19,                          // Vertical Start Width (in vertical lines)
        5,                           // Vertical End Width (in vertical lines)
        128,                         // Horizontal Sync Width (in pixel clock cyles)
        88,                          // Horizontal Start Width (in pixel clock cyles)
        40,                          // Horizontal End Width (in pixel clock cyles)
        40000000,                    // Pixel Clock Cycle Frequency: IPU clock 133Mhz, 133/40=3.325 so that we choose 4 as dividor.
#endif
        0,                           // Pixel Data Offset Position
        0,                           // Pixel Clock Up, in ns (if tick is 7.52ns(133Mhz), 0 HSP cycles)
        15,                          // Pixel Clock Down, in ns (if tick is 7.52ns(133Mhz), 1 HSP cycles)
        {       // Display Interface signal polarities
            TRUE,                        // Data mask enable
            FALSE,                       // Clock Idle enable
            FALSE,                       // Clock Select Enable
            FALSE,                       // Vertical Sync Polarity
            TRUE,                        // Output enable polarity(straight polarity for sharp signals)
            FALSE,                       // Data Pol   Inverse polarity.
            FALSE,                       // Clock Pol  Inverted polarity.
            FALSE,                       // HSync Pol
        },
        // TV only sync panel info
        0,                            // Field0 VStart width in lines
        0,                            // Field0 VEnd width in lines
        0,                            // Field1 VStart width in lines
        0,                            // Field1 VEnd width in lines
        FALSE,                        // TRUE if TV output data is interlaced; FALSE if progressive

        // Asynchronous Panel Info
        0,                           // Read Cycle Period
        0,                           // Read Up Position
        0,                           // Read Down Position
        0,                           // Write Cycle Period
        0,                           // Write Up Position
        0,                           // Write Down Position
        {       // ADC Display Interface signal polarities
            0,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
        },

        // Serial Mapping Data
        FALSE,                       // Use IPU serial communication with panel?
        0,                           // Number of serial data transfer cycles
        NULL,                        // Pointer to serial bus mappings
        {
            0,                       // Serial Clk Period
            0,                       // CS Up Position
            0,                       // CS Down Position
            0,                       // Clk Up Position
            0,                       // Clk Down Position
            0,                       // RS Up Position
            0,                       // RS Down Position
        },

        // Dynamic panel data
        DISPLAY_PANEL_DVI_SVGA,      // Starting panel mode corresponding to array index.
                                     // Mode may change if array is rearranged during driver init.
        FALSE,                       // Has panel been initialized yet?  Must be set to FALSE here.
        FALSE,                       // Is panel active (always start out off)
        DC_DISPLAY_0,                // IPU Display number (initialized to 0)
    },

    // DVI 1024x768 Definitions
    {
        (PUCHAR) "DVI XGA Panel",    // Name
        IPU_PANEL_SYNC_TYPE_SYNCHRONOUS, // Panel Sync Type
        DI_SELECT_DI0,               // Display Interface Number
        DISPLAY_PANEL_DVI_XGA,       // Panel ID
        DISPLAY_PORT_DVI,            // Port type
        DISPLAY_CLOCK_INTERNAL,      // di clk source
        IPU_PIX_FMT_RGB24,           // Pixel Format
        1024,                        // width
        768,                         // height
        60,                          // frequency
        1,                           // transfer cycles
        &DVIMapping,                 // Mapping for DVI RGB data

        // Synchronous Panel Info
#if ALTERNATE_SETTING //(0)
        //LLLLL's configure
        6,                           // Vertical Sync width (in vertical lines)
        22,                          // Vertical Start Width (in vertical lines)
        4,                           // Vertical End Width (in vertical lines)
        100,                         // Horizontal Sync Width (in pixel clock cyles)
        60,                          // Horizontal Start Width (in pixel clock cyles)
        40,                          // Horizontal End Width (in pixel clock cyles)
        58800000,                    // Pixel Clock Cycle Frequency: IPU clock 133Mhz, 133/24=5.54 so that we choose 6 as dividor.
#else
        6,                           // Vertical Sync width (in vertical lines)
        3,                          // Vertical Start Width (in vertical lines)
        29,                           // Vertical End Width (in vertical lines)
        136,                         // Horizontal Sync Width (in pixel clock cyles)
        24,                         // Horizontal Start Width (in pixel clock cyles)
        160,                          // Horizontal End Width (in pixel clock cyles)
        65000000,                    // Pixel Clock Cycle Frequency: IPU clock 133Mhz, 133/24=5.54 so that we choose 6 as dividor.
#endif
        0,                           // Pixel Data Offset Position
        0,                           // Pixel Clock Up, in ns (if tick is 7.52ns(133Mhz), 2 HSP cycles)
        8,                           // Pixel Clock Down, in ns (if tick is 7.52ns(133Mhz), 4.5 HSP cycles)
        {       // Display Interface signal polarities
            TRUE,                        // Data mask enable
            FALSE,                       // Clock Idle enable
            FALSE,                       // Clock Select Enable
            FALSE,                       // Vertical Sync Polarity
            TRUE,                        // Output enable polarity(straight polarity for sharp signals)
            FALSE,                       // Data Pol   Inverse polarity.
            FALSE,                       // Clock Pol  Inverse polarity.
            FALSE,                       // HSync Pol
        },
        // TV only sync panel info
        0,                            // Field0 VStart width in lines
        0,                            // Field0 VEnd width in lines
        0,                            // Field1 VStart width in lines
        0,                            // Field1 VEnd width in lines
        FALSE,                        // TRUE if TV output data is interlaced; FALSE if progressive

        // Asynchronous Panel Info
        0,                           // Read Cycle Period
        0,                           // Read Up Position
        0,                           // Read Down Position
        0,                           // Write Cycle Period
        0,                           // Write Up Position
        0,                           // Write Down Position
        {       // ADC Display Interface signal polarities
            0,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
        },

        // Serial Mapping Data
        FALSE,                       // Use IPU serial communication with panel?
        0,                           // Number of serial data transfer cycles
        NULL,                        // Pointer to serial bus mappings
        {
            0,                       // Serial Clk Period
            0,                       // CS Up Position
            0,                       // CS Down Position
            0,                       // Clk Up Position
            0,                       // Clk Down Position
            0,                       // RS Up Position
            0,                       // RS Down Position
        },

        // Dynamic panel data
        DISPLAY_PANEL_DVI_XGA,       // Starting panel mode corresponding to array index.
                                     // Mode may change if array is rearranged during driver init.
        FALSE,                       // Has panel been initialized yet?  Must be set to FALSE here.
        FALSE,                       // Is panel active (always start out off)
        DC_DISPLAY_0,                // IPU Display number (initialized to 0)
    },

    // DVI 1280x1024 Definitions
    {
        (PUCHAR) "DVI SXGA Panel",   // Name
        IPU_PANEL_SYNC_TYPE_SYNCHRONOUS, // Panel Sync Type
        DI_SELECT_DI0,               // Display Interface Number
        DISPLAY_PANEL_DVI_SXGA,      // Panel ID
        DISPLAY_PORT_DVI,            // Port type
        DISPLAY_CLOCK_EXTERNAL,      // di clk source
        IPU_PIX_FMT_RGB24,           // Pixel Format
        1280,                        // width
        1024,                        // height
        60,                          // frequency
        1,                           // transfer cycles
        &DVIMapping,                 // Mapping for DVI RGB data

        // Synchronous Panel Info
#if ALTERNATE_SETTING //(0)
        //LLLLL's configure
        6,                           // Vertical Sync width (in vertical lines)
        10,                          // Vertical Start Width (in vertical lines)
        4,                           // Vertical End Width (in vertical lines)
        100,                         // Horizontal Sync Width (in pixel clock cyles)
        69,                           // Horizontal Start Width (in pixel clock cyles)
        20,                          // Horizontal End Width (in pixel clock cyles)
        90900000,                    // Pixel Clock Cycle Frequency: IPU clock 133Mhz, 133/24=5.54 so that we choose 6 as dividor.
#else
        3,                           // Vertical Sync width (in vertical lines)
        1,//35,                          // Vertical Start Width (in vertical lines)
        38,//4,                           // Vertical End Width (in vertical lines)
        112,                         // Horizontal Sync Width (in pixel clock cyles)
        48,                         // Horizontal Start Width (in pixel clock cyles)
        248,//148,//                          // Horizontal End Width (in pixel clock cyles)
        108000000,//100000000,//                   // Pixel Clock Cycle Frequency: IPU clock 133Mhz, 133/24=5.54 so that we choose 6 as dividor.
#endif
        0,                           // Pixel Data Offset Position
        0,                           // Pixel Clock Up, in ns (if tick is 7.52ns(133Mhz), 2 HSP cycles)
        5,                           // Pixel Clock Down, in ns (if tick is 7.52ns(133Mhz), 4.5 HSP cycles)
        {       // Display Interface signal polarities
            TRUE,                        // Data mask enable
            FALSE,                       // Clock Idle enable
            FALSE,                       // Clock Select Enable
            FALSE,                       // Vertical Sync Polarity
            TRUE,                        // Output enable polarity(straight polarity for sharp signals)
            FALSE,                       // Data Pol   Inverse polarity.
            FALSE,                       // Clock Pol  Inverse polarity.
            FALSE,                       // HSync Pol
        },
        // TV only sync panel info
        0,                            // Field0 VStart width in lines
        0,                            // Field0 VEnd width in lines
        0,                            // Field1 VStart width in lines
        0,                            // Field1 VEnd width in lines
        FALSE,                        // TRUE if TV output data is interlaced; FALSE if progressive

        // Asynchronous Panel Info
        0,                           // Read Cycle Period
        0,                           // Read Up Position
        0,                           // Read Down Position
        0,                           // Write Cycle Period
        0,                           // Write Up Position
        0,                           // Write Down Position
        {       // ADC Display Interface signal polarities
            0,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
        },

        // Serial Mapping Data
        FALSE,                       // Use IPU serial communication with panel?
        0,                           // Number of serial data transfer cycles
        NULL,                        // Pointer to serial bus mappings
        {
            0,                       // Serial Clk Period
            0,                       // CS Up Position
            0,                       // CS Down Position
            0,                       // Clk Up Position
            0,                       // Clk Down Position
            0,                       // RS Up Position
            0,                       // RS Down Position
        },

        // Dynamic panel data
        DISPLAY_PANEL_DVI_SXGA,      // Starting panel mode corresponding to array index.
                                     // Mode may change if array is rearranged during driver init.
        FALSE,                       // Has panel been initialized yet?  Must be set to FALSE here.
        FALSE,                       // Is panel active (always start out off)
        DC_DISPLAY_0,                // IPU Display number (initialized to 0)
    },

    // DVI 1600x1200 Definitions
    {
        (PUCHAR) "DVI UXGA Panel",   // Name
        IPU_PANEL_SYNC_TYPE_SYNCHRONOUS, // Panel Sync Type
        DI_SELECT_DI0,               // Display Interface Number
        DISPLAY_PANEL_DVI_UXGA,      // Panel ID
        DISPLAY_PORT_DVI,            // Port type
        DISPLAY_CLOCK_EXTERNAL,      // di clk source
        IPU_PIX_FMT_RGB24,           // Pixel Format
        1600,                        // width
        1200,                        // height
        60,                          // frequency
        1,                           // transfer cycles
        &DVIMapping,                 // Mapping for DVI RGB data
    
        // Synchronous Panel Info
        3,                           // Vertical Sync width (in vertical lines)
        1,                           // Vertical Start Width (in vertical lines)
        46,                          // Vertical End Width (in vertical lines)
        192,                         // Horizontal Sync Width (in pixel clock cyles)
        64,                          // Horizontal Start Width (in pixel clock cyles)
        304,//150,//                         // Horizontal End Width (in pixel clock cyles)
        162000000,//150000000,//                   // Pixel Clock Cycle Frequency: IPU clock 133Mhz, 133/24=5.54 so that we choose 6 as dividor.
        0,                           // Pixel Data Offset Position
        0,                           // Pixel Clock Up, in ns (if tick is 7.52ns(133Mhz), 2 HSP cycles)
        4,                           // Pixel Clock Down, in ns (if tick is 7.52ns(133Mhz), 4.5 HSP cycles)
        {       // Display Interface signal polarities
            TRUE,                        // Data mask enable
            FALSE,                       // Clock Idle enable
            FALSE,                       // Clock Select Enable
            FALSE,                       // Vertical Sync Polarity
            TRUE,                        // Output enable polarity(straight polarity for sharp signals)
            FALSE,                       // Data Pol   Inverse polarity.
            FALSE,                       // Clock Pol  Inverse polarity.
            FALSE,                       // HSync Pol
        },
        // TV only sync panel info
        0,                            // Field0 VStart width in lines
        0,                            // Field0 VEnd width in lines
        0,                            // Field1 VStart width in lines
        0,                            // Field1 VEnd width in lines
        FALSE,                        // TRUE if TV output data is interlaced; FALSE if progressive
    
        // Asynchronous Panel Info
        0,                           // Read Cycle Period
        0,                           // Read Up Position
        0,                           // Read Down Position
        0,                           // Write Cycle Period
        0,                           // Write Up Position
        0,                           // Write Down Position
        {       // ADC Display Interface signal polarities
            0,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
        },
    
        // Serial Mapping Data
        FALSE,                       // Use IPU serial communication with panel?
        0,                           // Number of serial data transfer cycles
        NULL,                        // Pointer to serial bus mappings
        {
            0,                       // Serial Clk Period
            0,                       // CS Up Position
            0,                       // CS Down Position
            0,                       // Clk Up Position
            0,                       // Clk Down Position
            0,                       // RS Up Position
            0,                       // RS Down Position
        },
    
        // Dynamic panel data
        DISPLAY_PANEL_DVI_UXGA,      // Starting panel mode corresponding to array index.
                                     // Mode may change if array is rearranged during driver init.
        FALSE,                       // Has panel been initialized yet?  Must be set to FALSE here.
        FALSE,                       // Is panel active (always start out off)
        DC_DISPLAY_0,                // IPU Display number (initialized to 0)
    },

    // DVI 1920x1080 Definitions
    {
        (PUCHAR) "DVI 1080p Panel",   // Name
        IPU_PANEL_SYNC_TYPE_SYNCHRONOUS, // Panel Sync Type
        DI_SELECT_DI0,               // Display Interface Number
        DISPLAY_PANEL_DVI_1080P,      // Panel ID
        DISPLAY_PORT_DVI,            // Port type
        DISPLAY_CLOCK_EXTERNAL,      // di clk source
        IPU_PIX_FMT_RGB24,           // Pixel Format
        1920,                        // width
        1080,                        // height
        60,                          // frequency
        1,                           // transfer cycles
        &DVIMapping,                 // Mapping for DVI RGB data

        // Synchronous Panel Info
        5,                               // Vertical Sync width   
        4,                               // Vertical Start Width
        36,                              // Vertical End Width
        44,                              // Horizontal Sync Width 
        88,                              // Horizontal Start Width
        148,                             // Horizontal End Width
        148500000,                // Pixel Clock Cycle Frequency: 
        0,                           // Pixel Data Offset Position
        0,                           // Pixel Clock Up, in ns
        3,                           // Pixel Clock Down, in ns
        {       // Display Interface signal polarities
            TRUE,                        // Data mask enable
            FALSE,                       // Clock Idle enable
            FALSE,                       // Clock Select Enable
            FALSE,                       // Vertical Sync Polarity
            TRUE,                        // Output enable polarity(straight polarity for sharp signals)
            FALSE,                       // Data Pol   Inverse polarity.
            FALSE,                       // Clock Pol  Inverse polarity.
            FALSE,                       // HSync Pol
        },
        // TV only sync panel info
        0,                            // Field0 VStart width in lines
        0,                            // Field0 VEnd width in lines
        0,                            // Field1 VStart width in lines
        0,                            // Field1 VEnd width in lines
        FALSE,                        // TRUE if TV output data is interlaced; FALSE if progressive

        // Asynchronous Panel Info
        0,                           // Read Cycle Period
        0,                           // Read Up Position
        0,                           // Read Down Position
        0,                           // Write Cycle Period
        0,                           // Write Up Position
        0,                           // Write Down Position
        {       // ADC Display Interface signal polarities
            0,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
        },

        // Serial Mapping Data
        FALSE,                       // Use IPU serial communication with panel?
        0,                           // Number of serial data transfer cycles
        NULL,                        // Pointer to serial bus mappings
        {
            0,                       // Serial Clk Period
            0,                       // CS Up Position
            0,                       // CS Down Position
            0,                       // Clk Up Position
            0,                       // Clk Down Position
            0,                       // RS Up Position
            0,                       // RS Down Position
        },

        // Dynamic panel data
        DISPLAY_PANEL_DVI_1080P,     // Starting panel mode corresponding to array index.
                                     // Mode may change if array is rearranged during driver init.
        FALSE,                       // Has panel been initialized yet?  Must be set to FALSE here.
        FALSE,                       // Is panel active (always start out off)
        DC_DISPLAY_0,                // IPU Display number (initialized to 0)
    },
#if 0
    // Two DVI resolution configuration which will not be annouced to support currently.
    // DVI 1280x800 Definitions
    {
        (PUCHAR) "DVI SXGA Panel",   // Name
        IPU_PANEL_SYNC_TYPE_SYNCHRONOUS, // Panel Sync Type
        DI_SELECT_DI0,               // Display Interface Number
        DISPLAY_PANEL_DVI_SXGA,      // Panel ID
        DISPLAY_PORT_DVI,            // Port type
        DISPLAY_CLOCK_INTERNAL,      // di clk source
        IPU_PIX_FMT_RGB24,           // Pixel Format
        1280,                        // width
        800,                        // height
        60,                          // frequency
        1,                           // transfer cycles
        &DVIMapping,                 // Mapping for DVI RGB data
    
        // Synchronous Panel Info
        6,                           // Vertical Sync width (in vertical lines)
        10,                          // Vertical Start Width (in vertical lines)
        4,                           // Vertical End Width (in vertical lines)
        71,                         // Horizontal Sync Width (in pixel clock cyles)
        80,                         // Horizontal Start Width (in pixel clock cyles)
        20,                          // Horizontal End Width (in pixel clock cyles)
        71400000,                    // Pixel Clock Cycle Frequency: IPU clock 133Mhz, 133/24=5.54 so that we choose 6 as dividor.
        0,                           // Pixel Data Offset Position
        0,                           // Pixel Clock Up, in ns (if tick is 7.52ns(133Mhz), 2 HSP cycles)
        4,                          // Pixel Clock Down, in ns (if tick is 7.52ns(133Mhz), 4.5 HSP cycles)
    
        {       // Display Interface signal polarities
            TRUE,                        // Data mask enable
            FALSE,                       // Clock Idle enable
            FALSE,                       // Clock Select Enable
            FALSE,                       // Vertical Sync Polarity
            TRUE,                        // Output enable polarity(straight polarity for sharp signals)
            FALSE,                       // Data Pol   Inverse polarity.
            FALSE,                       // Clock Pol  Inverse polarity.
            FALSE,                       // HSync Pol
        },
        // TV only sync panel info
        0,                            // Field0 VStart width in lines
        0,                            // Field0 VEnd width in lines
        0,                            // Field1 VStart width in lines
        0,                            // Field1 VEnd width in lines
        FALSE,                        // TRUE if TV output data is interlaced; FALSE if progressive
    
        // Asynchronous Panel Info
        0,                           // Read Cycle Period
        0,                           // Read Up Position
        0,                           // Read Down Position
        0,                           // Write Cycle Period
        0,                           // Write Up Position
        0,                           // Write Down Position
        {       // ADC Display Interface signal polarities
            0,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
        },
    
        // Serial Mapping Data
        FALSE,                       // Use IPU serial communication with panel?
        0,                           // Number of serial data transfer cycles
        NULL,                        // Pointer to serial bus mappings
        {
            0,                       // Serial Clk Period
            0,                       // CS Up Position
            0,                       // CS Down Position
            0,                       // Clk Up Position
            0,                       // Clk Down Position
            0,                       // RS Up Position
            0,                       // RS Down Position
        },
    
        // Dynamic panel data
        DISPLAY_PANEL_DVI_SXGA,      // Starting panel mode corresponding to array index.
                                     // Mode may change if array is rearranged during driver init.
        FALSE,                       // Has panel been initialized yet?  Must be set to FALSE here.
        FALSE,                       // Is panel active (always start out off)
        DC_DISPLAY_0,                // IPU Display number (initialized to 0)
    },
#endif


#endif

#ifdef BSP_DISPLAY_LVDS1
#if USE_CHIMEI_1080P_LVDS_PANEL
    // LVDS 1920x1080 Definitions
    {
      (PUCHAR) "LVDS 1920x1080 Panel",   // Name
        IPU_PANEL_SYNC_TYPE_SYNCHRONOUS, // Panel Sync Type
        DI_SELECT_DI0,         // Display Interface Number
        DISPLAY_PANEL_LVDS1,       // Panel ID
        DISPLAY_PORT_DLVDS,       // Port type
        DISPLAY_CLOCK_EXTERNAL,    // di clk source
        IPU_PIX_FMT_RGB24,       // Pixel Format
        1920,             // width
        1080,              // height
        62,              // frequency
        1,               // transfer cycles
        &LVDSMapping,         // Mapping for LVDS RGB data
  
        // Synchronous Panel Info
        5,                 // Vertical Sync width   
        6,                // Vertical Start Width
        4,                 // Vertical End Width
        5,                 // Horizontal Sync Width 
        19,               // Horizontal Start Width
        40,               // Horizontal End Width
        130000000,          // Pixel Clock Cycle Frequency: IPU clock 133Mhz, 133/44.3=3 so that we choose 3 as dividor.
        0,               // Pixel Data Offset Position
        0,               // Pixel Clock Up, in ns
        4,               // Pixel Clock Down, in ns
        {    // Display Interface signal polarities
            TRUE,             // Data mask enable
            FALSE,             // Clock Idle enable
            FALSE,             // Clock Select Enable
            FALSE,             // Vertical Sync Polarity
            TRUE,             // Output enable polarity(straight polarity for sharp signals)
            FALSE,             // Data Pol   Inverse polarity.
            FALSE,             // Clock Pol  Inverse polarity.
            FALSE,             // HSync Pol
        },
      // TV only sync panel info
        0,                // Field0 VStart width in lines
        0,                // Field0 VEnd width in lines
        0,                // Field1 VStart width in lines
        0,                // Field1 VEnd width in lines
        FALSE,              // TRUE if TV output data is interlaced; FALSE if progressive
  
      // Asynchronous Panel Info
        0,               // Read Cycle Period
        0,               // Read Up Position
        0,               // Read Down Position
        0,               // Write Cycle Period
        0,               // Write Up Position
        0,               // Write Down Position
        {    // ADC Display Interface signal polarities
            0,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
        },
  
      // Serial Mapping Data
        FALSE,             // Use IPU serial communication with panel?
        0,               // Number of serial data transfer cycles
        NULL,             // Pointer to serial bus mappings
        {
            0,             // Serial Clk Period
            0,             // CS Up Position
            0,             // CS Down Position
            0,             // Clk Up Position
            0,             // Clk Down Position
            0,             // RS Up Position
            0,             // RS Down Position
        },
  
      // Dynamic panel data
        DISPLAY_PANEL_LVDS1,      // Starting panel mode corresponding to array index.
                     // Mode may change if array is rearranged during driver init.
        FALSE,             // Has panel been initialized yet?  Must be set to FALSE here.
        FALSE,             // Is panel active (always start out off)
        DC_DISPLAY_0,         // IPU Display number (initialized to 0)
    },
  
#else

    // LVDS 1024x768 Definitions
    {
        (PUCHAR) "LVDS 1024x768 Panel",   // Name
        IPU_PANEL_SYNC_TYPE_SYNCHRONOUS, // Panel Sync Type
        DI_SELECT_DI0,         // Display Interface Number
        DISPLAY_PANEL_LVDS1,       // Panel ID
        DISPLAY_PORT_LVDS1,       // Port type
        DISPLAY_CLOCK_EXTERNAL,    // di clk source
        IPU_PIX_FMT_RGB24,       // Pixel Format
        1024,             // width
        768,             // height
        62,              // frequency
        1,               // transfer cycles
        &LVDSMapping,         // Mapping for LVDS RGB data
   
      // Synchronous Panel Info
        9,               // Vertical Sync width (in vertical lines)
        24,              // Vertical Start Width (in vertical lines)
        5,               // Vertical End Width (in vertical lines)
        58,              // Horizontal Sync Width (in pixel clock cyles)
        174,              // Horizontal Start Width (in pixel clock cyles)
        88,              // Horizontal End Width (in pixel clock cyles)
        65000000,           // Display clock range from 38Mhz to 113Mhz
        0,               // Pixel Data Offset Position
        0,               // Pixel Clock Up, in ns
        8,               // Pixel Clock Down, in ns
        {    // Display Interface signal polarities
            TRUE,             // Data mask enable
            FALSE,             // Clock Idle enable
            FALSE,             // Clock Select Enable
            FALSE,             // Vertical Sync Polarity
            TRUE,             // Output enable polarity(straight polarity for sharp signals)
            FALSE,             // Data Pol   Inverse polarity.
            FALSE,             // Clock Pol  Inverse polarity.
            FALSE,             // HSync Pol
        },
      // TV only sync panel info
        0,                // Field0 VStart width in lines
        0,                // Field0 VEnd width in lines
        0,                // Field1 VStart width in lines
        0,                // Field1 VEnd width in lines
        FALSE,              // TRUE if TV output data is interlaced; FALSE if progressive
   
      // Asynchronous Panel Info
        0,               // Read Cycle Period
        0,               // Read Up Position
        0,               // Read Down Position
        0,               // Write Cycle Period
        0,               // Write Up Position
        0,               // Write Down Position
        {    // ADC Display Interface signal polarities
            0,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
        },
   
        // Serial Mapping Data
        FALSE,             // Use IPU serial communication with panel?
        0,               // Number of serial data transfer cycles
        NULL,             // Pointer to serial bus mappings
       {
            0,             // Serial Clk Period
            0,             // CS Up Position
            0,             // CS Down Position
            0,             // Clk Up Position
            0,             // Clk Down Position
            0,             // RS Up Position
            0,             // RS Down Position
        },
   
      // Dynamic panel data
        DISPLAY_PANEL_LVDS1,      // Starting panel mode corresponding to array index.
                     // Mode may change if array is rearranged during driver init.
        FALSE,             // Has panel been initialized yet?  Must be set to FALSE here.
        FALSE,             // Is panel active (always start out off)
        DC_DISPLAY_0,         // IPU Display number (initialized to 0)
    },
#endif
#endif

#ifdef BSP_DISPLAY_WVGA
    // CHUNGHWA WVGA Definitions
    {
        (PUCHAR) "CHUNGHWA WVGA Panel",          // Name
        IPU_PANEL_SYNC_TYPE_SYNCHRONOUS,         // Panel Sync Type
        DI_SELECT_DI0,                           // Display Interface Number
        DISPLAY_PANEL_CHUNGHWA_WVGA,                 // Type
        DISPLAY_PORT_CHUNGHWA,                // Mode ID
        DISPLAY_CLOCK_INTERNAL,      // di clk source
        IPU_PIX_FMT_RGB565,                      // Pixel Format
        800,                                     // Width
        480,                                     // Height
        60,                                      // Frequency
        1,                                       // Transfer cycles
        &ChunghwaMapping[0],                        // Mapping for CHUNGHWA RGB data
        
        // Synchronous Panel Info
        2,                                       // Vertical Sync width (in vertical lines)
        13,//   3                                       // Vertical Start Width (in vertical lines)
        5,                                       // Vertical End Width (in vertical lines)
        5,                                       // Horizontal Sync Width (in pixel clock cyles)
        65,                                       // Horizontal Start Width (in pixel clock cyles)
        14,//   10                                      // Horizontal End Width (in pixel clock cyles)
        26200000,//25951219,                                // Pixel Clock Cyle Frequency
        0,                                       // Pixel Data Offset Position
        11,                                      // Pixel Clock Up, in ns (if tick is 5ns(200Mhz), 3 HSP cycles)
        26,                                      // Pixel Clock Down, in ns (if tick is 5ns(200Mhz), 7 HSP cycles)
        {       // Display Interface signal polarities
            FALSE,                               // Data mask enable
            FALSE,                               // Clock Idle enable
            FALSE,                               // Clock Select Enable
            FALSE,                               // Vertical Sync Polarity
            TRUE,                                // Output enable polarity(straight polarity for sharp signals)
            FALSE,                               // Data Pol   Inverse polarity.
            FALSE,                               // Clock Pol  Inverse polarity.
            FALSE,                               // HSync Pol
        },
        // TV only sync panel info
        0,                                      // Field0 VStart width in lines
        0,                                      // Field0 VEnd width in lines
        0,                                      // Field1 VStart width in lines
        0,                                      // Field1 VEnd width in lines
        FALSE,                                  // TRUE if TV output data is interlaced; FALSE if progressive
    
        // Asynchronous Panel Info
        0,                                      // Read Cycle Period
        0,                                      // Read Up Position
        0,                                      // Read Down Position
        0,                                      // Write Cycle Period
        0,                                      // Write Up Position
        0,                                      // Write Down Position
        {      // ADC Display Interface signal polarities
            0,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
        },
        
        // Serial Mapping Data
        FALSE,                                  // Use IPU serial communication with panel?
        0,                                      // Number of serial data transfer cycles
        NULL,                                   // Pointer to serial bus mappings
        {
            0,                                  // Serial Clk Period
            0,                                  // CS Up Position
            0,                                  // CS Down Position
            0,                                  // Clk Up Position
            0,                                  // Clk Down Position
            0,                                  // RS Up Position
            0,                                  // RS Down Position
        },
    
        // Dynamic panel data
        DISPLAY_PANEL_CHUNGHWA_WVGA,            // Starting panel mode corresponding to array index.
                                                // Mode may change if array is rearranged during driver init.
        FALSE,                                  // Has panel been initialized yet?
        FALSE,                                  // Is panel active (always start out off)
        DC_DISPLAY_0,                           // IPU Display number (initialized to 0)
    },
#endif

#ifdef BSP_DISPLAY_LVDS2
    // LVDS 1024x768 Definitions
    {
        (PUCHAR) "LVDS 1024x768 Panel",   // Name
        IPU_PANEL_SYNC_TYPE_SYNCHRONOUS, // Panel Sync Type
        DI_SELECT_DI1,         // Display Interface Number
        DISPLAY_PANEL_LVDS2,       // Panel ID
        DISPLAY_PORT_LVDS2,       // Port type
        DISPLAY_CLOCK_EXTERNAL,    // di clk source
        IPU_PIX_FMT_RGB666,       // Pixel Format
        1024,             // width
        768,             // height
        63,              // frequency
        1,               // transfer cycles
        &LVDSMapping,         // Mapping for LVDS RGB data
   
      // Synchronous Panel Info
        9,               // Vertical Sync width (in vertical lines)
        24,              // Vertical Start Width (in vertical lines)
        5,               // Vertical End Width (in vertical lines)
        58,              // Horizontal Sync Width (in pixel clock cyles)
        174,              // Horizontal Start Width (in pixel clock cyles)
        88,              // Horizontal End Width (in pixel clock cyles)
        65000000,           // Pixel Clock Cycle Frequency: IPU clock 133Mhz, 133/44.3=3 so that we choose 3 as dividor.
        0,               // Pixel Data Offset Position
        0,               // Pixel Clock Up, in ns
        8,               // Pixel Clock Down, in ns
        {    // Display Interface signal polarities
            TRUE,             // Data mask enable
            FALSE,             // Clock Idle enable
            FALSE,             // Clock Select Enable
            FALSE,             // Vertical Sync Polarity
            TRUE,             // Output enable polarity(straight polarity for sharp signals)
            FALSE,             // Data Pol   Inverse polarity.
            FALSE,             // Clock Pol  Inverse polarity.
            FALSE,             // HSync Pol
        },
      // TV only sync panel info
        0,                // Field0 VStart width in lines
        0,                // Field0 VEnd width in lines
        0,                // Field1 VStart width in lines
        0,                // Field1 VEnd width in lines
        FALSE,              // TRUE if TV output data is interlaced; FALSE if progressive
   
      // Asynchronous Panel Info
        0,               // Read Cycle Period
        0,               // Read Up Position
        0,               // Read Down Position
        0,               // Write Cycle Period
        0,               // Write Up Position
        0,               // Write Down Position
        {    // ADC Display Interface signal polarities
            0,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
            IPU_POLARITY_ACTIVE_LOW,
        },
   
        // Serial Mapping Data
        FALSE,             // Use IPU serial communication with panel?
        0,               // Number of serial data transfer cycles
        NULL,             // Pointer to serial bus mappings
       {
            0,             // Serial Clk Period
            0,             // CS Up Position
            0,             // CS Down Position
            0,             // Clk Up Position
            0,             // Clk Down Position
            0,             // RS Up Position
            0,             // RS Down Position
        },
   
      // Dynamic panel data
        DISPLAY_PANEL_LVDS2,      // Starting panel mode corresponding to array index.
                     // Mode may change if array is rearranged during driver init.
        FALSE,             // Has panel been initialized yet?  Must be set to FALSE here.
        FALSE,             // Is panel active (always start out off)
        DC_DISPLAY_0,         // IPU Display number (initialized to 0)
    },
#endif
#endif

};
//#pragma pack(pop)


//------------------------------------------------------------------------------
// Functions

#endif   // __PANELS_H__
