//------------------------------------------------------------------------------
//
//  Copyright (C) 2009-2010, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  lvds.h
//
//  Provides routines for enabling, disabling, and initializing
//  communication with the LVDS display interface.
//
//------------------------------------------------------------------------------

#ifndef __LVDS_H__
#define __LVDS_H__

#ifdef __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------
// Defines


//------------------------------------------------------------------------------
// Types


//------------------------------------------------------------------------------
// Functions

void LVDSInitializePanel(PANEL_INFO *pPanelInfo);
void LVDSEnablePanel(PANEL_INFO *pPanelInfo);
void LVDSDisablePanel(PANEL_INFO *pPanelInfo);

#ifdef __cplusplus
}
#endif

#endif // __LVDS_H__

