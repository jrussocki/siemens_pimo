//------------------------------------------------------------------------------
//
//  Copyright (C) 2009-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  dvi.cpp
//
//  Provides routines for enabling, disabling, and initializing
//  communication through the DVI interface
//
//------------------------------------------------------------------------------

#pragma warning(push)
#pragma warning(disable: 4115 4201 4204 4214)
#include <windows.h>

#include <Devload.h>
#include <ceddk.h>
#include <cmnintrin.h>
#pragma warning(pop)

#include "IpuBuffer.h"
#include "Ipu_base.h"
#include "Ipu_common.h"

#include "dc.h"
#include "di.h"
#include "bsp.h"
#include "dvi.h"
#include "bspdisplay.h"

#include "i2cbus.h"

//------------------------------------------------------------------------------
// External Functions
extern BOOL I2CReadNBytes(HANDLE hI2C, BYTE byAddr, BYTE byReg, BYTE *pbyReadBuf, UINT32 nBytesToRead, LPINT lpiResult);
extern BYTE I2CReadOneByte(HANDLE hI2C, BYTE byAddr, BYTE byReg, LPINT lpiResult);
extern VOID I2CWriteNBytes(HANDLE hI2C, BYTE byAddr, BYTE byReg, BYTE *pbyWriteBuf, UINT32 nBytesToWrite, LPINT lpiResult);


//------------------------------------------------------------------------------
// External Variables



//------------------------------------------------------------------------------
// Defines
#define DVI_I2C_ADDRESS        0x50
#define DVI_I2C_SPEED          30000


//------------------------------------------------------------------------------
// Types


//------------------------------------------------------------------------------
// Global Variables


//------------------------------------------------------------------------------
// Local Variables
static HANDLE g_hDVIMonitorEvent;
static DWORD g_dwDVIIrq, g_dwDVISysIntr = (DWORD)SYSINTR_UNDEFINED;
static HANDLE g_hDVIMonitorThread;
static HANDLE g_hI2C = NULL;

//------------------------------------------------------------------------------
// Local Functions
void DVIDeinitPanel();
void DVIMonitorThread(LPVOID lpParameter);

//------------------------------------------------------------------------------
//
// Function: DVIInitializePanel
//
// This function initializes the DVI display interface.
//
// Parameters:
//      pPanelInfo
//          [in] Structure describing panel to initialize
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void DVIInitializePanel(PANEL_INFO *pPanelInfo)
{
    DEBUGMSG(0,(TEXT("[DISPLAY] %s () -\r\n"), __WFUNCTION__));
}

//------------------------------------------------------------------------------
//
// Function: DVIDeinitPanel
//
// This function closes down the DVI module connection and releases all
// resources.
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void DVIDeinitPanel()
{
  
}


//------------------------------------------------------------------------------
//
// Function: DVIEnablePanel
//
// This function enables the connection to the DVI module.
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void DVIEnablePanel(PANEL_INFO *pPanelInfo)
{
    DDK_CLOCK_BAUD_SOURCE DIClkSrc = DDK_CLOCK_BAUD_SOURCE_PLL5_VIDEO;
    DDK_CLOCK_SIGNAL ClkSig = DDK_CLOCK_SIGNAL_PLL5_VIDEO;
    DWORD DIPodf=1, pdffrac=12;
    DWORD i=0;    
    DWORD  ClkSrcFreq=0; 
    DVIMonitorInfo monitorInfo;

    memset(&monitorInfo, 0, sizeof(DVIMonitorInfo));   
        
    DVIGetMonitorInfo(&monitorInfo);

        
    RETAILMSG(TRUE,(_T("DVIEnablePanel !\r\n")));
    //RETAILMSG(TRUE,(_T("PIX_CLK= %d !\r\n"), pPanelInfo->PIX_CLK_FREQ));    
        
        
    //RETAILMSG(TRUE,(_T("Name=%s, PANELID=%d, PORTID=%d !\r\n"), pPanelInfo->NAME, pPanelInfo->PANELID, pPanelInfo->PORTID));  
    RETAILMSG(TRUE,(_T("WIDTH=%d, HEIGHT=%d, FREQUENCY=%d !\r\n"), pPanelInfo->WIDTH, pPanelInfo->HEIGHT, pPanelInfo->FREQUENCY));  
    
    
    for(i = 8; i >= 1; i--)
    {
        ClkSrcFreq = pPanelInfo->PIX_CLK_FREQ * i;
        
        if((ClkSrcFreq > 650000000) && (ClkSrcFreq < 1300000000))
        {
            ClkSig = DDK_CLOCK_SIGNAL_PLL5_VIDEO;
            DIClkSrc = DDK_CLOCK_BAUD_SOURCE_PLL5_VIDEO;
            DIPodf = i;   
            RETAILMSG(1, (TEXT("DI Clock source from PLL5 ClkDiv=%d, ClkSrcFreq=%d\r\n "),DIPodf, ClkSrcFreq)) ;           
            break;
        }
        else  if(ClkSrcFreq > 246000000)
        {
            //Clock source from  480M_PFD1_540M
               
            ClkSig = DDK_CLOCK_SIGNAL_480M_PFD1_540M;
            DIClkSrc = DDK_CLOCK_BAUD_SOURCE_480M_PFD1_540M;
            DIPodf = i;  
            RETAILMSG(1, (TEXT("DI Clock source from 480M_PFD1_540M ClkDiv=%d, ClkSrcFreq=%d\r\n "),DIPodf, ClkSrcFreq)) ;             
            break;              
        }                 
    }
        
    if( i == 0 )
    {
        ERRORMSG(1, (TEXT("PixClock frequency is Out of PLL capability PixClk=%d\r\n "),pPanelInfo->PIX_CLK_FREQ)) ;
        return;
    }
    
    if(DIClkSrc == DDK_CLOCK_BAUD_SOURCE_480M_PFD1_540M)
    {
        DWORD j=0, k=0;
        DWORD deltaFreq= pPanelInfo->PIX_CLK_FREQ, targetFreq;
        DWORD DeltaTmp=0;
        for(j = 12; j <= 35; j ++)
        {
            for(k = 1; k <= 8; k ++) 
            {
               targetFreq = 480000 * 18 / ( j * k) * 1000;

               DeltaTmp = (pPanelInfo->PIX_CLK_FREQ >= targetFreq) ? (pPanelInfo->PIX_CLK_FREQ - targetFreq) : (targetFreq - pPanelInfo->PIX_CLK_FREQ);
    
               if(deltaFreq > DeltaTmp)  
              {
                  deltaFreq = DeltaTmp;
                  DIPodf = k;  
                  pdffrac = j;                   
               }
            }
        }
            
        ClkSrcFreq = pPanelInfo->PIX_CLK_FREQ * DIPodf;         
        RETAILMSG(1, (TEXT("DIPodf=%d, pdffrac=%d, ClkSrcFreq=%d\r\n"),DIPodf, pdffrac, ClkSrcFreq ));
    }
    
    if(pPanelInfo->DICLK_SOURCE == DISPLAY_CLOCK_EXTERNAL)
    {
        if(pPanelInfo->DI_NUM == DI_SELECT_DI0)
        {
            RETAILMSG(TRUE,(_T("DI0 pll clock source!\r\n")));   

            DDKClockSetFreq(ClkSig, ClkSrcFreq);
            DDKClockConfigBaud(DDK_CLOCK_SIGNAL_IPU1_DI0, DIClkSrc, 0, 0, DIPodf-1);              
        }
        else
        {
            RETAILMSG(TRUE,(_T("DI1 pll clock source!\r\n")));   

            DDKClockSetFreq(ClkSig, ClkSrcFreq);
            DDKClockConfigBaud(DDK_CLOCK_SIGNAL_IPU1_DI1, DIClkSrc, 0, 0, DIPodf-1);                    
        }    
    }

    if(pPanelInfo->DI_NUM == DI_SELECT_DI0)
    {     
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU_DI0, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU, DDK_CLOCK_GATE_MODE_ENABLED_ALL);            
    }
    else
    {
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU_DI1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU, DDK_CLOCK_GATE_MODE_ENABLED_ALL);              
    }

}
//------------------------------------------------------------------------------
//
// Function: DVIDisablePanel
//
// This function disables the DVI connection.
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void DVIDisablePanel()
{

}


//------------------------------------------------------------------------------
//
// Function: DVIMonitorThread
//
// This function is the main thread for monitoring the DVI module to detect
// connection to a new display.
//
// Parameters:
//      lpParameter
//          [in] NULL.
//
// Returns:
//      None
//
//------------------------------------------------------------------------------
void DVIMonitorThread(LPVOID lpParameter)
{
    UNREFERENCED_PARAMETER(lpParameter);
}


//------------------------------------------------------------------------------
//
// Function: DVIGetMonitorInfo
//
// This function retrieve EDID monitor info from the monitor, and filters
// it into useful info for the display driver.
//
// Parameters:
//      monitorInfo
//          [out] Structure holding monitor info.
//
// Returns:
//      TRUE if EDID read operation was successful
//      FALSE if EDID read operation failed
//
//------------------------------------------------------------------------------
BOOL DVIGetMonitorInfo(DVIMonitorInfo *monitorInfo)
{
    UNREFERENCED_PARAMETER(monitorInfo);
    BOOL retVal = TRUE;
    DWORD dwFrequency;
    BYTE bySlaveAddr;
    BYTE EDIDdata[128];

    BYTE establishedTimings1, establishedTimings2;

    dwFrequency = DVI_I2C_SPEED;
    bySlaveAddr = DVI_I2C_ADDRESS;

    memset(EDIDdata, 0, 128);

    // Enable level shifter before using I2C to read EDID
    //EnableDVII2CLevelShifter();

    // Open handle to I2C port and set address and frequency
    if(g_hI2C == NULL)
    {
        g_hI2C = I2COpenHandle(I2C3_FID);   
        if (g_hI2C == INVALID_HANDLE_VALUE)
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: CreateFile for I2C failed!\r\n"), __WFUNCTION__));
        }

        if (!I2CSetMasterMode(g_hI2C))
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: Set I2C Master mode failed!\r\n"), __WFUNCTION__));
        }

        // Initialize the device internal fields
        if (!I2CSetFrequency(g_hI2C, dwFrequency))
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: Set HSI2C frequency failed!\r\n"), __WFUNCTION__));
        }
    }

    INT iResult;

    // Read EDID info
    if (!I2CReadNBytes(g_hI2C, bySlaveAddr, 0, EDIDdata, 128, &iResult))
    {
        retVal = FALSE;
    }

    // Dump EDID data
    for (int i = 0; i < 128; i++)
    {
        RETAILMSG(0,(TEXT("EDID[%d]: 0x%x \r\n"), i, EDIDdata[i]));
    }

    monitorInfo->Height = EDIDdata[21];
    monitorInfo->Width = EDIDdata[22];

    establishedTimings1 = EDIDdata[35];
    establishedTimings2 = EDIDdata[36];
    monitorInfo->iTimingsMask = establishedTimings2 | (establishedTimings1 << 8);

    return retVal;
}
