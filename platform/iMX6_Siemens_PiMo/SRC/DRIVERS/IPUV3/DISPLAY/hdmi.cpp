//------------------------------------------------------------------------------
//
//  Copyright (C) 2009-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  HDMI.cpp
//
//  Provides routines for enabling, disabling, and initializing
//  communication through the  HDMI interface.
//
//------------------------------------------------------------------------------

#pragma warning(push)
#pragma warning(disable: 4115 4201 4204 4214)
#include <windows.h>

#include <Devload.h>
#include <ceddk.h>
#include <cmnintrin.h>
#pragma warning(pop)

#include "IpuBuffer.h"
#include "Ipu_base.h"
#include "Ipu_common.h"

#include "dc.h"
#include "di.h"
#include "bsp.h"
#include "hdmi.h"
#include "bspdisplay.h"

#include "i2cbus.h"

#define DEBUG_HDMI					0

//------------------------------------------------------------------------------
// External Functions
extern BOOL I2CReadNBytes(HANDLE hI2C, BYTE byAddr, BYTE byReg, BYTE *pbyReadBuf, UINT32 nBytesToRead, LPINT lpiResult);
extern BYTE I2CReadOneByte(HANDLE hI2C, BYTE byAddr, BYTE byReg, LPINT lpiResult);
extern VOID I2CWriteNBytes(HANDLE hI2C, BYTE byAddr, BYTE byReg, BYTE *pbyWriteBuf, UINT32 nBytesToWrite, LPINT lpiResult);

//------------------------------------------------------------------------------
// External Variables


//------------------------------------------------------------------------------
// Defines
#define HDMI_I2C_ADDRESS        0x50
#define HDMI_I2C_SPEED          30000

#define HDMI_MUX_CTL_WID 2
#define HDMI_MUX_CTL_LSH 2
#define HDMI_MUX_CTL_IPU1_DI0 0
#define HDMI_MUX_CTL_IPU1_DI1 1
#define HDMI_MUX_CTL_IPU2_DI0 2
#define HDMI_MUX_CTL_IPU2_DI1 3

//------------------------------------------------------------------------------
// Types


//------------------------------------------------------------------------------
// Global Variables


//------------------------------------------------------------------------------
// Local Variables
static HANDLE g_hHDMIMonitorEvent;
static DWORD g_dwHDMIIrq, g_dwHDMISysIntr = (DWORD)SYSINTR_UNDEFINED;
static HANDLE g_hHDMIMonitorThread;
static HANDLE g_hI2C = NULL;

static HDMIMonitorInfo monitorInfo;  

//------------------------------------------------------------------------------
// Local Functions
void HDMIDeinitPanel();
void HDMIMonitorThread(LPVOID lpParameter);


//------------------------------------------------------------------------------
//
// Function: HDMIInitializePanel
//
// This function initializes the HDMI display interface.
//
// Parameters:
//      pPanelInfo
//          [in] Structure describing panel to initialize
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void HDMIInitializePanel(PANEL_INFO *pPanelInfo)
{
    BOOL rc = FALSE; 

	// HDMI CEC
	// TC : IOMux deleted because the configuration is made in the bootloader

    //select ipu1 di0/di1 as hdmi output     
    UINT32 ConfData;
    ConfData = DDKIomuxGetGpr(3);
	
	//Check if it's a dual display configuration
#if defined(BSP_DISPLAY_LVDS2) && defined(BSP_DISPLAY_HDMI)	
	RETAILMSG(DEBUG_HDMI,(_T("HDMI routed to DI1 dual screen mode\r\n")));
	INSREG32BF(&ConfData,HDMI_MUX_CTL,HDMI_MUX_CTL_IPU1_DI1);
#else      
	RETAILMSG(DEBUG_HDMI,(_T("HDMI routed to DI0 single screen mode\r\n")));
	INSREG32BF(&ConfData,HDMI_MUX_CTL,HDMI_MUX_CTL_IPU1_DI0);
#endif

	RETAILMSG(DEBUG_HDMI,(_T(" GPR3 := [0x%x] .\r\n"),ConfData));
	DDKIomuxSetGpr(3, ConfData);

    memset(&monitorInfo, 0, sizeof(HDMIMonitorInfo));

    HDMIGetMonitorInfo(&monitorInfo);  
    
#if 0
    if(!g_hHDMIMonitorThread)
    {
        g_hHDMIMonitorEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
        if(g_hHDMIMonitorEvent == NULL)
        {
            ERRORMSG(1, (TEXT("Create DVI monitor update event failed!\r\n")));
        }

        g_dwHDMIIrq = IRQ_GPIO3_PIN31;
        if (!KernelIoControl(IOCTL_HAL_REQUEST_SYSINTR, &g_dwDVIIrq, sizeof(DWORD),
            &g_dwHDMISysIntr, sizeof(DWORD), NULL)){
            ERRORMSG(TRUE, (TEXT("%s(): fail to map irq into sys intr\r\n"),  __WFUNCTION__));
            goto cleanUp;
        }
     
        // register DVIIrq sys intr
        if (!InterruptInitialize(g_dwDVISysIntr, g_hDVIMonitorEvent, NULL, 0)) {
            ERRORMSG(TRUE, (TEXT("%s(): fail to register sys intr\r\n"),  __WFUNCTION__));
            goto cleanUp;
        }


        PREFAST_SUPPRESS(5451, "affects 64 bit windows only");
        g_hDVIMonitorThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)DVIMonitorThread, 
                                    (LPVOID)NULL, 0, NULL);

        if (g_hDVIMonitorThread == NULL)
        {
            ERRORMSG(1,(TEXT("%s: CreateThread failed!\r\n"), __WFUNCTION__));
        }
        else
        {
            DEBUGMSG(1, (TEXT("%s: create DVI Monitor thread success\r\n"), __WFUNCTION__));
        }
    }
#endif

    rc = TRUE;
     
//cleanUp:
    if (!rc)
        HDMIDeinitPanel();

    DEBUGMSG(0,(TEXT("[DISPLAY] %s () -\r\n"), __WFUNCTION__));
}

//------------------------------------------------------------------------------
//
// Function: HDMIDeinitPanel
//
// This function closes down the HDMI module connection and releases all
// resources.
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void HDMIDeinitPanel()
{
#if 0
    // Release DVI bindings to IOMUX/GPIO
    DDKGpioSetConfig(DDK_GPIO_PORT3, DVI_DETECT, DDK_GPIO_DIR_IN, DDK_GPIO_INTR_NONE);
    DDKGpioClearIntrPin(DDK_GPIO_PORT3, DVI_DETECT);

    if (g_dwDVISysIntr != SYSINTR_UNDEFINED)
    {
        InterruptDone(g_dwDVISysIntr);
        InterruptDisable(g_dwDVISysIntr);
        KernelIoControl(IOCTL_HAL_RELEASE_SYSINTR, &g_dwDVISysIntr,
                        sizeof(g_dwDVISysIntr), NULL, 0, NULL);
        g_dwDVISysIntr = (DWORD)SYSINTR_UNDEFINED;
    }

    if (g_hDVIMonitorEvent != NULL)
    {
        CloseHandle(g_hDVIMonitorEvent);
        g_hDVIMonitorEvent = NULL;
    }
#endif    
}

//------------------------------------------------------------------------------
//
// Function: HDMIEnablePanel
//
// This function enables the connection to the HDMI module.
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void HDMIEnablePanel(PANEL_INFO *pPanelInfo)
{
    //pdmiConfigData pHDMIConf=NULL;
    //UINT8 CHNum=0;
    DDK_CLOCK_BAUD_SOURCE DIClkSrc = DDK_CLOCK_BAUD_SOURCE_PLL5_VIDEO;
    DDK_CLOCK_SIGNAL ClkSig = DDK_CLOCK_SIGNAL_PLL5_VIDEO;
    DWORD DIPodf=1, pdffrac=12;
    DWORD i=0;    
    DWORD  ClkSrcFreq=0; 
    pHDMIConfigData phdmiConf = (HDMIConfigData *)LocalAlloc(LPTR, sizeof(HDMIConfigData));
    
    memset(phdmiConf, 0, sizeof(HDMIConfigData));

    
    //RETAILMSG(TRUE,(_T("HDMIEnablePanelclock !\r\n")));
    RETAILMSG(DEBUG_HDMI,(_T("PIX_CLK= %d !\r\n"), pPanelInfo->PIX_CLK_FREQ));    
    
    
    //RETAILMSG(TRUE,(_T("Name=%s, PANELID=%d, PORTID=%d !\r\n"), pPanelInfo->NAME, pPanelInfo->PANELID, pPanelInfo->PORTID));  
    RETAILMSG(DEBUG_HDMI,(_T("WIDTH=%d, HEIGHT=%d, FREQUENCY=%d !\r\n"), pPanelInfo->WIDTH, pPanelInfo->HEIGHT, pPanelInfo->FREQUENCY));  

    if(pPanelInfo->PIXEL_FMT == IPU_PIX_FMT_YUV422)
    {
        phdmiConf->enc_in_format = eYCC422;
    }
    else
    {
        phdmiConf->enc_in_format = eRGB;
    }

    if(monitorInfo.PIX_FORMATE == eYCC444)
    {
         phdmiConf->enc_out_format = eYCC444;
    }
    else if (monitorInfo.PIX_FORMATE == eYCC422)
    {
       phdmiConf->enc_out_format = eYCC422;
    }
    else
    {
        phdmiConf->enc_out_format = eRGB;   //Default output formate
    }
    
    phdmiConf->enc_color_depth = 8;
    phdmiConf->colorimetry = eITU709;
    phdmiConf->pix_repet_factor = 0;
    phdmiConf->hdcp_enable = FALSE;
         
    
    phdmiConf->bHdmiEnable = TRUE;
    phdmiConf->video_mode.mRVBlankInOSC = FALSE;

    //phdmiConf->video_mode.bDataEnablePolarity = pPanelInfo->SYNC_SIG_POL.DATA_POL;
    
    phdmiConf->video_mode.bDataEnablePolarity = TRUE;
    
    
    phdmiConf->video_mode.mHActive = pPanelInfo->WIDTH; //1280;
    phdmiConf->video_mode.mVActive = pPanelInfo->HEIGHT; //720;
    phdmiConf->video_mode.mHBlanking = pPanelInfo->HSTARTWIDTH + pPanelInfo->HENDWIDTH +  pPanelInfo->HSYNCWIDTH; //370;
    phdmiConf->video_mode.mVBlanking =  pPanelInfo->VSTARTWIDTH + pPanelInfo->VENDWIDTH +  pPanelInfo->VSYNCWIDTH;  //30;
    phdmiConf->video_mode.mHSyncOffset = pPanelInfo->HSTARTWIDTH;  //110;
    phdmiConf->video_mode.mVSyncOffset = pPanelInfo->VSTARTWIDTH; //5;
    phdmiConf->video_mode.mHSyncPulseWidth = pPanelInfo->HSYNCWIDTH;  //40;
    phdmiConf->video_mode.mVSyncPulseWidth = pPanelInfo->VSYNCWIDTH;  //5;
    phdmiConf->video_mode.bHSyncPolarity =  pPanelInfo->SYNC_SIG_POL.HSYNC_POL;
    phdmiConf->video_mode.bVSyncPolarity =  pPanelInfo->SYNC_SIG_POL.VSYNC_POL;
    //phdmiConf->video_mode.mHSyncPolarity =  TRUE;
    //phdmiConf->video_mode.mVSyncPolarity =  TRUE;     
    phdmiConf->video_mode.bInterlaced = pPanelInfo->ISINTERLACE; //FALSE;
    phdmiConf->video_mode.mPixelClock = pPanelInfo->PIX_CLK_FREQ / 10000 ; // 7417 : 7425;

    //        
    HDMIConfigure(phdmiConf);

  
    if(pPanelInfo->DICLK_SOURCE == DISPLAY_CLOCK_EXTERNAL)
    {
        
        for(i = 8; i >= 1; i--)
        {
            ClkSrcFreq = pPanelInfo->PIX_CLK_FREQ * i;
            
            if((ClkSrcFreq > 650000000) && (ClkSrcFreq < 1300000000))
            {
               ClkSig = DDK_CLOCK_SIGNAL_PLL5_VIDEO;
               DIClkSrc = DDK_CLOCK_BAUD_SOURCE_PLL5_VIDEO;
               DIPodf = i;   
               RETAILMSG(DEBUG_HDMI, (TEXT("DI Clock source from PLL5 ClkDiv=%d, ClkSrcFreq=%d\r\n "),DIPodf, ClkSrcFreq)) ;           
               break;
            }
            else  if(ClkSrcFreq > 246000000)
            {
                //Clock source from  480M_PFD1_540M               
                ClkSig = DDK_CLOCK_SIGNAL_480M_PFD1_540M;
                DIClkSrc = DDK_CLOCK_BAUD_SOURCE_480M_PFD1_540M;
                DIPodf = i;  
                RETAILMSG(DEBUG_HDMI, (TEXT("DI Clock source from 480M_PFD1_540M ClkDiv=%d, ClkSrcFreq=%d\r\n "),DIPodf, ClkSrcFreq)) ;             
                break;              
            }   

            if( i == 0 )
            {
                 ERRORMSG(1, (TEXT("PixClock Out of PLL5 and PFD540M capability PixClk=%d\r\n "),pPanelInfo->PIX_CLK_FREQ)) ;
                 return;
            }                    
        }
            

        if(DIClkSrc == DDK_CLOCK_BAUD_SOURCE_480M_PFD1_540M)
        {
            DWORD j = 0, k = 0;
            DWORD deltaFreq = pPanelInfo->PIX_CLK_FREQ, targetFreq;
            DWORD DeltaTmp = 0;
            
            for(j = 12; j <= 35; j ++)
            {
                for(k = 1; k <= 8; k ++) 
                {
                   targetFreq = 480000 * 18 / ( j * k) * 1000;
                   //RETAILMSG(1, (TEXT("dtargetFreq=%d\r\n"),targetFreq));
        
                   DeltaTmp = (pPanelInfo->PIX_CLK_FREQ >= targetFreq) ? (pPanelInfo->PIX_CLK_FREQ - targetFreq) : (targetFreq - pPanelInfo->PIX_CLK_FREQ);
    
                   if(deltaFreq > DeltaTmp)  
                   {
                       deltaFreq = DeltaTmp;
                       DIPodf = k;  
                       pdffrac = j;
                       //RETAILMSG(1, (TEXT("deltaFreq=%d, DIPodf=%d, pdffrac=%d\r\n"),deltaFreq, DIPodf, pdffrac));
                   }
                }
            }
                
            ClkSrcFreq = pPanelInfo->PIX_CLK_FREQ * DIPodf;         
            RETAILMSG(DEBUG_HDMI, (TEXT("DIPodf=%d, pdffrac=%d, ClkSrcFreq=%d\r\n"),DIPodf, pdffrac, ClkSrcFreq ));
        }
        
    
        if(pPanelInfo->DI_NUM == DI_SELECT_DI0)
        { 
           RETAILMSG(DEBUG_HDMI, (TEXT("config DI0 clock ClkSig=%d, ClkSrcFreq=%d \r\n"), ClkSig,ClkSrcFreq));
            DDKClockSetFreq(ClkSig, ClkSrcFreq);
            DDKClockConfigBaud(DDK_CLOCK_SIGNAL_IPU1_DI0, DIClkSrc, 0, 0, DIPodf-1);         
        }
        else
        {
           RETAILMSG(DEBUG_HDMI, (TEXT("config DI1 clock ClkSig=%d, ClkSrcFreq=%d \r\n"), ClkSig,ClkSrcFreq));        
            DDKClockSetFreq(ClkSig, ClkSrcFreq);
            DDKClockConfigBaud(DDK_CLOCK_SIGNAL_IPU1_DI1, DIClkSrc, 0, 0, DIPodf-1);             
        }    
    }

    if(pPanelInfo->DI_NUM == DI_SELECT_DI0)
    {    
        RETAILMSG(DEBUG_HDMI, (TEXT("HDMI Enable DI0 clock \r\n")));
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU_DI0, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU, DDK_CLOCK_GATE_MODE_ENABLED_ALL);            
    }
    else
    {
        RETAILMSG(DEBUG_HDMI, (TEXT("HDMI Enable DI1 clock \r\n")));    
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU_DI1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU, DDK_CLOCK_GATE_MODE_ENABLED_ALL);              
    }

}
//------------------------------------------------------------------------------
//
// Function: DVIDisablePanel
//
// This function disables the HDMI connection.
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void HDMIDisablePanel(PANEL_INFO *pPanelInfo)
{

    if(pPanelInfo->DI_NUM == DI_SELECT_DI0)
    {
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU_DI0, DDK_CLOCK_GATE_MODE_DISABLED);
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU, DDK_CLOCK_GATE_MODE_DISABLED);         
    }
    else
    {

        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU_DI1, DDK_CLOCK_GATE_MODE_DISABLED); 
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU, DDK_CLOCK_GATE_MODE_DISABLED);          
    } 
}


//------------------------------------------------------------------------------
//
// Function: HDMIMonitorThread
//
// This function is the main thread for monitoring the HDMI module to detect
// connection to a new display.
//
// Parameters:
//      lpParameter
//          [in] NULL.
//
// Returns:
//      None
//
//------------------------------------------------------------------------------
void HDMIMonitorThread(LPVOID lpParameter)
{
    UNREFERENCED_PARAMETER(lpParameter);

    // Main loop waits for GPIO interrupt indicating new monitor
    for (;;)
    {
        //WaitForSingleObject(g_hHDMIMonitorEvent, INFINITE);
        //DDKGpioClearIntrPin(DDK_GPIO_PORT3, HDMI_DETECT);
        //InterruptDone(g_dwHDMISysIntr);
    }
}


//------------------------------------------------------------------------------
//
// Function: HDMIGetMonitorInfo
//
// This function retrieve EDID monitor info from the monitor, and filters
// it into useful info for the display driver.
//
// Parameters:
//      monitorInfo
//          [out] Structure holding monitor info.
//
// Returns:
//      TRUE if EDID read operation was successful
//      FALSE if EDID read operation failed
//
//------------------------------------------------------------------------------
BOOL HDMIGetMonitorInfo(HDMIMonitorInfo *pmonitorInfo)
{
    UNREFERENCED_PARAMETER(pmonitorInfo);
    BOOL retVal = TRUE;
    DWORD dwFrequency;
    BYTE bySlaveAddr;
    BYTE EDIDdata[256];

    BYTE establishedTimings1, establishedTimings2;

    dwFrequency = HDMI_I2C_SPEED;
    bySlaveAddr = HDMI_I2C_ADDRESS;

    memset(EDIDdata, 0, 256);

    // Open handle to I2C port and set address and frequency
    if(g_hI2C == NULL)
    {
        g_hI2C = I2COpenHandle(I2C2_FID);   
        if (g_hI2C == INVALID_HANDLE_VALUE)
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: CreateFile for I2C failed!\r\n"), __WFUNCTION__));
        }

        if (!I2CSetMasterMode(g_hI2C))
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: Set I2C Master mode failed!\r\n"), __WFUNCTION__));
        }

        // Initialize the device internal fields
        if (!I2CSetFrequency(g_hI2C, dwFrequency))
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: Set HSI2C frequency failed!\r\n"), __WFUNCTION__));
        }
    }

    INT iResult;

    // Read EDID info
    if (!I2CReadNBytes(g_hI2C, bySlaveAddr, 0, EDIDdata, 128, &iResult))
    {
        retVal = FALSE;
    }

    if(EDIDdata[126] != 0)
    {
        if (!I2CReadNBytes(g_hI2C, bySlaveAddr, 128, EDIDdata+128, 128, &iResult))
        {
            retVal = FALSE;
        }
    }

    // Dump EDID data
    for (int i = 0; i < 256; i++)
    {
        RETAILMSG(0,(TEXT("EDID[%d]: 0x%x \r\n"), i, EDIDdata[i]));
    }

    //Monitor support pix formate
    if(((EDIDdata[24] & 0x18) >> 3) == 2)
    {
        if(EDIDdata[131] & 0x20)
        {
            pmonitorInfo->PIX_FORMATE = eYCC444;            
        }
        else if(EDIDdata[131] & 0x10)
        {
            pmonitorInfo->PIX_FORMATE = eYCC422;            
        }            
    }
    else 
    {
        pmonitorInfo->PIX_FORMATE = eRGB;
    }
    
    pmonitorInfo->PIX_CLK_FREQ = ((EDIDdata[55] << 8) | EDIDdata[54]) * 10000; 

    if(pmonitorInfo->PIX_CLK_FREQ != 0)
    {
        pmonitorInfo->HEIGHT = EDIDdata[59] | ((EDIDdata[61] & 0XF0) << 4);
        pmonitorInfo->WIDTH = EDIDdata[56] | ((EDIDdata[58] & 0XF0) << 4);
        
        RETAILMSG(DEBUG_HDMI,(TEXT("Display device MAX support resolution is  %dX%d\r\n"), pmonitorInfo->WIDTH, pmonitorInfo->HEIGHT));
    }
    establishedTimings1 = EDIDdata[35];
    establishedTimings2 = EDIDdata[36];
    pmonitorInfo->iTimingsMask = establishedTimings2 | (establishedTimings1 << 8);

    return retVal;
}
