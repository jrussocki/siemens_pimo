//------------------------------------------------------------------------------
//
//  Copyright (C) 2009-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  lvds.cpp
//
//  Provides routines for enabling, disabling, and initializing
//  communication through the LVDS display interface.
//
//------------------------------------------------------------------------------

#pragma warning(push)
#pragma warning(disable: 4115 4201 4204 4214)
#include <windows.h>

#include <Devload.h>
#include <ceddk.h>
#include <cmnintrin.h>
#pragma warning(pop)

#include "Ipu_common.h"

#include "bsp.h"
#include "lvds.h"
#include "bspdisplay.h"
#include "mx6_ldb.h"

#include "i2cbus.h"

#define DEBUG_LVDS									0

//GPR3 defines
#define LVDS1_MUX_CTL_WID 2
#define LVDS1_MUX_CTL_LSH 8
#define LVDS1_MUX_CTL_IPU1_DI0 0
#define LVDS1_MUX_CTL_IPU1_DI1 1
#define LVDS1_MUX_CTL_IPU2_DI0 2
#define LVDS1_MUX_CTL_IPU2_DI1 3

#define LVDS0_MUX_CTL_WID 2
#define LVDS0_MUX_CTL_LSH 6
#define LVDS0_MUX_CTL_IPU1_DI0 0
#define LVDS0_MUX_CTL_IPU1_DI1 1
#define LVDS0_MUX_CTL_IPU2_DI0 2
#define LVDS0_MUX_CTL_IPU2_DI1 3

//------------------------------------------------------------------------------
// External Functions
extern BOOL I2CReadNBytes(HANDLE hI2C, BYTE byAddr, BYTE byReg, BYTE *pbyReadBuf, UINT32 nBytesToRead, LPINT lpiResult);
extern BYTE I2CReadOneByte(HANDLE hI2C, BYTE byAddr, BYTE byReg, LPINT lpiResult);
extern VOID I2CWriteNBytes(HANDLE hI2C, BYTE byAddr, BYTE byReg, BYTE *pbyWriteBuf, UINT32 nBytesToWrite, LPINT lpiResult);


//------------------------------------------------------------------------------
// External Variables


//------------------------------------------------------------------------------
// Defines
#define LVDS_DIVIDER	   7
#define LVDS_I2C_ADDRESS        0x50
#define LVDS_I2C_SPEED          30000

//------------------------------------------------------------------------------
// Types
// Monitor descriptor info from EDID
typedef struct {
    UINT32 Width;
    UINT32 Height;
    UINT16 iTimingsMask;
} LVDSMonitorInfo, *PLVDSMonitorInfo;


//------------------------------------------------------------------------------
// Global Variables
extern DWORD g_dwBoardID; //Already defined in bspdisplay.cpp

BOOL LVDSGetMonitorInfo(LVDSMonitorInfo *monitorInfo);

//------------------------------------------------------------------------------
// Local Variables
static HANDLE g_hI2C = NULL;


//------------------------------------------------------------------------------
// Local Functions
void LVDSDeinitPanel();


//------------------------------------------------------------------------------
//
// Function: LVDSInitializePanel
//
// This function initializes the LVDS display interface.
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void LVDSInitializePanel(PANEL_INFO *pPanelInfo)
{
    DEBUGMSG(0,(TEXT("[DISPLAY] %s () +\r\n"), __WFUNCTION__));

	if(pPanelInfo->DI_NUM == DI_SELECT_DI1)
    {
		DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU_DI1, DDK_CLOCK_GATE_MODE_DISABLED);
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_LDB_DI1, DDK_CLOCK_GATE_MODE_DISABLED);
	}
	else
	{
		DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU_DI0, DDK_CLOCK_GATE_MODE_DISABLED);
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_LDB_DI0, DDK_CLOCK_GATE_MODE_DISABLED);
	}

    
	if(g_dwBoardID == 0)
    {
        if (!KernelIoControl(IOCTL_HAL_QUERY_BOARD_ID, NULL, 0,
             &g_dwBoardID, sizeof(g_dwBoardID), NULL))
        {
            ERRORMSG(TRUE, (TEXT("%s:Cannot obtain the board ID!\r\n"),__WFUNCTION__));
        }
        else
        {
            g_dwBoardID = BSP_CPU_BID(g_dwBoardID);
        }
    }
    
    DEBUGMSG(0,(TEXT("[DISPLAY] %s () -\r\n"), __WFUNCTION__));
}

//------------------------------------------------------------------------------
//
// Function: LVDSDeinitPanel
//
// This function closes down the LVDS module connection and releases all
// resources.
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void LVDSDeinitPanel()
{

}
//------------------------------------------------------------------------------
//
// Function: LVDSEnablePanelPower
//
// This function enables the power of LVDS panel.
//
// Parameters:
//      [in]enable.
//              True if enable, false for disable
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void LVDSEnablePanelPower(BOOL enable)
{
}
//------------------------------------------------------------------------------
//
// Function: LVDSEnablePanel
//
// This function enables the connection to the LVDS module.
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void LVDSEnablePanel(PANEL_INFO *pPanelInfo)
{
    lvdsConfigData LVDSConf;
    UINT8 CHNum = 0;
	DWORD  ClkSrcFreq=0;
	UINT32 ConfData;
	DDK_CLOCK_GATE_MODE mode_di0;
	DDK_CLOCK_GATE_MODE mode_di1;
	UINT8 MMDC_CH1_Divider = 0;

 	// pPanelInfo->PIX_CLK_FREQ is the LVDS frequency from the registry
 	// if frequency is below 40MHz we use a predivider /2
 	if ( pPanelInfo->PIX_CLK_FREQ < 40000000 ) {
 		MMDC_CH1_Divider = 1;
 		ClkSrcFreq = pPanelInfo->PIX_CLK_FREQ * LVDS_DIVIDER * 2;
 	}
 	else {
 		MMDC_CH1_Divider = 0;
 		ClkSrcFreq = pPanelInfo->PIX_CLK_FREQ * LVDS_DIVIDER;
 	}

	ConfData = DDKIomuxGetGpr(3); //IOMUXC_GPR3

	//Get the LVDS frequency from pannel.h
	ClkSrcFreq = pPanelInfo->PIX_CLK_FREQ * LVDS_DIVIDER;
	//Check if one of the DI is already set
	DDKClockGetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU_DI0, &mode_di0);
	DDKClockGetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU_DI1, &mode_di1);
	if ((DDK_CLOCK_GATE_MODE_ENABLED_ALL != mode_di0) && (DDK_CLOCK_GATE_MODE_ENABLED_ALL != mode_di1))
	{
		RETAILMSG(DEBUG_LVDS,(_T("Set ClkSrcFreq : %d\r\n"), ClkSrcFreq));
		DDKClockSetFreq(DDK_CLOCK_SIGNAL_528M_PFD0_352M, ClkSrcFreq);
	}
	else
	{
		RETAILMSG(DEBUG_LVDS,(_T("DDKClockGetGatingMode mode_di0 : %d\r\n"), mode_di0));
		RETAILMSG(DEBUG_LVDS,(_T("DDKClockGetGatingMode mode_di1 : %d\r\n"), mode_di1));
	}
	
    if(pPanelInfo->PORTID == DISPLAY_PORT_DLVDS)
    {
        CHNum = LVDS_CH0|LVDS_CH1;
    }
    else if(pPanelInfo->PORTID == DISPLAY_PORT_LVDS1)
    {
        CHNum = LVDS_CH0;
    }
    else if(pPanelInfo->PORTID == DISPLAY_PORT_LVDS2)
    {
        CHNum = LVDS_CH1;
    }

    // IOMUXing already done for backlight using PWM 4 - no need to repeat IOMUXing here

	RETAILMSG(DEBUG_LVDS,(_T("LVDS_CH%d routed to DI%d\r\n"),CHNum-1,pPanelInfo->DI_NUM));
    if(pPanelInfo->DI_NUM == DI_SELECT_DI1)
    {

        LVDSConf.RouteMode = LVDS_ROUTE_DI1;

        DDKClockConfigBaud(DDK_CLOCK_SIGNAL_LDB_DI1_SERIAL, DDK_CLOCK_BAUD_SOURCE_528M_PFD0_352M, 0, 0, 0);
        if(CHNum == (LVDS_CH0|LVDS_CH1) ||
            (LVDSChannelRoute((~CHNum)&(LVDS_CH0|LVDS_CH1)) == LVDS_ROUTE_DI1))
        {
            DDKClockConfigBaud(DDK_CLOCK_SIGNAL_LDB_DI1_IPU, DDK_CLOCK_BAUD_SOURCE_LDB_DI1_SERIAL, 0, 0, 0);         
            DDKClockConfigBaud(DDK_CLOCK_SIGNAL_IPU1_DI1, DDK_CLOCK_BAUD_SOURCE_LDB_DI1_IPU, 0, 0, 0);
        }
        else
        {
            DDKClockConfigBaud(DDK_CLOCK_SIGNAL_LDB_DI1_IPU, DDK_CLOCK_BAUD_SOURCE_LDB_DI1_SERIAL, 0, 0, 1);        
            DDKClockConfigBaud(DDK_CLOCK_SIGNAL_IPU1_DI1, DDK_CLOCK_BAUD_SOURCE_LDB_DI1_IPU, 0, 0, 0);
        }
        
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU_DI1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_LDB_DI1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
    }
    else
    {
        LVDSConf.RouteMode = LVDS_ROUTE_DI0;

        DDKClockConfigBaud(DDK_CLOCK_SIGNAL_LDB_DI0_SERIAL, DDK_CLOCK_BAUD_SOURCE_528M_PFD0_352M, 0, 0, 0);
        if(CHNum == (LVDS_CH0|LVDS_CH1) ||
            (LVDSChannelRoute((~CHNum)&(LVDS_CH0|LVDS_CH1)) == LVDS_ROUTE_DI0))
        {
            DDKClockConfigBaud(DDK_CLOCK_SIGNAL_LDB_DI0_IPU, DDK_CLOCK_BAUD_SOURCE_LDB_DI0_SERIAL, 0, 0, 0);
            DDKClockConfigBaud(DDK_CLOCK_SIGNAL_IPU1_DI0, DDK_CLOCK_BAUD_SOURCE_LDB_DI0_IPU, 0, 0, 0);
        }
        else
        {
            DDKClockConfigBaud(DDK_CLOCK_SIGNAL_LDB_DI0_IPU, DDK_CLOCK_BAUD_SOURCE_LDB_DI0_SERIAL, 0, 0, 1);        
            DDKClockConfigBaud(DDK_CLOCK_SIGNAL_IPU1_DI0, DDK_CLOCK_BAUD_SOURCE_LDB_DI0_IPU, 0, 0, 0);
        }
        
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU_DI0, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_LDB_DI0, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
    }
    
    if(pPanelInfo->PIXEL_FMT == IPU_PIX_FMT_RGB24)
        LVDSConf.DataWidth = LVDS_DW_24BIT;
    else
        LVDSConf.DataWidth = LVDS_DW_18BIT;

    LVDSConf.MappingType = LVDS_MAP_SPWG;  //All current supported panels use SPWG mode.
    LVDSConf.VsyncPolarity = LVDS_VSPOL_LOW; //Always should be low.

    INSREG32BF(&ConfData,LVDS0_MUX_CTL,LVDS0_MUX_CTL_IPU1_DI0);

	INSREG32BF(&ConfData,LVDS1_MUX_CTL,LVDS1_MUX_CTL_IPU1_DI1);

    LVDSConfigure(&LVDSConf,CHNum);
	RETAILMSG(DEBUG_LVDS,(_T(" GPR3 := [0x%x] .\r\n"),ConfData));
	DDKIomuxSetGpr(3, ConfData);

    LVDSEnablePanelPower(TRUE);

}


//------------------------------------------------------------------------------
//
// Function: LVDSDisablePanel
//
// This function disables the LVDS connection.
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void LVDSDisablePanel(PANEL_INFO *pPanelInfo)
{
    lvdsConfigData LVDSConf;
    UINT8 CHNum = 0;

    if(pPanelInfo->PORTID == DISPLAY_PORT_DLVDS)
    {
        CHNum = LVDS_CH0|LVDS_CH1;
    }
    else if(pPanelInfo->PORTID == DISPLAY_PORT_LVDS1)
    {
        CHNum = LVDS_CH0;
    }
    else if(pPanelInfo->PORTID == DISPLAY_PORT_LVDS2)
    {
        CHNum = LVDS_CH1;
    }

    // Disable backlight
    DDKGpioWriteDataPin(BSP_DISP_PWM_GPIO, BSP_DISP_PWM_GPIO_PIN, 0);        

    LVDSConf.RouteMode = LVDS_ROUTE_DISABLE;
    if(pPanelInfo->DI_NUM == DI_SELECT_DI1)
    {
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU_DI1, DDK_CLOCK_GATE_MODE_DISABLED);
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_LDB_DI1, DDK_CLOCK_GATE_MODE_DISABLED);
    }
    else
    {
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU_DI0, DDK_CLOCK_GATE_MODE_DISABLED);
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_LDB_DI0, DDK_CLOCK_GATE_MODE_DISABLED);
    }
    if(pPanelInfo->PIXEL_FMT == IPU_PIX_FMT_RGB24)
        LVDSConf.DataWidth = LVDS_DW_24BIT;
    else
        LVDSConf.DataWidth = LVDS_DW_18BIT;

    LVDSConf.MappingType = LVDS_MAP_SPWG;  //All current supported panels use SPWG mode.
    LVDSConf.VsyncPolarity = LVDS_VSPOL_LOW; //Always should be low.
    LVDSConfigure(&LVDSConf,CHNum);

    LVDSEnablePanelPower(FALSE);
}


//------------------------------------------------------------------------------
//
// Function: LVDSGetMonitorInfo
//
// This function retrieve EDID monitor info from the monitor, and filters
// it into useful info for the display driver.
//
// Parameters:
//      monitorInfo
//          [out] Structure holding monitor info.
//
// Returns:
//      TRUE if EDID read operation was successful
//      FALSE if EDID read operation failed
//
//------------------------------------------------------------------------------
BOOL LVDSGetMonitorInfo(LVDSMonitorInfo *monitorInfo)
{
    UNREFERENCED_PARAMETER(monitorInfo);
    BOOL retVal = TRUE;
    DWORD dwFrequency;
    BYTE bySlaveAddr;
    BYTE EDIDdata[128];

    BYTE establishedTimings1, establishedTimings2;

    dwFrequency = LVDS_I2C_SPEED;
    bySlaveAddr = LVDS_I2C_ADDRESS;

    memset(EDIDdata, 0, 128);

    // Open handle to I2C port and set address and frequency
    if(g_hI2C == NULL)
    {
        g_hI2C = I2COpenHandle(I2C2_FID);   
        if (g_hI2C == INVALID_HANDLE_VALUE)
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: CreateFile for I2C failed!\r\n"), __WFUNCTION__));
        }

        if (!I2CSetMasterMode(g_hI2C))
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: Set I2C Master mode failed!\r\n"), __WFUNCTION__));
        }

        // Initialize the device internal fields
        if (!I2CSetFrequency(g_hI2C, dwFrequency))
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: Set HSI2C frequency failed!\r\n"), __WFUNCTION__));
        }
    }

    INT iResult;

    // Read EDID info
    if (!I2CReadNBytes(g_hI2C, bySlaveAddr, 0, EDIDdata, 128, &iResult))
    {
        retVal = FALSE;
    }

    // Dump EDID data
    for (int i = 0; i < 128; i++)
    {
        RETAILMSG(DEBUG_LVDS,(TEXT("EDID[%d]: 0x%x \r\n"), i, EDIDdata[i]));
    }

    monitorInfo->Height = EDIDdata[21];
    monitorInfo->Width = EDIDdata[22];

    establishedTimings1 = EDIDdata[35];
    establishedTimings2 = EDIDdata[36];
    monitorInfo->iTimingsMask = establishedTimings2 | (establishedTimings1 << 8);

    return retVal;
}
