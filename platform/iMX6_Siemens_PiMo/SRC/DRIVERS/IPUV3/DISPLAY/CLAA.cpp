//------------------------------------------------------------------------------
//
//  Copyright (C) 2009-2010, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  claa.cpp
//
//  Provides routines for enabling, disabling, and initializing
//  the CLAA070VC01 dumb panel.
//
//------------------------------------------------------------------------------

#pragma warning(push)
#pragma warning(disable: 4115 4201 4204 4214)
#include <windows.h>

#include <Devload.h>
#include <ceddk.h>
#include <cmnintrin.h>
#include <NKIntr.h>
#pragma warning(pop)

#include "bsp.h"
#include "claa.h"

//------------------------------------------------------------------------------
// External Functions


//------------------------------------------------------------------------------
// External Variables
extern DWORD g_dwBoardID;

//------------------------------------------------------------------------------
// Defines

//------------------------------------------------------------------------------
// Types


//------------------------------------------------------------------------------
// Global Variables

//------------------------------------------------------------------------------
// Local Variables


//------------------------------------------------------------------------------
// Local Functions


//------------------------------------------------------------------------------
//
// Function: CLAAInitializePanel
//
// This function initializes the CLAA dumb panel.
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void CLAAInitializePanel()
{

}

//------------------------------------------------------------------------------
//
// Function: GiantplusEnablePanel
//
// This function turns on the CLAA dumb panel.
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void CLAAEnablePanel()
{

}


//------------------------------------------------------------------------------
//
// Function: GiantplusDisablePanel
//
// This function turns off the CLAA dumb panel.
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void CLAADisablePanel()
{

}
