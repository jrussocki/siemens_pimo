//------------------------------------------------------------------------------
//
//  Copyright (C) 2008-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  dvi.h
//
//  Provides routines for enabling, disabling, and initializing
//  communication with the DVI interface module.
//
//------------------------------------------------------------------------------

#ifndef __DVI_H__
#define __DVI_H__

#ifdef __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------
// Defines

#define DVI_PWR_GPIO_PIN    24
#define DVI_RST_GPIO_PIN    0
#define DVI_DETECT          31
#define DVI_I2C_ENABLE      28

#define ESTABLISHED_TIMING_720x400_70Hz   1 << 15
#define ESTABLISHED_TIMING_720x400_88Hz   1 << 14
#define ESTABLISHED_TIMING_640x480_60Hz   1 << 13
#define ESTABLISHED_TIMING_640x480_70Hz   1 << 12
#define ESTABLISHED_TIMING_640x480_72Hz   1 << 11
#define ESTABLISHED_TIMING_640x480_75Hz   1 << 10
#define ESTABLISHED_TIMING_800x600_56Hz   1 << 9
#define ESTABLISHED_TIMING_800x600_60Hz   1 << 8
#define ESTABLISHED_TIMING_800x600_72Hz   1 << 7
#define ESTABLISHED_TIMING_800x600_75Hz   1 << 6
#define ESTABLISHED_TIMING_832x624_75Hz   1 << 5
#define ESTABLISHED_TIMING_1024x768_87Hz  1 << 4
#define ESTABLISHED_TIMING_1024x768_60Hz  1 << 3
#define ESTABLISHED_TIMING_1024x768_70Hz  1 << 2
#define ESTABLISHED_TIMING_1024x768_75Hz  1 << 1
#define ESTABLISHED_TIMING_1280x1024_75Hz 1 << 0

//------------------------------------------------------------------------------
// Types


// Monitor descriptor info from EDID
typedef struct {
    UINT32 Width;
    UINT32 Height;
    UINT16 iTimingsMask;
} DVIMonitorInfo, *PDVIMonitorInfo;


//------------------------------------------------------------------------------
// Functions

void DVIInitializePanel(PANEL_INFO *pPanelInfo);
void DVIEnablePanel(PANEL_INFO *pPanelInfo);
void DVIDisablePanel(VOID);
BOOL DVIGetMonitorInfo(PDVIMonitorInfo);

#ifdef __cplusplus
}
#endif

#endif // __DVI_H__

