//------------------------------------------------------------------------------
//
//  Copyright (C) 2008-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  hdmi.h
//
//  Provides routines for enabling, disabling, and initializing
//  communication with the HDMI interface module.
//
//------------------------------------------------------------------------------

#ifndef __HDMI_H__
#define __HDMII_H__

#ifdef __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------
// Defines


//------------------------------------------------------------------------------
// Types


// Monitor descriptor info from EDID
typedef struct {
    UINT32 WIDTH;                     // Screen width in pixels
    UINT32 HEIGHT;                    // Screen height in pixels
    UINT32 PIX_CLK_FREQ;              // Pixel clock frequency, in Hz    
    UINT32 PIX_FORMATE;
    UINT16 iTimingsMask;
} HDMIMonitorInfo, *PHDMIMonitorInfo;


//------------------------------------------------------------------------------
// Functions

void HDMIInitializePanel(PANEL_INFO *pPanelInfo);
void HDMIEnablePanel(PANEL_INFO *pPanelInfo);
void HDMIDisablePanel(PANEL_INFO *pPanelInfo);
BOOL HDMIGetMonitorInfo(PHDMIMonitorInfo);

#ifdef __cplusplus
}
#endif

#endif // __HDMI_H__

