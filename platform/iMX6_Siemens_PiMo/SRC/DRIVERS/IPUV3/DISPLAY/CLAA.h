//------------------------------------------------------------------------------
//
//  Copyright (C) 2008-2010, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  claa.h
//
//  Provides routines for enabling, disabling, and initializing
//  the CLAA070VC01 dumb panel.
//
//------------------------------------------------------------------------------

#ifndef __CLAA_H__
#define __CLAA_H__

#ifdef __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------
// Defines
#define LCD_CONTRAST_GPIO_PIN 20
#define LCD_PWR_EN_GPIO_PIN  24

//------------------------------------------------------------------------------
// Types

//------------------------------------------------------------------------------
// Functions

void CLAAInitializePanel(VOID);
void CLAAEnablePanel(VOID);
void CLAADisablePanel(VOID);


#ifdef __cplusplus
}
#endif

#endif // __CLAA_H__
