//------------------------------------------------------------------------------
//
//  Copyright (C) 2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//-----------------------------------------------------------------------------
//
//  File: i2cutils.cpp
//
//  This file contains the ipu I2c access code.
//
//-----------------------------------------------------------------------------
#include <bsp.h>

#include "i2cbus.h"

//-----------------------------------------------------------------------------
// External Functions


//-----------------------------------------------------------------------------
// External Variables


//-----------------------------------------------------------------------------
// Defines



//-----------------------------------------------------------------------------
// Types


//-----------------------------------------------------------------------------
// Global Variables


//-----------------------------------------------------------------------------
// Local Variables


//-----------------------------------------------------------------------------
// Functions



//-----------------------------------------------------------------------------
//
// Function: I2CReadNBytes
//
// This function reads N bytes of data from the register stated in byReg.
//
// Parameters:
//      hI2C
//          [in] File handle to I2C Bus Interface.
//
//      byAddr
//          [in] I2C Slave device address.
//
//      byReg
//          [in] Register Index.
//
//      pbyReadBuf
//          [in/out] Buffer to hold data read from slave device.  Validity of buffer
//          must be managed by caller process.
//
//      nBytesToRead
//          [in] Number of bytes to read from slave device
//
//      lpiResult
//          [in] Pointer of the result. The I2C Bus will store the
//            result of the operation in location pointed to by
//            lpiResult.
//
// Returns:
//      TRUE if successful, FALSE if failure.
//
//-----------------------------------------------------------------------------
BOOL I2CReadNBytes(HANDLE hI2C, BYTE byAddr, BYTE byReg, BYTE *pbyReadBuf, UINT32 nBytesToRead, LPINT lpiResult)
{
    I2C_TRANSFER_BLOCK I2CXferBlock;
    I2C_PACKET I2CPacket[2];
    BYTE byOutData;

    byOutData = byReg;

    I2CPacket[0].pbyBuf = (PBYTE) &byOutData;
    I2CPacket[0].wLen = sizeof(byOutData);

    I2CPacket[0].byRW = I2C_RW_WRITE;
    I2CPacket[0].byAddr = byAddr;
    I2CPacket[0].lpiResult = lpiResult;

    I2CXferBlock.pI2CPackets = &I2CPacket[0];
    I2CXferBlock.iNumPackets = 1;

    if(!I2CTransfer(hI2C, &I2CXferBlock))
    {
        ERRORMSG(TRUE,(TEXT("%s:I2C Write to HDMI fail! - ERRORCode:%d \r\n"),
                            __WFUNCTION__,*lpiResult));
        return FALSE;
    }

    // at least 30ms between I2C packets.  Turns out that 35ms works best.
    Sleep(35);

    I2CPacket[1].pbyBuf = pbyReadBuf;
    I2CPacket[1].wLen = (WORD)nBytesToRead;

    I2CPacket[1].byRW = I2C_RW_READ;
    I2CPacket[1].byAddr = byAddr;
    I2CPacket[1].lpiResult = lpiResult;

    I2CXferBlock.pI2CPackets = &I2CPacket[1];
    I2CXferBlock.iNumPackets = 1;

    if(!I2CTransfer(hI2C, &I2CXferBlock))
    {
        ERRORMSG(TRUE,(TEXT("%s:I2C Read EDID from HDMI fail! - ERRORCode:%d \r\n"),
                            __WFUNCTION__,*lpiResult));
        return FALSE;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function: I2CWriteNBytes
//
// This function writes N bytes of data to the register stated in byReg.
//
// Parameters:
//      hI2C
//          [in] File handle to I2C Bus Interface.
//
//      byAddr
//          [in] I2C Slave device address.
//
//      byReg
//          [in] Register Index.
//
//      pbyWriteBuf
//          [in/out] Buffer to hold data read from slave device.  Validity of buffer
//          must be managed by caller process.
//
//      nBytesToWrite
//          [in] Number of bytes to read from slave device
//
//      lpiResult
//          [in] Pointer of the result. The I2C Bus will store the
//            result of the operation in location pointed to by
//            lpiResult.
//
// Returns:
//      None.
//
//-----------------------------------------------------------------------------
VOID I2CWriteNBytes(HANDLE hI2C, BYTE byAddr, BYTE byReg, BYTE *pbyWriteBuf, UINT32 nBytesToWrite, LPINT lpiResult)
{
    I2C_TRANSFER_BLOCK I2CXferBlock;
    I2C_PACKET I2CPacket;
    PBYTE pbyOutData;

    
    pbyOutData = (PBYTE)malloc(nBytesToWrite+1);
    if (pbyOutData)
    {
        pbyOutData[0] = byReg;
    }
    else
    {
        return;
    }
    memcpy(pbyOutData+1, pbyWriteBuf, nBytesToWrite);
    I2CPacket.wLen = (WORD)nBytesToWrite+1;
    I2CPacket.pbyBuf = pbyOutData;
    I2CPacket.byRW = I2C_RW_WRITE;
    I2CPacket.byAddr = byAddr;
    I2CPacket.lpiResult = lpiResult;
    I2CXferBlock.pI2CPackets = &I2CPacket;
    I2CXferBlock.iNumPackets = 1;

    if (!I2CTransfer(hI2C, &I2CXferBlock))
        ERRORMSG(TRUE,(TEXT("%s:I2C Write HDMI register fail! - ERRORCode:%d \r\n"),__WFUNCTION__,*lpiResult));
    free(pbyOutData);
    return;
}



//-----------------------------------------------------------------------------
//
// Function: I2CReadOneByte
//
// This function read a single byte data from the register stated in byReg.
//
// Parameters:
//      hI2C
//          [in] File handle to I2C Bus Interface.
//
//      byAddr
//          [in] I2C Slave device address.
//
//      byReg
//          [in] Register Index.
//
//      lpiResult
//          [in] Pointer of the result. The I2C Bus will store the
//            result of the operation in location pointed to by
//            lpiResult.
//
// Returns:
//      The single byte content stored in byReg.
//
//-----------------------------------------------------------------------------
BYTE I2CReadOneByte(HANDLE hI2C, BYTE byAddr, BYTE byReg, LPINT lpiResult)
{
    I2C_TRANSFER_BLOCK I2CXferBlock;
    I2C_PACKET I2CPacket[2];
    BYTE byOutData;
    BYTE byInData;

    byOutData = byReg;
    I2CPacket[0].pbyBuf = (PBYTE) &byOutData;
    I2CPacket[0].wLen = sizeof(byOutData);
    I2CPacket[0].byRW = I2C_RW_WRITE;
    I2CPacket[0].byAddr = byAddr;
    I2CPacket[0].lpiResult = lpiResult;

    I2CPacket[1].pbyBuf = (PBYTE) &byInData;
    I2CPacket[1].wLen = sizeof(byInData);
    I2CPacket[1].byRW = I2C_RW_READ;
    I2CPacket[1].byAddr = byAddr;
    I2CPacket[1].lpiResult = lpiResult;

    I2CXferBlock.pI2CPackets = I2CPacket;
    I2CXferBlock.iNumPackets = 2;

    if(!I2CTransfer(hI2C, &I2CXferBlock))
        ERRORMSG(TRUE,(TEXT("%s:I2C Read HDMI Register Fail\r\n"),__WFUNCTION__));

    return byInData;
}

