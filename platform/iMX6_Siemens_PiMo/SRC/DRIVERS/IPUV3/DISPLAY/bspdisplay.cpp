//------------------------------------------------------------------------------
//
//  Copyright (C) 2008-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  bspdisplay.cpp
//
//  Provides BSP-specific routines for use by IPU Display Interface layer.
//
//------------------------------------------------------------------------------

#pragma warning(push)
#pragma warning(disable: 4115 4201 4204 4214)
#include <windows.h>

#include <Devload.h>
#include <ceddk.h>
#include <cmnintrin.h>
#include <NKIntr.h>
#include <winddi.h>
#include <ddgpe.h>
#pragma warning(pop)

#include "IpuBuffer.h"
#include "Ipu_base.h"
#include "Ipu_common.h"
#include "i2cbus.h"

#include "cm.h"
#include "dc.h"
#include "di.h"
#include "bsp.h"
#include "lvds.h"
#include "hdmi.h"
#include "CLAA.h"
#include "dvi.h"

#include "bspdisplay.h"
#include "panels.h"

#define DEBUG_BSPDISPLAY													0

#ifndef _PREFAST_
#pragma warning(disable: 4068) // Disable pragma warnings
#endif

//------------------------------------------------------------------------------
// External Functions
extern "C" DWORD CSPIPUGetClk();


//------------------------------------------------------------------------------
// External Variables


//------------------------------------------------------------------------------
// Defines
#define DISPLAY_REG_PATH                      TEXT("Drivers\\Display\\DDIPU")
#define DI0_REG_PATH                          TEXT("Drivers\\Display\\DDIPU\\DI0")
#define DI1_REG_PATH                          TEXT("Drivers\\Display\\DDIPU\\DI1")
#define DUAL_DEVICE                           TEXT("DualDevice")


#define VIDEO_ROW_RES                         TEXT("CxScreen")
#define VIDEO_COL_RES                         TEXT("CyScreen")
#define PIXEL_DEPTH                           TEXT("Bpp")
#define VIDEO_PIXEL_DEPTH                     TEXT("VideoBpp")
#define PANEL_TYPE                            TEXT("PanelType")
#define PANEL_BOOT_STATE                      TEXT("EnableOnBoot")
#define SUPPORTED_PANEL_TYPES                 TEXT("SupportedPanelTypes")
#define DISPLAY_ROTATION                      TEXT("System\\GDI\\Rotation")
#define VIDEO_MEM_SIZE                        TEXT("VideoMemSize")

#if defined(USE_C2D_ROUTINES)
#define C2D_FLAG                              TEXT("C2DFlag")
#define C2D_THRESHOLD                         TEXT("C2DThreshold")
#endif //#if defined(USE_C2D_ROUTINES)

#define DI_COMMAND_WAVEFORM 0
#define DI_DATAWR_WAVEFORM  1
#define DI_DATARD_WAVEFORM  2
#define DI_SDC_WAVEFORM     3
#define DI_SERIAL_WAVEFORM  4

#define DI_RS_SIGNAL        0
#define DI_WR_SIGNAL        1
#define DI_RD_SIGNAL        1
#define DI_SER_CLK_SIGNAL   1
#define DI_CS_SIGNAL        2 
#define DI_NOUSE_SIGNAL     3

#define DI_DEN_SIGNAL       0

#define DI_COUNTER_BASECLK  0
#define DI_COUNTER_IHSYNC   1
#define DI_COUNTER_OHSYNC   2
#define DI_COUNTER_OVSYNC   3
#define DI_COUNTER_ALINE    4
#define DI_COUNTER_ACLOCK   5

#define DI_COUNTER_VGA_HSYNC   7
#define DI_COUNTER_VGA_VSYNC   8

#define DI_TVE_WAVEFORM           6

#define DI_TVE_COUNTER_BHSYNC     1
#define DI_TVE_COUNTER_F1VSYNC    2   // Field 1
#define DI_TVE_COUNTER_IHSYNC     3
#define DI_TVE_COUNTER_AFIELD     4
#define DI_TVE_COUNTER_ALINE      5
#define DI_TVE_COUNTER_F0VSYNC    6   // Field 0
#define DI_TVE_COUNTER_DCVSYNC    7   
#define DI_TVE_COUNTER_APIXEL     8
#define DI_TVE_COUNTER_NVSYNC     9   // Not shifted VSYNC

//------------------------------------------------------------------------------
// Types
#define BSPDISPLAY_FUNCTION_ENTRY() \
    RETAILMSG(DEBUG_BSPDISPLAY, (TEXT("++%s\r\n"), __WFUNCTION__))
#define BSPDISPLAY_FUNCTION_EXIT() \
    RETAILMSG(DEBUG_BSPDISPLAY, (TEXT("--%s\r\n"), __WFUNCTION__))


//------------------------------------------------------------------------------
// Global Variables
DWORD g_dwBoardID = 0;

//------------------------------------------------------------------------------
// Local Variables

//------------------------------------------------------------------------------
// Local Functions

DWORD BSPGetPixelDepthFromRegistry();
static int GetPanelTypeFromRegistry(DI_SELECT);
PANEL_INFO * InitPanelArrayFromRegistry();
BOOL GetConfigFromRegistry(PANEL_INFO * pInfo, LPCTSTR regPath);
BOOL GetHDMIConfigFromRegistry(PANEL_INFO * pInfo);
BOOL GetLVDS1ConfigFromRegistry(PANEL_INFO * pInfo);
BOOL GetLVDS2ConfigFromRegistry(PANEL_INFO * pInfo);
BOOL SetPanelInfoFromRegistry(DI_SELECT di_sel, int iPanelType, PANEL_INFO * pInfo);
DWORD BSPGetDualDeviceFromRegistry();
void AsyncDCConfig(DI_SELECT di_sel, PANEL_INFO* pDIPanelInfo, int *pMappingCounter, DWORD *pDispCounter, UINT32 *pMicroCodeAddr);
void SyncDCConfig(DI_SELECT di_sel, PANEL_INFO* pDIPanelInfo, int *pMappingCounter, DWORD *pDispCounter, UINT32 *pMicroCodeAddr);
void SyncDCConfigTV(DI_SELECT di_sel, PANEL_INFO* pDIPanelInfo, int *pMappingCounter, DWORD *pDispCounter, UINT32 *pMicroCodeAddr);
void SerialDCConfig(PANEL_INFO* pDIPanelInfo, int *pMappingCounter, DWORD *pDispCounter, UINT32 *pMicroCodeAddr);

void AsyncDIConfig(DI_SELECT di_sel, PANEL_INFO* pDIPanelInfo);
void SyncDIConfig(DI_SELECT di_sel, PANEL_INFO* pDIPanelInfo);
void SyncDIConfigTV2IL(DI_SELECT di_sel, PANEL_INFO* pDIPanelInfo);
void SyncDIConfigTV2P(DI_SELECT di_sel, PANEL_INFO* pDIPanelInfo);

void SerialDIConfig(DI_SELECT di_sel, PANEL_INFO* pDIPanelInfo);

//------------------------------------------------------------------------------
//
// Function: BSPGetPanelInfo
//
// Get the size of video RAM requested by user from registry.
//
// Parameters:
//      di_sel
//          [in] Select DI0 or DI1 to query panel info from
//
//      ppInfo
//          [out] PANEL_INFO structure to fill out.
//
// Returns:
//      TRUE if panel connected to di_sel Display Interface
//      FALSE if no panel connected to di_sel Display Interface
//
//------------------------------------------------------------------------------
BOOL BSPGetPanelInfo(DI_SELECT di_sel, PANEL_INFO ** ppInfo)
{
    int panelNum;
	int result = 0;
	/*get panel number from registry*/
    panelNum = GetPanelTypeFromRegistry(di_sel);
    if (panelNum == -1)
    {
        DEBUGMSG(1, (TEXT("BSPGetPanelInfo: Cannot return panel info.\r\n")));
        return FALSE;
    }

    if (panelNum == 0)
    {
        return FALSE;
    }

	RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Selected Panel Num=%d for DI_SELECT %d\r\n"), __WFUNCTION__, panelNum, di_sel ) );
    // To get valid panelNum, in order to index into array with panel info,
    // subtract 1 from panelNum that was read from registry. (0 in registry
    // represents no panel connected)
    panelNum = panelNum - 1;

	/*check if there is a least one configuration defined*/
    if (panelNum > NUM_SUPPORTED_PANELS) return FALSE;
   
	//Initialize the ppInfo at first.
	*ppInfo = &g_PanelArray[panelNum];
    //Set DI_NUM here, instead of panels.h which make more sense and reduce the number of panels in panels.h
    (*ppInfo)->DI_NUM = di_sel;

	// Traverse array to match panel num.
	// We can't just use panel num to index array, 
	// b/c array could have been reordered.
	for (int i = 0; i < NUM_SUPPORTED_PANELS; i++)
	{
		if (g_PanelArray[i].PANELID == (DWORD)panelNum)
		{
			*ppInfo = &g_PanelArray[i];

			/*Set panel configuration from registry*/
			result = SetPanelInfoFromRegistry(di_sel, panelNum, *ppInfo);
			RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:SetPanelInfoFromRegistry %s !\r\n"),
				__WFUNCTION__, 
				 result == TRUE ? L"SUCEEDED" : L"FAILED" ));

			return TRUE;
		}
	}

	/*no configuration for the corresponding panel id*/
	return FALSE;
}


//------------------------------------------------------------------------------
//
// Function: BSPGetModeInfo
//
// Retrieves the panel Info.
//
// Parameters:
//      modeNo
//          [in] Use modeNo to query  panel info from
//
//      ppInfo
//          [out] PANEL_INFO structure to fill out.
//
// Returns:
//      TRUE if the mode is supported and return the panel info
//      FALSE if the mode is not support
//
//------------------------------------------------------------------------------
BOOL BSPGetModeInfo(UINT16 modeNo, PANEL_INFO ** ppInfo)
{
    if (modeNo <  NUM_SUPPORTED_PANELS)
    {
        *ppInfo = &g_PanelArray[modeNo];
        return TRUE;
    }
    else
        return FALSE;
}


//------------------------------------------------------------------------------
//
// Function: BSPGetNumSupportedModes
//
// Retrieves the number of supported current panel type from registry.
//
// Parameters:
//      None.
//
// Returns:
//      Number of supported modes.  0 if failure.
//
//------------------------------------------------------------------------------
DWORD BSPGetNumSupportedModes(VOID)
{
    return NUM_SUPPORTED_PANELS;
}

//------------------------------------------------------------------------------
//
// Function: BSPGetSupportedModes
//
// Creates an array of supported display modes, with LCD modes first, and
// then followed by TV modes.
//
// Parameters:
//      pModeArray
//          [in] GPEMode array to fill with supported display modes.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void BSPGetSupportedModes(GPEMode *pModeArray)
{
    int universalBpp;
    UINT16 counter = 0;

    universalBpp = BSPGetPixelDepthFromRegistry();

    PANEL_INFO *pInitPanelInfo = NULL;

    BSPGetPanelInfo(DI_SELECT_DI0, &pInitPanelInfo);
    BSPGetPanelInfo(DI_SELECT_DI1, &pInitPanelInfo);
    // We retrieve panel info, because we may have to reorder
    // the display mode array. WinCE GDI will always initialize display
    // driver with mode 0, so if the mode that we want to start with is
    // not 0, we must re-order the modes so that the preferred initial 
    // mode is set to 0.
    if (pInitPanelInfo)
    {
        // If mode is 0, we are fine, the initial mode 
        // is already in the right place.
        if (pInitPanelInfo->MODEID != 0)
        {
            // IMPORTANT NOTE: We should not reorder if the mode is TV,
            // because it would render inaccurate the code in BSPIsTVMode().  DI0
            // should not be connected to a TV mode, so this is normally not a problem.

            // TODO: Final IMPORTANT NOTE: The preferred solution is to find a way to 
            // have GDI set the initial mode to a non-zero value, but we do not 
            // know how to do this as of now.  If/When we find out how, this code
            // should be removed.

            // Re-order array with initial panel at 0
            PANEL_INFO *tempPanelInfo = (PANEL_INFO *)LocalAlloc(LPTR, sizeof(PANEL_INFO));

            // Copy init panel info into temp
            memcpy(tempPanelInfo, pInitPanelInfo, sizeof(PANEL_INFO));

            // Shift all modes below init panel mode up one
            for (int i = (int)tempPanelInfo->MODEID; i > 0; i--)
            {
                memcpy(&g_PanelArray[i], &g_PanelArray[i - 1], sizeof(PANEL_INFO));
                // Update mode #
                g_PanelArray[i].MODEID = i;
            }

            // Finally, set array element 0 to the initial panel mode
            memcpy(&g_PanelArray[0], tempPanelInfo, sizeof(PANEL_INFO));
            // Update mode #
            g_PanelArray[0].MODEID = 0;

            LocalFree(tempPanelInfo);
        }

        // First, build up an array of supported LCD modes using the PANEL_INFO structure
        while (counter < NUM_SUPPORTED_PANELS)
        {
#pragma prefast(suppress: 6385, "Panel array manipulation alright...bounds not exceeded.")
            pModeArray[counter].modeId    = g_PanelArray[counter].MODEID;
            pModeArray[counter].width     = g_PanelArray[counter].WIDTH;
            pModeArray[counter].height    = g_PanelArray[counter].HEIGHT;
            pModeArray[counter].frequency = g_PanelArray[counter].FREQUENCY;
            pModeArray[counter].Bpp       = universalBpp;

            counter++;
        }
    }

    if(counter == 0)
    {
        ERRORMSG(1, (TEXT("No display panel is registered!!!\r\n")));
    }
}


//------------------------------------------------------------------------------
//
// Function: GetPanelTypeFromRegistry
//
// Gets the current panel type from registry.
//
// Parameters:
//      None.
//
// Returns:
//      Panel type number for initial display panel.
//
//------------------------------------------------------------------------------
int GetPanelTypeFromRegistry(DI_SELECT di_sel)
{
    int iPanelNum = -1;
    LONG  error;
    HKEY  hKey;
    int iPanelType;
    DWORD dwSize;
    LPCTSTR regPathString;

    regPathString = (di_sel == DI_SELECT_DI0) ? DI0_REG_PATH : DI1_REG_PATH;

    // Open registry key for display driver (for either DI0 or DI1)
    error = RegOpenKeyEx(HKEY_LOCAL_MACHINE, regPathString, 0 , 0, &hKey);
    if (error != ERROR_SUCCESS)
    {
        RETAILMSG(DEBUG_BSPDISPLAY,(TEXT("GetPanelTypeFromRegistry: Failed to open reg path:%s [Error:0x%x]\r\n"), regPathString, error));
        goto _donePanel;
    }
    RETAILMSG(DEBUG_BSPDISPLAY, (TEXT("@@@ OPEN REGISTER SUCCEED @@@ \n\r")));

    // Panel Type
    dwSize = sizeof(iPanelType);
    error = RegQueryValueEx(hKey, PANEL_TYPE, NULL, NULL, (LPBYTE)&iPanelType, (LPDWORD)&dwSize);
    if (error != ERROR_SUCCESS)
    {
        DEBUGMSG(1,
                 (TEXT("GetPanelTypeFromRegistry: Failed to get display panel type, Error 0x%X\r\n"), error));
        goto _donePanel;
    }

    iPanelNum = iPanelType;

_donePanel:
    // Close registry key
    RegCloseKey(hKey);

    DEBUGMSG(1, (TEXT("GetPanelTypeFromRegistry: %s!\r\n"), (iPanelNum != -1) ? L"succeeds" : L"fails"));

    return iPanelNum;
}

BOOL GetConfigFromRegistry(PANEL_INFO *pInfo, LPCTSTR regPath)
{
	BOOL bRet = false;
	LONG  error;
    HKEY  hKey;
	DWORD dwSize = sizeof(DWORD);
	INT32 tc;
	UINT32 res, width, height, vsyncwidth, vstartwidth, vendwidth, hsyncwidth, hstartwidth, hendwidth;
	UINT32 pixClk, pixOffset, pixClkUp, pixClkDown, rgbFormat;
	UINT32 dataMaskEn, clkIdleEn, clkSelEn, vSyncPol, outpuEnPol, dataPol, clkpol, hSyncPol,dataBusWidth;
	PBusMappingData		mapping;
	IPU_PIXEL_FORMAT	pixFormat;

	RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s: Reading configuration from path %s\r\n"), __WFUNCTION__, regPath ) );

	width	= 0;
	height	= 0;

	error = RegOpenKeyEx(HKEY_LOCAL_MACHINE, regPath, 0, 0, &hKey);
	if(error != ERROR_SUCCESS) {
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Can't open registry path \"%s\"\r\n"), __WFUNCTION__, regPath ) );
		goto _cleanup;
	}

	// RESOLUTION
	error = RegQueryValueEx(hKey, CONFIG_RESOLUTION, 0, 0, (LPBYTE)&res, (LPDWORD)&dwSize);
	if(error != ERROR_SUCCESS) {
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Error getting key \"%s\". Trying with 'Width' and 'Height' keys.\r\n"), __WFUNCTION__, CONFIG_RESOLUTION ) );
		res = DISPLAY_CUSTOM;
	}
	if(res == DISPLAY_CUSTOM) {
		dwSize = sizeof(DWORD);
		error = RegQueryValueEx(hKey, CONFIG_WIDTH, 0, 0, (LPBYTE)&width, (LPDWORD)&dwSize);
		if(error != ERROR_SUCCESS) {
			RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Error getting key \"%s\"\r\n"), __WFUNCTION__, CONFIG_WIDTH ) );
			goto _cleanup;
		}
		RETAILMSG(DEBUG_BSPDISPLAY, (TEXT("Width =%d"), width) );
		dwSize = sizeof(DWORD);
		error = RegQueryValueEx(hKey, CONFIG_HEIGHT, 0, 0, (LPBYTE)&height, (LPDWORD)&dwSize);
		if(error != ERROR_SUCCESS) {
			RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Error getting key \"%s\"\r\n"), __WFUNCTION__, CONFIG_HEIGHT ) );
			goto _cleanup;
		}
		RETAILMSG(DEBUG_BSPDISPLAY, (TEXT("Height =%d\r\n"), height) );
	}

	// BUS WIDTH
	dwSize = sizeof(DWORD);
	error = RegQueryValueEx(hKey, CONFIGURE_BUS_WIDTH, 0, 0, (LPBYTE)&dataBusWidth, (LPDWORD)&dwSize);
	if(error != ERROR_SUCCESS) {
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Error getting key \"%s\"\r\n"), __WFUNCTION__, CONFIGURE_BUS_WIDTH ) );
		goto _cleanup;
	}
	switch(dataBusWidth)
	{
	case 24:
		mapping		= &rgb24Mapping;
		pixFormat	= IPU_PIX_FMT_RGB24;
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s Using RGB24  format.\r\n"), __WFUNCTION__ ) );
		break;
	case 18:
		mapping		= &rgb18Mapping;
		pixFormat	= IPU_PIX_FMT_RGB666;
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s Using RGB18 format.\r\n"), __WFUNCTION__ ) );
		break;
	case 16:
		mapping		= &rgb16Mapping;
		pixFormat	= IPU_PIX_FMT_RGB565;
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s Using RGB16 format.\r\n"), __WFUNCTION__ ) );
		break;
	default:
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s: Error : RGB%d format not supported. Using RGB24 by default.\r\n"), __WFUNCTION__, dataBusWidth ) );
		mapping		= &rgb24Mapping;
		pixFormat	= IPU_PIX_FMT_RGB24;
		mapping		= &rgb24Mapping;
	}

	// MAPPINGS - Optional
	dwSize = sizeof(DWORD);
	error = RegQueryValueEx(hKey, CONFIGURE_PIXEL_FORMAT, 0, 0, (LPBYTE)&rgbFormat, (LPDWORD)&dwSize);
	if(error != ERROR_SUCCESS) {
		// Pixel format autoselected with BUS WIDTH
	}
	// Force a particular pixel format  
	switch(rgbFormat)
	{
		case 888: pixFormat	= IPU_PIX_FMT_RGB24; break;
		case 666: pixFormat	= IPU_PIX_FMT_RGB666; break;
		case 565: pixFormat	= IPU_PIX_FMT_RGB565; break;
	}

	// TRANSFER CYLCE
	dwSize = sizeof(DWORD);
	error = RegQueryValueEx(hKey, CONFIG_TRANSFER_CYCLE, 0, 0, (LPBYTE)&tc, (LPDWORD)&dwSize);
	if(error != ERROR_SUCCESS) {
		RETAILMSG(TRUE, ( TEXT("%s:Error getting key \"%s\"\r\n"), __WFUNCTION__, CONFIG_TRANSFER_CYCLE ) );
		goto _cleanup;
	}

	// VERTICAL SYNC WIDTH
	dwSize = sizeof(DWORD);
	error = RegQueryValueEx(hKey, CONFIG_V_SYNC_WIDTH, 0, 0, (LPBYTE)&vsyncwidth, (LPDWORD)&dwSize);
	if(error != ERROR_SUCCESS) {
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Error getting key \"%s\"\r\n"), __WFUNCTION__, CONFIG_V_SYNC_WIDTH ) );
		goto _cleanup;
	}

	// VERTICAL START WIDTH
	dwSize = sizeof(DWORD);
	error = RegQueryValueEx(hKey, CONFIG_V_START_WIDTH, 0, 0, (LPBYTE)&vstartwidth, (LPDWORD)&dwSize);
	if(error != ERROR_SUCCESS) {
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Error getting key \"%s\"\r\n"), __WFUNCTION__, CONFIG_V_START_WIDTH ) );
		goto _cleanup;
	}

	// VERTICAL END WIDTH
	dwSize = sizeof(DWORD);
	error = RegQueryValueEx(hKey, CONFIG_V_END_WIDTH, 0, 0, (LPBYTE)&vendwidth, (LPDWORD)&dwSize);
	if(error != ERROR_SUCCESS) {
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Error getting key \"%s\"\r\n"), __WFUNCTION__, CONFIG_V_END_WIDTH ) );
		goto _cleanup;
	}

	// HORIZONTAL SYNC WIDTH
	dwSize = sizeof(DWORD);
	error = RegQueryValueEx(hKey, CONFIG_H_SYNC_WIDTH, 0, 0, (LPBYTE)&hsyncwidth, (LPDWORD)&dwSize);
	if(error != ERROR_SUCCESS) {
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Error getting key \"%s\"\r\n"), __WFUNCTION__, CONFIG_H_SYNC_WIDTH ) );
		goto _cleanup;
	}

	// HORIZONTAL START WIDTH
	dwSize = sizeof(DWORD);
	error = RegQueryValueEx(hKey, CONFIG_H_START_WIDTH, 0, 0, (LPBYTE)&hstartwidth, (LPDWORD)&dwSize);
	if(error != ERROR_SUCCESS) {
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Error getting key \"%s\"\r\n"), __WFUNCTION__, CONFIG_H_START_WIDTH ) );
		goto _cleanup;
	}
	
	// HORIZONTAL END WIDTH
	dwSize = sizeof(DWORD);
	error = RegQueryValueEx(hKey, CONFIG_H_END_WIDTH, 0, 0, (LPBYTE)&hendwidth, (LPDWORD)&dwSize);
	if(error != ERROR_SUCCESS) {
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Error getting key \"%s\"\r\n"), __WFUNCTION__, CONFIG_H_END_WIDTH ) );
		goto _cleanup;
	}

	// PIXEL CLOCK FREQ
	dwSize = sizeof(DWORD);
	error = RegQueryValueEx(hKey, CONFIG_PIXEL_CLK, 0, 0, (LPBYTE)&pixClk, (LPDWORD)&dwSize);
	if(error != ERROR_SUCCESS) {
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Error getting key \"%s\"\r\n"), __WFUNCTION__, CONFIG_PIXEL_CLK ) );
		goto _cleanup;
	}

	// PIXEL DATA OFFSET
	dwSize = sizeof(DWORD);
	error = RegQueryValueEx(hKey, CONFIG_PIXEL_DATA_OFFSET, 0, 0, (LPBYTE)&pixOffset, (LPDWORD)&dwSize);
	if(error != ERROR_SUCCESS) {
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Error getting key \"%s\"\r\n"), __WFUNCTION__, CONFIG_PIXEL_DATA_OFFSET ) );
		goto _cleanup;
	}

	// PIXEL CLOCK UP
	dwSize = sizeof(DWORD);
	error = RegQueryValueEx(hKey, CONFIG_PIXEL_CLK_UP, 0, 0, (LPBYTE)&pixClkUp, (LPDWORD)&dwSize);
	if(error != ERROR_SUCCESS) {
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Error getting key \"%s\"\r\n"), __WFUNCTION__, CONFIG_PIXEL_CLK_UP ) );
		goto _cleanup;
	}

	// PIXEL CLOCK DOWN
	dwSize = sizeof(DWORD);
	error = RegQueryValueEx(hKey, CONFIG_PIXEL_CLK_DOWN, 0, 0, (LPBYTE)&pixClkDown, (LPDWORD)&dwSize);
	if(error != ERROR_SUCCESS) {
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Error getting key \"%s\"\r\n"), __WFUNCTION__, CONFIG_PIXEL_CLK_DOWN ) );
		goto _cleanup;
	}

	// DATA MASK ENABLE
	dwSize = sizeof(DWORD);
	error = RegQueryValueEx(hKey, CONFIG_DATA_MASK_EN, 0, 0, (LPBYTE)&dataMaskEn, (LPDWORD)&dwSize);
	if(error != ERROR_SUCCESS) {
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Error getting key \"%s\"\r\n"), __WFUNCTION__, CONFIG_DATA_MASK_EN ) );
		goto _cleanup;
	}

	// CLOCK IDLE ENABLE
	dwSize = sizeof(DWORD);
	error = RegQueryValueEx(hKey, CONFIG_CLK_IDLE_EN, 0, 0, (LPBYTE)&clkIdleEn, (LPDWORD)&dwSize);
	if(error != ERROR_SUCCESS) {
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Error getting key \"%s\"\r\n"), __WFUNCTION__, CONFIG_CLK_IDLE_EN ) );
		goto _cleanup;
	}

	// CLOCK SELECT ENABLE
	dwSize = sizeof(DWORD);
	error = RegQueryValueEx(hKey, CONFIG_CLK_SEL_EN, 0, 0, (LPBYTE)&clkSelEn, (LPDWORD)&dwSize);
	if(error != ERROR_SUCCESS) {
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Error getting key \"%s\"\r\n"), __WFUNCTION__, CONFIG_CLK_SEL_EN ) );
		goto _cleanup;
	}

	// VERTICAL SYNC POLARITY
	dwSize = sizeof(DWORD);
	error = RegQueryValueEx(hKey, CONFIG_V_SYNC_POLARITY, 0, 0, (LPBYTE)&vSyncPol, (LPDWORD)&dwSize);
	if(error != ERROR_SUCCESS) {
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Error getting key \"%s\"\r\n"), __WFUNCTION__, CONFIG_V_SYNC_POLARITY ) );
		goto _cleanup;
	}

	// OUTPUT ENABLE POLARITY
	dwSize = sizeof(DWORD);
	error = RegQueryValueEx(hKey, CONFIG_OUTPUT_ENABLE_POLARITY, 0, 0, (LPBYTE)&outpuEnPol, (LPDWORD)&dwSize);
	if(error != ERROR_SUCCESS) {
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Error getting key \"%s\"\r\n"), __WFUNCTION__, CONFIG_OUTPUT_ENABLE_POLARITY ) );
		goto _cleanup;
	}

	// DATA POLARITY
	dwSize = sizeof(DWORD);
	error = RegQueryValueEx(hKey, CONFIG_DATA_POL, 0, 0, (LPBYTE)&dataPol, (LPDWORD)&dwSize);
	if(error != ERROR_SUCCESS) {
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Error getting key \"%s\"\r\n"), __WFUNCTION__, CONFIG_DATA_POL ) );
		goto _cleanup;
	}

	// CLOCK POLARITY
	dwSize = sizeof(DWORD);
	error = RegQueryValueEx(hKey, CONFIG_CLK_POL, 0, 0, (LPBYTE)&clkpol, (LPDWORD)&dwSize);
	if(error != ERROR_SUCCESS) {
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Error getting key \"%s\"\r\n"), __WFUNCTION__, CONFIG_CLK_POL ) );
		goto _cleanup;
	}

	// HORIZONTAL SYNC POLARITY
	dwSize = sizeof(DWORD);
	error = RegQueryValueEx(hKey, CONFIG_H_SYNC_POLARITY, 0, 0, (LPBYTE)&hSyncPol, (LPDWORD)&dwSize);
	if(error != ERROR_SUCCESS) {
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Error getting key \"%s\"\r\n"), __WFUNCTION__, CONFIG_H_SYNC_POLARITY ) );
		goto _cleanup;
	}

	// DATA BUS WIDTH
	dwSize = sizeof(DWORD);
	error = RegQueryValueEx(hKey, CONFIGURE_BUS_WIDTH, 0, 0, (LPBYTE)&dataBusWidth, (LPDWORD)&dwSize);
	if(error != ERROR_SUCCESS) {
		dataBusWidth = 24;
		RETAILMSG(TRUE, ( TEXT("Default data bus [%d bits]"),dataBusWidth));
	}

	switch(res)
	{
		case DISPLAY_VGA:
			width	= 640;
			height	= 480;
			break;
		case DISPLAY_XGA:
			width	= 1024;
			height	= 768;
			break;
		case DISPLAY_SXGA:
			width	= 1280;
			height	= 1024;
			break;
		case DISPLAY_UXGA:
			width	= 1600;
			height	= 1200;
			break;
		case DISPLAY_HD:
			width	= 1280;
			height	= 720;
			break;
		case DISPLAY_FHD:
			width	= 1920;
			height	= 1080;
			break;
		case DISPLAY_WUXGA:
			width	= 1920;
			height	= 1200;
			break;
		case DISPLAY_CUSTOM:
			break;
		default:
			RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Bad \"Resolution\" value from registry !\r\n"), __WFUNCTION__ ) );
			goto _cleanup;
			break;
		}

	RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Selected resolution = %d*%d\r\n"), __WFUNCTION__, width, height ) );

	pInfo->WIDTH					= width;
	pInfo->HEIGHT					= height;
	pInfo->MAPPINGS					= mapping;
	pInfo->PIXEL_FMT				= pixFormat;
	pInfo->TRANSFER_CYCLES			= tc;
	pInfo->VSYNCWIDTH				= vsyncwidth;
	pInfo->VSTARTWIDTH				= vstartwidth;
	pInfo->VENDWIDTH				= vendwidth;
	pInfo->HSTARTWIDTH				= hstartwidth;
	pInfo->HSYNCWIDTH				= vsyncwidth;
	pInfo->HENDWIDTH				= hendwidth;
	pInfo->PIX_CLK_FREQ				= pixClk;
	pInfo->PIX_DATA_POS				= pixOffset;
	pInfo->PIX_CLK_UP				= pixClkUp;
	pInfo->PIX_CLK_DOWN				= pixClkDown;
	pInfo->SYNC_SIG_POL.DATAMASK_EN = dataMaskEn;
	pInfo->SYNC_SIG_POL.CLKIDLE_EN	= clkIdleEn;
	pInfo->SYNC_SIG_POL.CLKSEL_EN	= clkSelEn;
	pInfo->SYNC_SIG_POL.VSYNC_POL	= vSyncPol;
	pInfo->SYNC_SIG_POL.ENABLE_POL	= outpuEnPol;
	pInfo->SYNC_SIG_POL.DATA_POL	= dataPol;
	pInfo->SYNC_SIG_POL.CLK_POL		= clkpol;
	pInfo->SYNC_SIG_POL.HSYNC_POL	= hSyncPol;

	bRet							= TRUE;

_cleanup:
	// Close registry key
    RegCloseKey(hKey);

	return bRet;
}

BOOL GetHDMIConfigFromRegistry(PANEL_INFO * pInfo)
{
	return GetConfigFromRegistry(pInfo, CONFIG_REG_PATH_HDMI);
}

BOOL GetLVDS1ConfigFromRegistry(PANEL_INFO *pInfo)
{
	return GetConfigFromRegistry(pInfo, CONFIG_REG_PATH_LVDS1);
}

BOOL GetLVDS2ConfigFromRegistry(PANEL_INFO *pInfo)
{
	return GetConfigFromRegistry(pInfo, CONFIG_REG_PATH_LVDS2);
}

BOOL SetPanelInfoFromRegistry(DI_SELECT di_sel, int iPanelType, PANEL_INFO * pInfo)
{
	BOOL bRet = FALSE;

	switch(di_sel)
	{
	case DI_SELECT_DI0:
		// HDMI
		if(iPanelType == DISPLAY_PANEL_HDMI)
		{
			bRet = GetHDMIConfigFromRegistry(pInfo);
		}
		// LVDS1
		else if(iPanelType == DISPLAY_PANEL_LVDS1)
		{
			bRet = GetLVDS1ConfigFromRegistry(pInfo);
		}
		else if(iPanelType == DISPLAY_PANEL_LVDS2)
		{
			bRet = GetLVDS2ConfigFromRegistry(pInfo);
		}
		else 
		{
			bRet = RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Bad panel type for DI0!\r\n"), __WFUNCTION__ ) );
			goto _cleanup;
		}
		break;
    case DI_SELECT_DI1:
        if(iPanelType == DISPLAY_PANEL_LVDS1)
		{
			bRet = GetLVDS1ConfigFromRegistry(pInfo);
		}
		else if(iPanelType == DISPLAY_PANEL_LVDS2)
		{
			bRet = GetLVDS2ConfigFromRegistry(pInfo);
		}
        else if(iPanelType == DISPLAY_PANEL_HDMI)
		{
			bRet = GetHDMIConfigFromRegistry(pInfo);
		}
		else 
		{
			bRet = RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:Bad panel type for DI1!\r\n"), __WFUNCTION__ ) );
			goto _cleanup;
		}
		break;
	}

_cleanup:

	RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s: selected display = \"%s\"\r\n"), __WFUNCTION__, pInfo->NAME ) );

	return bRet;
}


//------------------------------------------------------------------------------
//
// Function: BSPIsEnabledOnBoot
//
// Retrieves info from registry on whether the panel should
// be enabled upon display driver startup.
//
// Parameters:
//      di_sel
//          [in] Select DI0 or DI1 to query panel info from
//
// Returns:
//      TRUE if panel should be enabled on boot
//      FALSE if panel should be disabled on boot
//
//------------------------------------------------------------------------------
BOOL BSPIsEnabledOnBoot(DI_SELECT di_sel)
{
    LONG  error;
    HKEY  hKey;
    BOOL  bIsEnabledOnBoot = FALSE;
    DWORD dwSize;
    LPCTSTR regPathString;

    regPathString = (di_sel == DI_SELECT_DI0) ? DI0_REG_PATH : DI1_REG_PATH;

    // Open registry key for display driver (for either DI0 or DI1)
    error = RegOpenKeyEx(HKEY_LOCAL_MACHINE, regPathString, 0 , 0, &hKey);
    if (error != ERROR_SUCCESS)
    {
        DEBUGMSG(1,(TEXT("BSPIsEnabledOnBoot: Failed to open reg path:%s [Error:0x%x]\r\n"), regPathString, error));
        goto _donePanel;
    }

    // Panel Type
    dwSize = sizeof(bIsEnabledOnBoot);
    error = RegQueryValueEx(hKey, PANEL_BOOT_STATE, NULL, NULL, (LPBYTE)&bIsEnabledOnBoot, (LPDWORD)&dwSize);
    if (error != ERROR_SUCCESS)
    {
        DEBUGMSG(1,
                 (TEXT("BSPIsEnabledOnBoot: Failed to get enabled on boot info, Error 0x%X\r\n"), error));
        goto _donePanel;
    }

_donePanel:
    // Close registry key
    RegCloseKey(hKey);

    return bIsEnabledOnBoot;
}

//------------------------------------------------------------------------------
//
// Function: BSPGetPixelDepthFromRegistry
//
// Gets the requested pixel depth from registry.
//
// Parameters:
//      None.
//
// Returns:
//      Pixel depth for initial display panel.
//
//------------------------------------------------------------------------------
DWORD BSPGetPixelDepthFromRegistry(VOID)
{
    LONG  error;
    HKEY  hKey;
    DWORD dwSize;
    DWORD dwPixelDepth = 0;

    // Open registry key for display driver
    error = RegOpenKeyEx(HKEY_LOCAL_MACHINE, DISPLAY_REG_PATH, 0 , 0, &hKey);
    if (error != ERROR_SUCCESS)
    {
        DEBUGMSG(1,(TEXT("DDIPU GetPixelDepthFromRegistry: Failed to open reg path:%s [Error:0x%x]\r\n"), DISPLAY_REG_PATH, error));
        goto _donePixel;
    }

    // Panel Type
    dwSize = sizeof(dwPixelDepth);
    error = RegQueryValueEx(hKey, PIXEL_DEPTH, NULL, NULL, (LPBYTE)&dwPixelDepth, (LPDWORD)&dwSize);
    if (error != ERROR_SUCCESS)
    {
        DEBUGMSG(1,
                 (TEXT("DDIPU GetPixelDepthFromRegistry: Failed to get display pixel depth, Error 0x%X\r\n"), error));
        goto _donePixel;
    }

_donePixel:
    // Close registry key
    RegCloseKey(hKey);

    DEBUGMSG(1, (TEXT("DDIPU GetPixelDepthFromRegistry: %s!\r\n"), (error == ERROR_SUCCESS) ? L"succeeds" : L"fails"));
    return dwPixelDepth;
}

//------------------------------------------------------------------------------
//
// Function: BSPGetVideoPixelDepthFromRegistry
//
// Gets the requested video pixel depth from registry.
//
// Parameters:
//      None.
//
// Returns:
//      Pixel depth for video output.
//
//------------------------------------------------------------------------------
DWORD BSPGetVideoPixelDepthFromRegistry(VOID)
{
    LONG  error;
    HKEY  hKey;
    DWORD dwSize;
    DWORD dwVideoPixelDepth = 0;

    // Open registry key for display driver
    error = RegOpenKeyEx(HKEY_LOCAL_MACHINE, DISPLAY_REG_PATH, 0 , 0, &hKey);
    if (error != ERROR_SUCCESS)
    {
        DEBUGMSG(1,(TEXT("DDIPU GetVideoPixelDepthFromRegistry: Failed to open reg path:%s [Error:0x%x]\r\n"), DISPLAY_REG_PATH, error));
        goto _doneVideoPixel;
    }

    // Panel Type
    dwSize = sizeof(dwVideoPixelDepth);
    error = RegQueryValueEx(hKey, VIDEO_PIXEL_DEPTH, NULL, NULL, (LPBYTE)&dwVideoPixelDepth, (LPDWORD)&dwSize);
    if (error != ERROR_SUCCESS)
    {
        DEBUGMSG(1,
                 (TEXT("DDIPU GetVideoPixelDepthFromRegistry: Failed to get video pixel depth, Error 0x%X\r\n"), error));
        goto _doneVideoPixel;
    }

_doneVideoPixel:
    // Close registry key
    RegCloseKey(hKey);

    DEBUGMSG(1, (TEXT("DDIPU GetVideoPixelDepthFromRegistry: %s!\r\n"), (error == ERROR_SUCCESS) ? L"succeeds" : L"fails"));

    return dwVideoPixelDepth;
}

//------------------------------------------------------------------------------
//
// Function: BSPGetDualDeviceFromRegistry
//
// Gets dual device support from registry.
//
// Parameters:
//      None.
//
// Returns:
//      Dual Device support status.
//
//------------------------------------------------------------------------------
DWORD BSPGetDualDeviceFromRegistry(VOID)
{
    LONG  error;
    HKEY  hKey;
    DWORD dwSize;
    DWORD dwDualDevice = 0;

    // Open registry key for display driver
    error = RegOpenKeyEx(HKEY_LOCAL_MACHINE, DI1_REG_PATH, 0 , 0, &hKey);
    if (error != ERROR_SUCCESS)
    {
        DEBUGMSG(1,(TEXT("BSPGetDualDeviceFromRegistry: Failed to open reg path:%s [Error:0x%x]\r\n"), DI1_REG_PATH, error));
        goto _doneDualDevice;
    }

    // Check if dual devices are support
    dwSize = sizeof(dwDualDevice);
    error = RegQueryValueEx(hKey, DUAL_DEVICE, NULL, NULL, (LPBYTE)&dwDualDevice, (LPDWORD)&dwSize);
    if (error != ERROR_SUCCESS)
    {
        DEBUGMSG(1,
                 (TEXT("BSPGetDualDeviceFromRegistry: Failed to get TVE panel type, Error 0x%X\r\n"), error));
        goto _doneDualDevice;
    }

_doneDualDevice:
    // Close registry key
    RegCloseKey(hKey);

    DEBUGMSG(1, (TEXT("BSPGetDualDeviceFromRegistry: %s!\r\n"), (error == ERROR_SUCCESS) ? L"succeeds" : L"fails"));

    return dwDualDevice;
}


#if defined(USE_C2D_ROUTINES)
//------------------------------------------------------------------------------
//
// Function: BSPGetC2DInfoFromRegistry
//
// Gets the info of c2d from registry.
//
// Parameters:
//      None.
//
// Returns:
//      C2d information.
//
//------------------------------------------------------------------------------
DWORD BSPGetC2DInfoFromRegistry(VOID)
{
    LONG  error;
    HKEY  hKey;
    DWORD dwSize;
    DWORD dwC2DInfo = 0;
    DWORD dwC2DFlag, dwC2DThreshold;

    // Open registry key for display driver
    error = RegOpenKeyEx(HKEY_LOCAL_MACHINE, DISPLAY_REG_PATH, 0 , 0, &hKey);
    if (error != ERROR_SUCCESS)
    {
        DEBUGMSG(1,(TEXT("BSPGetC2DInfoFromRegistry: Failed to open reg path:%s [Error:0x%x]\r\n"), DISPLAY_REG_PATH, error));
        goto _doneC2DInfo;
    }

    // Get the c2d info
    dwSize = sizeof(dwC2DFlag);
    error = RegQueryValueEx(hKey, C2D_FLAG, NULL, NULL, (LPBYTE)&dwC2DFlag, (LPDWORD)&dwSize);
    if (error != ERROR_SUCCESS)
    {
        DEBUGMSG(1,
                 (TEXT("BSPGetC2DInfoFromRegistry: Failed to get C2D info, Error 0x%X\r\n"), error));
        goto _doneC2DInfo;
    }
    
    dwC2DInfo = dwC2DFlag & 0xffff;
    
    dwSize = sizeof(dwC2DThreshold);
    error = RegQueryValueEx(hKey, C2D_THRESHOLD, NULL, NULL, (LPBYTE)&dwC2DThreshold, (LPDWORD)&dwSize);
    if (error != ERROR_SUCCESS)
    {
        dwC2DThreshold = 0;
    }
    dwC2DInfo = ((dwC2DThreshold & 0xffff)<<16)| dwC2DInfo;

_doneC2DInfo:
    // Close registry key
    RegCloseKey(hKey);

    DEBUGMSG(1, (TEXT("BSPGetC2DInfoFromRegistry: %s!\r\n"), (error == ERROR_SUCCESS) ? L"succeeds" : L"fails"));
    return dwC2DInfo;
}

#endif //#if defined(USE_C2D_ROUTINES)

//------------------------------------------------------------------------------
//
// Function: BSPIsRotationSupported
//
// Returns whether rotation is supported or not. 
//
// Parameters:
//      None.
//
// Returns:
//      TRUE if rotation supported, FALSE if not.
//
//------------------------------------------------------------------------------
BOOL BSPIsRotationSupported()
{
    return BSP_DIRECTDRAW_SUPPORT_ROTATION;
}

//------------------------------------------------------------------------------
//
// Function: BSPDropFrameQuietly
//
// Returns whether drop frame quietly or not. 
//
// Parameters:
//      None.
//
// Returns:
//      TRUE if rotation supported, FALSE if not.
//
//------------------------------------------------------------------------------
BOOL BSPDropFrameQuietly()
{
    return BSP_DROP_FRAMES_QUIETLY;
}

//------------------------------------------------------------------------------
//
// Function: BSPDisableNOICPath
//
// Returns whether disalbe No IC path, When set this flag, IC will always enabled for overlay 
// processing. 
//
// Parameters:
//      None.
//
// Returns:
//      TRUE if disable, FALSE if enable.
//
//------------------------------------------------------------------------------
BOOL BSPDisableNOICPath()
{
    return BSP_DISABLE_NO_IC_PATH;
}
//------------------------------------------------------------------------------
//
// Function: BSPEnableSecondaryPrimarySurface
//
// Returns whether enable secondary primary surface, When set this flag, secondary primary
// surface will be enabled when switching the primary display deivce to TV.
// processing. 
//
// Parameters:
//      None.
//
// Returns:
//      TRUE if disable, FALSE if enable.
//
//------------------------------------------------------------------------------
BOOL BSPEnableSecondaryPrimarySurface()
{
    return BSP_ENABLE_SECONDARY_PRIMARY_SURFACE;
}

//------------------------------------------------------------------------------
//
// Function: BSPDisplayIOMUXSetup
//
// This function makes the DDK call to configure the IOMUX
// pins required for the Display.
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void BSPDisplayIOMUXSetup()
{
	// TC : IO Mux deleted because the configuration is made in the bootloader
    return;
}


//------------------------------------------------------------------------------
//
// Function: BSPInitializePanel
//
// This function performs panel-specific initialization for a
// given display panel.
//
// Parameters:
//      pPanelInfo
//          [in] Structure describing panel to initialize
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void BSPInitializePanel(PANEL_INFO *pPanelInfo)
{
    // For both panels, we must configure HSC module (MIPI controller)
    // so that Display Data pins 0-5 come from IPU and not from HSC
#if 0
    // TODO: HSC driver (and API) needed to configure the legacy bits in register 1
    UINT32* pHSCRegs;
    PHYSICAL_ADDRESS phyAddr;
    phyAddr.QuadPart = CSP_BASE_REG_PA_MIPI;
    pHSCRegs = (UINT32*)MmMapIoSpace(phyAddr, sizeof(UINT32) /*We only need to access first reg*/, FALSE);
    DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_HSC_HSP, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
    OUTREG32(pHSCRegs,0x00000F00);
    DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_HSC_HSP, DDK_CLOCK_GATE_MODE_DISABLED);
    MmUnmapIoSpace(pHSCRegs, 0x20);
#endif
    if (pPanelInfo->PORTID == DISPLAY_PORT_DVI)
    {
        DVIInitializePanel(pPanelInfo);
    }
    else if (pPanelInfo->PORTID == DISPLAY_PORT_HDMI)
    {
        HDMIInitializePanel(pPanelInfo);
    }
    else if (pPanelInfo->PORTID == DISPLAY_PORT_CHUNGHWA)
    {
        CLAAInitializePanel();
    }
    else if ((pPanelInfo->PORTID == DISPLAY_PORT_LVDS1)
            ||(pPanelInfo->PORTID == DISPLAY_PORT_LVDS2)
            ||(pPanelInfo->PORTID == DISPLAY_PORT_DLVDS))  
    {
        LVDSInitializePanel(pPanelInfo);
    }
    
}

//------------------------------------------------------------------------------
//
// Function: BSPEnablePanel
//
// This function performs panel-specific enabling for all display panels
// currently set as active.
//
// Parameters:
//      pPanelInfo
//          [in] Structure describing panel to enable
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void BSPEnablePanel(PANEL_INFO *pPanelInfo)
{
    if (pPanelInfo->PORTID == DISPLAY_PORT_DVI)
    {
        DVIEnablePanel(pPanelInfo);
    }
    else if (pPanelInfo->PORTID == DISPLAY_PORT_HDMI)
    {
        HDMIEnablePanel(pPanelInfo);
    }
    else if (pPanelInfo->PORTID == DISPLAY_PORT_CHUNGHWA)
    {
        CLAAEnablePanel();
    }
    else if ((pPanelInfo->PORTID == DISPLAY_PORT_LVDS1)
            ||(pPanelInfo->PORTID == DISPLAY_PORT_LVDS2)
            ||(pPanelInfo->PORTID == DISPLAY_PORT_DLVDS))            
    {
        LVDSEnablePanel(pPanelInfo);
    }
}

//------------------------------------------------------------------------------
//
// Function: BSPDisablePanels
//
// This function performs panel-specific disabling for all display panels
// currently set as active.
//
// Parameters:
//      pPanelInfo
//          [in] Structure describing panel to disable
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void  BSPDisablePanel(PANEL_INFO *pPanelInfo)
{

    if (pPanelInfo->PORTID == DISPLAY_PORT_DVI)
    {
        DVIDisablePanel();
    }
    else if (pPanelInfo->PORTID == DISPLAY_PORT_HDMI)
    {
        HDMIDisablePanel(pPanelInfo);
    }
    else if (pPanelInfo->PORTID == DISPLAY_PORT_CHUNGHWA)
    {
        CLAADisablePanel();
    }
    else if ((pPanelInfo->PORTID == DISPLAY_PORT_LVDS1)
            ||(pPanelInfo->PORTID == DISPLAY_PORT_LVDS2)
            ||(pPanelInfo->PORTID == DISPLAY_PORT_DLVDS))            
    {
        LVDSDisablePanel(pPanelInfo);
    }
}

//------------------------------------------------------------------------------
//
// Function: BSPEnableDisplayClock
//
// This function enables any clocks needed for operation of the given panel.
//
// Parameters:
//      pPanelInfo
//          [in] Structure describing panel whose clocks we must enable
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void BSPEnableDisplayClock(PANEL_INFO *pPanelInfo)
{
    if (pPanelInfo->DI_NUM == DI_SELECT_DI0)
    {
        // For DI0, we use HSP clock to control pixel clock, and HSP clock
        // should already be enabled by IPU submodule enable
    }
    if (pPanelInfo->DI_NUM == DI_SELECT_DI1)
    {
        // Enable DI1 clock
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU_DI1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_LDB_DI1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
    }
}

//------------------------------------------------------------------------------
//
// Function: BSPDisableDisplayClock
//
// This function disables any clocks needed for operation of the given panel.
//
// Parameters:
//      pPanelInfo
//          [in] Structure describing panel whose clocks we must disable
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void  BSPDisableDisplayClock(PANEL_INFO *pPanelInfo)
{
    if (pPanelInfo->DI_NUM == DI_SELECT_DI0)
    {
        // For DI0, we use HSP clock to control pixel clock, and HSP clock
        // should already be enabled by IPU submodule enable
    }
    if (pPanelInfo->DI_NUM == DI_SELECT_DI1)
    {
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU, DDK_CLOCK_GATE_MODE_DISABLED);
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_IPU1_IPU_DI1, DDK_CLOCK_GATE_MODE_DISABLED);
        DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_LDB_DI1, DDK_CLOCK_GATE_MODE_DISABLED);
    }
}

//------------------------------------------------------------------------------
//
// Function: BSPInitializeDC
//
// This function performs panel-specific initialization of the
// IPU DC module, including configuring panel-specific microcode routines.
//
// Parameters:
//      di_sel
//          [in] Selects either DI0 or DI1 to configure
//
//      pDIPanelInfo
//          [in] Panel info used in configuration process.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void BSPInitializeDC(DI_SELECT di_sel, PANEL_INFO* pDIPanelInfo)
{
    static UINT32 iMicroCodeAddr = 0;
    static int mappingCounter = 0;
    static DWORD dispCounter = 0; // Select from one of 4 IPU displays

    // Check dispCounter.  If it is greater than 3, something has gone wrong, since
    // there are only 4 supported display numbers in the IPU.
    if ((dispCounter > 3) && !pDIPanelInfo->ISINITIALIZED)
    {
        ERRORMSG(1, (TEXT("Error in configuration function!  Display number greater than 3!  Aborting.\r\n")));
        return;
    }

    if (pDIPanelInfo->SYNC_TYPE == IPU_PANEL_SYNC_TYPE_ASYNCHRONOUS)
    {
        // Call function to configure DC for Async panel
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:AsyncDCConfig\r\n"), __WFUNCTION__ ) );
        AsyncDCConfig(di_sel, pDIPanelInfo, &mappingCounter, &dispCounter, &iMicroCodeAddr);
    }
    else if ((pDIPanelInfo->SYNC_TYPE == IPU_PANEL_SYNC_TYPE_SYNCHRONOUS) && (pDIPanelInfo->ISINTERLACE != TRUE))
    {
        // Configure DC for Sync TV 720P/1080P (progressive).
        if( pDIPanelInfo->PORTID == DISPLAY_PORT_TV)
        {
			RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:SyncDCConfigTV\r\n"), __WFUNCTION__ ) );
            SyncDCConfigTV(di_sel, pDIPanelInfo, &mappingCounter, &dispCounter, &iMicroCodeAddr);
        }
        else
        {
            // Configuration for non-interlaced synchronous displays
            // Call function to configure DC for Sync panel (progressive)
			RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:SyncDCConfig\r\n"), __WFUNCTION__ ) );
            SyncDCConfig(di_sel, pDIPanelInfo, &mappingCounter, &dispCounter, &iMicroCodeAddr);
        }
    }
    else if ((pDIPanelInfo->SYNC_TYPE == IPU_PANEL_SYNC_TYPE_SYNCHRONOUS) && (pDIPanelInfo->ISINTERLACE == TRUE))
    {
        // Configuration for interlaced-output synchronous displays

        // Call function to configure DC for Sync panel (TV interlaced)
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:SyncDCConfigTV\r\n"), __WFUNCTION__ ) );
        SyncDCConfigTV(di_sel, pDIPanelInfo, &mappingCounter, &dispCounter, &iMicroCodeAddr);
    }

    // Configure serial communication with panel if needed, and if we haven't already done so for this panel
    if (pDIPanelInfo->USESERIALPORT && !pDIPanelInfo->ISINITIALIZED)
    {
        // Check dispCounter again.  If it is greater than 3, something has gone wrong, since
        // there are only 4 supported display numbers in the IPU.
        if (dispCounter > 3)
        {
            ERRORMSG(1, (TEXT("Error in configuration function (during serial config)!  Display number greater than 3!  Aborting.\r\n")));
            return;
        }

        // Call function to configure DC for Serial communication with panel (progressive)
		RETAILMSG(DEBUG_BSPDISPLAY, ( TEXT("%s:SerialDCConfig\r\n"), __WFUNCTION__ ) );
        SerialDCConfig(pDIPanelInfo, &mappingCounter, &dispCounter, &iMicroCodeAddr);
    }
}


//------------------------------------------------------------------------------
//
// Function: AsyncDCConfig
//
// This function configures the DC for an asynchronous panel.
//
// Parameters:
//      di_sel
//          [in] Selects either DI0 or DI1 to configure
//
//      pDIPanelInfo
//          [in] Panel info used in configuration process.
//
//      pMappingCounter
//          [in/out] Mapping counter used to configure bus mappings.  Updated
//          value will be passed back to calling function
//
//      pDispCounter
//          [in/out] Display number counter used to configure display data.  Updated
//          value will be passed back to calling function
//
//      pMicroCodeAddr
//          [in/out] Microcode address counter used to configure microcode instructions.
//          Updated value will be passed back to calling function
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void AsyncDCConfig(DI_SELECT di_sel, PANEL_INFO* pDIPanelInfo, 
    int *pMappingCounter, DWORD *pDispCounter, UINT32 *pMicroCodeAddr)
{
    MicrocodeConfigData microcodeData;
    DCWriteChanConfData writeConfData;
    DCGeneralData genData;
    DCDispConfData dispConfData;
    BusMappingData mappingData;
    static int mappingCounterStartVal;
    int i;
    static DWORD HorizAddrMappingPtr, VertAddrMappingPtr;
    static DWORD InitDataMappingPtr, InitAddrMappingPtr;
    static UINT32 microcodeStartAddr = 0;
    UINT32 tempMicrocodeAddr;

    // Use local dereferenced value for mappingCounter, dispCounter, microcodeAddr
    // Restore reference values at end of function
    int mappingCounter = *pMappingCounter;
    DWORD dispCounter = *pDispCounter;
    UINT32 iMicroCodeAddr = *pMicroCodeAddr;

    if(!pDIPanelInfo->ISINITIALIZED)
    {
        // During first time initialization of panel, 
        // we select a display number for the panel.
        pDIPanelInfo->DISPLAY_NUM = (DC_DISPLAY) dispCounter;
        dispCounter++;
    }

    //********************************************************************
    // Configure Write Channel 1 and Channel 6 (main channel to write pixels to display)
    //********************************************************************
    writeConfData.dwStartTime = 0; //not needed: no anti tearing
    writeConfData.dwChanMode = IPU_DC_WR_CH_CONF_PROG_CHAN_TYP_NORMAL_NO_ANTI_TEARING;
    writeConfData.dwEventMask = IPU_DC_CHAN_MASK_DEFAULT_HIGHEST_PRI;
    writeConfData.dwFieldMode = IPU_DC_WR_CH_CONF_FIELD_MODE_FRAME_MODE;
    writeConfData.dwDispNum = (DWORD)pDIPanelInfo->DISPLAY_NUM;
    writeConfData.dwDINum = di_sel;
    writeConfData.dwWordSize = IPU_DC_W_SIZE_24BITS;
    DCConfigureWriteChannel(DC_CHANNEL_DC_SYNC_OR_ASYNC, &writeConfData);
    DCConfigureWriteChannel(DC_CHANNEL_DP_SECONDARY, &writeConfData);
    
    // Set Write Channel 1 start address
    DCSetWriteChannelAddress(DC_CHANNEL_DC_SYNC_OR_ASYNC, 0);
    DCSetWriteChannelAddress(DC_CHANNEL_DP_SECONDARY, 0);
    
    //*********************************************
    // Configure Write Channel 5 (Disabled)
    //*********************************************
    writeConfData.dwStartTime = 0; //not needed: no anti tearing
    writeConfData.dwChanMode = IPU_DC_WR_CH_CONF_PROG_CHAN_TYP_DISABLE;
    writeConfData.dwEventMask = IPU_DC_CHAN_MASK_DEFAULT_HIGHEST_PRI;
    writeConfData.dwFieldMode = IPU_DC_WR_CH_CONF_FIELD_MODE_FRAME_MODE;
    writeConfData.dwDispNum = (DWORD)pDIPanelInfo->DISPLAY_NUM;
    writeConfData.dwDINum = di_sel ? 0 : 1; // We must use the opposite DI setting as that used for Channel 1 (to disable it)
    writeConfData.dwWordSize = IPU_DC_W_SIZE_24BITS;
    DCConfigureWriteChannel(DC_CHANNEL_5, &writeConfData);

    //**********************************************************
    // Configure Write Channel 8 (channel to initialize panel)
    //**********************************************************
    writeConfData.dwStartTime = 0; //not needed: no anti tearing
    writeConfData.dwChanMode = IPU_DC_WR_CH_CONF_PROG_CHAN_TYP_NORMAL_NO_ANTI_TEARING;
    writeConfData.dwEventMask = IPU_DC_CHAN_MASK_DEFAULT_HIGHEST_PRI;
    writeConfData.dwFieldMode = IPU_DC_WR_CH_CONF_FIELD_MODE_FRAME_MODE;
    writeConfData.dwDispNum = (DWORD)pDIPanelInfo->DISPLAY_NUM;
    writeConfData.dwDINum = di_sel;
    writeConfData.dwWordSize = IPU_DC_W_SIZE_32BITS;
    DCConfigureWriteChannel(DC_CHANNEL_8, &writeConfData);

    // Set Write Channel 8 start address
    DCSetWriteChannelAddress(DC_CHANNEL_8, 0);

    //*********************************************
    // Configure Display Port
    //*********************************************
    dispConfData.dwDispType = IPU_DC_DISP_CONF1_DISP_TYP_PARALLEL_NO_BYTE_EN;
    dispConfData.dwAddrIncrement = IPU_DC_DISP_CONF1_ADDR_INCREMENT_2;
    dispConfData.dwByteEnableAddrIncrement = IPU_DC_DISP_CONF1_ADDR_BE_L_INC_0;
    dispConfData.dwAddrCompareMode = 0;
    dispConfData.dwPollingValueAndMaskSelect = 0;
    DCConfigureDisplay(pDIPanelInfo->DISPLAY_NUM, &dispConfData);

    DCSetDisplayStride(pDIPanelInfo->DISPLAY_NUM, 0xFF); // Stride is 255

    //*********************************************
    // Configure DC General
    //*********************************************
    genData.dwBlinkEnable = IPU_DC_GEN_DC_BK_EN_DISABLE;
    genData.dwBlinkRate = 0;
    genData.dwCh5SyncSel = IPU_DC_GEN_DC_CH5_TYPE_SYNC;
    genData.dwCh1Priority = IPU_DC_GEN_SYNC_PRIORITY_1_LOW;
    genData.dwCh5Priority = IPU_DC_GEN_SYNC_PRIORITY_5_HIGH;
    genData.dwMaskChanSelect = IPU_DC_GEN_MASK4CHAN_5_DP;
    genData.dwMaskChanEnable = IPU_DC_GEN_MASK_EN_DISABLE;
    genData.dwCh1SyncSel = IPU_DC_GEN_SYNC_1_6_CH1_HANDLES_ASYNC;
    DCConfigureGeneralData(&genData);

    if(!pDIPanelInfo->ISINITIALIZED)
    {
        // This code can only be run one time, since we only want to
        // use bus mapping pointers and microcode instruction addresses once

        //*********************************************
        // Create Bus Mappings
        //*********************************************

        // Use mapping counter to assign pointers for each bus mapping
        // These pointers will be used later when creating microcode instructions
        mappingCounterStartVal = mappingCounter;

        // Create a bus mapping for each data transfer cycle
        while (mappingCounter < pDIPanelInfo->TRANSFER_CYCLES + mappingCounterStartVal)
        {
            DWORD mappingIndex = mappingCounter-mappingCounterStartVal;

            // Configure Bus Mapping Data for data
            mappingData.dwComponent2Offset = pDIPanelInfo->MAPPINGS[mappingIndex].dwComponent2Offset;
            mappingData.dwComponent2Mask = pDIPanelInfo->MAPPINGS[mappingIndex].dwComponent2Mask;
            mappingData.dwComponent1Offset = pDIPanelInfo->MAPPINGS[mappingIndex].dwComponent1Offset;
            mappingData.dwComponent1Mask = pDIPanelInfo->MAPPINGS[mappingIndex].dwComponent1Mask;
            mappingData.dwComponent0Offset = pDIPanelInfo->MAPPINGS[mappingIndex].dwComponent0Offset;
            mappingData.dwComponent0Mask = pDIPanelInfo->MAPPINGS[mappingIndex].dwComponent0Mask;
            DCConfigureDataMapping(mappingCounter, &mappingData);

            mappingCounter++;
        }

        // Configure Bus Mapping Data for horizontal start address
        mappingData.dwComponent2Offset = 0x17;
        mappingData.dwComponent2Mask = 0x00;
        mappingData.dwComponent1Offset = 0xF;
        mappingData.dwComponent1Mask = 0x00;
        mappingData.dwComponent0Offset = 0x7;
        mappingData.dwComponent0Mask = 0xFF;
        DCConfigureDataMapping(mappingCounter, &mappingData);

        HorizAddrMappingPtr = mappingCounter;
        mappingCounter++;

        // Configure Bus Mapping Data for vertical start address
        mappingData.dwComponent2Offset = 0xF;
        mappingData.dwComponent2Mask = 0x01;
        mappingData.dwComponent1Offset = 0x7;
        mappingData.dwComponent1Mask = 0xFF;
        mappingData.dwComponent0Offset = 0x17;
        mappingData.dwComponent0Mask = 0x00;
        DCConfigureDataMapping(mappingCounter, &mappingData);

        VertAddrMappingPtr = mappingCounter;
        mappingCounter++;

        // Configure Bus Mapping Data for initialization command and data
        mappingData.dwComponent2Offset = 0x17;
        mappingData.dwComponent2Mask = 0xFF;
        mappingData.dwComponent1Offset = 0x0F;
        mappingData.dwComponent1Mask = 0xFF;
        mappingData.dwComponent0Offset = 0x07;
        mappingData.dwComponent0Mask = 0xFF;
        DCConfigureDataMapping(mappingCounter, &mappingData);

        InitDataMappingPtr = mappingCounter;
        mappingCounter++;

        // Configure Bus Mapping Data for initialization command and data
        mappingData.dwComponent2Offset = 0x15;
        mappingData.dwComponent2Mask = 0xFF;
        mappingData.dwComponent1Offset = 0x0D;
        mappingData.dwComponent1Mask = 0xFF;
        mappingData.dwComponent0Offset = 0x05;
        mappingData.dwComponent0Mask = 0xFC;
        DCConfigureDataMapping(mappingCounter, &mappingData);

        InitAddrMappingPtr = mappingCounter;
        mappingCounter++;

        // Mark the microcode start address
        microcodeStartAddr = iMicroCodeAddr;

        //*******************************************************************
        // Use several Events to trigger main screen update configuration
        // NEW_LINE, NEW_FRAME, NEW_FIELD, NEW_CHAN, NEW_ADDR
        // for Channel 1 and Channel 6
        //*******************************************************************
        DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_NEW_LINE, 5, iMicroCodeAddr);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_NEW_FRAME, 4, iMicroCodeAddr);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_NEW_FIELD, 3, iMicroCodeAddr);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_END_OF_FRAME, 0, 0);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_END_OF_FIELD, 0, 0);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_END_OF_LINE, 0, 0);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_NEW_CHAN, 7, iMicroCodeAddr);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_NEW_ADDR, 6, iMicroCodeAddr);
        
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_SECONDARY, DC_EVENT_NEW_LINE, 5, iMicroCodeAddr);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_SECONDARY, DC_EVENT_NEW_FRAME, 4, iMicroCodeAddr);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_SECONDARY, DC_EVENT_NEW_FIELD, 3, iMicroCodeAddr);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_SECONDARY, DC_EVENT_END_OF_FRAME, 0, 0);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_SECONDARY, DC_EVENT_END_OF_FIELD, 0, 0);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_SECONDARY, DC_EVENT_END_OF_LINE, 0, 0);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_SECONDARY, DC_EVENT_NEW_CHAN, 7, iMicroCodeAddr);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_SECONDARY, DC_EVENT_NEW_ADDR, 6, iMicroCodeAddr);
        
        //*******************************************************************
        // Create the main screen update configuration routine
        // Here, we set up the starting horizontal and vertical addresses
        // in the panel and send the first pixel of data.
        //*******************************************************************

        //*******************************************************
        // Microcode sequence using counter
        //*******************************************************

        // Write Panel register to set Horizontal GRAM Address
        microcodeData.dwWord = iMicroCodeAddr++;
        microcodeData.dwStop = DC_TEMPLATE_STOP_CONTINUE;
        microcodeData.Command = DC_MICROCODE_COMMAND_WRG;
        microcodeData.dwLf = 0;
        microcodeData.dwAf = 0;
        microcodeData.dwOperand = ASYNC_PANEL_SET_X_POSITION_CMD;
        microcodeData.dwMapping = 0;
        microcodeData.dwWaveform = DI_COMMAND_WAVEFORM + 1;
        microcodeData.dwGluelogic = 0x8;
        microcodeData.dwSync = DC_CHANNEL_NONE;
        DCConfigureMicrocode(&microcodeData);

        // Write horizontal start address
        microcodeData.dwWord = iMicroCodeAddr++;
        microcodeData.dwStop = DC_TEMPLATE_STOP_CONTINUE;
        microcodeData.Command = DC_MICROCODE_COMMAND_WROA;
        microcodeData.dwLf = 0;
        microcodeData.dwAf = 0;
        microcodeData.dwOperand = 0;
        microcodeData.dwMapping = HorizAddrMappingPtr + 1;
        microcodeData.dwWaveform = DI_DATAWR_WAVEFORM + 1;
        microcodeData.dwGluelogic = 0x4;
        microcodeData.dwSync = DC_CHANNEL_NONE;
        DCConfigureMicrocode(&microcodeData);

        // Write Panel register to set Vertical (Y) GRAM Address
        microcodeData.dwWord = iMicroCodeAddr++;
        microcodeData.dwStop = DC_TEMPLATE_STOP_CONTINUE;
        microcodeData.Command = DC_MICROCODE_COMMAND_WRG;
        microcodeData.dwLf = 0;
        microcodeData.dwAf = 0;
        microcodeData.dwOperand = ASYNC_PANEL_SET_Y_POSITION_CMD;
        microcodeData.dwMapping = 0;
        microcodeData.dwWaveform = DI_COMMAND_WAVEFORM + 1;
        microcodeData.dwGluelogic = 0x8;
        microcodeData.dwSync = DC_CHANNEL_NONE;
        DCConfigureMicrocode(&microcodeData);

        // Write vertical start address
        microcodeData.dwWord = iMicroCodeAddr++;
        microcodeData.dwStop = DC_TEMPLATE_STOP_CONTINUE;
        microcodeData.Command = DC_MICROCODE_COMMAND_WROA;
        microcodeData.dwLf = 0;
        microcodeData.dwAf = 0;
        microcodeData.dwOperand = 0;
        microcodeData.dwMapping = VertAddrMappingPtr + 1;
        microcodeData.dwWaveform = DI_DATAWR_WAVEFORM + 1;
        microcodeData.dwGluelogic = 0x4;
        microcodeData.dwSync = DC_CHANNEL_NONE;
        DCConfigureMicrocode(&microcodeData);

        // Write Panel register to Write Data to GRAM
        microcodeData.dwWord = iMicroCodeAddr++;
        microcodeData.dwStop = DC_TEMPLATE_STOP_CONTINUE;
        microcodeData.Command = DC_MICROCODE_COMMAND_WRG;
        microcodeData.dwLf = 0;
        microcodeData.dwAf = 0;
        microcodeData.dwOperand = ASYNC_PANEL_SEND_DATA_CMD;
        microcodeData.dwMapping = 0;
        microcodeData.dwWaveform = DI_COMMAND_WAVEFORM + 1;
        microcodeData.dwGluelogic = 0x8;
        microcodeData.dwSync = DC_CHANNEL_NONE;
        DCConfigureMicrocode(&microcodeData);

        // Create a microcode instruction for each transfer cycle
        for (i= mappingCounterStartVal; i < pDIPanelInfo->TRANSFER_CYCLES + mappingCounterStartVal; i++)
        {
            // Write data to panel
            microcodeData.dwWord = iMicroCodeAddr++;
            microcodeData.dwStop = (i+1 >= pDIPanelInfo->TRANSFER_CYCLES + mappingCounterStartVal) ? 
                DC_TEMPLATE_STOP_LAST_COMMAND : DC_TEMPLATE_STOP_CONTINUE; // If this is our last cycle, end the instruction sequence
            microcodeData.Command = DC_MICROCODE_COMMAND_WROD;
            microcodeData.dwLf = 0;
            microcodeData.dwAf = 0;
            microcodeData.dwOperand = 0;
            microcodeData.dwMapping = i + 1; // i here should equal the bus mapping pointer configure above
            microcodeData.dwWaveform = DI_DATAWR_WAVEFORM + 1;
            microcodeData.dwGluelogic = 0x4;
            microcodeData.dwSync = DC_CHANNEL_NONE;
            DCConfigureMicrocode(&microcodeData);
        }


        //*******************************************************************
        // Use NEW_DATA Event to trigger write to panel
        //*******************************************************************
        // Create microcode event for DC channel 1 and channel 6 data incoming
        DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_NEW_DATA, 1, iMicroCodeAddr);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_SECONDARY, DC_EVENT_NEW_DATA, 1, iMicroCodeAddr);
        
        //*******************************************************************
        // Create the main screen update instruction.
        // Here, we send pixel data to the panel as we receive it.
        //*******************************************************************
        // Create a microcode instruction for each transfer cycle
        for (i= mappingCounterStartVal; i < pDIPanelInfo->TRANSFER_CYCLES + mappingCounterStartVal; i++)
        {
            // Write data to panel
            microcodeData.dwWord = iMicroCodeAddr++;
            microcodeData.dwStop = (i+1 >= pDIPanelInfo->TRANSFER_CYCLES + mappingCounterStartVal) ? 
                DC_TEMPLATE_STOP_LAST_COMMAND : DC_TEMPLATE_STOP_CONTINUE; // If this is our last cycle, end the instruction sequence
            microcodeData.Command = DC_MICROCODE_COMMAND_WROD;
            microcodeData.dwLf = 0;
            microcodeData.dwAf = 0;
            microcodeData.dwOperand = 0;
            microcodeData.dwMapping = i + 1;
            microcodeData.dwWaveform = DI_DATAWR_WAVEFORM + 1;
            microcodeData.dwGluelogic = 0x0;
            microcodeData.dwSync = DC_CHANNEL_NONE;
            DCConfigureMicrocode(&microcodeData);
        }

        //*******************************************************************
        // Use NEW_ADDR or NEW_CHAN events for DC Channel 8 to trigger 
        // microcode to send panel address (register) and data
        //*******************************************************************
        // Create microcode events
        DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_ADDR, DC_READ_WRITE_SELECT_WRITE,
                                    DC_REGION_SELECT_REGION1, 3, iMicroCodeAddr);
        DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_ADDR, DC_READ_WRITE_SELECT_WRITE,
                                    DC_REGION_SELECT_REGION2, 3, iMicroCodeAddr);
        DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_CHAN, DC_READ_WRITE_SELECT_WRITE,
                                    DC_REGION_SELECT_REGION1, 4, iMicroCodeAddr);
        DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_CHAN, DC_READ_WRITE_SELECT_WRITE,
                                    DC_REGION_SELECT_REGION2, 4, iMicroCodeAddr);

        // Write panel address (register to write to)
        microcodeData.dwWord = iMicroCodeAddr++;
        microcodeData.dwStop = DC_TEMPLATE_STOP_CONTINUE;
        microcodeData.Command = DC_MICROCODE_COMMAND_WROA;
        microcodeData.dwLf = 0;
        microcodeData.dwAf = 0;
        microcodeData.dwOperand = 0;
        microcodeData.dwMapping = InitAddrMappingPtr + 1;
        microcodeData.dwWaveform = DI_COMMAND_WAVEFORM + 1;
        microcodeData.dwGluelogic = 0x8;
        microcodeData.dwSync = DC_CHANNEL_NONE;
        DCConfigureMicrocode(&microcodeData);

        // Write data to panel
        microcodeData.dwWord = iMicroCodeAddr++;
        microcodeData.dwStop = DC_TEMPLATE_STOP_LAST_COMMAND;
        microcodeData.Command = DC_MICROCODE_COMMAND_WROD;
        microcodeData.dwLf = 0;
        microcodeData.dwAf = 0;
        microcodeData.dwOperand = 0;
        microcodeData.dwMapping = InitDataMappingPtr + 1;
        microcodeData.dwWaveform = DI_DATAWR_WAVEFORM + 1;
        microcodeData.dwGluelogic = 0x4;
        microcodeData.dwSync = DC_CHANNEL_NONE;
        DCConfigureMicrocode(&microcodeData);

        //*******************************************************************
        // Use NEW_DATA event for DC Channel 8 to trigger 
        // microcode to send panel data
        //*******************************************************************
        //Create microcode for dc CHANNEL 8 write data 
        DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_DATA,DC_READ_WRITE_SELECT_WRITE,
                                    DC_REGION_SELECT_REGION1, 1, iMicroCodeAddr);
        DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_DATA,DC_READ_WRITE_SELECT_WRITE,
                                    DC_REGION_SELECT_REGION2, 1, iMicroCodeAddr);

        // Write data to panel
        microcodeData.dwWord = iMicroCodeAddr++;
        microcodeData.dwStop = DC_TEMPLATE_STOP_LAST_COMMAND;
        microcodeData.Command = DC_MICROCODE_COMMAND_WROD;
        microcodeData.dwLf = 0;
        microcodeData.dwAf = 0;
        microcodeData.dwOperand = 0;
        microcodeData.dwMapping = InitDataMappingPtr + 1;
        microcodeData.dwWaveform = DI_DATAWR_WAVEFORM + 1;
        microcodeData.dwGluelogic = 0x0;
        microcodeData.dwSync = DC_CHANNEL_NONE;
        DCConfigureMicrocode(&microcodeData);

        //*******************************************************************
        // Use NEW_ADDR or NEW_CHAN events for DC Channel 8 to trigger 
        // microcode to read from panel
        //*******************************************************************
        // Create microcode events
        DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_ADDR,DC_READ_WRITE_SELECT_READ,
                                    DC_REGION_SELECT_REGION1, 3, iMicroCodeAddr);
        DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_ADDR,DC_READ_WRITE_SELECT_READ,
                                    DC_REGION_SELECT_REGION2, 3, iMicroCodeAddr);
        DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_CHAN,DC_READ_WRITE_SELECT_READ,
                                    DC_REGION_SELECT_REGION1, 4, iMicroCodeAddr);
        DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_CHAN,DC_READ_WRITE_SELECT_READ,
                                    DC_REGION_SELECT_REGION2, 4, iMicroCodeAddr);

        // Write panel address (register to read from)
        microcodeData.dwWord = iMicroCodeAddr++;
        microcodeData.dwStop = DC_TEMPLATE_STOP_CONTINUE;
        microcodeData.Command = DC_MICROCODE_COMMAND_WROA;
        microcodeData.dwLf = 0;
        microcodeData.dwAf = 0;
        microcodeData.dwOperand = 0;   //delay value in DI_CLK for display's data latching by DI
        microcodeData.dwMapping = InitAddrMappingPtr + 1;
        microcodeData.dwWaveform = DI_COMMAND_WAVEFORM + 1;
        microcodeData.dwGluelogic = 0x8;
        microcodeData.dwSync = DC_CHANNEL_NONE;
        DCConfigureMicrocode(&microcodeData);

        // Read panel data
        microcodeData.dwWord = iMicroCodeAddr++;
        microcodeData.dwStop = DC_TEMPLATE_STOP_LAST_COMMAND;
        microcodeData.Command = DC_MICROCODE_COMMAND_RD;
        microcodeData.dwLf = 0;
        microcodeData.dwAf = 0;
        microcodeData.dwOperand = 26;
        microcodeData.dwMapping = InitDataMappingPtr + 1;
        microcodeData.dwWaveform = DI_DATARD_WAVEFORM + 1;
        microcodeData.dwGluelogic = 0x4;
        microcodeData.dwSync = DC_CHANNEL_NONE;
        DCConfigureMicrocode(&microcodeData);

        //*******************************************************************
        // Use NEW_DATA event for DC Channel 8 to trigger 
        // microcode to read panel data
        //*******************************************************************
        //Create microcode for dc CHANNEL 8 read data 
        DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_DATA,DC_READ_WRITE_SELECT_READ,
                                    DC_REGION_SELECT_REGION1, 1, iMicroCodeAddr);
        DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_DATA,DC_READ_WRITE_SELECT_READ,
                                    DC_REGION_SELECT_REGION2, 1, iMicroCodeAddr);

        // Read panel data
        microcodeData.dwWord = iMicroCodeAddr++;
        microcodeData.dwStop = DC_TEMPLATE_STOP_LAST_COMMAND;
        microcodeData.Command = DC_MICROCODE_COMMAND_RD;
        microcodeData.dwLf = 0;
        microcodeData.dwAf = 0;
        microcodeData.dwOperand = 26;   //delay value in DI_CLK for display's data latching by DI
        microcodeData.dwMapping = InitDataMappingPtr + 1;
        microcodeData.dwWaveform = DI_DATARD_WAVEFORM + 1;
        microcodeData.dwGluelogic = 0;
        microcodeData.dwSync = DC_CHANNEL_NONE;
        DCConfigureMicrocode(&microcodeData);
    }
    else
    {
        // Panel has already been initialized, so we do not need to
        // create bus mappings or microcode instructions, but we do
        // need to configure microcode events

        // Grab microcode start address configure during first-time init
        tempMicrocodeAddr = microcodeStartAddr;

        //*******************************************************************
        // Use several Events to trigger main screen update configuration
        // NEW_LINE, NEW_FRAME, NEW_FIELD, NEW_CHAN, NEW_ADDR
        // for Channel 1 and Channel 6
        //*******************************************************************
        DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_NEW_LINE, 5, tempMicrocodeAddr);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_NEW_FRAME, 4, tempMicrocodeAddr);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_NEW_FIELD, 3, tempMicrocodeAddr);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_END_OF_FRAME, 0, 0);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_END_OF_FIELD, 0, 0);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_END_OF_LINE, 0, 0);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_NEW_CHAN, 7, tempMicrocodeAddr);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_NEW_ADDR, 6, tempMicrocodeAddr);
        
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_SECONDARY, DC_EVENT_NEW_LINE, 5, tempMicrocodeAddr);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_SECONDARY, DC_EVENT_NEW_FRAME, 4, tempMicrocodeAddr);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_SECONDARY, DC_EVENT_NEW_FIELD, 3, tempMicrocodeAddr);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_SECONDARY, DC_EVENT_END_OF_FRAME, 0, 0);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_SECONDARY, DC_EVENT_END_OF_FIELD, 0, 0);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_SECONDARY, DC_EVENT_END_OF_LINE, 0, 0);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_SECONDARY, DC_EVENT_NEW_CHAN, 7, tempMicrocodeAddr);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_SECONDARY, DC_EVENT_NEW_ADDR, 6, tempMicrocodeAddr);

        // Increment to point at next set of instructions
        tempMicrocodeAddr += pDIPanelInfo->TRANSFER_CYCLES + 5;

        //*******************************************************************
        // Use NEW_DATA Event to trigger write to panel
        //*******************************************************************
        // Create microcode event for DC channel 1 and channel 6 data incoming
        DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_NEW_DATA, 1, tempMicrocodeAddr);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_SECONDARY, DC_EVENT_NEW_DATA, 1, tempMicrocodeAddr);

        // Increment to point at next set of instructions
        tempMicrocodeAddr += pDIPanelInfo->TRANSFER_CYCLES;

        //*******************************************************************
        // Use NEW_ADDR or NEW_CHAN events for DC Channel 8 to trigger 
        // microcode to send panel address (register) and data
        //*******************************************************************
        // Create microcode events
        DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_ADDR, DC_READ_WRITE_SELECT_WRITE,
                                    DC_REGION_SELECT_REGION1, 3, tempMicrocodeAddr);
        DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_ADDR, DC_READ_WRITE_SELECT_WRITE,
                                    DC_REGION_SELECT_REGION2, 3, tempMicrocodeAddr);
        DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_CHAN, DC_READ_WRITE_SELECT_WRITE,
                                    DC_REGION_SELECT_REGION1, 4, tempMicrocodeAddr);
        DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_CHAN, DC_READ_WRITE_SELECT_WRITE,
                                    DC_REGION_SELECT_REGION2, 4, tempMicrocodeAddr);

        // Increment to point at next set of instructions
        tempMicrocodeAddr += 2;

        //*******************************************************************
        // Use NEW_DATA event for DC Channel 8 to trigger 
        // microcode to send panel data
        //*******************************************************************
        //Create microcode for dc CHANNEL 8 write data 
        DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_DATA,DC_READ_WRITE_SELECT_WRITE,
                                    DC_REGION_SELECT_REGION1, 1, tempMicrocodeAddr);
        DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_DATA,DC_READ_WRITE_SELECT_WRITE,
                                    DC_REGION_SELECT_REGION2, 1, tempMicrocodeAddr);

        // Increment to point at next set of instructions
        tempMicrocodeAddr += 1;

        //*******************************************************************
        // Use NEW_ADDR or NEW_CHAN events for DC Channel 8 to trigger 
        // microcode to read from panel
        //*******************************************************************
        // Create microcode events
        DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_ADDR,DC_READ_WRITE_SELECT_READ,
                                    DC_REGION_SELECT_REGION1, 3, tempMicrocodeAddr);
        DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_ADDR,DC_READ_WRITE_SELECT_READ,
                                    DC_REGION_SELECT_REGION2, 3, tempMicrocodeAddr);
        DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_CHAN,DC_READ_WRITE_SELECT_READ,
                                    DC_REGION_SELECT_REGION1, 4, tempMicrocodeAddr);
        DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_CHAN,DC_READ_WRITE_SELECT_READ,
                                    DC_REGION_SELECT_REGION2, 4, tempMicrocodeAddr);

        // Increment to point at next set of instructions
        tempMicrocodeAddr += 2;

        //*******************************************************************
        // Use NEW_DATA event for DC Channel 8 to trigger 
        // microcode to read panel data
        //*******************************************************************
        //Create microcode for dc CHANNEL 8 read data 
        DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_DATA,DC_READ_WRITE_SELECT_READ,
                                    DC_REGION_SELECT_REGION1, 1, tempMicrocodeAddr);
        DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_DATA,DC_READ_WRITE_SELECT_READ,
                                    DC_REGION_SELECT_REGION2, 1, tempMicrocodeAddr);
    }

    //-------------------------------------------------------------------------

    // Disable Events 0-3
    DCDisableUserEventData(DC_USER_GENERAL_DATA_EVENT_0);
    DCDisableUserEventData(DC_USER_GENERAL_DATA_EVENT_1);
    DCDisableUserEventData(DC_USER_GENERAL_DATA_EVENT_2);
    DCDisableUserEventData(DC_USER_GENERAL_DATA_EVENT_3);

    // Set ISINITIALIZED to TRUE, unless we still need to configure serial setup, in
    // which case we wait and allow it to be set to TRUE after serial setup.
    if (!pDIPanelInfo->USESERIALPORT)
    {
        pDIPanelInfo->ISINITIALIZED = TRUE;
        for(i= 0;i < NUM_SUPPORTED_PANELS; i++)
        {
            if(g_PanelArray[i].PORTID == pDIPanelInfo->PORTID)
            {
                g_PanelArray[i].ISINITIALIZED = TRUE;
                g_PanelArray[i].DISPLAY_NUM = pDIPanelInfo->DISPLAY_NUM;
            }
        }
    }

    // Restore reference values
    *pMappingCounter = mappingCounter;
    *pDispCounter = dispCounter;
    *pMicroCodeAddr = iMicroCodeAddr;
}


//------------------------------------------------------------------------------
//
// Function: SyncDCConfig
//
// This function configures the DC for a Synchronous panel taking
// non-interlaced (progressive) data.
//
// Parameters:
//      di_sel
//          [in] Selects either DI0 or DI1 to configure
//
//      pDIPanelInfo
//          [in] Panel info used in configuration process.
//
//      pMappingCounter
//          [in/out] Mapping counter used to configure bus mappings.  Updated
//          value will be passed back to calling function
//
//      pDispCounter
//          [in/out] Display number counter used to configure display data.  Updated
//          value will be passed back to calling function
//
//      pMicroCodeAddr
//          [in/out] Microcode address counter used to configure microcode instructions.
//          Updated value will be passed back to calling function
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void SyncDCConfig(DI_SELECT di_sel, PANEL_INFO* pDIPanelInfo, 
    int *pMappingCounter, DWORD *pDispCounter, UINT32 *pMicroCodeAddr)
{
    MicrocodeConfigData microcodeData;
    DCWriteChanConfData writeConfData;
    DCGeneralData genData;
    DCDispConfData dispConfData;
    BusMappingData mappingData;
    static int mappingCounterStartVal;
    int i;
    static UINT32 microcodeStartAddr[2] = {0};
    UINT32 tempMicrocodeAddr;

    // Use local dereferenced value for mappingCounter, dispCounter, microcodeAddr
    // Restore reference values at end of function
    int mappingCounter = *pMappingCounter;
    DWORD dispCounter = *pDispCounter;
    UINT32 iMicroCodeAddr = *pMicroCodeAddr;

    if(!pDIPanelInfo->ISINITIALIZED)
    {
        // During first time initialization of panel, 
        // we select a display number for the panel.
        pDIPanelInfo->DISPLAY_NUM = (DC_DISPLAY) dispCounter;
        dispCounter++;
    }

    //********************************************************************
    // Configure Write Channel 5(main channel to write pixels to display)
    //********************************************************************
    writeConfData.dwStartTime = 0; //not needed: no anti tearing
    writeConfData.dwChanMode = IPU_DC_WR_CH_CONF_PROG_CHAN_TYP_DISABLE;
    writeConfData.dwEventMask = IPU_DC_CHAN_MASK_DEFAULT_HIGHEST_PRI;
    writeConfData.dwFieldMode = IPU_DC_WR_CH_CONF_FIELD_MODE_FRAME_MODE;
    writeConfData.dwDispNum = (DWORD)pDIPanelInfo->DISPLAY_NUM;
    writeConfData.dwDINum = di_sel;
    writeConfData.dwWordSize = IPU_DC_W_SIZE_24BITS;
    DCConfigureWriteChannel(DC_CHANNEL_DP_PRIMARY, &writeConfData);
    // Set Write Channel 5 start address
    DCSetWriteChannelAddress(DC_CHANNEL_DP_PRIMARY, 0);


    if(di_sel == DI_SELECT_DI0)
    {
        //*********************************************
        // Configure Write Channel 1 (Disabled it for DI0)
        //*********************************************
        writeConfData.dwStartTime = 0; //not needed: no anti tearing
        writeConfData.dwChanMode = IPU_DC_WR_CH_CONF_PROG_CHAN_TYP_DISABLE;
        writeConfData.dwEventMask = IPU_DC_CHAN_MASK_DEFAULT_HIGHEST_PRI;
        writeConfData.dwFieldMode = IPU_DC_WR_CH_CONF_FIELD_MODE_FRAME_MODE;
        writeConfData.dwDispNum = (DWORD)pDIPanelInfo->DISPLAY_NUM;
        writeConfData.dwDINum = 0;  // We must use the opposite DI setting as that used for Channel 1 (to disable it)
        writeConfData.dwWordSize = IPU_DC_W_SIZE_24BITS;
        DCConfigureWriteChannel(DC_CHANNEL_DC_SYNC_OR_ASYNC, &writeConfData);
        DCSetWriteChannelAddress(DC_CHANNEL_DC_SYNC_OR_ASYNC, 0);    
    }
    else
    {

        //*********************************************
        // Configure Write Channel 1 (Eanbled it for DI0, channel 5 is enabled for DI1) 
        //*********************************************
        DCChangeChannelMode(DC_CHANNEL_DC_SYNC_OR_ASYNC, 
            IPU_DC_WR_CH_CONF_PROG_CHAN_TYP_NORMAL_NO_ANTI_TEARING);
    }
    
    //*********************************************
    // Configure Display port
    //*********************************************
    dispConfData.dwDispType = IPU_DC_DISP_CONF1_DISP_TYP_PARALLEL_NO_BYTE_EN;
    dispConfData.dwAddrIncrement = IPU_DC_DISP_CONF1_ADDR_INCREMENT_1;
    dispConfData.dwByteEnableAddrIncrement = IPU_DC_DISP_CONF1_ADDR_BE_L_INC_0;
    dispConfData.dwAddrCompareMode = 0;
    dispConfData.dwPollingValueAndMaskSelect = 0;
    DCConfigureDisplay(pDIPanelInfo->DISPLAY_NUM, &dispConfData);

    DCSetDisplayStride(pDIPanelInfo->DISPLAY_NUM, pDIPanelInfo->WIDTH);

    //*********************************************
    // Configure DC General
    //*********************************************
    genData.dwBlinkEnable = IPU_DC_GEN_DC_BK_EN_DISABLE;
    genData.dwBlinkRate = 0;
    genData.dwCh5SyncSel = IPU_DC_GEN_DC_CH5_TYPE_SYNC;
    genData.dwCh1Priority = IPU_DC_GEN_SYNC_PRIORITY_1_LOW;
    genData.dwCh5Priority = IPU_DC_GEN_SYNC_PRIORITY_5_HIGH;
    genData.dwMaskChanSelect = IPU_DC_GEN_MASK4CHAN_5_DC;
    genData.dwMaskChanEnable = IPU_DC_GEN_MASK_EN_DISABLE;
    genData.dwCh1SyncSel = IPU_DC_GEN_SYNC_1_6_CH1_HANDLES_SYNC;
    DCConfigureGeneralData(&genData);

    //*********************************************
    // User events configuration
    //*********************************************
    // Disable Events 0-3
    DCDisableUserEventData(DC_USER_GENERAL_DATA_EVENT_0);
    DCDisableUserEventData(DC_USER_GENERAL_DATA_EVENT_1);
    DCDisableUserEventData(DC_USER_GENERAL_DATA_EVENT_2);
    DCDisableUserEventData(DC_USER_GENERAL_DATA_EVENT_3);


    //*********************************************
    // Create Bus Mappings
    //*********************************************
    // Configure Bus Mapping Data for data
    // This code can only be run one time for each panel.
    if(!pDIPanelInfo->ISINITIALIZED)
    {
        // Use mapping counter to assign pointers for each bus mapping
        // These pointers will be used later when creating microcode instructions
        mappingCounterStartVal = mappingCounter;

        // Create a bus mapping for each data transfer cycle
        while (mappingCounter < pDIPanelInfo->TRANSFER_CYCLES + mappingCounterStartVal)
        {
            DWORD mappingIndex = mappingCounter-mappingCounterStartVal;

            // Configure Bus Mapping Data for data
            mappingData.dwComponent2Offset = pDIPanelInfo->MAPPINGS[mappingIndex].dwComponent2Offset;
            mappingData.dwComponent2Mask = pDIPanelInfo->MAPPINGS[mappingIndex].dwComponent2Mask;
            mappingData.dwComponent1Offset = pDIPanelInfo->MAPPINGS[mappingIndex].dwComponent1Offset;
            mappingData.dwComponent1Mask = pDIPanelInfo->MAPPINGS[mappingIndex].dwComponent1Mask;
            mappingData.dwComponent0Offset = pDIPanelInfo->MAPPINGS[mappingIndex].dwComponent0Offset;
            mappingData.dwComponent0Mask = pDIPanelInfo->MAPPINGS[mappingIndex].dwComponent0Mask;
            DCConfigureDataMapping(mappingCounter, &mappingData);

            mappingCounter++;
        }

        // Mark the microcode start address
        microcodeStartAddr[di_sel] = iMicroCodeAddr;

        //*******************************************************************
        // Use several Events to trigger main screen update configuration
        // NEW_LINE
        //*******************************************************************
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_NEW_LINE, 3, iMicroCodeAddr);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_NEW_FRAME, 0, 0);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_NEW_FIELD, 0, 0);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_END_OF_FRAME, 0, 0);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_END_OF_FIELD, 0, 0);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_NEW_CHAN, 0, 0);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_NEW_ADDR, 0, 0);
        if(di_sel == DI_SELECT_DI0)
        {
            //DC channel is only configured for DI0
            DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_NEW_LINE, 3, iMicroCodeAddr);
            DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_NEW_FRAME, 0, 0);
            DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_NEW_FIELD, 0, 0);
            DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_END_OF_FRAME, 0, 0);
            DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_END_OF_FIELD, 0, 0);
            DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_NEW_CHAN, 0, 0);
            DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_NEW_ADDR, 0, 0);
        }
        //*******************************************************
        // Microcode sequence to refresh sync panel
        //*******************************************************

        // Create a microcode instruction for each transfer cycle
        for (i= mappingCounterStartVal; i < pDIPanelInfo->TRANSFER_CYCLES + mappingCounterStartVal; i++)
        {
            // Write Data to Sync panel
            microcodeData.dwWord = iMicroCodeAddr++;
            microcodeData.dwStop = (i+1 >= pDIPanelInfo->TRANSFER_CYCLES + mappingCounterStartVal) ?
                DC_TEMPLATE_STOP_LAST_COMMAND : DC_TEMPLATE_STOP_CONTINUE; // If this is our last cycle, end the instruction sequence
            microcodeData.Command = DC_MICROCODE_COMMAND_WROD;
            microcodeData.dwLf = 0;
            microcodeData.dwAf = 0;
            microcodeData.dwOperand = 0;
            microcodeData.dwMapping = i + 1; // i here should equal the bus mapping pointer configured above
            microcodeData.dwWaveform = DI_SDC_WAVEFORM + 1;
            microcodeData.dwGluelogic = 0x8;
            microcodeData.dwSync = DI_COUNTER_ACLOCK;
            DCConfigureMicrocode(&microcodeData);
        }

        //*******************************************************************
        // Use several Events to trigger main screen update configuration
        // NEW_LINE
        //*******************************************************************
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_END_OF_LINE, 2, iMicroCodeAddr);
        if(di_sel == DI_SELECT_DI0)
        {
            //DC channel is only configured for DI0
            DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_END_OF_LINE, 2, iMicroCodeAddr);
        }

        if(pDIPanelInfo->PORTID != DISPLAY_PORT_VGA)
        {
            // Create a microcode instruction for each transfer cycle
            for (i= mappingCounterStartVal; i < pDIPanelInfo->TRANSFER_CYCLES + mappingCounterStartVal; i++)
            {
                // Write Data to Sync panel
                microcodeData.dwWord = iMicroCodeAddr++;
                microcodeData.dwStop = (i+1 >= pDIPanelInfo->TRANSFER_CYCLES + mappingCounterStartVal) ?
                    DC_TEMPLATE_STOP_LAST_COMMAND : DC_TEMPLATE_STOP_CONTINUE; // If this is our last cycle, end the instruction sequence
                microcodeData.Command = DC_MICROCODE_COMMAND_WROD;
                microcodeData.dwLf = 0;
                microcodeData.dwAf = 0;
                microcodeData.dwOperand = 0;
                microcodeData.dwMapping = i + 1; // i here should equal the bus mapping pointer configured above
                microcodeData.dwWaveform = DI_SDC_WAVEFORM + 1;
                microcodeData.dwGluelogic = 0x4;
                microcodeData.dwSync = DI_COUNTER_ACLOCK;
                DCConfigureMicrocode(&microcodeData);
            }
        }
        else
        {
             microcodeData.dwWord = iMicroCodeAddr++;
             microcodeData.dwStop = DC_TEMPLATE_STOP_CONTINUE;
             microcodeData.Command = DC_MICROCODE_COMMAND_WROD;
             microcodeData.dwLf = 0;
             microcodeData.dwAf = 0;
             microcodeData.dwOperand = 0;
             microcodeData.dwMapping = mappingCounterStartVal + 1; 
             microcodeData.dwWaveform = DI_SDC_WAVEFORM + 1;
             microcodeData.dwGluelogic = 4;
             microcodeData.dwSync = DI_COUNTER_ACLOCK;
             DCConfigureMicrocode(&microcodeData);
                 
             microcodeData.dwWord = iMicroCodeAddr++;
             microcodeData.dwStop = DC_TEMPLATE_STOP_LAST_COMMAND;
             microcodeData.Command = DC_MICROCODE_COMMAND_WRG;
             microcodeData.dwLf = 0;
             microcodeData.dwAf = 0;
             microcodeData.dwOperand = 0;
             microcodeData.dwMapping = mappingCounterStartVal + 1; 
             microcodeData.dwWaveform = DI_SDC_WAVEFORM + 1;
             microcodeData.dwGluelogic = 0;
             microcodeData.dwSync = DI_COUNTER_BASECLK;
            DCConfigureMicrocode(&microcodeData);
        }

        //*******************************************************************
        // Use NEW_DATA Event to trigger write to panel
        //*******************************************************************
        // Create microcode event for DC channel 1 data incoming
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_NEW_DATA, 1, iMicroCodeAddr);

        if(di_sel == DI_SELECT_DI0)
        {
            //DC channel is only configured for DI0
            DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_NEW_DATA, 1, iMicroCodeAddr);
        }
        //*******************************************************************
        // Create the main screen update instruction.
        // Here, we send pixel data to the panel as we receive it.
        //*******************************************************************
        // Create a microcode instruction for each transfer cycle
        for (i= mappingCounterStartVal; i < pDIPanelInfo->TRANSFER_CYCLES + mappingCounterStartVal; i++)
        {
            // Write Data to Sync panel
            microcodeData.dwWord = iMicroCodeAddr++;
            microcodeData.dwStop = (i+1 >= pDIPanelInfo->TRANSFER_CYCLES + mappingCounterStartVal) ?
                DC_TEMPLATE_STOP_LAST_COMMAND : DC_TEMPLATE_STOP_CONTINUE; // If this is our last cycle, end the instruction sequence
            microcodeData.Command = DC_MICROCODE_COMMAND_WROD;
            microcodeData.dwLf = 0;
            microcodeData.dwAf = 0;
            microcodeData.dwOperand = 0;
            microcodeData.dwMapping = i + 1; // i here should equal the bus mapping pointer configured above
            microcodeData.dwWaveform = DI_SDC_WAVEFORM + 1;
            microcodeData.dwGluelogic = 0x0;
            microcodeData.dwSync = DI_COUNTER_ACLOCK;
            DCConfigureMicrocode(&microcodeData);
        }
    }
    else
    {
        // Panel has already been initialized, so we do not need to
        // create bus mappings or microcode instructions, but we do
        // need to configure microcode events

        // Grab microcode start address configure during first-time init
        tempMicrocodeAddr = microcodeStartAddr[di_sel];

        //*******************************************************************
        // Use several Events to trigger main screen update configuration
        // NEW_LINE
        //*******************************************************************
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_NEW_LINE, 3, tempMicrocodeAddr);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_NEW_FRAME, 0, 0);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_NEW_FIELD, 0, 0);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_END_OF_FRAME, 0, 0);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_END_OF_FIELD, 0, 0);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_NEW_CHAN, 0, 0);
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_NEW_ADDR, 0, 0);
        if(di_sel == DI_SELECT_DI0)
        {
            //DC channel is only configured for DI0
            DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_NEW_LINE, 3, tempMicrocodeAddr);
            DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_NEW_FRAME, 0, 0);
            DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_NEW_FIELD, 0, 0);
            DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_END_OF_FRAME, 0, 0);
            DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_END_OF_FIELD, 0, 0);
            DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_NEW_CHAN, 0, 0);
            DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_NEW_ADDR, 0, 0);
        }
        // Increment to point at next set of instructions
        tempMicrocodeAddr += pDIPanelInfo->TRANSFER_CYCLES;

        //*******************************************************************
        // Use several Events to trigger main screen update configuration
        // NEW_LINE
        //*******************************************************************
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_END_OF_LINE, 2, tempMicrocodeAddr);
        if(di_sel == DI_SELECT_DI0)
        {
            //DC channel is only configured for DI0
            DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_END_OF_LINE, 2, tempMicrocodeAddr);
        }
        if(pDIPanelInfo->PORTID != DISPLAY_PORT_VGA)
        {
        // Increment to point at next set of instructions
        tempMicrocodeAddr += pDIPanelInfo->TRANSFER_CYCLES;
        }
        else
            tempMicrocodeAddr += 2;
        //*******************************************************************
        // Use NEW_DATA Event to trigger write to panel
        //*******************************************************************
        // Create microcode event for DC channel 1 data incoming
        DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_NEW_DATA, 1, tempMicrocodeAddr);
        if(di_sel == DI_SELECT_DI0)
        {
            //DC channel is only configured for DI0
            DCConfigureMicrocodeEvent(DC_CHANNEL_DC_SYNC_OR_ASYNC, DC_EVENT_NEW_DATA, 1, tempMicrocodeAddr);
        }
    }

    // Set ISINITIALIZED to TRUE, unless we still need to configure serial setup, in
    // which case we wait and allow it to be set to TRUE after serial setup.
    if (!pDIPanelInfo->USESERIALPORT)
    {
        pDIPanelInfo->ISINITIALIZED = TRUE;
        // For the same port panel, it's no necessary to initialize them again. For most case, the only difference
        // between them is the resolution and refresh rate.
        for(i= 0;i < NUM_SUPPORTED_PANELS; i++)
        {
            if(g_PanelArray[i].PORTID == pDIPanelInfo->PORTID)
            {
                g_PanelArray[i].ISINITIALIZED = TRUE;
                g_PanelArray[i].DISPLAY_NUM = pDIPanelInfo->DISPLAY_NUM;
            }
        }
    }

    // Restore reference values
    *pMappingCounter = mappingCounter;
    *pDispCounter = dispCounter;
    *pMicroCodeAddr = iMicroCodeAddr;
}

//------------------------------------------------------------------------------
//
// Function: SyncDCConfigTV
//
// This function configures the DC for a Synchronous panel taking both interlaced data and progressive data.
//
// Parameters:
//      di_sel
//          [in] Selects either DI0 or DI1 to configure
//
//      pDIPanelInfo
//          [in] Panel info used in configuration process.
//
//      pMappingCounter
//          [in/out] Mapping counter used to configure bus mappings.  Updated
//          value will be passed back to calling function
//
//      pDispCounter
//          [in/out] Display number counter used to configure display data.  Updated
//          value will be passed back to calling function
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void SyncDCConfigTV(DI_SELECT di_sel, PANEL_INFO* pDIPanelInfo, 
    int *pMappingCounter, DWORD *pDispCounter, UINT32 *pMicroCodeAddr)
{
    MicrocodeConfigData microcodeData;
    DCWriteChanConfData writeConfData;
    DCGeneralData genData;
    DCDispConfData dispConfData;
    BusMappingData mappingData;
    static BOOL bAlreadyInit = FALSE;
    static DWORD DIDispNum = 0;
    static int mappingCounterStartVal;
    static UINT32 microcodeStartAddr = 0;
    UINT32 tempMicrocodeAddr;

    // Use local dereferenced value for mappingCounter, dispCounter, microcodeAddr
    // Restore reference values at end of function
    int mappingCounter = *pMappingCounter;
    DWORD dispCounter = *pDispCounter;
    UINT32 iMicroCodeAddr = *pMicroCodeAddr;

    // For interlaced displays, we can switch back and forth between panel types
    // (e.g. NTSC <-> PAL) many times, so we must reuse hardware resources, including:
    // - Mapping pointers
    // - display number
    // - microcode addresses
    // Thus, put the configuration of these things in a code segment that is guaranteed
    // to run only one time.
    if (!bAlreadyInit)
    {
        // First time initialization for any interlaced display type

        // During first time initialization of panel, 
        // we select a display number for the panel.
        pDIPanelInfo->DISPLAY_NUM = (DC_DISPLAY) dispCounter;
        DIDispNum = dispCounter;
        dispCounter++;

        // Configure Bus Mapping Data for data
        // This code can only be run one time for interlaced displays.

        // Use mapping counter to assign pointers for each bus mapping
        // These pointers will be used later when creating microcode instructions
        mappingCounterStartVal = mappingCounter;

        // Configure Bus Mapping Data   
        mappingData.dwComponent2Offset = 0x7;
        mappingData.dwComponent2Mask   = 0xFF;
        mappingData.dwComponent1Offset = 0x17; 
        mappingData.dwComponent1Mask   = 0xFF;
        mappingData.dwComponent0Offset = 0xF; 
        mappingData.dwComponent0Mask   = 0xFF;
        DCConfigureDataMapping(mappingCounter, &mappingData);

        mappingCounter++;

        // Mark the microcode start address
        microcodeStartAddr = iMicroCodeAddr;

        //*********************************************
        // Configure Microcode for TVE
        //*********************************************
        microcodeData.dwWord = iMicroCodeAddr++;
        microcodeData.dwStop = DC_TEMPLATE_STOP_LAST_COMMAND;
        microcodeData.Command = DC_MICROCODE_COMMAND_WROD;
        microcodeData.dwLf = 0;
        microcodeData.dwAf = 0;
        microcodeData.dwOperand = 0;
        microcodeData.dwMapping = mappingCounterStartVal + 1; 
        microcodeData.dwWaveform = DI_TVE_WAVEFORM + 1;
        microcodeData.dwGluelogic = 0;
        microcodeData.dwSync = DC_CHANNEL_8;
        DCConfigureMicrocode(&microcodeData);
        
        microcodeData.dwWord = iMicroCodeAddr++;
        microcodeData.dwStop = DC_TEMPLATE_STOP_LAST_COMMAND;
        microcodeData.Command = DC_MICROCODE_COMMAND_WROD;
        microcodeData.dwLf = 0;
        microcodeData.dwAf = 0;
        microcodeData.dwOperand = 0;
        microcodeData.dwMapping = mappingCounterStartVal + 1; 
        microcodeData.dwWaveform = DI_TVE_WAVEFORM + 1;
        microcodeData.dwGluelogic = 0;
        microcodeData.dwSync = DC_CHANNEL_8;
        DCConfigureMicrocode(&microcodeData);

        bAlreadyInit = TRUE;
    }
    else
    {
        // First time init for this specific interlaced display...other(s) have already been initialized.

        // During first time initialization of panel, 
        // we set the display number for the panel.  In this case,
        // we reuse the display number from the previous interlaced
        // display type
        pDIPanelInfo->DISPLAY_NUM = (DC_DISPLAY) DIDispNum;
    }

    // Write Channel configuration data 
    writeConfData.dwStartTime = 0;
    writeConfData.dwChanMode  = IPU_DC_WR_CH_CONF_PROG_CHAN_TYP_DISABLE; // 4 -- normal mode without anti-treaing
    writeConfData.dwEventMask = IPU_DC_CHAN_MASK_DEFAULT_HIGHEST_PRI;                   // 0 -- just higher priority event will be served
    
    if (pDIPanelInfo->ISINTERLACE)
    {
        writeConfData.dwFieldMode = IPU_DC_WR_CH_CONF_FIELD_MODE_FIELD_MODE;            // 1 -- field mode enable
    }
    else
    {
        writeConfData.dwFieldMode = IPU_DC_WR_CH_CONF_FIELD_MODE_FRAME_MODE;            // 0 -- frame mode enable
    }
    
    writeConfData.dwDispNum   = (DWORD)pDIPanelInfo->DISPLAY_NUM;                       // Display Number in IPU
    writeConfData.dwDINum     = di_sel;                                                 // select DI0 or DI1
    writeConfData.dwWordSize  = IPU_DC_W_SIZE_24BITS;                                   // 2 -- component size access to DC set to 24bit

    DCConfigureWriteChannel(DC_CHANNEL_5, &writeConfData);

    // Set Write Channel 5 start addresss
    DCSetWriteChannelAddress(DC_CHANNEL_5, 0); 

    //*********************************************
    // Configure Write Channel 1 (Enable)
    //*********************************************
    // We must set the DI for CH5 to DI1 if we are using CH1 for DI0.
    DCChangeChannelMode(DC_CHANNEL_DC_SYNC_OR_ASYNC, 
        IPU_DC_WR_CH_CONF_PROG_CHAN_TYP_NORMAL_NO_ANTI_TEARING);

    // Configure General Data Register
    genData.dwBlinkEnable = IPU_DC_GEN_DC_BK_EN_DISABLE;
    genData.dwBlinkRate = 0;
    genData.dwCh5SyncSel = IPU_DC_GEN_DC_CH5_TYPE_SYNC;             // 1 -- alterbnate sync or asyn flow
    genData.dwCh1Priority = IPU_DC_GEN_SYNC_PRIORITY_1_LOW;         
    genData.dwCh5Priority = IPU_DC_GEN_SYNC_PRIORITY_5_HIGH;        // 1 -- sets the priority of channel #5 to high
    genData.dwMaskChanSelect = IPU_DC_GEN_MASK4CHAN_5_DP;           // mask channel is associated the sync flow via DC (without DP)
    genData.dwMaskChanEnable = IPU_DC_GEN_MASK_EN_DISABLE;          // 0 -- mask channel is disabled
    genData.dwCh1SyncSel = IPU_DC_GEN_SYNC_1_6_CH1_HANDLES_SYNC;    // 2 -- channel 1 of the DC handless sync flow

    DCConfigureGeneralData(&genData);


    // Configure DISP_CONF register
    dispConfData.dwPollingValueAndMaskSelect = 0;   // Ignored
    dispConfData.dwAddrCompareMode           = 1;   
    dispConfData.dwByteEnableAddrIncrement   = IPU_DC_DISP_CONF1_ADDR_BE_L_INC_0;
    dispConfData.dwAddrIncrement             = IPU_DC_DISP_CONF1_ADDR_INCREMENT_1;              // auto address increase by 1
    dispConfData.dwDispType                  = IPU_DC_DISP_CONF1_DISP_TYP_PARALLEL_NO_BYTE_EN;  // paralel display without byte enable

    DCConfigureDisplay(pDIPanelInfo->DISPLAY_NUM, &dispConfData);

    // Set stride line
    DCSetDisplayStride(pDIPanelInfo->DISPLAY_NUM, pDIPanelInfo->WIDTH); 

    // Panel has already been initialized, so we do not need to
    // create bus mappings or microcode instructions, but we do
    // need to configure microcode events

    // Grab microcode start address configure during first-time init
    tempMicrocodeAddr = microcodeStartAddr;

    //*******************************************************************
    // Use several Events to trigger main screen update configuration
    // NEW_LINE
    //*******************************************************************
    DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_NEW_LINE, 3, tempMicrocodeAddr);
    DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_END_OF_LINE, 2, tempMicrocodeAddr);
    DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_NEW_FRAME, 0, 0);
    DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_NEW_FIELD, 0, 0);
    DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_END_OF_FRAME, 0, 0);
    DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_END_OF_FIELD, 0, 0);
    DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_NEW_CHAN, 0, 0);
    DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_NEW_ADDR, 0, 0);
    DCConfigureMicrocodeEvent(DC_CHANNEL_DP_PRIMARY, DC_EVENT_NEW_DATA, 1, tempMicrocodeAddr);

    // Configure User General Data Event 
    DCUserEventConfData userEventData;
 
    userEventData.dwCounterCompare            = 0;                                    // every data
    userEventData.dwCounterOffset             = 0;                                    // no offset
    userEventData.dwEventNumber               = pDIPanelInfo->WIDTH -1;
    userEventData.dwCounterTriggerSelect      = IPU_DC_UGDE_0_NF_NL_NEW_LINE;         // NL->0, NFrame->1, NFIELD->2
    userEventData.dwAutorestartEnable         = IPU_DC_UGDE_0_AUTO_RESTART_DISABLE;   // no autorestart
    userEventData.dwOddModeEnable             = IPU_DC_UGDE_0_ODD_EN_ENABLE;          // enable "odd"
    userEventData.dwOddModeMicrocodeStartAddr = microcodeStartAddr;                   // word 1 1st part
    userEventData.dwMicrocodeStartAddr        = microcodeStartAddr+1;                 // word 2 2nd part
    userEventData.dwEventPriority             = 1;                                    // enabled. All others are disabled.
    userEventData.dwEventDCChan               = DC_CHANNEL_DP_PRIMARY;                // IDMAC Ch 23

    DCConfigureUserEventData(DC_USER_GENERAL_DATA_EVENT_0, &userEventData);
 
    // Disable Events 1-3
    DCDisableUserEventData(DC_USER_GENERAL_DATA_EVENT_1);
    DCDisableUserEventData(DC_USER_GENERAL_DATA_EVENT_2); 
    DCDisableUserEventData(DC_USER_GENERAL_DATA_EVENT_3); 

    // Set ISINITIALIZED to TRUE, unless we still need to configure serial setup, in
    // which case we wait and allow it to be set to TRUE after serial setup.
    // For TV, .ISINITIALIZED is not used.
    if (!pDIPanelInfo->USESERIALPORT)
    {
        pDIPanelInfo->ISINITIALIZED = TRUE;
    }

    // Restore reference values
    *pMappingCounter = mappingCounter;
    *pDispCounter = dispCounter;
    *pMicroCodeAddr = iMicroCodeAddr;
}

//------------------------------------------------------------------------------
//
// Function: SerialDCConfig
//
// This function configures the DC for IPU serial communication with a panel.
//
// Parameters:
//      di_sel
//          [in] Selects either DI0 or DI1 to configure
//
//      pDIPanelInfo
//          [in] Panel info used in configuration process.
//
//      pMappingCounter
//          [in/out] Mapping counter used to configure bus mappings.  Updated
//          value will be passed back to calling function
//
//      pDispCounter
//          [in/out] Display number counter used to configure display data.  Updated
//          value will be passed back to calling function
//
//      pMicroCodeAddr
//          [in/out] Microcode address counter used to configure microcode instructions.
//          Updated value will be passed back to calling function
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void SerialDCConfig(PANEL_INFO* pDIPanelInfo, int *pMappingCounter, DWORD *pDispCounter, UINT32 *pMicroCodeAddr)
{
    MicrocodeConfigData microcodeData;
    DCWriteChanConfData writeConfData;
    DCDispConfData dispConfData;
    BusMappingData mappingData;
    int mappingCounterStartVal;
    int i;

    // Use local dereferenced value for mappingCounter, dispCounter, microcodeAddr
    // Restore reference values at end of function
    int mappingCounter = *pMappingCounter;
    DWORD dispCounter = *pDispCounter;
    UINT32 iMicroCodeAddr = *pMicroCodeAddr;

    //*********************************************
    // Create Bus Mappings
    //*********************************************
    // Configure Bus Mapping Data for data
    // This code can only be run one time for each panel.

    // We don't need to check ISINITIALIZED here, since all of serial config code is skipped if ISINITIALIZED is TRUE

    // Use mapping counter to assign pointers for each bus mapping
    // These pointers will be used later when creating microcode instructions
    mappingCounterStartVal = mappingCounter;

    // Create a bus mapping for each data transfer cycle
    while (mappingCounter < pDIPanelInfo->SERIAL_TRANSFER_CYCLES + mappingCounterStartVal)
    {
        DWORD mappingIndex = mappingCounter-mappingCounterStartVal;

        // Configure Bus Mapping Data for data
        mappingData.dwComponent2Offset = pDIPanelInfo->SERIAL_MAPPINGS[mappingIndex].dwComponent2Offset;
        mappingData.dwComponent2Mask = pDIPanelInfo->SERIAL_MAPPINGS[mappingIndex].dwComponent2Mask;
        mappingData.dwComponent1Offset = pDIPanelInfo->SERIAL_MAPPINGS[mappingIndex].dwComponent1Offset;
        mappingData.dwComponent1Mask = pDIPanelInfo->SERIAL_MAPPINGS[mappingIndex].dwComponent1Mask;
        mappingData.dwComponent0Offset = pDIPanelInfo->SERIAL_MAPPINGS[mappingIndex].dwComponent0Offset;
        mappingData.dwComponent0Mask = pDIPanelInfo->SERIAL_MAPPINGS[mappingIndex].dwComponent0Mask;
        DCConfigureDataMapping(mappingCounter, &mappingData);

        mappingCounter++;
    }

    //*******************************************************
    // Serial Configuration - for Synchronous panel communication
    //*******************************************************

    //*******************************************************************
    // Use NEW_DATA events for DC Channel 8 to trigger 
    // microcode to send serial data to panel
    // This priority must be higher than general event! Otherwise this event will lost.
    //*******************************************************************
    DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_DATA, DC_READ_WRITE_SELECT_WRITE,
                                DC_REGION_SELECT_REGION1, 2, iMicroCodeAddr);
    DCConfigureMicrocodeEventMCU(DC_CHANNEL_8,DC_EVENT_NEW_DATA, DC_READ_WRITE_SELECT_WRITE,
                                DC_REGION_SELECT_REGION2, 2, iMicroCodeAddr);

    //*******************************************************
    // Create a microcode instruction for each serial transfer cycle
    //*******************************************************
    for (i= mappingCounterStartVal; i < pDIPanelInfo->SERIAL_TRANSFER_CYCLES + mappingCounterStartVal; i++)
    {
        // Write Data to Sync panel
        microcodeData.dwWord = iMicroCodeAddr++;
        microcodeData.dwStop = (i >= pDIPanelInfo->SERIAL_TRANSFER_CYCLES) ?
            DC_TEMPLATE_STOP_LAST_COMMAND : DC_TEMPLATE_STOP_CONTINUE; // If this is our last cycle, end the instruction sequence
        microcodeData.Command = DC_MICROCODE_COMMAND_WROD;
        microcodeData.dwLf = 0;
        microcodeData.dwAf = 0;
        microcodeData.dwOperand = 0;
        microcodeData.dwMapping = i + 1; // i here should equal the bus mapping pointer configured above
        microcodeData.dwWaveform = DI_SERIAL_WAVEFORM + 1;
        microcodeData.dwGluelogic = 0;
        microcodeData.dwSync = DC_CHANNEL_NONE;
        DCConfigureMicrocode(&microcodeData);
    }

    //**********************************************************
    // Configure Write Channel 8 (channel to initialize panel)
    //**********************************************************
    writeConfData.dwEventMask = IPU_DC_CHAN_MASK_DEFAULT_HIGHEST_PRI;
    writeConfData.dwDispNum = dispCounter;
    writeConfData.dwWordSize = IPU_DC_W_SIZE_32BITS; // 24 MSB bits are used (RGB,8 LSB bits are ignored by the DC)
    DCConfigureWriteChannel(DC_CHANNEL_8, &writeConfData);

    // Set Write Channel 8 start address
    DCSetWriteChannelAddress(DC_CHANNEL_8, 0);

    //*********************************************
    // Configure Display port 1
    //*********************************************
    dispConfData.dwDispType = IPU_DC_DISP_CONF1_DISP_TYP_SERIAL;
    dispConfData.dwAddrIncrement = IPU_DC_DISP_CONF1_ADDR_INCREMENT_2;
    dispConfData.dwByteEnableAddrIncrement = IPU_DC_DISP_CONF1_ADDR_BE_L_INC_1;
    dispConfData.dwAddrCompareMode = 0;
    dispConfData.dwPollingValueAndMaskSelect = 0;
    DCConfigureDisplay((DC_DISPLAY)dispCounter, &dispConfData);

    // Increment display counter for next panel
    dispCounter++;

    pDIPanelInfo->ISINITIALIZED = TRUE;
    for(i= 0;i < NUM_SUPPORTED_PANELS; i++)
    {
        if(g_PanelArray[i].PORTID == pDIPanelInfo->PORTID)
        {
            g_PanelArray[i].ISINITIALIZED = TRUE;
            g_PanelArray[i].DISPLAY_NUM = pDIPanelInfo->DISPLAY_NUM;
        }
    }
    
    // Restore reference values
    *pMappingCounter = mappingCounter;
    *pDispCounter = dispCounter;
    *pMicroCodeAddr = iMicroCodeAddr;
}


//------------------------------------------------------------------------------
//
// Function: BSPInitializeDI
//
// This function performs panel-specific initialization of the
// IPU DI module, including configuring panel-specific microcode routines.
//
// Parameters:
//      di_sel
//          [in] Selects either DI0 or DI1 to configure
//
//      pDIPanelInfo
//          [in] Panel info used in configuration process.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void BSPInitializeDI(DI_SELECT di_sel, PANEL_INFO* pDIPanelInfo)
{
    //LVDS and TVE use external di_clk, we need re-set the di clk.

    if(pDIPanelInfo->DICLK_SOURCE == DISPLAY_CLOCK_EXTERNAL)
    {
        //To set the correct di clock, revert the ipu clock temporarily.
        DISetIPUClkFreq(pDIPanelInfo->PIX_CLK_FREQ);
        DISetDIClkFreq(di_sel,pDIPanelInfo->PIX_CLK_FREQ );
        //Set the correct IPU clock.
        DISetIPUClkFreq(CSPIPUGetClk());
    }
    if (pDIPanelInfo->SYNC_TYPE == IPU_PANEL_SYNC_TYPE_ASYNCHRONOUS)
    {
        AsyncDIConfig(di_sel, pDIPanelInfo);
    } 
    else if ((pDIPanelInfo->SYNC_TYPE == IPU_PANEL_SYNC_TYPE_SYNCHRONOUS) && (pDIPanelInfo->ISINTERLACE != TRUE))
    {
        // Configure DC for Sync TV 720P/1080P (progressive).
        if( pDIPanelInfo->PORTID == DISPLAY_PORT_TV)
        {
            SyncDIConfigTV2P(di_sel, pDIPanelInfo);
        }
        else
        {
            // Configuration for non-interlaced synchronous displays
            SyncDIConfig(di_sel, pDIPanelInfo);
        }
    }
    else if ((pDIPanelInfo->SYNC_TYPE == IPU_PANEL_SYNC_TYPE_SYNCHRONOUS) && (pDIPanelInfo->ISINTERLACE == TRUE))
    {
        // Configuration for interlaced-output synchronous displays (TVEv2)
        SyncDIConfigTV2IL(di_sel, pDIPanelInfo);      
    }

    // Configure serial communication with panel if needed, and if we haven't already done so for this panel
    if (pDIPanelInfo->USESERIALPORT)
    {
        SerialDIConfig(di_sel, pDIPanelInfo);
    }
}


//------------------------------------------------------------------------------
//
// Function: AsyncDIConfig
//
// This function configures the DI for an asynchronous panel.
//
// Parameters:
//      di_sel
//          [in] Selects either DI0 or DI1 to configure
//
//      pDIPanelInfo
//          [in] Panel info used in configuration process.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void AsyncDIConfig(DI_SELECT di_sel, PANEL_INFO* pDIPanelInfo)
{
    DIPointerConfigData ptrConfData;
    DIUpDownConfigData upDownConfData;
    DIPolarityConfigData polConfData;
    DIGeneralConfigData genConfData;

    // Set MCU_T to 1 so that we use CH8 for command writes.
    CMSetMCU_T(1);

    // Signal waveform for command write
    upDownConfData.dwPointer  = DI_COMMAND_WAVEFORM;
    upDownConfData.dwSet  = DI_CS_SIGNAL;
    upDownConfData.dwUpPos = 0;
    upDownConfData.dwDownPos = pDIPanelInfo->WR_CYCLE_PER;
    DIConfigureUpDown(di_sel, &upDownConfData);

    upDownConfData.dwSet  = DI_WR_SIGNAL;
    upDownConfData.dwUpPos = pDIPanelInfo->WR_UP_POS;
    upDownConfData.dwDownPos = pDIPanelInfo->WR_DOWN_POS;
    DIConfigureUpDown(di_sel, &upDownConfData);

    upDownConfData.dwSet  = DI_NOUSE_SIGNAL;  //NULL SIGNAL
    upDownConfData.dwUpPos = 0;
    upDownConfData.dwDownPos = 0;
    DIConfigureUpDown(di_sel, &upDownConfData);

    upDownConfData.dwSet  = DI_RS_SIGNAL;
    upDownConfData.dwUpPos = 16;
    upDownConfData.dwDownPos = pDIPanelInfo->WR_CYCLE_PER;
    DIConfigureUpDown(di_sel, &upDownConfData);

    ptrConfData.dwPointer  = upDownConfData.dwPointer;
    ptrConfData.dwAccessPeriod = pDIPanelInfo->WR_CYCLE_PER / 2;
    ptrConfData.dwComponentPeriod = pDIPanelInfo->WR_CYCLE_PER / 2;
    ptrConfData.dwCSPtr= DI_CS_SIGNAL;
    ptrConfData.dwPinPtr[0]= DI_WR_SIGNAL;  //pin11 wr
    ptrConfData.dwPinPtr[1]= DI_NOUSE_SIGNAL;  //pin12 rd clear
    ptrConfData.dwPinPtr[2]= DI_RS_SIGNAL;  //pin13 rs 
    ptrConfData.dwPinPtr[3]= DI_NOUSE_SIGNAL;  //pin14  clear
    ptrConfData.dwPinPtr[4]= DI_NOUSE_SIGNAL;  //pin15  clear
    ptrConfData.dwPinPtr[5]= DI_NOUSE_SIGNAL;  //pin16  clear
    ptrConfData.dwPinPtr[6]= DI_NOUSE_SIGNAL;  //pin17  clear
    DIConfigurePointer(di_sel, &ptrConfData);

    // Signal waveform for data write
    upDownConfData.dwPointer  = DI_DATAWR_WAVEFORM;
    upDownConfData.dwSet  = DI_CS_SIGNAL;
    upDownConfData.dwUpPos = 0;
    upDownConfData.dwDownPos = pDIPanelInfo->WR_CYCLE_PER;
    DIConfigureUpDown(di_sel, &upDownConfData);

    upDownConfData.dwSet  = DI_WR_SIGNAL;
    upDownConfData.dwUpPos = pDIPanelInfo->WR_UP_POS;
    upDownConfData.dwDownPos = pDIPanelInfo->WR_DOWN_POS;
    DIConfigureUpDown(di_sel, &upDownConfData);

    upDownConfData.dwSet  = DI_NOUSE_SIGNAL; //NULL SIGNAL
    upDownConfData.dwUpPos = 0;
    upDownConfData.dwDownPos = 0;
    DIConfigureUpDown(di_sel, &upDownConfData);

    upDownConfData.dwSet  = DI_RS_SIGNAL; //NULL SIGNAL
    upDownConfData.dwUpPos = 0;
    upDownConfData.dwDownPos = 0;
    DIConfigureUpDown(di_sel, &upDownConfData);

    ptrConfData.dwPointer  = upDownConfData.dwPointer;
    ptrConfData.dwAccessPeriod = pDIPanelInfo->WR_CYCLE_PER / 2;
    ptrConfData.dwComponentPeriod = pDIPanelInfo->WR_CYCLE_PER / 2;
    ptrConfData.dwCSPtr= DI_CS_SIGNAL;
    ptrConfData.dwPinPtr[0]= DI_WR_SIGNAL;  //pin11 wr
    ptrConfData.dwPinPtr[1]= DI_NOUSE_SIGNAL;  //pin12 rd clear
    ptrConfData.dwPinPtr[2]= DI_RS_SIGNAL;  //pin13 rs 
    ptrConfData.dwPinPtr[3]= DI_NOUSE_SIGNAL;  //pin14  clear
    ptrConfData.dwPinPtr[4]= DI_NOUSE_SIGNAL;  //pin15  clear
    ptrConfData.dwPinPtr[5]= DI_NOUSE_SIGNAL;  //pin16  clear
    ptrConfData.dwPinPtr[6]= DI_NOUSE_SIGNAL;  //pin17  clear
    DIConfigurePointer(di_sel, &ptrConfData);

    // Signal waveform for data read
    upDownConfData.dwPointer  = DI_DATARD_WAVEFORM;
    upDownConfData.dwSet  = DI_CS_SIGNAL;
    upDownConfData.dwUpPos = 0;
    upDownConfData.dwDownPos = pDIPanelInfo->RD_CYCLE_PER;
    DIConfigureUpDown(di_sel, &upDownConfData);

    upDownConfData.dwSet  = DI_NOUSE_SIGNAL; //NULL SIGNAL
    upDownConfData.dwUpPos = 0;
    upDownConfData.dwDownPos = 0;
    DIConfigureUpDown(di_sel, &upDownConfData);

    upDownConfData.dwSet  = DI_RD_SIGNAL; 
    upDownConfData.dwUpPos = pDIPanelInfo->RD_UP_POS;
    upDownConfData.dwDownPos = pDIPanelInfo->RD_DOWN_POS;
    DIConfigureUpDown(di_sel, &upDownConfData);

    upDownConfData.dwSet  = DI_RS_SIGNAL; //NULL SIGNAL
    upDownConfData.dwUpPos = 0;
    upDownConfData.dwDownPos = 0;
    DIConfigureUpDown(di_sel, &upDownConfData);

    ptrConfData.dwPointer  = upDownConfData.dwPointer;
    ptrConfData.dwAccessPeriod = pDIPanelInfo->RD_CYCLE_PER / 2;
    ptrConfData.dwComponentPeriod = pDIPanelInfo->RD_CYCLE_PER / 2;
    ptrConfData.dwCSPtr= DI_CS_SIGNAL;
    ptrConfData.dwPinPtr[0]= DI_NOUSE_SIGNAL;  //pin11 wr
    ptrConfData.dwPinPtr[1]= DI_RD_SIGNAL;  //pin12 rd clear
    ptrConfData.dwPinPtr[2]= DI_RS_SIGNAL;  //pin13 rs 
    ptrConfData.dwPinPtr[3]= DI_NOUSE_SIGNAL;  //pin14  clear
    ptrConfData.dwPinPtr[4]= DI_NOUSE_SIGNAL;  //pin15  clear
    ptrConfData.dwPinPtr[5]= DI_NOUSE_SIGNAL;  //pin16  clear
    ptrConfData.dwPinPtr[6]= DI_NOUSE_SIGNAL;  //pin17  clear
    DIConfigurePointer(di_sel, &ptrConfData);

    genConfData.dwLineCounterSelect = 0;
    genConfData.dwClockStopMode = IPU_DI_GENERAL_DI_CLOCK_STOP_MODE_NEXT_EDGE;
    genConfData.dwDisplayClockInitMode = IPU_DI_GENERAL_DI_DISP_CLOCK_INIT_STOPPED;
    genConfData.dwMaskSelect = IPU_DI_GENERAL_DI_MASK_SEL_COUNTER_2;
    genConfData.dwVSyncSource = IPU_DI_GENERAL_DI_VSYNC_EXT_EXTERNAL;
    genConfData.dwClkSource = IPU_DI_GENERAL_DI_CLK_EXT_INTERNAL;
    genConfData.dwWatchdogMode = IPU_DI_GENERAL_DI_WATCHDOG_MODE_4_CYCLES;
    genConfData.dwClkPolarity = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW;
    genConfData.dwSyncFlowCounterSelect = 0;
    genConfData.dwSyncFlowErrorTreatment = IPU_DI_GENERAL_DI_ERR_TREATMENT_DRIVE_LAST_COMPONENT;
    genConfData.dwERMVSyncSelect = 0;
    genConfData.dwCS1Polarity = pDIPanelInfo->ASYNC_SIG_POL.CS1_POL;
    genConfData.dwCS0Polarity = pDIPanelInfo->ASYNC_SIG_POL.CS0_POL;
    genConfData.dwPinPolarity[0] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    genConfData.dwPinPolarity[1] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    genConfData.dwPinPolarity[2] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    genConfData.dwPinPolarity[3] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    genConfData.dwPinPolarity[4] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    genConfData.dwPinPolarity[5] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    genConfData.dwPinPolarity[6] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    genConfData.dwPinPolarity[7] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    DIConfigureGeneral(di_sel, &genConfData);

    // Setup signal pin polarity
    // Use default setting should be ok.

    // Configure CS0 polarity data
    polConfData.PolaritySetSelect = DI_POLARITY_SET_CS0;
    polConfData.DataPolarity = pDIPanelInfo->ASYNC_SIG_POL.CS0_POL;
    polConfData.PinPolarity[0] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    polConfData.PinPolarity[1] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    polConfData.PinPolarity[2] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    polConfData.PinPolarity[3] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    polConfData.PinPolarity[4] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    polConfData.PinPolarity[5] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    polConfData.PinPolarity[6] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    DIConfigurePolarity(di_sel, &polConfData);

    // Configure CS1 polarity data
    polConfData.PolaritySetSelect = DI_POLARITY_SET_CS1;
    polConfData.DataPolarity = pDIPanelInfo->ASYNC_SIG_POL.CS1_POL;
    polConfData.PinPolarity[0] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    polConfData.PinPolarity[1] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    polConfData.PinPolarity[2] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    polConfData.PinPolarity[3] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    polConfData.PinPolarity[4] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    polConfData.PinPolarity[5] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    polConfData.PinPolarity[6] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    DIConfigurePolarity(di_sel, &polConfData);

    // Configure DRDY polarity data
    polConfData.PolaritySetSelect = DI_POLARITY_SET_DRDY;
    polConfData.DataPolarity = 0;
    polConfData.PinPolarity[0] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    polConfData.PinPolarity[1] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    polConfData.PinPolarity[2] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    polConfData.PinPolarity[3] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    polConfData.PinPolarity[4] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    polConfData.PinPolarity[5] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    polConfData.PinPolarity[6] = pDIPanelInfo->ASYNC_SIG_POL.DATA_POL;
    DIConfigurePolarity(di_sel, &polConfData);

    // Configure Wait polarity data
    polConfData.PolaritySetSelect = DI_POLARITY_SET_WAIT;
    polConfData.WaitPolarity = 0;
    DIConfigurePolarity(di_sel, &polConfData);

    // Configure CS0 byte enable polarity data
    polConfData.PolaritySetSelect = DI_POLARITY_SET_CS0_BYTE_ENABLE;
    polConfData.CS0ByteEnablePolarity = 0;
    DIConfigurePolarity(di_sel, &polConfData);

    // Configure CS1 Byte enable polarity data
    polConfData.PolaritySetSelect = DI_POLARITY_SET_CS1_BYTE_ENABLE;
    polConfData.CS1ByteEnablePolarity = 0;
    DIConfigurePolarity(di_sel, &polConfData);
    
}


//------------------------------------------------------------------------------
//
// Function: SyncDIConfig
//
// This function configures the DI for a synchronous panel.
//
// Parameters:
//      di_sel
//          [in] Selects either DI0 or DI1 to configure
//
//      pDIPanelInfo
//          [in] Panel info used in configuration process.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void SyncDIConfig(DI_SELECT di_sel, PANEL_INFO* pDIPanelInfo)
{
    DIPointerConfigData ptrConfData;
    DIUpDownConfigData upDownConfData;
    DIPolarityConfigData polConfData;
    DIGeneralConfigData genConfData;
    DISyncConfigData SyncConfigData;

    // Signal waveform for command write
    upDownConfData.dwPointer  = DI_SDC_WAVEFORM;
    upDownConfData.dwSet  = DI_DEN_SIGNAL;
    upDownConfData.dwUpPos = 0;
    upDownConfData.dwDownPos = (UINT32)((LONGLONG)1000000000*2/pDIPanelInfo->PIX_CLK_FREQ) + 1;
    DIConfigureUpDown(di_sel, &upDownConfData);

    upDownConfData.dwSet  = DI_NOUSE_SIGNAL;  //NULL SIGNAL
    upDownConfData.dwUpPos = 0;
    upDownConfData.dwDownPos = 0;
    DIConfigureUpDown(di_sel, &upDownConfData);

    ptrConfData.dwPointer  = upDownConfData.dwPointer;
    ptrConfData.dwAccessPeriod = (UINT32)((LONGLONG)1000000000/pDIPanelInfo->PIX_CLK_FREQ) + 1;
    ptrConfData.dwComponentPeriod = (UINT32)((LONGLONG)1000000000/pDIPanelInfo->PIX_CLK_FREQ) + 1;
    ptrConfData.dwCSPtr= DI_NOUSE_SIGNAL;
    ptrConfData.dwPinPtr[0]= DI_NOUSE_SIGNAL;  //pin11 clear
    ptrConfData.dwPinPtr[1]= DI_NOUSE_SIGNAL;  //pin12 clear
    ptrConfData.dwPinPtr[2]= DI_NOUSE_SIGNAL;  //pin13 clear 
    ptrConfData.dwPinPtr[3]= DI_NOUSE_SIGNAL;  //pin14  clear
    ptrConfData.dwPinPtr[4]= DI_DEN_SIGNAL;    //pin15  DEN
    ptrConfData.dwPinPtr[5]= DI_NOUSE_SIGNAL;  //pin16  clear
    ptrConfData.dwPinPtr[6]= DI_NOUSE_SIGNAL;  //pin17  clear
    DIConfigurePointer(di_sel, &ptrConfData);

    DIConfigureBaseSyncClockGen(di_sel, 
                                pDIPanelInfo->PIX_DATA_POS, 
                                pDIPanelInfo->PIX_CLK_FREQ,
                                pDIPanelInfo->PIX_CLK_UP,
                                pDIPanelInfo->PIX_CLK_DOWN);

    DISetScreenHeight(di_sel, 
                      pDIPanelInfo->HEIGHT + 
                      pDIPanelInfo->VSTARTWIDTH + 
                      pDIPanelInfo->VENDWIDTH + 
                      pDIPanelInfo->VSYNCWIDTH - 1);

    // TODO: REMOVE THE HARDCODE
    //internal HSYNC
    SyncConfigData.dwPointer = DI_COUNTER_IHSYNC;
    SyncConfigData.dwRunValue = pDIPanelInfo->WIDTH+
                                pDIPanelInfo->HSTARTWIDTH+
                                pDIPanelInfo->HENDWIDTH+
                                pDIPanelInfo->HSYNCWIDTH-1;
    SyncConfigData.dwRunResolution =DI_COUNTER_BASECLK+1;
    SyncConfigData.dwOffsetValue=0;
    SyncConfigData.dwOffsetResolution=0;
    SyncConfigData.dwPolarityClearSelect=0;
    SyncConfigData.dwToggleTriggerSelect=0;
    SyncConfigData.dwCounterClearSelect=0;
    SyncConfigData.bCounterAutoReload=1;
    SyncConfigData.dwPolarityGeneratorEnable=0;
    SyncConfigData.dwUpPos=0;   // in pixel clock
    SyncConfigData.dwDownPos=0; // in pixel clock
    SyncConfigData.dwStepRepeat=0;
    DIConfigureSync(di_sel,&SyncConfigData);

    //output HSYNC
    SyncConfigData.dwPointer = DI_COUNTER_OHSYNC;
    SyncConfigData.dwRunValue = pDIPanelInfo->WIDTH + 
                                pDIPanelInfo->HSTARTWIDTH + 
                                pDIPanelInfo->HENDWIDTH +
                                pDIPanelInfo->HSYNCWIDTH-1;
    SyncConfigData.dwRunResolution =DI_COUNTER_BASECLK+1;
    SyncConfigData.dwOffsetValue=0;
    SyncConfigData.dwOffsetResolution=0;
    SyncConfigData.dwPolarityClearSelect=0;
    SyncConfigData.dwToggleTriggerSelect=IPU_DI_SW_GEN1_DI_CNT_POLARITY_TRIGGER_SEL_DISP_CLK;
    SyncConfigData.dwCounterClearSelect=0;
    SyncConfigData.bCounterAutoReload=1;
    SyncConfigData.dwPolarityGeneratorEnable=1;
    SyncConfigData.dwUpPos=0;                          // in pixel clocks
    SyncConfigData.dwDownPos=pDIPanelInfo->HSYNCWIDTH; // in units of pixel clocks
    SyncConfigData.dwStepRepeat=0;
    DIConfigureSync(di_sel,&SyncConfigData);

    //Output Vsync
    SyncConfigData.dwPointer = DI_COUNTER_OVSYNC;
    SyncConfigData.dwRunValue = pDIPanelInfo->HEIGHT +
                                pDIPanelInfo->VSTARTWIDTH +
                                pDIPanelInfo->VENDWIDTH +
                                pDIPanelInfo->VSYNCWIDTH-1;
    SyncConfigData.dwRunResolution =DI_COUNTER_OHSYNC+1;
    SyncConfigData.dwOffsetValue=0;
    SyncConfigData.dwOffsetResolution=0;
    SyncConfigData.dwPolarityClearSelect=0;
    SyncConfigData.dwToggleTriggerSelect=DI_COUNTER_OHSYNC+1;
    SyncConfigData.dwCounterClearSelect=0;
    SyncConfigData.bCounterAutoReload=1;
    SyncConfigData.dwPolarityGeneratorEnable=1;
    SyncConfigData.dwUpPos=0;                           // in internal hsync periods
    SyncConfigData.dwDownPos=pDIPanelInfo->VSYNCWIDTH; // in internal hsync periods
    SyncConfigData.dwStepRepeat=0;
    DIConfigureSync(di_sel,&SyncConfigData);

    //Active Lines start points
    SyncConfigData.dwPointer = DI_COUNTER_ALINE;
    SyncConfigData.dwRunValue = 0;
    SyncConfigData.dwRunResolution =DI_COUNTER_OHSYNC+1;
    SyncConfigData.dwOffsetValue=pDIPanelInfo->VSTARTWIDTH + 
                                 pDIPanelInfo->VSYNCWIDTH;
    SyncConfigData.dwOffsetResolution=DI_COUNTER_OHSYNC+1;
    SyncConfigData.dwPolarityClearSelect=0;
    SyncConfigData.dwToggleTriggerSelect=0;
    SyncConfigData.dwCounterClearSelect=DI_COUNTER_OVSYNC+1;
    SyncConfigData.bCounterAutoReload=0;
    SyncConfigData.dwPolarityGeneratorEnable=0;
    SyncConfigData.dwUpPos=0;                           // in hsync periods
    SyncConfigData.dwDownPos=0;                         // in hsync periods
    SyncConfigData.dwStepRepeat=pDIPanelInfo->HEIGHT;
    DIConfigureSync(di_sel,&SyncConfigData);

    //Active clock start points
    SyncConfigData.dwPointer = DI_COUNTER_ACLOCK;
    SyncConfigData.dwRunValue = 0;
    SyncConfigData.dwRunResolution =DI_COUNTER_BASECLK+1;
    SyncConfigData.dwOffsetValue=pDIPanelInfo->HSTARTWIDTH + 
                                 pDIPanelInfo->HSYNCWIDTH;
    SyncConfigData.dwOffsetResolution=DI_COUNTER_BASECLK+1;
    SyncConfigData.dwPolarityClearSelect=0;
    SyncConfigData.dwToggleTriggerSelect=0;
    SyncConfigData.dwCounterClearSelect=DI_COUNTER_ALINE+1;
    SyncConfigData.bCounterAutoReload=0;
    SyncConfigData.dwPolarityGeneratorEnable=0;
    SyncConfigData.dwUpPos=0;                           // in hsync periods
    SyncConfigData.dwDownPos=0;                         // in hsync periods
    SyncConfigData.dwStepRepeat=pDIPanelInfo->WIDTH;
    DIConfigureSync(di_sel,&SyncConfigData);

    if(pDIPanelInfo->PORTID == DISPLAY_PORT_VGA)
    {
        // COUNTER_7 for delay HSYNC 
        SyncConfigData.dwPointer= DI_COUNTER_VGA_HSYNC;
        SyncConfigData.dwRunValue                  = pDIPanelInfo->WIDTH + 
                                                    pDIPanelInfo->HSTARTWIDTH + 
                                                    pDIPanelInfo->HENDWIDTH +
                                                    pDIPanelInfo->HSYNCWIDTH-1;                   
        SyncConfigData.dwRunResolution              = DI_COUNTER_BASECLK+1; 
        SyncConfigData.dwOffsetValue              = 15;                                         
        SyncConfigData.dwOffsetResolution          = DI_COUNTER_BASECLK+1;    
        SyncConfigData.dwPolarityClearSelect      = 0;
        SyncConfigData.dwToggleTriggerSelect      = DI_COUNTER_BASECLK+1;
        SyncConfigData.dwCounterClearSelect       = 0;                     
        SyncConfigData.bCounterAutoReload          = 1;
        SyncConfigData.dwPolarityGeneratorEnable  = 1;
        SyncConfigData.dwUpPos                      = 0;     
        SyncConfigData.dwDownPos                  = pDIPanelInfo->HSYNCWIDTH; 
        SyncConfigData.dwStepRepeat               = 0; 
        SyncConfigData.dwGentimeSelect              = 0;
        
        DIConfigureSync(di_sel, &SyncConfigData);
        
        
        // COUNTER_8 for delay VSYNC
        SyncConfigData.dwPointer                  = DI_COUNTER_VGA_VSYNC;
        SyncConfigData.dwRunValue                  = pDIPanelInfo->HEIGHT +
                                                    pDIPanelInfo->VSTARTWIDTH +
                                                    pDIPanelInfo->VENDWIDTH +
                                                    pDIPanelInfo->VSYNCWIDTH-1;;                      
        SyncConfigData.dwRunResolution              = DI_COUNTER_IHSYNC + 1; 
        SyncConfigData.dwOffsetValue              = 2;                                    
        SyncConfigData.dwOffsetResolution          = DI_COUNTER_IHSYNC + 1;    
        SyncConfigData.dwPolarityClearSelect      = 0;
        SyncConfigData.dwToggleTriggerSelect      = DI_COUNTER_IHSYNC + 1;
        SyncConfigData.dwCounterClearSelect       = 0;                     
        SyncConfigData.bCounterAutoReload          = 1;
        SyncConfigData.dwPolarityGeneratorEnable  = 1;
        SyncConfigData.dwUpPos                      = 0;     
        SyncConfigData.dwDownPos                  = pDIPanelInfo->VSYNCWIDTH; 
        SyncConfigData.dwStepRepeat               = 0; 
        SyncConfigData.dwGentimeSelect              = 0;
        
        DIConfigureSync(di_sel, &SyncConfigData);
    }

    DISetSyncStart(di_sel,2);//2lines predictions
    DIVSyncCounterSelect(di_sel,2); //PIN3 as VSYNC

    genConfData.dwLineCounterSelect = 0;
    genConfData.dwClockStopMode = IPU_DI_GENERAL_DI_CLOCK_STOP_MODE_NEXT_EDGE;
    genConfData.dwDisplayClockInitMode = IPU_DI_GENERAL_DI_DISP_CLOCK_INIT_STOPPED;
    genConfData.dwMaskSelect = IPU_DI_GENERAL_DI_MASK_SEL_COUNTER_2;
    if(pDIPanelInfo->DICLK_SOURCE == DISPLAY_CLOCK_EXTERNAL)
    {    
        genConfData.dwClkSource = IPU_DI_GENERAL_DI_CLK_EXT_EXTERNAL;
        genConfData.dwVSyncSource = IPU_DI_GENERAL_DI_VSYNC_EXT_INTERNAL;
    }
    else
    {
        genConfData.dwClkSource = IPU_DI_GENERAL_DI_CLK_EXT_INTERNAL;
        genConfData.dwVSyncSource = IPU_DI_GENERAL_DI_VSYNC_EXT_INTERNAL;
    }
    genConfData.dwWatchdogMode = IPU_DI_GENERAL_DI_WATCHDOG_MODE_4_CYCLES;
    genConfData.dwClkPolarity = pDIPanelInfo->SYNC_SIG_POL.CLK_POL;
    genConfData.dwSyncFlowCounterSelect = 0;
    genConfData.dwSyncFlowErrorTreatment = IPU_DI_GENERAL_DI_ERR_TREATMENT_DRIVE_LAST_COMPONENT;
    genConfData.dwERMVSyncSelect = 0;
    genConfData.dwCS1Polarity = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW;
    genConfData.dwCS0Polarity = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW;
    genConfData.dwPinPolarity[0] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW;
    genConfData.dwPinPolarity[1] = pDIPanelInfo->SYNC_SIG_POL.HSYNC_POL;    //PIN2 as HSYNC
    genConfData.dwPinPolarity[2] = pDIPanelInfo->SYNC_SIG_POL.VSYNC_POL;    //PIN3 as VSYNC
    genConfData.dwPinPolarity[3] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    genConfData.dwPinPolarity[4] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    genConfData.dwPinPolarity[5] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW;
    genConfData.dwPinPolarity[6] = pDIPanelInfo->SYNC_SIG_POL.HSYNC_POL; 
    genConfData.dwPinPolarity[7] = pDIPanelInfo->SYNC_SIG_POL.VSYNC_POL;
    

    DIConfigureGeneral(di_sel, &genConfData);

    // Setup signal pin polarity
    // Use default setting should be ok.

    // Configure CS0 polarity data
    polConfData.PolaritySetSelect = DI_POLARITY_SET_CS0;
    polConfData.DataPolarity = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[0] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[1] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[2] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[3] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[4] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[5] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[6] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    DIConfigurePolarity(di_sel, &polConfData);

    // Configure CS1 polarity data
    polConfData.PolaritySetSelect = DI_POLARITY_SET_CS1;
    polConfData.DataPolarity = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[0] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[1] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[2] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[3] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[4] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[5] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[6] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    DIConfigurePolarity(di_sel, &polConfData);

    // Configure DRDY polarity data
    polConfData.PolaritySetSelect = DI_POLARITY_SET_DRDY;
    polConfData.DataPolarity = pDIPanelInfo->SYNC_SIG_POL.DATA_POL;
    polConfData.PinPolarity[0] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[1] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[2] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[3] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[4] = pDIPanelInfo->SYNC_SIG_POL.ENABLE_POL;
    polConfData.PinPolarity[5] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    polConfData.PinPolarity[6] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    DIConfigurePolarity(di_sel, &polConfData);

    // Configure Wait polarity data
    polConfData.PolaritySetSelect = DI_POLARITY_SET_WAIT;
    polConfData.WaitPolarity = 0;
    DIConfigurePolarity(di_sel, &polConfData);

    // Configure CS0 byte enable polarity data
    polConfData.PolaritySetSelect = DI_POLARITY_SET_CS0_BYTE_ENABLE;
    polConfData.CS0ByteEnablePolarity = 0;
    DIConfigurePolarity(di_sel, &polConfData);

    // Configure CS1 Byte enable polarity data
    polConfData.PolaritySetSelect = DI_POLARITY_SET_CS1_BYTE_ENABLE;
    polConfData.CS1ByteEnablePolarity = 0;
    DIConfigurePolarity(di_sel, &polConfData);

    // Set MCU_T to 2
    CMSetMCU_T(2);
}


//------------------------------------------------------------------------------
//
// Function: SyncDIConfigTV2IL
//
// This function configures the DI for a synchronous panel (TVEv2) taking interlaced data.
//
// Parameters:
//      di_sel
//          [in] Selects either DI0 or DI1 to configure
//
//      pDIPanelInfo
//          [in] Panel info used in configuration process.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void SyncDIConfigTV2IL(DI_SELECT di_sel, PANEL_INFO* pDIPanelInfo)
{
    DIPointerConfigData ptrConfData;
    DIUpDownConfigData upDownConfData;
    DIPolarityConfigData polConfData;
    DIGeneralConfigData genConfData;
    DISyncConfigData SyncConfigData;

   // int field0_offset=0, field1_offset=0;

    // Set DI Pointer configuration data 
    ptrConfData.dwPointer         = DI_TVE_WAVEFORM;
    ptrConfData.dwAccessPeriod    = (UINT32)((LONGLONG)1000000000/pDIPanelInfo->PIX_CLK_FREQ + 1);
    ptrConfData.dwComponentPeriod = (UINT32)((LONGLONG)1000000000/pDIPanelInfo->PIX_CLK_FREQ + 1);
    ptrConfData.dwCSPtr           = 0;
    ptrConfData.dwPinPtr[0]       = 0;
    ptrConfData.dwPinPtr[1]       = 0;
    ptrConfData.dwPinPtr[2]       = 0;
    ptrConfData.dwPinPtr[3]       = 0;
    ptrConfData.dwPinPtr[4]       = 3;
    ptrConfData.dwPinPtr[5]       = 0;
    ptrConfData.dwPinPtr[6]       = 0;

    DIConfigurePointer(di_sel, &ptrConfData);

    // Set DI Up and Down configuration data 
    upDownConfData.dwPointer = DI_TVE_WAVEFORM;
    upDownConfData.dwSet     = 0;
    upDownConfData.dwUpPos   = 0;
    upDownConfData.dwDownPos = 0;

    DIConfigureUpDown(di_sel, &upDownConfData);

    // Set DI Up and Down configuration data 
    upDownConfData.dwPointer = DI_TVE_WAVEFORM;
    upDownConfData.dwSet     = 1;
    upDownConfData.dwUpPos   = 0;
    upDownConfData.dwDownPos = 0;

    DIConfigureUpDown(di_sel, &upDownConfData);

    // Set DI Up and Down configuration data 
    upDownConfData.dwPointer = DI_TVE_WAVEFORM;
    upDownConfData.dwSet     = 2;
    upDownConfData.dwUpPos   = 0;
    upDownConfData.dwDownPos = 0;

    DIConfigureUpDown(di_sel, &upDownConfData);

    // Set DI Up and Down configuration data     
    upDownConfData.dwPointer = DI_TVE_WAVEFORM;
    upDownConfData.dwSet     = 3;
    upDownConfData.dwUpPos   = 0;
    upDownConfData.dwDownPos = (UINT32)((LONGLONG)1000000000*2/pDIPanelInfo->PIX_CLK_FREQ) + 1;

    DIConfigureUpDown(di_sel, &upDownConfData);

    if (pDIPanelInfo->WIDTH > 720)
    {
        DIConfigureBaseSyncClockGen(di_sel,
                                    pDIPanelInfo->PIX_DATA_POS, 
                                    pDIPanelInfo->PIX_CLK_FREQ,
                                    pDIPanelInfo->PIX_CLK_UP,
                                    pDIPanelInfo->PIX_CLK_DOWN);
    }
    else
    {
        DIConfigureBaseSyncClockGen(di_sel,
                                    pDIPanelInfo->PIX_DATA_POS, 
                                    pDIPanelInfo->PIX_CLK_FREQ/2,
                                    pDIPanelInfo->PIX_CLK_UP,
                                    pDIPanelInfo->PIX_CLK_DOWN);    
    }

    // Configure DI1 Screen Hight using field0
    DISetScreenHeight(di_sel, pDIPanelInfo->VSTARTWIDTH_FIELD0 + 
                                     pDIPanelInfo->HEIGHT/2 + 
                                     pDIPanelInfo->VENDWIDTH_FIELD0 - 1);

    // Clean-up all 9 DI1 COUNTERS.
    for (int i=1; i<=9; i++)
    {
        SyncConfigData.dwPointer                  = i;
        SyncConfigData.dwRunValue                 = 0;
                                                
        SyncConfigData.dwRunResolution            = 0;
        SyncConfigData.dwOffsetValue              = 0;
        SyncConfigData.dwOffsetResolution         = 0;
        SyncConfigData.dwPolarityClearSelect      = 0;
        SyncConfigData.dwToggleTriggerSelect      = 0;
        SyncConfigData.dwCounterClearSelect       = 0;
        SyncConfigData.bCounterAutoReload         = 0;
        SyncConfigData.dwPolarityGeneratorEnable  = 0;
        SyncConfigData.dwUpPos                    = 0;   
        SyncConfigData.dwDownPos                  = 0;
        SyncConfigData.dwStepRepeat               = 0; 
        SyncConfigData.dwGentimeSelect            = 0;

        DIConfigureSync(di_sel, &SyncConfigData);
    }
        
    
    // COUNTER_1 for lines: basic HSYNC
    SyncConfigData.dwPointer                  = DI_TVE_COUNTER_BHSYNC;
    SyncConfigData.dwRunValue                 = (pDIPanelInfo->HSTARTWIDTH +  
                                                pDIPanelInfo->HSYNCWIDTH +
                                                pDIPanelInfo->WIDTH + 
                                                pDIPanelInfo->HENDWIDTH)/2 - 1;
    SyncConfigData.dwRunResolution            = IPU_DI_SW_GEN0_DI_RUN_RESOLUTION_DISP_CLK;
    SyncConfigData.dwOffsetValue              = 0;
    SyncConfigData.dwOffsetResolution         = IPU_DI_SW_GEN0_DI_OFFSET_RESOLUTION_DISABLE;
    SyncConfigData.dwPolarityClearSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_CLR_SEL_INVERTED;
    SyncConfigData.dwToggleTriggerSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_TRIGGER_SEL_DISABLE;
    SyncConfigData.dwCounterClearSelect       = IPU_DI_SW_GEN1_DI_CNT_CLR_SEL_DISABLE;
    SyncConfigData.bCounterAutoReload         = 1;
    SyncConfigData.dwPolarityGeneratorEnable  = IPU_DI_SW_GEN1_DI_CNT_POLARITY_GEN_EN_DISABLE;
    SyncConfigData.dwUpPos                    = 0;   
    SyncConfigData.dwDownPos                  = 0;
    SyncConfigData.dwStepRepeat               = 0; 
    SyncConfigData.dwGentimeSelect            = 0;

    DIConfigureSync(di_sel, &SyncConfigData);

    // COUNTER_2
    SyncConfigData.dwPointer                  = DI_TVE_COUNTER_F1VSYNC;
    SyncConfigData.dwRunValue                 = pDIPanelInfo->HSTARTWIDTH +
                                                pDIPanelInfo->HSYNCWIDTH  +
                                                pDIPanelInfo->WIDTH       + 
                                                pDIPanelInfo->HENDWIDTH - 1;

    SyncConfigData.dwRunResolution            = IPU_DI_SW_GEN0_DI_RUN_RESOLUTION_DISP_CLK;
    SyncConfigData.dwOffsetValue              = 0;
    SyncConfigData.dwOffsetResolution         = IPU_DI_SW_GEN0_DI_OFFSET_RESOLUTION_DISABLE; 
    SyncConfigData.dwPolarityClearSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_GEN_EN_DISABLE;
    SyncConfigData.dwToggleTriggerSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_TRIGGER_SEL_DISABLE;
    SyncConfigData.dwCounterClearSelect       = IPU_DI_SW_GEN1_DI_CNT_CLR_SEL_DISABLE;
    SyncConfigData.bCounterAutoReload         = 1;
    SyncConfigData.dwPolarityGeneratorEnable  = IPU_DI_SW_GEN1_DI_CNT_POLARITY_GEN_EN_DISABLE;
    SyncConfigData.dwUpPos                    = 0;   
    
    if (pDIPanelInfo->WIDTH > 720)
    {
        SyncConfigData.dwDownPos              = 1;
    }
    else
    {
        SyncConfigData.dwDownPos              = 2; 
    }
    
    SyncConfigData.dwStepRepeat               = 0;
    SyncConfigData.dwGentimeSelect            = 0;

    DIConfigureSync(di_sel, &SyncConfigData);


    // COUNTER_3
    SyncConfigData.dwPointer                  = DI_TVE_COUNTER_IHSYNC;
    SyncConfigData.dwRunValue                 = (pDIPanelInfo->VSTARTWIDTH_FIELD0 +  
                                                pDIPanelInfo->VENDWIDTH_FIELD0    +
                                                pDIPanelInfo->HEIGHT              +
                                                pDIPanelInfo->VSTARTWIDTH_FIELD1  +
                                                pDIPanelInfo->VENDWIDTH_FIELD1)*2 - 1;
                                                
    SyncConfigData.dwRunResolution            = IPU_DI_SW_GEN0_DI_RUN_RESOLUTION_COUNTER_1;
    SyncConfigData.dwOffsetValue              = 1;
    SyncConfigData.dwOffsetResolution         = IPU_DI_SW_GEN0_DI_OFFSET_RESOLUTION_COUNTER_1;
    SyncConfigData.dwPolarityClearSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_GEN_EN_DISABLE;
    SyncConfigData.dwToggleTriggerSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_TRIGGER_SEL_DISABLE;
    SyncConfigData.dwCounterClearSelect       = IPU_DI_SW_GEN1_DI_CNT_CLR_SEL_DISABLE;
    SyncConfigData.bCounterAutoReload         = 1;
    SyncConfigData.dwPolarityGeneratorEnable  = IPU_DI_SW_GEN1_DI_CNT_POLARITY_GEN_EN_DISABLE;
    SyncConfigData.dwUpPos                    = 0;   
    
    if (pDIPanelInfo->WIDTH > 720)
    {
        SyncConfigData.dwDownPos              = 1;
    }
    else
    {
        SyncConfigData.dwDownPos              = 2;
    }
    
    SyncConfigData.dwStepRepeat               = 0; 
    SyncConfigData.dwGentimeSelect            = 0;

    DIConfigureSync(di_sel, &SyncConfigData);

              
    // COUNTER_4: first active line of each field field0, field1.
    SyncConfigData.dwPointer                  = DI_TVE_COUNTER_AFIELD;
 
    SyncConfigData.dwRunValue                 = pDIPanelInfo->VENDWIDTH_FIELD0 +  
                                                pDIPanelInfo->HEIGHT / 2       +
                                                pDIPanelInfo->VSTARTWIDTH_FIELD1 -1;

    SyncConfigData.dwRunResolution            = IPU_DI_SW_GEN0_DI_RUN_RESOLUTION_COUNTER_2;                                      
    SyncConfigData.dwOffsetValue              = pDIPanelInfo->VSTARTWIDTH_FIELD0;
    SyncConfigData.dwOffsetResolution         = IPU_DI_SW_GEN0_DI_OFFSET_RESOLUTION_COUNTER_2;
    SyncConfigData.dwPolarityClearSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_GEN_EN_DISABLE;
    SyncConfigData.dwToggleTriggerSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_TRIGGER_SEL_DISABLE;
    SyncConfigData.dwCounterClearSelect       = IPU_DI_SW_GEN1_DI_CNT_CLR_SEL_COUNTER_3;
    SyncConfigData.bCounterAutoReload         = 0;
    SyncConfigData.dwPolarityGeneratorEnable  = IPU_DI_SW_GEN1_DI_CNT_POLARITY_GEN_EN_DISABLE;
    SyncConfigData.dwUpPos                    = 0;   
    SyncConfigData.dwDownPos                  = 0;
    SyncConfigData.dwStepRepeat               = 2; // Repeat one time for FIELD0
    SyncConfigData.dwGentimeSelect            = 0;

    DIConfigureSync(di_sel, &SyncConfigData);


    // COUNTER_5 for active lines for each field
    SyncConfigData.dwPointer                  = DI_TVE_COUNTER_ALINE;
    SyncConfigData.dwRunValue                 = 0;   
    SyncConfigData.dwRunResolution            = IPU_DI_SW_GEN0_DI_RUN_RESOLUTION_COUNTER_2;
    SyncConfigData.dwOffsetValue              = 0;  
    SyncConfigData.dwOffsetResolution         = IPU_DI_SW_GEN0_DI_OFFSET_RESOLUTION_DISABLE;                     
    SyncConfigData.dwPolarityClearSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_CLR_SEL_INVERTED;
    SyncConfigData.dwToggleTriggerSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_TRIGGER_SEL_DISABLE;
    SyncConfigData.dwCounterClearSelect       = IPU_DI_SW_GEN1_DI_CNT_CLR_SEL_COUNTER_4;
    SyncConfigData.bCounterAutoReload         = 0;
    SyncConfigData.dwPolarityGeneratorEnable  = IPU_DI_SW_GEN1_DI_CNT_POLARITY_GEN_EN_DISABLE;
    SyncConfigData.dwUpPos                    = 0;   
    SyncConfigData.dwDownPos                  = 0;                                         
    SyncConfigData.dwStepRepeat               = pDIPanelInfo->HEIGHT / 2;
    SyncConfigData.dwGentimeSelect            = 0;

    DIConfigureSync(di_sel, &SyncConfigData);

                
    // COUNTER_6 VSYNC for field1 only
    SyncConfigData.dwPointer                  = DI_TVE_COUNTER_F0VSYNC;     
    SyncConfigData.dwRunValue                 = pDIPanelInfo->VSTARTWIDTH_FIELD0 +
                                                pDIPanelInfo->VENDWIDTH_FIELD0   +
                                                pDIPanelInfo->HEIGHT             +
                                                pDIPanelInfo->VSTARTWIDTH_FIELD1 +
                                                pDIPanelInfo->VENDWIDTH_FIELD1 - 1;

    SyncConfigData.dwRunResolution            = IPU_DI_SW_GEN0_DI_RUN_RESOLUTION_COUNTER_2;
    SyncConfigData.dwOffsetValue              = 0;  
    SyncConfigData.dwOffsetResolution         = IPU_DI_SW_GEN0_DI_OFFSET_RESOLUTION_DISABLE;                    
    SyncConfigData.dwPolarityClearSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_CLR_SEL_INVERTED;
    SyncConfigData.dwToggleTriggerSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_TRIGGER_SEL_DISABLE;
    SyncConfigData.dwCounterClearSelect       = IPU_DI_SW_GEN1_DI_CNT_CLR_SEL_DISABLE;                     
    SyncConfigData.bCounterAutoReload         = 1;
    SyncConfigData.dwPolarityGeneratorEnable  = IPU_DI_SW_GEN1_DI_CNT_POLARITY_GEN_EN_DISABLE;
    SyncConfigData.dwUpPos                    = 0;   
    SyncConfigData.dwDownPos                  = 0;
    SyncConfigData.dwStepRepeat               = 0;                      
    SyncConfigData.dwGentimeSelect            = 0;

    DIConfigureSync(di_sel, &SyncConfigData);

    // COUNTER_7 for 2 VSYNC for field1 and field0 
    SyncConfigData.dwPointer                  = DI_TVE_COUNTER_DCVSYNC;
    SyncConfigData.dwRunValue                 = pDIPanelInfo->VENDWIDTH_FIELD0   +
                                                pDIPanelInfo->HEIGHT / 2         +
                                                pDIPanelInfo->VSTARTWIDTH_FIELD1 - 1;
                                               
    
    SyncConfigData.dwRunResolution            = IPU_DI_SW_GEN0_DI_RUN_RESOLUTION_COUNTER_2;
    SyncConfigData.dwOffsetValue              = 9;                   
    SyncConfigData.dwOffsetResolution         = IPU_DI_SW_GEN0_DI_OFFSET_RESOLUTION_COUNTER_2;     
    SyncConfigData.dwPolarityClearSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_CLR_SEL_INVERTED;
    SyncConfigData.dwToggleTriggerSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_TRIGGER_SEL_DISABLE;
    SyncConfigData.dwCounterClearSelect       = IPU_DI_SW_GEN1_DI_CNT_CLR_SEL_COUNTER_3;                   
    SyncConfigData.bCounterAutoReload         = 0;
    SyncConfigData.dwPolarityGeneratorEnable  = IPU_DI_SW_GEN1_DI_CNT_POLARITY_GEN_EN_DISABLE;
    SyncConfigData.dwUpPos                    = 0;  
    SyncConfigData.dwDownPos                  = 0;
    SyncConfigData.dwStepRepeat               = 2;  
    SyncConfigData.dwGentimeSelect            = 0;

    DIConfigureSync(di_sel, &SyncConfigData);


    // COUNTER_8 for active pixel out.
    SyncConfigData.dwPointer                  = DI_TVE_COUNTER_APIXEL;
    SyncConfigData.dwRunValue                 = 0;                    
    SyncConfigData.dwRunResolution            = IPU_DI_SW_GEN0_DI_RUN_RESOLUTION_DISP_CLK;

    SyncConfigData.dwOffsetValue              = pDIPanelInfo->HSTARTWIDTH +       
                                                pDIPanelInfo->HSYNCWIDTH;
                                              
    SyncConfigData.dwOffsetResolution         = IPU_DI_SW_GEN0_DI_OFFSET_RESOLUTION_DISP_CLK; 
    SyncConfigData.dwPolarityClearSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_CLR_SEL_INVERTED;
    SyncConfigData.dwToggleTriggerSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_TRIGGER_SEL_DISABLE;
    SyncConfigData.dwCounterClearSelect       = IPU_DI_SW_GEN1_DI_CNT_CLR_SEL_COUNTER_5;                    
    SyncConfigData.bCounterAutoReload         = 0;
    SyncConfigData.dwPolarityGeneratorEnable  = IPU_DI_SW_GEN1_DI_CNT_POLARITY_GEN_EN_DISABLE;
    SyncConfigData.dwUpPos                    = 0;   
    SyncConfigData.dwDownPos                  = 0;
     
    SyncConfigData.dwStepRepeat               = pDIPanelInfo->WIDTH; 
    SyncConfigData.dwGentimeSelect            = 0;

    DIConfigureSync(di_sel, &SyncConfigData);


    // COUNTER_9 for VSYNC for field1 only
    SyncConfigData.dwPointer                  = DI_TVE_COUNTER_NVSYNC; 
    SyncConfigData.dwRunValue                 = pDIPanelInfo->VSTARTWIDTH_FIELD0 +
                                                pDIPanelInfo->VENDWIDTH_FIELD0   +
                                                pDIPanelInfo->HEIGHT             +
                                                pDIPanelInfo->VSTARTWIDTH_FIELD1 +
                                                pDIPanelInfo->VENDWIDTH_FIELD1 - 1; 

    SyncConfigData.dwRunResolution            = IPU_DI_SW_GEN0_DI_RUN_RESOLUTION_COUNTER_1;                   
    SyncConfigData.dwOffsetValue              = pDIPanelInfo->VSTARTWIDTH_FIELD1 +
                                                pDIPanelInfo->HEIGHT/2 +
                                                pDIPanelInfo->VENDWIDTH_FIELD0;
                                          
    SyncConfigData.dwOffsetResolution         = IPU_DI_SW_GEN0_DI_OFFSET_RESOLUTION_COUNTER_1;                  
    SyncConfigData.dwPolarityClearSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_CLR_SEL_INVERTED;
    SyncConfigData.dwToggleTriggerSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_TRIGGER_SEL_DISABLE;
    SyncConfigData.dwCounterClearSelect       = IPU_DI_SW_GEN1_DI_CNT_CLR_SEL_COUNTER_2;                                   
    SyncConfigData.bCounterAutoReload         = IPU_DI_SW_GEN1_DI_CNT_AUTO_RELOAD_FOREVER;
    SyncConfigData.dwPolarityGeneratorEnable  = IPU_DI_SW_GEN1_DI_CNT_POLARITY_GEN_EN_DISABLE;
    SyncConfigData.dwUpPos                    = 0;   
    SyncConfigData.dwDownPos                  = 2;
    SyncConfigData.dwStepRepeat               = 0;
    SyncConfigData.dwGentimeSelect            = 2; // 3-1:connect with wave form of generator 3              

    DIConfigureSync(di_sel, &SyncConfigData);


    // Set DI Sync start line and select VSYNC counter
    DISetSyncStart(di_sel, 0x2);       // 2 lines predictions
    DIVSyncCounterSelect(di_sel, 0x6); // PIN7 as VSYNC


    // Configure DI General data register
    genConfData.dwLineCounterSelect = 1;
    
    genConfData.dwClockStopMode = IPU_DI_GENERAL_DI_CLOCK_STOP_MODE_NEXT_EDGE; 
    genConfData.dwDisplayClockInitMode = IPU_DI_GENERAL_DI_DISP_CLOCK_INIT_STOPPED;
    genConfData.dwMaskSelect = IPU_DI_GENERAL_DI_MASK_SEL_COUNTER_2;
    genConfData.dwVSyncSource = IPU_DI_GENERAL_DI_VSYNC_EXT_EXTERNAL;
    genConfData.dwClkSource = IPU_DI_GENERAL_DI_CLK_EXT_EXTERNAL;      // select clock from TVE as display clock
    genConfData.dwWatchdogMode = IPU_DI_GENERAL_DI_WATCHDOG_MODE_4_CYCLES;
    genConfData.dwClkPolarity = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW; // VIDEO_DATA_CLK POLARITY
    genConfData.dwSyncFlowCounterSelect = 0; 
    genConfData.dwSyncFlowErrorTreatment = IPU_DI_GENERAL_DI_ERR_TREATMENT_DRIVE_LAST_COMPONENT; 
    genConfData.dwERMVSyncSelect = IPU_DI_GENERAL_DI_VSYNC_EXT_INTERNAL;
    genConfData.dwCS1Polarity = 0; 
    genConfData.dwCS0Polarity = 0; 
    genConfData.dwPinPolarity[0] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW;
    genConfData.dwPinPolarity[1] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH; // VSYNC polarity
    genConfData.dwPinPolarity[2] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH; // HSYNC Polarity
    genConfData.dwPinPolarity[3] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    genConfData.dwPinPolarity[4] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    genConfData.dwPinPolarity[5] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW;
    genConfData.dwPinPolarity[6] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW;
    genConfData.dwPinPolarity[7] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
 
    DIConfigureGeneral(di_sel, &genConfData); 


    // Set DI Polarity Configure data
    polConfData.PolaritySetSelect = DI_POLARITY_SET_DRDY;
    polConfData.DataPolarity   = 0; 
    polConfData.PinPolarity[0] = 0;
    polConfData.PinPolarity[1] = 0;
    polConfData.PinPolarity[2] = 0;
    polConfData.PinPolarity[3] = 0; 
    polConfData.PinPolarity[4] = 1; // VIDEO_DATA_EN POLARITY
    polConfData.PinPolarity[5] = 0;
    polConfData.PinPolarity[6] = 0;
    polConfData.WaitPolarity = 0;
    polConfData.CS1ByteEnablePolarity = 0;
    polConfData.CS0ByteEnablePolarity = 0;

    DIConfigurePolarity(di_sel, &polConfData);

}

//------------------------------------------------------------------------------
//
// Function: SyncDIConfigTV2P
//
// This function configures the DI for a synchronous panel taking progressive data 
// for TV 720P/1080P.
//
// Parameters:
//      di_sel
//          [in] Selects either DI0 or DI1 to configure
//
//      pDIPanelInfo
//          [in] Panel info used in configuration process.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void SyncDIConfigTV2P(DI_SELECT di_sel, PANEL_INFO* pDIPanelInfo)
{
    DIPointerConfigData ptrConfData;
    DIUpDownConfigData upDownConfData;
    DIPolarityConfigData polConfData;
    DIGeneralConfigData genConfData;
    DISyncConfigData SyncConfigData;

    // Set DI Pointer configuration data 
    ptrConfData.dwPointer         = DI_TVE_WAVEFORM;
    ptrConfData.dwAccessPeriod    = (UINT32)((LONGLONG)1000000000/pDIPanelInfo->PIX_CLK_FREQ + 1);
    ptrConfData.dwComponentPeriod = (UINT32)((LONGLONG)1000000000/pDIPanelInfo->PIX_CLK_FREQ + 1);
    ptrConfData.dwCSPtr           = 0;
    ptrConfData.dwPinPtr[0]       = 0;
    ptrConfData.dwPinPtr[1]       = 0;
    ptrConfData.dwPinPtr[2]       = 0;
    ptrConfData.dwPinPtr[3]       = 0;
    ptrConfData.dwPinPtr[4]       = 3;
    ptrConfData.dwPinPtr[5]       = 0;
    ptrConfData.dwPinPtr[6]       = 0;

    DIConfigurePointer(di_sel, &ptrConfData);

    // Set DI Up and Down configuration data   
    upDownConfData.dwPointer = DI_TVE_WAVEFORM;
    upDownConfData.dwSet     = 0;
    upDownConfData.dwUpPos   = 0;
    upDownConfData.dwDownPos = 0;

    DIConfigureUpDown(di_sel, &upDownConfData);

    // Set DI Up and Down configuration data 
    upDownConfData.dwPointer = DI_TVE_WAVEFORM;
    upDownConfData.dwSet     = 1;
    upDownConfData.dwUpPos   = 0;
    upDownConfData.dwDownPos = 0;

    DIConfigureUpDown(di_sel, &upDownConfData);

    // Set DI Up and Down configuration data 
    upDownConfData.dwPointer = DI_TVE_WAVEFORM;
    upDownConfData.dwSet     = 2;
    upDownConfData.dwUpPos   = 0;
    upDownConfData.dwDownPos = 0;

    DIConfigureUpDown(di_sel, &upDownConfData);

    // Set DI Up and Down configuration data   
    upDownConfData.dwPointer = DI_TVE_WAVEFORM;
    upDownConfData.dwSet     = 3;
    upDownConfData.dwUpPos   = 0;
    upDownConfData.dwDownPos = (UINT32)((LONGLONG)1000000000*2/pDIPanelInfo->PIX_CLK_FREQ) + 1;

    DIConfigureUpDown(di_sel, &upDownConfData);

    DIConfigureBaseSyncClockGen(di_sel,
                                pDIPanelInfo->PIX_DATA_POS, 
                                pDIPanelInfo->PIX_CLK_FREQ,
                                pDIPanelInfo->PIX_CLK_UP,
                                pDIPanelInfo->PIX_CLK_DOWN);

    // Configure DI1 Screen Hight
    DISetScreenHeight(di_sel, pDIPanelInfo->VSYNCWIDTH +
                              pDIPanelInfo->VSTARTWIDTH + 
                              pDIPanelInfo->HEIGHT + 
                              pDIPanelInfo->VENDWIDTH - 1);

    // Clean up all 9 DI1 COUNTERS.
    for (int i=1; i<=9; i++)
    {
        SyncConfigData.dwPointer                  = i;
        SyncConfigData.dwRunValue                 = 0;
                                                
        SyncConfigData.dwRunResolution            = 0;
        SyncConfigData.dwOffsetValue              = 0;
        SyncConfigData.dwOffsetResolution         = 0;
        SyncConfigData.dwPolarityClearSelect      = 0;
        SyncConfigData.dwToggleTriggerSelect      = 0;
        SyncConfigData.dwCounterClearSelect       = 0;
        SyncConfigData.bCounterAutoReload         = 0;
        SyncConfigData.dwPolarityGeneratorEnable  = 0;
        SyncConfigData.dwUpPos                    = 0;   
        SyncConfigData.dwDownPos                  = 0;
        SyncConfigData.dwStepRepeat               = 0; 
        SyncConfigData.dwGentimeSelect            = 0;

        DIConfigureSync(di_sel, &SyncConfigData);
    }
    
    // COUNTER_2
    SyncConfigData.dwPointer                  = DI_TVE_COUNTER_F1VSYNC;
    SyncConfigData.dwRunValue                 = pDIPanelInfo->HSTARTWIDTH +
                                                pDIPanelInfo->HSYNCWIDTH +
                                                pDIPanelInfo->WIDTH + 
                                                pDIPanelInfo->HENDWIDTH - 1;
    SyncConfigData.dwRunResolution            = IPU_DI_SW_GEN0_DI_RUN_RESOLUTION_DISP_CLK;
    SyncConfigData.dwOffsetValue              = 0;
    SyncConfigData.dwOffsetResolution         = IPU_DI_SW_GEN0_DI_OFFSET_RESOLUTION_DISABLE;
    SyncConfigData.dwPolarityClearSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_CLR_SEL_INVERTED;
    SyncConfigData.dwToggleTriggerSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_TRIGGER_SEL_DISABLE;
    SyncConfigData.dwCounterClearSelect       = IPU_DI_SW_GEN1_DI_CNT_CLR_SEL_DISABLE;
    SyncConfigData.bCounterAutoReload         = 1;
    SyncConfigData.dwPolarityGeneratorEnable  = IPU_DI_SW_GEN1_DI_CNT_POLARITY_GEN_EN_DISABLE;
    SyncConfigData.dwUpPos                    = 0;   
    SyncConfigData.dwDownPos                  = 1;
    SyncConfigData.dwStepRepeat               = 0; 
    SyncConfigData.dwGentimeSelect            = 0;

    DIConfigureSync(di_sel, &SyncConfigData);

    
    // COUNTER_3
    SyncConfigData.dwPointer                  = DI_TVE_COUNTER_IHSYNC;
    SyncConfigData.dwRunValue                 = pDIPanelInfo->VSYNCWIDTH +
                                                pDIPanelInfo->VSTARTWIDTH +
                                                pDIPanelInfo->HEIGHT + 
                                                pDIPanelInfo->VENDWIDTH - 1;
    SyncConfigData.dwRunResolution            = IPU_DI_SW_GEN0_DI_RUN_RESOLUTION_COUNTER_2;
    SyncConfigData.dwOffsetValue              = 0; 
    SyncConfigData.dwOffsetResolution         = IPU_DI_SW_GEN0_DI_OFFSET_RESOLUTION_DISABLE;   
    SyncConfigData.dwPolarityClearSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_GEN_EN_DISABLE;
    SyncConfigData.dwToggleTriggerSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_TRIGGER_SEL_DISABLE;
    SyncConfigData.dwCounterClearSelect       = IPU_DI_SW_GEN1_DI_CNT_CLR_SEL_DISABLE;
    SyncConfigData.bCounterAutoReload         = IPU_DI_SW_GEN1_DI_CNT_AUTO_RELOAD_FOREVER;
    SyncConfigData.dwPolarityGeneratorEnable  = IPU_DI_SW_GEN1_DI_CNT_POLARITY_GEN_EN_DISABLE;
    SyncConfigData.dwUpPos                    = 0;   
    SyncConfigData.dwDownPos                  = 1;
    SyncConfigData.dwStepRepeat               = 0;
    SyncConfigData.dwGentimeSelect            = 0;
  
    DIConfigureSync(di_sel, &SyncConfigData);



    // COUNTER_5 for active lines
    SyncConfigData.dwPointer                  = DI_TVE_COUNTER_ALINE;
    SyncConfigData.dwRunValue                 = 0;   
    SyncConfigData.dwRunResolution            = IPU_DI_SW_GEN0_DI_OFFSET_RESOLUTION_COUNTER_2;
    SyncConfigData.dwOffsetValue              = pDIPanelInfo->VSYNCWIDTH +
                                                pDIPanelInfo->VSTARTWIDTH - 1;                                              
    SyncConfigData.dwOffsetResolution         = IPU_DI_SW_GEN0_DI_OFFSET_RESOLUTION_COUNTER_2;
    SyncConfigData.dwPolarityClearSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_CLR_SEL_INVERTED;
    SyncConfigData.dwToggleTriggerSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_TRIGGER_SEL_DISABLE;
    SyncConfigData.dwCounterClearSelect       = IPU_DI_SW_GEN1_DI_CNT_CLR_SEL_COUNTER_3;
    SyncConfigData.bCounterAutoReload         = 0;
    SyncConfigData.dwPolarityGeneratorEnable  = IPU_DI_SW_GEN1_DI_CNT_POLARITY_GEN_EN_DISABLE;
    SyncConfigData.dwUpPos                    = 0;   
    SyncConfigData.dwDownPos                  = 0; 
    SyncConfigData.dwStepRepeat               = pDIPanelInfo->HEIGHT;
    SyncConfigData.dwGentimeSelect            = 0;

    DIConfigureSync(di_sel, &SyncConfigData);

    

    // COUNTER_8 for active pixel out
    SyncConfigData.dwPointer                  = DI_TVE_COUNTER_APIXEL;
    SyncConfigData.dwRunValue                 = 0;                    
    SyncConfigData.dwRunResolution            = IPU_DI_SW_GEN0_DI_RUN_RESOLUTION_DISP_CLK; 
    SyncConfigData.dwOffsetValue              = pDIPanelInfo->HSTARTWIDTH +       
                                                pDIPanelInfo->HSYNCWIDTH;                                       
    SyncConfigData.dwOffsetResolution         = IPU_DI_SW_GEN0_DI_OFFSET_RESOLUTION_DISP_CLK;  
    SyncConfigData.dwPolarityClearSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_CLR_SEL_INVERTED;
    SyncConfigData.dwToggleTriggerSelect      = IPU_DI_SW_GEN1_DI_CNT_POLARITY_TRIGGER_SEL_DISABLE;
    SyncConfigData.dwCounterClearSelect       = IPU_DI_SW_GEN1_DI_CNT_CLR_SEL_COUNTER_5;                     
    SyncConfigData.bCounterAutoReload         = 0;
    SyncConfigData.dwPolarityGeneratorEnable  = IPU_DI_SW_GEN1_DI_CNT_POLARITY_GEN_EN_DISABLE;
    SyncConfigData.dwUpPos                    = 0;   
    SyncConfigData.dwDownPos                  = 0; 
    SyncConfigData.dwStepRepeat               = pDIPanelInfo->WIDTH; 
    SyncConfigData.dwGentimeSelect            = 0;

    DIConfigureSync(di_sel, &SyncConfigData);


    // Set DI Sync start line and select VSYNC counter
    DISetSyncStart(di_sel, 0x2);       // 2 lines predictions
    DIVSyncCounterSelect(di_sel, 0x2); // PIN3 as VSYNC


    // Configure DI General data register
    genConfData.dwLineCounterSelect = 1; // PIN2 as HSYNC
    genConfData.dwClockStopMode = IPU_DI_GENERAL_DI_CLOCK_STOP_MODE_NEXT_EDGE; 
    genConfData.dwDisplayClockInitMode = IPU_DI_GENERAL_DI_DISP_CLOCK_INIT_STOPPED;
    genConfData.dwMaskSelect = IPU_DI_GENERAL_DI_MASK_SEL_COUNTER_2;
    genConfData.dwVSyncSource = IPU_DI_GENERAL_DI_VSYNC_EXT_EXTERNAL;
    genConfData.dwClkSource = IPU_DI_GENERAL_DI_CLK_EXT_EXTERNAL;      // select clock from TVE as display clock
    genConfData.dwWatchdogMode = IPU_DI_GENERAL_DI_WATCHDOG_MODE_4_CYCLES;
    genConfData.dwClkPolarity = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW;
    genConfData.dwSyncFlowCounterSelect = 0; 
    genConfData.dwSyncFlowErrorTreatment = IPU_DI_GENERAL_DI_ERR_TREATMENT_DRIVE_LAST_COMPONENT; 
    genConfData.dwERMVSyncSelect = IPU_DI_GENERAL_DI_VSYNC_EXT_INTERNAL; 
    genConfData.dwCS1Polarity = 0; 
    genConfData.dwCS0Polarity = 0; 
    genConfData.dwPinPolarity[0] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW;
    genConfData.dwPinPolarity[1] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH; 
    genConfData.dwPinPolarity[2] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH; 
    genConfData.dwPinPolarity[3] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    genConfData.dwPinPolarity[4] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    genConfData.dwPinPolarity[5] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW;
    genConfData.dwPinPolarity[6] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW;
    genConfData.dwPinPolarity[7] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
 
    DIConfigureGeneral(di_sel, &genConfData); 


    // Set DI Polarity Configure data
    polConfData.PolaritySetSelect = DI_POLARITY_SET_DRDY;
    polConfData.DataPolarity   = 0; 
    polConfData.PinPolarity[0] = 0;
    polConfData.PinPolarity[1] = 0;
    polConfData.PinPolarity[2] = 0;
    polConfData.PinPolarity[3] = 0; 
    polConfData.PinPolarity[4] = 1; // VIDEO_DATA_EN POLARITY
    polConfData.PinPolarity[5] = 0;
    polConfData.PinPolarity[6] = 0;
    polConfData.WaitPolarity = 0;
    polConfData.CS1ByteEnablePolarity = 0;
    polConfData.CS0ByteEnablePolarity = 0;

    DIConfigurePolarity(di_sel, &polConfData);

}


//------------------------------------------------------------------------------
//
// Function: SerialDIConfig
//
// This function configures the DI for IPU serial communication with a panel.
//
// Parameters:
//      di_sel
//          [in] Selects either DI0 or DI1 to configure
//
//      pDIPanelInfo
//          [in] Panel info used in configuration process.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void SerialDIConfig(DI_SELECT di_sel, PANEL_INFO* pDIPanelInfo)
{
    DIUpDownConfigData upDownConfData;
    DIPointerConfigSerialData ptrConfSerData;
    DISerialConfigData serConfData;
    DIGeneralConfigData genConfData;

    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(pDIPanelInfo);

    // Configure DI General data register
    genConfData.dwLineCounterSelect = 0;
    genConfData.dwClockStopMode = IPU_DI_GENERAL_DI_CLOCK_STOP_MODE_NEXT_EDGE;//0 
    genConfData.dwDisplayClockInitMode = IPU_DI_GENERAL_DI_DISP_CLOCK_INIT_STOPPED;//0
    genConfData.dwMaskSelect = IPU_DI_GENERAL_DI_MASK_SEL_COUNTER_2;//0 
    genConfData.dwVSyncSource = IPU_DI_GENERAL_DI_VSYNC_EXT_INTERNAL;// 0
    genConfData.dwClkSource = IPU_DI_GENERAL_DI_CLK_EXT_INTERNAL;      // select clock from TVE as display clock
    genConfData.dwWatchdogMode = IPU_DI_GENERAL_DI_WATCHDOG_MODE_4_CYCLES;//0 
    genConfData.dwClkPolarity = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW; // VIDEO_DATA_CLK POLARITY
    genConfData.dwSyncFlowCounterSelect = 0; 
    genConfData.dwSyncFlowErrorTreatment = IPU_DI_GENERAL_DI_ERR_TREATMENT_DRIVE_LAST_COMPONENT;//0 
    genConfData.dwERMVSyncSelect = IPU_DI_GENERAL_DI_VSYNC_EXT_INTERNAL;//0 
    genConfData.dwCS1Polarity = 0; 
    genConfData.dwCS0Polarity = 0; 
    genConfData.dwPinPolarity[0] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW;
    genConfData.dwPinPolarity[1] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW; 
    genConfData.dwPinPolarity[2] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW; 
    genConfData.dwPinPolarity[3] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH; //Set di2_pin4 reset pin to high
    genConfData.dwPinPolarity[4] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW;
    genConfData.dwPinPolarity[5] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW;
    genConfData.dwPinPolarity[6] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW;
    genConfData.dwPinPolarity[7] = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW;
    DIConfigureGeneral(DI_SELECT_DI1, &genConfData); 

    //*******************************************************
    // Serial Configuration for communication with display panel
    //*******************************************************

    // Signal waveform for command write
    upDownConfData.dwPointer  = DI_SERIAL_WAVEFORM;
    upDownConfData.dwSet  = DI_CS_SIGNAL;
    upDownConfData.dwUpPos = pDIPanelInfo->SERIAL_TIMING.dwCSUp;
    upDownConfData.dwDownPos = pDIPanelInfo->SERIAL_TIMING.dwCSDown;
    DIConfigureUpDown(di_sel, &upDownConfData);

    upDownConfData.dwSet  = DI_SER_CLK_SIGNAL;
    upDownConfData.dwUpPos = pDIPanelInfo->SERIAL_TIMING.dwClkUp;
    upDownConfData.dwDownPos = pDIPanelInfo->SERIAL_TIMING.dwClkDown;
    DIConfigureUpDown(di_sel, &upDownConfData);

    upDownConfData.dwSet  = DI_RS_SIGNAL;
    upDownConfData.dwUpPos = pDIPanelInfo->SERIAL_TIMING.dwRSUp;
    upDownConfData.dwDownPos = pDIPanelInfo->SERIAL_TIMING.dwRSDown;
    DIConfigureUpDown(di_sel, &upDownConfData);

    upDownConfData.dwSet  = DI_NOUSE_SIGNAL;  //NULL SIGNAL
    upDownConfData.dwUpPos = 0;
    upDownConfData.dwDownPos = 0;
    DIConfigureUpDown(di_sel, &upDownConfData);

    ptrConfSerData.dwPointer  = upDownConfData.dwPointer;
    ptrConfSerData.dwSerialPeriod =pDIPanelInfo->SERIAL_TIMING.dwSerClkPeriod;
    ptrConfSerData.dwStartPeriod =0;
    ptrConfSerData.dwCSPtr= DI_NOUSE_SIGNAL; // serial cs use gpio instead in TO2 CPU BOARD
    ptrConfSerData.dwSerialValidBits= 8;
    ptrConfSerData.dwSerialRSPtr= DI_RS_SIGNAL;   // serial RS
    ptrConfSerData.dwSerialClkPtr= DI_SER_CLK_SIGNAL;  // serial clk 
    DIConfigureSerialPointer(di_sel, &ptrConfSerData);

    serConfData.dwRS0ReadPtr = 0;
    serConfData.dwRS1ReadPtr = 0;
    serConfData.dwRS0WritePtr = 0;
    serConfData.dwRS1WritePtr = 0;
    serConfData.dwDI0SerLatchDelay = 0;
    serConfData.dwDirectSerLLA = 0;
    serConfData.dwSerClkPol = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_HIGH;
    serConfData.dwSerDataPol = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW;
    serConfData.dwSerRSPol = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW;
    serConfData.dwSerCSPol = IPU_DI_GENERAL_DI_POLARITY_ACTIVE_LOW;
    serConfData.dwWait4Serial = 0;
    DIConfigureSerial(di_sel, &serConfData);
}


//------------------------------------------------------------------------------
//
// Function: BSPEnableDCChannels
//
// This function performs panel-specific enabling of DC channels, by modifying
// the PROG_CHAN_TYP field of the DC Write Configuration registers.
//
// Parameters:
//      syncType
//          [in] Selects synchronous type used to control DC channels to enable.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void BSPEnableDCChannels(IPU_PANEL_SYNC_TYPE syncType)
{
    if (syncType == IPU_PANEL_SYNC_TYPE_ASYNCHRONOUS)
    {
        //********************************************************************
        // Configure Write Channel 1 and Channel 6 (main channel to write pixels to display)
        //********************************************************************
        DCChangeChannelMode(DC_CHANNEL_DC_SYNC_OR_ASYNC, 
            IPU_DC_WR_CH_CONF_PROG_CHAN_TYP_NORMAL_NO_ANTI_TEARING);
        DCChangeChannelMode(DC_CHANNEL_DP_SECONDARY, 
            IPU_DC_WR_CH_CONF_PROG_CHAN_TYP_NORMAL_NO_ANTI_TEARING);
    }
    else if (syncType == IPU_PANEL_SYNC_TYPE_SYNCHRONOUS)   
    {
        //********************************************************************
        // Configure Write Channel 5(main channel to write pixels to display)
        //********************************************************************
        DCChangeChannelMode(DC_CHANNEL_DP_PRIMARY, 
            IPU_DC_WR_CH_CONF_PROG_CHAN_TYP_NORMAL_NO_ANTI_TEARING);
    }

}


//------------------------------------------------------------------------------
//
// Function: BSPDisableDCChannels
//
// This function performs panel-specific disabling of DC channels, by modifying
// the PROG_CHAN_TYP field of the DC Write Configuration registers.
//
// Parameters:
//      syncType
//          [in] Selects synchronous type used to control DC channels to enable.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void BSPDisableDCChannels(IPU_PANEL_SYNC_TYPE syncType)
{
    if (syncType == IPU_PANEL_SYNC_TYPE_ASYNCHRONOUS)
    {
        //********************************************************************
        // Configure Write Channel 1 and Channel 6 (main channel to write pixels to display)
        //********************************************************************
        DCChangeChannelMode(DC_CHANNEL_DC_SYNC_OR_ASYNC, 
            IPU_DC_WR_CH_CONF_PROG_CHAN_TYP_DISABLE);
        DCChangeChannelMode(DC_CHANNEL_DP_SECONDARY, 
            IPU_DC_WR_CH_CONF_PROG_CHAN_TYP_DISABLE);
    }
    else if (syncType == IPU_PANEL_SYNC_TYPE_SYNCHRONOUS)   
    {
        //********************************************************************
        // Configure Write Channel 5(main channel to write pixels to display)
        //********************************************************************
        DCChangeChannelMode(DC_CHANNEL_DP_PRIMARY, 
            IPU_DC_WR_CH_CONF_PROG_CHAN_TYP_DISABLE);
    }
}

