//------------------------------------------------------------------------------
//
//  Copyright (C) 2007-2010, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  bsppp.c
//
//  Provides BSP-specific configuration routines for the Post-processing peripheral.
//
//------------------------------------------------------------------------------

#pragma warning(push)
#pragma warning(disable: 4115 4201 4204 4214)
#include <windows.h>
#pragma warning(pop)

//#include "bsp.h"

//------------------------------------------------------------------------------
// External Functions


//------------------------------------------------------------------------------
// External Variables


//------------------------------------------------------------------------------
// Defines


//------------------------------------------------------------------------------
// Types


//------------------------------------------------------------------------------
// Global Variables


//------------------------------------------------------------------------------
// Local Variables

//------------------------------------------------------------------------------
// Local Functions

//------------------------------------------------------------------------------
//
// Function: BSPSetPPISRPriority
//
// This function sets the thread priority for
// the Post-processor ISR.
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void BSPSetPPISRPriority()
{
    CeSetThreadPriority(GetCurrentThread(), 100);
    return;
}


//------------------------------------------------------------------------------
//
// Function: BSPGetYUV420OutPutSupport
//
// This function get the statue of IPU YUV420 output support
//
// Parameters:
//      None.
//
// Returns:
//      TRUE  :  support.
//      FALSE :  not support.
//
//------------------------------------------------------------------------------

BOOL BSPGetYUV420OutPutSupport()
{
    return TRUE;
}


