//------------------------------------------------------------------------------
//
//  Copyright (C) 2007-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  bspusdhcdma.cpp
//
//  Provides BSP-specific SDMA routines for use by USDHC driver.
//  For MX6, these functions are just stubs because SDMA is not implemented.
//  These functions will never get called because internal DMA will be used.
//
//------------------------------------------------------------------------------

#include "bsp.h"
#include "usdhc.h"
#include "usdhcdma.hpp"


//------------------------------------------------------------------------------
// External Functions


//------------------------------------------------------------------------------
// External Variables


//------------------------------------------------------------------------------
// Defines


//------------------------------------------------------------------------------
// Types


//------------------------------------------------------------------------------
// Global Variables

//------------------------------------------------------------------------------
// Local Variables


//------------------------------------------------------------------------------
// Local Functions

CUSDHCBaseEDMA::CUSDHCBaseEDMA(CUSDHCBase & SDHCSlotBase) : CUSDHCBaseDMA(SDHCSlotBase)
{
    
}

CUSDHCBaseEDMA::~CUSDHCBaseEDMA()
{

}

BOOL CUSDHCBaseEDMA::BspEDMAInit()
{
    return FALSE;
}


BOOL CUSDHCBaseEDMA::BspEDMAArm(SD_BUS_REQUEST& Request, BOOL fToDevice)
{
    UNREFERENCED_PARAMETER(Request);
    UNREFERENCED_PARAMETER(fToDevice);
    
    return FALSE;

}

BOOL CUSDHCBaseEDMA::BspEDMANotifyEvent(SD_BUS_REQUEST & Request, DMAEVENT dmaEvent)
{
    UNREFERENCED_PARAMETER(Request);
    UNREFERENCED_PARAMETER(dmaEvent);  

    return FALSE;

}


DMAEVENT CUSDHCBaseEDMA::BspEDMACheckCompletion(BOOL fToDevice)
{


    DMAEVENT dmaEvent = NO_DMA;

    UNREFERENCED_PARAMETER(fToDevice);
    
    return dmaEvent;

}

