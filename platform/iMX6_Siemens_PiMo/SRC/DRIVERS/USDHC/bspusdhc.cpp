//------------------------------------------------------------------------------
//
//  Copyright (C) 2012, Adeneo Embedded All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//  Copyright (C) 2007-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  bspesdhc.cpp
//
//  Provides BSP-specific routines for use by USDHC driver.
//
//------------------------------------------------------------------------------

#include "bsp.h"
#include "usdhc.h"
#include "pmic.h"

//------------------------------------------------------------------------------
// External Functions




//------------------------------------------------------------------------------
// External Variables

//------------------------------------------------------------------------------
// Defines
#define SHC_CONTROLLER_INDEX_KEY    TEXT("Index")
#define SHC_DISABLEDMA_KEY              TEXT("DisableDMA")
#define SHC_USE_EXTDMA_KEY              TEXT("UseExternalDMA")
#define SHC_SDIO_PRIORITY_KEY           TEXT("SDIOPriority")
#define SHC_FREQUENCY_KEY               TEXT("MaximumClockFrequency")
#define SHC_RW_TIMEOUT_KEY              TEXT("ReadWriteTimeout")
#define SHC_WAKEUP_SOURCE_KEY          TEXT("WakeupSource")
#define SHC_CDDET_OPT_KEY               TEXT("CardDetectOpt")
#define SHC_PCRM_INT_KEY                TEXT("PollingIntervalForRemoval")
#define SHC_PCINS_INT_KEY               TEXT("PollingIntervalForInsertion")

#define USDHC2_CD_GPIO_PORT     DDK_GPIO_PORT2
#define USDHC2_CD_GPIO_PIN      2

#define USDHC3_CD_GPIO_PORT     DDK_GPIO_PORT2
#define USDHC3_CD_GPIO_PIN      0

#define USDHC2_WP_GPIO_PORT     DDK_GPIO_PORT2
#define USDHC2_WP_GPIO_PIN      3

#define USDHC3_WP_GPIO_PORT     DDK_GPIO_PORT2
#define USDHC3_WP_GPIO_PIN      1

#define USDHC4_CD_GPIO_PORT     DDK_GPIO_PORT2
#define USDHC4_CD_GPIO_PIN      6

#define USDHC_DEBOUNCE_PERIOD  100    // 100 ms
#define USDHC_DEBOUNCE_CHECKS  2
#define USDHC_DEBOUNCE_TIMEOUT 1000 // 1000 ms

//------------------------------------------------------------------------------
// Types


//------------------------------------------------------------------------------
// Global Variables

// since same supply powers both slots on the 3DS, we need this global to determine when it is ok to turn power off
DWORD g_fCardPresent = 0;
// clock gating globals for all 4 USDHC controllers
BOOL g_fClkGatingSupported[4] = {FALSE, FALSE, FALSE, FALSE};
BOOL g_fClkUngated[4] = {FALSE, FALSE, FALSE, FALSE};

DWORD g_dwVoltage = 0;


//------------------------------------------------------------------------------
// Local Variables


//------------------------------------------------------------------------------
// Local Functions


// Read the registry settings
BOOL CUSDHCBase::BspGetRegistrySettings( CReg *pReg )
{
    BOOL fRet = TRUE;

    DEBUGCHK(pReg);

    // get the controller index (instance of USDHC in the SOC: 1,2, 3, or 4)
    m_dwControllerIndex = pReg->ValueDW(SHC_CONTROLLER_INDEX_KEY);
    if (m_dwControllerIndex == 0)
    {
        DEBUGMSG(SDCARD_ZONE_ERROR, (L"CUSDHCBase::GetRegistrySettings: No controller index found. Can't load USDHC driver.\r\n"));
        fRet = FALSE;
        goto EXIT;
    }

    // Should we use any form of DMA?
    m_fDisableDMA = pReg->ValueDW(SHC_DISABLEDMA_KEY, 0);

    // For MX6, External DMA (SDMA) software is not implemented, so ignore any registry setting
    m_fUseExternalDMA = 0;

    // get the controller IST thread priority
    m_dwSDIOPriority = pReg->ValueDW(SHC_SDIO_PRIORITY_KEY, SHC_CARD_CONTROLLER_PRIORITY);

    // get the max clock frequency from the registry (we allow the registry to override)
    m_dwMaxClockRate = pReg->ValueDW(SHC_FREQUENCY_KEY);
    if (m_dwMaxClockRate == 0) 
    {
        // PERCLK needs to be at either 50 MHz or multiple of 50 for high speed (50 Mhz) SD clock
        m_dwMaxClockRate = min(BspUSDHCGetBaseClk(), USDHC_MAX_CLOCK_RATE); 
    }

    // get the read/write timeout value
    m_dwMaxTimeout = pReg->ValueDW(SHC_RW_TIMEOUT_KEY, DEFAULT_TIMEOUT_VALUE);

    // get the wakeup sources
    m_fWakeupSource = pReg->ValueDW(SHC_WAKEUP_SOURCE_KEY, 0);

    // get the card detect option
    // 0: using GPIO or CD pin
    // 1: using DAT3 
    // 2: using Polling
    m_dwCdOpt = pReg->ValueDW(SHC_CDDET_OPT_KEY, 0);

    // get the interval of polling removal while card exists.
    m_dwPollingRmInt = pReg->ValueDW(SHC_PCRM_INT_KEY, 5);

    // get the interval of polling insertion while card exists.
    m_dwPollingInsInt = pReg->ValueDW(SHC_PCINS_INT_KEY, 1);

    //m_fSDIOReset4CMD53 = TRUE;
EXIT:
    return fRet;
}


// Get the base clock of the USDHC module
ULONG CUSDHCBase::BspUSDHCGetBaseClk()
{
    UINT32 ulBaseFreq = 0;
    DDK_CLOCK_SIGNAL index = DDK_CLOCK_SIGNAL_ENUM_END;
    
    switch (m_dwControllerIndex)
    {
        case 1:
            index = DDK_CLOCK_SIGNAL_USDHC1;
            break;
                
        case 2:
            index = DDK_CLOCK_SIGNAL_USDHC2;
            break;

        case 3:
            index = DDK_CLOCK_SIGNAL_USDHC3;
            break;

        case 4:
            index = DDK_CLOCK_SIGNAL_USDHC4;
            break;


        default:
            goto EXIT;
            break;

    }

    DDKClockGetFreq(index, &ulBaseFreq);

EXIT:    
    return ulBaseFreq;

}

// Initialize the IOMux signals for particular USDHC module
BOOL CUSDHCBase::BspUSDHCInit()
{
    BOOL fRet = TRUE;

    switch (m_dwControllerIndex)
    {
        case 2: // µSD socket on board 4-bit, no write protect pin
			// TC : IOMux deleted because the configuration is made in the bootloader

			// initially, configure interrupt for low level (to detect an already inserted card)
            DDKGpioSetConfig(USDHC2_CD_GPIO_PORT, USDHC2_CD_GPIO_PIN, DDK_GPIO_DIR_IN, DDK_GPIO_INTR_LOW_LEV);

            #if BSP_CLK_GATING_BETWEEN_CMDS_SDHC2
                g_fClkGatingSupported[m_dwControllerIndex-1] = TRUE;
            #endif

			break;
		case 3: // SD 4-bit
			// TC : IOMux deleted because the configuration is made in the bootloader

			// initially, configure interrupt for low level (to detect an already inserted card)
            DDKGpioSetConfig(USDHC3_CD_GPIO_PORT, USDHC3_CD_GPIO_PIN, DDK_GPIO_DIR_IN, DDK_GPIO_INTR_LOW_LEV);

            #if BSP_CLK_GATING_BETWEEN_CMDS_SDHC3
                g_fClkGatingSupported[m_dwControllerIndex-1] = TRUE;
            #endif

            break;
        case 4: // eMMC 8GB onboard 8-bit, no card detect pin, no write protect pin
			// TC : IOMux deleted because the configuration is made in the bootloader
            #if BSP_CLK_GATING_BETWEEN_CMDS_SDHC4
                g_fClkGatingSupported[m_dwControllerIndex-1] = TRUE;
            #endif

            break;
        default:
            fRet = FALSE;
            goto EXIT;
            break;

    }


    // since CD pin is not muxed to the controller, use DAT[3] to detect card insertion
    // this will alow SDIO interrupts to propagate through the controller
    if (m_bDat3AsCd || m_bNeedCDPolling)
    {
        RETAILMSG(m_bDat3AsCd, (L"SDHC[%d] is using DAT3 as card detect pin\r\n", m_dwControllerIndex));
        RETAILMSG(m_bNeedCDPolling, (L"SDHC[%d] is polling card detection\r\n", m_dwControllerIndex));
        INSREG32BF(&m_pUSDHCReg->PROCTL, USDHC_PROCTL_D3CD, 1); 
    }

EXIT:

    return fRet;
}


// Do de-init at BSP level if needed
VOID CUSDHCBase::BspUSDHCDeinit()
{
    // For now, nothing to do
}

// Multiple IRQs (upto 4) can be mapped to same SYSINTR (for eg, besides the SD Controller IRQ, can also add GPIO IRQ)
BOOL CUSDHCBase::BspUSDHCSysIntrSetup()
{
    BOOL fRet = FALSE;
    DWORD dwIrqs[6]; // in case we need to map multiple IRQs, including SDMA to one SYSINTR
    DWORD dwIrqsSize = sizeof(dwIrqs);

    // append SDMA IRQs to the end of the array
    if (m_fUseExternalDMA && m_SlotDma)
    {
        dwIrqs[4] = m_dwEDMAChanTx + IRQ_SDMA_CH0;
        dwIrqs[5] = m_dwEDMAChanRx + IRQ_SDMA_CH0;
    }

    else
    {
        // do not map SDMA IRQs or other GPIO irq
        dwIrqsSize -= sizeof(DWORD) * 2;
    }

    // Using -1 indicates we are not using the legacy calling convention
    dwIrqs[0] = (DWORD) -1;
    // Flags: in this case we want the existing sysintr if it
    // has already been allocated, and a new sysintr otherwise.
    dwIrqs[1] = OAL_INTR_TRANSLATE;

    switch(m_dwControllerIndex)
    {
        case 1:
            // Now, let's add the controller IRQ
            dwIrqs[2] = IRQ_USDHC1;
            // card detect IRQ is needed since it is configured as GPIO
            //dwIrqs[3] = IRQ_GPIO1_PIN1;
            dwIrqsSize -= sizeof(DWORD);
            break;
        case 2:
            // Now, let's add the controller IRQ
            dwIrqs[2] = IRQ_USDHC2;
            // card detect IRQ is needed since it is configured as GPIO
            dwIrqs[3] = IRQ_GPIO2_PIN2;
            break;
        case 3:
            // Now, let's add the controller IRQ
            dwIrqs[2] = IRQ_USDHC3;
            // card detect IRQ is needed since it is configured as GPIO
            dwIrqs[3] = IRQ_GPIO2_PIN0;
            break;
        case 4:
            // Now, let's add the controller IRQ
            dwIrqs[2] = IRQ_USDHC4;
            // card detect IRQ is needed since it is configured as GPIO
            //dwIrqs[3] = IRQ_GPIO2_PIN6;
            dwIrqsSize -= sizeof(DWORD);
            break;
        default:
            goto EXIT;
            break;
    }

    // convert the hardware IRQs for USDHC into a logical SYSINTR value
    if (KernelIoControl(IOCTL_HAL_REQUEST_SYSINTR, dwIrqs, dwIrqsSize, &m_dwControllerSysIntr, sizeof(DWORD), NULL))
    {
        fRet = TRUE;
    }

    // enable wakeup on sd card insertion/removal: since sysintr is wakeup source, all associated IRQs can enable wakeup, but
    // other IRQs (such as IRQ_USDHC) will not trigger when system is suspended, so only GPIO interrupt (for insertion/removal) will wakeup system
    if (fRet && m_fWakeupSource)
    {
        KernelIoControl(IOCTL_HAL_ENABLE_WAKE, &m_dwControllerSysIntr, sizeof(m_dwControllerSysIntr), NULL, 0, NULL);
    }
    

EXIT:
    return fRet;

}

VOID CUSDHCBase::BspUSDHCSetClockGating(DWORD dwPowerState)
{
    DDK_CLOCK_GATE_INDEX index;

    switch (m_dwControllerIndex)
    {
        case 1:
            index = DDK_CLOCK_GATE_INDEX_USDHC1;
            break;
                
        case 2:
            index = DDK_CLOCK_GATE_INDEX_USDHC2;
            break;

        case 3:
            index = DDK_CLOCK_GATE_INDEX_USDHC3;
            break;
            
        case 4:
            index = DDK_CLOCK_GATE_INDEX_USDHC4;
            break;

        default:
            goto EXIT;
            break;
    }

    switch(dwPowerState)
    {
        case D0:
            // enable clocks if clock gating is supported on this slot, or if this is the first time so the clocks have to be ungated
            if (g_fClkGatingSupported[m_dwControllerIndex-1] || !g_fClkUngated[m_dwControllerIndex-1])
            {
                // enable clocks for memory cards (!m_fCardIsSDIO), or for SDIO and memory cards if the card was just inserted
                // we cannot do clock gating at CCM for SDIO cards because it will prevent wakeup on CINT
                if (!m_fCardIsSDIO || (m_fCardPresent && BspUSDHCSlotStatusChanged()))
                {
                    // we cannot use auto-clock-gating mode because CLSL will be stuck at 0 after multi-block writes, causing cmd errors 
                    // for the commands following the multi-block write.            
                    ClockGateOff();
                    
                    DDKClockSetGatingMode(index, DDK_CLOCK_GATE_MODE_ENABLED_ALL);

                    g_fClkUngated[m_dwControllerIndex-1] = TRUE;
                }
            }
            break;

        case D1:
        case D2:
        case D3:
        case D4:
            if (g_fClkGatingSupported[m_dwControllerIndex-1])
            {
                // disable clocks for memory cards (!m_fCardIsSDIO), or for SDIO and memory cards if the card was just removed
                // we cannot do clock gating at CCM for SDIO cards because it will prevent wakeup on CINT
                if ( !m_fCardIsSDIO ||  !m_fCardPresent)
                {
                    // turn on auto-clock gating mode
                    ClockGateOn();
                    DDKClockSetGatingMode(index, DDK_CLOCK_GATE_MODE_DISABLED);
                }
            }
            break;

        default:
            break;

    }

EXIT:
    return;
}


//PMIC is not populated on the board REV A1 so nothing 
//is done by this function.
VOID CUSDHCBase::BspUSDHCSetSlotVoltage(DWORD dwVoltage)
{
    PMIC_STATUS status;    

    // if the voltage is already set to desired value, do nothing
    if (g_dwVoltage == dwVoltage)
        goto exit;

    // for now, just turn on or off the 3V supply from PMIC
    switch (dwVoltage)
    {
        case 0:
            // only turn off power when no card is present in either slot, or only this slot has card inserted in it
            if (g_fCardPresent == 0 || ( g_fCardPresent == (DWORD) (1 << (m_dwControllerIndex - 1)) ) )
            {
                //FIXME: workaround
                // Turn off PMIC MC13892's SD power supply (VGEN2 powers both slots on 3DS)
                // Let PMIC driver determine if supply is shared with other peripherals
                //status = PmicPowerRelease (L"USDHC1");
                status = PMIC_SUCCESS;

                if (status != PMIC_SUCCESS)
                    ERRORMSG(1, (TEXT("BspUSDHCSetSlotVoltage:  PmicVoltageRegulatorOff failed\r\n")));
                else
                    g_dwVoltage = dwVoltage;
            }
            else
                DEBUGMSG(1, (TEXT("BspUSDHCSetSlotVoltage:  Cannot turn off the voltage, card is inserted in at least 1 slot: 0x%x\r\n"), g_fCardPresent));                
            
            break;


        // all other voltage levels will for now be defaulted to 3 Volts.
        default:

           
            //FIXME: workaround
            //status = PmicPowerRequest(L"USDHC1");
            status = PMIC_SUCCESS;

            if (status != PMIC_SUCCESS)
                ERRORMSG(1, (TEXT("BspUSDHCSetSlotVoltage:  PmicVoltageRegulatorOn failed\r\n")));                

            // allow power supply to ramp-up (1000 us for VGEN2)
            else
            {
                StallExecution(1000);
                g_dwVoltage = dwVoltage;
            }
            
            break;
    }

    exit:
       return; 
}

BOOL CUSDHCBase::BspUSDHCIsCardPresent()
{
    DDK_GPIO_PORT CardDetectPort;
    DWORD dwCardDetectPin;
    UINT32 dwPinVal1 = 1, dwPinVal2 = 1, dwNumchecks = 0;
    DWORD dwTimeElapsed = 0;
    BOOL fCardPresent = FALSE;

    switch(m_dwControllerIndex)
    {
        case 2:
            CardDetectPort = USDHC2_CD_GPIO_PORT;
            dwCardDetectPin = USDHC2_CD_GPIO_PIN;
            break;
		case 3:
			CardDetectPort = USDHC3_CD_GPIO_PORT;
            dwCardDetectPin = USDHC3_CD_GPIO_PIN;
			break;
        case 4:
            CardDetectPort = USDHC4_CD_GPIO_PORT;
            dwCardDetectPin = USDHC4_CD_GPIO_PIN;
            break;
        case 1:
        default:
            goto EXIT;
    }

    DDKGpioReadDataPin(CardDetectPort, dwCardDetectPin, &dwPinVal1);

    // debounce: timeout after 1s, or if same value is read twice (min of 2 debounce periods)
    for (dwNumchecks = 0; dwNumchecks < USDHC_DEBOUNCE_CHECKS && dwTimeElapsed < USDHC_DEBOUNCE_TIMEOUT; dwTimeElapsed += USDHC_DEBOUNCE_PERIOD)
    {
        Sleep(USDHC_DEBOUNCE_PERIOD);
        DDKGpioReadDataPin(CardDetectPort, dwCardDetectPin, &dwPinVal2);

        if (dwPinVal1 == dwPinVal2)
            dwNumchecks++;

        // if value changed in between reads, reset the count
        else
        {
            dwPinVal1 = dwPinVal2;
            dwNumchecks = 0;
        }
    }

	DEBUGMSG(1,(_T("m_dwControllerIndex=%d, dwPinVal2=%d\r\n"),m_dwControllerIndex, dwPinVal2));

	// card is present when pin is low
    if (!dwPinVal2)
        fCardPresent = TRUE;

EXIT:
    if (4 == m_dwControllerIndex)
    {
        // eMMC is always present
        fCardPresent = TRUE;
    }
    // track if either slot has a card inserted, then we cannot turn off the power supply
    if (fCardPresent)
    {
        g_fCardPresent |= ( 1 << (m_dwControllerIndex - 1) );
    }
    else
    {
        g_fCardPresent &= ~(1 << (m_dwControllerIndex - 1) );
    }
        
	DEBUGMSG(1,(_T("fCardPresent=%d\r\n"),fCardPresent));
    return fCardPresent;
}


BOOL CUSDHCBase::BspUSDHCSlotStatusChanged()
{
    BOOL fSlotStatusChanged = FALSE;
    DDK_GPIO_PORT CardDetectPort;
    UINT32 dwCardDetectPin, dwCDIntrStatus;
    
    switch(m_dwControllerIndex)
    {
        case 2:
            CardDetectPort = USDHC2_CD_GPIO_PORT;
            dwCardDetectPin = USDHC2_CD_GPIO_PIN;
            break;
        case 3:
			CardDetectPort = USDHC3_CD_GPIO_PORT;
            dwCardDetectPin = USDHC3_CD_GPIO_PIN;
			break;
		case 4:
            CardDetectPort = USDHC4_CD_GPIO_PORT;
            dwCardDetectPin = USDHC4_CD_GPIO_PIN;
            break;
        case 1:
        default:
            goto EXIT;
    }

    // if ISR of the GPIO pin is set, then some change occured (because interrupt is configured as both edge triggered)
    DDKGpioReadIntrPin(CardDetectPort, dwCardDetectPin, &dwCDIntrStatus);

    if (dwCDIntrStatus)
        fSlotStatusChanged = TRUE;

EXIT:
    DEBUGMSG(TRUE, (L"SDHC[%d] status %s\r\n", m_dwControllerIndex, fSlotStatusChanged ? L"changed" : L"did not change"));
    return fSlotStatusChanged;
}


VOID CUSDHCBase::BspUSDHCCardDetectInt(BOOL fDetectInsertion)
{
    DDK_GPIO_PORT CardDetectPort;
    UINT32 dwCardDetectPin;
	DDK_GPIO_INTR interruptLevel;
    //BOOL fCardPresented = FALSE;
    
    switch(m_dwControllerIndex)
    {
        case 2:
            CardDetectPort = USDHC2_CD_GPIO_PORT;
            dwCardDetectPin = USDHC2_CD_GPIO_PIN;
            break;
        case 3:
			CardDetectPort = USDHC3_CD_GPIO_PORT;
            dwCardDetectPin = USDHC3_CD_GPIO_PIN;
			break;
		case 4:
            CardDetectPort = USDHC4_CD_GPIO_PORT;
            dwCardDetectPin = USDHC4_CD_GPIO_PIN;
            break;
		case 1:
        default:
            goto EXIT;
    }

    // First, clear interrupt status bit for the GPIO line
    DDKGpioClearIntrPin(CardDetectPort, dwCardDetectPin);
    
	// if insertion is to be detected, then interrupt level = low, if removal is to be detected, then interrupt level = high
    interruptLevel = fDetectInsertion ? DDK_GPIO_INTR_LOW_LEV : DDK_GPIO_INTR_HIGH_LEV;
   
	DEBUGMSG(1,(_T("m_dwControllerIndex=%d, fDetectInsertion=%d, interruptLevel=%d \r\n"),m_dwControllerIndex, fDetectInsertion, interruptLevel));

	DDKGpioSetConfig(CardDetectPort, dwCardDetectPin, DDK_GPIO_DIR_IN, interruptLevel);

EXIT:
    return;
}

BOOL CUSDHCBase::BspUSDHCIsWriteProtected()
{
    DDK_GPIO_PORT wp_port;
    DWORD wp_pin;
    UINT32 pin_val1 = 0, pin_val2 = 0, num_checks = 0;
    DWORD time_elapsed = 0;
    BOOL is_wp = FALSE;

    switch (m_dwControllerIndex) {
    case 2:
		wp_port = USDHC2_WP_GPIO_PORT;
        wp_pin = USDHC2_WP_GPIO_PIN;
        break;
    case 3:
		wp_port = USDHC3_WP_GPIO_PORT;
        wp_pin = USDHC3_WP_GPIO_PIN;
        break;
	case 1:
	case 4:
    default:
        goto EXIT;
    }

    DDKGpioReadDataPin(wp_port, wp_pin, &pin_val1);

    // debounce: timeout after 1s, or if same value is read twice (min of 2 debounce periods)
    for (num_checks = 0; num_checks < USDHC_DEBOUNCE_CHECKS && time_elapsed < USDHC_DEBOUNCE_TIMEOUT; time_elapsed += USDHC_DEBOUNCE_PERIOD) {
        Sleep(USDHC_DEBOUNCE_PERIOD);
        DDKGpioReadDataPin(wp_port, wp_pin, &pin_val2);

        // if value changed in between reads, reset the count
        if (pin_val1 == pin_val2) {
            num_checks++;
        } else {
            pin_val1 = pin_val2;
            num_checks = 0;
        }
    }
    // Card is write protected when pin is high
    if (pin_val2)
        is_wp = TRUE;

EXIT:
    return is_wp;
}

BOOL CUSDHCBase::BspUSDHCSlotSupport8Bit()
{
    BOOL bSupport8Bit = FALSE;

    switch(m_dwControllerIndex)
    {
        case 4:
            bSupport8Bit = TRUE;// TRUE;
            break;
        case 1:
        case 2:
        case 3:
        default:
            break;
    }
    return bSupport8Bit;   
}

BOOL CUSDHCBase::BspUSDHCEnableDDRMode()
{
    //if(m_dwControllerIndex == 3)
    //{    
    //    m_fDDRMode = TRUE;
    //    return TRUE;
    //}
    return FALSE;
}

BOOL CUSDHCBase::BspUSDHCDisableDDRMode()
{
    //if(m_dwControllerIndex == 3)
    //{    
    //    m_fDDRMode = FALSE;
    //    return TRUE;
    //}
    return FALSE;
}

VOID CUSDHCBase::BspUSDHCConfigDat3AsCd(BOOL bEnable)
{
    if (bEnable)
        INSREG32BF(&m_pUSDHCReg->PROCTL, USDHC_PROCTL_D3CD, 1); 
    else
        INSREG32BF(&m_pUSDHCReg->PROCTL, USDHC_PROCTL_D3CD, 0); 
}
