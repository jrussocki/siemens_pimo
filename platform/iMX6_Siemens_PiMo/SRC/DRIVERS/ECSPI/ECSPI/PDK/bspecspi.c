/*---------------------------------------------------------------------------
* Copyright (C) 2004-2011, Freescale Semiconductor, Inc. All Rights Reserved.
* THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
* AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT 
*--------------------------------------------------------------------------*/
//------------------------------------------------------------------------------
//
//  File:  bspecspi.c
//
//  Provides BSP-specific configuration routines for the eCSPI peripheral.
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

#pragma warning(push)
#pragma warning(disable: 4115 4201 4204 4214)
#include <windows.h>
#pragma warning(pop)

#include "bsp.h"

//-----------------------------------------------------------------------------
// External Functions


//-----------------------------------------------------------------------------
// External Variables


//-----------------------------------------------------------------------------
// Defines
#ifndef BSP_SDMA_CHNPRI_CSPI_TX    
#define BSP_SDMA_CHNPRI_CSPI_TX    (SDMA_CHNPRI_CHNPRI_HIGHEST-3)
#endif

#ifndef BSP_SDMA_CHNPRI_CSPI_RX    
#define BSP_SDMA_CHNPRI_CSPI_RX    (SDMA_CHNPRI_CHNPRI_HIGHEST-2)
#endif

#ifndef BSP_SDMA_SUPPORT_CSPI1
#define BSP_SDMA_SUPPORT_CSPI1        TRUE
#endif

#ifndef BSP_SDMA_SUPPORT_CSPI2
#define BSP_SDMA_SUPPORT_CSPI2        FALSE
#endif


#define CSPI_MAX_DIV_RATE             15 // 2^15

//-----------------------------------------------------------------------------
// Types


//-----------------------------------------------------------------------------
// Global Variables


//-----------------------------------------------------------------------------
// Local Variables


//-----------------------------------------------------------------------------
//
// Function: CalculateDivRate
//
// This is a private function to calculate the data rate divider from input frequency.
// Rule: the actual clock is less than (1+10%)*Frequency requested; 
//
// Parameters:
//      dwFrequency
//          [in] Frequency requested.
//
// Returns:
//      Data rate divisor for requested frequency.
//-----------------------------------------------------------------------------
void BSPCSPICalculateDivRate(UINT32 dwFrequency, UINT32 dwTolerance, UINT8 *PREDIV, UINT8 *POSTDIV )
{
    UINT32 dwCspiClk;
    UINT32 tmp=0;

    DDKClockGetFreq(DDK_CLOCK_SIGNAL_ECSPI, &dwCspiClk);
  
    tmp = dwCspiClk/dwFrequency;
    tmp = (dwCspiClk<(tmp*(dwFrequency+dwTolerance)))?(tmp-1):tmp;
    while( tmp >=16 )
    {
        *POSTDIV+=1;
        tmp >>=1;
    }

    *PREDIV = (UINT8)tmp;

    return ;
}
//-----------------------------------------------------------------------------
//
// Function: SetIOMux
//
// This is a private function which request IOMUX for 
// the corresponding CSPI Bus.
//
// Parameters:
//      dwIndex
//          [in] cspi port to be configured.
//
// Returns:
//      TRUE if successful, FALSE otherwise.
//-----------------------------------------------------------------------------
BOOL BSPCSPISetIOMux(UINT32 dwIndex)
{
    BOOL rc = FALSE;

    if(dwIndex == 1)
    {
		// TC : IOMux deleted because the configuration is made in the bootloader
        rc = TRUE;
    }
    else if(dwIndex == 2)
    {
        // eCSPI2 is not used

        rc = FALSE;
    }

    return rc;
}

//-----------------------------------------------------------------------------
//
// Function: ReleaseIOMux
//
// This is a private function which releases the 
// IOMUX pins selected for the CSPI bus.
//
// Parameters:
//      dwIndex
//          [in] cspi port to be released.
//
// Returns:
//      TRUE if successful, FALSE otherwise.
//-----------------------------------------------------------------------------
BOOL BSPCSPIReleaseIOMux(UINT32 dwIndex)
{
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(dwIndex);

    return TRUE;    
}

//-----------------------------------------------------------------------------
//
// Function: EnableClock
//
// Provides the platform-specific SPI clock gating control to enable/disable 
// module clocks.
//
// Parameters:
//      Index
//          [in] cspi port to be configured.
//      bEnable
//          [in] Set to TRUE to enable eCSPI clocks, set to FALSE to disable
//          CSPI clocks.
//
// Returns:
//      TRUE if successful, FALSE otherwise.
//
//-----------------------------------------------------------------------------
BOOL BSPCSPIEnableClock(UINT32 Index, BOOL bEnable)
{
    BOOL rc = FALSE; 
    DDK_CLOCK_GATE_MODE mode = bEnable ? 
        DDK_CLOCK_GATE_MODE_ENABLED_ALL : DDK_CLOCK_GATE_MODE_DISABLED;
    
    switch(Index)
    {
        case 1: 
			rc = DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_ECSPI1, mode);
            break;
        case 2: 
			rc = DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_ECSPI2, mode);
            break;
        default:
            ERRORMSG(TRUE, (TEXT("Invalid Index\r\n"))); 
    }

    return rc;
}
//-----------------------------------------------------------------------------
//
// Function: BSPCspiGetChannelPriority
//
// This function returns the sdma priority for cspi
//
// Parameters:
//        None
//
// Returns:
//      The channel Priority.
//
//-----------------------------------------------------------------------------
BOOL BSPCspiGetChannelPriority(UINT8 (*priority)[2])
{
    (*priority)[0] =BSP_SDMA_CHNPRI_CSPI_TX;
    (*priority)[1] =BSP_SDMA_CHNPRI_CSPI_RX;
    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function: BSPCspiIsDMAEnabled
//
// This function returns whether sdma is 
// enabled or diabled for a given cspi index
//
// Parameters:
//        CSPI Index
//
// Returns:
//      Whether DMA is Enabled for a given CSPI index.
//
//-----------------------------------------------------------------------------
BOOL BSPCspiIsDMAEnabled(UINT8 Index)
{
    switch (Index)
    {
        case 1:
            return BSP_SDMA_SUPPORT_CSPI1;
        case 2:
            return BSP_SDMA_SUPPORT_CSPI2;
        default:
            ERRORMSG(TRUE, (TEXT("Invalid Index\r\n"))); 
    }
    return FALSE;
}

//-----------------------------------------------------------------------------
//
// Function: BSPCspiAcquireGprBit
//
// This function is a wrapper for CSPI to acquire a muxed SDMA request line.
//
// Parameters:
//      Index
//          [in] Index of the CSPI 
// Returns:
//      TRUE if success.
//
//-----------------------------------------------------------------------------
BOOL BSPCspiAcquireGprBit(UINT8 Index)
{
    switch(Index)
    {
        case 1:
        case 2:
        case 3:
            return TRUE;
        default:
            ERRORMSG(TRUE, (TEXT("Invalid Index\r\n"))); 
    }
    return FALSE;
}

//-----------------------------------------------------------------------------
//
// Function: BSPCspiIsAllowPolling
//
// This function returns whether polling method is 
// allowed or not allowed for a given cspi index
//
// Parameters:
//        CSPI Index
//
// Returns:
//      Whether polling method is allowed for a given CSPI index.
//
//-----------------------------------------------------------------------------
BOOL BSPCspiIsAllowPolling(UINT8 Index)
{
    UNREFERENCED_PARAMETER(Index);
    return FALSE;
}


//-----------------------------------------------------------------------------
//
// Function: BSPCspiExchange
//
// This function exchanges eCSPI data
//
// Parameters:
//        
//
// Returns:
//      
//
//-----------------------------------------------------------------------------
BOOL BSPCspiExchange(VOID *lpInBuf, LPVOID pRxBuf, UINT32 nOutBufSize, LPDWORD BytesReturned)
{
    UNREFERENCED_PARAMETER(lpInBuf);
    UNREFERENCED_PARAMETER(pRxBuf);
    UNREFERENCED_PARAMETER(nOutBufSize);
    UNREFERENCED_PARAMETER(BytesReturned);

    return FALSE;
}


//-----------------------------------------------------------------------------
//
// Function: BSPeCSPICSHigh
//
// This function implements channelselect of inactive channels as inactive status.
// For ENGcm09397
//
// Parameters:
//      Index
//          [in] Index of the eCSPI 
//      CSPOL
//          [in] ChannelSelects active polarity
//
// Returns:
//      None.
//
//-----------------------------------------------------------------------------
void BSPeCSPICSHigh(UINT32 Index, UINT8 CSPOL)
{
    switch(Index)
    {   
        // eCSPI1
        case 1:

            break;

        // eCPSI2
        case 2:  

            break;

        default:
            ERRORMSG(TRUE, (TEXT("Invalid Index\r\n"))); 
            break;
    }

    return;
}

