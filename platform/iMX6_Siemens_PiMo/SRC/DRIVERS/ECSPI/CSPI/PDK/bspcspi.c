/*---------------------------------------------------------------------------
* Copyright (C) 2004-2011, Freescale Semiconductor, Inc. All Rights Reserved.
* THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
* AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT 
*--------------------------------------------------------------------------*/
//------------------------------------------------------------------------------
//
//  File:  bspcspi.c
//
//  Provides BSP-specific configuration routines for the CSPI peripheral.
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

#pragma warning(push)
#pragma warning(disable: 4115 4201 4204 4214)
#include <windows.h>
#pragma warning(pop)

#include "bsp.h"

//-----------------------------------------------------------------------------
// External Functions


//-----------------------------------------------------------------------------
// External Variables


//-----------------------------------------------------------------------------
// Defines
#ifndef BSP_SDMA_CHNPRI_CSPI_TX    
#define BSP_SDMA_CHNPRI_CSPI_TX    (SDMA_CHNPRI_CHNPRI_HIGHEST-3)
#endif

#ifndef BSP_SDMA_CHNPRI_CSPI_RX    
#define BSP_SDMA_CHNPRI_CSPI_RX    (SDMA_CHNPRI_CHNPRI_HIGHEST-2)
#endif

#ifndef BSP_SDMA_SUPPORT_CSPI3
#define BSP_SDMA_SUPPORT_CSPI3        TRUE
#endif


#ifndef BSP_ALLOW_POLLING_CSPI3
#define BSP_ALLOW_POLLING_CSPI3       TRUE
#endif


#define CSPI_MAX_DIV_RATE             9 // 2^9= 512

//-----------------------------------------------------------------------------
// Types


//-----------------------------------------------------------------------------
// Global Variables


//-----------------------------------------------------------------------------
// Local Variables


//-----------------------------------------------------------------------------
//
// Function: CalculateDivRate
//
// This is a private function to calculate the data
// rate divider from input frequency.
//
// Parameters:
//      dwFrequency
//          [in] Frequency requested.
//
// Returns:
//      Data rate divisor for requested frequency.
//-----------------------------------------------------------------------------
UINT32 BSPCSPICalculateDivRate(UINT32 dwFrequency, UINT32 dwTolerance)
{
    UINT32 dwDivisor;
    UINT32 dwCspiClk;

    DDKClockGetFreq(DDK_CLOCK_SIGNAL_IPG, &dwCspiClk);

    for (dwDivisor = 2; dwDivisor < CSPI_MAX_DIV_RATE; dwDivisor++)
    {
        if ((dwCspiClk>>dwDivisor) <= dwFrequency+dwTolerance)
        {
            break;
        }
    }

    return (dwDivisor - 2);

}
//-----------------------------------------------------------------------------
//
// Function: SetIOMux
//
// This is a private function which request IOMUX for 
// the corresponding CSPI Bus.
//
// Parameters:
//      dwIndex
//          [in] cspi port to be configured.
//
// Returns:
//      TRUE if successful, FALSE otherwise.
//-----------------------------------------------------------------------------
BOOL BSPCSPISetIOMux(UINT32 dwIndex)
{
    BOOL rc = FALSE;

    if(dwIndex == 3)
    {
        // Configure IOMUX to request CSPI pins

        rc = TRUE;
    }

    return rc;
}
//-----------------------------------------------------------------------------
//
// Function: ReleaseIOMux
//
// This is a private function which releases the 
// IOMUX pins selected for the CSPI bus.
//
// Parameters:
//      dwIndex
//          [in] cspi port to be released.
//
// Returns:
//      TRUE if successful, FALSE otherwise.
//-----------------------------------------------------------------------------
BOOL BSPCSPIReleaseIOMux(UINT32 dwIndex)
{
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(dwIndex);

    return TRUE;    
}
//-----------------------------------------------------------------------------
//
// Function: EnableClock
//
// Provides the platform-specific SPI clock gating control to enable/disable 
// module clocks.
//
// Parameters:
//      Index
//          [in] cspi port to be configured.
//      bEnable
//          [in] Set to TRUE to enable CSPI clocks, set to FALSE to disable
//          CSPI clocks.
//
// Returns:
//      TRUE if successful, FALSE otherwise.
//
//-----------------------------------------------------------------------------
BOOL BSPCSPIEnableClock(UINT32 Index, BOOL bEnable)
{
    BOOL rc = FALSE;

    DDK_CLOCK_GATE_MODE mode = bEnable ? 
        DDK_CLOCK_GATE_MODE_ENABLED_ALL : DDK_CLOCK_GATE_MODE_DISABLED;
    switch(Index)
    {
        case 3: 
            rc = DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_CSPI_IPG, mode); 
            break;
        default:
            ERRORMSG(TRUE, (TEXT("Invalid Index\r\n"))); 
    }

    return rc;
}
//-----------------------------------------------------------------------------
//
// Function: BSPCspiGetChannelPriority
//
// This function returns the sdma priority for cspi
//
// Parameters:
//        None
//
// Returns:
//      The channel Priority.
//
//-----------------------------------------------------------------------------
BOOL BSPCspiGetChannelPriority(UINT8 (*priority)[2])
{
    (*priority)[0] =BSP_SDMA_CHNPRI_CSPI_TX;
    (*priority)[1] =BSP_SDMA_CHNPRI_CSPI_RX;
    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function: BSPCspiIsDMAEnabled
//
// This function returns whether sdma is 
// enabled or diabled for a given cspi index
//
// Parameters:
//        CSPI Index
//
// Returns:
//      Whether DMA is Enabled for a given CSPI index.
//
//-----------------------------------------------------------------------------
BOOL BSPCspiIsDMAEnabled(UINT8 Index)
{
    switch (Index)
    {
        case 3:
            return BSP_SDMA_SUPPORT_CSPI3;
        default:
            ERRORMSG(TRUE, (TEXT("Invalid Index\r\n"))); 
    }
    return FALSE;
}

//-----------------------------------------------------------------------------
//
// Function: BSPCspiAcquireGprBit
//
// This function is a wrapper for CSPI to acquire a muxed SDMA request line.
//
// Parameters:
//      Index
//          [in] Index of the CSPI 
// Returns:
//      TRUE if success.
//
//-----------------------------------------------------------------------------
BOOL BSPCspiAcquireGprBit(UINT8 Index)
{
    switch(Index)
    {
        case 3:
            return TRUE;
        default:
            ERRORMSG(TRUE, (TEXT("Invalid Index\r\n"))); 
    }
    return FALSE;
}

//-----------------------------------------------------------------------------
//
// Function: BSPCspiIsAllowPolling
//
// This function returns whether polling method is 
// allowed or not allowed for a given cspi index
//
// Parameters:
//        CSPI Index
//
// Returns:
//      Whether polling method is allowed for a given CSPI index.
//
//-----------------------------------------------------------------------------
BOOL BSPCspiIsAllowPolling(UINT8 Index)
{
    switch (Index)
    {
        case 3:
            return BSP_ALLOW_POLLING_CSPI3;
        default:
            ERRORMSG(TRUE, (TEXT("Invalid Index\r\n"))); 
    }
    return FALSE;
}

//-----------------------------------------------------------------------------
//
// Function: BSPCspiExchange
//
// This function exchanges eCSPI data
//
// Parameters:
//        
//
// Returns:
//      
//
//-----------------------------------------------------------------------------
BOOL BSPCspiExchange(VOID *lpInBuf, LPVOID pRxBuf, UINT32 nOutBufSize, LPDWORD BytesReturned)
{
    UNREFERENCED_PARAMETER(lpInBuf);
    UNREFERENCED_PARAMETER(pRxBuf);
    UNREFERENCED_PARAMETER(nOutBufSize);
    UNREFERENCED_PARAMETER(BytesReturned);

    return FALSE;
}

//------------------------------------------------------------------------------
//
// Function: BSPCheckPort
//
//  This function checks if the cspi port is used for NIC card
//
// Parameters:
//      None.
//
//  Returns:
//      TRUE - The port is mutliplex
//      FALSE - The port is not mutliplex
//
//------------------------------------------------------------------------------
BOOL BSPCheckPort(UINT32 Index)
{
    // CSPI is not multiplex with debug board
    UNREFERENCED_PARAMETER(Index);
    return FALSE;
}

