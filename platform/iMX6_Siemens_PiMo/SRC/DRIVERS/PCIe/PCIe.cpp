// PCIe.cpp : Defines the entry point for the DLL application.
//

//
// Routines to configure and initialize the PCIbus hardware
//
#include <windows.h>
#include <ceddk.h>
#include <bsp.h>
#include <common_pcie.h>
#include <mx6_anatop.h>
#include <winbase.h>
#include <resmgr.h>

extern BOOL PCIeCoreConfig();

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
        case DLL_PROCESS_ATTACH:
        case DLL_THREAD_ATTACH:
        case DLL_THREAD_DETACH:
        case DLL_PROCESS_DETACH:
            break;
    }
    return TRUE;
}

HANDLE Init(LPCWSTR ActiveKey)
{
	return (HANDLE) PCIeCoreConfig();
}

void Deinit(HANDLE hInitKey)
{
}

BOOL PowerUp(DWORD dwData)
{
    return TRUE;
}
BOOL PowerDown(DWORD dwData)
{
    return TRUE;
}

HANDLE
Open(
        HANDLE  pHead,          // @parm Handle returned by COM_Init.
        DWORD   AccessCode,     // @parm access code.
        DWORD   ShareMode       // @parm share mode - Not used in this driver.
        )
{
    return NULL;
}

BOOL Close(HANDLE pOpenHead)
{
    return FALSE;
}

BOOL
IOControl(HANDLE pOpenHead,
              DWORD dwCode, PBYTE pBufIn,
              DWORD dwLenIn, PBYTE pBufOut, DWORD dwLenOut,
              PDWORD pdwActualOut)
{
    return FALSE;
}
