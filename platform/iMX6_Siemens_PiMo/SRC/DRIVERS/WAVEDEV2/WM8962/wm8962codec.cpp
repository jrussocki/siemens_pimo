
#include "bsp.h"
#include "i2cbus.h"
#include "wm8962codec.h"

WM8962Codec::WM8962Codec()
{
    m_bI2CInited = FALSE;
	m_bForceSpeaker = FALSE;
	m_bHPDetected = TRUE;
    m_lADCVol = WM8962_DEFAULT_ADC_VOLUME;
	m_rADCVol = WM8962_DEFAULT_ADC_VOLUME;
}

WM8962Codec::~WM8962Codec()
{
    if (m_bI2CInited)
    {	
		ChipPowerDown();
        I2CClose();
    }
}

	/*******************
	*	I2C Functions	*
	********************/
BOOL WM8962Codec::I2CInit()
{
    // Check if already initialized
    if (m_bI2CInited)
    {
		return  WM8962_SUCCESS;
    }

	m_hI2C = I2COpenHandle(WM8962_I2C_BUS);
	
	// Check on handle value
	if (m_hI2C == INVALID_HANDLE_VALUE)
    {
		ERRORMSG(TRUE, (TEXT("%s(): Invalid handle value!\r\n"),__WFUNCTION__));
		return  WM8962_FAILURE;
    }
	
	// WM8962 used as a slave, so master mode on cpu side.
	if (!I2CSetMasterMode(m_hI2C))
    {
		ERRORMSG(TRUE, (TEXT("%s(): Master mode failed!\r\n"),__WFUNCTION__));
        I2CClose();
        return WM8962_FAILURE;
    }
	
	// Initialize the device internal fields
	if (!I2CSetFrequency(m_hI2C, WM8962_I2C_SPEED))
    {
		ERRORMSG(TRUE, (TEXT("%s(): Set frequency (%d Hz) failed!\r\n"),__WFUNCTION__,WM8962_I2C_SPEED));
        I2CClose();
        return WM8962_FAILURE;
    }

    // Create I2C critical section
    InitializeCriticalSection(&m_csI2C);

    m_bI2CInited = TRUE;

    return  WM8962_SUCCESS;
}

VOID WM8962Codec::I2CClose()
{
    if (m_hI2C)
    {
        I2CCloseHandle(m_hI2C);
        m_hI2C = INVALID_HANDLE_VALUE;
        m_bI2CInited = FALSE;
        DeleteCriticalSection(&m_csI2C);
    }
}

BOOL WM8962Codec::I2CWriteRegister(UINT16 regAddr, UINT16 data)
{
	I2C_TRANSFER_BLOCK I2CXferBlock;
    I2C_PACKET I2CPacket;
    INT iResult;
    UINT8 byBuffer[4];

	// Check validity of I2C handle
    if (m_hI2C == INVALID_HANDLE_VALUE)
    {
		ERRORMSG(TRUE, (TEXT("%s(): Invalid handle value! \r\n"),__WFUNCTION__));
        return WM8962_FAILURE;
    }

    // set bytes buffer with register value and data
    byBuffer[0] = (UINT8)(( regAddr >> 8 ) & 0xFF);
    byBuffer[1] = (UINT8)(( regAddr ) & 0xFF);
    byBuffer[2] = (UINT8)(( data >> 8 ) & 0xFF);
    byBuffer[3] = (UINT8)(( data ) & 0xFF);

	I2CPacket.wLen = 4;
    I2CPacket.byRW = I2C_RW_WRITE;
    I2CPacket.pbyBuf = (PBYTE)&byBuffer;
    I2CPacket.byAddr = WM8962_I2C_DEVICE_ID;
    // if I2C encounters an error, it will write to *lpiResult. 
    I2CPacket.lpiResult = &iResult;

    I2CXferBlock.pI2CPackets = &I2CPacket;
    I2CXferBlock.iNumPackets = 1;

    // Write register via I2C
	if(TRUE != I2CTransfer(m_hI2C, &I2CXferBlock))
    {
		RETAILMSG(TRUE, (TEXT("%s(0x%x)= 0x%x, failed!\r\n"),__WFUNCTION__, regAddr, data));
		return WM8962_FAILURE;
    }

    return WM8962_SUCCESS;
}

BOOL WM8962Codec::I2CReadRegister(UINT16 regAddr, UINT16 * pData)
{
    I2C_TRANSFER_BLOCK I2CXferBlock;
    I2C_PACKET I2CPacketArray[2];
    INT32 iResult;
    UINT8 reg[2];
    UINT8 data[2];

	// Check validity of I2C handle
    if (m_hI2C == INVALID_HANDLE_VALUE)
    {
		ERRORMSG(TRUE, (TEXT("%s(): Invalid handle value! \r\n"),__WFUNCTION__));
        return WM8962_FAILURE;
    }

    // set byte buffer with register value
    reg[0] = (UINT8)((regAddr & 0xFF00) >> 8);
    reg[1] = (UINT8)(regAddr & 0x00FF);

    I2CPacketArray[0].pbyBuf = (PBYTE) &reg[0];
    I2CPacketArray[0].wLen = 2; // let I2C know how much to transmit

    I2CPacketArray[0].byRW = I2C_RW_WRITE;
    I2CPacketArray[0].byAddr = WM8962_I2C_DEVICE_ID;
    // if I2C encounters an error, it will write to *lpiResult.    
    I2CPacketArray[0].lpiResult = &iResult; 

    I2CPacketArray[1].pbyBuf = (PBYTE) &data;
    I2CPacketArray[1].wLen = 2; // let I2C know how much to receive

    I2CPacketArray[1].byRW = I2C_RW_READ;
    I2CPacketArray[1].byAddr =  WM8962_I2C_DEVICE_ID;
    // if I2C encounters an error, it will write to *lpiResult.
    I2CPacketArray[1].lpiResult = &iResult; 

    I2CXferBlock.pI2CPackets = I2CPacketArray;
    I2CXferBlock.iNumPackets = 2;

	
	// Read register via I2C
    if (I2CTransfer(m_hI2C,&I2CXferBlock))
    {
		// Form 16-bit register value
		*pData = ((UINT16)data[0] << 8) | (UINT16)data[1];
    }
	else
	{
		RETAILMSG(TRUE, (TEXT("%s(0x%x)= 0x%x, failed!\r\n"),__WFUNCTION__, regAddr, *pData));
		return WM8962_FAILURE;
	}

    return WM8962_SUCCESS;
}

BOOL WM8962Codec::I2CModifyRegister(UINT16 regAddr, UINT16 mask, UINT16 bitFieldValue)
{
	BOOL ret = WM8962_SUCCESS;
    UINT16 value;
	
	// After the register value is read below, another thread could potentially
    // overwrite the value before this function writes the new value.  To prevent
    // this, acquire a critical section lock
    EnterCriticalSection(&m_csI2C);
	
	// read current setting
	ret = I2CReadRegister(regAddr, &value);

	if(WM8962_SUCCESS == ret)
	{
		// mask out old setting
		value &= ~mask;

		// set new bit field values, but only bit fields that were cleared first
		value |= (mask) & bitFieldValue;

		// write out new value
		ret = I2CWriteRegister(regAddr, value);
		
		if(WM8962_FAILURE == ret)
		{
			RETAILMSG(TRUE, (TEXT("%s(0x%x)= 0x%x, WriteRegister failed!\r\n"),__WFUNCTION__, regAddr, value));
		}
	}
	else
	{
		RETAILMSG(TRUE, (TEXT("%s(0x%x): ReadRegister failed!\r\n"),__WFUNCTION__, regAddr));
	}
	
    // Leave critical section lock
    LeaveCriticalSection(&m_csI2C);

    return ret;
}

	/************************
	*	Codec Functions	*
	*************************/
BOOL WM8962Codec::CodecInit()
{
	// Configure clocking
	SetSysClock(0x0);   

	// Set VMID reference and bias current
	ChipPowerUp();
	
	// Enable Write Sequencer
	I2CModifyRegister(WM8962_WRITE_SEQUENCER_CONTROL_1, 
                                    WM8962_WSEQ_ENA_MASK, 
                                    WM8962_WSEQ_ENA);	
    
	// Enable I2S
	SetupDigitalAudio();
    
	// Power-up Output
	PowerOnDAC();	
	PowerOnSpeakers();
	PowerOnHP();
    
    // Select ADC source + setup microphone
    SelectADCInput(WM8962_INPUT_MIC);
    
    return WM8962_SUCCESS;
}

BOOL WM8962Codec::GetID()
{
	// Check on handle value
	if (m_hI2C == INVALID_HANDLE_VALUE)
    {
		ERRORMSG(TRUE, (TEXT("%s(): Invalid handle value!\r\n"),__WFUNCTION__));
    }

	UINT16 val = 0x0;
	I2CReadRegister(WM8962_RIGHT_INPUT_VOLUME, &val);	
	
	// Check Customer ID and revision ID
	if(val != WM8962_EXPECTED_ID)
	{
		ERRORMSG(TRUE, (TEXT("%s(): Bad value for Customer or Revison ID!\r\n"),__WFUNCTION__));
		return WM8962_FAILURE;
	}
	
	return WM8962_SUCCESS;
}

BOOL WM8962Codec::SetSysClock(UINT16 param)
{
    //SYSCLOCK Disable(Should be disabled, when reconfiguring)+No Automatic Clocking
    I2CModifyRegister(WM8962_CLOCKING2,
            WM8962_SYSCLK_ENA_MASK | WM8962_CLKREG_OVD_MASK, 
            0 | WM8962_CLKREG_OVD);
   
   //Clocksource: MCLK Pin
    I2CModifyRegister(WM8962_CLOCKING2,
            WM8962_SYSCLK_SRC_MASK , 
            0 << WM8962_SYSCLK_SRC_SHIFT);
   
   //SYSCLOCK Enable
    I2CModifyRegister(WM8962_CLOCKING2,
            WM8962_SYSCLK_ENA_MASK, 
            WM8962_SYSCLK_ENA);
	
	return WM8962_SUCCESS;

}

BOOL WM8962Codec::ChipPowerUp()
{
	// Anti-pop
	I2CModifyRegister(WM8962_ANTI_POP, 
            WM8962_STARTUP_BIAS_ENA_MASK | WM8962_VMID_BUF_ENA_MASK, 
            WM8962_STARTUP_BIAS_ENA | WM8962_VMID_BUF_ENA);
	
	// VMID and bias current
	I2CModifyRegister(WM8962_PWR_MGMT_1, 
            WM8962_VMID_SEL_MASK | WM8962_BIAS_ENA_MASK, 
            (WM8962_VMID_CHARGING_MODE << WM8962_VMID_SEL_SHIFT) | WM8962_BIAS_ENA);
	
	return WM8962_SUCCESS;
}

BOOL WM8962Codec::ChipPowerDown()
{
	// Chip Power Down sequence from Write Sequencer
	I2CWriteRegister(WM8962_WRITE_SEQUENCER_CONTROL_2, 0x9B);
	Sleep(32);

	return WM8962_SUCCESS;
}

BOOL WM8962Codec::PowerOnDAC()
{	
	// Enable DACL / DACR
	I2CModifyRegister(WM8962_PWR_MGMT_2, 
            WM8962_DACL_ENA_MASK | WM8962_DACR_ENA_MASK, 
            WM8962_DACL_ENA | WM8962_DACR_ENA);
	
    // Unmute DAC
    MuteDAC(FALSE);
    
	return WM8962_SUCCESS;
}

BOOL WM8962Codec::PowerOffDAC()
{	    
    // Mute DAC
    MuteDAC(TRUE);
    
	// Disable DACL / DACR
	I2CModifyRegister(WM8962_PWR_MGMT_2, 
            WM8962_DACL_ENA_MASK | WM8962_DACR_ENA_MASK, 
            0);

	return WM8962_SUCCESS;
}

BOOL WM8962Codec::SetDACVolume(BYTE leftVol, BYTE rightVol)
{
    // Write vol value
	I2CModifyRegister(WM8962_LEFT_DAC_VOLUME, 
            WM8962_DACL_VOL_MASK, 
            leftVol);
    I2CModifyRegister(WM8962_RIGHT_DAC_VOLUME, 
            WM8962_DACR_VOL_MASK, 
            rightVol);
    
    // Update both channels
    I2CModifyRegister(WM8962_LEFT_DAC_VOLUME, 
            WM8962_DAC_VU_MASK, 
            WM8962_DAC_VU);
    
	return WM8962_SUCCESS;
}

BOOL WM8962Codec::MuteDAC(BOOL bMuteVal)
{
	// Soft-mute (enabled by default)
	// I2CModifyRegister(WM8962_ADC_DAC_CONTROL_1, WM8962_DAC_MUTE_RAMP_MASK, WM8962_DAC_MUTE_RAMP);
    
	if(bMuteVal == TRUE)
	{
		// Mute
		I2CModifyRegister(WM8962_ADC_DAC_CONTROL_1,  
                WM8962_DAC_MUTE_MASK, 
                WM8962_DAC_MUTE);
	}
	else
	{
		// Unmute
		I2CModifyRegister(WM8962_ADC_DAC_CONTROL_1,  
                WM8962_DAC_MUTE_MASK,  
                0);
	}	
	
    // Update
	I2CModifyRegister(WM8962_LEFT_DAC_VOLUME,
									WM8962_DAC_VU_MASK,
									WM8962_DAC_VU);
	
	return WM8962_SUCCESS;
}

BOOL WM8962Codec::SetLoopback(BOOL bEnable)
{   
    BOOL res = WM8962_SUCCESS;
    
    if(bEnable == TRUE)
	{
		res = I2CModifyRegister(WM8962_AUDIO_INTERFACE_1,
                        WM8962_LOOPBACK_MASK,
                        WM8962_LOOPBACK);
	}
	else
	{
		res = I2CModifyRegister(WM8962_AUDIO_INTERFACE_1,
                        WM8962_LOOPBACK_MASK, 
                        0);
	}	

    return  res;
}

BOOL WM8962Codec::SelectADCInput(WM8962_ADC_INPUT_TYPE inputType)
{
    switch(inputType)
    {
        case WM8962_INPUT_MIC :
           
            SetuptMic(WM8962_MIC_PIN);
            
            break;
            
        case WM8962_INPUT_LINEIN :
        
        default :
        
            RETAILMSG(TRUE, (TEXT("%s(): Only Mic currently supported ! \r\n"),__WFUNCTION__));
            return WM8962_FAILURE;
            
            break;
    }
    
    return WM8962_SUCCESS;
}

BOOL WM8962Codec::PowerOnADC()
{    
    // Enable ADC
    I2CModifyRegister(WM8962_PWR_MGMT_1, 
            WM8962_ADCL_ENA_MASK | WM8962_ADCR_ENA_MASK, 
            WM8962_ADCL_ENA | WM8962_ADCR_ENA);
            
    // Unmute ADC
    MuteADC(FALSE);
    
    return WM8962_SUCCESS;
}

BOOL WM8962Codec::PowerOffADC()
{
    // Unmute ADC
    MuteADC(TRUE);

    // Disable ADC
    I2CModifyRegister(WM8962_PWR_MGMT_1, 
            WM8962_ADCL_ENA_MASK | WM8962_ADCR_ENA_MASK, 
            WM8962_ADCL_ENA | WM8962_ADCR_ENA);

    return WM8962_SUCCESS;
}

BOOL WM8962Codec::SetADCVolume(BYTE leftVol, BYTE rightVol)
{
    // Write vol value
	I2CModifyRegister(WM8962_LEFT_ADC_VOLUME, 
            WM8962_ADCL_VOL_MASK, 
            leftVol);
    I2CModifyRegister(WM8962_RIGHT_ADC_VOLUME, 
            WM8962_ADCR_VOL_MASK, 
            rightVol);

    // Update both channels
    I2CModifyRegister(WM8962_LEFT_ADC_VOLUME, 
            WM8962_ADC_VU_MASK, 
            WM8962_ADC_VU);
            
	return WM8962_SUCCESS;
}

BOOL WM8962Codec::MuteADC(BOOL bMuteVal)
{  
    if(bMuteVal == TRUE)    // Mute
	{
		// Save current ADC volume  
        I2CReadRegister(WM8962_LEFT_ADC_VOLUME, &m_lADCVol);
        I2CReadRegister(WM8962_RIGHT_ADC_VOLUME, &m_rADCVol);
        
        // Set volume to 0
       SetADCVolume(0, 0);
	}
	else    // Unmute
	{     
        // Restore previous ADC volume
        SetADCVolume(m_lADCVol & 0x00FF,
                                   m_rADCVol & 0x00FF);
	}	

	return WM8962_SUCCESS;
}

BOOL WM8962Codec::PowerOnSpeakers()
{
	// Enable SPKOUT
	I2CModifyRegister(WM8962_PWR_MGMT_2, 
            WM8962_SPKOUTL_PGA_ENA_MASK, 
            WM8962_SPKOUTL_PGA_ENA);
	I2CModifyRegister(WM8962_PWR_MGMT_2,
            WM8962_SPKOUTR_PGA_ENA_MASK,
            WM8962_SPKOUTR_PGA_ENA);
	
	// Spk (PGA) volume
	I2CModifyRegister(WM8962_SPKOUTL_VOLUME, 
            WM8962_SPKOUTL_VOL_MASK,  
            WM8962_SPKOUT_PGA_VOLUME);
	I2CModifyRegister(WM8962_SPKOUTR_VOLUME,
            WM8962_SPKOUTR_VOL_MASK,  
            WM8962_SPKOUT_PGA_VOLUME);

	//  SpeakerWake
	I2CWriteRegister(WM8962_WRITE_SEQUENCER_CONTROL_2, 0xE8);
	Sleep(2);
	
	return WM8962_SUCCESS;
}

BOOL WM8962Codec::PowerOffSpeakers()
{
	//  SpeakerSleep
	I2CWriteRegister(WM8962_WRITE_SEQUENCER_CONTROL_2, 0xE4);	
	Sleep(2);
	
	// Spk (PGA) volume to 0 
	I2CModifyRegister(WM8962_SPKOUTL_VOLUME, 
            WM8962_SPKOUTL_VOL_MASK,  
            0);
	I2CModifyRegister(WM8962_SPKOUTR_VOLUME, 
            WM8962_SPKOUTR_VOL_MASK,  
            0);

	// Disable SPKOUT
	I2CModifyRegister(WM8962_PWR_MGMT_2, 
            WM8962_SPKOUTL_PGA_ENA_MASK, 
            0);
	I2CModifyRegister(WM8962_PWR_MGMT_2, 
            WM8962_SPKOUTR_PGA_ENA_MASK,  
            0);

	return WM8962_SUCCESS;
}

BOOL WM8962Codec::MuteSpeakers(BOOL bMuteVal)
{
	if(bMuteVal == TRUE)
	{
		// Mute
		I2CModifyRegister(WM8962_CLASS_D_CONTROL_1, 
										WM8962_SPKOUTL_PGA_MUTE_MASK | WM8962_SPKOUTR_PGA_MUTE_MASK,
										WM8962_SPKOUTL_PGA_MUTE | WM8962_SPKOUTR_PGA_MUTE);
	}
	else
	{
		// Unmute
		I2CModifyRegister(WM8962_CLASS_D_CONTROL_1, 
										WM8962_SPKOUTL_PGA_MUTE_MASK | WM8962_SPKOUTR_PGA_MUTE_MASK,
										0);
	}	
		
    // Update
	I2CModifyRegister(WM8962_SPKOUTL_VOLUME,
										WM8962_SPKOUT_VU_MASK ,
										WM8962_SPKOUT_VU);
										
	return WM8962_SUCCESS;
}

VOID WM8962Codec::ForceSpeaker(BOOL bForceSpeaker)
{
    m_bForceSpeaker = bForceSpeaker;
}

VOID WM8962Codec::SpeakerHeadphoneSelection()
{
	// If force speaker flag is set or if no headphone is detected, unmute the speakers and mute the HP
    if (m_bForceSpeaker || !m_bHPDetected)
    {      
		MuteHP(TRUE);

		MuteSpeakers(FALSE);
    }
    // If force speaker flag is not set and if headphone is plugged, unmute the HP and mute the speakers
    else 
    {
        MuteHP(FALSE);
     
		MuteSpeakers(TRUE);
    }
}

BOOL WM8962Codec::PowerOnHP()
{
	// DAC to Headphone PowerUp sequence from Write Sequencer
	I2CWriteRegister(WM8962_WRITE_SEQUENCER_CONTROL_2, 0x80);
	Sleep(93);
	
	// HP (PGA) Volume
	I2CModifyRegister(WM8962_HPOUTL_VOLUME, 
            WM8962_HPOUTL_VOL_MASK | WM8962_HPOUT_VU_MASK,
            (WM8962_HPOUT_PGA_VOLUME << WM8962_HPOUTL_VOL_SHIFT) | WM8962_HPOUT_VU);
	I2CModifyRegister(WM8962_HPOUTR_VOLUME, 
            WM8962_HPOUTR_VOL_MASK | WM8962_HPOUT_VU_MASK,  
            (WM8962_HPOUT_PGA_VOLUME << WM8962_HPOUTR_VOL_SHIFT) | WM8962_HPOUT_VU);
	
	return WM8962_SUCCESS;
}

BOOL WM8962Codec::PowerOffHP()
{
    // Mute
    I2CModifyRegister(WM8962_ADC_DAC_CONTROL_1,
            WM8962_DAC_MUTE_MASK, 
            WM8962_DAC_MUTE);
            
    // HP Volume=0
    I2CModifyRegister(WM8962_HPOUTL_VOLUME,
            WM8962_HPOUTL_VOL_MASK | WM8962_HPOUT_VU_MASK,
            WM8962_HPOUT_VU);
    I2CModifyRegister(WM8962_HPOUTR_VOLUME,
            WM8962_HPOUTR_VOL_MASK | WM8962_HPOUT_VU_MASK,
            WM8962_HPOUT_VU);

    // Remove short
    I2CModifyRegister(WM8962_ANALOGUE_HP_0,
            WM8962_HP1L_RMV_SHORT_MASK,
            0);
    I2CModifyRegister(WM8962_ANALOGUE_HP_0,
            WM8962_HP1R_RMV_SHORT_MASK,
            0);
    Sleep(1);

    // Servo disable
    I2CModifyRegister(WM8962_DC_SERVO_1, 
            WM8962_HP1L_DCS_ENA | WM8962_HP1R_DCS_ENA | WM8962_HP1L_DCS_STARTUP | WM8962_HP1R_DCS_STARTUP, 
            0);

    // HP1L/R Disable+Intermediate stage disable+Output stage disable(Page 126)
	I2CModifyRegister(WM8962_ANALOGUE_HP_0, 
            WM8962_HP1L_ENA_MASK | WM8962_HP1R_ENA_MASK | WM8962_HP1L_ENA_DLY_MASK | WM8962_HP1R_ENA_DLY_MASK |WM8962_HP1L_ENA_OUTP | WM8962_HP1R_ENA_OUTP,
            0);
	
	return WM8962_SUCCESS;
}

BOOL WM8962Codec::MuteHP(BOOL bMuteVal)
{
	if(bMuteVal == TRUE)
	{
		// Mute
		I2CModifyRegister(WM8962_PWR_MGMT_2, 
                WM8962_HPOUTL_PGA_MUTE_MASK | WM8962_HPOUTR_PGA_MUTE_MASK,
                WM8962_HPOUTL_PGA_MUTE | WM8962_HPOUTR_PGA_MUTE);
	}
	else
	{
		// Unmute
		I2CModifyRegister(WM8962_PWR_MGMT_2, 
                WM8962_HPOUTL_PGA_MUTE_MASK | WM8962_HPOUTR_PGA_MUTE_MASK,
                0);
	}		

    // Update	
	I2CModifyRegister(WM8962_HPOUTL_VOLUME,
            WM8962_HPOUT_VU_MASK ,
            WM8962_HPOUT_VU);
	
	return WM8962_SUCCESS;
}

VOID WM8962Codec::HPDetected(BOOL bHPDetected)
{
	m_bHPDetected = bHPDetected;
}

VOID WM8962Codec::Beep()
{
	//  beep 1
	I2CModifyRegister(WM8962_BEEP_GENERATOR_1, 
            WM8962_BEEP_RATE_MASK,  
            0x03 << WM8962_BEEP_RATE_SHIFT);
	I2CModifyRegister(WM8962_BEEP_GENERATOR_1, 
            WM8962_BEEP_GAIN_MASK, 
            0x0C << WM8962_BEEP_GAIN_SHIFT);	
	I2CModifyRegister(WM8962_BEEP_GENERATOR_1, 
            WM8962_BEEP_ENA_MASK,  
            WM8962_BEEP_ENA);
	Sleep(2000);
	I2CModifyRegister(WM8962_BEEP_GENERATOR_1, 
            WM8962_BEEP_ENA_MASK, 
            0);

	//  beep 2
	I2CModifyRegister(WM8962_BEEP_GENERATOR_1, 
            WM8962_BEEP_RATE_MASK,  
            0x01 << WM8962_BEEP_RATE_SHIFT);
	I2CModifyRegister(WM8962_BEEP_GENERATOR_1, 
            WM8962_BEEP_GAIN_MASK, 
            0x0F << WM8962_BEEP_GAIN_SHIFT);
	I2CModifyRegister(WM8962_BEEP_GENERATOR_1, 
            WM8962_BEEP_ENA_MASK, 
            WM8962_BEEP_ENA);
	Sleep(2000);
	I2CModifyRegister(WM8962_BEEP_GENERATOR_1, 
            WM8962_BEEP_ENA_MASK, 
            0);
}

VOID WM8962Codec::TestMCLKPinToClockout5()
{
		//SYSCLOCK Disable(Should be disabled, when reconfiguring)+No Automatic Clocking
        I2CModifyRegister(WM8962_CLOCKING2,
                WM8962_SYSCLK_ENA_MASK | WM8962_CLKREG_OVD_MASK, 
                0 | WM8962_CLKREG_OVD);

        //Clocksource: MCLK Pin
        I2CModifyRegister(WM8962_CLOCKING2,
                WM8962_SYSCLK_SRC_MASK , 
                0 << WM8962_SYSCLK_SRC_SHIFT);

        //FLL Enable
        I2CModifyRegister(WM8962_FLL_CONTROL_1,
                WM8962_FLL_ENA_MASK, 
                WM8962_FLL_ENA);
        
		//BCLK = Source for CLKOUT
        I2CModifyRegister(WM8962_FLL_CONTROL_1,
                WM8962_FLL_REFCLK_SRC_MASK,
                1 << WM8962_FLL_REFCLK_SRC_SHIFT);
        
		//FLL ClockDivider:8
        I2CModifyRegister(WM8962_FLL_CONTROL_2,
                WM8962_FLL_OUTDIV_MASK,
                8 << WM8962_FLL_OUTDIV_SHIFT);
        ////FLL Forced control select(Page 172) + Oscillator Value = 12 Mhz
        //I2CModifyRegister(WM8962_FLL_CONTROL_5,WM8962_FLL_FRC_NCO | WM8962_FLL_FRC_NCO_VAL_MASK, 
        //    1 << WM8962_FLL_FRC_NCO_VAL_SHIFT | 0x19 << WM8962_FLL_FRC_NCO_VAL_SHIFT);

        //SYSCLOCK Enable
        I2CModifyRegister(WM8962_CLOCKING2,
                WM8962_SYSCLK_ENA_MASK, 
                WM8962_SYSCLK_ENA);

        //CLOCKOUT5_SEL:FLL
        I2CModifyRegister(WM8962_CLOCKING1,
                WM8962_CLKOUT5_SEL_MASK,
                1 << WM8962_CLKOUT5_SEL_SHIFT);
       
		//CLKOUT5 Output Enable
        I2CModifyRegister(WM8962_CLOCKING_3,
                WM8962_CLKOUT5_OE_MASK,
                WM8962_CLKOUT5_OE);
}

VOID WM8962Codec::SetupDigitalAudio()
{
	//put codec to Master Mode
    I2CModifyRegister(WM8962_AUDIO_INTERFACE_0,
            WM8962_MSTR_MASK,
            WM8962_MSTR);

    // ADCDAT is tri-stated; BCLK & LRCLK are set as inputs 
    //I2CModifyRegister(WM8962_ADDITIONAL_CONTROL_1,WM8962_AIF_TRI_MASK,WM8962_AIF_TRI);
	
    // Companding type Law-A
    //I2CModifyRegister(WM8962_AUDIO_INTERFACE_1, WM8962_DAC_COMPMODE_MASK | WM8962_ADC_COMPMODE_MASK , WM8962_DAC_COMPMODE | WM8962_ADC_COMPMODE);
  
	// Companding enable
    //I2CModifyRegister(WM8962_AUDIO_INTERFACE_1, WM8962_DAC_COMP_MASK | WM8962_ADC_COMP_MASK , WM8962_DAC_COMP | WM8962_ADC_COMP);

    // Unmute DAC
    //MuteDAC(FALSE);
}

BOOL WM8962Codec::SetuptMic(WM8962_ANA_MIC_INPUT_PIN micPin)
{
    switch(micPin)
    {
    case WM8962_IN3R:
    
        // Select IN3R as mic input 
        I2CModifyRegister(WM8962_RIGHT_INPUT_PGA_CONTROL, 
                WM8962_IN1R_TO_INPGAR_MASK | WM8962_IN2R_TO_INPGAR_MASK | WM8962_IN3R_TO_INPGAR_MASK | WM8962_IN4R_TO_INPGAR_MASK,
                WM8962_IN1R_TO_INPGAR | WM8962_IN3R_TO_INPGAR);
                
        // Analogue Input Power Up sequence from Write Sequencer
        I2CWriteRegister(WM8962_WRITE_SEQUENCER_CONTROL_2, 0x92);
        Sleep(75);
        
        // Enable INPGA
        I2CModifyRegister(WM8962_RIGHT_INPUT_VOLUME,
                WM8962_INPGAL_MUTE_MASK,
                WM8962_INPGAL_MUTE);
        I2CModifyRegister(WM8962_RIGHT_INPUT_VOLUME,
                WM8962_IN_VU_MASK,
                WM8962_IN_VU);
                
        // Enable ALC on right channel, and set traget range to high
        I2CModifyRegister(WM8962_ALC1,
                WM8962_ALCR_ENA_MASK | WM8962_ALC_LVL_MODE_MASK,
                WM8962_ALCR_ENA | WM8962_ALC_LVL_MODE);
                
        // Set INPGAR to MIXINR & IN3R to MIXINR volumes
        I2CModifyRegister(WM8962_RIGHT_INPUT_MIXER_VOLUME, 
                WM8962_INPGAR_MIXINR_VOL_MASK | WM8962_IN3R_MIXINR_VOL_MASK , 
                (WM8962_INPGAR_TO_MIXINR_VOLUME << WM8962_INPGAR_MIXINR_VOL_SHIFT) | WM8962_IN3R_TO_MIXINR_VOLUME);
                
        // Enable Boost-Mixer on IN3R (Mixer already enabled)
        I2CModifyRegister(WM8962_INPUT_MIXER_CONTROL_2, 
                WM8962_IN3R_TO_MIXINR_MASK, 
                WM8962_IN3R_TO_MIXINR);   

        break;
        
    default :
    
        RETAILMSG(TRUE, (TEXT("%s(%d): Pin not implemented !\r\n"),__WFUNCTION__, micPin));
        return WM8962_FAILURE;
            
        break;
    }

    return WM8962_SUCCESS;
}