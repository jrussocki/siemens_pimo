#ifndef __WM8962CODEC_H__
#define __WM8962CODEC_H__

#include "wm8962_regs.h"

/*
	Notes:  
	- Only I2C is implemented as a control interface
	- Only support for clock on external pin (MCLK) implemented
	- Hardware automute not implemented
	- Sample rate is untouched, possibility to change not implemented
	- Output PGA Volumes are set as defines, only DAC volume is changeable
	- SPKMIX / HPMIX additional features not implemented
	- No Audio Enhancement DSP features touched
    - No Microphone detection/push button implemented
    - Digital Microphone not implemented
*/

/****************************		Codec Config		*****************************************/
#define WM8962_EXPECTED_ID	0x69f
#define WM8962_BITSPERSAMPLE	24
#define WM8962_VMID_CHARGING_MODE	0x01	// Normal mode
#define WM8962_SPKOUT_PGA_VOLUME	0xE0
#define WM8962_HPOUT_PGA_VOLUME	0xDD
#define WM8962_DEFAULT_ADC_VOLUME 0xFC // +22.5dB
#define WM8962_INPGAR_TO_MIXINR_VOLUME 0x110 // +27dB
#define WM8962_IN3R_TO_MIXINR_VOLUME 0x111 // +6dB
#define WM8962_MIC_PIN  WM8962_IN3R

/*****************************		I2C Config		*****************************************/
#define WM8962_I2C_BUS			I2C1_FID
#define WM8962_I2C_DEVICE_ID	0x1A	// Datasheet states 0x34, but as an 8-bit address (includes already the R/W bit)
#define WM8962_I2C_SPEED		I2C_MAX_FREQUENCY


typedef enum
{
    WM8962_SUCCESS = TRUE,
    WM8962_FAILURE = FALSE
} WM8962_RETURN_VALUE;

typedef enum
{
    WM8962_INPUT_MIC    = 0,
    WM8962_INPUT_LINEIN = 1
} WM8962_ADC_INPUT_TYPE;

typedef enum
{
    WM8962_IN1L = 0,
    WM8962_IN1R = 1,
    WM8962_IN2L = 2,
    WM8962_IN2R = 3,
    WM8962_IN3L = 4,
    WM8962_IN3R = 5,
    WM8962_IN4L = 6,
    WM8962_IN4R = 7
} WM8962_ANA_MIC_INPUT_PIN;


class WM8962Codec
{
private:
    HANDLE m_hI2C;
    BOOL m_bI2CInited;
    CRITICAL_SECTION m_csI2C;
	BOOL m_bForceSpeaker;
	BOOL m_bHPDetected;
    UINT16 m_lADCVol;
	UINT16 m_rADCVol;
    
public:
	WM8962Codec();
	~WM8962Codec();
	
	/*******************
	*	I2C Functions	*
	********************/
    BOOL I2CInit();
    VOID I2CClose();
    BOOL I2CWriteRegister(UINT16 regAddr, UINT16 data);
    BOOL I2CReadRegister(UINT16 regAddr, UINT16 * pData);
    BOOL I2CModifyRegister(UINT16 regAddr, UINT16 mask, UINT16 bitFieldValue);
	
	/************************
	*	Codec Functions	*
	*************************/	
	// Setups
	BOOL CodecInit(); 
	BOOL GetID();  // useful to check hardware revison
	BOOL SetSysClock(UINT16 param);
	BOOL ChipPowerUp(); 
	BOOL ChipPowerDown();
	
	// DAC
	BOOL PowerOnDAC();
	BOOL PowerOffDAC();
	BOOL SetDACVolume(BYTE leftVol, BYTE rightVol);
	BOOL MuteDAC(BOOL bMuteVal);
	
	// ADC
    BOOL SetLoopback(BOOL bEnable);
    BOOL SelectADCInput(WM8962_ADC_INPUT_TYPE inputType);
    BOOL PowerOnADC();
	BOOL PowerOffADC();
    BOOL SetADCVolume(BYTE leftVol, BYTE rightVol);
	BOOL MuteADC(BOOL bMuteVal);
    
	// Speakers	
	BOOL PowerOnSpeakers();
	BOOL PowerOffSpeakers();
	BOOL MuteSpeakers(BOOL bMuteVal); 
	VOID ForceSpeaker(BOOL bForceSpeaker);
	VOID SpeakerHeadphoneSelection();
	
	// Headphone
	BOOL PowerOnHP();
	BOOL PowerOffHP();
	BOOL MuteHP(BOOL bMuteVal);
	VOID HPDetected(BOOL bHPDetected);
	
	// Test/Debug
	VOID Beep();	// allow to test output without a working DAI
	VOID TestMCLKPinToClockout5(); // enable SYSCLK on CLOCKOUT5
	
	// Digital Audio Interface (I2S)
	VOID SetupDigitalAudio();
	
	// Digital Microphone/GPIO
    
    // Analogue Microphone
    BOOL SetuptMic(WM8962_ANA_MIC_INPUT_PIN micPin);
};


#endif /*__WM8962CODEC_H__*/