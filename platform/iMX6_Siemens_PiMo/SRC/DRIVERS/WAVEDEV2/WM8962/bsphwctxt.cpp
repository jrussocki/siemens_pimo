#include "bsp.h"
#include "mx6_ddk.h"
#include "wavemain.h"
#include "wm8962codec.h"

//-----------------------------------------------------------------------------
// Defines
#define WM8962_REG_PATH	"Drivers\\BuiltIn\\Audio"
#define DEBOUNCE_TIME_10MS  40   // 400 MS

//-----------------------------------------------------------------------------
// Global Variables
static WM8962Codec *g_pWM8962Codec;
DWORD g_dwBoardID = 0;

// Local Variables (only for HP Detection Debouncing)
static  BOOL g_bDebounceStarted = FALSE;
static  BOOL g_bDebounceThreadTerminated = FALSE;
static  BOOL g_bLastHPState;
static HANDLE g_hHPDetectDebounceEvent;
static HANDLE g_hHPDetectDebounceThread;
static CRITICAL_SECTION g_CritSecDebounce;
static INT32 g_DebounceLeftTimes;


//-----------------------------------------------------------------------------
//
//  Function: BSPAudioGetInputDeviceNum
//
//  Get device number capabile of recording
//
//  Parameters:
//      None
//
//  Returns:
//      UINT8 Device number
//
//-----------------------------------------------------------------------------
UINT8 BSPAudioGetInputDeviceNum()
{
#ifdef AUDIO_NOREC
    return 0;
#else
    return 1;
#endif
}

//-----------------------------------------------------------------------------
//
//  Function: BSPAllocOutputDMABuffer
//
//  Allocate the audio output DMA buffers.
//
//  Parameters:
//      pVirtAddr
//          [out] The virtual address of the buffer.
//
//      pPhysAddr
//          [out] The physical address of the buffer.
//
//  Returns:
//      The buffer size in bytes.
//
//-----------------------------------------------------------------------------
UINT16 BSPAllocOutputDMABuffer(PBYTE *pVirtAddr, PHYSICAL_ADDRESS *pPhysAddr)
{
    // Use a statically defined memory region for the audio DMA buffers. This
    // may be either internal or external memory depending upon the address
    // region that is selected.
    pPhysAddr->HighPart = 0;
    pPhysAddr->LowPart  = BSP_AUDIO_DMA_BUF_ADDR;

    *pVirtAddr = (PBYTE)MmMapIoSpace(*pPhysAddr, BSP_AUDIO_DMA_BUF_SIZE, FALSE);

    // Default mapping of audio buffer will be noncacheable-nonbufferable (strongly ordered).
    // Strongly ordered memory will cause a data memory barrier to be issued before and
    // after each memory access and prevents burst accesses.  Audio rendering performance 
    // can be significantly improved by remapping the audio buffer as noncacheable-bufferable 
    // (normal outer/inner non-cacheable).
    //
    // Small Page Descriptor:
    //
    //  TEX[2:0], C, B = 1, 0, 0 => normal, outer/inner non-cacheable
    //
    //                       FLAGS                    MASK
    //  TEX = 1 = (1 << 6) = 0x040      (0x7 << 6) = 0x1C0
    //    C = 0 = (0 << 3) = 0x000      (0x1 << 3) = 0x008
    //    B = 0 = (0 << 2) = 0x000      (0x1 << 2) = 0x004
    //  --------------------------------------------------
    //                       0x040                   0x1CC
    //
    //
    if (*pVirtAddr)
    {
        VirtualSetAttributes(*pVirtAddr, BSP_AUDIO_DMA_BUF_SIZE, 0x040, 0x1CC, NULL);
    }

    return BSP_AUDIO_DMA_BUF_SIZE;
}

//-----------------------------------------------------------------------------
//
//  Function: BSPDeallocOutputDMABuffer
//
//  Deallocate the audio output DMA buffers.
//
//  Parameters:
//      VirtAddr
//          [in] The virtual address of the buffer.
//
//  Returns:
//      The buffer size in bytes.
//
//-----------------------------------------------------------------------------
VOID BSPDeallocOutputDMABuffer(PBYTE VirtAddr)
{
    // Nothing do dealloc since we get output DMA buffer from internal RAM
    UNREFERENCED_PARAMETER(VirtAddr);
}

//-----------------------------------------------------------------------------
//
//  Function: BSPAllocInputDMABuffer
//
//  Allocate the audio input DMA buffers.
//
//  Parameters:
//      pVirtAddr
//          [out] The virtual address of the buffer.
//
//      pPhysAddr
//          [out] The physical address of the buffer.
//
//  Returns:
//      The buffer size in bytes.
//
//-----------------------------------------------------------------------------
UINT16 BSPAllocInputDMABuffer(PBYTE *pVirtAddr, PHYSICAL_ADDRESS *pPhysAddr)
{
    DMA_ADAPTER_OBJECT Adapter;
    memset(&Adapter, 0, sizeof(DMA_ADAPTER_OBJECT));
    Adapter.InterfaceType = Internal;
    Adapter.ObjectSize    = sizeof(DMA_ADAPTER_OBJECT);

    // Allocate the input DMA buffers (same size as output) from external memory
    *pVirtAddr = (PBYTE)HalAllocateCommonBuffer(
                  &Adapter,
                  BSP_AUDIO_DMA_BUF_SIZE,
                  pPhysAddr,
                  FALSE);
    
    return BSP_AUDIO_DMA_BUF_SIZE;
}

//-----------------------------------------------------------------------------
//
//  Function: BSPDeallocOutputDMABuffer
//
//  Deallocate the audio output DMA buffers.
//
//  Parameters:
//      VirtAddr
//          [in] The virtual address of the buffer.
//
//  Returns:
//      The buffer size in bytes.
//
//-----------------------------------------------------------------------------
VOID BSPDeallocInputDMABuffer(PBYTE VirtAddr)
{
    // Only deallocate the audio DMA buffer memory if it was previously
    // dynamically allocated.
    PHYSICAL_ADDRESS phyAddr;

    // Logical address parameter is ignored
    phyAddr.QuadPart = 0;

    DMA_ADAPTER_OBJECT Adapter;
    memset(&Adapter, 0, sizeof(DMA_ADAPTER_OBJECT));
    Adapter.InterfaceType = Internal;
    Adapter.ObjectSize    = sizeof(DMA_ADAPTER_OBJECT);

    HalFreeCommonBuffer(&Adapter, BSP_AUDIO_DMA_BUF_SIZE, phyAddr, VirtAddr, FALSE);
}

//-----------------------------------------------------------------------------
//
//  Function: BSPAudioDeinitCodec
//
//  This function de-initializes audio codec.
//
//  Parameters:
//      None.
//
//  Returns:
//      TRUE for success, and FALSE for failure.
//
//-----------------------------------------------------------------------------
VOID BSPAudioDeinitCodec()
{
	if (g_pWM8962Codec)
    {
        delete g_pWM8962Codec;
    }	
	
	// Deactivate CLK0
    DDKClockSetCKO1(false, DDK_CLOCK_CKO1_SRC_VIDEO_27M, DDK_CLOCK_CKO_DIV_1);
	
	// Power-Down Audio Chip
	DDKGpioWriteDataPin(BSP_AUDIO_POWER_GPIO_PORT, BSP_AUDIO_POWER_GPIO_PIN, 0);
}

//-----------------------------------------------------------------------------
//
//  Function: BSPAudioStartDAC
//
//  This function configures the codec for duplex mode operation and powers on
//  audio codec for playback operation
//
//  Parameters:
//      None.
//
//  Returns:
//      None.
//
//-----------------------------------------------------------------------------
VOID BSPAudioStartDAC()
{ 
    g_pWM8962Codec->PowerOnDAC();
}

//-----------------------------------------------------------------------------
//
//  Function: BSPAudioStopDAC
//
//  This function stop playback operation
//  Parameters:
//      None.
//
//  Returns:
//      None.
//
//-----------------------------------------------------------------------------
VOID BSPAudioStopDAC()
{
    g_pWM8962Codec->PowerOffDAC();
}

//-----------------------------------------------------------------------------
//
//  Function: BSPAudioStartADC
//
//  This function configures the codec for duplex mode operation and powers on
//  audio codec for record operation
//
//  Parameters:
//      None.
//
//  Returns:
//      None.
//
//-----------------------------------------------------------------------------
VOID BSPAudioStartADC()
{
    g_pWM8962Codec->PowerOnADC();
}

//-----------------------------------------------------------------------------
//
//  Function: BSPAudioStopADC
//
//  This function stop record operation
//
//  Parameters:
//      None.
//
//  Returns:
//      None.
//
//-----------------------------------------------------------------------------
VOID BSPAudioStopADC()
{
    g_pWM8962Codec->PowerOffADC();
}

//-----------------------------------------------------------------------------
//
//  Function: BSPAudioSelectADCSource
//
//  This function selects the input source into audio codec ADC.
//
//  Parameters:
//      nIndex
//          [in] Input source index. 0 - MIC, 1 - Line In.
//
//  Returns:
//      None.
//
//-----------------------------------------------------------------------------
VOID BSPAudioSelectADCSource(DWORD nIndex)
{
    if (nIndex == 0)
    {
        g_pWM8962Codec->SelectADCInput(WM8962_INPUT_MIC);
    }
    else
    {
        g_pWM8962Codec->SelectADCInput(WM8962_INPUT_LINEIN); 
    }
}

//-----------------------------------------------------------------------------
//
//  Function: BSPAudioSetCodecLoopback
//
//  This function turns on/off codec loopback function.
//
//  Parameters:
//      bEnable
//          [out] TRUE for on, FALSE for OFF.
//
//  Returns:
//      TRUE for success, and FALSE for failure.
//
//-----------------------------------------------------------------------------
BOOL BSPAudioSetCodecLoopback(BOOL bEnable)
{
    return g_pWM8962Codec->SetLoopback(bEnable);
}

//-----------------------------------------------------------------------------
//
//  Function: BSPAudioSetOutputGain
//
//  This function configures the Stereo DAC output digital volume
//  level based upon the volume level that Windows is currently requesting.
//
//  Parameters:
//      dwGain
//          [in] The desired output volume level.
//
//  Returns:
//      None.
//
//-----------------------------------------------------------------------------
VOID BSPAudioSetOutputGain(DWORD dwGain)
{
    // Get left and right MSB
    DWORD leftGain = (dwGain & 0xFF00) >> 8; 
    DWORD rightGain = (dwGain & 0xFF000000) >> 24;
     
    g_pWM8962Codec->SetDACVolume((BYTE) leftGain, (BYTE) rightGain);
}

//-----------------------------------------------------------------------------
//
//  Function: BSPAudioSetInputGain
//
//  This function configures the Stereo ADC's digital input gain
//  level based upon the volume level that Windows is currently requesting.
//
//  Parameters:
//      dwGain
//          [in] The desired input volume level.
//
//  Returns:
//      None.
//
//-----------------------------------------------------------------------------
VOID BSPAudioSetInputGain(DWORD dwGain)
{
    // Get left and right MSB
    DWORD leftGain = (dwGain & 0xFF00) >> 8; 
    DWORD rightGain = (dwGain & 0xFF000000) >> 24;
    
    g_pWM8962Codec->SetADCVolume((BYTE) leftGain, (BYTE) rightGain);
}

//-----------------------------------------------------------------------------
//
//  Function: BSPAudioIomuxConfig
//
//  This function configures the IOMUX pins required for the AUDMUX.
//
//  Parameters:
//      None.
//
//  Returns:
//      None.
//
//-----------------------------------------------------------------------------
VOID BSPAudioIomuxConfig()
{
	/*********************************************************   Audio MUX  (4pins)  ***************************/
	// The key here is to configure the 4-wire interface (framesync, bitclock,
    // TX, and RX) through the IOMUX by enabling the functional/normal mode.
	// DONE in the bootloader
	
	/*********************************************************   HEADPHONE_DET   *******************************/	
	DDKGpioSetConfig(BSP_AUDIO_HPD_GPIO_PORT, BSP_AUDIO_HPD_GPIO_PIN, DDK_GPIO_DIR_IN, DDK_GPIO_INTR_FALL_EDGE);
	DDKGpioClearIntrPin(BSP_AUDIO_HPD_GPIO_PORT, BSP_AUDIO_HPD_GPIO_PIN);
	
	/*********************************************************   MIC_DETECT   **********************************/
	
}

//-----------------------------------------------------------------------------
//
//  Function: BSPSsiEnableClock
//
//  This function is to enable/disable SSI clock.
//
//  Parameters:
//      index
//          [in] Index specifying the SSI instance.
//
//      bEnable
//          [in] TRUE if the clock is to be enabled, otherwise FALSE.
//
//  Returns:
//      TRUE if successfully performed the required action.
//
//-----------------------------------------------------------------------------
BOOL BSPSsiEnableClock(UINT32 index, BOOL bEnable)
{

	DDK_CLOCK_GATE_INDEX clockGateIndex;
	
	/* Note: platform\common\src\soc\COMMON_FSL_V3\WAVEDEV2\hwctxt.cpp uses SSI2, 
				 so only index = 2 should happen here	*/
    switch (index)
    {
        case 1:
            clockGateIndex = DDK_CLOCK_GATE_INDEX_SSI1;
            break;

        case 2:
            clockGateIndex = DDK_CLOCK_GATE_INDEX_SSI2;
            break;

        default:
            return FALSE;
    }
    
    if (bEnable)
    {
        return DDKClockSetGatingMode(clockGateIndex, 
                                     DDK_CLOCK_GATE_MODE_ENABLED_ALL);
    }
    else
    {
        return DDKClockSetGatingMode(clockGateIndex, 
                                     DDK_CLOCK_GATE_MODE_DISABLED);
    }
}

//-----------------------------------------------------------------------------
//
//  Function: BSPAudmuxEnableClock
//
//  This function is to enable/disable AUDMUX clock.
//
//  Parameters:
//      bEnable
//          [in] TRUE if the clock is to be enabled, otherwise FALSE.
//
//  Returns:
//      TRUE if successfully performed the required action.
//
//-----------------------------------------------------------------------------
BOOL BSPAudmuxEnableClock(BOOL bEnable)
{
	UNREFERENCED_PARAMETER(bEnable);
	return TRUE;
}

//-----------------------------------------------------------------------------
//
//  Function: BSPAudioGetSdmaPriority
//
//  This function returns the priority of the SDMA channel used by Audio driver.
//
//  Parameters:
//      None.
//
//  Returns:
//      The priority of the SDMA channel.
//
//-----------------------------------------------------------------------------
UINT8 BSPAudioGetSdmaPriority()
{
    return BSP_SDMA_CHNPRI_AUDIO;
}

//-----------------------------------------------------------------------------
//
//  Function: BSPAudioPowerUp
//
//  This function powers up the SSI, AUDMUX, and external audio chip.
//
//  Parameters:
//      fullPowerUp
//          [in] Flag of full power-up.
//
//  Returns:
//      None.
//
//-----------------------------------------------------------------------------
VOID BSPAudioPowerUp(const BOOL fullPowerUp)
{
    if (fullPowerUp)
    {
        // config CLKO
        DDKClockSetCKO1(true, DDK_CLOCK_CKO1_SRC_VIDEO_27M, DDK_CLOCK_CKO_DIV_1);
        
        g_pWM8962Codec->CodecInit();
    }
}

//-----------------------------------------------------------------------------
//
//  Function: BSPAudioPowerDown
//
//  This function powers down the SSI, AUDMUX, and external audio chip.
//
//  Parameters:
//      fullPowerOff
//          [in] Flag of full power-down.
//
//  Returns:
//      None.
//
//-----------------------------------------------------------------------------
VOID BSPAudioPowerDown(const BOOL fullPowerOff)
{
    if (fullPowerOff)
    {
        g_pWM8962Codec->ChipPowerDown();
        
        // config CLKO
        DDKClockSetCKO1(false, DDK_CLOCK_CKO1_SRC_VIDEO_27M, DDK_CLOCK_CKO_DIV_1);
    }
}

//-----------------------------------------------------------------------------
//
//  Function: BSPAudioGetProtocol
//
//  This function gets the audio protocol used on SSI.
//
//  Parameters:
//      None.
//
//  Returns:
//      The audio protocol.
//
//-----------------------------------------------------------------------------
AUDIO_PROTOCOL BSPAudioGetProtocol()
{
    return AUDIO_PROTOCOL_I2S;
}

//-----------------------------------------------------------------------------
//
//  Function: BSPGetAudmuxIntPort
//
//  This function is to get Audmux internal port
//
//  Parameters:
//     None
//
//  Returns:
//      return the Audmux internal port
//
//-----------------------------------------------------------------------------
AUDMUX_INTERNAL_PORT BSPGetAudmuxIntPort()
{
	return AUDMUX_PORT2;	// platform\common\src\soc\COMMON_FSL_V3\WAVEDEV2\hwctxt.cpp uses SSI2
}

//-----------------------------------------------------------------------------
//
//  Function: BSPGetAudmuxExtPort
//
//  This function is to get Audmux external port
//
//  Parameters:
//     None
//
//  Returns:
//      return the Audmux external port
//
//-----------------------------------------------------------------------------
AUDMUX_EXTERNAL_PORT BSPGetAudmuxExtPort()
{
	return AUDMUX_PORT3;
}

//-----------------------------------------------------------------------------
//
//  Function: BSPSetSpeakerEnable
//
//  the WaveDev2 architecture can send a waveoutmessage to enable the
//  audio output stream to play out of the external speaker.  This function sets
//  a flag on codec to indicate whether MM_WOM_FORCESPEAKER flag is set or not 
//
//  Parameters:
//      bEnable
//          [in] enable/disable Speaker
//
//  Returns:
//      None.
//
//-----------------------------------------------------------------------------
VOID BSPSetSpeakerEnable(BOOL bEnable)
{
    if (bEnable)
    {
        // Set a flag indicating the codec that force speaker flag is set
        g_pWM8962Codec->ForceSpeaker(TRUE);
    }
    else
    {
        // Update the force speaker flag
        g_pWM8962Codec->ForceSpeaker(FALSE);
    }

    // Audio playback will be done via speakers or headphones based on 
    // MM_WOM_FORCESPEAKER flag and whether the Headphones are plugged in
    g_pWM8962Codec->SpeakerHeadphoneSelection();
}

//-----------------------------------------------------------------------------
//
//  Function: DebounceLock
//
//  Lock for debounce operation 
//
//  Parameters:
//      None.
//
//  Returns:
//      None.
//
//----------------------------------------------------------------------------- 
static inline VOID DebounceLock()
{
    EnterCriticalSection(&g_CritSecDebounce);
}

//-----------------------------------------------------------------------------
//
//  Function: DebounceUnLock
//
//  UnLock for debounce operation 
//
//  Parameters:
//      None.
//
//  Returns:
//      None.
//
//-----------------------------------------------------------------------------
static inline VOID DebounceUnLock()
{
    LeaveCriticalSection(&g_CritSecDebounce);
}

//-----------------------------------------------------------------------------
//
//  Function: BSPStartHPDetectDebounce
//
//  This function check if a HP detect interrupt debounce needed 
//  detect
//
//  Parameters:
//      bHPDetected
//          [in] Flag of HP detected state.
//
//  Returns:
//      None.
//
//----------------------------------------------------------------------------- 
static VOID BSPStartHPDetectDebounce(BOOL bHPDetected)
{   
    g_bLastHPState = bHPDetected; 
    if (!g_bDebounceStarted)
    {
        DebounceLock();
        g_bDebounceStarted = TRUE;
        g_DebounceLeftTimes = DEBOUNCE_TIME_10MS; 
        DebounceUnLock();
        SetEvent(g_hHPDetectDebounceEvent); 
    }
    else
    {
        DebounceLock();
        g_DebounceLeftTimes = DEBOUNCE_TIME_10MS;
        DebounceUnLock();      
    } 
} 

//-----------------------------------------------------------------------------
//
//  Function: BSPStopHPDetectDebounce
//
//  This function stop debounce of HP detect interrupt 
//  detect
//
//  Parameters:
//      None.
//
//  Returns:
//      None.
//
//-----------------------------------------------------------------------------
static VOID BSPStopHPDetectDebounce()
{ 
    if (g_bDebounceStarted)   
    {
        // Audio playback will be done via speakers or headphones based on 
        // MM_WOM_FORCESPEAKER flag and whether the Headphones are plugged in
        g_pWM8962Codec->HPDetected(g_bLastHPState);
        g_pWM8962Codec->SpeakerHeadphoneSelection();
        
        DebounceLock();
        g_bDebounceStarted = FALSE;
        DebounceUnLock();
    }
} 

//-----------------------------------------------------------------------------
//
//  Function: BSPHPDetectDebounceThreadProc
//
//  main function of debounce of HP detect interrupt 
//  detect
//
//  Parameters:
//      lpParam
//          [in] not used
//
//  Returns:
//      BOOL, thread return code.
//
//-----------------------------------------------------------------------------
static VOID BSPHPDetectDebounceThreadProc(LPVOID lpParam)
{
    UNREFERENCED_PARAMETER(lpParam);
 
    while (!g_bDebounceThreadTerminated)
    {
        if (WaitForSingleObject(g_hHPDetectDebounceEvent, INFINITE) == WAIT_OBJECT_0)
        {
            if (g_bDebounceThreadTerminated)
                break;

            while (g_DebounceLeftTimes > 0)
            {
                DebounceLock();
                g_DebounceLeftTimes--; 
                DebounceUnLock();

                if (g_DebounceLeftTimes == 0)
                    BSPStopHPDetectDebounce();
                else
                    Sleep(10);
            }

        }
    }

    ExitThread(TRUE);
}

//-----------------------------------------------------------------------------
//
//  Function: BSPAudioHandleHPDetectIRQ
//
//  This function handles the HP detect IRQ on the GPIO pin.  It implements the
//  following:
//  1. Checks interrupt status (ISR) to see if the GPIO IRQ event has happened
//  2. If yes, read the state of the GPIO pin.  If high, HP is plugged (value = 1)
//     If low, HP is not plugged (value = 0)
//  3. Set the HP detect value on the codec object which will handle it appropriately
//  4. Clear the ISR

//  Parameters:
//      None.
//
//  Returns:
//      None.
//
//-----------------------------------------------------------------------------
VOID BSPAudioHandleHPDetectIRQ() 
{
	UINT32 HPDetected, status;
  
	// Read the interrupt status register associated with HP Detection
    DDKGpioReadIntr(BSP_AUDIO_HPD_GPIO_PORT, BSP_AUDIO_HPD_GPIO_PIN, &status);

    // If the status is high, 
    if(status)
    {
        // Read the value on the GPIO pin
        DDKGpioReadDataPin(BSP_AUDIO_HPD_GPIO_PORT, BSP_AUDIO_HPD_GPIO_PIN, &HPDetected);       

        // Low : HP plugin
        // High :HP remove 
        HPDetected = !HPDetected;

        // Set the value on the codec object which will handle it appropriately
        g_pWM8962Codec->HPDetected(HPDetected);
        BSPStartHPDetectDebounce(HPDetected);

        // clears the interrupt status register
        DDKGpioClearIntrPin(BSP_AUDIO_HPD_GPIO_PORT, BSP_AUDIO_HPD_GPIO_PIN);
    }
}

//-----------------------------------------------------------------------------
//
//  Function: BSPIsHeadphoneDetectSupported
//
//  This function returns whether the iMX platform supports HP detection
//
//  Parameters:
//      None
//
//  Returns:
//      Bool - True or False
//
//-----------------------------------------------------------------------------
BOOL BSPIsHeadphoneDetectSupported()
{
	return TRUE;	 
}

//-----------------------------------------------------------------------------
//
//  Function: BSPGetHPDetectIRQ
//
//  This function returns the IRQ number for the GPIO connected to headphone 
//  detect
//
//  Parameters:
//      None
//
//  Returns:
//      IRQ number.
//
//-----------------------------------------------------------------------------
DWORD BSPGetHPDetectIRQ()
{
    return BSP_AUDIO_HPD_IRQ;
}

//-----------------------------------------------------------------------------
//
//  Function: BSPGetBitsPerSample
//
//  This function returns BitsPerSample supported on this platform
//
//  Parameters:
//      None.
//
//  Returns:
//      BitsPerSample.
//
//-----------------------------------------------------------------------------
WORD BSPGetBitsPerSample()
{
	return WM8962_BITSPERSAMPLE;
}
//-----------------------------------------------------------------------------
//
//  Function: BSPAudioInitCodec
//
//  This function initializes audio codec.
//
//  Parameters:
//      None.
//
//  Returns:
//      TRUE for success, and FALSE for failure.
//
//-----------------------------------------------------------------------------
BOOL BSPAudioInitCodec()
{	
	/******************************************************  Init HPDetectDebounce ************************************************/
	g_hHPDetectDebounceEvent =  CreateEvent(NULL, FALSE, FALSE, TEXT("EVENT_HP_DEBOUNCE"));
    g_hHPDetectDebounceThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)BSPHPDetectDebounceThreadProc, 0, 0, NULL);

    if (!g_hHPDetectDebounceThread)
    {
        ERRORMSG(TRUE ,(TEXT("%s() : CreateThread failed\r\n"), __WFUNCTION__));
    }

    InitializeCriticalSection(&g_CritSecDebounce);
	
	/******************************************************   Power-Up Audio Chip   ************************************************/

	DDKGpioSetConfig(BSP_AUDIO_POWER_GPIO_PORT, BSP_AUDIO_POWER_GPIO_PIN, DDK_GPIO_DIR_OUT, DDK_GPIO_INTR_NONE);
	DDKGpioWriteDataPin(BSP_AUDIO_POWER_GPIO_PORT, BSP_AUDIO_POWER_GPIO_PIN, 1);

	/*********************************************************   SYS_MCLK   ***********************************************************/
    // Config CLK0
    DDKClockSetCKO1(true, DDK_CLOCK_CKO1_SRC_VIDEO_27M, DDK_CLOCK_CKO_DIV_1);

	// Codec creation
	g_pWM8962Codec = new WM8962Codec;


    if (g_pWM8962Codec == NULL)
    {
		ERRORMSG(TRUE, (TEXT("%s(): Init WM8962Codec class failed!\r\n"),__WFUNCTION__));
		BSPAudioDeinitCodec();
        return FALSE;
    }

	// WM8962 Initialize
	if (!g_pWM8962Codec->I2CInit())
    {
		ERRORMSG(TRUE, (TEXT("%s(): I2C Init failed!\r\n"),__WFUNCTION__));
        return FALSE;
    }
	g_pWM8962Codec->CodecInit();

	// Need to check the HP when starting up
    BSPAudioHandleHPDetectIRQ();    
	
	return TRUE;
}