
DATE: 	25/04/2013

What has been done:
- I2C communications
- Clocking and sample rates: the codec is configured to use the MCLK Pin as clocksource
- Power On/Off DAC, HP, Speakers
- Beep generation, to test speakers/HP
- function TestMCLKPinToClockout5() for testing the clock output on the CLKOUT5 pin
- Digital Audio Interface configured (I2S, master mode)
- Output volume control between BSP and codec
- Microphone path configuration on pin IN3R
- Power On/Off ADC
- Integration in the BSP: Catalog Item, platform.bib/reg, bsphwctxt.cpp functions, 
    correct pin muxing (also in bspi2c.c), defines in bsp_cfg.h

To do: 
- Improve microphone/input volume
- Optimizations (automute, improve power consumption)
- Use of RegKey
- See also the Notes in wm8962codec.h
