//------------------------------------------------------------------------------
//
//  Copyright (C) 2004-2011 Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  bsptouch.c
//
//  Provides the implementation for BSP-specific touch support.
//
//------------------------------------------------------------------------------

#pragma warning(push)
#pragma warning(disable: 4115 4201)
#include <windows.h>
#pragma warning(pop)

#include "bsp.h"
#include "i2cbus.h"
#include <tchstream.h>


#define USE_LVDS0 
//------------------------------------------------------------------------------
// TOUCH Board I2C Configuration   
//------------------------------------------------------------------------------
#define TCH_ALL_BITS               0xFF
#define TCH_I2C_HSD100_ADDRESS     0x41
//#define TCH_I2C_HSD100_ADDRESS     0x21

#define TCH_I2C_EXC7200_ADDRESS    0x4
#define TCH_VER_ID                 0x10
#define TCH_I2C_SPEED              400000
#define COMMOND_DELAY              50 
#define CSI_I2C_ADDRESS            00
//------------------------------------------------------------------------------

typedef enum _TOUCH_ID {
    TOUCH_EXC7200 = 0,
    TOUCH_HSD100,
    TOUCH_NULL
} TOUCH_ID;

static  BYTE Idle_Common[10]    = {0x03,0x06,0x0a,0x04,0x36,0x3f,0x01,0x1,0x0,0x0};
static  BYTE Sleep_Common[10]   = {0x03,0x06,0x0a,0x03,0x36,0x3f,0x02,0x0,0x0,0x0};
static  BYTE Version_Common[10] = {0x03,0x03,0x0a,0x1, 'D',0x0,0x0,0x0,0x0,0x0};

//-----------------------------------------------------------------------------
// External Functions

//-----------------------------------------------------------------------------
// External Variables
extern const int MIN_CAL_COUNT = 20;

static HANDLE   g_hI2C=NULL;
static TOUCH_ID g_id  =TOUCH_EXC7200;


//-----------------------------------------------------------------------------
// Defines

// Maximum allowed variance in the X coordinate samples.
#define DELTA_X_COORD_VARIANCE          60

// Maximum allowed variance in the X coordinate samples.
#define DELTA_Y_COORD_VARIANCE          60

#define ABS(x)  ((x) >= 0 ? (x) : (-(x)))

#define MAX_RESIT_VALUE 800

#ifndef ZONE_FUNCTION
#define ZONE_FUNCTION   DEBUGZONE(2)
#endif

//-----------------------------------------------------------------------------
// Types

#define TSC_SINGLE_TOUCH            0x1
#define TSC_DIALOG_MODE             0x3
#define TSC_MULTI_TOUCH             0x4

#define RK_HARDWARE_DEVICEMAP_TOUCH     (TEXT("HARDWARE\\DEVICEMAP\\TOUCH"))
#define RV_CALIBRATION_DATA             (TEXT("CalibrationData"))

#define HSD100_CalibrationData          (TEXT("2217,1650 942,693 946,2616 3496,2622 3494,702"))
#define EXC7200_CalibrationData         (TEXT("2050,2103 870,880 870,3321 3233,3346 3261,906"))

#define TSC_ID_ADDRESS    0x0

#if (BSP_TOUCH_HSD100_LVDS1>0)
    #define BSP_TSC_I2C_DEVICE       I2C2_FID
    //
    #define BSP_TSC_PENIRQ_GPIO_PORT DDK_GPIO_PORT6
    #define BSP_TSC_PENIRQ_GPIO_PIN  8
    #define BSP_TSC_PENIRQ_IOMUX_PIN DDK_IOMUX_PIN_NANDF_ALE
    #define BSP_TSC_PENIRQ_IOMUX_PAD DDK_IOMUX_PAD_NANDF_ALE
    #define BSP_TSC_PEN_IRQ          IRQ_GPIO6_PIN8
    #define BSP_TSC_PEN_IOMUX_ALT    DDK_IOMUX_PIN_MUXMODE_ALT5
#endif

#if (BSP_TOUCH_HSD100_LVDS1<=0)
    #define BSP_TSC_I2C_DEVICE       I2C3_FID
    //
    #define BSP_TSC_PENIRQ_GPIO_PORT DDK_GPIO_PORT6
    #define BSP_TSC_PENIRQ_GPIO_PIN  7
    #define BSP_TSC_PENIRQ_IOMUX_PIN DDK_IOMUX_PIN_NANDF_CLE
    #define BSP_TSC_PENIRQ_IOMUX_PAD DDK_IOMUX_PAD_NANDF_CLE
    #define BSP_TSC_PEN_IRQ          IRQ_GPIO6_PIN7
    #define BSP_TSC_PEN_IOMUX_ALT    DDK_IOMUX_PIN_MUXMODE_ALT5
#endif

//-----------------------------------------------------------------------------
// Global Variables

//-----------------------------------------------------------------------------
// Local Variables

static TCHAR *gEventNamePri = TEXT("EVENT_TS_PRI");
static TCHAR *gEventNameAlt = TEXT("EVENT_TS_ALT");
// Named event for interrupt registration
static TCHAR *gTouchEventName;
// The flag indicates the status of the PMIC interrupt
static BOOL gPMICInterruptFlag = FALSE;
static BOOL PowerFlag=FALSE;
static BOOL HSD_PowerFlag = FALSE;

static DWORD gPenIrq, gPenSysIntr = (DWORD)SYSINTR_UNDEFINED;
static HANDLE gPenIntrEvent;


void EnableTouchPower(BOOL enable);

void BSPTouchTypeIdentify();

//------------------------------------------------------------------------------
//
// Function: BSPSetRegisterData
//
// Initializes the touch CalibrationData Register  data for different touch panel
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------

void  BSPSetRegisterData()
{
    HKEY hKey;
 
    DEBUGMSG(ZONE_FUNCTION, (TEXT("CalibrationThread+\r\n")));

    // try to open [HKLM\hardware\devicemap\touch] key
    if (ERROR_SUCCESS != RegOpenKeyEx(HKEY_LOCAL_MACHINE, RK_HARDWARE_DEVICEMAP_TOUCH, 0, KEY_ALL_ACCESS, &hKey))
    {
        RETAILMSG(1, (TEXT("CalibrationThread: calibration: Can't find [HKLM/%s]\r\n"), RK_HARDWARE_DEVICEMAP_TOUCH));
        return ;
    }

    // check for calibration data (query the type of data only)
    if(g_id==TOUCH_HSD100)
        RegSetValueEx(hKey, RV_CALIBRATION_DATA, 0, REG_SZ, (LPBYTE)HSD100_CalibrationData, sizeof(HSD100_CalibrationData));
    else
        RegSetValueEx(hKey, RV_CALIBRATION_DATA, 0, REG_SZ, (LPBYTE)EXC7200_CalibrationData, sizeof(EXC7200_CalibrationData));
    RegCloseKey(hKey);

    DEBUGMSG(ZONE_FUNCTION, (TEXT("CalibrationThread-\r\n")));

    return ;
}


//------------------------------------------------------------------------------
//
// Function: TCH I2C Init
//
// Initializes the TCH 
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void TOUCHI2cInit()
{
    DWORD dwFrequency;

    dwFrequency = TCH_I2C_SPEED;

    g_hI2C = I2COpenHandle(BSP_TSC_I2C_DEVICE);

    if (g_hI2C == INVALID_HANDLE_VALUE)
    {
        DEBUGMSG(ZONE_ERROR, (TEXT("%s: CreateFile for I2C failed!\r\n"), __WFUNCTION__));
    }
    
    if (!I2CSetMasterMode(g_hI2C))
    {
        DEBUGMSG(ZONE_ERROR, (TEXT("%s: Set I2C Master failed!\r\n"), __WFUNCTION__));
    }
    // Initialize the device internal fields
    if (!I2CSetFrequency(g_hI2C, dwFrequency))
    {
        DEBUGMSG(ZONE_ERROR, (TEXT("%s: Set I2C frequency failed!\r\n"), __WFUNCTION__));
    }
        
  }

//------------------------------------------------------------------------------
//
// Function: TCH I2C DeInit
//
// DeInitializes the TCH me.
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
void TOUCHI2cDeInit()
{        
    if (g_hI2C)
    {
        if (!I2CCloseHandle(g_hI2C))
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("%s: I2CCloseHandle!\r\n"), __WFUNCTION__));
        }
        g_hI2C = INVALID_HANDLE_VALUE;
    }
}

//-----------------------------------------------------------------------------
//
// Function: I2CReadTenBytes
//
// This function read a single byte data from the register stated in byReg.
//
// Parameters:
//      hI2C
//          [in] File handle to I2C Bus Interface.
//
//      byAddr
//          [in] I2C Slave device address.
//
//      byReg
//          [in] Register Index.
//
//      lpiResult
//          [in] Pointer of the result. The I2C Bus will store the
//            result of the operation in location pointed to by
//            lpiResult.
//
// Returns:
//      The single byte content stored in byReg.
//
//-----------------------------------------------------------------------------
BYTE HSD100I2CReadTenBytes(HANDLE hI2C, BYTE byAddr, BYTE byReg,BYTE *byOutData, BYTE OutDatabytes,LPINT lpiResult)
{
    I2C_TRANSFER_BLOCK I2CXferBlock;
    I2C_PACKET I2CPacket[2];
    BYTE byInData;
    INT Res1, Res2;

    byInData = byReg;
 
    I2CPacket[0].pbyBuf = (PBYTE) &byInData;
    I2CPacket[0].wLen = sizeof(byInData);
 
    I2CPacket[0].byRW = I2C_RW_WRITE;   
    I2CPacket[0].byAddr = byAddr;
    I2CPacket[0].lpiResult = &Res1;
 
    I2CPacket[1].pbyBuf = byOutData;
    I2CPacket[1].wLen = OutDatabytes;
 
    I2CPacket[1].byRW = I2C_RW_READ;    

    I2CPacket[1].byAddr = byAddr;
    I2CPacket[1].lpiResult = &Res2;
 
    I2CXferBlock.pI2CPackets = I2CPacket;
    I2CXferBlock.iNumPackets = 2;


    if(I2CTransfer(hI2C, &I2CXferBlock))
    {
        if(Res1 != I2C_NO_ERROR)    
            *lpiResult = Res1;
        else
            *lpiResult = Res2;
        return 1;
    }
    else 
    {
        if(Res1 != I2C_NO_ERROR)    
            *lpiResult = Res1;
        else
            *lpiResult = Res2;
        return 0;
    }
}

//-----------------------------------------------------------------------------
//
// Function: GetRegister
//
// This function retrives a TCH register.
//
// Parameters:
//      addr
//          [in] index of the register.
//      content
//          [out] the content of the register.
//
// Returns:
//      None.
//
//-----------------------------------------------------------------------------
VOID
HSD100GetRegister(BYTE byReg,BYTE *byOutData, BYTE OutDatabytes)
{
    INT iResult;
    HSD100I2CReadTenBytes(g_hI2C,TCH_I2C_HSD100_ADDRESS, byReg,byOutData,OutDatabytes,  &iResult) ;

    if(iResult!=I2C_NO_ERROR)
    {
        memset(byOutData,0x0,OutDatabytes);
        ERRORMSG(TRUE, 
                 (TEXT("%s: I2CReadTenBytes failed! Error:%d \r\n"), __WFUNCTION__,iResult));
    } 
    
}
    
//-----------------------------------------------------------------------------
//
// Function: EXC7200I2CReadTenBytes
//
// This function read a ten bytes data from the register stated in byReg.
//
// Parameters:
//      hI2C
//          [in] File handle to I2C Bus Interface.
//
//      byAddr
//          [in] I2C Slave device address.
//
//      byReg
//          [in] Register Index.
//
//      lpiResult
//          [in] Pointer of the result. The I2C Bus will store the
//            result of the operation in location pointed to by
//            lpiResult.
//
// Returns:
//      The single byte content stored in byReg.
//
//-----------------------------------------------------------------------------
BYTE EXC7200I2CReadTenBytes(HANDLE hI2C, BYTE byAddr, BYTE byOutData[10], LPINT lpiResult)
{
    I2C_TRANSFER_BLOCK I2CXferBlock;
    I2C_PACKET I2CPacket;
    INT iResult;

    I2CPacket.pbyBuf = (PBYTE)byOutData;
    I2CPacket.wLen =10;

    I2CPacket.byRW = I2C_RW_READ;
    I2CPacket.byAddr = byAddr;
    I2CPacket.lpiResult = &iResult;

    I2CXferBlock.pI2CPackets = &I2CPacket;
    I2CXferBlock.iNumPackets = 1;

    if(I2CTransfer(hI2C, &I2CXferBlock))
    {
        *lpiResult = iResult;
        return 1;
    }
    else 
    {
        *lpiResult = iResult;
        return 0;
    }
}

//-----------------------------------------------------------------------------
//
// Function: EXC7200I2CWriteTenBytes
//
// This function write a ten bytes data to the register stated in byReg.
//
// Parameters:
//      hI2C
//          [in] File handle to I2C Bus Interface.
//
//      byAddr
//          [in] I2C Slave device address.
//
//      byReg
//          [in] Register Index.
//
//      lpiResult
//          [in] Pointer of the result. The I2C Bus will store the
//            result of the operation in location pointed to by
//            lpiResult.
//
// Returns:
//      The single byte content stored in byReg.
//
//-----------------------------------------------------------------------------
BYTE EXC7200I2CWriteTenBytes(HANDLE hI2C, BYTE byAddr, BYTE byOutData[10], LPINT lpiResult)
{
    I2C_TRANSFER_BLOCK I2CXferBlock;
    I2C_PACKET I2CPacket;
    INT iResult;

    I2CPacket.pbyBuf = (PBYTE)byOutData;
    I2CPacket.wLen =10;

    I2CPacket.byRW = I2C_RW_WRITE;
    I2CPacket.byAddr = byAddr;
    I2CPacket.lpiResult = &iResult;

    I2CXferBlock.pI2CPackets = &I2CPacket;
    I2CXferBlock.iNumPackets = 1;

    if(I2CTransfer(hI2C, &I2CXferBlock))
    {
        *lpiResult = iResult;
        return 1;
    }
    else 
    {
        *lpiResult = iResult;
        return 0;
    }
}

//-----------------------------------------------------------------------------
//
// Function: EXC7200GetRegister
//
// This function retrives a EXC7200 TCH register.
//
// Parameters:
//      addr
//          [in] index of the register.
//      content
//          [out] the content of the register.
//
// Returns:
//      None.
//
//-----------------------------------------------------------------------------
VOID
EXC7200GetRegister(BYTE content[10])
{
    INT iResult;
    EXC7200I2CReadTenBytes(g_hI2C,TCH_I2C_EXC7200_ADDRESS, content,  &iResult) ;
    
    if(iResult!=I2C_NO_ERROR)
    {
        memset(content,0x0,sizeof(content[10]));
        ERRORMSG(TRUE, 
            (TEXT("%s: EXC7200I2CReadTenBytes failed! Error:%d \r\n"), __WFUNCTION__,iResult));
    } 
    
}

//-----------------------------------------------------------------------------
//
// Function: EXC7200SetRegister
//
// This function Set a EXC7200 TCH register.
//
// Parameters:
//      addr
//          [in] index of the register.
//      content
//          [out] the content of the register.
//
// Returns:
//      None.
//
//----------------------------------------------------------------------------
VOID
EXC7200SetRegister(BYTE content[10])
{
    INT iResult;
    EXC7200I2CWriteTenBytes(g_hI2C,TCH_I2C_EXC7200_ADDRESS, content,  &iResult) ;
    
    if(iResult!=I2C_NO_ERROR)
    {
        ERRORMSG(TRUE, 
            (TEXT("%s: EXC7200I2CWriteTenBytes failed! Error:%d \r\n"), __WFUNCTION__,iResult));
    } 
    
}

//-----------------------------------------------------------------------------
//
// Function: BSPTouchHwInit
//
// The function performs BSP-specific Init fimctopm.
//
// Parameters:
//      None.
//
// Returns:
//      None
//
//-----------------------------------------------------------------------------

void BSPTouchHwInit()
{
    //First We try to reume the panel
	
	EnableTouchPower(TRUE); 
    TOUCHI2cInit();
    
    BSPTouchTypeIdentify();
    
}

//-----------------------------------------------------------------------------
//
// Function: BSPTouchAttach
//
// The function performs BSP-specific attachment allocations and assignments.
//
// Parameters:
//      None.
//
// Returns:
//      Handle to an event that will be used by the touch MDD for interrupt
//      registration.
//
//-----------------------------------------------------------------------------
HANDLE BSPTouchAttach(void)
{
    gTouchEventName = gEventNamePri;
    gPenIntrEvent=CreateEvent(NULL, FALSE, FALSE, gTouchEventName);
    return gPenIntrEvent;
}

//-----------------------------------------------------------------------------
//
// Function: BSPTouchInterruptDisable
//
// Disables the interrupt by deregistering with the PMIC.
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//-----------------------------------------------------------------------------
void BSPTouchInterruptDisable(void)
{
    DEBUGMSG(ZONE_FUNCTION, (TEXT("-%s()\r\n"), __WFUNCTION__));

    // Clear the global status flag to indicate PMIC interrupt is not registered.
    gPMICInterruptFlag = FALSE;

    if (gPenSysIntr != SYSINTR_UNDEFINED)
    {
        InterruptDone( gPenSysIntr );
        InterruptDisable(gPenSysIntr );
        KernelIoControl(IOCTL_HAL_RELEASE_SYSINTR, &gPenSysIntr,
                        sizeof(gPenSysIntr), NULL, 0, NULL);
        gPenSysIntr = (DWORD)SYSINTR_UNDEFINED;
    }

    if (gPenIntrEvent != NULL)
    {
        CloseHandle(gPenIntrEvent);
        gPenIntrEvent = NULL;
    }

    DEBUGMSG(ZONE_FUNCTION, (TEXT("-%s()\r\n"), __WFUNCTION__));
}

//-----------------------------------------------------------------------------
//
// Function: BSPTouchInterruptEnable
//
// Initializes interrupt by registering a named event with the PMIC.
//
// Parameters:
//      None.
//
// Returns:
//      TRUE  indicates success.
//      FALSE indicates failure.
//
//-----------------------------------------------------------------------------
BOOL BSPTouchInterruptEnable(void)
{
    BOOL rc=FALSE;
    
    DEBUGMSG(ZONE_FUNCTION, (TEXT("+%s()\r\n"), __WFUNCTION__));

	DDKGpioSetConfig(BSP_TSC_PENIRQ_GPIO_PORT, BSP_TSC_PENIRQ_GPIO_PIN, DDK_GPIO_DIR_IN, DDK_GPIO_INTR_LOW_LEV);//DDK_GPIO_INTR_LOW_LEV);
    DDKGpioClearIntrPin(BSP_TSC_PENIRQ_GPIO_PORT, BSP_TSC_PENIRQ_GPIO_PIN);

    //   DDKIomuxSetPinMux(BSP_TSC_ENABLE_IOMUX_PIN, BSP_TSC_ENABLE_IOMUX_ALT, DDK_IOMUX_PIN_SION_REGULAR);
    //   DDKGpioSetConfig(BSP_TSC_ENABLE_GPIO_PORT, BSP_TSC_ENABLE_GPIO_PIN, DDK_GPIO_DIR_OUT, DDK_GPIO_INTR_NONE);
    //    DDKGpioWriteDataPin(BSP_TSC_ENABLE_GPIO_PORT, BSP_TSC_ENABLE_GPIO_PIN, 0);


    gPenIrq= BSP_TSC_PEN_IRQ;

    if (!KernelIoControl(IOCTL_HAL_REQUEST_SYSINTR, &gPenIrq, sizeof(DWORD),
            &gPenSysIntr, sizeof(DWORD), NULL)){
            ERRORMSG(TRUE, (TEXT("PmInitPowerPolicy: fail to map irq into sys intr\r\n")));
            goto cleanUp;
    }

    // register PENIRQ sys intr
    if (!InterruptInitialize(gPenSysIntr, gPenIntrEvent, NULL, 0)) {
        ERRORMSG(TRUE, (TEXT("%s():  fail to register sys intr\r\n"),  __WFUNCTION__));
        goto cleanUp;
    }
    
    rc=TRUE;

cleanUp:

    DEBUGMSG(ZONE_FUNCTION, (TEXT("-%s()\r\n"), __WFUNCTION__));

    return rc;

}

//-----------------------------------------------------------------------------
//
// Function: BSPTouchInterruptDone
//
// DESCRIPTION:
//      Interrupt done for touch pen down.
//
// Parameters:
//
// Returns:
//      None.
//
//-----------------------------------------------------------------------------
void BSPTouchInterruptDone(void)
{
    DDKGpioClearIntrPin(BSP_TSC_PENIRQ_GPIO_PORT, BSP_TSC_PENIRQ_GPIO_PIN);

    //TODO Make sure pen down interrupt is unmasked
    InterruptDone( gPenSysIntr );
}

//-----------------------------------------------------------------------------
//
// Function: BSPHSD100GetSample
//
// This function is used to get touch screen samples from the ADS7843
// touch screen controller.
//
// Parameters:
//      x
//          [out] Points to x sample coordinate.
//
//      y
//          [out] Points to y sample coordinate.
//
// Returns:
//      Current touch panel status as TOUCH_PANEL_SAMPLE_FLAGS type.
//
//-----------------------------------------------------------------------------
TOUCH_PANEL_SAMPLE_FLAGS BSPHSD100GetSample(INT *x, INT *y,INT *ID)
{
    BYTE content[9];
    TOUCH_PANEL_SAMPLE_FLAGS StateFlags = TouchSampleDownFlag;
    static INT prex=-1,prey=-1;


    if(HSD_PowerFlag)
    {
        StateFlags |= TouchSampleIgnore;
        DEBUGMSG(ZONE_FUNCTION, (TEXT("-%s() TouchSampleIgnore\r\n"), __WFUNCTION__));
        goto cleanUp;
    }
    
    HSD100GetRegister(0x10,content,9);
    
    DEBUGMSG(ZONE_FUNCTION, (TEXT("-%s() :%x %x %x %x  %x  %x %x %x  %x %x\r\n"), __WFUNCTION__,content[0],content[1],content[2],content[3],content[4],content[5],content[6],content[7],content[8],content[8]));

    if((content[0])==0x0)
    {
        StateFlags = TouchSampleIgnore;
        DEBUGMSG(ZONE_FUNCTION, (TEXT("-%s() TouchSampleIgnore\r\n"), __WFUNCTION__));
        prex=-1;
        prey=-1;
        goto cleanUp;
    }
    else if(content[0]==0x3)
    {
        x[0] = (content[1]+content[2]*0xff);
        y[0] =(content[3]+content[4]*0xff);
        x[1] = (content[5]+content[6]*0xff);
        y[1]=(content[7]+content[8]*0xff);
        *ID=0x1;
       
        DEBUGMSG(ZONE_FUNCTION, (TEXT(" TOUCH 2 X:%d  Y:%d   X:%d  Y:%d \r\n"), (x[0]),(y[0]) ,(x[1]),(y[1])));
    }
    else if(content[0]==0x1)
    {    
        *ID=0x0;
        if((prex==-1)&&(prey==-1))
        {
            prex=(content[1]+content[2]*0xff);
            prey=(content[3]+content[4]*0xff);    
            StateFlags = TouchSampleIgnore;
            DEBUGMSG(ZONE_FUNCTION, (TEXT("-%s() TouchSampleIgnore\r\n"), __WFUNCTION__));
            goto cleanUp;
        }
        *x=prex;
        *y=prey;
        prex=(content[1]+content[2]*0xff);
        prey=(content[3]+content[4]*0xff);    
        DEBUGMSG(ZONE_FUNCTION, (TEXT(" One TOUCH 1 X:%d  Y:%d   \r\n"), (*x),(*y)));        
    }
    else
    {
        StateFlags |= TouchSampleIgnore;
        DEBUGMSG(ZONE_FUNCTION, (TEXT("-%s() TouchSampleIgnore\r\n"), __WFUNCTION__));
        goto cleanUp;
    }

     // Tell upper layers that this attempt was valid
    StateFlags |= TouchSampleValidFlag;
         
cleanUp:

    // If we need to look for next pen down event
    if (!(StateFlags & TouchSampleDownFlag))
    {
        // Make sure pen down interrupt is unmasked
        BSPTouchInterruptDone();
    }

    DEBUGMSG(ZONE_FUNCTION, (TEXT("-%s()\r\n"), __WFUNCTION__));
    
return StateFlags;
}

//-----------------------------------------------------------------------------
//
// Function: BSPTouchGetSample
//
// This function is used to get touch screen samples from the ADS7843
// touch screen controller.
//
// Parameters:
//      x
//          [out] Points to x sample coordinate.
//
//      y
//          [out] Points to y sample coordinate.
//
// Returns:
//      Current touch panel status as TOUCH_PANEL_SAMPLE_FLAGS type.
//
//-----------------------------------------------------------------------------
TOUCH_PANEL_SAMPLE_FLAGS BSPEXC7200GetSample(INT *x, INT *y,INT *ID)
{
    BYTE content[10];
    static INT prex=-1,prey=-1;
    static BOOL MutiFlag=FALSE;

    TOUCH_PANEL_SAMPLE_FLAGS StateFlags = TouchSampleDownFlag;


    if(PowerFlag)
    {
        StateFlags |= TouchSampleIgnore;
        DEBUGMSG(ZONE_FUNCTION, (TEXT("-%s() TouchSampleIgnore\r\n"), __WFUNCTION__));
        goto cleanUp;
    }
    
    EXC7200GetRegister(content);
    
    DEBUGMSG(ZONE_FUNCTION, (TEXT("-%s() :%x %x %x %x  %x             %x  %x %x  %x %x\r\n"), __WFUNCTION__,content[0],content[1],content[2],content[3],content[4],content[5],content[6],content[7],content[8],content[9]));

    if(content[0]==0x0)
    {
        StateFlags |= TouchSampleIgnore;
        DEBUGMSG(ZONE_FUNCTION, (TEXT("-%s() TouchSampleIgnore\r\n"), __WFUNCTION__));
        goto cleanUp;
    }
    
    if((content[1]&0x1)==0x0)
    {
        StateFlags = TouchSampleIgnore;
  
        DEBUGMSG(ZONE_FUNCTION, (TEXT("-%s() TouchSampleIgnore UP\r\n"), __WFUNCTION__));
        goto cleanUp;
    }

    
    if(content[0]==TSC_MULTI_TOUCH)
    {
 
     if((content[1]&0x80)==0x80)
    {
        *x = (content[2]+content[3]*0xff)/8;
        *y =(content[4]+content[5]*0xff)/8;
        *ID=(content[1]&0x7C);

        if(*ID==0x0)
        {
            prex=*x;
            prey=*y;

            if( MutiFlag==TRUE)
            {
                StateFlags |= TouchSampleIgnore;
                MutiFlag=FALSE;
                goto cleanUp;
            }
        
            DEBUGMSG(ZONE_FUNCTION, (TEXT(" TOUCH 1 X:%d  Y:%d   X:%d  Y:%d \r\n"), (x[0]),(y[0]) ,(x[1]),(y[1])));
        }
        else if(*ID==0x4)
        {
            x[1] = prex;
            y[1] = prey;
            *ID=0x1;                   
            MutiFlag=TRUE;
            DEBUGMSG(ZONE_FUNCTION, (TEXT(" TOUCH 2 X:%d  Y:%d   X:%d  Y:%d \r\n"), (x[0]),(y[0]) ,(x[1]),(y[1])));
        }
        
    }
    }
    else
    {
        StateFlags |= TouchSampleIgnore;
        DEBUGMSG(ZONE_FUNCTION, (TEXT("-%s() TouchSampleIgnore\r\n"), __WFUNCTION__));
        goto cleanUp;
    }

     // Tell upper layers that this attempt was valid
    StateFlags |= TouchSampleValidFlag;
         
cleanUp:

    // If we need to look for next pen down event
    if (!(StateFlags & TouchSampleDownFlag))
    {       
        // Make sure pen down interrupt is unmasked        
        BSPTouchInterruptDone();    
    }
       
    DEBUGMSG(ZONE_FUNCTION, (TEXT("-%s()\r\n"), __WFUNCTION__));
    
return StateFlags;
}

//-----------------------------------------------------------------------------
//
// Function: BSPEXC7200PowerHandler
//
// DESCRIPTION:
//      This function controls the touchpanel driver state when a power state
//      transition occurs.
//
// Parameters:
//      boff - TRUE  : power down
//           - FALSE : power up
//
// Returns:
//      None.
//
//-----------------------------------------------------------------------------
void BSPEXC7200PowerHandler(BOOL boff)
{
    DEBUGMSG(ZONE_FUNCTION, (TEXT("+%s()\r\n"), __WFUNCTION__));   
    if(boff)
    {
        PowerFlag=TRUE;
        EXC7200SetRegister(Sleep_Common);      
    }
    else
    {
        DDKGpioSetConfig(BSP_TSC_PENIRQ_GPIO_PORT, BSP_TSC_PENIRQ_GPIO_PIN, DDK_GPIO_DIR_OUT, DDK_GPIO_INTR_NONE);
        DDKGpioWriteDataPin(BSP_TSC_PENIRQ_GPIO_PORT, BSP_TSC_PENIRQ_GPIO_PIN, 0);
        StallExecution(1000);
        DDKGpioWriteDataPin(BSP_TSC_PENIRQ_GPIO_PORT, BSP_TSC_PENIRQ_GPIO_PIN, 1);
        DDKGpioSetConfig(BSP_TSC_PENIRQ_GPIO_PORT, BSP_TSC_PENIRQ_GPIO_PIN, DDK_GPIO_DIR_IN, DDK_GPIO_INTR_LOW_LEV);
        DDKGpioClearIntrPin(BSP_TSC_PENIRQ_GPIO_PORT, BSP_TSC_PENIRQ_GPIO_PIN); 
        PowerFlag=FALSE;
    }   
    DEBUGMSG(ZONE_FUNCTION, (TEXT("-%s()\r\n"), __WFUNCTION__));
}

//-----------------------------------------------------------------------------
//
// Function: BSPHSD100PowerHandler
//
// DESCRIPTION:
//      This function controls the touchpanel driver state when a power state
//      transition occurs.
//
// Parameters:
//      boff - TRUE  : power down
//           - FALSE : power up
//
// Returns:
//      None.
//
//-----------------------------------------------------------------------------
void BSPHSD100PowerHandler(BOOL boff)
{
    DEBUGMSG(ZONE_FUNCTION, (TEXT("+%s()\r\n"), __WFUNCTION__));
	if(boff)
    {
        HSD_PowerFlag=TRUE;    
    }
    else
    {
        HSD_PowerFlag=FALSE;
    }

    DEBUGMSG(ZONE_FUNCTION, (TEXT("-%s()\r\n"), __WFUNCTION__));
}

//-----------------------------------------------------------------------------
//
// Function: BSPTouchGetSample
//
// This function is used to get touch screen samples from the ADS7843
// touch screen controller.
//
// Parameters:
//      x
//          [out] Points to x sample coordinate.
//
//      y
//          [out] Points to y sample coordinate.
//
// Returns:
//      Current touch panel status as TOUCH_PANEL_SAMPLE_FLAGS type.
//
//-----------------------------------------------------------------------------
TOUCH_PANEL_SAMPLE_FLAGS BSPTouchGetSample(INT *x, INT *y,INT *ID)
{
      if(g_id==TOUCH_EXC7200)
        return BSPEXC7200GetSample(x, y,ID);
     else if(g_id==TOUCH_HSD100)
        return BSPHSD100GetSample(x, y,ID);
     else 
        return TouchSampleIgnore;
}
//-----------------------------------------------------------------------------
//
// Function: BSPTouchTypeIdentify
//
// DESCRIPTION:
//      recognize the type of touch panel, HSD100 or EXC7200.
//
// Parameters:
//
// Returns:
//      None.
//
//-----------------------------------------------------------------------------

void BSPTouchTypeIdentify()
{
    INT iResult;
    BYTE content[10];
    if(g_hI2C)
    {  
        HSD100I2CReadTenBytes(g_hI2C,TCH_I2C_HSD100_ADDRESS, 0x40,content,0x3,  &iResult) ;
        RETAILMSG(0,(TEXT("HSD100Read:Result(%d),(%d),(%d),(%d)\r\n"),iResult,content[0],content[1],content[2]));
        if((iResult==I2C_NO_ERROR)&&(content[0]==2))
            {
            g_id=TOUCH_HSD100;
            }
        else
            {
            EXC7200I2CWriteTenBytes(g_hI2C,TCH_I2C_EXC7200_ADDRESS, Version_Common,  &iResult) ;
            EXC7200GetRegister(content);
            if(iResult != I2C_NO_ERROR)
                {
                g_id=TOUCH_NULL;
                ERRORMSG(TRUE, 
                        (TEXT("%s: Can not get any touch panel info.\r\n"), __WFUNCTION__));
                }
            else
                g_id=TOUCH_EXC7200;
        }
        
        BSPSetRegisterData();
        }
        RETAILMSG(0,(TEXT("TOUCH GID(%d)\r\n"),g_id));
}
//-----------------------------------------------------------------------------
//
// Function: BSPTouchPowerHandler
//
// DESCRIPTION:
//      This function controls the touchpanel driver state when a power state
//      transition occurs.
//
// Parameters:
//      boff - TRUE  : power down
//           - FALSE : power up
//
// Returns:
//      None.
//
//-----------------------------------------------------------------------------
void BSPTouchPowerHandler(BOOL boff)
{
    DEBUGMSG(ZONE_FUNCTION, (TEXT("+%s()\r\n"), __WFUNCTION__));
    
    if(g_id==TOUCH_EXC7200)
        BSPEXC7200PowerHandler(boff);
    else if(g_id==TOUCH_HSD100)
         BSPHSD100PowerHandler(boff);
    
    DEBUGMSG(ZONE_FUNCTION, (TEXT("-%s()\r\n"), __WFUNCTION__));
}
//-----------------------------------------------------------------------------
//
// Function: EnableTouchPower
//
// DESCRIPTION:
//      Enable lvds0 power.
//
// Parameters:
//      enable - TRUE  : power up
//           - FALSE : power down
//
// Returns:
//      None.
//
//-----------------------------------------------------------------------------

void EnableTouchPower(BOOL enable)
{
}


