/*---------------------------------------------------------------------------
* Copyright (C) 2005-2011, Freescale Semiconductor, Inc. All Rights Reserved.
* THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
* AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT 
*--------------------------------------------------------------------------*/


/*
 *  File:     hwinit.c
 *  Purpose:  Platform-specific host driver initialisation and configuration
 *            Pin muxing
 *            CPLD configuration
 *            transceiver, wakeup, Vbus configuration
 *
 *  Functions:  BSPUsbhCheckConfigPower  - verify power available for devices
 *              BSPUsbSetWakeUp          - setup wakeup sources for host port
 *              InitializeTransceiver    - any setup required for attached transceiver   
 *              SetPHYPowerMgmt          - suspend or resume the transceiver
 */

#pragma warning(push)
#pragma warning(disable: 4115 4201 4204 4214)
#include <windows.h>
#include <Winbase.h>
#include <ceddk.h>
#pragma warning(pop)

#include "bsp.h"
#include "mx6_usb.h"
#include "common_usbname.h"
#include "common_usbcommon.h"
#include <..\USBD\OS\oscheckkitl.c>
#include "regsusbphy.h"

#define UnusedParameter(x)  x = x
#define HIGH_SPEED      1
#define FULL_SPEED      2
#define OTG_DEBUG_MSG	0

PCSP_USB_REGS gRegs; 
WORD gSel;

volatile UINT32 *gQueueHead;
#define QUEUE_HEAD_SIZE 0x200

// Really needs this
TCHAR gszOTGGroup[30];
DWORD gdwOTGSupport;
BOOL BSPUsbCheckWakeUp(void);
extern void BSPUsbPhyStartUp(void);
extern void BSPUsbSetBusConfig(PUCHAR baseMem);
extern BOOL Initialize3317(CSP_USB_REGS * regs, WORD sel);
extern void SetULPIHostMode3317(PCSP_USB_REGS pRegs, WORD pSel, BOOL fSuspend);
extern void PostSetULPIHostMode3317(PCSP_USB_REGS pRegs, WORD pSel, BOOL fSuspend);

//-----------------------------------------------------------------------------
//
//  Function: PreSetPHYPowerMgmt
//
//  This function is to set configure before set the transceiver to suspend or resume mode.
//
//  Parameters:
//     N/A
//     
//  Returns:
//     NULL
//
//-----------------------------------------------------------------------------
void PreSetPHYPowerMgmt()
{
    // This function need to be called only when system resume and USB_WAKEUP_NONE is defined
#if defined USB_WAKEUP_NONE
    // config RESETB pin, we must write 0 first then 1.
    // config KEY_COL4(4-14) pad for usboh3 instance USB_H2_PHY_RST_B port 
    DDKIomuxSetPinMux(DDK_IOMUX_PIN_KEY_COL4, DDK_IOMUX_PIN_MUXMODE_ALT1, 
                       DDK_IOMUX_PIN_SION_REGULAR);

    DDKGpioSetConfig(DDK_GPIO_PORT4, 14, DDK_GPIO_DIR_OUT, DDK_GPIO_INTR_NONE);
   
    DDKGpioWriteDataPin(DDK_GPIO_PORT4, 14, 0);

    DDKGpioWriteDataPin(DDK_GPIO_PORT4, 14, 1);    
#endif
}

//-----------------------------------------------------------------------------
//
//  Function: SetPHYPowerMgmt
//
//  This function is to configure the transceiver to suspend or resume mode.
//
//  Parameters:
//     fSuspend - TRUE : Suspend request, FALSE : Resume request
//     blSysSus - TRUE : System Suspend , FALSE : USB Low Power mode
//     
//  Returns:
//     NULL
//
//-----------------------------------------------------------------------------
void SetPHYPowerMgmt(BOOL fSuspend, BOOL blSysSus)
{
    if (gSel == 1)
    {
        //H2 3317
        if(!fSuspend && blSysSus)
        {
            // call only system resume
            PreSetPHYPowerMgmt();
        }

        SetULPIHostMode3317(gRegs, gSel, fSuspend);
        
        if(blSysSus)
        {
            PostSetULPIHostMode3317(gRegs, gSel, fSuspend);
        }
    }
    return;
}

//------------------------------------------------------------------------------
// Function: InitializeOTGMux
//
// Description: This function is to configure the IOMUX for USB OTG Core
//
// Parameters:
//     NULL
//     
// Returns:
//     NULL
//
//------------------------------------------------------------------------------
static void InitializeOTGMux()
{
	RETAILMSG(OTG_DEBUG_MSG,(L"+InitializeOTGMux\r\n"));
	// TC : IO MUX IOCTL removed, because the configuration is made in the bootloader.
    DDKGpioSetConfig(DDK_GPIO_PORT3, 22, DDK_GPIO_DIR_OUT, DDK_GPIO_INTR_NONE);
    DDKGpioWriteDataPin(DDK_GPIO_PORT3, 22, 1);

	RETAILMSG(OTG_DEBUG_MSG,(L"-InitializeOTGMux\r\n"));
}

//------------------------------------------------------------------------------
// Function: InitializeHost1Mux
//
// Description: This function is to configure the IOMUX for USB HOST 1 Core
//
// Parameters:
//     NULL
//     
// Returns:
//     NULL
//
//------------------------------------------------------------------------------
static void InitializeHost1Mux()
{
	//  NO IOMUX 
}
  
//------------------------------------------------------------------------------
// Function: InitializeHost2Mux
//
// Description: This function is to configure the IOMUX for USB HOST 2 Core
//
// Parameters:
//     NULL
//     
// Returns:
//     NULL
//
//------------------------------------------------------------------------------
static void InitializeHost2Mux()
{
    //  NO IOMUX
}
    
//------------------------------------------------------------------------------
// Function: InitializeMux
//
// This function is to configure the IOMUX for USB Host Cores
//
// Parameters:
//     sel
//         [IN] Selection of the USB cores (0 - OTG, 1 - HOST2, 2 - HOST1)
//     
// Returns:
//     offset of the USB core register to be configured.
//
//------------------------------------------------------------------------------
static DWORD InitializeMux(int sel)
{
    DWORD off = offset(CSP_USB_REGS, H1) + offset(CSP_USB_REG, CAPLENGTH);
	
	RETAILMSG(OTG_DEBUG_MSG,(L"+InitializeMux\r\n"));
     
    if (sel == 0) {   // OTG
        InitializeOTGMux();
        off = offset(CSP_USB_REG, CAPLENGTH);
    }
    else if(sel == 2)
    {
        InitializeHost1Mux(); // HOST 1
        off = offset(CSP_USB_REGS, H1) + offset(CSP_USB_REG, CAPLENGTH);
    }       
    
	RETAILMSG(OTG_DEBUG_MSG,(L"-InitializeMux\r\n"));
    return off;
}

//------------------------------------------------------------------------------
// Function: SelectUSBCore
//
// Description: This function is to select the corresponding USB core type 
//              according to the  micro USB_HOST_MODE. 
//
// Parameters:
//     sel
//         [OUT] the value returned which indicates the type of the USB core
//              (USB OTG or USB High Speed HOST port 2). sel = 0 means USB OTG 
//               core, sel = 1 means USB High Speed HOST 2 core. sel = 2 means USB
//               HIGH Speed Host 1 core.
//     
// Returns:
//     Speed of the USB core - HIGH_SPEED or FULL_SPEED
//
//------------------------------------------------------------------------------
static int SelectUSBCore(WORD * sel)
{
    int CoreSpeed;

	RETAILMSG(OTG_DEBUG_MSG,((L"+SelectUSBCore sel= [%d]\r\n"),*sel));	
 
#if (USB_HOST_MODE == 0)||(USB_HOST_MODE == 1)
    // OTG port, be sure to turn off the USB function driver.   
    *sel = 0;
#if (USB_HOST_MODE == 0)  // OTG Full Speed - not implemented here
    CoreSpeed = FULL_SPEED;
#else                     // OTG High Speed - Internal transceiver(default)
    CoreSpeed = HIGH_SPEED;
    DEBUGMSG(ZONE_INIT, (L"High Speed USB OTG Host\r\n"));
#endif
#else                     // High Speed USB Host 
#if (USB_HOST_MODE == 3) // High Speed USB Host 1
    *sel = 2;
    CoreSpeed = HIGH_SPEED;
#else
    // High Speed USB Host 2
    *sel = 1;        
    CoreSpeed = HIGH_SPEED;
    
    DEBUGMSG(ZONE_INIT, (L"High Speed USB Host Port 2\r\n"));
#endif    
#endif  

	RETAILMSG(OTG_DEBUG_MSG,((L"-SelectUSBCore speed= [%d]\r\n"),CoreSpeed));	
    return CoreSpeed;
}

//-----------------------------------------------------------------------------
//
//  Function: DumpDeviceState
//
//  This function is to dump the device status
//
//  Parameters:
//     state - pointer to PORTSC register
//     
//  Returns:
//     NULL
//
//-----------------------------------------------------------------------------
#ifdef DEBUG
static void DumpDeviceState( USB_PORTSC_T * state)  
{ 
    if (state->CCS)
        DEBUGMSG(1, (L"\t\tCurrent Connect Status: Attached\r\n"));
    if (state->CSC)
        DEBUGMSG (1, (L"\t\tConnect Status Change: Changed\r\n"));
    if (state->PE)
        DEBUGMSG (1, (L"\t\tPort Enabled\r\n"));
    if (state->PEC)
        DEBUGMSG (1, (L"\t\tPort Enable/Disable Change\r\n"));
    if (state->OCA)
        DEBUGMSG (1, (L"\t\tOver-current Active\r\n"));
    if (state->OCC)
        DEBUGMSG (1, (L"\t\tOver-current Change\r\n"));
    if (state->FPR)
        DEBUGMSG (1, (L"\t\tForce Port Resume\r\n"));
    if (state->SUSP)
        DEBUGMSG (1, (L"\t\tSuspend\r\n"));
    if (state->PR)
        DEBUGMSG (1, (L"\t\tPort Reset\r\n"));
    if (state->HSP)
        DEBUGMSG (1, (L"\t\tHigh-Speed Port \r\n"));
    
    DEBUGMSG (1, (L"\t\tLine Status: %x", state->LS));
    switch (state->LS) 
    {
        case 0:
            DEBUGMSG (1, (L"\t\t\tSE0\r\n"));
            break;
        case 1:
            DEBUGMSG (1, (L"\t\t\tJ-state\r\n"));
            break;
        case 2:
            DEBUGMSG (1, (L"\t\t\tK-state\r\n"));
            break;
        default:
            DEBUGMSG (1, (L"\t\t\tUndefined\r\n"));
            break;
    }

    if (state->PP)
        DEBUGMSG (1, (L"\t\t??? Should be 0 for device\r\n"));
    if (state->PO)
        DEBUGMSG (1, (L"\t\tPort Owner\r\n"));
    if (state->PIC) {
        DEBUGMSG (1, (L"\t\tPort Indicator Control"));
        switch (state->PIC) {
            case 1:
                DEBUGMSG (1, (L"\t\t\tAmber\r\n"));
                break;
            case 2:
                DEBUGMSG (1, (L"\t\t\tGreen\r\n"));
                break;
          
            default:
                DEBUGMSG (1, (L"\t\t\tUndefined\r\n"));
                break;
        }
    }   
    if (state->PTC) 
        DEBUGMSG (1, (L"\t\tPort Test Control: %x\r\n", state->PTC));
        
    if (state->WKCN) 
        DEBUGMSG (1, (L"\t\tWake on Connect Enable (WKCNNT_E)\r\n"));
                        
    if (state->WKDC) 
        DEBUGMSG (1, (L"\t\tWake on Disconnect Enable (WKDSCNNT_E) \r\n"));

    if (state->WKOC) 
        DEBUGMSG (1, (L"\t\tWake on Over-current Enable (WKOC_E) \r\n"));
    
    if (state->PHCD) 
        DEBUGMSG (1, (L"\t\tPHY Low Power Suspend - Clock Disable (PLPSCD) \r\n"));

    if (state->PFSC) 
        DEBUGMSG (1, (L"\t\tPort Force Full Speed Connect \r\n"));

    DEBUGMSG (1, (L"\t\tPort Speed: %x->", state->PSPD));
    switch (state->PSPD) 
    {
        case 0:
            DEBUGMSG (1, (L"\t\t\tFull Speed\r\n"));
            break;
        case 1:
            DEBUGMSG (1, (L"\t\t\tLow Speed\r\n"));
            break;
        case 2:
            DEBUGMSG (1, (L"\t\t\tHigh Speed\r\n"));
            break;
        
        default:
            DEBUGMSG (1, (L"\t\t\tUndefined\r\n"));
            break;
    }
    DEBUGMSG (1, (L"\t\tParallel Transceiver Width:%x->", state->PTW));
    if (state->PTW) 
        DEBUGMSG (1, (L"\t\t\t16 bits\r\n"));
    else
        DEBUGMSG (1, (L"\t\t\t8 bits\r\n"));
    if (state->STS) 
        DEBUGMSG (1, (L"\t\tSerial Transceiver Select \r\n"));
    
    DEBUGMSG (1, (L"\t\tParallel Transceiver Select:%x->", state->PTS));
    switch (state->PTS) {
        case 0:
            DEBUGMSG (1, (L"\t\t\tUTMI/UTMI+\r\n"));
            break;
        case 1:
            DEBUGMSG (1, (L"\t\t\tPhilips Classic\r\n"));
            break;
        case 2:
            DEBUGMSG (1, (L"\t\t\tULPI\r\n"));
            break;
        case 3:
            DEBUGMSG (1, (L"\t\t\tSerial/1.1 PHY (FS Only)\r\n"));
            break;
        default:
            DEBUGMSG (1, (L"\t\t\tUndefined\r\n"));
            break;
    }                   
}

//-----------------------------------------------------------------------------
//
//  Function: DumpUSBRegs
//
//  This function is to dump the USB Register detail
//
//  Parameters:
//     regs - Pointer to 3 USB Core Register
//     sel - 0: H2, 1: H1, 2: OTG
//     
//  Returns:
//     NULL
//
//-----------------------------------------------------------------------------
static void DumpUSBRegs(PCSP_USB_REGS regs, WORD sel)
{    
    CSP_USB_REG *pReg;
    
    if (sel == 0)
        RETAILMSG (1, (L"Dump OTG Regs\r\n"));
    else
        RETAILMSG (1, (L"Dump H2 Regs\r\n"));
    
    if (sel == 0)
        pReg = (PCSP_USB_REG)(&(regs->OTG));
    else
        pReg=(PCSP_USB_REG)(&(regs->H2));

    DEBUGMSG (1,(L"\tID(%xh)=%x\r\n",offset(CSP_USB_REG,ID), INREG32(&pReg->ID)));
    DEBUGMSG (1,(L"\tHWGENERAL(%xh)=%x\r\n",offset(CSP_USB_REG,HWGENERAL),INREG32(&pReg->HWGENERAL)));
    DEBUGMSG (1,(L"\tHWHOST(%xh)=%x\r\n",offset(CSP_USB_REG,HWHOST),INREG32(&pReg->HWHOST)));
    DEBUGMSG (1,(L"\tHWDEVICE(%xh)=%x\r\n",offset(CSP_USB_REG,HWDEVICE),INREG32(&pReg->HWDEVICE)));
    DEBUGMSG (1,(L"\tHWTXBUF(%xh)=%x\r\n",offset(CSP_USB_REG,HWTXBUF),INREG32(&pReg->HWTXBUF)));
    DEBUGMSG (1,(L"\tHWRXBUF(%xh)=%x\r\n",offset(CSP_USB_REG,HWRXBUF),INREG32(&pReg->HWRXBUF)));
    DEBUGMSG (1,(L"\tCAPLENGTH(%xh)=%x\r\n",offset(CSP_USB_REG,CAPLENGTH),INREG8(&pReg->CAPLENGTH)));
    DEBUGMSG (1,(L"\tHCIVERSION(%xh)=%x\r\n",offset(CSP_USB_REG,HCIVERSION),INREG16(&pReg->HCIVERSION)));
    DEBUGMSG (1,(L"\tHCSPARAMS(%xh)=%x\r\n",offset(CSP_USB_REG,HCSPARAMS),INREG32(&pReg->HCSPARAMS)));
    DEBUGMSG (1,(L"\tHCCPARAMS(%xh)=%x\r\n",offset(CSP_USB_REG,HCCPARAMS),INREG32(&pReg->HCCPARAMS)));
    DEBUGMSG (1,(L"\tDCIVERSION(%xh)=%x\r\n",offset(CSP_USB_REG,DCIVERSION),INREG16(&pReg->DCIVERSION)));
    DEBUGMSG (1,(L"\tDCCPARAMS(%xh)=%x\r\n",offset(CSP_USB_REG,DCCPARAMS),INREG32(&pReg->DCCPARAMS)));
    DEBUGMSG (1,(L"\tUSBCMD(%xh)=%x\r\n",offset(CSP_USB_REG,USBCMD),INREG32(&pReg->USBCMD)));
    DEBUGMSG (1,(L"\tUSBSTS(%xh)=%x\r\n",offset(CSP_USB_REG,USBSTS),INREG32(&pReg->USBSTS)));
    DEBUGMSG (1,(L"\tUSBINTR(%xh)=%x\r\n",offset(CSP_USB_REG,USBINTR),INREG32(&pReg->USBINTR)));
    DEBUGMSG (1,(L"\tPORTSC(%xh)[0]=%x\r\n",offset(CSP_USB_REG,PORTSC[0]),INREG32(&pReg->PORTSC[0])));
    {
        USB_PORTSC_T state;
        DWORD * temp=(DWORD*)&state;
        *temp=INREG32(&pReg->PORTSC[0]);
        DumpDeviceState( & state);
    }
    DEBUGMSG (1,(L"\tOTGSC(%xh)=%x\r\n",offset(CSP_USB_REG,OTGSC),INREG32(&pReg->OTGSC)));
    DEBUGMSG (1,(L"\tUSBMODE(%xh)=%x\r\n",offset(CSP_USB_REG,USBMODE),INREG32(&pReg->USBMODE)));
    DEBUGMSG (1,(L"\tULPI_VIEWPORT(%xh)=%x\r\n",offset(CSP_USB_REG,ULPI_VIEWPORT),INREG32(&pReg->ULPI_VIEWPORT)));

    DEBUGMSG (1,(L"\t*********************\r\n"));
    DEBUGMSG (1,(L"\tAddress of ASYNCLISTADDR at 0x%x\r\n", &pReg->T_158H.ASYNCLISTADDR));
    DEBUGMSG (1,(L"\tASYNCLISTADDR(%xh)=%x\r\n",offset(CSP_USB_REG,T_158H),INREG32(&pReg->T_158H.ASYNCLISTADDR)));
    DEBUGMSG (1,(L"\tENDPTSETUPSTAT(%xh)=%x\r\n",offset(CSP_USB_REG,ENDPTSETUPSTAT),INREG32(&pReg->ENDPTSETUPSTAT)));
    DEBUGMSG (1,(L"\tENDPTPRIME(%xh)=%x\r\n",offset(CSP_USB_REG,ENDPTPRIME),INREG32(&pReg->ENDPTPRIME)));
    DEBUGMSG (1,(L"\tENDPTFLUSH(%xh)=%x\r\n",offset(CSP_USB_REG,ENDPTFLUSH),INREG32(&pReg->ENDPTFLUSH)));
    DEBUGMSG (1,(L"\tENDPTSTATUS(%xh)=%x\r\n",offset(CSP_USB_REG,ENDPTSTATUS),INREG32(&pReg->ENDPTSTATUS)));
    DEBUGMSG (1,(L"\tENDPTCOMPLETE(%xh)=%x\r\n",offset(CSP_USB_REG,ENDPTCOMPLETE),INREG32(&pReg->ENDPTCOMPLETE)));
    DEBUGMSG (1,(L"\tENDPTCTRL0(%xh)=%x\r\n",offset(CSP_USB_REG,ENDPTCTRL0),INREG32(&pReg->ENDPTCTRL0)));
    DEBUGMSG (1,(L"\t*********************\r\n")); 
    DEBUGMSG (1,(L"\tOTG_CTRL(%xh)=%x\r\n",offset(CSP_USB_REGS,OTG_CTRL),INREG32(&regs->OTG_CTRL)));
   
}

#endif

//------------------------------------------------------------------------------
//
//  Function: ConfigH1
//
//  This function is to configure the USB H1 Core.
//
//  Parameters:
//     pRegs - Pointer to USB Core(USB OTG and HOST 2 ports) Registers
//     
//  Returns:
//     NULL
//
//------------------------------------------------------------------------------
static void ConfigH1(CSP_USB_REGS *pRegs)
{
    USB_PORTSC_T portsc;
    UH1_CTRL_T   ctrl;
    USB_USBCMD_T cmd;

    DWORD *temp;
            
    // Stop the controller first
    {
        temp = (DWORD *)&cmd;
        *temp = INREG32(&pRegs->H1.USBCMD);
        cmd.RS = 0;
        OUTREG32(&pRegs->H1.USBCMD, *temp);
        while ((INREG32(&pRegs->H1.USBCMD) & 0x1) == 0x1){
             Sleep(100);
        }
    }

    // Do a reset first no matter what
    {
        temp = (DWORD *)&cmd;
        *temp = INREG32(&pRegs->H1.USBCMD);
        cmd.RST = 1;
        OUTREG32(&pRegs->H1.USBCMD, *temp);
        while ((INREG32(&pRegs->H1.USBCMD) & 0x1<<1) == (0x1<<1)){
             Sleep(100);
        }
    }
    
    //set UH1 oc polarity
    {
        UH1_CTRL_T Uh1_Ctrl;
        DWORD * tempPhyCtrl0;
        tempPhyCtrl0 = (DWORD*)&Uh1_Ctrl;
        *tempPhyCtrl0 = INREG32(&pRegs->UH1_CTRL);
        RETAILMSG(0,(L"before ConfigUH1HOST:: regs->UH1_CTRL= %x\r\n",*tempPhyCtrl0));
        Uh1_Ctrl.OVER_CUR_POL = 0;
        Uh1_Ctrl.WIE = 1;
        Uh1_Ctrl.PM = 1;
        Uh1_Ctrl.SUSPENDM=1; 
        OUTREG32(&pRegs->UH1_CTRL, *tempPhyCtrl0);
        *temp = INREG32(&pRegs->UH1_CTRL);
        RETAILMSG(0,(L"after ConfigUH1HOST:: regs->UH1_CTRL= %x\r\n",*temp));
        RETAILMSG(0,(L"after ConfigUH1HOST:: regs->UH1_CTRL= %x\r\n",INREG32(&pRegs->UH1_PHY_CTRL_0)));
    }

    //usb_hs1_UTMI_interface_configure
    temp  = (DWORD *)&portsc;
    *temp = INREG32(&pRegs->H1.PORTSC);
    portsc.PTS = 0x0;
    OUTREG32(&pRegs->H1.PORTSC, *temp);
    RETAILMSG(0,(TEXT("SET portsc.PTS = 0x0\r\n")));
    
    // otg_frindex_setup(1);
    OUTREG32(&pRegs->H1.FRINDEX, 1);

    // otg_set_configflag_on();
    OUTREG32(&pRegs->H1.CONFIGFLAG, 1);

    //usb_h1_interrupt_enable
    temp =  (DWORD *)&ctrl;
    *temp = INREG32(&pRegs->UH1_CTRL);
    ctrl.WIE = 1;
    ctrl.PM = 1;
    OUTREG32(&pRegs->UH1_CTRL, *temp);

   
    // set interrupt interval to 0 for immediate interrupt
    {
        DWORD * tempCmd;
        tempCmd=(DWORD *)&cmd;
        *tempCmd=INREG32(&pRegs->H1.USBCMD);
        cmd.ITC=0;
        OUTREG32(&pRegs->H1.USBCMD, *tempCmd);
    }

	 //reset the controller
    {
        temp = (DWORD *)&cmd;
        *temp = INREG32(&pRegs->H1.USBCMD);
        cmd.RST = 1;
        OUTREG32(&pRegs->H1.USBCMD, *temp);
        while ((INREG32(&pRegs->H1.USBCMD) & 0x1<<1) == (0x1<<1)){
             Sleep(100);
        }
    }

    Sleep(100);

    BSPUsbSetBusConfig((PUCHAR)(&pRegs->H1));

    return;
}


//------------------------------------------------------------------------------
//
//  Function: ConfigH2
//
//  This function is to configure the USB H2 Core.
//
//  Parameters:
//     pRegs - Pointer to 2 USB Core(USB OTG and HOST 2 ports) Registers
//     
//  Returns:
//     NULL
//
//------------------------------------------------------------------------------
static void ConfigH2(CSP_USB_REGS *pRegs)
{
     UNREFERENCED_PARAMETER(pRegs);
    return;
}

//------------------------------------------------------------------------------
// Function: ConfigOTG
//
// Description: This function is to configure the USB OTG Core.
//
// Parameters:
//     pRegs
//         [IN] Pointer to 2 USB Core Registers
//     
// Returns:
//     NULL
//
//------------------------------------------------------------------------------
static void ConfigOTG(CSP_USB_REGS *pRegs, int speed)
{
    USB_PORTSC_T portsc;
    OTG_CTRL_T   ctrl;
    USB_USBMODE_T mode;
    USB_USBCMD_T cmd;   
    DWORD *temp;  

	RETAILMSG(OTG_DEBUG_MSG,(L"+ConfigOTG\r\n"));	

    UNREFERENCED_PARAMETER(speed); 

    // Stop the controller first
    {
        temp = (DWORD *)&cmd;
        *temp = INREG32(&pRegs->OTG.USBCMD);
        cmd.RS = 0;
        OUTREG32(&pRegs->OTG.USBCMD, *temp);
        while ((INREG32(&pRegs->OTG.USBCMD) & 0x1) == 0x1){
             Sleep(100);
        }
    }

    // Do a reset first no matter what
    {
        temp = (DWORD *)&cmd;
        *temp = INREG32(&pRegs->OTG.USBCMD);
        cmd.RST = 1;
        OUTREG32(&pRegs->OTG.USBCMD, *temp);
        while ((INREG32(&pRegs->OTG.USBCMD) & 0x1<<1) == (0x1<<1)){
             Sleep(100);
        }

    }

    //set otg oc polarity
    {
        OTG_CTRL_T Otg_Ctrl;
        DWORD * tempPhyCtrl0;
        tempPhyCtrl0 = (DWORD*)&Otg_Ctrl;
        *tempPhyCtrl0 = INREG32(&pRegs->OTG_CTRL);
        RETAILMSG(0,(L"before ConfigOTGHOST:: regs->OTG_CTRL= %x\r\n",*tempPhyCtrl0));
        Otg_Ctrl.OVER_CUR_POL = 0;
        Otg_Ctrl.WKUP_ID_EN = 1;
        Otg_Ctrl.WKUP_VBUS_EN = 1;
        Otg_Ctrl.SUSPENDM=1; 
        OUTREG32(&pRegs->OTG_CTRL, *tempPhyCtrl0);
        *temp = INREG32(&pRegs->OTG_CTRL);
        RETAILMSG(OTG_DEBUG_MSG,(L"before ConfigOTGHOST:: regs->OTG_CTRL= %x\r\n",*temp));
        RETAILMSG(OTG_DEBUG_MSG,(L"after ConfigOTGHOST:: regs->OTG_CTRL= %x\r\n",INREG32(&pRegs->OTG_PHY_CTRL_0)));
    }

    temp  = (DWORD *)&portsc;
    *temp = INREG32(&pRegs->OTG.PORTSC);
    portsc.PTS = 0x0;     // UTMI/UTMI+ transceiver for Ringo
    OUTREG32(&pRegs->OTG.PORTSC, *temp);

    // usb_otg_interrupt_enable
    temp = (DWORD *)&ctrl;
    *temp = INREG32(&pRegs->OTG_CTRL);
    ctrl.WIE = 1;
    OUTREG32(&pRegs->OTG_CTRL, *temp);

    // usb_otg_power_mask_enable
    temp = (DWORD *)&ctrl;
    *temp = INREG32(&pRegs->OTG_CTRL);
    ctrl.PM = 1;
    OUTREG32(&pRegs->OTG_CTRL, *temp);

     // otg_setmode
    temp = (DWORD *)&cmd;
    *temp = INREG32(&pRegs->OTG.USBCMD);
    cmd.RST = 1;
    OUTREG32(&pRegs->OTG.USBCMD, *temp);

    while (INREG32(&pRegs->OTG.USBCMD)& (0x1 << 1));

     temp = (DWORD *)&mode;
    *temp = INREG32(&pRegs->OTG.USBMODE);
    mode.CM = 0x3;
    OUTREG32(&pRegs->OTG.USBMODE, *temp);

    Sleep(10);
    if ((INREG32(&pRegs->OTG.USBMODE)& 0x3) != 0x3)
    {
        return;
    }

    BSPUsbSetBusConfig((PUCHAR)(&pRegs->OTG));

     // otg_power_on_port1
    if (INREG32(&pRegs->OTG.HCSPARAMS) &(0x1 << 4))
    {
        DWORD mask = (0x1<<1) + (0x1<<3)+(0x1<<5);
        CLRREG32(&pRegs->OTG.PORTSC, mask);
        temp = (DWORD *)&portsc;
        *temp = INREG32(&pRegs->OTG.PORTSC);
        portsc.PP = 1;      
        OUTREG32(&pRegs->OTG.PORTSC, *temp);
    }
    else
        DEBUGMSG (1, (TEXT("Host does not control power\r\n")));

 #ifdef OTG_TEST_MODE 
    //otg_controller_reset
    temp=(DWORD *)&cmd; 
    *temp=0;
    cmd.RST=1;  
    CLRREG32(&pRegs->OTG.USBCMD, *temp);
    *temp = INREG32(&pRegs->OTG.USBCMD);
    cmd.RST=1;
    OUTREG32(&pRegs->OTG.USBCMD, *temp);
    while (INREG32(&pRegs->OTG.USBCMD) & (0x1 << 1));

    temp = (DWORD *)&mode;
    *temp = INREG32(&pRegs->OTG.USBMODE);
    mode.CM = 0x3;
    OUTREG32(&pRegs->OTG.USBMODE, *temp);


    Sleep(10);
    while ((INREG32(&pRegs->OTG.USBMODE)& 0x3) != 0x3);

     // Disable async and periodic schedule
    // Disable async and periodic schedule
    temp = (DWORD *)&cmd;
    *temp = 0;
    *temp = INREG32(&pRegs->OTG.USBCMD);
    *temp = *temp & ~0x30;
    OUTREG32(&pRegs->OTG.USBCMD, *temp);

 
    // Go to suspend mode, please
    temp  = (DWORD *)&portsc;
    *temp = 0;
    *temp = INREG32(&pRegs->OTG.PORTSC);
    *temp  |= 0x00000080;
    OUTREG32(&pRegs->OTG.PORTSC, *temp);

    // Set to TEST_MODE, please
    temp  = (DWORD *)&portsc;
    *temp = 0;
    *temp = INREG32(&pRegs->OTG.PORTSC);
    portsc.PTC = 0x4; // PTC = 4 -> test pattern
    OUTREG32(&pRegs->OTG.PORTSC, *temp);

    // Set run/stop bit now
    temp = (DWORD *)&cmd;
    *temp = 0;
    *temp = INREG32(&pRegs->OTG.USBCMD);
    cmd.RS = 1;
    OUTREG32(&pRegs->OTG.USBCMD, *temp);

    while(1);

#else

     temp = (DWORD *)&cmd;
    *temp = INREG32(&pRegs->OTG.USBCMD);
    cmd.RS=0;
    OUTREG32(&pRegs->OTG.USBCMD, *temp);
    
    // Handshake to wait for Halted
    while ((INREG32(&pRegs->OTG.USBSTS) & 0x1000) != 0x1000);

#endif

     // Enable interrupts
    OUTREG32(&pRegs->OTG.USBINTR, 0x5ff);

	RETAILMSG(OTG_DEBUG_MSG,(L"-ConfigOTG\r\n"));	
 }

//-----------------------------------------------------------------------------
//
//  Function: gfnGetOTGGroup
//
//  This function is to return the OTG Group 
//
//  Parameters:
//     NULL
//     
//  Returns:
//     the OTG Group Name, that is used for creating the mode switching event semaphore
//     the value is reading from registry
//
//-----------------------------------------------------------------------------
TCHAR *gfnGetOTGGroup(void)
{
    return (gszOTGGroup);
}

//-----------------------------------------------------------------------------
//
//  Function: gfnIsOTGSupport
//
//  This function is whether to have OTG support
//
//  Parameters:
//     NULL
//     
//  Returns:
//     0 - Not OTG Support, 1 - OTG Support
//
//-----------------------------------------------------------------------------
DWORD gfnIsOTGSupport(void)
{
    return gdwOTGSupport;
}

//-----------------------------------------------------------------------------
//
//  Function: InitializeH1
//
//  This function is to initialize USB Host 1 Core
//
//  Parameters:
//     NULL
//     
//  Returns:
//     0 - Not OTG Support, 1 - OTG Support
//
//-----------------------------------------------------------------------------
DWORD InitializeH1(CSP_USB_REGS *pRegs)
{
    InitializeHost1Mux();
    ConfigH1(pRegs);

    RETAILMSG(0,(TEXT("after ConfigH1\r\n")));

    return (DWORD)TRUE;
}

//-----------------------------------------------------------------------------
//
//  Function: InitializeH2
//
//  This function is to initialize USB Host 2 Core, ULPI PHY
//
//  Parameters:
//     NULL
//     
//  Returns:
//     0 - Not OTG Support, 1 - OTG Support
//
//-----------------------------------------------------------------------------
DWORD InitializeH2(CSP_USB_REGS *pRegs)
{
    InitializeHost2Mux();
    ConfigH2(pRegs);

    return (DWORD)TRUE;
}

//------------------------------------------------------------------------------
//
//  Function: InitializeTransceiver
//
//  This function is to configure the USB Transceiver, register the interrupt
//
//  Parameters:
//     NULL
//     
//  Returns:
//     TRUE - success, FALSE - failure
//
//------------------------------------------------------------------------------
BOOL InitializeTransceiver(PVOID* context, DWORD * phyregs, DWORD* dwOffset,
                           DWORD * sysintr, DWORD dwOTGSupport, TCHAR *pOTGGroup)
{
    DWORD dwRet = TRUE, speed, temp = 0;
    int off = 0, irq = 0;
    WORD sel = 0;
    WORD StringSize = 0;
    PCSP_USB_REGS* regs = (PCSP_USB_REGS*)context;

    StringSize = sizeof(gszOTGGroup) / sizeof(TCHAR);
    StringCchCopy(gszOTGGroup,StringSize,pOTGGroup);
    gdwOTGSupport = dwOTGSupport;

    dwRet = SelectUSBCore(&sel);
    speed = dwRet;
    gSel = sel;
    

    //only OTG port need to check KITL
	if(sel == 0 && FslUfnIsUSBKitlEnable())
	{
		RETAILMSG(1,(_T("USB Host load failure because Usb Kitl Enabled\r\n")));
		return FALSE;
	}

	/*Start clocks and PLL*/
    USBClockInit();
    BSPUsbPhyStartUp();
    

	switch(sel) 
	{
	case 0: // OTG
		{
			off = InitializeMux(sel); 
			ConfigOTG(*regs, speed);
			irq = IRQ_USB_OTG;
		} break;
	case 1: // Host 2
		{
			InitializeH2(*regs);
			off = offset(CSP_USB_REGS, H2) + offset(CSP_USB_REG, CAPLENGTH);
			irq = IRQ_USB_HOST2;

		} break;
	case 2: // Host 1
		{
			off = InitializeMux(sel); 
			ConfigH1(*regs);
			irq = IRQ_USB_HOST1;
		} break;
	}



    if (irq == IRQ_USB_OTG)  // OTG
    {
        INT32 aIrqs[3];
        aIrqs[0] = -1;
#ifdef GENERAL_OTG
        aIrqs[1] = OAL_INTR_FORCE_STATIC;  
#else
        aIrqs[1] = OAL_INTR_TRANSLATE;
#endif
        aIrqs[2] = IRQ_USB_OTG;
        KernelIoControl(IOCTL_HAL_REQUEST_SYSINTR, aIrqs, sizeof(aIrqs), sysintr, sizeof(DWORD), NULL);
    }
	else // HOST1 || HOST 2
		KernelIoControl(IOCTL_HAL_REQUEST_SYSINTR, &irq, sizeof(DWORD), sysintr, sizeof(DWORD), NULL);      

	DEBUGMSG(1, (TEXT("InitializeTransceiver: IRQ=%d, sysIntr=%d, sel=%d\r\n"),
		irq, *sysintr,sel));

	/*set controller to Host mode */
	{
		USB_USBMODE_T mode;
		DWORD * ptemp=(DWORD *)&mode;

		// Set USB Mode 
		*ptemp = 0;
		mode.CM = 3; // Host 

		switch (sel)
		{
			case 0: OUTREG32(&(*regs)->OTG.USBMODE, *ptemp); break;	
			case 1: OUTREG32(&(*regs)->H2.USBMODE, *ptemp);  break;
			case 2: OUTREG32(&(*regs)->H1.USBMODE, *ptemp);  break;	
		}
	}

    // power on port
    {
        if (sel == 0)
        {
            DWORD *ptemp;
            USB_HCSPARAMS_T hcs;
            ptemp=(DWORD *)&hcs;
            *ptemp=INREG32(&(*regs)->OTG.HCSPARAMS);

			//Check whether the host controller implementation includes port power control
            if (hcs.PPC) 
            {
                USB_PORTSC_T portsc;
                DWORD * temp2= (DWORD *)&portsc;

                *temp2 = INREG32(&(*regs)->OTG.PORTSC);
                portsc.PP = 1;
                SETREG32(&(*regs)->OTG.PORTSC, *temp2);
            }   
        }
        else if (sel == 1) //H2
        {
            DWORD *ptemp;
            USB_HCSPARAMS_T hcs;
            ptemp=(DWORD *)&hcs;
            *ptemp=INREG32(&(*regs)->H2.HCSPARAMS);

			//Check whether the host controller implementation includes port power control
            if (hcs.PPC) 
            {
                USB_PORTSC_T portsc;
                DWORD * temp2= (DWORD *)&portsc;

                *temp2 = INREG32(&(*regs)->H2.PORTSC);
                portsc.PP = 1;
                SETREG32(&(*regs)->H2.PORTSC, *temp2);
            }   
        }
        else //H1
        {
            DWORD *ptemp;
            USB_HCSPARAMS_T hcs;
            ptemp=(DWORD *)&hcs;
            *ptemp=INREG32(&(*regs)->H1.HCSPARAMS);

			//Check whether the host controller implementation includes port power control
            if (hcs.PPC) 
            {
                USB_PORTSC_T portsc;
                DWORD * temp2= (DWORD *)&portsc;

                *temp2 = INREG32(&(*regs)->H1.PORTSC);
                portsc.PP = 1;
                SETREG32(&(*regs)->H1.PORTSC, *temp2);
            }   
        }
    }
    
    gRegs = (PCSP_USB_REGS)(*regs);
    gSel = sel;

    if(sel == 1)//H2
    {
        dwRet=Initialize3317(*regs, sel);
    }

    temp=*(DWORD*)regs;
    temp+=off;
    *(DWORD*)regs=temp;
    *phyregs+=off;
    *dwOffset = off;

    return dwRet;
}

//-----------------------------------------------------------------------------
//
//  Function:  BSPUsbCheckConfigPower
//
//  Check power required by specific device configuration and return whether it
//  can be supported on this platform.  For CEPC, this is trivial, just limit
//  to the 500mA requirement of USB.  For battery powered devices, this could
//  be more sophisticated, taking into account current battery status or other 
//  info.
//
// Parameters:
//      bPort
//          [in] Port number
//
//      dwCfgPower
//          [in] Power required by configuration in mA.
//
//      dwTotalPower
//          [in] Total power currently in use on port in mA.
//
// Returns:
//      Return TRUE if configuration can be supported, FALSE if not.
//
//-----------------------------------------------------------------------------
BOOL BSPUsbhCheckConfigPower(UCHAR bPort, DWORD dwCfgPower, DWORD dwTotalPower)
{
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(bPort);

    return ((dwCfgPower + dwTotalPower) > 500) ? FALSE : TRUE;
}

#ifdef DISABLE_DETACH_WAKEUP
#undef DISABLE_DETACH_WAKEUP
#endif
 //-----------------------------------------------------------------------------
//
//  Function: BSPUsbSetWakeUp
//
//  This function is to enable/disable the wakeup interrupt bit in USBCONTROL
//
//  Parameters:
//     bEnable - TRUE : Enable, FALSE : Disable
//     
//  Returns:
//     NULL
//
//-----------------------------------------------------------------------------
void BSPUsbSetWakeUp(BOOL bEnable)
{
    // Access the USB Control Register
    volatile DWORD  *temp;
    OTG_CTRL_T otgctrl;
    UH1_CTRL_T uh1ctrl;
    USB_PORTSC_T portsc;
    CSP_USB_REG *pReg; 

    // still need to check it first before proceed
    BSPUsbCheckWakeUp();
        
    switch (gSel) 
    {
    case 0:
        pReg = (PCSP_USB_REG)(&(gRegs->OTG));
        break;
    case 1:
        pReg = (PCSP_USB_REG)(&(gRegs->H2));
        break;
    case 2:
        pReg = (PCSP_USB_REG)(&(gRegs->H1));
        break;
    
    default:
        pReg = (PCSP_USB_REG)(&(gRegs->OTG));
        break;
    }
    temp = (DWORD *)&portsc;
    *temp = INREG32(&pReg->PORTSC[0]);
    // If Current Connect Status = 1, we should not set WKCN or it would 
    // wake up right away.  With this we can enable wake up on attach
    if (bEnable)
    {
        //portsc.WKDC & portsc.WDCN is mutex, means at the same time, only one
        //can be set to 1, so we need to set WKDC or WKCN according to current
        //connect status
        if(portsc.CCS == 0)
        {
            portsc.WKDC = 0;
            portsc.WKCN = 1;
        }
        else
        {
#if defined USB_WAKEUP_CNANDDN
            portsc.WKDC = 1;
            portsc.WKCN = 0;
#else
            portsc.WKDC = 0;
            portsc.WKCN = 0;
#endif
        }

        portsc.WKOC = 1;
        
                
    }
    else
    {
        portsc.WKOC = 0;
        portsc.WKDC = 0;
        portsc.WKCN = 0;
    }
    // This delay is very important, if no delay here, a ULPI interrupt will be issued
    // immediately and system can't suspend at all.
    StallExecution(10000);
    OUTREG32(&pReg->PORTSC[0], *temp);

    if(gSel == 0 || gSel == 2)
    {
      
        switch (gSel)
        {
        case 0:
            
            temp = (DWORD *)&otgctrl;
            if (bEnable)
              *temp = INREG32(&gRegs->OTG_CTRL);
            else
                *temp = 0;
                otgctrl.WIE = 1;

            if (bEnable)
                OUTREG32(&gRegs->OTG_CTRL, *temp);
            else
                CLRREG32(&gRegs->OTG_CTRL, *temp);
            break;

        case 2:

            temp = (DWORD *)&otgctrl;
             if (bEnable)
               *temp = INREG32(&gRegs->UH1_CTRL);
             else
                 *temp = 0;
            uh1ctrl.WIE = 1;
            #if defined USB_WAKEUP_CNANDDN
      //      uh1ctrl.UIE = 1;
            #else
      //      uh1ctrl.IE = 0;
            #endif
             if (bEnable)
                 OUTREG32(&gRegs->UH1_CTRL, *temp);
             else
                 CLRREG32(&gRegs->UH1_CTRL, *temp);

            break;
        default:
            break;
        }

      
    }
   

    return;
}

//------------------------------------------------------------------------------
//
//  Function: BSPUsbCheckWakeUp
//
//  This function is called by CSP to clear the wakeup interrupt bit in USBCONTROL. According to
//  Ringo specification, disable the wake-up enable bit also clear the interrupt request bit.
//  This wake-up interrupt enable should be disable after receiving a wakeup request.
//
//  Parameters:
//     NULL
//     
//  Returns:
//     TRUE - there is a wakeup.  FALSE - no wakeup is set.
//
//------------------------------------------------------------------------------
BOOL BSPUsbCheckWakeUp(void)
{

    // Access the USB Control Register
    volatile DWORD  *temp;
    volatile DWORD  *temp2;
    OTG_CTRL_T      ctrl;
    UH1_CTRL_T     ctrl2;
    BOOL fWakeUp = FALSE;

    temp = (DWORD *)&ctrl;
    *temp = INREG32(&gRegs->OTG_CTRL);

   temp2 = (DWORD*)&ctrl2;
   *temp2 = INREG32(&gRegs->UH1_CTRL);

 
    switch (gSel) {
    case 0:
    if( ctrl.WIR == 1)
    {
        *temp = 0;
        fWakeUp = TRUE;
        ctrl.WIE = 1;
    }
    break;

    case 2:
       
    if (ctrl2.WIR == 1)
    {
        *temp2 = 0;
        fWakeUp = TRUE;
        ctrl2.WIE = 1;
    }
    break;

#if 0
    case 2:
        if(ctrl.H1WIR == 1)
        {
            *temp = 0;
            fWakeUp = TRUE;
            ctrl.H1WIE = 1;
        }  
        break;
#endif
    default:
        break;
    }

    if (fWakeUp)
    {
        if(gSel == 0 )
            CLRREG32(&gRegs->OTG_CTRL, *temp);  
        else if(gSel == 2)
            CLRREG32(&gRegs->UH1_CTRL, *temp2);  
      
    }

     return fWakeUp;
}

//------------------------------------------------------------------------------
//
//  Function: BSPUsbHostLowPowerModeEnable
//
//  This function is called by CSP to get the information about if we need enable
//  Low Power Mode, enable means close PHY clock, disable means keep PHY clock
//
//  Parameters:
//     NULL
//     
//  Returns:
//     TRUE - enable.  FALSE - disable.
//
//------------------------------------------------------------------------------
BOOL BSPUsbHostLowPowerModeEnable(void)
{
    return TRUE;
}

//------------------------------------------------------------------------------
// Function: BSPUSBHostVbusControl
//
// This function is to control USB VBus
//
// Parameters:
//     blOn
//         [IN] Pull UP/DOWN USB VBUS 
//     
// Returns:
//     
//
//------------------------------------------------------------------------------
void BSPUSBHostVbusControl(BOOL blOn)
{
    UNREFERENCED_PARAMETER(blOn);
}

//------------------------------------------------------------------------------
// Function: BSPNeedCloseVBus
//
// This function is used to control if we need to do a VBUS off-on Cycle on public
// code druing resuem (power-up) opertion
//
// Parameters:
//          N.A
//     
// Returns:
//      TRUE - public code need to close VBUS and open it again 
//      FALSE - don't need to do so
//     
//
//------------------------------------------------------------------------------
BOOL BSPNeedCloseVBus(void)
{
    return TRUE;
}


//------------------------------------------------------------------------------
// Function: IsLowPowerSameAsSuspend
//
// This function is used to return a bool value to indicate if additional process
// is need to proper handle LowPower mode and Suspend mode
//
// Parameters:
//          blSuspend -- if current state is suspend or not
//     
// Returns:
//      TRUE - public code need to close VBUS and open it again 
//      FALSE - don't need to do so
//     
//
//------------------------------------------------------------------------------
BOOL IsLowPowerSameAsSuspend(BOOL blSuspend)
{
    if(blSuspend)
    {
#if defined USB_WAKEUP_CNANDDN || defined USB_WAKEUP_CN
        return TRUE;
#else
        return FALSE;
#endif
    }
    else
    {
        return FALSE;
    }
}
