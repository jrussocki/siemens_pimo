/*---------------------------------------------------------------------------
* Copyright (C) 2005-2010, Freescale Semiconductor, Inc. All Rights Reserved.
* THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
* AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT 
*--------------------------------------------------------------------------*/

 
/*
 *  File:     USB3317.c
 *  Purpose:  Platform-specific host transceiver configuration
 *            Interrupt, USB signal pull-up, and Vbus driving configuration
 *
 *  Functions:  
 *              Initialize3317 - initial set up for host mode
 *              SetULPIHostMode - change those parts for swapping to host 
 *                                from client mode
 */              

#pragma warning(push)
#pragma warning(disable: 4115)
#pragma warning(disable: 4201)
#pragma warning(disable: 4204)
#pragma warning(disable: 4214)
#include <windows.h>
#pragma warning(pop)
#include <Winbase.h>
#pragma warning(push)
#pragma warning(disable: 4214)
#include <ceddk.h>
#pragma warning(pop)
#pragma warning(push)
#pragma warning(disable: 4201)
#include "bsp.h"
#pragma warning(pop)
#include "mx6_usb.h"
#include "common_usbname.h"
#include "common_usbcommon.h"

//-----------------------------------------------------------------------------
//
//  Function: Initialize3317
//
//  This function is to configure the USB3317 transceiver and corresponding 
//  USB OTG Core. This function is called by InitializeTransceiver in hwinit.c
//
//  Parameters:
//     regs - Pointer to the 3 USB Core registers.
//     
//  Returns:
//     TRUE - success, FALSE - failure
//
//-----------------------------------------------------------------------------

BOOL Initialize3317(CSP_USB_REGS * regs, WORD sel)
{
//          
    DWORD * temp;
    volatile DWORD * view_port = NULL;

    if (sel == 1)
    {
        USB_HCSPARAMS_T hcs;
        temp=(DWORD *)&hcs;
        *temp=INREG32(&regs->H2.HCSPARAMS);

        if (hcs.PPC) {
            USB_PORTSC_T portsc;
            temp=(DWORD *)&portsc;
            *temp=0;
            portsc.CSC=1; portsc.PEC=1; portsc.OCC=1;
            *temp=(~*(temp))&INREG32(&regs->H2.PORTSC);

                
            SETREG32(&regs->H2.PORTSC, *temp);
        }

        //DumpRegs((CSP_USB_REG *)&regs->H2);
        view_port = (volatile DWORD*)&regs->H2.ULPI_VIEWPORT;

    }                   
        
    DEBUGMSG(ZONE_INIT, (TEXT("Access PHY set to host mode - USBHOST\r\n")));
    // Execute reset
    USB3317_WriteReg(view_port, USB3317_FUNCTION_CTRL_S, 0x20);

    USB3317_WriteReg(view_port, USB3317_OTG_CTRL_S, 0x1); // ID_PULLUP      
    // enable external vbus supply
    // onboard power supply MC13783 feeds through MAX4787 current limit switch
    USB3317_WriteReg(view_port, USB3317_OTG_CTRL_S, 0x40); // DRV_VBUS_EXT

    // disable vbus (internal)
    // use external VBus power supply from MC17383 DC-DC converter
    USB3317_WriteReg(view_port, USB3317_OTG_CTRL_S, 0x20); // DRV_VBUS

    // enable external vbus (fault) indicator
    // use MAX4787 fault flag to indicate over current condition
    USB3317_WriteReg(view_port, USB3317_OTG_CTRL_S, 0x80); // USE_EXT_VBUS_IND

    USB3317_WriteReg(view_port, USB3317_OTG_CTRL_S, 0x10); // VBus Charge
    return TRUE;
}

//-----------------------------------------------------------------------------
//
//  Function: SetULPIToHostMode
//
//  This function is called by CSP public module to configure the ULPI to host
//  mode before putting the ULPI to suspend mode or after resume.
//
//  Parameters:
//     pRegs - Pointer to the 3 USB Core registers.
//     pSel - USB Core to be configured. 
//     fSuspend - Suspend or resume
//     blSysSus - system suspend or not
//     
//  Returns:
//     NONE
//
//-----------------------------------------------------------------------------

void SetULPIHostMode3317(PCSP_USB_REGS pRegs, WORD pSel, BOOL fSuspend)
{
    volatile DWORD * view_port;
    DEBUGMSG (ZONE_FUNCTION, (TEXT("SetULPIHostMode sel = %d\r\n"),pSel));
    if (pSel == 1)
        view_port =(volatile DWORD*) &pRegs->H2.ULPI_VIEWPORT;
    else
        return;

    DEBUGMSG (ZONE_FUNCTION, (TEXT("Dump UH2_CTRL = 0x%x\r\n"), INREG32(&pRegs->UH2_CTRL)));
    if (fSuspend)
    {
        USB3317_WriteReg(view_port, USB3317_FUNCTION_CTRL_C, 0x3); // Clear the XCVRSELECT
        USB3317_WriteReg(view_port, USB3317_FUNCTION_CTRL_S, 0x1); // Set the XCVRSELECT

        USB3317_WriteReg(view_port, USB3317_FUNCTION_CTRL_S, 0x4); // Set the TERM SELECT

        USB3317_WriteReg(view_port, USB3317_FUNCTION_CTRL_C, 0x18); // Set the OPMODE = 00

        USB3317_WriteReg(view_port, USB3317_OTG_CTRL_S, 0x2); // DP_PULL_DOWN

        USB3317_WriteReg(view_port, USB3317_OTG_CTRL_S, 0x4); // DM_PULL_DOWN
    }
    else
    {
        USB3317_WriteReg(view_port, USB3317_FUNCTION_CTRL_S, 0x20);

        USB3317_WriteReg(view_port, USB3317_OTG_CTRL_S, 0x1); // ID_PULLUP      
        // enable external vbus supply
        // onboard power supply MC13783 feeds through MAX4787 current limit switch
        USB3317_WriteReg(view_port, USB3317_OTG_CTRL_S, 0x40); // DRV_VBUS_EXT

        // disable vbus (internal)
        // use external VBus power supply from MC17383 DC-DC converter
        USB3317_WriteReg(view_port, USB3317_OTG_CTRL_S, 0x20); // DRV_VBUS

        // enable external vbus (fault) indicator
        // use MAX4787 fault flag to indicate over current condition
        USB3317_WriteReg(view_port, USB3317_OTG_CTRL_S, 0x80); // USE_EXT_VBUS_IND
    }
}

//-----------------------------------------------------------------------------
//
//  Function: PostSetULPIToHostMode
//
//  This function is called by CSP public module to configure the ULPI to host
//  mode before putting the ULPI to suspend mode or after resume.
//
//  Parameters:
//     pRegs - Pointer to the 3 USB Core registers.
//     pSel - USB Core to be configured. 
//     fSuspend - Suspend or resume
//     
//  Returns:
//     NONE
//
//-----------------------------------------------------------------------------
void PostSetULPIHostMode3317(PCSP_USB_REGS pRegs, WORD pSel, BOOL fSuspend)
{
    volatile DWORD * view_port;
    DEBUGMSG (ZONE_FUNCTION, (TEXT("SetULPIHostMode sel = %d\r\n"),pSel));
    if (pSel == 1)
        view_port =(volatile DWORD*) &pRegs->H2.ULPI_VIEWPORT;
    else
        return;

    DEBUGMSG (ZONE_FUNCTION, (TEXT("Dump UH2_CTRL = 0x%x\r\n"), INREG32(&pRegs->UH2_CTRL)));
    if (fSuspend)
    {
        {
#if defined USB_WAKEUP_NONE
            USB3317_WriteReg(view_port, USB3317_OTG_CTRL_C, 0x60); // DRV_VBUS & DRV_VBUS_EXT
#endif
        }
    }
    else
    {
        {
#if !defined USB_WAKEUP_NONE
            USB3317_WriteReg(view_port, USB3317_OTG_CTRL_S, 0x10); // VBus Charge
#endif
        }
    }
}



