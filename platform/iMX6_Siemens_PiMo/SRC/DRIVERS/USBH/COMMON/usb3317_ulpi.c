/*---------------------------------------------------------------------------
* Copyright (C) 2005-2010, Freescale Semiconductor, Inc. All Rights Reserved.
* THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
* AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT 
*--------------------------------------------------------------------------*/

 

/*
 *  File:     USB3317_ULPI.c
 *  Purpose:  Common routines for manipulating USB1504 controller
 *  Functions: WakeUpULPI   - initiate wakeup on suspended 1504 transceiver
 *           The following 3 functions access transceiver directly via ULPI and
 *           must be coordinated with host/device controller access to ULPI.
 *           Controller should be stopped to avoid ULPI bus clash.
 *             USB3317_ReadReg()     read a transceiver register via viewport
 *             USB3317_WriteReg()    write a transceiver register via viewport
 *             DumpULPIRegs  - diagnostic dump of transceiver registers
 */
#pragma warning(push)
#pragma warning(disable: 4115)
#pragma warning(disable: 4201)
#pragma warning(disable: 4204)
#pragma warning(disable: 4214)
#include <windows.h>
#pragma warning(pop)
#include <Winbase.h>
#pragma warning(push)
#pragma warning(disable: 4214)
#include <ceddk.h>
#pragma warning(pop)
#pragma warning(push)
#pragma warning(disable: 4201)
#include "bsp.h"
#pragma warning(pop)
#include "mx6_usb.h"
#include "common_usbname.h"
#include "common_usbcommon.h"

extern WORD BSPGetUSBControllerType(void);

//-----------------------------------------------------------------------------
//
//  Function: WakeUpULPI
//
//  This function is to wakeup the ULPI if it is suspend through viewport
//
//  Parameters:
//     reg - Pointer to the corresponding ULPI viewport registers
//     
//  Returns:
//     TRUE - success, FALSE - failure
//
//-----------------------------------------------------------------------------

static BOOL WakeUpULPI(volatile DWORD *reg)
{
    USB_ULPI_VIEWPORT_T ulpi;
    int i;
    DWORD * temp = (DWORD *)&ulpi;
    
    *temp = INREG32(reg);    
    if (ulpi.ULPISS == 0)
    {
        *temp = 0;
        ulpi.ULPIWU = 1;
        ulpi.ULPIRUN = 0;
        OUTREG32(reg, *temp);
        for (i = 0; i < 2000; i++) {
            *temp = INREG32(reg);
            if (ulpi.ULPIWU == 0)
                return TRUE;
            Sleep(0);
        }
        *temp = INREG32(reg);
        if (ulpi.ULPIWU == 1)
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("WakeUpULPI return FALSE\r\n")));
            return FALSE;
        }
    }
    return TRUE;
}

//-----------------------------------------------------------------------------
//
//  Function: USB3317_ReadReg
//
//  This function is to read the value of ULPI transceiver via viewport
//
//  Parameters:
//     reg - Pointer to the ULPI viewport register
//     idx - Offset of the ULPI viewport register want to access.
//     Refer to mx31_usbcommon.h in csp\arm\freescale\mx31\inc for full list
//     
//  Returns:
//     value in the register in unsigned character format.
//     0 - indicate failure
//
//-----------------------------------------------------------------------------

UCHAR USB3317_ReadReg(volatile DWORD * reg, UCHAR idx)
{
    USB_ULPI_VIEWPORT_T ulpi;
    DWORD * temp=(DWORD *)&ulpi;
    int iAttempts;
    
    WakeUpULPI(reg);
    *temp=0;
    ulpi.ULPIRUN=1;
    ulpi.ULPIADDR=idx;
    OUTREG32(reg, *temp);   

    iAttempts = 0;
    do {
        Sleep(0);
        *temp = INREG32(reg);
        
        if ( !ulpi.ULPIRUN )
            return ((UCHAR)ulpi.ULPIDATRD);

    } while ( iAttempts++ < 1000 );

    DEBUGMSG (ZONE_ERROR,(TEXT("Port(%d):USB3317_ReadReg: ############ Error -- failed to read ULPI reg %d\r\n"),BSPGetUSBControllerType(), idx));

    return 0;
}

//-----------------------------------------------------------------------------
//
//  Function: USB3317_WriteReg
//
//  This function is to write the data to register of ULPI transceiver via viewport
//
//  Parameters:
//     reg - Pointer to the ULPI viewport register
//     idx - Offset of the ULPI viewport register want to access.
//     Refer to mx31_usbcommon.h in csp\arm\freescale\mx31\inc for full list
//     data - Data to be written
//     
//  Returns:
//     TRUE - success, FALSE - failure
//
//-----------------------------------------------------------------------------

BOOL USB3317_WriteReg(volatile DWORD * reg, UCHAR idx, UCHAR data)
{
    USB_ULPI_VIEWPORT_T ulpi;
    DWORD * temp=(DWORD *)&ulpi;
    int iAttempts;

    WakeUpULPI(reg);
    *temp=0;
    ulpi.ULPIRUN=1;
    ulpi.ULPIADDR=idx;
    ulpi.ULPIRW=1;
    ulpi.ULPIDATWR=data;
    OUTREG32(reg, *temp);   

    iAttempts = 0;
    do {
        Sleep(0);
        *temp = INREG32(reg);

        if ( !ulpi.ULPIRUN )
            return TRUE;

    } while ( iAttempts++ < 1000 );

    DEBUGMSG (ZONE_ERROR,(TEXT("Port (%d):USB3317_WriteReg: ############ Error -- failed to write ULPI reg %d\r\n"),
                        BSPGetUSBControllerType(), idx));
    return FALSE;
}

//-----------------------------------------------------------------------------
//
//  Function: DumpULPIRegs
//
//  This function is to dump the ULPI register for debug purpose
//
//  Parameters:
//     regs - Pointer to the 3 USB Core Registers
//     
//  Returns:
//     NULL
//
//-----------------------------------------------------------------------------

void DumpULPIRegs3317(CSP_USB_REGS * regs)
{
    volatile CSP_USB_REG *pReg;
    // Read the ID
    WORD sel = BSPGetUSBControllerType();
    
    if (sel == USB_SEL_OTG)
        pReg = &regs->OTG;
    else if (sel == USB_SEL_H2)
        pReg = &regs->H2;
    else
        return;

    DEBUGMSG (ZONE_FUNCTION, (L"Dump PORTSC (0x%x)\r\n", INREG32(&pReg->PORTSC[0])));
    DEBUGMSG (ZONE_FUNCTION, (L"Dump ULPI registers(%x)\r\n", INREG32(&pReg->ULPI_VIEWPORT)));    
    DEBUGMSG (ZONE_FUNCTION, (L"\tULPI_VID=(%x,%x)\r\n", 
            USB3317_ReadReg((volatile DWORD*)&pReg->ULPI_VIEWPORT, USB3317_VENDORID_LOW_R),
            USB3317_ReadReg((volatile DWORD*)&pReg->ULPI_VIEWPORT, USB3317_VENDORID_HIGH_R)));
    DEBUGMSG (ZONE_FUNCTION, (L"\tULPI_PID=(%x,%x)\r\n", 
            USB3317_ReadReg((volatile DWORD*)&pReg->ULPI_VIEWPORT, USB3317_PRODUCT_LOW_R),
            USB3317_ReadReg((volatile DWORD*)&pReg->ULPI_VIEWPORT, USB3317_PRODUCT_HIGH_R)));

     DEBUGMSG (ZONE_FUNCTION, (L"\tFunction Control(%x)=%x\r\n", 
            USB3317_FUNCTION_CTRL_RW,
            USB3317_ReadReg((volatile DWORD*)&pReg->ULPI_VIEWPORT, USB3317_FUNCTION_CTRL_RW)));
    DEBUGMSG (ZONE_FUNCTION, (L"\tInterface Control(%x)=%x\r\n", 
            USB3317_INTERFACE_CTRL_RW,
            USB3317_ReadReg((volatile DWORD*)&pReg->ULPI_VIEWPORT, USB3317_INTERFACE_CTRL_RW)));
      DEBUGMSG (ZONE_FUNCTION, (L"\tOTG Control(%x)=%x\r\n", 
            USB3317_OTG_CTRL_RW,
            USB3317_ReadReg((volatile DWORD*)&pReg->ULPI_VIEWPORT, USB3317_OTG_CTRL_RW)));
    DEBUGMSG (ZONE_FUNCTION, (L"\tInterrupt Enable rising(%x)=%x\r\n", 
            USB3317_INTR_RISING_RW,
            USB3317_ReadReg((volatile DWORD*)&pReg->ULPI_VIEWPORT, USB3317_INTR_RISING_RW)));
    DEBUGMSG (ZONE_FUNCTION, (L"\tInterrupt Enable Falling(%x)=%x\r\n", 
            USB3317_INTR_FALLING_RW,
            USB3317_ReadReg((volatile DWORD*)&pReg->ULPI_VIEWPORT, USB3317_INTR_FALLING_RW)));
    DEBUGMSG (ZONE_FUNCTION, (L"\tInterrupt Status(%x)=%x\r\n", 
            USB3317_INTR_STATUS_R,
            USB3317_ReadReg((volatile DWORD*)&pReg->ULPI_VIEWPORT, USB3317_INTR_STATUS_R)));
    DEBUGMSG (ZONE_FUNCTION, (L"\tInterrupt Latch(%x)=%x\r\n", 
            USB3317_INTR_LATCH_RC,
            USB3317_ReadReg((volatile DWORD*)&pReg->ULPI_VIEWPORT, USB3317_INTR_LATCH_RC)));
    DEBUGMSG (ZONE_FUNCTION, (L"\tDebug(%x)=%x\r\n", 
            USB3317_DEBUG_R,
            USB3317_ReadReg((volatile DWORD*)&pReg->ULPI_VIEWPORT, USB3317_DEBUG_R)));
    DEBUGMSG (ZONE_FUNCTION, (L"\tScratch(%x)=%x\r\n", 
            USB3317_SCRATCH_RW,
            USB3317_ReadReg((volatile DWORD*)&pReg->ULPI_VIEWPORT, USB3317_SCRATCH_RW)));
    DEBUGMSG (ZONE_FUNCTION, (L"\tPower(%x)=%x\r\n", 
            USB3317_POWER_CTRL_RW,
            USB3317_ReadReg((volatile DWORD*)&pReg->ULPI_VIEWPORT, USB3317_POWER_CTRL_RW)));
}
