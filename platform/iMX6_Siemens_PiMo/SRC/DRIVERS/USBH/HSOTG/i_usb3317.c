//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2010, Freescale Semiconductor, Inc. All Rights Reserved.
// THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
// AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------ 
//
//  File:  i_USB3317
//  This file is linked to USB3317.c in the USBH Common
//
//
#define USB_HOST_MODE 1         // OTG high speed
#include <..\common\USB3317.c>
