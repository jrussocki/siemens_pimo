//-------------------------------------------------------------------------------------------------------------------
// Copyright (C) 2007-2011, Freescale Semiconductor, Inc. All Rights Reserved.
// THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
// AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
//
//  File:  bspvpu.c
//
//  Provides BSP-specific configuration routines for the VPU module.
//
//-------------------------------------------------------------------------------------------------------------------

#pragma warning(push)
#pragma warning(disable: 4115)
#pragma warning(disable: 4201)
#pragma warning(disable: 4204)
#pragma warning(disable: 4214)
#include <windows.h>
#include "vpu_api.h"
#pragma warning(pop)
#include "bsp.h"
//-------------------------------------------------------------------------------------------------------------------
// External Variables
//-------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------
// Global Variables
//-------------------------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------------------------
// Defines
//-------------------------------------------------------------------------------------------------------------------


//=====================================================================================
//
//  Following IRMA setting is for Decoder
//
// 15K bytes Internal RAM for VPU to save intermediate data such as MB prediction data
#define VPU_DEC_BIT_IRAM_PA_START    (IMAGE_WINCE_VPU_IRAM_PA_START)
#define VPU_DEC_BIT_IRAM_SIZE        (15*1024)
// 15K bytes Internal RAM for VPU to save intermediate data such as intra/ACDC prediction
#define VPU_DEC_IP_IRAM_PA_START    (VPU_DEC_BIT_IRAM_PA_START+VPU_DEC_BIT_IRAM_SIZE)
#define VPU_DEC_IP_IRAM_SIZE        (15*1024)
// 60K bytes Internal RAM for VPU to save intermediate data for deblocking filter
#define VPU_DEC_DBKY_IRAM_PA_START    (VPU_DEC_IP_IRAM_PA_START+VPU_DEC_IP_IRAM_SIZE)
#define VPU_DEC_DBKY_IRAM_SIZE        (30*1024)
#define VPU_DEC_DBKC_IRAM_PA_START    (VPU_DEC_DBKY_IRAM_PA_START+VPU_DEC_DBKY_IRAM_SIZE)
#define VPU_DEC_DBKC_IRAM_SIZE        (30*1024)
// 10K bytes Internal RAM for VPU to save intermediate data for overlap filter
#define VPU_DEC_OVL_IRAM_PA_START    (VPU_DEC_DBKC_IRAM_PA_START+VPU_DEC_DBKC_IRAM_SIZE)
#define VPU_DEC_OVL_IRAM_SIZE        (10*1024)

//=====================================================================================
//
//  Following IRMA setting is for Encoder
//
// 10K bytes Internal RAM for VPU to save intermediate data such as MB prediction data
#define VPU_ENC_BIT_IRAM_PA_START    (IMAGE_WINCE_VPU_IRAM_PA_START)
#define VPU_ENC_BIT_IRAM_SIZE        (10*1024)
// 10K bytes Internal RAM for VPU to save intermediate data such as intra/ACDC prediction
#define VPU_ENC_IP_IRAM_PA_START    (VPU_ENC_BIT_IRAM_PA_START+VPU_ENC_BIT_IRAM_SIZE)
#define VPU_ENC_IP_IRAM_SIZE        (10*1024)
// 20K bytes Internal RAM for VPU to save intermediate data for deblocking filter
#define VPU_ENC_DBKY_IRAM_PA_START    (VPU_ENC_IP_IRAM_PA_START+VPU_ENC_IP_IRAM_SIZE)
#define VPU_ENC_DBKY_IRAM_SIZE        (10*1024)
#define VPU_ENC_DBKC_IRAM_PA_START    (VPU_ENC_DBKY_IRAM_PA_START+VPU_ENC_DBKY_IRAM_SIZE)
#define VPU_ENC_DBKC_IRAM_SIZE        (10*1024)
// 7K bytes Internal RAM for VPU to save intermediate data for overlap filter
#define VPU_ENC_OVL_IRAM_PA_START    (VPU_ENC_DBKC_IRAM_PA_START+VPU_ENC_DBKC_IRAM_SIZE)
#define VPU_ENC_OVL_IRAM_SIZE        (7*1024)
// 
#define VPU_ENC_SEARCH_IRAM_PA_START (VPU_ENC_OVL_IRAM_PA_START + VPU_ENC_OVL_IRAM_SIZE)
#define VPU_ENC_MAX_WIDTH             (1280) // MAX 720P is supported by VPU encode

#define GET_IRAM(X, Y) ((X) ? VPU_DEC_##Y##_IRAM_PA_START : VPU_ENC_##Y##_IRAM_PA_START)


#define VPU_DECODER_REG_PATH                        TEXT("Drivers\\VPU\\Decoder")
#define VPU_DECODER_USERDATA_SIZE                   TEXT("UserDataBufferSize")
//-------------------------------------------------------------------------------------------------------------------
// Types
//-------------------------------------------------------------------------------------------------------------------
typedef struct {
    BOOL Initialized;
    UINT uPowerFlag;
    UINT uAutoClkFlag;
    UINT8 UsedInst[MAX_NUM_INSTANCE];
} BSP_VPU_GLOBALS, *PBSP_VPU_GLOBALS;

typedef struct {
    int useBitEnable;
    int useIpEnable;
    int useDbkYEnable;
    int useDbkCEnable;
    int useOvlEnable;
    int useBtpEnable;

    int useHostBitEnable;
    int useHostIpEnable;
    int useHostDbkYEnable;
    int useHostDbkCEnable;
    int useHostOvlEnable;
    int useHostBtpEnable;
    
    PhysicalAddress bufBitUse;
    PhysicalAddress bufIpAcDcUse;
    PhysicalAddress bufDbkYUse;
    PhysicalAddress bufDbkCUse;
    PhysicalAddress bufOvlUse;
    PhysicalAddress bufBtpUse;
} SecAxiUse;

//-------------------------------------------------------------------------------------------------------------------
// Local Variables
//-------------------------------------------------------------------------------------------------------------------
static BSP_VPU_GLOBALS g_VPUGlobal;
static CRITICAL_SECTION g_VpuArgCs;; // Used to protect clock gating

//-------------------------------------------------------------------------------------------------------------------
//
// Function: BSVpuPInit
//
// This method does BSP specific globale varialbe initializing
//
//
// Parameters: None
//
// Returns:
//     TRUE for success, FALSE for failure
//
//-------------------------------------------------------------------------------------------------------------------
BOOL BSPVpuInit()
{
    int i;
    BOOL initState;

    InitializeCriticalSection(&g_VpuArgCs);
    
    EnterCriticalSection(&g_VpuArgCs);
    for (i = 0; i < MAX_NUM_INSTANCE; ++i)
        g_VPUGlobal.UsedInst[i] = 0;
    g_VPUGlobal.uPowerFlag = 0;

    // Always enable VPU BUSY override bit in the intialization
    g_VPUGlobal.uAutoClkFlag = 1;
    DDKClockSetOverride(DDK_CLOCK_OVERRIDE_ENABLE_VPU, DDK_CLOCK_OVERRIDE_MODE_ENABLED);
    LeaveCriticalSection(&g_VpuArgCs);
    
    // Disable AXI bus clock and Decoding core clock
    initState = DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_VPU, DDK_CLOCK_GATE_MODE_DISABLED);
    if (initState != TRUE)
    {
        ERRORMSG(TRUE, (TEXT("VPU AXI and Core clocks gating failed!\r\n")));
        goto cleanUp;
    }

    return TRUE;

cleanUp:
    DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_VPU, DDK_CLOCK_GATE_MODE_DISABLED);

    DeleteCriticalSection(&g_VpuArgCs);
    
    return FALSE;
}

//--------------------------------------------------------------------------------------------------------------------
//
// Function: BSPVpuAllocWorkingBuffer
//
// This method allocates the physical memmory used for working buffer,
//
// Parameters: 
//      size
//          [in] The size of physical memory requested 
//      pPhysAdd
//          [out] Pointer to a PhysicalAddress that stores 
//          the physical address of the memory allocation.   
// Returns:
//          The virtual address of working buffer
//
//-------------------------------------------------------------------------------------------------------------------
void* BSPVpuAllocWorkingBuffer(Uint32 size, PPhysicalAddress pPhysAdd)
{

#ifdef RESERVED_WORKING_BUFFER
    PHYSICAL_ADDRESS phyAddr;
    phyAddr.QuadPart = *pPhysAdd = IMAGE_WINCE_VPU_RAM_PA_START;
    return MmMapIoSpace(phyAddr, size, FALSE);
#else
    return AllocPhysMem(size, PAGE_READWRITE, 0, 0, pPhysAdd);
#endif
}

//-------------------------------------------------------------------------------------------------------------------
//
// Function: BSPVpuFreeWorkingBuffer
//
// This method releases the allocated working buffer,
//
// Parameters: 
//      size
//          [in] The size of physical memory requested 
//      pVirtAdd
//          [out] Pointer to the base virtual address to allocated physical pages  
// Returns:
//          No return
//
//-------------------------------------------------------------------------------------------------------------------
void BSPVpuFreeWorkingBuffer(void *pVirtAdd, Uint32 size)
{
#ifdef RESERVED_WORKING_BUFFER
    MmUnmapIoSpace(pVirtAdd, size);
#else
    UNREFERENCED_PARAMETER(size);
    FreePhysMem(pVirtAdd);
#endif
}

//-------------------------------------------------------------------------------------------------------------------
//
// Function: BSPVpuDeinit
//
// This method does the board specific deinitialization
//
//
// Parameters: The pointer to the Uint32 that stores the flag, which indicates
//             if the hardware is used by other application
//
// Returns:
//     TRUE for success, FALSE for failure
//
//-------------------------------------------------------------------------------------------------------------------
BOOL BSPVpuDeinit(void)
{
    DeleteCriticalSection(&g_VpuArgCs);    
    // Disable AXI bus clock and Decoding core clock
    DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_VPU, DDK_CLOCK_GATE_MODE_DISABLED);

    return TRUE;
}

//-------------------------------------------------------------------------------------------------------------------
//
//  Function:  BSPVpuGetCodecInstance
//
//  This function obtains a processing instance
//
//  Parameters:
//      No input
//
//  Returns:
//      Index allocated for current decoder instance
//
//-------------------------------------------------------------------------------------------------------------------

UINT32 BSPVpuGetCodecInstance(void)
{
    int i;
    int AvailableInstidx = MAX_NUM_INSTANCE;

    // Update VPU global parameters
    EnterCriticalSection(&g_VpuArgCs);
    for (i = 0; i < MAX_NUM_INSTANCE; ++i)
    {
        if (!g_VPUGlobal.UsedInst[i])
        {
            g_VPUGlobal.UsedInst[i] = 1;
            AvailableInstidx = i;
            break;
        }
    }
    LeaveCriticalSection(&g_VpuArgCs);

    return AvailableInstidx;
}

//-------------------------------------------------------------------------------------------------------------------
//
//  Function:  BSPVpuFreeCodecInstance
//
//  This function releases a processing instance
//
//  Parameters:
//      uInstIdx
//          [in] Index of decoder instance that'll be freed
//
//  Returns:
//          No return value.
//
//-------------------------------------------------------------------------------------------------------------------
void BSPVpuFreeCodecInstance(UINT32 uInstIdx)
{
    // Update VPU global parameters
    EnterCriticalSection(&g_VpuArgCs);
    g_VPUGlobal.UsedInst[uInstIdx] = 0;
    LeaveCriticalSection(&g_VpuArgCs);
}

//-------------------------------------------------------------------------------------------------------------------
//
// Function: BSPVpuSetSecondAXIIRAM
//
// This method allocates the internal RAM for VPU decoding temporary buffer that
// can reduce system bandwidth. MBwidth is the width of image in the unit of MB.
// If disable to use IRAM, just set both flash and base address to 0.
//
// Parameters:
//         secAxiIramInfo
//
//    int useHostBitEnable --  If enabled, the size of BitIram should be 128*MBwidth
//    int useHostIpEnable  --  If enabled, the size of IpIram should be 128*MBwidth 
//    int useHostDbkEnable --  If enabled, the size of DbkIram should be 512*MBwidth
//    int useHostOvlEnable --  If enabled, the size of OvlIram should be 80*MBwidth
//    PhysicalAddress bufBitUse;
//    PhysicalAddress bufIpAcDcUse;
//    PhysicalAddress bufDbkYUse;
//    PhysicalAddress bufDbkCUse;
//    PhysicalAddress bufOvlUse;
//
//         bDecode
//    TURE -- Set for Decoder; FALSE -- Set for Encoder
//
// Returns:
//     No return value
//
//-------------------------------------------------------------------------------------------------------------------

void BSPVpuSetSecondAXIIRAM(SecAxiUse *psecAxiIramInfo, BOOL bDecode)
{
    // i.MX6 has the secondary AXI bus to access on chip RAM.
    // Set both useXXXX  and useHostXXXX as 1 to enable 
    // corresponding secondary AXI IRAM.
    psecAxiIramInfo->useBitEnable = 0;
    psecAxiIramInfo->useIpEnable = 0;
    psecAxiIramInfo->useDbkYEnable = 0;
    psecAxiIramInfo->useDbkCEnable = 0;
    psecAxiIramInfo->useOvlEnable = 0;
    psecAxiIramInfo->useBtpEnable = 0;

    psecAxiIramInfo->useHostBitEnable = 0;
    psecAxiIramInfo->useHostIpEnable = 0;
    psecAxiIramInfo->useHostDbkYEnable = 0;
    psecAxiIramInfo->useHostDbkCEnable = 0;
    psecAxiIramInfo->useHostOvlEnable = 0;
    psecAxiIramInfo->useHostBtpEnable = 0;

    psecAxiIramInfo->bufBitUse = GET_IRAM(bDecode, BIT);
    psecAxiIramInfo->bufIpAcDcUse = GET_IRAM(bDecode, IP);
    psecAxiIramInfo->bufDbkYUse = GET_IRAM(bDecode, DBKY);
    psecAxiIramInfo->bufDbkCUse = GET_IRAM(bDecode, DBKC);
    psecAxiIramInfo->bufOvlUse = GET_IRAM(bDecode, OVL); 
    psecAxiIramInfo->bufBtpUse = 0;
}

//------------------------------------------------------------------------------
//
// Function: BSPVpuSetEncSearchRAM
//
// This method allocates the contiguously physical memory for encoder search RAM. 
// The size should be ((picWidth+15)&~15)*36+2048 at least, picWidth is supported 
// width of maximal resolution. It's 720P for i.MX6. And picWidth of searchramsize 
// must be a multiple of 16;
//
// Parameters:
//      pPhysAdd
//          [out] Pointer to the base physical address to allocated memory 
//
// Returns:
//      Size of allocated buffer.
//      -1 - Error
//                      
//
//------------------------------------------------------------------------------
int BSPVpuSetEncSearchRAM(PPhysicalAddress pPhysAdd)
{
    (*pPhysAdd) = (PhysicalAddress)VPU_ENC_SEARCH_IRAM_PA_START;
    return ((VPU_ENC_MAX_WIDTH + 15) & ~15) * 36 + 2048;
}

//------------------------------------------------------------------------------
//
// Function: BSPVpuGetUserDataBufferSize
//
// Gets the size of buffer to save user data from the registry. VPU decoder reports
// the user data to host application if this report feature is enabled. If the size
// of buffer is not big enough to save all user data after a frame decoding command, 
// VPU will user data to whole buffer and tell buffer full to host application.
//
// Parameters:
//      None.
//
// Returns:
//      Size of buffer to save user data. If the size is smaller than 0x10000, Driver
//      will use 0x10000 as default size.
//     -1 - Error
//                      
//
//------------------------------------------------------------------------------
int  BSPVpuGetUserDataBufferSize(void)
{
    LONG  error;
    HKEY  hKey;
    DWORD dwSize;
    int iUserDataSize = -1;

    // Open registry key for VPU driver
    error = RegOpenKeyEx(HKEY_LOCAL_MACHINE, VPU_DECODER_REG_PATH, 0 , 0, &hKey);
    if (error != ERROR_SUCCESS)
    {
        ERRORMSG(TRUE, (TEXT("BSPVpuGetUserDataBufferSize: Failed to open reg path:%s [Error:0x%x]\r\n"), VPU_DECODER_REG_PATH, error));
        goto _doneSize;
    }

    // Read the size of user data buffer
    dwSize = sizeof(iUserDataSize);
    error = RegQueryValueEx(hKey, VPU_DECODER_USERDATA_SIZE, NULL, NULL, (LPBYTE)&iUserDataSize, (LPDWORD)&dwSize);
    if (error != ERROR_SUCCESS)
    {
        ERRORMSG(TRUE, (TEXT("BSPVpuGetUserDataBufferSize: Failed to get user data buffer size, Error 0x%X\r\n"), error));
        goto _doneSize;
    }

    if (iUserDataSize < 0) // Make sure no negative value for successful operation
        iUserDataSize = 0;
    
_doneSize:
    // Close registry key
    RegCloseKey(hKey);

    return iUserDataSize;
}

//-------------------------------------------------------------------------------------------------------------------
//
// Function: BSPVpuClockGateMode
//
// This method gates clocks for VPU
//
//
// Parameters:
//      bTurnOn
//          [in] TRUE for gate on, FALSE for gate off
//
// Returns:
//     No return value
//
//-------------------------------------------------------------------------------------------------------------------
void BSPVpuClockGateMode(BOOL bTurnOn)
{
    BOOL initState;

    // VPU global uPowerFlag parameter
    EnterCriticalSection(&g_VpuArgCs);
    if (bTurnOn)
    {
        g_VPUGlobal.uPowerFlag ++;
        if (g_VPUGlobal.uPowerFlag == 1)
        {
            // Enable AXI bus clock and Decoding core clock
            initState = DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_VPU, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
            if (initState != TRUE)
            {
                ERRORMSG(TRUE, (TEXT("VPU AXI and Core clocks gating failed!\r\n")));
                goto EXIT;
            }
        }
    }
    else
    {
        if (g_VPUGlobal.uPowerFlag <= 0)
        {
            g_VPUGlobal.uPowerFlag = 0;
            // Disable AXI bus clock and Decoding core clock
            initState = DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_VPU, DDK_CLOCK_GATE_MODE_DISABLED);
            if (initState != TRUE)
            {
                ERRORMSG(TRUE, (TEXT("VPU AXI and Core clocks gating failed!\r\n")));
                goto EXIT;
            }
            goto EXIT;
        }
        g_VPUGlobal.uPowerFlag --;
        if (g_VPUGlobal.uPowerFlag == 0)
        {
            // Disable AXI bus clock and Decoding core clock
            initState = DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_VPU, DDK_CLOCK_GATE_MODE_DISABLED);
            if (initState != TRUE)
            {
                ERRORMSG(TRUE, (TEXT("VPU AXI and Core clocks gating failed!\r\n")));
                goto EXIT;
            }
        }
    }
EXIT:
    LeaveCriticalSection(&g_VpuArgCs);
}

void BSPVpuAutoClockGatingMode(BOOL bTurnOn)
{
    // VPU global uAutoClkFlag parameter
    EnterCriticalSection(&g_VpuArgCs);
    if (bTurnOn)
    {
        g_VPUGlobal.uAutoClkFlag --;
        if (g_VPUGlobal.uAutoClkFlag == 0)
        {
            DDKClockSetOverride(DDK_CLOCK_OVERRIDE_ENABLE_VPU, DDK_CLOCK_OVERRIDE_MODE_DISABLED);
        }
    }
    else
    {
        g_VPUGlobal.uAutoClkFlag ++;
        if (g_VPUGlobal.uAutoClkFlag == 1)
        {
            DDKClockSetOverride(DDK_CLOCK_OVERRIDE_ENABLE_VPU, DDK_CLOCK_OVERRIDE_MODE_ENABLED);
 
        }
    }
    LeaveCriticalSection(&g_VpuArgCs);
}


