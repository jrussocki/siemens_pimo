//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//
//------------------------------------------------------------------------------
//
//  Copyright (C) 2008 -2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  bspacc.h
//
//  Provides prototypes for BSP-specific accelerometer routines to communicate via I2C.
//
//------------------------------------------------------------------------------

#ifndef __BSPACC_H__
#define __BSPACC_H__

//------------------------------------------------------------------------------
// Defines

// MMA8451 Register Address Definition
#define MMA8451_STATUS                0x00      // Status or FIFO Status Register
#define MMA8451_F_STATUS              0x00      // Status or FIFO Status Register
#define MMA8451_OUT_X_MSB             0x01
#define MMA8451_OUT_X_LSB             0x02
#define MMA8451_OUT_Y_MSB             0x03
#define MMA8451_OUT_Y_LSB             0x04
#define MMA8451_OUT_Z_MSB             0x05
#define MMA8451_OUT_Z_LSB             0x06
#define MMA8451_F_SETUP               0x09
#define MMA8451_TRIG_CFG              0x0A
#define MMA8451_SYSMOD                0x0B
#define MMA8451_INT_SOURCE            0x0C
#define MMA8451_WHO_AM_I              0x0D
#define MMA8451_XYZ_DATA_CFG          0x0E      // Mode control
#define MMA8451_HP_FILTER_CUTOFF      0x0F
#define MMA8451_PL_STATUS             0x10
#define MMA8451_PL_CFG                0x11
#define MMA8451_PL_COUNT              0x12
#define MMA8451_PL_BF_ZCOMP           0x13
#define MMA8451_PL_THS_REG            0x14
#define MMA8451_FF_MT_CFG             0x15
#define MMA8451_FF_MT_SRC             0x16
#define MMA8451_FF_MT_THS             0x17
#define MMA8451_FF_MT_COUNT           0x18
#define MMA8451_TRANSIENT_CFG         0x1D
#define MMA8451_TRANSIENT_SRC         0x1E
#define MMA8451_TRANSIENT_THS         0x1F
#define MMA8451_TRANSIENT_COUNT       0x20
#define MMA8451_PULSE_CFG             0x21
#define MMA8451_PULSE_SRC             0x22
#define MMA8451_PULSE_THSX            0x23
#define MMA8451_PULSE_THSY            0x24
#define MMA8451_PULSE_THSZ            0x25
#define MMA8451_PULSE_TMLT            0x26
#define MMA8451_PULSE_LTCY            0x27
#define MMA8451_PULSE_WIND            0x28
#define MMA8451_ASLP_COUNT            0x29
#define MMA8451_CTRL_REG1             0x2A
#define MMA8451_CTRL_REG2             0x2B
#define MMA8451_CTRL_REG3             0x2C
#define MMA8451_CTRL_REG4             0x2D
#define MMA8451_CTRL_REG5             0x2E
#define MMA8451_OFF_X                 0x2F
#define MMA8451_OFF_Y                 0x30
#define MMA8451_OFF_Z                 0x31
#define MMA8451_REG_END               0x32

// Status0 reg                       
#define MMA8451_STATUS_ZYXOW          0x80
#define MMA8451_STATUS_XOW            0x40
#define MMA8451_STATUS_YOW            0x20
#define MMA8451_STATUS_ZOW            0x10
#define MMA8451_STATUS_ZYXDR          0x08
#define MMA8451_STATUS_ZDR            0x04
#define MMA8451_STATUS_YDR            0x02
#define MMA8451_STATUS_XDR            0x01
#define MMA8451_STATUS_ZYXOW_MASK     0x80
#define MMA8451_STATUS_XOW_MASK       0x40
#define MMA8451_STATUS_YOW_MASK       0x20
#define MMA8451_STATUS_ZOW_MASK       0x10
#define MMA8451_STATUS_ZYXDR_MASK     0x08
#define MMA8451_STATUS_ZDR_MASK       0x04
#define MMA8451_STATUS_YDR_MASK       0x02
#define MMA8451_STATUS_XDR_MASK       0x01
// XYZ_DATA_CFG reg
#define MMA8451_XYZDATACFG_HPFOUT_LSH 4
#define MMA8451_XYZDATACFG_FS_MASK    0x3
#define MMA8451_XYZDATACFG_HPFOUT_MASK  0x10
#define MMA8451_XYZDATACFG_FS(x)      (x&MMA8451_XYZDATACFG_FS_MASK)
#define MMA8451_XYZDATACFG_HPFOUT(x)  ((x<<MMA8451_XYZDATACFG_HPFOUT_LSH)&MMA8451_XYZDATACFG_HPFOUT_MASK)
// PL_STATUS reg
#define MMA8451_PLSTATUS_NEWLP_LSH    7
#define MMA8451_PLSTATUS_LO_LSH       6
#define MMA8451_PLSTATUS_LAPO_LSH     1
#define MMA8451_PLSTATUS_BAFRO_LSH    0
#define MMA8451_PLSTATUS_NEWLP_MASK    0x80
#define MMA8451_PLSTATUS_LO_MASK       0x40
#define MMA8451_PLSTATUS_LAPO_MASK    0x06
#define MMA8451_PLSTATUS_BAFRO_MASK   0x01
// PL_CFG reg
#define MMA8451_PLCFG_BDCNTM_LSH      7
#define MMA8451_PLCFG_PLEN_LSH        6
#define MMA8451_PLCFG_BDCNTM_MASK     0x80
#define MMA8451_PLCFG_PLEN_MASK       0x40
#define MMA8451_PLCFG_BDCNTM(x)       ((x<<MMA8451_PLCFG_BDCNTM_LSH)&MMA8451_PLCFG_BDCNTM_MASK)
#define MMA8451_PLCFG_PLEN(x)         ((x<<MMA8451_PLCFG_PLEN_LSH)&MMA8451_PLCFG_PLEN_MASK)
// PL_BF_ZCOMP reg
#define MMA8451_PLBFZCOMP_ZLOCK_LSH   0
#define MMA8451_PLBFZCOMP_BKFR_LSH    6
#define MMA8451_PLBFZCOMP_ZLOCK_MASK  0x07
#define MMA8451_PLBFZCOMP_BKFR_MASK   0xC0
#define MMA8451_PLBFZCOMP_ZLOCK(x)    ((x<<MMA8451_PLBFZCOMP_ZLOCK_LSH)&MMA8451_PLBFZCOMP_ZLOCK_MASK)
#define MMA8451_PLBFZCOMP_BKFR(x)     ((x<<MMA8451_PLBFZCOMP_BKFR_LSH)&MMA8451_PLBFZCOMP_BKFR_MASK)
// PL_THS reg
#define MMA8451_PLTHS_HYS_LSH         0
#define MMA8451_PLTHS_PLTHS_LSH       3
#define MMA8451_PLTHS_PLTHS_MASK      0xF8
#define MMA8451_PLTHS_HYS_MASK        0x07
#define MMA8451_PLTHS_HYS(x)          ((x<<MMA8451_PLTHS_HYS_LSH)&MMA8451_PLTHS_HYS_MASK)
#define MMA8451_PLTHS_PLTHS(x)        ((x<<MMA8451_PLTHS_PLTHS_LSH)&MMA8451_PLTHS_PLTHS_MASK)
// Ctrl1 reg
#define MMA8451_CTRLREG1_DR_LSH       3
#define MMA8451_CTRLREG1_DR_MASK      0x38
#define MMA8451_CTRLREG1_ACTIVE_MASK  0x01
#define MMA8451_CTRLREG1_DR(x)        ((x<<3)&MMA8451_CTRLREG1_DR_MASK)
// CTRL2 reg
#define MMA8451_CTRLREG2_RST          0x40
#define MMA8451_CTRLREG2_ST_LSH       7
#define MMA8451_CTRLREG2_RST_LSH      6
#define MMA8451_CTRLREG2_SLPE_LSH     2
#define MMA8451_CTRLREG2_MODS_LSH     0
#define MMA8451_CTRLREG2_ST_MASK      0x80
#define MMA8451_CTRLREG2_RST_MASK     0x80
#define MMA8451_CTRLREG2_SLPE_MASK    0x04
#define MMA8451_CTRLREG2_MODS_MASK    0x03
#define MMA8451_CTRLREG2_ST(x)        ((x<<MMA8451_CTRLREG2_ST_LSH)&MMA8451_CTRLREG2_ST_MASK)
#define MMA8451_CTRLREG2_SLPE(x)      ((x<<MMA8451_CTRLREG2_SLPE_LSH)&MMA8451_CTRLREG2_SLPE_MASK)
#define MMA8451_CTRLREG2_MODS(x)      ((x<<MMA8451_CTRLREG2_MODS_LSH)&MMA8451_CTRLREG2_MODS_MASK)
// CTRL3 reg                          
#define MMA8451_CTRLREG3_INTR_LSH     0
#define MMA8451_CTRLREG3_INTR_MASK    0x3
#define MMA8451_CTRLREG3_INTR(x)      ((x<<MMA8451_CTRLREG3_INTR_LSH)&MMA8451_CTRLREG3_INTR_MASK)


//------------------------------------------------------------------------------
// Functions
//------------------------------------------------------------------------------
DWORD BSPACCGetIRQ1(void);
DWORD BSPACCGetIRQ2(void);
bool BSPClearGPIOIntr(INT8 index);
bool BSPACCIOMUXConfig();
bool BSPACCPowerUp(void);
bool BSPACCPowerDown(void);
bool BSPEnableACC(void);
bool BSPDisableACC(void);
bool BSPACCCalibration(INT8 byGSel);
bool BSPACCSetMode(ACC_MODE byMode);
bool BSPACCSetStandby(void);
bool BSPACCSetGSel(INT8 byGSel);
bool BSPACCSetDR(INT8 bDR);
bool BSPACCSetPLAngle(INT8 iAngle);
bool BSPACCSetLPHysteresis(INT8 iAngle);
bool BSPACCSetPLEN(bool bEnable);
bool BSPACCSetPLCOUNT(INT8 bGOFF);
bool BSPACCSetPLBFAngle(INT8 bBKFR);
bool BSPACCSetPLZLOCK(INT8 bZLOCK);
bool BSPACCAutoSleep(INT8 bTRUE);
bool BSPACCLowPowerMode(INT8 bTRUE);
bool BSPACCSetHPF(bool bEnable);
bool BSPACCSetXOffset(INT8 iXOffset);
bool BSPACCSetYOffset(INT8 iYOffset);
bool BSPACCSetZOffset(INT8 iZOffset);
bool BSPACCSetOffset(INT8 iXOffset, INT8 iYOffset, INT8 iZOffset);
bool BSPACCSetIntrPin(INT8 bIntr);
bool BSPACCSetIntrPinRout(INT8 bINTn);
bool BSPACCSetIntrSource(INT8 bIntrSource);
bool BSPACCClearIntrSource(INT8 bIntrSource);
bool BSPACCGetIntrStatus(INT8* byStatus);
bool BSPACCGetIntrSource(INT8* pbySource);
bool BSPACCGetStatus(INT8* byStatus);
bool BSPACCGetOutput8bit(INT8* iXOutput, INT8* iYOutput, INT8* iZOutput);
bool BSPACCGetOutput14bit(INT16* iXOutput, INT16* iYOutput, INT16* iZOutput);
bool BSPACCGetXOffset(INT8* piXOffset);
bool BSPACCGetYOffset(INT8* piYOffset);
bool BSPACCGetZOffset(INT8* piZOffset);
bool BSPACCGetOffset(INT8* iXOffset, INT8* iYOffset, INT8* iZOffset);
bool BSPACCGetMode(INT8* pbyMode);
bool BSPACCGetDR(INT8* pbDR);
bool BSPACCGetGSel(INT8* pbyGSel);
bool BSPACCGetPLAngle(INT8* pAngle);
bool BSPACCGetPLStatus(INT8* mPLOrientatiaon, INT8* mFBOrientation);
INT8 BSPACCWHOAMI(void);

VOID I2CWriteOneByte(HANDLE hI2C, BYTE byAddr, BYTE byReg, BYTE byData, LPINT lpiResult);
INT8 I2CReadOneByte(HANDLE hI2C, BYTE byAddr, BYTE byReg, LPINT lpiResult);
VOID I2CWriteTwoBytes(HANDLE hI2C, BYTE byAddr, BYTE byReg, BYTE byData1, BYTE byData2, LPINT lpiResult);
WORD I2CReadTwoBytes(HANDLE hI2C, BYTE byAddr, BYTE byReg, LPINT lpiResult);
VOID I2CWriteMultiBytes(HANDLE hI2C, BYTE byAddr, BYTE byReg, BYTE* pbyData, BYTE byDataNum, LPINT lpiResult);
void I2CReadMultiBytes(HANDLE hI2C, BYTE byAddr, BYTE byReg, BYTE* pbyData, BYTE byDataNum, LPINT lpiResult);
bool BSPACC_I2COpen(VOID);
bool BSPACC_I2CClose(VOID);

#endif //__BSPACC_H__
