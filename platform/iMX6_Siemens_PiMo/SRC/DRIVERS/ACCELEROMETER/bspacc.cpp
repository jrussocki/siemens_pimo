//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//
//------------------------------------------------------------------------------
//
//  Copyright (C) 2008 -2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  bspacc.c
//
//  Provides BSP-specific configuration routines for the accelerometer MMA8451Q.
//
//------------------------------------------------------------------------------
#include <windows.h>
#pragma warning(push)
#pragma warning(disable: 4005)
#include "bsp.h"
#pragma warning(pop)

#include "i2cbus.h"
#include <ceddk.h>
#include <devload.h>
#include <NKIntr.h>
#include "acc.h"
#include "bspacc.h"


//------------------------------------------------------------------------------
// External Functions


//------------------------------------------------------------------------------
// External Variables
extern ACC_MODE g_eMode;


//------------------------------------------------------------------------------
// Defines
#define BSP_ACC_FUNCTION_ENTRY() \
    DEBUGMSG(TRUE, (TEXT("++%s\r\n"), __WFUNCTION__))
#define BSP_ACC_FUNCTION_EXIT() \
    DEBUGMSG(TRUE, (TEXT("--%s\r\n"), __WFUNCTION__))

// SAO=0(lsb); i2c_address=0x1c(7-bit)/0x38(8-bit)
#define ACC_IIC_ADDRESS        0x1C
#define ACC_IIC_SPEED          100000
#define MMA8451Q_Version       0x1A
//------------------------------------------------------------------------------
// Types


//------------------------------------------------------------------------------
// Global Variables

// static Variables
static HANDLE s_hACCI2C = NULL;


//------------------------------------------------------------------------------
// Local Variables


//------------------------------------------------------------------------------
// Local Functions
bool BSPACCSetIntrPin(INT8 bIntr);
bool BSPACCSetIntrPinRout(INT8 bINTn);

//-----------------------------------------------------------------------------
//
// Function:  BSPACCGetIRQ1
//
// This function get IRQ1 for the ACC.
//
// Parameters:
//      None.
//
// Returns:
//      IRQ number.
//
//-----------------------------------------------------------------------------
DWORD BSPACCGetIRQ1(void)
{
    return (DWORD)IRQ_GPIO1_PIN18;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCGetIRQ2
//
// This function get IRQ2 for the ACC.
//
// Parameters:
//      None.
//
// Returns:
//      IRQ number.
//
//-----------------------------------------------------------------------------
DWORD BSPACCGetIRQ2(void)
{
    return (DWORD)IRQ_GPIO1_PIN18;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCIOMUXConfig
//
// This function makes the DDK call to configure the IOMUX
// pins required for the ACC.
//
// Parameters:
//      None.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCIOMUXConfig()
{
    BSP_ACC_FUNCTION_ENTRY();

    bool retVal = TRUE;

	// TC IO MUX removed because the configuration is made in the bootloader

    DDKGpioSetConfig(DDK_GPIO_PORT1, 18, DDK_GPIO_DIR_IN, DDK_GPIO_INTR_FALL_EDGE);
    DDKGpioClearIntrPin(DDK_GPIO_PORT1, 18);

	/* SENSOR_PWR_EN is enabled */
	DDKGpioSetConfig(DDK_GPIO_PORT2, 31, DDK_GPIO_DIR_OUT, DDK_GPIO_INTR_NONE);
    DDKGpioWriteDataPin(DDK_GPIO_PORT2, 31, 1);

    BSP_ACC_FUNCTION_EXIT();

    return retVal;
}

//-----------------------------------------------------------------------------
//
// Function: BSPACC_I2COpen
//
// This function open I2C for read/write operation.
//
// Parameters:
//      None.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACC_I2COpen(VOID)
{
    DWORD dwFrequency = ACC_IIC_SPEED;

    s_hACCI2C = I2COpenHandle(I2C1_FID);

    if (s_hACCI2C == INVALID_HANDLE_VALUE)
    {
        return FALSE;
    }

    if (!I2CSetMasterMode(s_hACCI2C))
    {
        CloseHandle(s_hACCI2C);
        return FALSE;
    }

    // Initialize the device internal fields
    if (!I2CSetFrequency(s_hACCI2C, dwFrequency))
    {
        CloseHandle(s_hACCI2C);
        return FALSE;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function: BSPACC_I2CClose
//
// This function close I2C for read/write operation.
//
// Parameters:
//      None.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACC_I2CClose(VOID)
{
    CloseHandle(s_hACCI2C);
    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function: BSPACCPowerUp
//
// This function power up accelerometer.
//
// Parameters:
//      None.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCPowerUp(void)
{

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function: BSPACCPowerDown
//
// This function put accelerometer into powerdown mode.
//
// Parameters:
//      None.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCPowerDown(void)
{

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPClearGPIOIntr
//
// This function clear GPIO interrupt
//
// Parameters:
//      hIntr
//          [in] interrupt which should be clear
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPClearGPIOIntr(INT8 index)
{
    UNREFERENCED_PARAMETER(index);

    // interrupt 1
    DDKGpioClearIntrPin(DDK_GPIO_PORT1, 18);
    // interrupt 2 (unused)
    //DDKGpioClearIntrPin(DDK_GPIO_PORT6, 16);

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCReset
//
// This function reset accelerometer
//
// Parameters:
//      None.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCReset()
{
    INT8  bACCCtrl2Reg;
    INT32 iResult;

    bACCCtrl2Reg = MMA8451_CTRLREG2_RST;
    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG2, bACCCtrl2Reg, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        ERRORMSG(TRUE, (TEXT("BSPACCReset-->I2CWriteOneByte failed!\r\n")));
        return FALSE;
    }

    do{
        Sleep(10);
        bACCCtrl2Reg = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG2, &iResult);
        if( iResult != I2C_NO_ERROR )
        {
            ERRORMSG(TRUE, (TEXT("BSPACCReset-->I2CReadOneByte failed %d!\r\n"),iResult));
            return FALSE;
        }
    }while(bACCCtrl2Reg&MMA8451_CTRLREG2_RST);

    return TRUE;
}

//------------------------------------------------------------------------------
//
// Function: BSPEnableACC
//
// This function enable the ACC sensor module.
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
bool BSPEnableACC(void)
{
    BSP_ACC_FUNCTION_ENTRY();

    if(!BSPACCIOMUXConfig())
    {
        ERRORMSG(TRUE, (TEXT("BSPACCIOMUXConfig failed!\r\n")));
        return FALSE;
    }

    if(!BSPACC_I2COpen())
    {
        ERRORMSG(TRUE, (TEXT("BSPACC_I2COpen failed!\r\n")));
        return FALSE;
    }

    BSPACCPowerUp();

    // Workaround: Fix first acess accelerometer fai
    UINT8 readID;
    UINT8 cnt = 0;
    do {
        readID = (BSPACCWHOAMI()&0xFF);
    } while (readID == 0 && cnt < 2);

    if(MMA8451Q_Version != readID)
    {
        ERRORMSG(TRUE, (TEXT("Failed to access MMA8451Q!!! ID = %x\r\n"),BSPACCWHOAMI()));
        return FALSE;
    }

    // reset accelerometer
    BSPACCReset();

    // Rout interrupt to INT1 pin
    BSPACCSetIntrPinRout(0x0);

    // Interrupt pin active low and open drain
    BSPACCSetIntrPin(0x01);

    BSP_ACC_FUNCTION_EXIT();

    return TRUE;
}

//------------------------------------------------------------------------------
//
// Function: BSPDisableACC
//
// This function disable the ACC sensor module.
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//------------------------------------------------------------------------------
bool BSPDisableACC(void)
{
    BSP_ACC_FUNCTION_ENTRY();

    if(!BSPACC_I2CClose())
    {
        return FALSE;
    }

    BSPACCPowerDown();

    BSP_ACC_FUNCTION_EXIT();

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCCalibration
//
// This function is used to calibrate the offset of acc.
// Note: Calibration is done only when accelerometer starts
//
// Parameters:
//      byGSel
//          [in] accelerometer g-sel.
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCCalibration(INT8 byGSel)
{
    INT32 iResult;
    INT8  bACCModeReg;
    INT8  bACCOldModeReg;
    INT8  bACCOldDR;
    INT8  bStatus;
    INT8  bInrSource;
    INT16 iXOutput;
    INT16 iYOutput;
    INT16 iZOutput;
    INT16 iXOffset;
    INT16 iYOffset;
    INT16 iZOffset;
    INT16 iZTargetOffset;
    bool  bRet;

    UNREFERENCED_PARAMETER(byGSel);
    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    BSPACCPowerUp();
    
    bACCModeReg = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG1, &iResult);
    bACCOldModeReg = bACCModeReg;
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }
    // put accelerometer into standby mode
    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG1, bACCModeReg&~MMA8451_CTRLREG1_ACTIVE_MASK, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    if(!BSPACCGetDR(&bACCOldDR))
    {
        return FALSE;
    }

    // Date Rate is 1.563Hz
    if(!BSPACCSetDR(ACC_DR_1P56HZ))
    {
        return FALSE;
    }

    // Disable DRDY interrupt
    if(!BSPACCGetIntrSource(&bInrSource))
    {
        return FALSE;
    }
    else if(!BSPACCClearIntrSource(ACC_INTREN_DRDY))
    {
        return FALSE;
    }

    // put accelerometer into 8g mode
    if(!BSPACCSetGSel(ACC_GSEL_8G))
    {
        return FALSE;
    }

    // get status util data is ready
    bRet = BSPACCGetStatus(&bStatus);

    while(bRet && ((bStatus & MMA8451_STATUS_ZYXDR_MASK) != MMA8451_STATUS_ZYXDR))
    {
        Sleep(8);
        bRet = BSPACCGetStatus(&bStatus);
    }

    if(!bRet)
    {
        ERRORMSG(TRUE, (TEXT("BSPACCCalibration-->BSPACCGetStatus failed!\r\n")));
        return FALSE;
    }

    bRet = BSPACCGetOutput14bit(&iXOutput, &iYOutput, &iZOutput);
    if(!bRet)
    {
        ERRORMSG(TRUE, (TEXT("BSPACCCalibration-->BSPACCGetOutput failed!\r\n")));
        return FALSE;
    }
    // set offset to calibrate acc.(0g,0g,-1g)
#if 0
    if(byGSel == 1) //2G
    {
        iZTargetOffset = 0x100;
        iXOffset = (0 - (INT16)iXOutput)>>2;
        iYOffset = (0 - (INT16)iYOutput)>>2;
        iZOffset = (iZTargetOffset - (INT16)iZOutput)>>2;
    }
    else if(byGSel ==2) //4G
    {
        iZTargetOffset = 0x200;
        iXOffset = (0 - (INT16)iXOutput)>>1;
        iYOffset = (0 - (INT16)iYOutput)>>1;
        iZOffset = (iZTargetOffset - (INT16)iZOutput)>>1;
    }
    else //8G
    {
        iZTargetOffset = 0x400;
        iXOffset = 0 - (INT16)iXOutput;
        iYOffset = 0 - (INT16)iYOutput;
        iZOffset = iZTargetOffset - (INT16)iZOutput;
    }
#endif
    iZTargetOffset = 0x400;
    iXOffset = (0 - (INT16)iXOutput) / 2;
    iYOffset = (0 - (INT16)iYOutput) / 2;
    iZOffset = (iZTargetOffset - (INT16)iZOutput) /2;

    // put accelerometer into standby mode
    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG1, bACCOldModeReg&~MMA8451_CTRLREG1_ACTIVE_MASK, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    // Only when offset registers value is zero, calibiration is valid
    bRet = BSPACCSetOffset(0, 0, 0);
    if(!bRet)
    {
        ERRORMSG(TRUE, (TEXT("BSPACCCalibration-->BSPACCSetOffset failed!\r\n")));
        return FALSE;
    }

    bRet = BSPACCSetOffset((INT8)iXOffset, (INT8)iYOffset, (INT8)iZOffset);
    if(!bRet)
    {
        ERRORMSG(TRUE, (TEXT("BSPACCCalibration-->BSPACCSetOffset failed!\r\n")));
        return FALSE;
    }

    // recover Date Rate
    if(!BSPACCSetDR(bACCOldDR))
    {
        return FALSE;
    }

    // recover DRDY interrupt
    if(bInrSource&(1<<(INT8)ACC_INTREN_DRDY))
    {
        if(!BSPACCSetIntrSource(ACC_INTREN_DRDY))
        {
            return FALSE;
        }
    }

    // recover g mode
    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG1, bACCOldModeReg, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCSetMode
//
// This function is used to set mode for acc through I2C.
//
// Parameters:
//      byGSel
//          [in] accelerometer g-sel.
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCSetMode(ACC_MODE byMode)
{
    INT32 iResult;
    BYTE  bACCModeReg;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(byMode == ACC_MODE_STANDBY)
    {
        bACCModeReg = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG1, &iResult);
        if( iResult != I2C_NO_ERROR )
        {
            return FALSE;
        }

        bACCModeReg = ((bACCModeReg&~MMA8451_CTRLREG1_ACTIVE_MASK)|(INT8)byMode);
        // Set standby mode
        I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG1, bACCModeReg, &iResult);
        if( iResult != I2C_NO_ERROR )
        {
            return FALSE;
        }

        // Disable streaming mode and orientation mode
        BSPACCSetPLEN(FALSE);
        // Clear all interrupt source
        I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG4, 0xFF, &iResult);
    }
    else if(byMode == ACC_MODE_ORIENTATION_DETECT)
    {
        BSPACCSetPLEN(TRUE);
        BSPACCSetIntrSource((INT8)ACC_INTREN_LNDPRT);
    }
    else if(byMode == ACC_MODE_STREAMING)
    {
        BSPACCSetIntrSource((INT8)ACC_INTREN_DRDY);
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCSetStandby
//
// This function is used to set standby mode for acc through I2C.
//
// Parameters:
//      None.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCSetStandby(void)
{
    INT32 iResult;
    INT8  bACCModeReg;
    
    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    bACCModeReg = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG1, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    // put accelerometer into standby mode
    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG1, bACCModeReg&~MMA8451_CTRLREG1_ACTIVE_MASK, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCSetActive
//
// This function is used to set active mode for acc through I2C.
//
// Parameters:
//      None.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCSetActive(void)
{
    INT32 iResult;
    INT8  bACCModeReg;
    
    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(g_eMode == ACC_MODE_STANDBY)
    {
        return FALSE;
    }

    bACCModeReg = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG1, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    // put accelerometer into standby mode
    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG1, bACCModeReg|MMA8451_CTRLREG1_ACTIVE_MASK, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCSetGSel
//
// This function is used to set g-sel for acc through I2C.
//
// Parameters:
//      byGSel
//          [in] accelerometer g-sel.
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCSetGSel(INT8 byGSel)
{
    INT32 iResult;
    INT8  bACCCTLReg1, bACCXYZCFGReg, bACCRange;
    
    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    bACCCTLReg1 = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG1, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    bACCXYZCFGReg = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_XYZ_DATA_CFG, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    // put accelerometer into standby mode
    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG1, bACCCTLReg1&~MMA8451_CTRLREG1_ACTIVE_MASK, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    bACCRange = MMA8451_XYZDATACFG_FS(byGSel);

    bACCXYZCFGReg = ((bACCXYZCFGReg&~MMA8451_XYZDATACFG_FS_MASK)|bACCRange);
    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_XYZ_DATA_CFG, bACCXYZCFGReg, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG1, bACCCTLReg1, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCSetDR
//
// This function is used to set data output rate.
//
// Parameters:
//      bDR
//          [in] data rate selection.
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCSetDR(INT8 bDR)
{
    INT32 iResult;
    INT8  bACCCTL1Reg;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(!BSPACCSetStandby())
    {
        return FALSE;
    }

    bACCCTL1Reg = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG1, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    bACCCTL1Reg = ((bACCCTL1Reg&~MMA8451_CTRLREG1_DR_MASK)|MMA8451_CTRLREG1_DR(bDR));
    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG1, bACCCTL1Reg, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    if(!BSPACCSetActive())
    {
        return FALSE;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCSetPLAngle
//
// This function is used to set the trip threthrold angle for the image transition between portrait 
//  and landscape orietation through I2C.
//
// Parameters:
//      iAngle
//          [in] the trip angle
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCSetPLAngle(INT8 iAngle)
{
    INT32 iResult;
    UINT8 bValue1, bValue2;
    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(!BSPACCSetStandby())
    {
        return FALSE;
    }

    switch(iAngle)
    {
    case 15:
        bValue1 = 0x07;
        break;
    case 20:
        bValue1 = 0x09;
        break;
    case 30:
        bValue1 = 0x0C;
        break;
    case 35:
        bValue1 = 0x0D;
        break;
    case 40:
        bValue1 = 0x0F;
        break;
    case 45:
        bValue1 = 0x10;
        break;
    case 55:
        bValue1 = 0x13;
        break;
    case 60:
        bValue1 = 0x14;
        break;
    case 70:
        bValue1 = 0x17;
        break;
    case 75:
        bValue1 = 0x19;
        break;
    default:
        bValue1 = 0x10;
        break;
    }
    DEBUGMSG(TRUE, (TEXT("BSPACCSetPLAngle Angle = %d (%x)\r\n"), iAngle, bValue1));

    bValue2 = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_PL_THS_REG, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }
    bValue2 &= ~MMA8451_PLTHS_PLTHS_MASK;
    bValue1 = MMA8451_PLTHS_PLTHS(bValue1);

    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_PL_THS_REG, (bValue1 | bValue2), &iResult);
    if(iResult != I2C_NO_ERROR)
    {
        return FALSE;
    }

    if(!BSPACCSetActive())
    {
        return FALSE;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCSetPLHysteresis
//
// This function is used to set the Hysteresis angle for the image transition between portrait 
//  and landscape orietation through I2C.
//
// Parameters:
//      iAngle
//          [in] the trip angle
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCSetPLHysteresis(INT8 iAngle)
{
    INT32 iResult;
    UINT8 bValue1, bValue2;
    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(!BSPACCSetStandby())
    {
        return FALSE;
    }

    switch(iAngle)
    {
    case 0:
        bValue1 = 0;
        break;
    case 4:
        bValue1 = 1;
        break;
    case 7:
        bValue1 = 2;
        break;
    case 11:
        bValue1 = 3;
        break;
    case 14:
        bValue1 = 4;
        break;
    case 17:
        bValue1 = 5;
        break;
    case 21:
        bValue1 = 6;
        break;
    case 24:
        bValue1 = 7;
        break;
    default:
        bValue1 = 4;
        break;
    }
    DEBUGMSG(TRUE, (TEXT("BSPACCSetPLHysteresis Angle = %d (%x)\r\n"), iAngle, bValue1));

    bValue2 = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_PL_THS_REG, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }
    bValue2 &= ~MMA8451_PLTHS_HYS_MASK;
    bValue1 = MMA8451_PLTHS_HYS(bValue1);
    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_PL_THS_REG, (bValue1 | bValue2), &iResult);
    if(iResult != I2C_NO_ERROR)
    {
        return FALSE;
    }

    if(!BSPACCSetActive())
    {
        return FALSE;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCSetPLEN
//
// This function is used to enable Portrait_Landscape Detection through I2C.
//
// Parameters:
//      bEnable
//          [in]  Enable or Disable detection function
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCSetPLEN(bool bEnable)
{
    INT32 iResult;
    INT8  bACCPLCFGReg;
    INT8  bPLEN = bEnable?1:0;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(!BSPACCSetStandby())
    {
        return FALSE;
    }

    bACCPLCFGReg = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_PL_CFG, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    bACCPLCFGReg = ((bACCPLCFGReg&~MMA8451_PLCFG_PLEN_MASK)|MMA8451_PLCFG_PLEN(bPLEN));
    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_PL_CFG, bACCPLCFGReg, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    if(!BSPACCSetActive())
    {
        return FALSE;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCSetPLCOUNT
//
// This function is used to set the debounce count for the orientation state transition through I2C.
//
// Parameters:
//      bGOFF   
//          [in] the debounce count
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCSetPLCOUNT(INT8 bGOFF)
{
    INT32 iResult;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(!BSPACCSetStandby())
    {
        return FALSE;
    }

    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_PL_COUNT, bGOFF, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    if(!BSPACCSetActive())
    {
        return FALSE;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCSetPLBFAngle
//
// This function is used to set the Back to Front trip angle threshold through I2C.
//
// Parameters:
//      bGOFF   
//          [in] the Angle step number
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCSetPLBFAngle(INT8 bBKFR)
{
    INT32 iResult;
    INT8  bACCPLBFZCOMPReg;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(!BSPACCSetStandby())
    {
        return FALSE;
    }

    bACCPLBFZCOMPReg = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_PL_BF_ZCOMP, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    bACCPLBFZCOMPReg = ((bACCPLBFZCOMPReg&~MMA8451_PLBFZCOMP_BKFR_MASK)|MMA8451_PLBFZCOMP_BKFR(bBKFR));
    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_PL_BF_ZCOMP, bACCPLBFZCOMPReg, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    if(!BSPACCSetActive())
    {
        return FALSE;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCSetPLZLOCK
//
// This function is used to set the Z-Tilt angel compensation to adjust the Z-Lockout region 
//  from 14" to 23"  through I2C.
//
// Parameters:
//      bZLOCK   
//          [in] the Angle step number
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCSetPLZLOCK(INT8 bZLOCK)
{
    INT32 iResult;
    INT8  bACCPLBFZCOMPReg;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(!BSPACCSetStandby())
    {
        return FALSE;
    }

    bACCPLBFZCOMPReg = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_PL_BF_ZCOMP, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    bACCPLBFZCOMPReg = ((bACCPLBFZCOMPReg&~MMA8451_PLBFZCOMP_ZLOCK_MASK)|MMA8451_PLBFZCOMP_ZLOCK(bZLOCK));
    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_PL_BF_ZCOMP, bACCPLBFZCOMPReg, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    if(!BSPACCSetActive())
    {
        return FALSE;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCAutoSleep
//
// This function is used to enable auto sleep function through I2C.
//
// Parameters:
//      bTRUE   
//          [in] enable or disable auto sleep mode
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCAutoSleep(INT8 bTRUE)
{
    INT32 iResult;
    INT8  bACCCTRLReg2;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(!BSPACCSetStandby())
    {
        return FALSE;
    }

    bACCCTRLReg2 = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG2, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    bACCCTRLReg2 = ((bACCCTRLReg2&~MMA8451_CTRLREG2_SLPE_MASK)|MMA8451_CTRLREG2_SLPE(bTRUE));
    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG2, bACCCTRLReg2, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    if(!BSPACCSetActive())
    {
        return FALSE;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCLowPowerMode
//
// This function is used to set low power mode on active mode through I2C.
//
// Parameters:
//      bTRUE   
//          [in] set low power mode
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCLowPowerMode(INT8 bTRUE)
{
    INT32 iResult;
    INT8  bACCCTRLReg2;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(!BSPACCSetStandby())
    {
        return FALSE;
    }

    bACCCTRLReg2 = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG2, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    bACCCTRLReg2 = ((bACCCTRLReg2&~MMA8451_CTRLREG2_MODS_MASK)|MMA8451_CTRLREG2_MODS(bTRUE));
    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG2, bACCCTRLReg2, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    if(!BSPACCSetActive())
    {
        return FALSE;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCSetHPF
//
// This function is used to enable high pass data output data out through I2C.
//
// Parameters:
//      bEnable
//          [in]  Enable or Disable this function
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCSetHPF(bool bEnable)
{
    INT32 iResult;
    INT8  bACCXYZCFGReg;
    INT8  bDATAOUT = bEnable?1:0;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(!BSPACCSetStandby())
    {
        return FALSE;
    }

    bACCXYZCFGReg = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_XYZ_DATA_CFG, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    bACCXYZCFGReg = ((bACCXYZCFGReg&~MMA8451_XYZDATACFG_HPFOUT_MASK)|MMA8451_XYZDATACFG_HPFOUT(bDATAOUT));
    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_XYZ_DATA_CFG, bACCXYZCFGReg, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    if(!BSPACCSetActive())
    {
        return FALSE;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCSetXOffset
//
// This function is used to set x-axis offset for acc through I2C.
//
// Parameters:
//      iXOffset
//          [in] x-axis offset
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCSetXOffset(INT8 iXOffset)
{
    INT32 iResult;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(!BSPACCSetStandby())
    {
        return FALSE;
    }

    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_OFF_X, iXOffset, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    if(!BSPACCSetActive())
    {
        return FALSE;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCSetYOffset
//
// This function is used to set y-axis offset for acc through I2C.
//
// Parameters:
//      iYOffset
//          [in] y-axis offset
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCSetYOffset(INT8 iYOffset)
{
    INT32 iResult;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(!BSPACCSetStandby())
    {
        return FALSE;
    }

    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_OFF_Y, iYOffset,  &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    if(!BSPACCSetActive())
    {
        return FALSE;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCSetZOffset
//
// This function is used to set z-axis offset for acc through I2C.
//
// Parameters:
//      iXOffset
//          [in] z-axis offset
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCSetZOffset(INT8 iZOffset)
{
    INT32 iResult;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(!BSPACCSetStandby())
    {
        return FALSE;
    }

    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_OFF_Z, iZOffset, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    if(!BSPACCSetActive())
    {
        return FALSE;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCSetOffset
//
// This function is used to set offset of 3-axis for acc through I2C.
//
// Parameters:
//      iXOffset
//          [in] x-axis offset
//      iYOffset
//          [in] y-axis offset
//      iZOffset
//          [in] z-axis offset
// Notes, XOffset, YOffset, ZOffset reg unit is 1/2 LSB, so in this function
// it should be multipled by 2
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCSetOffset(INT8 iXOffset, INT8 iYOffset, INT8 iZOffset)
{
    INT32 iResult;
    INT8  byData[3];

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(!BSPACCSetStandby())
    {
        return FALSE;
    }

    byData[0] = iXOffset;
    byData[1] = iYOffset;
    byData[2] = iZOffset;

    I2CWriteMultiBytes(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_OFF_X, (BYTE*)byData, 3, &iResult);

    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    if(!BSPACCSetActive())
    {
        return FALSE;
    }

    return TRUE;
}


//-----------------------------------------------------------------------------
//
// Function:  BSPACCSetIntrPin
//
// This function is used to set Interrupt pin polarity, push-pull or open-drain through I2C.
//
// Parameters:
//      bIntr 
//          [in] 00:active low/push-pull
//                01:active low/open-drain
//                10:active high/push-pull
//                11:active high/open-drain
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCSetIntrPin(INT8 bIntr)
{
    INT32 iResult;
    INT8  bACCCTRLReg3;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(!BSPACCSetStandby())
    {
        return FALSE;
    }

    bACCCTRLReg3 = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG3, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    bACCCTRLReg3 = ((bACCCTRLReg3&~MMA8451_CTRLREG3_INTR_MASK)|MMA8451_CTRLREG3_INTR(bIntr));

    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG3, bACCCTRLReg3, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }
    
    if(!BSPACCSetActive())
    {
        return FALSE;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCSetIntrPinRout
//
// This function is used to set interrupt pin rout to INT1 or INT2 through I2C.
//
// Parameters:
//      bINTn 
//          [in] rout to INT1 or INT2
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCSetIntrPinRout(INT8 bINTn)
{
    INT32 iResult;
    BYTE  bACCCTRLReg5;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(!BSPACCSetStandby())
    {
        return FALSE;
    }

    if(bINTn == 0)
        bACCCTRLReg5 = 0xFF;    // rout to INT1 pin
    else  bACCCTRLReg5 = 0x0;   // rout to INT2 pin

    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG5, bACCCTRLReg5, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }
    
    if(!BSPACCSetActive())
    {
        return FALSE;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCSetIntrSource
//
// This function is used to set Interrupt source through I2C.
//
// Parameters:
//      bIntr 
//          [in] interrupt source
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCSetIntrSource(INT8 bIntrSource)
{
    INT32 iResult;
    INT8  bACCCTRLReg4;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(!BSPACCSetStandby())
    {
        return FALSE;
    }

    bACCCTRLReg4 = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG4, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    bACCCTRLReg4 |= (1<<bIntrSource);
    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG4, bACCCTRLReg4, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    if(!BSPACCSetActive())
    {
        return FALSE;
    }

    return TRUE;
}


//-----------------------------------------------------------------------------
//
// Function:  BSPACCClearIntrSource
//
// This function is used to clear Interrupt source through I2C.
//
// Parameters:
//      bIntr 
//          [in] interrupt source
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCClearIntrSource(INT8 bIntrSource)
{
    INT32 iResult;
    INT8  bACCCTRLReg4;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(!BSPACCSetStandby())
    {
        return FALSE;
    }

    bACCCTRLReg4 = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG4, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    bACCCTRLReg4 &= ~(1<<bIntrSource);

    I2CWriteOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG4, bACCCTRLReg4, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    if(!BSPACCSetActive())
    {
        return FALSE;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCGetIntrStatus
//
// This function is used to get interrupt register status for acc through I2C.
//
// Parameters:
//      byStatus
//          [OUT] interrupt register status
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCGetIntrStatus(INT8* byStatus)
{
    INT32 iResult;
    INT8  bACCIntrStatus;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(NULL == byStatus)
    {
        return FALSE;
    }

    bACCIntrStatus = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_INT_SOURCE, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    *byStatus = bACCIntrStatus;

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCGetIntrSource
//
// This function is used to get detection source for acc through I2C.
//
// Parameters:
//      pbySource
//          [OUT] interrupt source
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCGetIntrSource(INT8* pbySource)
{
    INT32 iResult;
    INT8  bACCCTRLReg4;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    bACCCTRLReg4 = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG4, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    *pbySource = bACCCTRLReg4;

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCGetStatus
//
// This function is used to get status register for acc through I2C.
//
// Parameters:
//      byStatus
//          [OUT]  status register 
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCGetStatus(INT8* byStatus)
{
    INT32 iResult;
    INT8  bACCStatusReg;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(NULL == byStatus)
    {
        return FALSE;
    }

    bACCStatusReg = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_STATUS, &iResult);

    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    *byStatus = bACCStatusReg;

    return TRUE;
}


//-----------------------------------------------------------------------------
//
// Function:  BSPACCGetOutput8bit
//
// This function is used to get 3-axis 8 bit output of acc through I2C.
//
// Parameters:
//      iXOutput
//          [OUT] 8 bit x-axis output
//      iYOutput
//          [OUT] 8 bit y-axis output
//      iZOutput
//          [OUT] 8 bit z-axis output
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCGetOutput8bit(INT8* iXOutput, INT8* iYOutput, INT8* iZOutput)
{
    INT32 iResult;
    INT8  iOutput[6];
    //INT8 iTempX;
    //INT8 iTempY;
    //INT8 iTempZ;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if( (NULL == iXOutput) || (NULL == iYOutput) || (NULL == iZOutput) )
    {
        return FALSE;
    }

    I2CReadMultiBytes(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_OUT_X_MSB, (BYTE*)iOutput, 6, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }
#if 1
    *iXOutput = iOutput[0];
    *iYOutput = iOutput[2];
    *iZOutput = iOutput[4];

#else

    iTempX = iOutput[0];
    iTempY = iOutput[1];
    iTempZ = iOutput[2];

    if (iTempX < (1 << (8 - 1)))
    {
        // Positive
        *iXOutput = iTempX;
    }
    else
    {
        // Negative
        *iXOutput = (-1) * ((1 << 8) - iTempX);
    }

    if (iTempY < (1 << (8 - 1)))
    {
        // Positive
        *iYOutput = iTempY;
    }
    else
    {
        // Negative
        *iYOutput = (-1) * ((1 << 8) - iTempY);
    }

    if (iTempZ < (1 << (8 - 1)))
    {
        // Positive
        *iZOutput = iTempZ;
    }
    else
    {
        // Negative
        *iZOutput = (-1) * ((1 << 8) - iTempZ);
    }
#endif
    DEBUGMSG(FALSE, (TEXT("BSPACCGetOutput8bit X = %d, Y = %d, Z = %d\r\n"), *iXOutput, *iYOutput, *iZOutput));

    return TRUE;
}


//-----------------------------------------------------------------------------
//
// Function:  BSPACCGetOutput14bit
//
// This function is used to get 3-axis 14 bit output of acc through I2C.
//
// Parameters:
//      iXOutput
//          [OUT] 14 bit x-axis output
//      iYOutput
//          [OUT] 14 bit y-axis output
//      iZOutput
//          [OUT] 14 bit z-axis output
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCGetOutput14bit(INT16* iXOutput, INT16* iYOutput, INT16* iZOutput)
{
    INT32 iResult;
    INT8  iOutput[6];
    INT16 iTempX;
    INT16 iTempY;
    INT16 iTempZ;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if( (NULL == iXOutput) || (NULL == iYOutput) || (NULL == iZOutput) )
    {
        return FALSE;
    }

    I2CReadMultiBytes(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_OUT_X_MSB, (BYTE*)iOutput, 6, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    iTempX = (INT16)(((iOutput[0] << 6)/* & 0x3fC0*/) | ((iOutput[1] >> 2) & 0x003f));
    iTempY = (INT16)(((iOutput[2] << 6)/* & 0x3fC0*/) | ((iOutput[3] >> 2) & 0x003f));;
    iTempZ = (INT16)(((iOutput[4] << 6)/* & 0x3fC0*/) | ((iOutput[5] >> 2) & 0x003f));;

#if 1

    *iXOutput = (INT16)((iTempX << (16-14)) >> (16-14));
    *iYOutput = (INT16)((iTempY << (16-14)) >> (16-14));
    *iZOutput = (INT16)((iTempZ << (16-14)) >> (16-14));

#else

    //for 12bit output, it should pay attention to the signed bit
    // Signed 12 bits output
    if (iTempX < (1 << (12 - 1)))
    {
        // Positive
        *iXOutput = iTempX;
    }
    else
    {
        // Negative
        *iXOutput = (-1) * ((1 << 12) - iTempX);
    }

    if (iTempY < (1 << (12 - 1)))
    {
        // Positive
        *iYOutput = iTempY;
    }
    else
    {
        // Negative
        *iYOutput = (-1) * ((1 << 12) - iTempY);
    }

    if (iTempZ < (1 << (12 - 1)))
    {
        // Positive
        *iZOutput = iTempZ;
    }
    else
    {
        // Negative
        *iZOutput = (-1) * ((1 << 12) - iTempZ);
    }
#endif
    DEBUGMSG(FALSE, (TEXT("BSPACCGetOutput14bit X = %d, Y = %d, Z = %d\r\n"), *iXOutput, *iYOutput, *iZOutput));

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCGetXOffset
//
// This function is used to get x-axis offset for acc through I2C.
//
// Parameters:
//      piXOffset
//          [OUT] x-axis offset
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCGetXOffset(INT8* piXOffset)
{
    INT32 iResult;
    INT8  iOutput;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if( (NULL == piXOffset) )
    {
        return FALSE;
    }

    iOutput = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_OFF_X, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    *piXOffset = iOutput;

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCGetYOffset
//
// This function is used to get y-axis offset for acc through I2C.
//
// Parameters:
//      piYOffset
//          [OUT] y-axis offset
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCGetYOffset(INT8* piYOffset)
{
    INT32 iResult;
    INT8  iOutput;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if( (NULL == piYOffset) )
    {
        return FALSE;
    }

    iOutput = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_OFF_Y, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    *piYOffset = iOutput;

    return TRUE;
}


//-----------------------------------------------------------------------------
//
// Function:  BSPACCGetZOffset
//
// This function is used to get z-axis offset for acc through I2C.
//
// Parameters:
//      piZOffset
//          [OUT] z-axis offset
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCGetZOffset(INT8* piZOffset)
{
    INT32 iResult;
    INT8  iOutput;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if( (NULL == piZOffset) )
    {
        return FALSE;
    }

    iOutput = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_OFF_Z, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    *piZOffset = iOutput;

    return TRUE;
}


//-----------------------------------------------------------------------------
//
// Function:  BSPACCGetOffset
//
// This function is used to get offset of 3-axis for acc through I2C.
//
// Parameters:
//      iXOffset
//          [OUT] x-axis offset
//      iYOffset
//          [OUT] y-axis offset
//      iZOffset
//          [OUT] z-axis offset
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCGetOffset(INT8* iXOffset, INT8* iYOffset, INT8* iZOffset)
{
    INT32 iResult;
    INT8  iOutput[3];

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if( (NULL == iXOffset) || (NULL == iYOffset) || (NULL == iZOffset) )
    {
        return FALSE;
    }

    I2CReadMultiBytes(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_OFF_X, (BYTE*)iOutput, 3, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    *iXOffset = iOutput[0];
    *iYOffset = iOutput[1];
    *iZOffset = iOutput[2];

    return TRUE;

}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCGetMode
//
// This function is used to get mode for acc through I2C.
//
// Parameters:
//      pbyMode
//          [OUT] accelerometer mode.
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCGetMode(INT8* pbyMode)
{
    INT32 iResult;
    INT8  bACCModeReg;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(NULL == pbyMode)
    {
        return FALSE;
    }

    bACCModeReg = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG1, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    *pbyMode = bACCModeReg & MMA8451_CTRLREG1_ACTIVE_MASK;

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCGetDR
//
// This function is used to get data rate selection.
//
// Parameters:
//      pbDFBW
//          [OUT] pointer to digital output rate.
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCGetDR(INT8* pbDR)
{
    INT32 iResult;
    INT8  bACCCTL1Reg;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    bACCCTL1Reg = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_CTRL_REG1, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    *pbDR = (bACCCTL1Reg & MMA8451_CTRLREG1_DR_MASK) >> MMA8451_CTRLREG1_DR_LSH;

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCGetGSel
//
// This function is used to get g-sel for acc through I2C.
//
// Parameters:
//      pbyGSel
//          [OUT] accelerometer g-sel.
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCGetGSel(INT8* pbyGSel)
{
    INT32 iResult;
    INT8  bACCXYZCFGReg;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(NULL == pbyGSel)
    {
        return FALSE;
    }

    bACCXYZCFGReg = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_XYZ_DATA_CFG, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    *pbyGSel = bACCXYZCFGReg & MMA8451_XYZDATACFG_FS_MASK;

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCGetPLAngle
//
// This function is used to get the trip angle for the image transition from the portrait orietation 
// to landscape orietation through I2C.
//
// Parameters:
//      pLDTH
//          [OUT]  the trip angle
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCGetPLAngle(INT8* pAngle)
{
    INT32 iResult;
    INT8  bACCPLReg;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    if(NULL == pAngle)
    {
        return FALSE;
    }

    bACCPLReg = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_PL_THS_REG, &iResult);

    DEBUGMSG(TRUE, (TEXT("BSPACCGetPLAngle (%x)\r\n"), bACCPLReg));

    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    bACCPLReg;

    switch ((bACCPLReg >> MMA8451_PLTHS_PLTHS_LSH) & 0x1F)
    {
    case 0x07:
        *pAngle = 15;
        break;
    case 0x09:
        *pAngle = 20;
        break;
    case 0x0C:
        *pAngle = 30;
        break;
    case 0x0D:
        *pAngle = 35;
        break;
    case 0x0F:
        *pAngle = 40;
        break;
    case 0x10:
        *pAngle = 45;
        break;
    case 0x13:
        *pAngle = 55;
        break;
    case 0x14:
        *pAngle = 60;
        break;
    case 0x17:
        *pAngle = 70;
        break;
    case 0x19:
        *pAngle = 75;
        break;
    default:
        return FALSE;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCGetPLStatus
//
// This function is used to get Portrait-Landscape status  for acc through I2C.
//
// Parameters:
//      pbyGSel
//          [OUT] accelerometer g-sel.
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------
bool BSPACCGetPLStatus(INT8* mPLOrientatiaon, INT8* mFBOrientation)
{
    INT32 iResult;
    UINT8  bACCPLStatusReg;

    if(NULL == s_hACCI2C)
    {
        return FALSE;
    }

    bACCPLStatusReg = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_PL_STATUS, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return FALSE;
    }

    if((bACCPLStatusReg & MMA8451_PLSTATUS_NEWLP_MASK)&&(!(bACCPLStatusReg & MMA8451_PLSTATUS_LO_MASK)))
    {
        *mPLOrientatiaon = (bACCPLStatusReg & MMA8451_PLSTATUS_LAPO_MASK)>>MMA8451_PLSTATUS_LAPO_LSH;
        *mFBOrientation = bACCPLStatusReg & MMA8451_PLSTATUS_BAFRO_MASK; 
        return TRUE;
    }

    return FALSE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPACCWHOAMI
//
// This function is used to get version  for acc through I2C.
//
// Parameters:
//      None.
// Returns:
//      acc version.
//
//-----------------------------------------------------------------------------
INT8 BSPACCWHOAMI(void)
{
    INT32 iResult;
    INT8  bACCPLStatusReg;

    if(NULL == s_hACCI2C)
    {
        return 0;
    }

    bACCPLStatusReg = I2CReadOneByte(s_hACCI2C, ACC_IIC_ADDRESS, MMA8451_WHO_AM_I, &iResult);
    if( iResult != I2C_NO_ERROR )
    {
        return 0;
    }

    return bACCPLStatusReg;
}

//-----------------------------------------------------------------------------
//
// Function: I2CWriteOneByte
//
// This function writes a single byte byData to the register stated in byReg.
//
// Parameters:
//      hI2C
//          [in] File handle to I2C Bus Interface.
//
//      byAddr
//          [in] I2C Slave device address.
//
//      byReg
//          [in] Register Index.
//
//      byData
//          [in] Data to write to byReg.
//
//      lpiResult
//          [in] Pointer of the result. The I2C Bus will store the
//            result of the operation in location pointed to by
//            lpiResult.
//
// Returns:
//      None.
//
//-----------------------------------------------------------------------------
VOID I2CWriteOneByte(HANDLE hI2C, BYTE byAddr, BYTE byReg, BYTE byData, LPINT lpiResult)
{
    I2C_TRANSFER_BLOCK I2CXferBlock;
    I2C_PACKET I2CPacket;
    BYTE byOutData[2];

    byOutData[0] = byReg;
    byOutData[1] = byData;
    I2CPacket.wLen = sizeof(byOutData);
    I2CPacket.pbyBuf = (PBYTE) &byOutData;

    I2CPacket.byRW = I2C_RW_WRITE;
    I2CPacket.byAddr = byAddr;
    I2CPacket.lpiResult = lpiResult;

    I2CXferBlock.pI2CPackets = &I2CPacket;
    I2CXferBlock.iNumPackets = 1;

    if(!I2CTransfer(hI2C, &I2CXferBlock))
        ERRORMSG(TRUE,(TEXT("%s:I2C Write Register Fail\r\n"),__WFUNCTION__));
}

//-----------------------------------------------------------------------------
//
// Function: I2CReadOneByte
//
// This function read a single byte data from the register stated in byReg.
//
// Parameters:
//      hI2C
//          [in] File handle to I2C Bus Interface.
//
//      byAddr
//          [in] I2C Slave device address.
//
//      byReg
//          [in] Register Index.
//
//      lpiResult
//          [in] Pointer of the result. The I2C Bus will store the
//            result of the operation in location pointed to by
//            lpiResult.
//
// Returns:
//      The single byte content stored in byReg.
//
//-----------------------------------------------------------------------------
INT8 I2CReadOneByte(HANDLE hI2C, BYTE byAddr, BYTE byReg, LPINT lpiResult)
{
    I2C_TRANSFER_BLOCK I2CXferBlock;
    I2C_PACKET I2CPacket[2];
    BYTE byOutData;
    BYTE byInData;

    byOutData = byReg;

    I2CPacket[0].pbyBuf = (PBYTE) &byOutData;
    I2CPacket[0].wLen = sizeof(byOutData);

    I2CPacket[0].byRW = I2C_RW_WRITE;
    I2CPacket[0].byAddr = byAddr;
    I2CPacket[0].lpiResult = lpiResult;

    I2CPacket[1].pbyBuf = (PBYTE) &byInData;
    I2CPacket[1].wLen = sizeof(byInData);

    I2CPacket[1].byRW = I2C_RW_READ;
    I2CPacket[1].byAddr = byAddr;
    I2CPacket[1].lpiResult = lpiResult;

    I2CXferBlock.pI2CPackets = I2CPacket;
    I2CXferBlock.iNumPackets = 2;

    if(!I2CTransfer(hI2C, &I2CXferBlock))
        ERRORMSG(TRUE,(TEXT("%s:I2C Read Register Fail\r\n"),__WFUNCTION__));

    return byInData;
}

//-----------------------------------------------------------------------------
//
// Function: I2CWriteTwoBytes
//
// This function writes a two byte data to the register stated in byReg. The
// function will write byData1 first, followed by byData2. If byData1 is not
// written properly, the entire function will terminate without attempting to
// write byData2.
//
// Parameters:
//      hI2C
//          [in] File handle to I2C Bus Interface.
//
//      byAddr
//          [in] I2C Slave device address.
//
//      byReg
//          [in] Register Index.
//
//      byData1
//          [in] 1st Data to write to byReg.
//
//      byData2
//          [in] 2nd Data to write to byReg
//
//      lpiResult
//          [in] Pointer of the result. The I2C Bus will store the
//            result of the operation in location pointed to by
//            lpiResult.
//
// Returns:
//      None.
//
//-----------------------------------------------------------------------------
VOID I2CWriteTwoBytes(HANDLE hI2C, BYTE byAddr, BYTE byReg, BYTE byData1, BYTE byData2, LPINT lpiResult)
{
    I2C_TRANSFER_BLOCK I2CXferBlock;
    I2C_PACKET I2CPacket;
    BYTE byOutData[3];

    byOutData[0] = byReg;
    byOutData[1] = byData1;
    byOutData[2] = byData2;

    I2CPacket.wLen = sizeof(byOutData);
    I2CPacket.pbyBuf = (PBYTE) &byOutData;

    I2CPacket.byRW = I2C_RW_WRITE;
    I2CPacket.byAddr = byAddr;
    I2CPacket.lpiResult = lpiResult;

    I2CXferBlock.pI2CPackets = &I2CPacket;
    I2CXferBlock.iNumPackets = 1;

    I2CTransfer(hI2C, &I2CXferBlock);
}

//-----------------------------------------------------------------------------
//
// Function: I2CReadTwoBytes
//
// This function reads two bytes of data from the register stated in byReg.
//
// Parameters:
//      hI2C
//          [in] File handle to I2C Bus Interface.
//
//      byAddr
//          [in] I2C Slave device address.
//
//      byReg
//          [in] Register Index.
//
//      lpiResult
//          [in] Pointer of the result. The I2C Bus will store the
//          result of the operation in location pointed to by
//          lpiResult.
//
// Returns:
//      The two byte content stored in byReg.
//
//-----------------------------------------------------------------------------
WORD I2CReadTwoBytes(HANDLE hI2C, BYTE byAddr, BYTE byReg, LPINT lpiResult)
{
    I2C_TRANSFER_BLOCK I2CXferBlock;
    I2C_PACKET I2CPacket[2];
    BYTE byOutData;
    BYTE byInData[2];

    byOutData = byReg;

    I2CPacket[0].pbyBuf = (PBYTE) &byOutData;
    I2CPacket[0].wLen = sizeof(byOutData);

    I2CPacket[0].byRW = I2C_RW_WRITE;
    I2CPacket[0].byAddr = byAddr;
    I2CPacket[0].lpiResult = lpiResult;

    I2CPacket[1].pbyBuf = (PBYTE) &byInData;
    I2CPacket[1].wLen = sizeof(byInData);

    I2CPacket[1].byRW = I2C_RW_READ;
    I2CPacket[1].byAddr = byAddr;
    I2CPacket[1].lpiResult = lpiResult;

    I2CXferBlock.pI2CPackets = I2CPacket;
    I2CXferBlock.iNumPackets = 2;

    I2CTransfer(hI2C, &I2CXferBlock);

    return ((0xFF & byInData[1]) | (((WORD) byInData[0]) << 8));
}

//-----------------------------------------------------------------------------
//
// Function: I2CWriteMultiBytes
//
// This function writes Multi bytes data to the register stated in byReg. The
// function will write byData1 first, followed by byData2. If byData1 is not
// written properly, the entire function will terminate without attempting to
// write byData2.
//
// Parameters:
//      hI2C
//          [in] File handle to I2C Bus Interface.
//
//      byAddr
//          [in] I2C Slave device address.
//
//      byReg
//          [in] Register Index.
//
//      pbyData
//          [in] 1st Data address.
//
//      byDataNum
//          [in] Data Number, the MAX value is 31
//
//      lpiResult
//          [in] Pointer of the result. The I2C Bus will store the
//            result of the operation in location pointed to by
//            lpiResult.
//
// Returns:
//      None.
//
//-----------------------------------------------------------------------------
VOID I2CWriteMultiBytes(HANDLE hI2C, BYTE byAddr, BYTE byReg, BYTE* pbyData, BYTE byDataNum, LPINT lpiResult)
{
    I2C_TRANSFER_BLOCK I2CXferBlock;
    I2C_PACKET I2CPacket;
    BYTE byOutData[32];

    memset(byOutData, 32, 0);
    byOutData[0] = byReg;
    memcpy(&(byOutData[1]), pbyData, byDataNum);

    I2CPacket.wLen = byDataNum+1;
    I2CPacket.pbyBuf = (PBYTE) &byOutData;

    I2CPacket.byRW = I2C_RW_WRITE;
    I2CPacket.byAddr = byAddr;
    I2CPacket.lpiResult = lpiResult;

    I2CXferBlock.pI2CPackets = &I2CPacket;
    I2CXferBlock.iNumPackets = 1;

    I2CTransfer(hI2C, &I2CXferBlock);
}


//-----------------------------------------------------------------------------
//
// Function: I2CReadMultiBytes
//
// This function reads Multi bytes of data from the register stated in byReg.
//
// Parameters:
//      hI2C
//          [in] File handle to I2C Bus Interface.
//
//      byAddr
//          [in] I2C Slave device address.
//
//      byReg
//          [in] Register Index.
//
//      pbyData
//          [in] 1st Data address to save.
//
//      byDataNum
//          [in] Data Number, the MAX value is 31
//
//      lpiResult
//          [in] Pointer of the result. The I2C Bus will store the
//          result of the operation in location pointed to by
//          lpiResult.
//
// Returns:
//      The two byte content stored in byReg.
//
//-----------------------------------------------------------------------------
void I2CReadMultiBytes(HANDLE hI2C, BYTE byAddr, BYTE byReg, BYTE* pbyData, BYTE byDataNum, LPINT lpiResult)
{
    I2C_TRANSFER_BLOCK I2CXferBlock;
    I2C_PACKET I2CPacket[2];
    BYTE byOutData;

    if(NULL == pbyData)
    {
        return;
    }

    byOutData = byReg;

    I2CPacket[0].pbyBuf = (PBYTE) &byOutData;
    I2CPacket[0].wLen = sizeof(byOutData);

    I2CPacket[0].byRW = I2C_RW_WRITE;
    I2CPacket[0].byAddr = byAddr;
    I2CPacket[0].lpiResult = lpiResult;

    I2CPacket[1].pbyBuf = (PBYTE) pbyData;
    I2CPacket[1].wLen = byDataNum;

    I2CPacket[1].byRW = I2C_RW_READ;
    I2CPacket[1].byAddr = byAddr;
    I2CPacket[1].lpiResult = lpiResult;

    I2CXferBlock.pI2CPackets = I2CPacket;
    I2CXferBlock.iNumPackets = 2;

    I2CTransfer(hI2C, &I2CXferBlock);

    return ;
}
