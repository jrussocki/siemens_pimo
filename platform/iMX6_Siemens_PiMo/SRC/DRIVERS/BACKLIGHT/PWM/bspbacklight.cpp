/*---------------------------------------------------------------------------
* Copyright (C) 2005-2011, Freescale Semiconductor, Inc. All Rights Reserved.
* THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
* AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT 
*--------------------------------------------------------------------------*/
//---------------------------------------------------------------------------

//
//  File:  bspbacklight.c
//
//  Provides BSP-specific configuration routines for the backlight driver.
//
//---------------------------------------------------------------------------

#pragma warning(push)
#pragma warning(disable: 4201)
#include <windows.h>
#pragma warning(pop)

#include "bsp.h"

//---------------------------------------------------------------------------

// External Functions

//---------------------------------------------------------------------------

// External Variables


//---------------------------------------------------------------------------

// Defines

#define BKL_PWM_PERIOD      63


#define BKL_PWM1_INDEX       PWM_INDEX_PWM0
#define BKL_PWM1_IOMUX_PIN   DDK_IOMUX_PIN_SD1_DAT3



//---------------------------------------------------------------------------

// Types


//---------------------------------------------------------------------------

// Global Variables


//---------------------------------------------------------------------------

// Local Variables


//---------------------------------------------------------------------------

// Local Functions


//---------------------------------------------------------------------------

//
// Function: BSPBacklightInitialize
//
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//---------------------------------------------------------------------------

void BSPBacklightInitialize()
{
    // Setup PWM1,PWM2 IOMUX
    PWMIomuxSetPin(BKL_PWM1_INDEX, BKL_PWM1_IOMUX_PIN);
    
    // Initial PWM1,2
    PWMInitialize(BKL_PWM1_INDEX);
    
    // Set Period value    
    PWMSetPeriod(BKL_PWM1_INDEX, BKL_PWM_PERIOD);
    
    // Set Sample value    
    PWMSetSample(BKL_PWM1_INDEX, BKL_PWM_PERIOD);
    
    // Enable backlight    
    PWMStart(BKL_PWM1_INDEX);

	RETAILMSG(0,(L"Backlight Driver loaded\n"));
}

//---------------------------------------------------------------------------
//
// Function: BSPBacklightEnable
//
// 
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//---------------------------------------------------------------------------

void BSPBacklightEnable()
{

}

//---------------------------------------------------------------------------

//
// Function: BSPBacklightRelease
//
// 
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//---------------------------------------------------------------------------

void BSPBacklightRelease()
{

}


//---------------------------------------------------------------------------

//
// Function: BSPBacklightSetIntensity
//
// 
//
// Parameters:
//      None.
//
// Returns:
//      None.
//
//---------------------------------------------------------------------------

void BSPBacklightSetIntensity(DWORD level)
{
    static BOOL bEnabled = TRUE; //Backlight is already enabled when initialization.
    if( level < 0 )
         level = 0;
    level = level / 4;

    if( level > BKL_PWM_PERIOD )
         level = BKL_PWM_PERIOD;

    //RETAILMSG(1, (TEXT("BSPBacklightSetIntensity level=%d\r\n"), level));
    
    // Setup PWM IOMUX
    PWMIomuxSetPin(BKL_PWM1_INDEX, BKL_PWM1_IOMUX_PIN);

    if (level == 0)        
    {
        PWMStop(BKL_PWM1_INDEX);
        
        //Turn off pwm clock
        PWMSetClockGatingMode(BKL_PWM1_INDEX, FALSE);

        
        bEnabled = FALSE;
    }
    else
    {
        // If backlight is not enabled,enable it.        
        if (!bEnabled)
        {
            //Turn on pwm clock
            PWMSetClockGatingMode(BKL_PWM1_INDEX, TRUE);
            
            PWMStart(BKL_PWM1_INDEX);
            bEnabled = TRUE;
        } 
        PWMSetSample(BKL_PWM1_INDEX, level);

    } 
}

