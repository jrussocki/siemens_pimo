//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//

#pragma once

#include <pwsdef.h>

extern "C" extern POWER_BROADCAST_POWER_INFO gSystemPowerStatus;

#define PM_UNKNOWN_POWER_STATE (-1)

typedef enum { 
    On,                     // system is running normally with UI enabled
    UserIdle,               // User Idle state.
    SystemIdle,
    Resuming,               // system is determining what to do after a resume
    Suspend,                 // system suspended, all devices off (or wake-enabled)
    ColdReboot,
    Reboot,
    Shutdown,
    Critical,
    UnknownState = PM_UNKNOWN_POWER_STATE,       // Unknown
} PLATFORM_ACTIVITY_STATE, *PPLATFORM_ACTIVITY_STATE;

typedef enum {
    NoTimeoutItem,
    SuspendTimeout,
    SystemActivityTimeout,
    UserActivityTimeout,
} TIMEOUT_ITEM, *PTIMEOUT_ITEM;

class PowerStateManager: public DefaultPowerStateManager {
public:
    PowerStateManager(PVOID pParam);
    ~PowerStateManager();
    BOOL Init();
    virtual void ResetUserIdleTimeout(BOOL fIdle);
    virtual void ResetSystemIdleTimeTimeout(BOOL fIdle);
    virtual void SubtractTimeout(DWORD dwTicks);
    virtual DWORD ThreadRun();
    virtual void ReAdjustTimeOuts();
    virtual DWORD GetSmallestTimeout(PTIMEOUT_ITEM pTimeoutItem);
    void DisableUserIdleTimeout() { m_dwCurUserIdleTimeout = INFINITE; };
    void DisableSuspendTimeout() { m_dwCurSuspendTimeout = INFINITE; };
    void DisableSystemIdleTimeout() { m_dwCurTimeoutToSystemIdle = INFINITE; };
    virtual void ReInitTimeOuts();
    virtual void PlatformResumeSystem(BOOL fSuspened);
    virtual PLATFORM_ACTIVITY_STATE RequestedSystemPowerState();
    virtual void RequestComplete(DWORD dwStatus = ERROR_SUCCESS );

private:
    PowerState * GetFirstPowerState();

protected:        
    virtual void PlatformLoadTimeouts();
    virtual BOOL CreatePowerStateList();
    // Timeout Function.
    DWORD GetSuspendTimeOut () {
        return (gSystemPowerStatus.bACLineStatus == AC_LINE_OFFLINE? m_dwBattSuspendTimeout: m_dwACSuspendTimeout);
    }
    DWORD GetSystemIdleTimeOut() {
        return (gSystemPowerStatus.bACLineStatus == AC_LINE_OFFLINE? m_dwBattTimeoutToSystemIdle: m_dwACTimeoutToSystemIdle);
    }
    DWORD GetUserIdleTimeOut() {
        return (gSystemPowerStatus.bACLineStatus == AC_LINE_OFFLINE? m_dwBattUserIdleTimeout: m_dwACUserIdleTimeout);
    }

protected:

    DWORD m_dwACSuspendTimeout;
    DWORD m_dwACTimeoutToSystemIdle ;
    DWORD m_dwACUserIdleTimeout;
    
    DWORD m_dwBattSuspendTimeout ;
    DWORD m_dwBattTimeoutToSystemIdle ;
    DWORD m_dwBattUserIdleTimeout;
        
    DWORD m_dwCurSuspendTimeout;
    DWORD m_dwCurTimeoutToSystemIdle ;
    DWORD m_dwCurUserIdleTimeout;

    BOOL  m_fActiveManagement;

};
