//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//

#include <pmimpl.h>

// This routine determines how many listeners there are for a particular 
// class of notifications.  If none, it returns zero.  A listener for any
// of the bits in dwMask will get counted once, regardless of how many
// mask bits match.
DWORD 
GetNotificationRequestCount(DWORD dwMask)
{
    PPOWER_NOTIFICATION ppn;
    DWORD dwCount = 0;
    
    PMLOCK();
    for(ppn = gpPowerNotifications; ppn != NULL; ppn = ppn->pNext) {
        if((ppn->dwFlags & dwMask) != 0) {
            dwCount++;
        }
    }
    PMUNLOCK();

    return dwCount;
}
// This routine determines how to react to Power Button Press Notification 
// from Others.
DWORD gdwCurTickCount = 0;
BOOL SuspendButtonPressed(BOOL bReleaseSupported)
{
    if (bReleaseSupported) {
        gdwCurTickCount = GetTickCount();
    }
    else {
        // Treat this as external request.
        VERIFY(SUCCEEDED(PmSetSystemPowerState_I(NULL, POWER_STATE_SUSPEND, 0, FALSE )));
    }
    return TRUE;
}
// This routine determines how to react to Power Button Release Notification 
// from Others if it is supported.
BOOL  SuspendButtonReleased(BOOL bReleaseSupported)
{
    if (bReleaseSupported) {
        // Treat this as external request.
        VERIFY(SUCCEEDED(PmSetSystemPowerState_I(NULL, POWER_STATE_SUSPEND, 0, FALSE )));
    }
    return TRUE;
}


