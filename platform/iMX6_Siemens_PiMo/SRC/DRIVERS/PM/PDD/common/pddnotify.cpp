//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//

#include <pmimpl.h>

// accesses to this variable should be protected by the PM lock
extern "C" extern POWER_BROADCAST_POWER_INFO gSystemPowerStatus;

// This routine is called whenever a client requests some form of notifications from
// the PM.  In some cases it may be appropriate to send an immediate notification
// or take other action on behalf of the client.  The caller of this routine must
// hold the PM lock.
VOID
PlatformSendInitialNotifications(PPOWER_NOTIFICATION ppn, DWORD dwFlags)
{
    POWER_BROADCAST_BUFFER pbb;

    // The only notifications we need to process when they are requested 
    // is battery notifications.  This allows the client to immediately
    // synchronize with the current system power status.
    if((dwFlags & PBT_POWERSTATUSCHANGE) != 0) {
        // send a notification if the line status is known
        if(gSystemPowerStatus.bACLineStatus == AC_LINE_OFFLINE
        || gSystemPowerStatus.bACLineStatus == AC_LINE_ONLINE) {
            const PPOWER_BROADCAST_POWER_STATUS_CHANGE pPbStatusChange = (const PPOWER_BROADCAST_POWER_STATUS_CHANGE)&pbb;
            pPbStatusChange->Message = PBT_POWERSTATUSCHANGE;
            pPbStatusChange->Flags = 0;
            pPbStatusChange->Length = sizeof(pPbStatusChange->NewSystemPowerState);
            pPbStatusChange->NewSystemPowerState = gSystemPowerStatus.bACLineStatus;
            SendNotification(ppn, (PPOWER_BROADCAST) pPbStatusChange, sizeof(POWER_BROADCAST_POWER_STATUS_CHANGE));
        }
    }

    // Send a PBT_POWERINFOCHANGE notification with whatever we know about power status.
    if((dwFlags & PBT_POWERINFOCHANGE) != 0) {
        PPOWER_BROADCAST_POWER_INFO ppbpi = (PPOWER_BROADCAST_POWER_INFO) &pbb.SystemPowerState[0];
        
        // update the notification buffer
        pbb.Message = PBT_POWERINFOCHANGE;
        pbb.Flags = 0;
        pbb.Length = sizeof(*ppbpi);
        *ppbpi = gSystemPowerStatus;
        SendNotification(ppn, (PPOWER_BROADCAST) &pbb, pbb.Length + (3 * sizeof(DWORD)));
    }
}
