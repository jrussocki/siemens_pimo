#ifndef BSPALS_H
#define BSPALS_H

#include <windows.h>

#include "i2cbus.h"

#define ALS_I2C_DEVICE      I2C3_FID

LPCWSTR GetI2C();

#endif //BSPALS_H