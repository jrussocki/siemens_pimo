//------------------------------------------------------------------------------
//
//  Copyright (C) 2012, Adeneo Embedded All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//  Copyright (C) 2007-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//
//  File:  bspserial.c
//
//  Provides BSP-specific configuration routines for the UART peripheral.
//
//-----------------------------------------------------------------------------

#pragma warning(push)
#pragma warning(disable: 4115 4201 4204 4214)
#include <windows.h>
#pragma warning(pop)

#include "bsp.h"
#include "common_uart.h"

//-----------------------------------------------------------------------------
// External Functions

//-----------------------------------------------------------------------------
// External Variables

//-----------------------------------------------------------------------------
// Defines

#define UART_MAX_DIV            7

//-----------------------------------------------------------------------------
// Types

//-----------------------------------------------------------------------------
// Global Variables

//-----------------------------------------------------------------------------
// Local Variables

//-----------------------------------------------------------------------------
// Local Functions

//-----------------------------------------------------------------------------
//
// Function: BSPUartCalRFDIV
//
// This is a private function to calculate the data
// rate divider from input frequency.
//
// Parameters:
//      dwFrequency
//          [in] Frequency requested.
//
// Returns:
//      Data rate divisor for requested frequency.
//-----------------------------------------------------------------------------
UCHAR BSPUartCalRFDIV(ULONG* pRefFreq)
{
    UCHAR dwDivisor; // the data rate divisor
    UINT32 freq;
    *pRefFreq = UART_REF_FREQ;

    DDKClockGetFreq(DDK_CLOCK_SIGNAL_UART, &freq);
    dwDivisor = (UCHAR) (freq / *pRefFreq);

    if ( (dwDivisor != 0) && ((freq / dwDivisor) < UART_REF_FREQ))
    {
       dwDivisor--; 
       if (dwDivisor < 0)
           dwDivisor = 0;
    }

//    else if ((freq / dwDivisor) > UART_REF_FREQ)
//             dwDivisor++;

    if (dwDivisor == 0)
    {
        dwDivisor = 1;
    }
    else if (dwDivisor > UART_MAX_DIV)
    {
        dwDivisor = UART_MAX_DIV;
    }

    *pRefFreq = freq /dwDivisor;

    return dwDivisor;
}

//-----------------------------------------------------------------------------
//
// Function: BSPUartGetType
//
// This is a private function to get the UART type with specified Uart
// IO address.
//
// Parameters:
//      HWAddr
//          [in] Physical IO address.
//      pType
//          [out] Serial device type (DCE/DTE).
//
// Returns:
//      corresponding uart type.
//
// NOTE: if change DCE/DTE mode, please change DDKIomuxSelectInput() for RXD/RTS
//-----------------------------------------------------------------------------
BOOL BSPUartGetType(ULONG HWAddr, uartType_c * pType)
{
    switch (HWAddr)
    {
        case CSP_BASE_REG_PA_UART1:
            *pType = DCE;
            return TRUE;
        case CSP_BASE_REG_PA_UART2:
            *pType = DCE;
            return TRUE;
        case CSP_BASE_REG_PA_UART3:
            *pType = DCE;
            return TRUE;
        case CSP_BASE_REG_PA_UART4:
            *pType = DCE;
            return TRUE;
		case CSP_BASE_REG_PA_UART5:
            *pType = DCE;
            return TRUE;
        default:
            return FALSE;
    }
}

//-----------------------------------------------------------------------------
//
// Function: BSPUartEnableClock
//
// This function is a wrapper for Uart to enable/disable its clock using a valid
// CRM handle.
//
// Parameters:
//      HWAddr
//          [in] Physical IO address.
//      bEnable
//          [in] TRUE if Uart Clock is to be enabled. FALSE if Uart Clock is
//                to be disabled.
//
// Returns:
//      TRUE if successfully performed the required action.
//
//-----------------------------------------------------------------------------
BOOL BSPUartEnableClock(ULONG HWAddr, BOOL bEnable)
{
    BOOL result = FALSE;
    DDK_CLOCK_GATE_INDEX cgIndex_IPG;
    DDK_CLOCK_GATE_INDEX cgIndex_PERCLK;    

    cgIndex_IPG = DDK_CLOCK_GATE_INDEX_UART;
    cgIndex_PERCLK = DDK_CLOCK_GATE_INDEX_UART_SERIAL;
    if (bEnable)
    {
        result = DDKClockSetGatingMode(cgIndex_IPG, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        result &= DDKClockSetGatingMode(cgIndex_PERCLK, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
    }
    else
    {
        result &= DDKClockSetGatingMode(cgIndex_PERCLK, DDK_CLOCK_GATE_MODE_DISABLED);      
        result = DDKClockSetGatingMode(cgIndex_IPG, DDK_CLOCK_GATE_MODE_DISABLED);  
    }
    return result;
}

//-----------------------------------------------------------------------------
//
// Function: BSPUartConfigTranceiver
//
// This function is used to configure the Tranceiver.
//
// Parameters:
//      HWAddr
//          [in] Physical IO address.
//      bEnable
//          [in] TRUE if tranceiver is to be enabled,
//                FALSE if tranceiver is to be disabled.
//               
//
// Returns:
//      TRUE if successfully performed the required action.
//
//-----------------------------------------------------------------------------
VOID BSPUartConfigTranceiver(ULONG HWAddr,BOOL bEnable)
{
    UNREFERENCED_PARAMETER(HWAddr);
    UNREFERENCED_PARAMETER(bEnable);
}

//-----------------------------------------------------------------------------
//
// Function: BSPUartConfigureGPIO
//
// This function is used to configure the GPIO.
//
// Parameters:
//      HWAddr
//          [in] Physical IO address.
//
// Returns:
//      TRUE if successfully performed the required action.
//
//-----------------------------------------------------------------------------
BOOL BSPUartConfigureGPIO(ULONG HWAddr)
{
    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function: BSPSerGetChannelPriority
//
// This function is a wrapper for Uart to find out the SDMA channel priority.
//
// Parameters:
//
// Returns:
//      SDMA channel priority for Serial.
//
//-----------------------------------------------------------------------------
UINT8 BSPSerGetChannelPriority()
{
    return BSP_SDMA_CHNPRI_SERIAL;
}

//-----------------------------------------------------------------------------
//
// Function: BSPSerGetDMAIsEnabled
//
// This function is a wrapper for Uart to find out if SDMA is to be used or not.
// The UARTs are identified based on the based address.
//
// Parameters:
//      HWAddr
//          [in] Physical IO address.
// Returns:
//      TRUE if SDMA is to be used for this UART.
//
//-----------------------------------------------------------------------------
BOOL BSPSerGetDMAIsEnabled(ULONG HWAddr)
{
    BOOL useDMA = FALSE;
    switch (HWAddr) {
        case CSP_BASE_REG_PA_UART1:
#if BSP_SDMA_SUPPORT_UART1
            useDMA = TRUE;
#endif
            break;
        case CSP_BASE_REG_PA_UART2:
#if BSP_SDMA_SUPPORT_UART2
            useDMA = TRUE;
#endif
            break;
        case CSP_BASE_REG_PA_UART3:
#if BSP_SDMA_SUPPORT_UART3
            useDMA = TRUE;
#endif
            break;
        case CSP_BASE_REG_PA_UART4:
#if BSP_SDMA_SUPPORT_UART4
            useDMA = TRUE;
#endif
            break;
		case CSP_BASE_REG_PA_UART5:
#if BSP_SDMA_SUPPORT_UART5
            useDMA = TRUE;
#endif
            break;
        default:
            break;
    }
    return useDMA;
}

//-----------------------------------------------------------------------------
//
// Function: BSPSerGetDMARequest
//
// This function is a wrapper for Uart to find out the TX/RX SDMA request line.
// The UARTs are identified based on the based address.
//
// Parameters:
//      HWAddr
//          [in] Physical IO address.
//      reqRx
//          [out] the RX SDMA request line to be used for this UART.
//      reqTx
//          [out] the TX SDMA request line to be used for this UART.
// Returns:
//      TRUE if SDMA is to be used for this UART.
//
//-----------------------------------------------------------------------------
BOOL BSPSerGetDMARequest(ULONG HWAddr, int* reqRx, int* reqTx)
{
    switch (HWAddr)
    {
        case CSP_BASE_REG_PA_UART1:
            *reqRx = DDK_DMA_REQ_UART1_RX;
            *reqTx = DDK_DMA_REQ_UART1_TX;
            break;
        case CSP_BASE_REG_PA_UART2:
            *reqRx = DDK_DMA_REQ_UART2_RX;
            *reqTx = DDK_DMA_REQ_UART2_TX;
            break;
        case CSP_BASE_REG_PA_UART3:
            *reqRx = DDK_DMA_REQ_UART3_RX;
            *reqTx = DDK_DMA_REQ_UART3_TX;
            break;
        case CSP_BASE_REG_PA_UART4:
            *reqRx = DDK_DMA_REQ_UART4_RX;
            *reqTx = DDK_DMA_REQ_UART4_TX;
            break;
		case CSP_BASE_REG_PA_UART5:
			*reqRx = DDK_DMA_REQ_UART5_RX;
            *reqTx = DDK_DMA_REQ_UART5_TX;
        default:
            return FALSE;
    }
    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function: BSPSerSetDMAReqGpr
//
// This function is a wrapper for Uart to acquire a muxed SDMA request line.
// The UARTs are identified based on the based address.
//
// Parameters:
//      HWAddr
//          [in] Physical IO address.
// Returns:
//      TRUE if success.
//
//-----------------------------------------------------------------------------
BOOL BSPSerAcquireDMAReqGpr(ULONG HWAddr)
{

    BOOL bRet = TRUE;
    UNREFERENCED_PARAMETER(HWAddr);
    return bRet;
}

//-----------------------------------------------------------------------------
//
// Function: BSPSerRestoreDMAReqGpr
//
// This function is a wrapper for Uart to restore a muxed SDMA request line.
// The UARTs are identified based on the based address.
//
// Parameters:
//      HWAddr
//          [in] Physical IO address.
// Returns:
//      TRUE if success.
//
//-----------------------------------------------------------------------------
BOOL BSPSerRestoreDMAReqGpr(ULONG HWAddr)
{
    BOOL bRet = TRUE;
    UNREFERENCED_PARAMETER(HWAddr);

    return bRet;
}

//-----------------------------------------------------------------------------
//
//  Function: GetSdmaChannelIRQ
//
//  This function returns the IRQ number of the specified SDMA channel.
//
//  Parameters:
//      chan
//          [in] The SDMA channel.
//
//  Returns:
//      IRQ number.
//
//-----------------------------------------------------------------------------
DWORD GetSdmaChannelIRQ(UINT32 chan)
{
    return (IRQ_SDMA_CH0 + chan);
}

//-----------------------------------------------------------------------------
//
// Function: BSPGetDMABuffSize
//
// This function is a wrapper for Uart to find out the TX/RX SDMA.
// The UARTs are identified based on the based address.
//
// Parameters:
//      buffRx
//          [out] the RX SDMA buffer size.
//
//      buffTx
//          [out] the RX SDMA buffer size 
// Returns:
//      
//
//-----------------------------------------------------------------------------
VOID BSPGetDMABuffSize(UINT16* buffRx, UINT16 * buffTx)
{
    *buffRx = SERIAL_SDMA_RX_BUFFER_SIZE;
    *buffTx = SERIAL_SDMA_TX_BUFFER_SIZE;
}
