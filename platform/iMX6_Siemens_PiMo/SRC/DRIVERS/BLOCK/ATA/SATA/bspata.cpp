//------------------------------------------------------------------------------
//
// Copyright (C) 2006-2011 Freescale Semiconductor, Inc. All Rights Reserved.
// THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
// AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  bspata.cpp
//
//  Provides BSP-specific configuration routines for ATA module.
//
//------------------------------------------------------------------------------

#include <windows.h>
#include "bsp.h"
#include "bsp_clocks.h"

//------------------------------------------------------------------------------
// External Variables
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Global Variables
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Defines
//------------------------------------------------------------------------------

#ifdef DEBUG
#define ZONE_FUNCTION         DEBUGZONE(3)
#endif

//------------------------------------------------------------------------------
// Types
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Local Variables
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Local Functions

//-----------------------------------------------------------------------------
//
// Function:  BSPATAPinMux
//
// This function init ATA pin mux
//
// Parameters:
//        none
//
// Returns:
//        Returns TRUE if successful, otherwise returns FALSE
//
//-----------------------------------------------------------------------------

BOOL BSPATAPinMux(void)
{

    return TRUE;    
}

//-----------------------------------------------------------------------------
//
// Function:  BSPATASupported 
//
// This function checks if SATA is supported on this board
//
// Parameters:
//        none
//
// Returns:
//        Returns TRUE if successful, otherwise returns FALSE
//
//-----------------------------------------------------------------------------
BOOL BSPATASupported()
{
	// Structure containing SoC supported features
	OAL_PLAT_INFO pInfo;

	//Get Processor type
	KernelIoControl(IOCTL_PLATFORM_INFO, NULL, 0, &pInfo, sizeof(OAL_PLAT_INFO), NULL);

	//RETAILMSG(FALSE, ((L"Initialize SATA  board : [x0%x]\r\n"),pInfo.ChipType));
	// Check if SATA is supported  on this board
	if(CHIP_MX6DQ != pInfo.ChipType)
	{
		RETAILMSG(TRUE, ((L"WARNING: SATA is not supported on this board : [0x%x]\r\n"),pInfo.ChipType));
		return FALSE;   
	}
	return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPATAPadConfig
//
// This function init ATA pad config
//
// Parameters:
//        none
//
// Returns:
//        Returns TRUE if successful, otherwise returns FALSE
//
//-----------------------------------------------------------------------------

BOOL BSPATAPadConfig(void)
{

    return TRUE;    
}

//-----------------------------------------------------------------------------
//
// Function:  BSPATAPower
//
// This function init ATA power
//
// Parameters:
//        none
//
// Returns:
//        Returns TRUE if successful, otherwise returns FALSE
//
//-----------------------------------------------------------------------------

BOOL BSPATAPower(void)
{

    return TRUE;    
}

//------------------------------------------------------------------------------
//
//       Function:  BSPATAIOMUXConfig
//
//             This function makes the DDK call to configure the IOMUX
//             pins required for the ATA.
//
//      Parameters:
//             none
//
//      Returns:
//             TRUE if success; FALSE if failure.
//
//------------------------------------------------------------------------------

BOOL BSPATAIOMUXConfig(void)
{

    return TRUE;    
}

//------------------------------------------------------------------------------
//
//       Function:  ConfigureSataPhy
//
//             This function configures the SATA PHY by writting the GPR13 register
//				Note :  this function alsa enables the SATA PHY pll bby writting to
//						bit 2 of the GPR13 register. This feature is not documented
//						in the imx6 TRM rev D, but is used in WCE7 and linux init
//						code.
//
//      Parameters:
//             none
//
//      Returns:
//             none
//
//------------------------------------------------------------------------------
VOID ConfigureSataPhy()
{
	PHYSICAL_ADDRESS phyAddr;

	phyAddr.QuadPart = CSP_BASE_REG_PA_IOMUXC;
	PCSP_IOMUX_REGS pIOMUX = (PCSP_IOMUX_REGS)MmMapIoSpace(phyAddr,sizeof(CSP_BASE_REG_PA_IOMUXC), FALSE);

	// PHY configuration
	// rx_eq_val_0		iomuxc_gpr13[26:24]
	// los_lvl			iomuxc_gpr13[23:19]
	// rx_dpll_mode_0	iomuxc_gpr13[18:16]
	// sata_speed		iomuxc_gpr13[15]
	// mpll_ss_en		iomuxc_gpr13[14]
	// tx_atten_0		iomuxc_gpr13[13:11]
	// tx_boost_0		iomuxc_gpr13[10:7]
	// tx_lvl			iomuxc_gpr13[6:2]
	// mpll_ck_off		iomuxc_gpr13[1]		** This is not ducumented in rev D of the imx6 TRM,  **
	//										** but appears in linux and CE7 initialization codes **
	// tx_edgerate_0	iomuxc_gpr13[0]

	// Configure PHY
	INSREG32(&pIOMUX->GPR[13], 0xFFFFFFFD, 0x0593A044);

	// Enable PHY PLL
	INSREG32(&pIOMUX->GPR[13], 0x00000003, 0x2);

	MmUnmapIoSpace(pIOMUX, sizeof(CSP_BASE_REG_PA_IOMUXC));
}

//------------------------------------------------------------------------------
//
//       Function:  EnableSataRefClock
//
//             This function enables the SATA ref clock from ethernet PLL
//				Ethernet PLL is responsible for generating the 100MHz SATA
//				reference clock.
//			   Note: We have to do stuff by hands here since DDK does not really deal with this clock
//				(comment from DDK: )
//				 "The following PLL6~PLL8 are not explicitly in clock tree, we need"
//				 "to consider expose a interface to open them"
//				(PLL8 IS the ethernet PLL)
//
//      Parameters:
//             none
//
//      Returns:
//             none
//
//------------------------------------------------------------------------------
void EnableSataRefClock()
{
	PHYSICAL_ADDRESS phyAddr;
	PCSP_PLL_BASIC_REGS pPLL_ENET;

	// Found no way from DDK to enable SATA ref clock, do it by hands...
	phyAddr.QuadPart = CSP_BASE_REG_PA_PLL_ENET; 
	pPLL_ENET = (PCSP_PLL_BASIC_REGS) MmMapIoSpace(phyAddr, sizeof(CSP_PLL_BASIC_REGS),FALSE);

	// Enable SATA 100MHz output
	INSREG32BF(&pPLL_ENET->CTRL, PLL_ENET_CTRL_ENABLE_SATA, 0x1);
	// Clear PLL POWERDOWN
	INSREG32BF(&pPLL_ENET->CTRL, PLL_ENET_CTRL_POWERDOWN, 0x0);
	// Clear PLL BYPASS
	INSREG32BF(&pPLL_ENET->CTRL, PLL_ENET_CTRL_BYPASS, 0x0);
	// Wait until PLL is locked
	while (PLL_CTL_NO_LOCKED == EXTREG32BF(&pPLL_ENET->CTRL, PLL_ENET_CTRL_LOCK));

	MmUnmapIoSpace(pPLL_ENET, sizeof(CSP_PLL_BASIC_REGS));

	// If we don't sleep there, device won't be detected (spin up detection timeout),
	// maybe PLL needs some time to propagate ?
	Sleep(1);
}

//------------------------------------------------------------------------------
//
//       Function:  void BSPBoard(UINT32* pValue)
//
//             This function initializes SATA clocks and PHY.
//
//      Parameters:
//             pValue [out]
//					value of the register to configure PHY. Since we use GPR13 to
//					configure PHY we set in to 0 to indicate to the common part
//					that no additional configuration has to be done
//
//      Returns:
//             none
//
//------------------------------------------------------------------------------
void BSPBoard(UINT32* pValue)
{
	// Enable SATA PLL (100MHz)
    DDKClockSetGatingMode (DDK_CLOCK_GATE_INDEX_SATA, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
	
	// Configure SATA PHY from GPR13
	ConfigureSataPhy();

	// Enable SATA ref clock (100MHz) from CCM_ANALOG_PLL_ENET
	EnableSataRefClock();

	// SATA PHY was configured in ConfigureSataPhy, don't use this
	*pValue = 0;
}

//------------------------------------------------------------------------------
//
//       Function:  void BSPSetPower(BOOL fOn)
//
//             This function sets power state
//
//      Parameters:
//             fOn
//					Power state to be set
//
//      Returns:
//             none
//
//		Note: 
//			   Currently we only turn on or off the SATA PLL.
//			   PHY and ENET PLL could also be disabled to achieve better performances.
//
//------------------------------------------------------------------------------
void BSPSetPower(BOOL fOn)
{
    if (fOn == FALSE) // Disable SATA PLL
    {
        DDKClockSetGatingMode (DDK_CLOCK_GATE_INDEX_SATA, DDK_CLOCK_GATE_MODE_DISABLED);    
    }
    else
    {
        DDKClockSetGatingMode (DDK_CLOCK_GATE_INDEX_SATA, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
    }
}
