//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES.

/*---------------------------------------------------------------------------
* Copyright (C) 2010, Freescale Semiconductor, Inc. All Rights Reserved.
* THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
* AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT 
*--------------------------------------------------------------------------*/
//
// GIISR.c
//
// Generic Installable Interrupt Service Routine.
//
//
#pragma warning(disable: 4115 4100 4213 4214)
#include <windows.h>
#include <nkintr.h>
#include <ceddk.h>
#include <giisr.h>
#include "bsp.h"

#ifdef DEBUG
#define ZONE_FUNCTION         DEBUGZONE(3)
#endif

#define GIISR_ClockSetGatingMode(index, mode) \
    INSREG32(&g_pCCM->CCGR[CCM_CGR_INDEX(index)], CCM_CGR_MASK(index), \
             CCM_CGR_VAL(index, mode))

#define GIISR_ClockGetGatingMode(index, mode) \
    mode = ((INREG32(&g_pCCM->CCGR[CCM_CGR_INDEX(index)])) >> CCM_CGR_SHIFT(index)) & CCM_CGR_CG_MASK;

PCSP_CCM_REGS g_pCCM = NULL;

// Globals
static GIISR_INFO g_Info[MAX_GIISR_INSTANCES];
static DWORD g_PortValue[MAX_GIISR_INSTANCES];
static BOOL g_InstanceValid[MAX_GIISR_INSTANCES] = {FALSE};
static DWORD g_Instances = 0;


/*
 @doc INTERNAL
 @func    BOOL | DllEntry | Process attach/detach api.
 *
 @rdesc The return is a BOOL, representing success (TRUE) or failure (FALSE).
 */
BOOL __stdcall
DllEntry(
    HINSTANCE hinstDll,         // @parm Instance pointer.
    DWORD dwReason,             // @parm Reason routine is called.
    LPVOID lpReserved           // @parm system parameter.
     )
{
     if (dwReason == DLL_PROCESS_ATTACH) {
     }

     if (dwReason == DLL_PROCESS_DETACH) {
     }

     return TRUE;
}


DWORD CreateInstance(
    void
    )
{
    DWORD InstanceIndex;
    PHYSICAL_ADDRESS pa;

    if(g_pCCM == NULL)
    {   
        pa.QuadPart = CSP_BASE_REG_PA_CCM;
        g_pCCM = (PCSP_CCM_REGS) MmMapIoSpace(pa, sizeof(CSP_CCM_REGS), FALSE);
        if(g_pCCM == NULL)
        {
            return (DWORD)-1;
        }
    }
    
    if (g_Instances >= MAX_GIISR_INSTANCES) {
        return (DWORD)-1;
    }
    
    g_Instances++;

    // Search for next free instance
    for (InstanceIndex = 0; InstanceIndex < MAX_GIISR_INSTANCES; InstanceIndex++) {
        if (!g_InstanceValid[InstanceIndex]) break;
    }

    // Didn't find a free instance (this shouldn't happen)
    if (InstanceIndex >= MAX_GIISR_INSTANCES) {
        return (DWORD)-1;
    }

    g_InstanceValid[InstanceIndex] = TRUE;
    
    // Initialize info
    g_Info[InstanceIndex].SysIntr = SYSINTR_CHAIN;
    g_Info[InstanceIndex].CheckPort = FALSE;
    g_Info[InstanceIndex].UseMaskReg = FALSE;

    // Initialize data
    g_PortValue[InstanceIndex] = 0;

    return InstanceIndex;
}


void DestroyInstance(
    DWORD InstanceIndex 
    )
{
    if (g_InstanceValid[InstanceIndex]) {
        g_InstanceValid[InstanceIndex] = FALSE;
        g_Instances--;
    }
    
    if(g_pCCM != NULL)
    {
        MmUnmapIoSpace((VOID*)g_pCCM, sizeof(CSP_CCM_REGS));
        g_pCCM = NULL;
    }
}


// The compiler generates a call to memcpy() for assignments of large objects.
// Since this library is not linked to the CRT, define our own copy routine.
void
InfoCopy(
    PGIISR_INFO dst,
    const GIISR_INFO *src
    )
{
    size_t count = sizeof(GIISR_INFO);
    
    while (count--) {
        *((PBYTE)dst)++ = *((PBYTE)src)++;
    }
}


BOOL 
IOControl(
    DWORD   InstanceIndex,
    DWORD   IoControlCode, 
    LPVOID  pInBuf, 
    DWORD   InBufSize,
    LPVOID  pOutBuf, 
    DWORD   OutBufSize, 
    LPDWORD pBytesReturned
    ) 
{
    if (pBytesReturned) {
        *pBytesReturned = 0;
    }

    switch (IoControlCode) {
    case IOCTL_GIISR_PORTVALUE:
        if ((OutBufSize != g_Info[InstanceIndex].PortSize) || !g_Info[InstanceIndex].CheckPort || !pOutBuf) {
            // Invalid size of output buffer, not checking port, or invalid output buffer pointer
            return FALSE;
        }

        if (pBytesReturned) {
            *pBytesReturned = g_Info[InstanceIndex].PortSize;
        }
        
        switch (g_Info[InstanceIndex].PortSize) {
        case sizeof(BYTE):
            *(BYTE *)pOutBuf = (BYTE)g_PortValue[InstanceIndex];
            break;
            
        case sizeof(WORD):
            *(WORD *)pOutBuf = (WORD)g_PortValue[InstanceIndex];
            break;

        case sizeof(DWORD):
            *(DWORD *)pOutBuf = g_PortValue[InstanceIndex];
            break;

        default:
            if (pBytesReturned) {
                *pBytesReturned = 0;
            }
            
            return FALSE;
        }

        break;
        
    case IOCTL_GIISR_INFO:
        // Copy instance information
        if ((InBufSize != sizeof(GIISR_INFO)) || !pInBuf) {
            // Invalid size of input buffer or input buffer pointer
            return FALSE;
        }

        // The compiler may generate a memcpy call for a structure assignment,
        // and we're not linking with the CRT, so use our own copy routine.
        InfoCopy(&g_Info[InstanceIndex], (PGIISR_INFO)pInBuf);

        break;

    default:
        // Invalid IOCTL
        return FALSE;
    }
    
    return TRUE;
}

DWORD
ISRHandler(
    DWORD InstanceIndex
    )
{
    DDK_CLOCK_GATE_MODE curIPGMode;
   
    DWORD Mask = g_Info[InstanceIndex].Mask;
    
    if (!g_Info[InstanceIndex].CheckPort) {
        return g_Info[InstanceIndex].SysIntr;
    }
    
    if (g_Info[InstanceIndex].PortAddr == 0 ) {
        return SYSINTR_CHAIN ;
    }

    g_PortValue[InstanceIndex] = 0;

    //before we access the memory, we should open all the USB clock
    GIISR_ClockGetGatingMode(DDK_CLOCK_GATE_INDEX_USBOH3, curIPGMode);

    GIISR_ClockSetGatingMode(DDK_CLOCK_GATE_INDEX_USBOH3, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
    
    // Check port
    if (g_Info[InstanceIndex].PortIsIO) {
        
        // I/O port
        switch (g_Info[InstanceIndex].PortSize) {
        case sizeof(BYTE):
            g_PortValue[InstanceIndex] = READ_PORT_UCHAR((PUCHAR)g_Info[InstanceIndex].PortAddr);

            if (g_Info[InstanceIndex].UseMaskReg) {
                // Read mask from mask register
                Mask = READ_PORT_UCHAR((PUCHAR)g_Info[InstanceIndex].MaskAddr);
            }
            
            break;

        case sizeof(WORD):
            g_PortValue[InstanceIndex] = READ_PORT_USHORT((PUSHORT)g_Info[InstanceIndex].PortAddr);

            if (g_Info[InstanceIndex].UseMaskReg) {
                // Read mask from mask register
                Mask = READ_PORT_USHORT((PUSHORT)g_Info[InstanceIndex].MaskAddr);
            }
            
            break;

        case sizeof(DWORD):
            g_PortValue[InstanceIndex] = READ_PORT_ULONG((PULONG)g_Info[InstanceIndex].PortAddr);

            if (g_Info[InstanceIndex].UseMaskReg) {
                // Read mask from mask register
                Mask = READ_PORT_ULONG((PULONG)g_Info[InstanceIndex].MaskAddr);
            }
            
            break;

        default:
            // No interrupt will be returned
            break;
        }
    } else {
        // Memory-mapped port
        
        switch (g_Info[InstanceIndex].PortSize) {
        case sizeof(BYTE):
            g_PortValue[InstanceIndex] = READ_REGISTER_UCHAR((BYTE *)g_Info[InstanceIndex].PortAddr);

            if (g_Info[InstanceIndex].UseMaskReg) {
                // Read mask from mask register
                Mask = READ_REGISTER_UCHAR((BYTE *)g_Info[InstanceIndex].MaskAddr);
            }
            
            break;

        case sizeof(WORD):
            g_PortValue[InstanceIndex] = READ_REGISTER_USHORT((WORD *)g_Info[InstanceIndex].PortAddr);

            if (g_Info[InstanceIndex].UseMaskReg) {
                // Read mask from mask register
                Mask = READ_REGISTER_USHORT((WORD *)g_Info[InstanceIndex].MaskAddr);
            }
            
            break;

        case sizeof(DWORD):
            g_PortValue[InstanceIndex] = READ_REGISTER_ULONG((DWORD *)g_Info[InstanceIndex].PortAddr);

            if (g_Info[InstanceIndex].UseMaskReg) {
                // Read mask from mask register
                Mask = READ_REGISTER_ULONG((DWORD *)g_Info[InstanceIndex].MaskAddr);
            }
            
            break;

        default:
            // No interrupt will be returned
            break;
        }    
    }
    
    //restore the state of CCM
    GIISR_ClockSetGatingMode(DDK_CLOCK_GATE_INDEX_USBOH3, curIPGMode);

    // If interrupt bit set, return corresponding SYSINTR
    return (g_PortValue[InstanceIndex] & Mask) ? g_Info[InstanceIndex].SysIntr : SYSINTR_CHAIN;
}
