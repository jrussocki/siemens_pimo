//-----------------------------------------------------------------------------
//
//  Copyright (C) 2013, Adeneo Embedded. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//-----------------------------------------------------------------------------
//
//  File: tempmon.c
//
//  The TEMPMON implementation for Freescale MX6x SoC.
//
//-----------------------------------------------------------------------------


#include <windows.h>
#include <ceddk.h>

#include "bsp.h"







////////////////////////////////////////////////////////////////////////
// Function: TEMPMONGetLastTemp
// Description:
// Really not sure about this one, should activate USB1_PLL
//TEST
////////////////////////////////////////////////////////////////////////
BOOL BSPTEMPMONSetupClock()
{
    return DDKClockSetFreq(DDK_CLOCK_SIGNAL_PLL3_USBOTG, FREQ_480MHZ);
}
