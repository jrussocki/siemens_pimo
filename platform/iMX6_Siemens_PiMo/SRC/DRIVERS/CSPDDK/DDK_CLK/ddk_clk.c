//------------------------------------------------------------------------------
//
//  Copyright (C) 2007-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  ddk_clk.c
//
//  This file contains the CCM DDK interface that is used by applications and
//  other drivers to access the capabilities of the CCM driver.
//
//-----------------------------------------------------------------------------

#pragma warning(push)
#pragma warning(disable: 4115 4201 4204 4214)
#include <windows.h>
#include <ceddk.h>
#pragma warning(pop)

#include "bsp.h"
#include "dvfs.h"

//-----------------------------------------------------------------------------
// External Functions
VOID DDKClockLock(VOID);
VOID DDKClockUnlock(VOID);


//-----------------------------------------------------------------------------
// External Variables
extern PDDK_CLK_CONFIG g_pDdkClkConfig;
extern HANDLE g_hDvfcWorkerEvent;
extern PCSP_CCM_REGS g_pCCM;

//-----------------------------------------------------------------------------
// Defines
#define AUDIO_VIDEO_MIN_CLK_FREQ 100000000

//-----------------------------------------------------------------------------
// Types


//-----------------------------------------------------------------------------
// Global Variables


//-----------------------------------------------------------------------------
// Local Variables
static BSP_ARGS *g_pBspArgs = NULL;
static HANDLE g_hSetPointEvent = NULL;

static PCSP_PLL_BASIC_REGS g_pPLL_CPU = NULL;
static PCSP_PLL_BASIC_REGS g_pPLL_USBOTG = NULL;
static PCSP_PLL_BASIC_REGS g_pPLL_USBHOST = NULL;
static PCSP_PLL_BUS_REGS   g_pPLL_BUS = NULL;
static PCSP_PLL_MEDIA_REGS g_pPLL_AUD = NULL;
static PCSP_PLL_MEDIA_REGS g_pPLL_VIDEO = NULL;
static PCSP_PLL_BASIC_REGS g_pPLL_MLB = NULL;
static PCSP_PLL_BASIC_REGS g_pPLL_ENET = NULL;
static PCSP_PLL_BASIC_REGS g_pPLL_PFD_480MHZ = NULL;
static PCSP_PLL_BASIC_REGS g_pPLL_PFD_528MHZ = NULL;

static PCSP_GPC_REGS g_pGPC = NULL;

//-----------------------------------------------------------------------------
// Local Functions
VOID DumpCCMRegs();
VOID DumpAnatopRegs();

//-----------------------------------------------------------------------------
//
// Function:  sig2BaudSrc
//
// This function translate signal name to baudsrc name
//
// Parameters:
//      sigName - [in] signal name
//
// Returns:
//      Returns valid baudsrc name if there is a corrsponding.
//      Returns DDK_CLOCK_BAUD_SOURCE_ENUM_END if there is not a match
//
//-----------------------------------------------------------------------------
DDK_CLOCK_BAUD_SOURCE sig2BaudSrc(DDK_CLOCK_SIGNAL sigName)
{
    DDK_CLOCK_BAUD_SOURCE baudSrcName;

    switch (sigName)
    {
        case DDK_CLOCK_SIGNAL_PLL1_CPU:
            baudSrcName = DDK_CLOCK_BAUD_SOURCE_PLL1_CPU;
            break;
        case DDK_CLOCK_SIGNAL_PLL2_BUS:
            baudSrcName = DDK_CLOCK_BAUD_SOURCE_PLL2_BUS;
            break;
        case DDK_CLOCK_SIGNAL_PLL3_USBOTG:
            baudSrcName = DDK_CLOCK_BAUD_SOURCE_PLL3_USBOTG;
            break;
        case DDK_CLOCK_SIGNAL_PLL4_AUDIO:
            baudSrcName = DDK_CLOCK_BAUD_SOURCE_PLL4_AUDIO;
            break;
        case DDK_CLOCK_SIGNAL_PLL5_VIDEO:
            baudSrcName = DDK_CLOCK_BAUD_SOURCE_PLL5_VIDEO;
            break;
        case DDK_CLOCK_SIGNAL_PLL6_MLB:
            baudSrcName = DDK_CLOCK_BAUD_SOURCE_PLL6_MLB;
            break;
        case DDK_CLOCK_SIGNAL_PLL7_USBHOST:
            baudSrcName = DDK_CLOCK_BAUD_SOURCE_PLL7_USBHOST;
            break;
        case DDK_CLOCK_SIGNAL_PLL8_ENET:
            baudSrcName = DDK_CLOCK_BAUD_SOURCE_PLL8_ENET;
            break;
        case DDK_CLOCK_SIGNAL_528M_PFD0_352M:
            baudSrcName = DDK_CLOCK_BAUD_SOURCE_528M_PFD0_352M;
            break;
        case DDK_CLOCK_SIGNAL_528M_PFD1_594M:
            baudSrcName = DDK_CLOCK_BAUD_SOURCE_528M_PFD1_594M;
            break;
        case DDK_CLOCK_SIGNAL_528M_PFD2_400M:
            baudSrcName = DDK_CLOCK_BAUD_SOURCE_528M_PFD2_400M;
            break;
        case DDK_CLOCK_SIGNAL_528M_PFD3_200M:
            baudSrcName = DDK_CLOCK_BAUD_SOURCE_528M_PFD3_200M;
            break;
        case DDK_CLOCK_SIGNAL_480M_PFD0_720M:
            baudSrcName = DDK_CLOCK_BAUD_SOURCE_480M_PFD0_720M;
            break;
        case DDK_CLOCK_SIGNAL_480M_PFD1_540M:
            baudSrcName = DDK_CLOCK_BAUD_SOURCE_480M_PFD1_540M;
            break;
        case DDK_CLOCK_SIGNAL_480M_PFD2_508M:
            baudSrcName = DDK_CLOCK_BAUD_SOURCE_480M_PFD2_508M;
            break;
        case DDK_CLOCK_SIGNAL_480M_PFD3_454M:
            baudSrcName = DDK_CLOCK_BAUD_SOURCE_480M_PFD3_454M;
            break;
        default:
            baudSrcName = DDK_CLOCK_BAUD_SOURCE_ENUM_END;
            break;
    }

    return baudSrcName;
}

//-----------------------------------------------------------------------------
//
// Function:  BSPClockDealloc
//
// This function deallocates the data structures required for interaction
// with the clock configuration hardware.
//
// Parameters:
//      None.
//
// Returns:
//      Returns TRUE.
//
//-----------------------------------------------------------------------------
BOOL BSPClockDealloc(void)
{
    // Unmap peripheral address space
    if (g_pBspArgs)
    {
        MmUnmapIoSpace(g_pBspArgs, sizeof(BSP_ARGS));
        g_pBspArgs = NULL;
    }

    if (g_pDdkClkConfig)
    {
        MmUnmapIoSpace(g_pDdkClkConfig, sizeof(DDK_CLK_CONFIG));
        g_pDdkClkConfig = NULL;
    }

    if (g_hSetPointEvent)
    {
        CloseHandle(g_hSetPointEvent);
        g_hSetPointEvent = NULL;
    }


    return TRUE;
}


//-----------------------------------------------------------------------------
//
// Function:  BSPClockAlloc
//
// This function allocates the data structures required for interaction
// with the clock configuration hardware.
//
// Parameters:
//      None.
//
// Returns:
//      Returns TRUE if successful, otherwise returns FALSE.
//
//-----------------------------------------------------------------------------
BOOL BSPClockAlloc(void)
{
    BOOL rc = FALSE;
    PHYSICAL_ADDRESS phyAddr;

    if (g_pBspArgs == NULL)
    {
        phyAddr.QuadPart = IMAGE_SHARE_ARGS_RAM_PA_START;

        // Map peripheral physical address to virtual address
        g_pBspArgs = (BSP_ARGS *) MmMapIoSpace(phyAddr, sizeof(BSP_ARGS),
            FALSE);

        // Check if virtual mapping failed
        if (g_pBspArgs == NULL)
        {
            DBGCHK((_T("CSPDDK")),  FALSE);
            ERRORMSG(TRUE, (_T("BSPClockAlloc:  MmMapIoSpace failed!\r\n")));
            goto cleanUp;
        }

    }

    if (g_pDdkClkConfig == NULL)
    {
        phyAddr.QuadPart = IMAGE_WINCE_DDKCLK_RAM_PA_START;

        // Map peripheral physical address to virtual address
        g_pDdkClkConfig = (PDDK_CLK_CONFIG) MmMapIoSpace(phyAddr, sizeof(DDK_CLK_CONFIG),
            FALSE);

        // Check if virtual mapping failed
        if (g_pDdkClkConfig == NULL)
        {
            DBGCHK((_T("CSPDDK")),  FALSE);
            ERRORMSG(TRUE, (_T("BSPClockAlloc:  MmMapIoSpace failed!\r\n")));
            goto cleanUp;
        }

    }

    /*
     * CPU
     */
    if (g_pPLL_CPU == NULL)
    {
        phyAddr.QuadPart = CSP_BASE_REG_PA_PLL_CPU;

        // map physical address to virtual address
        g_pPLL_CPU = (PCSP_PLL_BASIC_REGS) MmMapIoSpace(phyAddr, sizeof(CSP_PLL_BASIC_REGS), FALSE);

        if (g_pPLL_CPU == NULL)
        {
            DBGCHK((_T("CSPDDK")),  FALSE);
            ERRORMSG(TRUE, (_T("BSPClockAlloc:  MmMapIoSpace failed!\r\n")));
            goto cleanUp;
        }
    }

    /*
     * USBOTG
     */
    if (g_pPLL_USBOTG == NULL)
    {
        phyAddr.QuadPart = CSP_BASE_REG_PA_PLL_USBOTG;

        // map physical address to virtual address
        g_pPLL_USBOTG = (PCSP_PLL_BASIC_REGS) MmMapIoSpace(phyAddr, sizeof(CSP_PLL_BASIC_REGS), FALSE);

        if (g_pPLL_USBOTG == NULL)
        {
            DBGCHK((_T("CSPDDK")),  FALSE);
            ERRORMSG(TRUE, (_T("BSPClockAlloc:  MmMapIoSpace failed!\r\n")));
            goto cleanUp;
        }
    }

    /*
     * USBHOST
     */
    if (g_pPLL_USBHOST == NULL)
    {
        phyAddr.QuadPart = CSP_BASE_REG_PA_PLL_USBHOST;

        // map physical address to virtual address
        g_pPLL_USBHOST = (PCSP_PLL_BASIC_REGS) MmMapIoSpace(phyAddr, sizeof(CSP_PLL_BASIC_REGS), FALSE);

        if (g_pPLL_USBHOST == NULL)
        {
            DBGCHK((_T("CSPDDK")),  FALSE);
            ERRORMSG(TRUE, (_T("BSPClockAlloc:  MmMapIoSpace failed!\r\n")));
            goto cleanUp;
        }
    }

    /*
     * BUS
     */
    if (g_pPLL_BUS == NULL)
    {
        phyAddr.QuadPart = CSP_BASE_REG_PA_PLL_BUS;

        // map physical address to virtual address
        g_pPLL_BUS = (PCSP_PLL_BUS_REGS) MmMapIoSpace(phyAddr, sizeof(CSP_PLL_BUS_REGS), FALSE);

        if (g_pPLL_BUS == NULL)
        {
            DBGCHK((_T("CSPDDK")),  FALSE);
            ERRORMSG(TRUE, (_T("BSPClockAlloc:  MmMapIoSpace failed!\r\n")));
            goto cleanUp;
        }
    }

    /*
     * Audio
     */
    if (g_pPLL_AUD == NULL)
    {
        phyAddr.QuadPart = CSP_BASE_REG_PA_PLL_AUD;

        // map physical address to virtual address
        g_pPLL_AUD = (PCSP_PLL_MEDIA_REGS) MmMapIoSpace(phyAddr, sizeof(CSP_PLL_MEDIA_REGS), FALSE);

        if (g_pPLL_AUD == NULL)
        {
            DBGCHK((_T("CSPDDK")),  FALSE);
            ERRORMSG(TRUE, (_T("BSPClockAlloc:  MmMapIoSpace failed!\r\n")));
            goto cleanUp;
        }
    }

    /*
     * Video
     */
    if (g_pPLL_VIDEO == NULL)
    {
        phyAddr.QuadPart = CSP_BASE_REG_PA_PLL_VIDEO;

        // map physical address to virtual address
        g_pPLL_VIDEO = (PCSP_PLL_MEDIA_REGS) MmMapIoSpace(phyAddr, sizeof(CSP_PLL_MEDIA_REGS), FALSE);

        if (g_pPLL_VIDEO == NULL)
        {
            DBGCHK((_T("CSPDDK")),  FALSE);
            ERRORMSG(TRUE, (_T("BSPClockAlloc:  MmMapIoSpace failed!\r\n")));
            goto cleanUp;
        }
    }

    /*
     * MLB
     */
    if (g_pPLL_MLB == NULL)
    {
        phyAddr.QuadPart = CSP_BASE_REG_PA_PLL_MLB;

        // map physical address to virtual address
        g_pPLL_MLB = (PCSP_PLL_BASIC_REGS) MmMapIoSpace(phyAddr, sizeof(CSP_PLL_BASIC_REGS), FALSE);

        if (g_pPLL_MLB == NULL)
        {
            DBGCHK((_T("CSPDDK")),  FALSE);
            ERRORMSG(TRUE, (_T("BSPClockAlloc:  MmMapIoSpace failed!\r\n")));
            goto cleanUp;
        }
    }

    /*
     * Enet
     */
    if (g_pPLL_ENET == NULL)
    {
        phyAddr.QuadPart = CSP_BASE_REG_PA_PLL_ENET;

        // map physical address to virtual address
        g_pPLL_ENET = (PCSP_PLL_BASIC_REGS) MmMapIoSpace(phyAddr, sizeof(CSP_PLL_BASIC_REGS), FALSE);

        if (g_pPLL_ENET == NULL)
        {
            DBGCHK((_T("CSPDDK")),  FALSE);
            ERRORMSG(TRUE, (_T("BSPClockAlloc:  MmMapIoSpace failed!\r\n")));
            goto cleanUp;
        }
    }

    /*
     * PFD_480
     */
    if (g_pPLL_PFD_480MHZ == NULL)
    {
        phyAddr.QuadPart = CSP_BASE_REG_PA_PLL_PFD_480MHZ;

        // map physical address to virtual address
        g_pPLL_PFD_480MHZ = (PCSP_PLL_BASIC_REGS) MmMapIoSpace(phyAddr, sizeof(CSP_PLL_BASIC_REGS), FALSE);

        if (g_pPLL_PFD_480MHZ == NULL)
        {
            DBGCHK((_T("CSPDDK")),  FALSE);
            ERRORMSG(TRUE, (_T("BSPClockAlloc:  MmMapIoSpace failed!\r\n")));
            goto cleanUp;
        }
    }

    /*
     * PFD_528
     */
    if (g_pPLL_PFD_528MHZ == NULL)
    {
        phyAddr.QuadPart = CSP_BASE_REG_PA_PLL_PFD_528MHZ;

        // map physical address to virtual address
        g_pPLL_PFD_528MHZ = (PCSP_PLL_BASIC_REGS) MmMapIoSpace(phyAddr, sizeof(CSP_PLL_BASIC_REGS), FALSE);

        if (g_pPLL_PFD_528MHZ == NULL)
        {
            DBGCHK((_T("CSPDDK")),  FALSE);
            ERRORMSG(TRUE, (_T("BSPClockAlloc:  MmMapIoSpace failed!\r\n")));
            goto cleanUp;
        }
    }

    if (g_pGPC == NULL)
    {
        phyAddr.QuadPart = CSP_BASE_REG_PA_GPC;

        // Map peripheral physical address to virtual address
        g_pGPC = (PCSP_GPC_REGS) MmMapIoSpace(phyAddr, sizeof(CSP_GPC_REGS),
            FALSE);

        // Check if virtual mapping failed
        if (g_pGPC == NULL)
        {
            DBGCHK((_T("CSPDDK")),  FALSE);
            ERRORMSG(TRUE, (_T("BSPClockAlloc:  MmMapIoSpace failed!\r\n")));
            goto cleanUp;
        }
    }

    // Create event to sync with DVFC setpoint transitions
    if (g_hSetPointEvent == NULL)
    {
        g_hSetPointEvent = CreateEvent(NULL, TRUE, FALSE, L"EVENT_SETPOINT");

        if (g_hSetPointEvent == NULL)
        {
            ERRORMSG(1, (_T("CreateEvent failed!\r\n")));
            goto cleanUp;
        }
    }

#if 0
    DumpCCMRegs();
    DumpAnatopRegs();
#endif

    rc = TRUE;

cleanUp:

    if (!rc) BSPClockDealloc();

    return rc;
}


//-----------------------------------------------------------------------------
//
// Function: BSPClockGetFreq
//
// Retrieves the clock frequency in Hz for the specified clock signal.
//
// Parameters:
//      sig
//           [in] Clock signal.
//
//      freq
//           [out] Current frequency in Hz.
//
// Returns:
//      Returns TRUE if successful, otherwise returns FALSE.
//
//-----------------------------------------------------------------------------
BOOL BSPClockGetFreq(DDK_CLOCK_SIGNAL sig, UINT32 *freq)
{
    *freq = g_pBspArgs->clockFreq[sig];

    return TRUE;
}


//-----------------------------------------------------------------------------
//
// Function: BSPClockSetFreq
//
// Sets the clock frequency in Hz for the specified clock signal.
//
// Parameters:
//      sig
//           [in] Clock signal.
//
//      freq
//           [in] Requested frequency in Hz.
//
// Returns:
//      Returns TRUE if successful, otherwise returns FALSE.
//
//-----------------------------------------------------------------------------
// MX6_BRING_UP
BOOL BSPClockSetFreq(DDK_CLOCK_SIGNAL sig, UINT32 freq)
{
    UINT32 curFreq;
    UINT32 refFreq;
    UINT32 num, denom;
    UINT32 div = 0;
    BOOL rc = TRUE;
    DDK_CLOCK_BAUD_SOURCE baudSrc;

    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] BSPClockSetFreq Sig [%s], freq [%d] +++\r\n", ClkSigName[sig], freq));

    if ((baudSrc = sig2BaudSrc(sig)) != DDK_CLOCK_BAUD_SOURCE_ENUM_END)
    {
        // PLLs can only be updated when there are no active references    
        if (g_pDdkClkConfig->rootRefCount[baudSrc] != 0)
        {
            ERRORMSG(TRUE, (_T("Cannot update PLL[%s] with active references!\r\n"), ClkSigName[sig]));
            return FALSE;
        }
    }
    else
    {
        RETAILMSG(TRUE, (L"[WARNING] Can't translate sig [%s] to BaudName\r\n", ClkSigName[sig]));
    }


    BSPClockGetFreq(sig, &curFreq);
    if (curFreq == freq)
    {
        RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] already in this freq, nothing to do\r\n"));
        return TRUE;
    }

    switch (sig)
    {
        case DDK_CLOCK_SIGNAL_PLL1_CPU:
            // Fout = Fin * Div / 2;
            refFreq = FREQ_24MHZ;
            div = ROUND_DIV(freq * 2, refFreq);
            div = (div < 54) ? 54 : div;
            div = (div > 108) ? 108 : div;
            INSREG32BF(&g_pPLL_CPU->CTRL, PLL_CPU_CTRL_DIV_SELECT, div);
            freq = ROUND_DIV(refFreq * div, 2);
            RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] PLL1 div %d, freq %d\r\n", div, freq));

            if (!EXTREG32BF(&g_pPLL_CPU->CTRL, PLL_CPU_CTRL_LOCK))
            {
                // Not Locked, Try to Enable
                // Clear Power Down
                OUTREG32(&g_pPLL_CPU->CTRL_CLR, 1 << PLL_CPU_CTRL_POWERDOWN_LSH);
                // Enable
                OUTREG32(&g_pPLL_CPU->CTRL_SET, 1 << PLL_CPU_CTRL_ENABLE_LSH);
                // Bypass
                OUTREG32(&g_pPLL_CPU->CTRL_CLR, 1 << PLL_CPU_CTRL_BYPASS_LSH);
                // Wait Lock
                while (EXTREG32BF(&g_pPLL_CPU->CTRL, PLL_CPU_CTRL_LOCK) == PLL_CTL_NO_LOCKED);
            }
            RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] Lock Done\r\n"));
            break;

        case DDK_CLOCK_SIGNAL_PLL2_BUS:
            // Fout = 480M or 528M
            // There is NUM and DENOM registers for PLL2_528, don't see necessary to set them
            switch (freq)
            {
                case FREQ_480MHZ:
                    div = 0;
                    break;
                case FREQ_528MHZ:
                    div = 1;
                    break;
                default:
                    rc = FALSE;
                    ERRORMSG(1, (L"BAD BUS FREQ SETTING, PLS CHECK.\r\n"));
            }
            RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] PLL2 div %d\r\n", div));
            INSREG32BF(&g_pPLL_BUS->CTRL, PLL_BUS_CTRL_DIV_SELECT, div);

            if (!EXTREG32BF(&g_pPLL_BUS->CTRL, PLL_CPU_CTRL_LOCK))
            {
                // Not Locked, Try to Enable
                // Clear Power Down
                OUTREG32(&g_pPLL_BUS->CTRL_CLR, 1 << PLL_BUS_CTRL_POWERDOWN_LSH);
                // Enable
                OUTREG32(&g_pPLL_BUS->CTRL_SET, 1 << PLL_BUS_CTRL_ENABLE_LSH);
                // Bypass
                OUTREG32(&g_pPLL_BUS->CTRL_CLR, 1 << PLL_BUS_CTRL_BYPASS_LSH);
                // Wait Lock
                while (EXTREG32BF(&g_pPLL_BUS->CTRL, PLL_BUS_CTRL_LOCK) == PLL_CTL_NO_LOCKED);
            }
            RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] Lock Done\r\n"));
            break;

        case DDK_CLOCK_SIGNAL_PLL3_USBOTG:
            // Fout = 480M or 528M
            switch (freq)
            {
                case FREQ_480MHZ:
                    div = 0;
                    break;
                case FREQ_528MHZ:
                    div = 1;
                    break;
                default:
                    rc = FALSE;
                    ERRORMSG(1, (L"BAD OTG FREQ SETTING, PLS CHECK.\r\n"));
            }
            RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] PLL3 div %d\r\n", div));
            INSREG32BF(&g_pPLL_USBOTG->CTRL, PLL_USBOTG_CTRL_DIV_SELECT, div);

            if (!EXTREG32BF(&g_pPLL_USBOTG->CTRL, PLL_CPU_CTRL_LOCK))
            {
                // Not Locked, Try to Enable
                // Clear Power Down
                OUTREG32(&g_pPLL_USBOTG->CTRL_SET, 1 << PLL_USBOTG_CTRL_POWER_LSH);
                // Enable
                OUTREG32(&g_pPLL_USBOTG->CTRL_SET, 1 << PLL_USBOTG_CTRL_ENABLE_LSH);
                // Bypass
                OUTREG32(&g_pPLL_USBOTG->CTRL_CLR, 1 << PLL_USBOTG_CTRL_BYPASS_LSH);
                // Wait Lock
                while (EXTREG32BF(&g_pPLL_USBOTG->CTRL, PLL_USBOTG_CTRL_LOCK) == PLL_CTL_NO_LOCKED);
            }
            RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] Lock Done\r\n"));
            break;

        case DDK_CLOCK_SIGNAL_PLL4_AUDIO:
            // Fout = Fin * (MFI + MFD / MFN)
            refFreq = FREQ_24MHZ;
            div = freq / refFreq;
            num = freq % refFreq;
            denom = refFreq;
            RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] PLL4 div %d, NUM %d, DENUM %d, freq %d\r\n", div, num, denom, freq));

            INSREG32BF(&g_pPLL_AUD->CTRL, PLL_MEDIA_CTRL_DIV_SELECT, div);
            if ((div < 27) || (div > 54))
            {
                RETAILMSG(TRUE, (L"WARNING!!! AUDIO divider not in 27 - 54\r\n"));
            }
            OUTREG32(&g_pPLL_AUD->NUM, num);
            OUTREG32(&g_pPLL_AUD->DENOM, denom);
            
            if (!EXTREG32BF(&g_pPLL_AUD->CTRL, PLL_CPU_CTRL_LOCK))
            {
                // Not Locked, Try to Enable
                // Clear Power Down
                OUTREG32(&g_pPLL_AUD->CTRL_CLR, 1 << PLL_MEDIA_CTRL_POWERDOWN_LSH);
                // Enable
                OUTREG32(&g_pPLL_AUD->CTRL_SET, 1 << PLL_MEDIA_CTRL_ENABLE_LSH);
                // Bypass
                OUTREG32(&g_pPLL_AUD->CTRL_CLR, 1 << PLL_MEDIA_CTRL_BYPASS_LSH);
                // Wait Lock
                while (EXTREG32BF(&g_pPLL_AUD->CTRL, PLL_MEDIA_CTRL_LOCK) == PLL_CTL_NO_LOCKED);
            }
            RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] Lock Done\r\n"));
            break;

        case DDK_CLOCK_SIGNAL_PLL5_VIDEO:
#ifdef Fix_HDMI
            {
                UINT32  parent_rate = FREQ_24MHZ;
                UINT32  test_div_sel = 2;
                UINT32  control3 = 0;
                UINT32  pre_div_rate;
                UINT32  mfn, mfd = 1000000;
                INT64   temp64;
                UINT32  div;
                PHYSICAL_ADDRESS    phyAddr;
                PCSP_ANAMISC_REGS   g_AnaMiscRegs = NULL;

                phyAddr.QuadPart = CSP_BASE_REG_PA_ANAMISC;

                g_AnaMiscRegs = (PCSP_ANAMISC_REGS) MmMapIoSpace(phyAddr, sizeof(PCSP_ANAMISC_REGS), FALSE);
                if (g_AnaMiscRegs == NULL)
                {
                    RETAILMSG(TRUE, (L"WARNING!!! MmMapIoSpace for PCSP_ANAMISC_REGS failed \r\n"));
                }

                pre_div_rate = freq;

#if 0
                int rev = mx6q_revision();
                if ((rev >= IMX_CHIP_REVISION_1_1) || cpu_is_mx6dl())
#endif
                {
                    while (pre_div_rate < AUDIO_VIDEO_MIN_CLK_FREQ)
                    {
                        pre_div_rate *= 2;
                        /*
                         * test_div_sel field values:
                         * 2 -> Divide by 1
                         * 1 -> Divide by 2
                         * 0 -> Divide by 4
                         *
                         * control3 field values:
                         * 0 -> Divide by 1
                         * 1 -> Divide by 2
                         * 3 -> Divide by 4
                         */
                        
                        if (test_div_sel != 0)
                            test_div_sel--;
                        else
                        {
                            control3++;
                            if (control3 == 2)
                                control3++;
                        }
                    }
                }
            
                div = pre_div_rate / parent_rate;
                temp64 = (UINT64) (pre_div_rate - (div * parent_rate));
                temp64 *= mfd;
                temp64 = temp64 / parent_rate;
                mfn = (UINT32)temp64;

                RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] PLL5 div %d, NUM %d, DENUM %d, freq %d\r\n", div, num, denom, freq));

                INSREG32BF(&g_pPLL_VIDEO->CTRL, PLL_MEDIA_CTRL_TEST_DIV_SELECT, test_div_sel); 
                INSREG32BF(&g_pPLL_VIDEO->CTRL, PLL_MEDIA_CTRL_DIV_SELECT, div);
                if ((div < 27) || (div > 54))
                {
                    RETAILMSG(TRUE, (L"WARNING!!! VIDEO divider not in 27 - 54\r\n"));
                }
                OUTREG32(&g_pPLL_VIDEO->NUM, mfn);
                OUTREG32(&g_pPLL_VIDEO->DENOM, mfd);
#if 0
                if (rev >= IMX_CHIP_REVISION_1_1 && g_AnaMiscRegs)
                    INSREG32BF(&g_AnaMiscRegs->ANA_MISC2, ANALOG_MISC2_CONTROL3, control3); 
#else
                if (g_AnaMiscRegs)
                    INSREG32BF(&g_AnaMiscRegs->ANA_MISC2, ANALOG_MISC2_CONTROL3, control3); 
#endif
                if (g_AnaMiscRegs)
                {
                    MmUnmapIoSpace(g_AnaMiscRegs, sizeof(PCSP_ANAMISC_REGS));
                    g_AnaMiscRegs = NULL;
                }
            }
#else
            // Fout = Fin * (MFI + MFD / MFN)
            refFreq = FREQ_24MHZ;
            div = freq / refFreq;
            num = freq % refFreq;
            denom = refFreq;
            RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] PLL5 div %d, NUM %d, DENUM %d, freq %d\r\n", div, num, denom, freq));

            INSREG32BF(&g_pPLL_VIDEO->CTRL, PLL_MEDIA_CTRL_DIV_SELECT, div);
            if ((div < 27) || (div > 54))
            {
                RETAILMSG(TRUE, (L"WARNING!!! VIDEO divider not in 27 - 54\r\n"));
            }
            OUTREG32(&g_pPLL_VIDEO->NUM, num);
            OUTREG32(&g_pPLL_VIDEO->DENOM, denom);
#endif

            if (!EXTREG32BF(&g_pPLL_VIDEO->CTRL, PLL_CPU_CTRL_LOCK))
            {
                // Not Locked, Try to Enable
                // Clear Power Down
                OUTREG32(&g_pPLL_VIDEO->CTRL_CLR, 1 << PLL_MEDIA_CTRL_POWERDOWN_LSH);
                // Enable
                OUTREG32(&g_pPLL_VIDEO->CTRL_SET, 1 << PLL_MEDIA_CTRL_ENABLE_LSH);
                // Bypass
                OUTREG32(&g_pPLL_VIDEO->CTRL_CLR, 1 << PLL_MEDIA_CTRL_BYPASS_LSH);
                // Wait Lock
                while (EXTREG32BF(&g_pPLL_VIDEO->CTRL, PLL_MEDIA_CTRL_LOCK) == PLL_CTL_NO_LOCKED);
            }
            RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] Lock Done\r\n"));
            break;

        case DDK_CLOCK_SIGNAL_PLL6_MLB:
            // Undocumented, TODO
            break;

        case DDK_CLOCK_SIGNAL_PLL7_USBHOST:
            // Fout = 480M or 528M
            switch (freq)
            {
                case FREQ_480MHZ:
                    div = 0;
                    break;
                case FREQ_528MHZ:
                    div = 1;
                    break;
                default:
                    rc = FALSE;
                    ERRORMSG(1, (L"BAD BUS FREQ SETTING, PLS CHECK.\r\n"));
            }
            INSREG32BF(&g_pPLL_USBHOST->CTRL, PLL_USBHOST_CTRL_DIV_SELECT, div);
            RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] PLL7 div %d\r\n", div));
            
            if (!EXTREG32BF(&g_pPLL_USBHOST->CTRL, PLL_CPU_CTRL_LOCK))
            {
                // Not Locked, Try to Enable
                // Clear Power Down
                OUTREG32(&g_pPLL_USBHOST->CTRL_SET, 1 << PLL_USBHOST_CTRL_POWER_LSH);
                // Enable
                OUTREG32(&g_pPLL_USBHOST->CTRL_SET, 1 << PLL_USBHOST_CTRL_ENABLE_LSH);
                // Bypass
                OUTREG32(&g_pPLL_USBHOST->CTRL_CLR, 1 << PLL_USBHOST_CTRL_BYPASS_LSH);
                // Wait Lock
                while (EXTREG32BF(&g_pPLL_USBHOST->CTRL, PLL_USBHOST_CTRL_LOCK) == PLL_CTL_NO_LOCKED);
            }
            RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] Lock Done\r\n"));
            break;

        case DDK_CLOCK_SIGNAL_PLL8_ENET:
            switch (freq)
            {
                case FREQ_25MHZ:
                    div = 0;
                    break;
                case FREQ_50MHZ:
                    div = 1;
                    break;
                case FREQ_100MHZ:
                    div = 2;
                    break;
                case FREQ_125MHZ:
                    div = 3;
                    break;
                default:
                    rc = FALSE;
                    ERRORMSG(1, (L"BAD ENET FREQ SETTING, PLS CHECK.\r\n"));
                    break;
            }
            INSREG32BF(&g_pPLL_ENET->CTRL, PLL_ENET_CTRL_DIV_SELECT, div);
            RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] PLL8 div %d\r\n", div));

            if (!EXTREG32BF(&g_pPLL_ENET->CTRL, PLL_CPU_CTRL_LOCK))
            {
                // Not Locked, Try to Enable
                // Clear Power Down
                OUTREG32(&g_pPLL_ENET->CTRL_CLR, 1 << PLL_ENET_CTRL_POWERDOWN_LSH);
                // Enable
                OUTREG32(&g_pPLL_ENET->CTRL_SET, 1 << PLL_ENET_CTRL_ENABLE_LSH);
                // Bypass
                OUTREG32(&g_pPLL_ENET->CTRL_CLR, 1 << PLL_ENET_CTRL_BYPASS_LSH);
                // Wait Lock
                while (EXTREG32BF(&g_pPLL_ENET->CTRL, PLL_ENET_CTRL_LOCK) == PLL_CTL_NO_LOCKED);
            }
            RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] Lock Done\r\n"));
            break;

        case DDK_CLOCK_SIGNAL_528M_PFD0_352M:
            // Fout = Fin * 18 / Div
            BSPClockGetFreq(DDK_CLOCK_SIGNAL_PLL2_BUS, &refFreq);
            if (refFreq == 0)
            {
                ERRORMSG(1, (L"PLEASE OPEN PLL2 FIRST\r\n"));   
                rc = FALSE;
            }
            else
            {
                div = ROUND_DIV((LONGLONG)refFreq * 18, freq);
                div = (div < 12) ? 12 : div;
                div = (div > 35) ? 35 : div;
                INSREG32BF(&g_pPLL_PFD_528MHZ->CTRL, PLL_PFD_CTRL_PFD0_FRAC, div);
                freq = ROUND_DIV((LONGLONG)refFreq * 18, div);
                RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] PFD_352 div %d, freq %d\r\n", div, freq));

                // Make sure if PFD clock is ungated
                if (EXTREG32BF(&g_pPLL_PFD_528MHZ->CTRL, PLL_PFD_CTRL_PFD0_CLKGATE))
                {
                    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] CLR PFD GATE\r\n"));
                    OUTREG32(&g_pPLL_PFD_528MHZ->CTRL_CLR, CSP_BITFVAL(PLL_PFD_CTRL_PFD0_CLKGATE, 1));
                }

                // Wait PFD STABLE
                // The Spec indicate that PFD locking is quick enough so that
                // there is no need to check the STABLE bit. It is only for debugging
                // purpose. And unlike normal PLL LOCK bit. It is inverted to show 
                // lock done

                // while (EXTREG32BF(&g_pPLL_PFD_528MHZ->CTRL, PLL_PFD_CTRL_PFD0_STABLE))
            }
            break;

        case DDK_CLOCK_SIGNAL_528M_PFD1_594M:
            // Fout = Fin * 18 / Div
            BSPClockGetFreq(DDK_CLOCK_SIGNAL_PLL2_BUS, &refFreq);
            if (refFreq == 0)
            {
                ERRORMSG(1, (L"PLEASE OPEN PLL2 FIRST\r\n"));   
                rc = FALSE;
            }
            else
            {
                div = ROUND_DIV((LONGLONG)refFreq * 18, freq);
                div = (div < 12) ? 12 : div;
                div = (div > 35) ? 35 : div;
                INSREG32BF(&g_pPLL_PFD_528MHZ->CTRL, PLL_PFD_CTRL_PFD1_FRAC, div);
                freq = ROUND_DIV((LONGLONG)refFreq * 18, div);
                RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] PFD_594 div %d, freq %d\r\n", div, freq));

                // Make sure if PFD clock is ungated
                if (EXTREG32BF(&g_pPLL_PFD_528MHZ->CTRL, PLL_PFD_CTRL_PFD1_CLKGATE))
                {
                    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] CLR PFD GATE\r\n"));
                    OUTREG32(&g_pPLL_PFD_528MHZ->CTRL_CLR, CSP_BITFVAL(PLL_PFD_CTRL_PFD1_CLKGATE, 1));
                }
            }
            break;

        case DDK_CLOCK_SIGNAL_528M_PFD2_400M:
            // Fout = Fin * 18 / Div
            BSPClockGetFreq(DDK_CLOCK_SIGNAL_PLL2_BUS, &refFreq);
            if (refFreq == 0)
            {
                ERRORMSG(1, (L"PLEASE OPEN PLL2 FIRST\r\n"));   
                rc = FALSE;
            }
            else
            {
                div = ROUND_DIV((LONGLONG)refFreq * 18, freq);
                div = (div < 12) ? 12 : div;
                div = (div > 35) ? 35 : div;
                INSREG32BF(&g_pPLL_PFD_528MHZ->CTRL, PLL_PFD_CTRL_PFD2_FRAC, div);
                freq = ROUND_DIV((LONGLONG)refFreq * 18, div);
                RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] PFD_400 div %d, freq %d\r\n", div, freq));

                // Make sure if PFD clock is ungated
                if (EXTREG32BF(&g_pPLL_PFD_528MHZ->CTRL, PLL_PFD_CTRL_PFD2_CLKGATE))
                {
                    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] CLR PFD GATE\r\n"));
                    OUTREG32(&g_pPLL_PFD_528MHZ->CTRL_CLR, CSP_BITFVAL(PLL_PFD_CTRL_PFD2_CLKGATE, 1));
                }
            }
            break;

        case DDK_CLOCK_SIGNAL_528M_PFD3_200M:
            // Fout = Fin * 18 / Div
            BSPClockGetFreq(DDK_CLOCK_SIGNAL_PLL2_BUS, &refFreq);
            if (refFreq == 0)
            {
                ERRORMSG(1, (L"PLEASE OPEN PLL2 FIRST\r\n"));   
                rc = FALSE;
            }
            else
            {
                div = ROUND_DIV((LONGLONG)refFreq * 18, freq);
                div = (div < 12) ? 12 : div;
                div = (div > 35) ? 35 : div;
                INSREG32BF(&g_pPLL_PFD_528MHZ->CTRL, PLL_PFD_CTRL_PFD3_FRAC, div);
                freq = ROUND_DIV((LONGLONG)refFreq * 18, div);
                RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] PFD_200 div %d, freq %d\r\n", div, freq));

                // Make sure if PFD clock is ungated
                if (EXTREG32BF(&g_pPLL_PFD_528MHZ->CTRL, PLL_PFD_CTRL_PFD3_CLKGATE))
                {
                    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] CLR PFD GATE\r\n"));
                    OUTREG32(&g_pPLL_PFD_528MHZ->CTRL_CLR, CSP_BITFVAL(PLL_PFD_CTRL_PFD3_CLKGATE, 1));
                }
            }
            break;

        case DDK_CLOCK_SIGNAL_480M_PFD0_720M:
            // Fout = Fin * 18 / Div
            BSPClockGetFreq(DDK_CLOCK_SIGNAL_PLL3_USBOTG, &refFreq);
            if (refFreq == 0)
            {
                ERRORMSG(1, (L"PLEASE OPEN PLL3 FIRST\r\n"));   
                rc = FALSE;
            }
            else
            {
                div = ROUND_DIV((LONGLONG)refFreq * 18, freq);
                div = (div < 12) ? 12 : div;
                div = (div > 35) ? 35 : div;
                INSREG32BF(&g_pPLL_PFD_480MHZ->CTRL, PLL_PFD_CTRL_PFD0_FRAC, div);
                freq = ROUND_DIV((LONGLONG)refFreq * 18, div);
                RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] PFD_720 div %d, freq %d\r\n", div, freq));

                // Make sure if PFD clock is ungated
                if (EXTREG32BF(&g_pPLL_PFD_480MHZ->CTRL, PLL_PFD_CTRL_PFD0_CLKGATE))
                {
                    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] CLR PFD GATE\r\n"));
                    OUTREG32(&g_pPLL_PFD_480MHZ->CTRL_CLR, CSP_BITFVAL(PLL_PFD_CTRL_PFD0_CLKGATE, 1));
                }
            }
            break;

        case DDK_CLOCK_SIGNAL_480M_PFD1_540M:
            // Fout = Fin * 18 / Div
            BSPClockGetFreq(DDK_CLOCK_SIGNAL_PLL3_USBOTG, &refFreq);
            if (refFreq == 0)
            {
                ERRORMSG(1, (L"PLEASE OPEN PLL3 FIRST\r\n"));   
                rc = FALSE;
            }
            else
            {
                div = ROUND_DIV((LONGLONG)refFreq * 18, freq);
                div = (div < 12) ? 12 : div;
                div = (div > 35) ? 35 : div;
                INSREG32BF(&g_pPLL_PFD_480MHZ->CTRL, PLL_PFD_CTRL_PFD1_FRAC, div);
                freq = ROUND_DIV((LONGLONG)refFreq * 18, div);
                RETAILMSG(1, (L"\t[DDKCLK] PFD_540 div %d, freq %d\r\n", div, freq));

                // Make sure if PFD clock is ungated
                if (EXTREG32BF(&g_pPLL_PFD_480MHZ->CTRL, PLL_PFD_CTRL_PFD1_CLKGATE))
                {
                    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] CLR PFD GATE\r\n"));
                    OUTREG32(&g_pPLL_PFD_480MHZ->CTRL_CLR, CSP_BITFVAL(PLL_PFD_CTRL_PFD1_CLKGATE, 1));
                }
            }
            break;

        case DDK_CLOCK_SIGNAL_480M_PFD2_508M:
            // Fout = Fin * 18 / Div
            BSPClockGetFreq(DDK_CLOCK_SIGNAL_PLL3_USBOTG, &refFreq);
            if (refFreq == 0)
            {
                ERRORMSG(1, (L"PLEASE OPEN PLL3 FIRST\r\n"));   
                rc = FALSE;
            }
            else
            {
                div = ROUND_DIV((LONGLONG)refFreq * 18, freq);
                div = (div < 12) ? 12 : div;
                div = (div > 35) ? 35 : div;
                INSREG32BF(&g_pPLL_PFD_480MHZ->CTRL, PLL_PFD_CTRL_PFD2_FRAC, div);
                freq = ROUND_DIV((LONGLONG)refFreq * 18, div);
                RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] PFD_508 div %d, freq %d\r\n", div, freq));

                // Make sure if PFD clock is ungated
                if (EXTREG32BF(&g_pPLL_PFD_480MHZ->CTRL, PLL_PFD_CTRL_PFD2_CLKGATE))
                {
                    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] CLR PFD GATE\r\n"));
                    OUTREG32(&g_pPLL_PFD_480MHZ->CTRL_CLR, CSP_BITFVAL(PLL_PFD_CTRL_PFD2_CLKGATE, 1));
                }
            }
            break;

        case DDK_CLOCK_SIGNAL_480M_PFD3_454M:
            // Fout = Fin * 18 / Div
            BSPClockGetFreq(DDK_CLOCK_SIGNAL_PLL3_USBOTG, &refFreq);
            if (refFreq == 0)
            {
                ERRORMSG(1, (L"PLEASE OPEN PLL3 FIRST\r\n"));   
                rc = FALSE;
            }
            else
            {
                div = ROUND_DIV((LONGLONG)refFreq * 18, freq);
                div = (div < 12) ? 12 : div;
                div = (div > 35) ? 35 : div;
                INSREG32BF(&g_pPLL_PFD_480MHZ->CTRL, PLL_PFD_CTRL_PFD3_FRAC, div);
                freq = ROUND_DIV((LONGLONG)refFreq * 18, div);
                RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] PFD_454 div %d, freq %d\r\n", div, freq));

                // Make sure if PFD clock is ungated
                if (EXTREG32BF(&g_pPLL_PFD_480MHZ->CTRL, PLL_PFD_CTRL_PFD3_CLKGATE))
                {
                    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] CLR PFD GATE\r\n"));
                    OUTREG32(&g_pPLL_PFD_480MHZ->CTRL_CLR, CSP_BITFVAL(PLL_PFD_CTRL_PFD3_CLKGATE, 1));
                }
            }
            break;

        default:
            break;
    }

    if (rc) g_pBspArgs->clockFreq[sig] = freq;

    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] BSPClockSetFreq ---\r\n"));
    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function: BSPClockDumpFreq
//
// Dump the clock frequency in Hz for all clock signals
//
// Parameters:
//      NA
//
// Returns:
//      NA
//
//-----------------------------------------------------------------------------
VOID BSPClockDumpFreq()
{
    RETAILMSG(TRUE, (TEXT("BSP Clock Configuration:\r\n")));
    RETAILMSG(TRUE, (L"    OSC             = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_OSC]));
    RETAILMSG(TRUE, (L"    PLL1 (CPU)      = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL1_CPU]));
    RETAILMSG(TRUE, (L"    PLL2 (BUS)      = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL2_BUS]));
    RETAILMSG(TRUE, (L"    PLL3 (USBOTG)   = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG]));
    RETAILMSG(TRUE, (L"    PLL4 (AUDIO)    = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL4_AUDIO]));
    RETAILMSG(TRUE, (L"    PLL5 (VIDEO)    = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL5_VIDEO]));
    RETAILMSG(TRUE, (L"    PLL6 (MLB)      = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL6_MLB]));
    RETAILMSG(TRUE, (L"    PLL7 (USBHOST)  = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL7_USBHOST]));
    RETAILMSG(TRUE, (L"    PLL8 (ENET)     = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL8_ENET]));
    RETAILMSG(TRUE, (L"    PLL1_REF        = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL1_REF]));
    RETAILMSG(TRUE, (L"    PLL2_BURNIN     = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL2_BURNIN]));
    RETAILMSG(TRUE, (L"    PLL3_60M        = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_60M]));
    RETAILMSG(TRUE, (L"    PLL3_80M        = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_80M]));
    RETAILMSG(TRUE, (L"    PLL3_120M       = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_120M]));
    RETAILMSG(TRUE, (L"    480M_PFD0_720M  = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD0_720M]));
    RETAILMSG(TRUE, (L"    480M_PFD1_540M  = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD1_540M]));
    RETAILMSG(TRUE, (L"    480M_PFD2_508M  = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD2_508M]));
    RETAILMSG(TRUE, (L"    480M_PFD3_454M  = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD3_454M]));
    RETAILMSG(TRUE, (L"    528M_PFD0_352M  = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD0_352M]));
    RETAILMSG(TRUE, (L"    528M_PFD1_594M  = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD1_594M]));
    RETAILMSG(TRUE, (L"    528M_PFD2_400M  = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD2_400M]));
    RETAILMSG(TRUE, (L"    528M_PFD3_200M  = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD3_200M]));
    RETAILMSG(TRUE, (L"    ARM             = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_ARM]));
    RETAILMSG(TRUE, (L"    AXI             = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_AXI]));
    RETAILMSG(TRUE, (L"    PERIPH          = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_PERIPH]));
    RETAILMSG(TRUE, (L"    MMDC_CH0_AXI    = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_MMDC_CH0_AXI]));
    RETAILMSG(TRUE, (L"    MMDC_CH1_AXI    = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_MMDC_CH1_AXI]));
    RETAILMSG(TRUE, (L"    AHB_132M        = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_AHB_132M]));
    RETAILMSG(TRUE, (L"    IPG             = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_IPG]));
    RETAILMSG(TRUE, (L"    PERCLK          = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_PERCLK]));
    RETAILMSG(TRUE, (L"    CKIL_SYNC       = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_CKIL_SYNC]));
    RETAILMSG(TRUE, (L"    CKIH            = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_CKIH]));
    RETAILMSG(TRUE, (L"    IPU2_HSP        = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_IPU2_HSP]));
    RETAILMSG(TRUE, (L"    IPU1_HSP        = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_IPU1_HSP]));
    RETAILMSG(TRUE, (L"    PCIE_AXI        = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_PCIE_AXI]));
    RETAILMSG(TRUE, (L"    VDO_AXI         = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_VDO_AXI]));
    RETAILMSG(TRUE, (L"    VPU_AXI         = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_VPU_AXI]));
    RETAILMSG(TRUE, (L"    USDHC1          = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_USDHC1]));
    RETAILMSG(TRUE, (L"    USDHC2          = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_USDHC2]));
    RETAILMSG(TRUE, (L"    USDHC3          = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_USDHC3]));
    RETAILMSG(TRUE, (L"    USDHC4          = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_USDHC4]));
    RETAILMSG(TRUE, (L"    SSI1            = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_SSI1]));
    RETAILMSG(TRUE, (L"    SSI2            = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_SSI2]));
    RETAILMSG(TRUE, (L"    SSI3            = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_SSI3]));
    RETAILMSG(TRUE, (L"    CAN             = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_CAN]));
    RETAILMSG(TRUE, (L"    UART            = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_UART]));
    RETAILMSG(TRUE, (L"    SPDIF0          = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_SPDIF0]));
    RETAILMSG(TRUE, (L"    SPDIF1          = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_SPDIF1]));
    RETAILMSG(TRUE, (L"    ESAI            = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_ESAI]));
    RETAILMSG(TRUE, (L"    ECSPI           = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_ECSPI]));
    RETAILMSG(TRUE, (L"    ACLK_EMI_SLOW   = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_ACLK_EMI_SLOW]));
    RETAILMSG(TRUE, (L"    ACLK_EMI        = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_ACLK_EMI]));
    RETAILMSG(TRUE, (L"    ENFC            = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_ENFC]));
    RETAILMSG(TRUE, (L"    HSI_TX          = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_HSI_TX]));
    RETAILMSG(TRUE, (L"    VIDEO_27M       = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_VIDEO_27M]));
    RETAILMSG(TRUE, (L"    LDB_DI0_SERIAL  = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_LDB_DI0_SERIAL]));
    RETAILMSG(TRUE, (L"    LDB_DI1_SERIAL  = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_LDB_DI1_SERIAL]));
    RETAILMSG(TRUE, (L"    LDB_DI0_IPU     = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_LDB_DI0_IPU]));
    RETAILMSG(TRUE, (L"    LDB_DI1_IPU     = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_LDB_DI1_IPU]));
    RETAILMSG(TRUE, (L"    IPU1_DI0        = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_IPU1_DI0]));
    RETAILMSG(TRUE, (L"    IPU1_DI1        = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_IPU1_DI1]));
    RETAILMSG(TRUE, (L"    IPU2_DI0        = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_IPU2_DI0]));
    RETAILMSG(TRUE, (L"    IPU2_DI1        = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_IPU2_DI1]));
    RETAILMSG(TRUE, (L"    ASRC            = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_ASRC]));
    RETAILMSG(TRUE, (L"    WRCK            = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_WRCK]));
    RETAILMSG(TRUE, (L"    GPU2D_CORE      = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_GPU2D_CORE]));
    RETAILMSG(TRUE, (L"    GPU2D_AXI       = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_GPU2D_AXI]));
    RETAILMSG(TRUE, (L"    GPU3D_CORE      = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_GPU3D_CORE]));
    RETAILMSG(TRUE, (L"    GPU3D_AXI       = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_GPU3D_AXI]));
    RETAILMSG(TRUE, (L"    GPU3D_SHADER    = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_GPU3D_SHADER]));
    RETAILMSG(TRUE, (L"    IPP_DI0         = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_IPP_DI0]));
    RETAILMSG(TRUE, (L"    IPP_DI1         = %10d Hz\r\n", g_pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_IPP_DI1]));
}

//-----------------------------------------------------------------------------
//
// Function: BSPClockUpdateFreq
//
// Updates the clock frequency in Hz for the specified clock signal.
//
// Parameters:
//      sig
//           [in] Clock signal.
//      src
//          [in] Selects the input clock source.
//
//      preDiv
//          [in] Specifies the value programmed into the baud clock predivider.
//
//      postDiv
//          [in] Specifies the value programmed into the baud clock postdivider.
//
// Returns:
//      Returns TRUE if successful, otherwise returns FALSE.
//
//  TODO :
//      This function don't consider cases that a configured signal is root
//      to other signals. In that cases, all the leaf clocks should also be 
//      clocked. May consider to add this feature per demand
//
//-----------------------------------------------------------------------------
BOOL BSPClockUpdateFreq(DDK_CLOCK_SIGNAL sig, DDK_CLOCK_BAUD_SOURCE src,
    UINT32 preDivTier1, UINT32 postDivTier1, UINT32 divTier2)
{
    BOOL rc = TRUE;
    DDK_CLOCK_SIGNAL sigsrc;

    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] BSPClockUpdateFreq +++\r\n", sig));

    switch (src)
    {
    case DDK_CLOCK_BAUD_SOURCE_PLL1_CPU:
         sigsrc = DDK_CLOCK_SIGNAL_PLL1_CPU;
         break;

    case DDK_CLOCK_BAUD_SOURCE_PLL1_REF:
         sigsrc = DDK_CLOCK_SIGNAL_PLL1_REF;
         break;

    case DDK_CLOCK_BAUD_SOURCE_PLL2_BURNIN:
         sigsrc = DDK_CLOCK_SIGNAL_PLL2_BURNIN;
         break;

    case DDK_CLOCK_BAUD_SOURCE_PLL2_BUS:
         sigsrc = DDK_CLOCK_SIGNAL_PLL2_BUS;
         break;

    case DDK_CLOCK_BAUD_SOURCE_PLL3_USBOTG:
         sigsrc = DDK_CLOCK_SIGNAL_PLL3_USBOTG;
         break;

    case DDK_CLOCK_BAUD_SOURCE_PLL4_AUDIO:
         sigsrc = DDK_CLOCK_SIGNAL_PLL4_AUDIO;
         break;

    case DDK_CLOCK_BAUD_SOURCE_PLL5_VIDEO:
         sigsrc = DDK_CLOCK_SIGNAL_PLL5_VIDEO;
         break;

    case DDK_CLOCK_BAUD_SOURCE_120M:
         sigsrc = DDK_CLOCK_SIGNAL_PLL3_120M;
         break;

    case DDK_CLOCK_BAUD_SOURCE_528M_PFD3_200M:
         sigsrc = DDK_CLOCK_SIGNAL_528M_PFD3_200M;
         break;

    case DDK_CLOCK_BAUD_SOURCE_528M_PFD0_352M:
         sigsrc = DDK_CLOCK_SIGNAL_528M_PFD0_352M;
         break;

    case DDK_CLOCK_BAUD_SOURCE_528M_PFD2_400M:
         sigsrc = DDK_CLOCK_SIGNAL_528M_PFD2_400M;
         break;

    case DDK_CLOCK_BAUD_SOURCE_480M_PFD3_454M:
         sigsrc = DDK_CLOCK_SIGNAL_480M_PFD3_454M;
         break;

    case DDK_CLOCK_BAUD_SOURCE_480M_PFD2_508M:
         sigsrc = DDK_CLOCK_SIGNAL_480M_PFD2_508M;
         break;

    case DDK_CLOCK_BAUD_SOURCE_480M_PFD1_540M:
         sigsrc = DDK_CLOCK_SIGNAL_480M_PFD1_540M;
         break;

    case DDK_CLOCK_BAUD_SOURCE_528M_PFD1_594M:
         sigsrc = DDK_CLOCK_SIGNAL_528M_PFD1_594M;
         break;

    case DDK_CLOCK_BAUD_SOURCE_480M_PFD0_720M:
         sigsrc = DDK_CLOCK_SIGNAL_480M_PFD0_720M;
         break;

    case DDK_CLOCK_BAUD_SOURCE_PLL3_60M:
         sigsrc = DDK_CLOCK_SIGNAL_PLL3_60M;
         break;

    case DDK_CLOCK_BAUD_SOURCE_PLL3_80M:
         sigsrc = DDK_CLOCK_SIGNAL_PLL3_80M;
         break;

    case DDK_CLOCK_BAUD_SOURCE_AHB_132M:
         sigsrc = DDK_CLOCK_SIGNAL_AHB_132M;
         break;

    case DDK_CLOCK_BAUD_SOURCE_PERIPH:
         sigsrc = DDK_CLOCK_SIGNAL_PERIPH;
         break;

    case DDK_CLOCK_BAUD_SOURCE_AXI:
         sigsrc = DDK_CLOCK_SIGNAL_AXI;
         break;

    case DDK_CLOCK_BAUD_SOURCE_MMDC_CH0:
         sigsrc = DDK_CLOCK_SIGNAL_MMDC_CH0_AXI;
         break;

	case DDK_CLOCK_BAUD_SOURCE_MMDC_CH1:
         sigsrc = DDK_CLOCK_SIGNAL_MMDC_CH1_AXI;
         break;
 
    case DDK_CLOCK_BAUD_SOURCE_IPG:
         sigsrc = DDK_CLOCK_SIGNAL_IPG;
         break;

    case DDK_CLOCK_BAUD_SOURCE_IPP_DI0:
         sigsrc = DDK_CLOCK_SIGNAL_IPP_DI0;
         break;

    case DDK_CLOCK_BAUD_SOURCE_IPP_DI1:
         sigsrc = DDK_CLOCK_SIGNAL_IPP_DI1;
         break;

    case DDK_CLOCK_BAUD_SOURCE_LDB_DI0_SERIAL:
         sigsrc = DDK_CLOCK_SIGNAL_LDB_DI0_SERIAL;
         break;

    case DDK_CLOCK_BAUD_SOURCE_LDB_DI1_SERIAL:
         sigsrc = DDK_CLOCK_SIGNAL_LDB_DI1_SERIAL;
         break;

    case DDK_CLOCK_BAUD_SOURCE_LDB_DI0_IPU:
         sigsrc = DDK_CLOCK_SIGNAL_LDB_DI0_IPU;
         break;

    case DDK_CLOCK_BAUD_SOURCE_LDB_DI1_IPU:
         sigsrc = DDK_CLOCK_SIGNAL_LDB_DI1_IPU;
         break;

    default:
        sigsrc = 0;
        rc = FALSE;
    }
    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] Calc S[%s] from [%s]\r\n", ClkSigName[sig], ClkSigName[sigsrc]));

    if (rc)
    {
        // General
        UINT32 totalDiv = (preDivTier1+1) * (postDivTier1+1) * (divTier2 + 1);
        RETAILMSG(DDKCLK_WORK_LOG, (L"\t[CLOCKDIV] totalDiv is %d [%d:%d:%d], src Freq is %d\r\n",
                    totalDiv, preDivTier1, postDivTier1, divTier2, g_pBspArgs->clockFreq[sigsrc]));
        g_pBspArgs->clockFreq[sig] = g_pBspArgs->clockFreq[sigsrc] / totalDiv;

        // Modification for specific case
        if ((sig == DDK_CLOCK_SIGNAL_LDB_DI0_IPU) || (sig == DDK_CLOCK_SIGNAL_LDB_DI1_IPU))
        {
            g_pBspArgs->clockFreq[sig] = (g_pBspArgs->clockFreq[sigsrc] * 2) / (7 * (divTier2 + 1));
        }
    }
    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] BSPClockUpdateFreq ---\r\n"));

    return TRUE;
}


//-----------------------------------------------------------------------------
//
//  Function: UpdateSetpointRequestCount
//
//  This function increments/decrements the DVFC setpoint request count
//  based on the peripheral type.  The DVFC driver evaluates these counts
//  to determine the best setpoint for the system.
//
//  Parameters:
//      index
//          [in] Index of peripheral clock gating signal.
//
//      bAddRequest
//          [in] Boolean set to TRUE if a setpoint request should be added
//               for the specified peripheral.  Set to FALSE if a setpoint
//               request should be removed.
//
//  Returns:
//      Returns the DVFC setpoint mapping for the peripheral.
//
//-----------------------------------------------------------------------------
DDK_DVFC_SETPOINT UpdateSetpointRequestCount(DDK_CLOCK_GATE_INDEX index,
                                             BOOL bAddRequest)
{
    DDK_DVFC_SETPOINT setpointReq;
    LONG cnt;
    PDDK_DVFC_SETPOINT_INFO pSetPointInfo;

    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] UpdateSetpointRequestCount [%s] %s +++\r\n", ClkGateName[index], bAddRequest?L"ADD":L"SUB"));
    pSetPointInfo = &g_pDdkClkConfig->periphSetpointReq[index];

    if ((pSetPointInfo->mV > BSP_DVFS_PER_HIGH_mV) ||
        (pSetPointInfo->freq[DDK_DVFC_FREQ_AHB] > BSP_DVFS_PER_HIGH_PERBUS_FREQ) ||
        (pSetPointInfo->freq[DDK_DVFC_FREQ_AXI] > BSP_DVFS_PER_HIGH_AXI_B_FREQ))
    {
        setpointReq = DDK_DVFC_SETPOINT_HIGH2;
    }
    else if ((pSetPointInfo->mV > BSP_DVFS_PER_MED_mV) ||
        (pSetPointInfo->freq[DDK_DVFC_FREQ_AHB] > BSP_DVFS_PER_MED_PERBUS_FREQ) ||
        (pSetPointInfo->freq[DDK_DVFC_FREQ_AXI] > BSP_DVFS_PER_MED_AXI_B_FREQ))
    {
        setpointReq = DDK_DVFC_SETPOINT_HIGH;
    }
    else if ((pSetPointInfo->mV > BSP_DVFS_PER_LOW_mV) ||
        (pSetPointInfo->freq[DDK_DVFC_FREQ_AHB] > BSP_DVFS_PER_LOW_PERBUS_FREQ) ||
        (pSetPointInfo->freq[DDK_DVFC_FREQ_AXI] > BSP_DVFS_PER_LOW_AXI_B_FREQ))
    {
        setpointReq = DDK_DVFC_SETPOINT_MEDIUM;
    }
    else
    {
        setpointReq = DDK_DVFC_SETPOINT_LOW;
    }
    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] Setpoint is %d\r\n", setpointReq));

    // No need to keep track of requests for lowest setpoint
    if (setpointReq != DDK_DVFC_SETPOINT_LOW)
    {
        if (bAddRequest)
        {
            RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] Peiph [%d] Req ++\r\n", setpointReq));
            InterlockedIncrement((LPLONG) &g_pDdkClkConfig->setpointReqCount[DDK_DVFC_DOMAIN_PERIPH][setpointReq]);
        }
        else
        {
            RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] Peiph [%d] Req --\r\n", setpointReq));
            cnt = InterlockedDecrement((LPLONG) &g_pDdkClkConfig->setpointReqCount[DDK_DVFC_DOMAIN_PERIPH][setpointReq]);
            if (cnt < 0)
            {
                DBGCHK((_T("CSPDDK")),  FALSE);
                ERRORMSG(TRUE, (_T("UpdateSetpointRequestCount:  setpointReqCount[%d] went below zero\r\n"), setpointReq));
            }
        }
    }
    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] UpdateSetpointRequestCount ---\r\n"));

    return setpointReq;
}


//-----------------------------------------------------------------------------
//
//  Function: BSPClockRootEnable
//
//  This function enables the specified root clock.
//
//  Parameters:
//      root
//           [in] Index for referencing the root clock
//
//  Returns:
//      None.
//
//-----------------------------------------------------------------------------
// MX6_BRING_UP
VOID BSPClockRootEnable(DDK_CLOCK_BAUD_SOURCE root)
{
    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] BSPClockRootEnable [%s] +++\r\n", BaudSrcName[root]));

    switch(root)
    {
    case DDK_CLOCK_BAUD_SOURCE_PLL1_CPU:
        break;
    case DDK_CLOCK_BAUD_SOURCE_PLL2_BUS:
        break;
    case DDK_CLOCK_BAUD_SOURCE_PLL3_USBOTG:     // default open
        break;
    case DDK_CLOCK_BAUD_SOURCE_PLL4_AUDIO:
        break;
    case DDK_CLOCK_BAUD_SOURCE_PLL5_VIDEO:
        break;

    // The following PLL6~PLL8 are not explicitly in clock tree, we need 
    // to consider expose a interface to open them
    case DDK_CLOCK_BAUD_SOURCE_PLL6_MLB:
        break;
    case DDK_CLOCK_BAUD_SOURCE_PLL7_USBHOST:    // default open
        break;
    case DDK_CLOCK_BAUD_SOURCE_PLL8_ENET:
        break;

    case DDK_CLOCK_BAUD_SOURCE_528M_PFD0_352M:
        OUTREG32(&g_pPLL_PFD_528MHZ->CTRL_CLR, CSP_BITFVAL(PLL_PFD_CTRL_PFD0_CLKGATE, 1));
        break;
    case DDK_CLOCK_BAUD_SOURCE_528M_PFD1_594M:
        OUTREG32(&g_pPLL_PFD_528MHZ->CTRL_CLR, CSP_BITFVAL(PLL_PFD_CTRL_PFD1_CLKGATE, 1));
        break;
    case DDK_CLOCK_BAUD_SOURCE_528M_PFD2_400M:
        OUTREG32(&g_pPLL_PFD_528MHZ->CTRL_CLR, CSP_BITFVAL(PLL_PFD_CTRL_PFD2_CLKGATE, 1));
        break;
    case DDK_CLOCK_BAUD_SOURCE_528M_PFD3_200M:
        OUTREG32(&g_pPLL_PFD_528MHZ->CTRL_CLR, CSP_BITFVAL(PLL_PFD_CTRL_PFD3_CLKGATE, 1));
        break;
    case DDK_CLOCK_BAUD_SOURCE_480M_PFD0_720M:
        OUTREG32(&g_pPLL_PFD_480MHZ->CTRL_CLR, CSP_BITFVAL(PLL_PFD_CTRL_PFD0_CLKGATE, 1));
        break;
    case DDK_CLOCK_BAUD_SOURCE_480M_PFD1_540M:
        OUTREG32(&g_pPLL_PFD_480MHZ->CTRL_CLR, CSP_BITFVAL(PLL_PFD_CTRL_PFD1_CLKGATE, 1));
        break;
    case DDK_CLOCK_BAUD_SOURCE_480M_PFD2_508M:
        OUTREG32(&g_pPLL_PFD_480MHZ->CTRL_CLR, CSP_BITFVAL(PLL_PFD_CTRL_PFD2_CLKGATE, 1));
        break;
    case DDK_CLOCK_BAUD_SOURCE_480M_PFD3_454M:
        OUTREG32(&g_pPLL_PFD_480MHZ->CTRL_CLR, CSP_BITFVAL(PLL_PFD_CTRL_PFD3_CLKGATE, 1));
        break;
    default:
        break;
    }
    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] BSPClockRootEnable ---\r\n"));
}


//-----------------------------------------------------------------------------
//
//  Function: BSPClockRootDisable
//
//  This function disables the specified root clock.
//
//  Parameters:
//      root
//           [in] Index for referencing the root clock
//
//  Returns:
//      None.
//
//-----------------------------------------------------------------------------
VOID BSPClockRootDisable(DDK_CLOCK_BAUD_SOURCE root)
{
    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] BSPClockRootDisable [%s] +++\r\n", BaudSrcName[root]));

    switch(root)
    {
    case DDK_CLOCK_BAUD_SOURCE_PLL1_CPU:
        break;
    case DDK_CLOCK_BAUD_SOURCE_PLL2_BUS:
        break;
    case DDK_CLOCK_BAUD_SOURCE_PLL3_USBOTG:
        break;
    case DDK_CLOCK_BAUD_SOURCE_PLL4_AUDIO:
        break;
    case DDK_CLOCK_BAUD_SOURCE_PLL5_VIDEO:
        break;
    case DDK_CLOCK_BAUD_SOURCE_528M_PFD0_352M:
        OUTREG32(&g_pPLL_PFD_528MHZ->CTRL_SET, CSP_BITFVAL(PLL_PFD_CTRL_PFD0_CLKGATE, 1));
        break;
    case DDK_CLOCK_BAUD_SOURCE_528M_PFD1_594M:
        OUTREG32(&g_pPLL_PFD_528MHZ->CTRL_SET, CSP_BITFVAL(PLL_PFD_CTRL_PFD1_CLKGATE, 1));
        break;
    case DDK_CLOCK_BAUD_SOURCE_528M_PFD2_400M:
        OUTREG32(&g_pPLL_PFD_528MHZ->CTRL_SET, CSP_BITFVAL(PLL_PFD_CTRL_PFD2_CLKGATE, 1));
        break;
    case DDK_CLOCK_BAUD_SOURCE_528M_PFD3_200M:
        OUTREG32(&g_pPLL_PFD_528MHZ->CTRL_SET, CSP_BITFVAL(PLL_PFD_CTRL_PFD3_CLKGATE, 1));
        break;
    case DDK_CLOCK_BAUD_SOURCE_480M_PFD0_720M:
        OUTREG32(&g_pPLL_PFD_480MHZ->CTRL_SET, CSP_BITFVAL(PLL_PFD_CTRL_PFD0_CLKGATE, 1));
        break;
    case DDK_CLOCK_BAUD_SOURCE_480M_PFD1_540M:
        OUTREG32(&g_pPLL_PFD_480MHZ->CTRL_SET, CSP_BITFVAL(PLL_PFD_CTRL_PFD1_CLKGATE, 1));
        break;
    case DDK_CLOCK_BAUD_SOURCE_480M_PFD2_508M:
        OUTREG32(&g_pPLL_PFD_480MHZ->CTRL_SET, CSP_BITFVAL(PLL_PFD_CTRL_PFD2_CLKGATE, 1));
        break;
    case DDK_CLOCK_BAUD_SOURCE_480M_PFD3_454M:
        OUTREG32(&g_pPLL_PFD_480MHZ->CTRL_SET, CSP_BITFVAL(PLL_PFD_CTRL_PFD3_CLKGATE, 1));
        break;
    default:
        break;
    }
    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] BSPClockRootDisable ---\r\n"));
}


//-----------------------------------------------------------------------------
//
//  Function: BSPClockPowerUp
//
//  This function powers up the module for the specified clock node.
//
//  Parameters:
//      index
//           [in] Index for referencing the clock node.
//
//  Returns:
//      None.
//
//-----------------------------------------------------------------------------
VOID BSPClockPowerUp(DDK_CLOCK_GATE_INDEX index)
{
    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] BSPClockPowerUp +++\r\n"));
    switch(index)
    {
    }
    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] BSPClockPowerUp ---\r\n"));
}


//-----------------------------------------------------------------------------
//
//  Function: BSPClockPowerDown
//
//  This function powers down the module for the specified clock node.
//
//  Parameters:
//      index
//           [in] Index for referencing the clock node.
//
//  Returns:
//      None.
//
//-----------------------------------------------------------------------------
// MX6_BRING_UP
VOID BSPClockPowerDown(DDK_CLOCK_GATE_INDEX index)
{
    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] BSPClockPowerDown [%d] +++\r\n", index));
    switch(index)
    {
    }
    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] BSPClockPowerDown ---\r\n"));
}


//-----------------------------------------------------------------------------
//
//  Function: BSPClockSetGatingModePrepare
//
//  This function is called prior to updating the clock gating mode of
//  peripherals to determine if the system is currently at a voltage/frequency
//  setpoint that is sufficient to support the peripheral.  If it is
//  determined that the current setpoint is not sufficient, then a request
//  to raise the setpoint is submitted to the DVFC driver.
//
//  Parameters:
//      index
//           [in] Index for referencing the peripheral clock gating control
//           bits.
//
//  Returns:
//      Returns TRUE if successful, otherwise returns FALSE.
//
//-----------------------------------------------------------------------------
BOOL BSPClockSetGatingModePrepare(DDK_CLOCK_GATE_INDEX index)
{
    DDK_DVFC_SETPOINT setpointReq;

    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] BSPClockSetGatingModePrepare +++\r\n"));
    setpointReq = UpdateSetpointRequestCount(index, TRUE);

    // Only worry about updating the voltage/frequency setpoint if the DVFC
    // driver is active and the required setpoint is not LOW
    if ((g_pDdkClkConfig->bDvfcActive) &&
        (setpointReq != g_pDdkClkConfig->setpointMin[DDK_DVFC_DOMAIN_PERIPH]))
    {
        DDKClockUnlock();

        // Until the current setpoint is sufficient
        while ((g_pDdkClkConfig->setpointCur[DDK_DVFC_DOMAIN_PERIPH] > setpointReq) ||
               g_pDdkClkConfig->bSetpointPending)
        {
            RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] Triggle DVFC\r\n"));
            ResetEvent(g_hSetPointEvent);

            // Signal the DVFC driver about our request
            SetEvent(g_hDvfcWorkerEvent);

            // Wait for setpoint to be granted by the DVFC driver
            WaitForSingleObject(g_hSetPointEvent, INFINITE);
        }

        DDKClockLock();
    }
    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] BSPClockSetGatingModePrepare ---\r\n"));
    return TRUE;
}


//-----------------------------------------------------------------------------
//
//  Function: BSPClockSetGatingModeComplete
//
//  Scans the clock tree to determine the usage of EMI clock and PERCLK domains.
//  Configures the system for EMI clock gating when possible.  Updates global
//  data structure shared with DVFC driver to determine when power state
//  transistions that effect system clocking are possible.
//
//  Parameters:
//      index
//           [in] Index for referencing the peripheral clock gating control
//           bits.
//
//      mode
//           [in] Requested clock gating mode for the peripheral.
//
//  Returns:
//      Returns TRUE if successful, otherwise returns FALSE.
//
//-----------------------------------------------------------------------------
BOOL BSPClockSetGatingModeComplete(DDK_CLOCK_GATE_INDEX index, DDK_CLOCK_GATE_MODE mode)
{
    DDK_DVFC_SETPOINT setpointReq;

    // If a clock is being disabled, determine if setpoint can be adjusted
    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] BSPClockSetGatingModeComplete CG [%s] : mode %x +++\r\n", ClkGateName[index], mode));

    if (mode == DDK_CLOCK_GATE_MODE_DISABLED)
    {
        setpointReq = UpdateSetpointRequestCount(index, FALSE);

        // If the DVFC driver is active determine if the
        // setpoint can be lowered
        if (g_pDdkClkConfig->bDvfcActive)
        {
            // If the peripheral clock being disabled was the last one
            // that required the current setpoint, and the current setpoint
            // is not already the lowest
            if ((g_pDdkClkConfig->setpointReqCount[DDK_DVFC_DOMAIN_PERIPH][setpointReq] == 0) &&
                (g_pDdkClkConfig->setpointCur[DDK_DVFC_DOMAIN_PERIPH] != g_pDdkClkConfig->setpointMin[DDK_DVFC_DOMAIN_PERIPH]) &&
                (g_pDdkClkConfig->setpointCur[DDK_DVFC_DOMAIN_PERIPH] == setpointReq))
            {
                // Request DVFC to reevaluate the current setpoint
                RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] Triggle DVFC\r\n"));
                SetEvent(g_hDvfcWorkerEvent);
            }
        }
    }

    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] BSPClockSetGatingModeComplete ---\r\n"));

    return TRUE;
}


//-----------------------------------------------------------------------------
//
//  Function: BSPClockSetpointRequest
//
//  Requests the specified setpoint optionally blocks until the setpoint
//  is granted.
//
//  Parameters:
//      setpoint
//          [in] - Specifies the setpoint to be requested.
//
//      bBlock
//          [in] - Set TRUE to block until the setpoint has been granted.
//                 Set FALSE to return immediately after request has
//                 submitted.
//
//  Returns:
//      Returns TRUE if successful, otherwise returns FALSE.
//
//-----------------------------------------------------------------------------
BOOL BSPClockSetpointRequest(DDK_DVFC_SETPOINT setpoint,
                             DDK_DVFC_DOMAIN domain, BOOL bBlock)
{
    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] BSPClockSetpointRequest domain[%d], setpoint[%d], %s+++\r\n", domain, setpoint, bBlock?L"BLOCK":L"NONBLOCK"));

    if (setpoint > g_pDdkClkConfig->setpointMin[domain])
        return FALSE;

    if (setpoint != g_pDdkClkConfig->setpointMin[domain])
    {
        InterlockedIncrement((LPLONG) &g_pDdkClkConfig->setpointReqCount[domain][setpoint]);

        // If caller wants to block until setpoint is granted
        if (bBlock && g_pDdkClkConfig->bDvfcActive)
        {
            // Until the current setpoint is sufficient
            while ((g_pDdkClkConfig->setpointCur[domain] > setpoint) ||
                   g_pDdkClkConfig->bSetpointPending)
            {
                ResetEvent(g_hSetPointEvent);

                // Signal the DVFC driver about our request
                SetEvent(g_hDvfcWorkerEvent);

                // Wait for setpoint to be granted by the DVFC driver
                WaitForSingleObject(g_hSetPointEvent, INFINITE);
            }
        }
        else
        {
            // Signal the DVFC driver about our request
            SetEvent(g_hDvfcWorkerEvent);
        }

    }
    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] BSPClockSetpointRequest\r\n"));
    return TRUE;
}


//-----------------------------------------------------------------------------
//
//  Function: BSPClockSetpointRelease
//
//  Releases a setpoint previously requested using DDKClockSetpointRequest.
//
//  Parameters:
//      setpoint
//          [in] - Specifies the setpoint to be released.
//
//  Returns:
//      Returns TRUE if successful, otherwise returns FALSE.
//
//-----------------------------------------------------------------------------
BOOL BSPClockSetpointRelease(DDK_DVFC_SETPOINT setpoint,
                             DDK_DVFC_DOMAIN domain)

{
    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] BSPClockSetpointRelease domain [%d], setpoint [%d]+++\r\n", domain, setpoint));

    if (setpoint > g_pDdkClkConfig->setpointMin[DDK_DVFC_DOMAIN_PERIPH])
        return FALSE;

    if (setpoint != g_pDdkClkConfig->setpointMin[DDK_DVFC_DOMAIN_PERIPH])
    {
        InterlockedDecrement((LPLONG) &g_pDdkClkConfig->setpointReqCount[domain][setpoint]);
        // Signal the DVFC driver about our request
        SetEvent(g_hDvfcWorkerEvent);
    }
    RETAILMSG(DDKCLK_WORK_LOG, (L"\t[DDKCLK] BSPClockSetpointRelease\r\n"));
    return TRUE;
}

//-----------------------------------------------------------------------------
//
//  Function: DumpCCMRegister
//
//  Dump all CCM registers
//
//  Parameters:
//
//  Returns:
//      NA
//
//-----------------------------------------------------------------------------
VOID DumpCCMRegs()
{
    DWORD offset;
    DWORD rangeS = 0x0, rangeE = 0x1f;
    PUCHAR startAddr;
    PHYSICAL_ADDRESS phyAddr;
    phyAddr.QuadPart = CSP_BASE_REG_PA_CCM;

    startAddr = (PUCHAR)MmMapIoSpace(phyAddr, 0x1000, FALSE); // uncached
    RETAILMSG(1, (L"start addr of CCM is %x [%x]\r\n", startAddr, g_pCCM));
    
    RETAILMSG(1, (L"------------------------ Dump CCM Registers ------------------------\r\n"));
    RETAILMSG(1, (L"%4x ~ %4x\t", rangeS, rangeE));
    for (offset = 0; offset < sizeof(CSP_CCM_REGS); offset += 4)
    {
        RETAILMSG(1, (L"%12x", INREG32((PUCHAR)(startAddr) + offset)));
        if ((offset + 4) % 32 == 0) 
        {
            RETAILMSG(1, (L"\r\n"));
            rangeS += 32;
            rangeE += 32;
            RETAILMSG(1, (L"%4x ~ %4x\t", rangeS, rangeE));
        }
    }
    RETAILMSG(1, (L"\r\n"));
    RETAILMSG(1, (L"--------------------------------------------------------------------\r\n"));
    MmUnmapIoSpace((LPVOID)startAddr, 0x1000);
}

//-----------------------------------------------------------------------------
//
//  Function: DumpAnatopRegister
//
//  Dump all Anatop registers
//
//  Parameters:
//
//  Returns:
//      NA
//
//-----------------------------------------------------------------------------
VOID DumpAnatopRegs()
{
    DWORD offset;
    DWORD rangeS = 0x0, rangeE = 0x1f;
    PUCHAR startAddr;
    PHYSICAL_ADDRESS phyAddr;
    phyAddr.QuadPart = CSP_BASE_REG_PA_ANATOP;

    startAddr = (PUCHAR)MmMapIoSpace(phyAddr, 0x1000, FALSE); // uncached
    RETAILMSG(1, (L"start addr of Anatop is %x [%x][%x]\r\n", startAddr, g_pPLL_CPU, g_pPLL_USBOTG));
    
    RETAILMSG(1, (L"------------------------ Dump Anatop Registers ------------------------\r\n"));
    RETAILMSG(1, (L"%4x ~ %4x\t", rangeS, rangeE));
    for (offset = 0; offset < 0x300; offset += 4)
    {
        RETAILMSG(1, (L"%12x", INREG32((PUCHAR)(startAddr) + offset)));
        if ((offset + 4) % 32 == 0) 
        {
            RETAILMSG(1, (L"\r\n"));
            rangeS += 32;
            rangeE += 32;
            RETAILMSG(1, (L"%4x ~ %4x\t", rangeS, rangeE));
        }
    }
    RETAILMSG(1, (L"\r\n"));
    RETAILMSG(1, (L"-----------------------------------------------------------------------\r\n"));
    MmUnmapIoSpace((LPVOID)startAddr, 0x1000);
}


//-----------------------------------------------------------------------------
//
//  Function: BSPClockGetOscFreq
//
//  Retrieve the board oscillator frequency
//
//  Parameters:
//
//  Returns:
//      Oscillator Frequency
//
//-----------------------------------------------------------------------------
UINT32  BSPClockGetOscFreq()
{
    return BSP_CLK_OSC_FREQ;
}