//------------------------------------------------------------------------------
//  Copyright (C) 2012, Adeneo Embedded All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
// Copyright (C) 2007-2010, Freescale Semiconductor, Inc. All Rights Reserved.
// THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
// AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//------------------------------------------------------------------------------
//
//  File:  bspenet_ndis6.c
//
//  Implementation of ENET Driver
//
//  This file implements the bsp level code for ENET driver.
//
//-----------------------------------------------------------------------------
#include "bsp.h"

//-----------------------------------------------------------------------------
// External Functions
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// External Variables
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Defines
//-----------------------------------------------------------------------------
#ifdef ZONE_FUNCTION
#undef ZONE_FUNCTION
#endif

#define ZONE_FUNCTION 0

//-----------------------------------------------------------------------------
// Types
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Global Variables
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Local Variables
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Local Functions
//-----------------------------------------------------------------------------

#define RGMII_nRST_GPIO_PORT	DDK_GPIO_PORT1
#define RGMII_nRST_GPIO_PIN		25
#define RGMII_nRST_PIN			DDK_IOMUX_PIN_ENET_CRS_DV
#define RGMII_nRST_PIN_MODE		DDK_IOMUX_PIN_MUXMODE_ALT5
#define RGMII_nRST_PAD			DDK_IOMUX_PAD_ENET_CRS_DV
#define RGMII_RXC_GPIO_PORT		DDK_GPIO_PORT6
#define RGMII_RXC_GPIO_PIN		30
#define RGMII_RD0_GPIO_PORT		DDK_GPIO_PORT6
#define RGMII_RD0_GPIO_PIN		25
#define RGMII_RD1_GPIO_PORT		DDK_GPIO_PORT6
#define RGMII_RD1_GPIO_PIN		27
#define RGMII_RD2_GPIO_PORT		DDK_GPIO_PORT6
#define RGMII_RD2_GPIO_PIN		28
#define RGMII_RD3_GPIO_PORT		DDK_GPIO_PORT6
#define RGMII_RD3_GPIO_PIN		29
#define RGMII_RX_CTL_GPIO_PORT	DDK_GPIO_PORT6
#define RGMII_RX_CTL_GPIO_PIN	24
#define RGMII_ENET_REF_CLK_GPIO_PORT	DDK_GPIO_PORT1
#define RGMII_ENET_REF_CLK_GPIO_PIN		23
// GPIO defines for ethernet workaround
#define RGMII_nRST_GPIO_PIN_MASK		GPIO_PIN_MASK(25)
#define RGMII_RXC_GPIO_PIN_MASK			GPIO_PIN_MASK(30)
#define RGMII_RD0_GPIO_PIN_MASK			GPIO_PIN_MASK(25)
#define RGMII_RD1_GPIO_PIN_MASK			GPIO_PIN_MASK(27)
#define RGMII_RD2_GPIO_PIN_MASK			GPIO_PIN_MASK(28)
#define RGMII_RD3_GPIO_PIN_MASK			GPIO_PIN_MASK(29)
#define RGMII_RX_CTL_GPIO_PIN_MASK		GPIO_PIN_MASK(24)
#define RGMII_ENET_REF_CLK_GPIO_PIN_MASK	GPIO_PIN_MASK(23)

//------------------------------------------------------------------------------
//
// Function: BSPGetENETMACAddressFromNAND
//
// This fuction will retrieve the MAC address stored in hardware and read by
// the loader.
//
// Parameters:
//        FecMacArray
//            [out] Caller supplied array to return the MAC address from EEPROM
//
//        length
//            [in]  length in word of the supplied array
//
// Return Value:
//        None.
//
//------------------------------------------------------------------------------
void BSPGetENETMACAddressFromNAND(__out_ecount(length)UINT16 * FecMacArray, UINT length)
{
    if( length == 3 )
    {
	    if (!KernelIoControl(IOCTL_HAL_GET_MAC_ADDRESS, NULL, 0,
             (void*)FecMacArray, 6, NULL))
        {
            memset(FecMacArray, 0, 6);
        }
    }
}


//------------------------------------------------------------------------------
//
// Function: BSPENETIomuxConfig
//
// This fuction will enable/disable the ENET GPIO according to the parameter
// "Enable".
//
// Parameters:
//        index
//          [in]    Index specifying the ENET module.
//
//        Enable
//            [in] TRUE for enabling the ENET GPIO, FALSE for disabling the
//                 ENET GPIO.
//
// Return Value:
//        TRUE for success, FALSE if failure.
//
//------------------------------------------------------------------------------

BOOL BSPENETIomuxConfig(BOOL bEnable )
{
	BOOL ret = TRUE;
    if(bEnable)
    {
    // Config PinMux for ENET
    //MDC
#if 0
    ret &= DDKIomuxSetPinMux(DDK_IOMUX_PIN_ENET_MDC, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
    ret &= DDKIomuxSetPadConfig(DDK_IOMUX_PAD_ENET_MDC,
                         DDK_IOMUX_PAD_SLEW_SLOW,
                         DDK_IOMUX_PAD_DRIVE_40_OHM,
                         DDK_IOMUX_PAD_SPEED_NULL,
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
                         DDK_IOMUX_PAD_PULL_DOWN_100K,
                         DDK_IOMUX_PAD_HYSTERESIS_NULL,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);

    //MDIO
    ret &= DDKIomuxSetPinMux(DDK_IOMUX_PIN_ENET_MDIO, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
    ret &= DDKIomuxSetPadConfig(DDK_IOMUX_PAD_ENET_MDIO,
                         DDK_IOMUX_PAD_SLEW_FAST,
                         DDK_IOMUX_PAD_DRIVE_40_OHM,
                         DDK_IOMUX_PAD_SPEED_MED_100MHZ,
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
                         DDK_IOMUX_PAD_PULL_DOWN_100K,
                         DDK_IOMUX_PAD_HYSTERESIS_NULL,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);
    ret &= DDKIomuxSelectInput(DDK_IOMUX_SELECT_INPUT_ENET_IPP_IND_MAC0_MDIO, 0);

    //RGMII TX
    ret &= DDKIomuxSetPinMux(DDK_IOMUX_PIN_RGMII_TXC, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
    ret &= DDKIomuxSetPadConfig(DDK_IOMUX_PAD_RGMII_TXC,
                         DDK_IOMUX_PAD_SLEW_SLOW,
                         DDK_IOMUX_PAD_DRIVE_40_OHM,
                         DDK_IOMUX_PAD_SPEED_NULL,
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
                         DDK_IOMUX_PAD_PULL_DOWN_100K,
                         DDK_IOMUX_PAD_HYSTERESIS_NULL,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);

    ret &= DDKIomuxSetPinMux(DDK_IOMUX_PIN_RGMII_TD0, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
    ret &= DDKIomuxSetPadConfig(DDK_IOMUX_PAD_RGMII_TD0,
                         DDK_IOMUX_PAD_SLEW_SLOW,
                         DDK_IOMUX_PAD_DRIVE_40_OHM,
                         DDK_IOMUX_PAD_SPEED_NULL,
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
                         DDK_IOMUX_PAD_PULL_DOWN_100K,
                         DDK_IOMUX_PAD_HYSTERESIS_NULL,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);

    ret &= DDKIomuxSetPinMux(DDK_IOMUX_PIN_RGMII_TD1, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
    ret &= DDKIomuxSetPadConfig(DDK_IOMUX_PAD_RGMII_TD1,
                         DDK_IOMUX_PAD_SLEW_SLOW,
                         DDK_IOMUX_PAD_DRIVE_40_OHM,
                         DDK_IOMUX_PAD_SPEED_NULL,
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
                         DDK_IOMUX_PAD_PULL_DOWN_100K,
                         DDK_IOMUX_PAD_HYSTERESIS_NULL,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);

    ret &= DDKIomuxSetPinMux(DDK_IOMUX_PIN_RGMII_TD2, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
    ret &= DDKIomuxSetPadConfig(DDK_IOMUX_PAD_RGMII_TD2,
                         DDK_IOMUX_PAD_SLEW_SLOW,
                         DDK_IOMUX_PAD_DRIVE_40_OHM,
                         DDK_IOMUX_PAD_SPEED_NULL,
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
                         DDK_IOMUX_PAD_PULL_DOWN_100K,
                         DDK_IOMUX_PAD_HYSTERESIS_NULL,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);

    ret &= DDKIomuxSetPinMux(DDK_IOMUX_PIN_RGMII_TD3, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
    ret &= DDKIomuxSetPadConfig(DDK_IOMUX_PAD_RGMII_TD3,
                         DDK_IOMUX_PAD_SLEW_SLOW,
                         DDK_IOMUX_PAD_DRIVE_40_OHM,
                         DDK_IOMUX_PAD_SPEED_NULL,
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
                         DDK_IOMUX_PAD_PULL_DOWN_100K,
                         DDK_IOMUX_PAD_HYSTERESIS_NULL,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);

    ret &= DDKIomuxSetPinMux(DDK_IOMUX_PIN_RGMII_TX_CTL, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
    ret &= DDKIomuxSetPadConfig(DDK_IOMUX_PAD_RGMII_TX_CTL,
                        DDK_IOMUX_PAD_SLEW_SLOW,
                        DDK_IOMUX_PAD_DRIVE_40_OHM,
                        DDK_IOMUX_PAD_SPEED_NULL,
                        DDK_IOMUX_PAD_ODT_NULL,
                        DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
                        DDK_IOMUX_PAD_PULL_DOWN_100K,
                        DDK_IOMUX_PAD_HYSTERESIS_NULL,
                        DDK_IOMUX_PAD_DDRINPUT_NULL,
                        DDK_IOMUX_PAD_DDRSEL_NULL);


    //  RSTn
	ret &= DDKIomuxSetPinMux(RGMII_nRST_PIN, RGMII_nRST_PIN_MODE, DDK_IOMUX_PIN_SION_REGULAR);
	ret &= DDKIomuxSetPadConfig(RGMII_nRST_PAD,
                     DDK_IOMUX_PAD_SLEW_SLOW,
                     DDK_IOMUX_PAD_DRIVE_40_OHM,
                     DDK_IOMUX_PAD_SPEED_NULL,
                     DDK_IOMUX_PAD_ODT_NULL,
                     DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
                     DDK_IOMUX_PAD_PULL_KEEPER,
                     DDK_IOMUX_PAD_HYSTERESIS_NULL,
                     DDK_IOMUX_PAD_DDRINPUT_NULL,
                     DDK_IOMUX_PAD_DDRSEL_NULL);

	//  CLK_25M
	ret &= DDKIomuxSetPinMux(DDK_IOMUX_PIN_ENET_REF_CLK, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
	ret &= DDKIomuxSetPadConfig(DDK_IOMUX_PAD_ENET_REF_CLK,
                     DDK_IOMUX_PAD_SLEW_NULL,
                     DDK_IOMUX_PAD_DRIVE_NULL,
                     DDK_IOMUX_PAD_SPEED_NULL,
                     DDK_IOMUX_PAD_ODT_NULL,
                     DDK_IOMUX_PAD_OPENDRAIN_NULL,
                     DDK_IOMUX_PAD_PULL_NULL,
                     DDK_IOMUX_PAD_HYSTERESIS_NULL,
                     DDK_IOMUX_PAD_DDRINPUT_NULL,
                     DDK_IOMUX_PAD_DDRSEL_NULL);

    /*
        RXD0 is PHYADDRESS0 (hardwired to 1)
        RXD1 is PHYADDRESS1 (hardwired to 0)
        LED_ACT is PHYADDRESS2 (hardwired to 0)
        PHYADDRESS[4:3] = 00b

        So, our PHYADDRESS is 0x1 (00001b)
    */

    //  RGMII RX
	ret &= DDKIomuxSetPinMux(DDK_IOMUX_PIN_RGMII_RXC, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
	ret &= DDKIomuxSetPadConfig(DDK_IOMUX_PAD_RGMII_RXC,
                         DDK_IOMUX_PAD_SLEW_SLOW,
                         DDK_IOMUX_PAD_DRIVE_40_OHM,
                         DDK_IOMUX_PAD_SPEED_NULL,
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
                         DDK_IOMUX_PAD_PULL_DOWN_100K,
                         DDK_IOMUX_PAD_HYSTERESIS_NULL,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);
	ret &= DDKIomuxSelectInput(DDK_IOMUX_SELECT_INPUT_ENET_IPP_IND_MAC0_RXCLK,0);

	ret &= DDKIomuxSetPinMux(DDK_IOMUX_PIN_RGMII_RD0, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
    ret &= DDKIomuxSetPadConfig(DDK_IOMUX_PAD_RGMII_RD0,
                        DDK_IOMUX_PAD_SLEW_SLOW,
                        DDK_IOMUX_PAD_DRIVE_40_OHM,
                        DDK_IOMUX_PAD_SPEED_NULL,
                        DDK_IOMUX_PAD_ODT_NULL,
                        DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
                        DDK_IOMUX_PAD_PULL_DOWN_100K,
                        DDK_IOMUX_PAD_HYSTERESIS_NULL,
                        DDK_IOMUX_PAD_DDRINPUT_NULL,
                        DDK_IOMUX_PAD_DDRSEL_NULL);
    ret &= DDKIomuxSelectInput(DDK_IOMUX_SELECT_INPUT_ENET_IPP_IND_MAC0_RXDATA_0,0);

	ret &= DDKIomuxSetPinMux(DDK_IOMUX_PIN_RGMII_RD1, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
	ret &= DDKIomuxSetPadConfig(DDK_IOMUX_PAD_RGMII_RD1,
                        DDK_IOMUX_PAD_SLEW_SLOW,
                        DDK_IOMUX_PAD_DRIVE_40_OHM,
                        DDK_IOMUX_PAD_SPEED_NULL,
                        DDK_IOMUX_PAD_ODT_NULL,
                        DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
                        DDK_IOMUX_PAD_PULL_DOWN_100K,
                        DDK_IOMUX_PAD_HYSTERESIS_NULL,
                        DDK_IOMUX_PAD_DDRINPUT_NULL,
                        DDK_IOMUX_PAD_DDRSEL_NULL);
	ret &= DDKIomuxSelectInput(DDK_IOMUX_SELECT_INPUT_ENET_IPP_IND_MAC0_RXDATA_1,0);

	ret &= DDKIomuxSetPinMux(DDK_IOMUX_PIN_RGMII_RD2, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
	ret &= DDKIomuxSetPadConfig(DDK_IOMUX_PAD_RGMII_RD2,
                        DDK_IOMUX_PAD_SLEW_SLOW,
                        DDK_IOMUX_PAD_DRIVE_40_OHM,
                        DDK_IOMUX_PAD_SPEED_NULL,
                        DDK_IOMUX_PAD_ODT_NULL,
                        DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
                        DDK_IOMUX_PAD_PULL_DOWN_100K,
                        DDK_IOMUX_PAD_HYSTERESIS_NULL,
                        DDK_IOMUX_PAD_DDRINPUT_NULL,
                        DDK_IOMUX_PAD_DDRSEL_NULL);
	ret &= DDKIomuxSelectInput(DDK_IOMUX_SELECT_INPUT_ENET_IPP_IND_MAC0_RXDATA_2,0);

	ret &= DDKIomuxSetPinMux(DDK_IOMUX_PIN_RGMII_RD3, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
	ret &= DDKIomuxSetPadConfig(DDK_IOMUX_PAD_RGMII_RD3,
                        DDK_IOMUX_PAD_SLEW_SLOW,
                        DDK_IOMUX_PAD_DRIVE_40_OHM,
                        DDK_IOMUX_PAD_SPEED_NULL,
                        DDK_IOMUX_PAD_ODT_NULL,
                        DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
                        DDK_IOMUX_PAD_PULL_DOWN_100K,
                        DDK_IOMUX_PAD_HYSTERESIS_NULL,
                        DDK_IOMUX_PAD_DDRINPUT_NULL,
                        DDK_IOMUX_PAD_DDRSEL_NULL);
	ret &= DDKIomuxSelectInput(DDK_IOMUX_SELECT_INPUT_ENET_IPP_IND_MAC0_RXDATA_3,0);

	ret &= DDKIomuxSetPinMux(DDK_IOMUX_PIN_RGMII_RX_CTL, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
    ret &= DDKIomuxSetPadConfig(DDK_IOMUX_PAD_RGMII_RX_CTL,
                         DDK_IOMUX_PAD_SLEW_SLOW,
                         DDK_IOMUX_PAD_DRIVE_40_OHM,
                         DDK_IOMUX_PAD_SPEED_NULL,
                         DDK_IOMUX_PAD_ODT_NULL,
                         DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
                         DDK_IOMUX_PAD_PULL_DOWN_100K,
                         DDK_IOMUX_PAD_HYSTERESIS_NULL,
                         DDK_IOMUX_PAD_DDRINPUT_NULL,
                         DDK_IOMUX_PAD_DDRSEL_NULL);
    ret &= DDKIomuxSelectInput(DDK_IOMUX_SELECT_INPUT_ENET_IPP_IND_MAC0_RXEN,0);
#endif

		ret &= DDKGpioSetConfig(RGMII_nRST_GPIO_PORT, RGMII_nRST_GPIO_PIN, DDK_GPIO_DIR_OUT, DDK_GPIO_INTR_NONE);
		ret &= DDKGpioWriteDataPin(RGMII_nRST_GPIO_PORT, RGMII_nRST_GPIO_PIN, 1);

    }

    return ret;
}

//------------------------------------------------------------------------------
//
// Function: BSPENETClockConfig
//
// This fuction will enable/disable the ENET Clock according to the parameter
// "Enable".
//
// Parameters:
//      Enable
//          [in] TRUE for enabling the ENET clock, FALSE for disabling the
//               ENET clock.
//
// Return Value:
//      TRUE for success, FALSE if failure.
//
//------------------------------------------------------------------------------

BOOL BSPENETClockConfig( IN BOOL Enable )
{
    BOOL cfgState;

    DEBUGMSG(ZONE_FUNCTION, (TEXT("ENET: +BSPENETClockConfig\r\n")));

    if(Enable)
    {
        cfgState = DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_ENET, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        if(cfgState == FALSE)
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("BSPENETClockConfig: Enable ENET IPG clock failed.\r\n")));
            return FALSE;
        }

        cfgState = DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_ENET, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        if(cfgState == FALSE)
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("BSPENETClockConfig: Enable ENET AHB clock failed.\r\n")));
            return FALSE;
        }
    }
    else
    {
        cfgState = DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_ENET, DDK_CLOCK_GATE_MODE_DISABLED);
        if(cfgState == FALSE)
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("BSPENETClockConfig: Disable ENET IPG clock failed.\r\n")));
            return FALSE;
        }

        cfgState = DDKClockSetGatingMode(DDK_CLOCK_GATE_INDEX_ENET, DDK_CLOCK_GATE_MODE_DISABLED);
        if(cfgState == FALSE)
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("BSPENETClockConfig: Disable ENET AHB clock failed.\r\n")));
            return FALSE;
        }
    }

    DEBUGMSG(ZONE_FUNCTION, (TEXT("ENET: -BSPENETClockConfig\r\n")));

    return TRUE;
}

//------------------------------------------------------------------------------
//
// Function: void BSPPhyReset()
//
// This function resets the ENET
//
// Return Value:
//          None.
//
//------------------------------------------------------------------------------
void BSPPhyReset()
{
	DDKGpioWriteDataPin(RGMII_nRST_GPIO_PORT, RGMII_nRST_GPIO_PIN, 0);
	Sleep(500);
	DDKGpioWriteDataPin(RGMII_nRST_GPIO_PORT, RGMII_nRST_GPIO_PIN, 1);
}

/*
 * KSZ9021RN /KSZ9031RN workaround
 * This function sets up the proper phyaddress and modes that the
 * ethernet PHY expects on reset. This is done in HW normally, but
 * this SW rework is required for the board with KSZ9021RN / KSZ9031RN PHY.
 */
BOOL KSZ9031RN_workaround()
{
	/* 
	 * reference: power-on Strapping pins on schematic
	 * mode and address are hardwired on sdb platform
	 */
	
	return TRUE;
}

//------------------------------------------------------------------------------
//
// Function:    EthernetIntf BSPENETInterface()
//
//
// Return Value:
//        This fuction returns ethernet interface type
//
//------------------------------------------------------------------------------
EthernetIntf BSPENETInterface()
{
    return RGMII_INTERFACE;
}

//------------------------------------------------------------------------------
//
// Function:    DWORD BSPENETGetIrq()
//
// This fuction returns the ENET_IRQ
//      index
//          [in]    Index specifying the ENET module.
//
// Return Value:
//        ENET irq number.
//
//------------------------------------------------------------------------------
DWORD BSPENETGetIrq(DWORD index)
{
    UNREFERENCED_PARAMETER(index);
    return IRQ_ENET;
}

//------------------------------------------------------------------------------
//
// Function:    BOOL BSPENETEnableDBSWP()
//
//
// Return Value:
//        This fuction returns TRUE if need to enable descriptor byte swapping
//
//------------------------------------------------------------------------------
BOOL BSPENETEnableDBSWP()
{
    return TRUE;
}
