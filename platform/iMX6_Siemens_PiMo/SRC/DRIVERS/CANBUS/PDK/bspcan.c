//------------------------------------------------------------------------------
//
//  Copyright (C) 2009-2010, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  bspcan.c
//
//  Implementation of CAN Driver
//
//  This file implements the bsp level code for CANBUS driver.
//
//-----------------------------------------------------------------------------
#pragma warning(push)
#pragma warning(disable: 4115 4201 4204 4214)
#include <windows.h>
#include <nkintr.h>
#include <ceddk.h>
#pragma warning(pop)

#include "bsp.h"



//------------------------------------------------------------------------------
// External Variables
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Global Variables
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Defines

//-----------------------------------------------------------------------------
// Types
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Local Variables
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Local Functions
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//
// Function:  CANConfigureGPIO
//
// This function makes the DDK call to configure the IOMUX
// 
//
// Parameters:
//      canmode
//          [in] can iomux index mode
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------

BOOL CANConfigureGPIO(DWORD index)
{
    switch (index)
    {         
    case 1:     
		// TC : IOMux deleted because the configuration is made in the 
		break;
        
    default:
		RETAILMSG(1, (L"%S: Port %d is not supported\r\n", __FUNCTION__, index));
		return FALSE; 
    }

    return TRUE;
}


//-----------------------------------------------------------------------------
//
// Function: BSPCANClockConfig
//
// This function is a wrapper for CAN  to enable/disable its clock using a valid
// CRM handle.
//
// Parameters:
//      index
//          [in]    Index specifying the CAN module.
//      bEnable
//          [in]    TRUE if CAN Clock is to be enabled. FALSE if CAN Clock is
//                  to be disabled.
//
// Returns:
//      TRUE if successfully performed the required action.
//
//-----------------------------------------------------------------------------

BOOL BSPCANClockConfig( DWORD index,IN BOOL Enable )
{
    
    BOOL cfgState;
    DDK_CLOCK_GATE_INDEX Ci;

    switch (index)
    {
        case 1:
           Ci=DDK_CLOCK_GATE_INDEX_CAN1_SERIAL;
           break;
        case 2:
           Ci=DDK_CLOCK_GATE_INDEX_CAN2_SERIAL;
            break;
       
        default:
            return  FALSE;
    } 
   
    if(Enable)
    {
        cfgState = DDKClockSetGatingMode(Ci, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        if(cfgState == FALSE)
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("BSPCANClockConfig: Enable CAN clock failed.\r\n")));
            return FALSE;
        }

    }
    else
    {
        cfgState = DDKClockSetGatingMode(Ci,DDK_CLOCK_GATE_MODE_DISABLED );
        if(cfgState == FALSE)
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("BSPCANClockConfig: Disable CAN clock failed.\r\n")));
            return FALSE;
        }

    }

return TRUE;

}


//-----------------------------------------------------------------------------
//
// Function:  BSPCANConfigureGPIO
//
// This function makes the DDK call to configure the IOMUX
// pins required for the CAN.
//
// Parameters:
//      index
//          [in] Index of the CAN device requested.
//
// Returns:
//      TRUE if success; FALSE if failure.
//
//-----------------------------------------------------------------------------

void  BSPCANConfigureGPIO(DWORD index)
{
	CANConfigureGPIO(index);
}

//-----------------------------------------------------------------------------
//
// Function:  CANGetBaseRegAddr
//
// This function returns the physical base address for the
// CAN registers based on the device index requested.
//
// Parameters:
//      index
//          [in] Index of the CAN device requested.
//
// Returns:
//      Physical base address for CAN registers, or 0 if an
//      invalid index was passed.
//
//-----------------------------------------------------------------------------


DWORD  BSPGetCANBaseRegAddr(DWORD index)
{
   
   switch (index)
   {
       case 1:
           return CSP_BASE_REG_PA_CAN1;
       case 2:
           return CSP_BASE_REG_PA_CAN2;
      
       default:
           return 0;
   } 

}

//-----------------------------------------------------------------------------
//
// Function:  BSPGetCANIRQ
//
// This function returns the physical base address for the
// CAN registers based on the device index requested.
//
// Parameters:
//      index
//          [in] Index of the CAN device requested.
//
// Returns:
//      Physical IRQ  for CAN  bus
//
//-----------------------------------------------------------------------------
DWORD  BSPGetCANIRQ(DWORD index)
{
  
  switch (index)
     {
         case 1:
             return IRQ_CAN1;
         case 2:
             return IRQ_CAN2;
         default:
             return 0;
     }
}

//-----------------------------------------------------------------------------
//
// Function:  BSPSetCanPower
//
// This function enabe can transceiver power for special canbus controller
//
// Parameters:
//      index
//          [in] Index of the CAN device requested.
//      bEnable
//
//          [in]    TRUE if CAN Power  is to be enabled. FALSE if CAN Power is
//                  to be disabled.
//
// Returns:
//     
//
//-----------------------------------------------------------------------------
void   BSPSetCanPowerEnable(DWORD index,BOOL Enable)
{
    UNREFERENCED_PARAMETER(index);
    UNREFERENCED_PARAMETER(Enable);

    //DDKGpioSetConfig(DDK_GPIO_PORT4, 15, DDK_GPIO_DIR_OUT, DDK_GPIO_INTR_NONE);
    //DDKGpioWriteDataPin(DDK_GPIO_PORT4, 15, 1);
 
    DDKGpioSetConfig(DDK_GPIO_PORT4, 5, DDK_GPIO_DIR_OUT, DDK_GPIO_INTR_NONE);
    DDKGpioWriteDataPin(DDK_GPIO_PORT4, 5, 0);

}

//-----------------------------------------------------------------------------
//
// Function:  BSPGetCanBaseClock
//
// This function gets the CAN controller base clock
//
// Parameters:
//      None
//
// Returns:
//     The base frequency is Hz
//
//-----------------------------------------------------------------------------
UINT32 BSPGetCanBaseClock()
{
	UINT32 freq;
	DDKClockGetFreq(DDK_CLOCK_SIGNAL_CAN, &freq);
	return freq;
}
