//
//  Copyright (c) Microsoft Corporation.  All rights reserved.
//
//  Use of this source code is subject to the terms of the Microsoft end-user
//  license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
//  If you did not accept the terms of the EULA, you are not authorized to use
//  this source code. For a copy of the EULA, please see the LICENSE.RTF on your
//  install media.
//
//------------------------------------------------------------------------------
//
//  Copyright (C) 2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT 
//
//------------------------------------------------------------------------------
//
//  File:  cantest.cpp
//
//  This module provides a stream interface to query HDMI EDID
//  capabilities.
//
//------------------------------------------------------------------------------

#pragma warning(push)
#pragma warning(disable: 4115 4201 4204 4214)
#include <windows.h>
#include <Devload.h>
#include <windev.h>
#include <ceddk.h>
#pragma warning(pop)

#pragma warning(push)
#pragma warning(disable: 4512)
#include "marshal.hpp" //helper classes to marshal/alloc embedded/async buffer
#pragma warning(pop)

#include "bsp.h"
#include "canbus.h"
#include "cantest_iocontrol.h"

//------------------------------------------------------------------------------
// External Functions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// External Variables
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Defines
//------------------------------------------------------------------------------

#ifdef DEBUG
// Debug zone bit positions
#define ZONECANTEST_INIT         0
#define ZONECANTEST_DEINIT       1
#define ZONECANTEST_OPEN         2
#define ZONECANTEST_CLOSE        3
#define ZONECANTEST_IOCTL        4
#define ZONECANTEST_THREAD       5
#define ZONECANTEST_FUNCTION     13
#define ZONECANTEST_WARN         14
#define ZONECANTEST_ERROR        15

// Debug zone masks
#define ZONEMASK_INIT       (1 << ZONECANTEST_INIT)
#define ZONEMASK_DEINIT     (1 << ZONECANTEST_DEINIT)
#define ZONEMASK_OPEN       (1 << ZONECANTEST_OPEN)
#define ZONEMASK_CLOSE      (1 << ZONECANTEST_CLOSE)
#define ZONEMASK_IOCTL      (1 << ZONECANTEST_IOCTL)
#define ZONEMASK_THREAD     (1 << ZONECANTEST_THREAD)
#define ZONEMASK_FUNCTION   (1 << ZONECANTEST_FUNCTION)
#define ZONEMASK_WARN       (1 << ZONECANTEST_WARN)
#define ZONEMASK_ERROR      (1 << ZONECANTEST_ERROR)

#ifdef ZONE_INIT
#undef ZONE_INIT
#endif //ZONE_INIT

#ifdef ZONE_ERROR
#undef ZONE_ERROR
#endif //ZONE_ERROR

#define ZONE_INIT       DEBUGZONE(ZONECANTEST_INIT)
#define ZONE_DEINIT     DEBUGZONE(ZONECANTEST_DEINIT)
#define ZONE_OPEN       DEBUGZONE(ZONECANTEST_OPEN)
#define ZONE_CLOSE      DEBUGZONE(ZONECANTEST_CLOSE)
#define ZONE_IOCTL      DEBUGZONE(ZONECANTEST_IOCTL)
#define ZONE_THREAD     DEBUGZONE(ZONECANTEST_THREAD)
#define ZONE_FUNCTION   DEBUGZONE(ZONECANTEST_FUNCTION)
#define ZONE_WARN       DEBUGZONE(ZONECANTEST_WARN)
#define ZONE_ERROR      DEBUGZONE(ZONECANTEST_ERROR)

DBGPARAM dpCurSettings = {
    TEXT("CanTest"), {
        TEXT("Init"),TEXT("Deinit"),TEXT("Open"),TEXT("Close"),
        TEXT("IOCtl"),TEXT("Thread"),TEXT(""),TEXT(""),
        TEXT(""),TEXT(""),TEXT(""),TEXT(""),
        TEXT(""),TEXT("Function"),TEXT("Warning"),TEXT("Error") },
    (ZONEMASK_WARN | ZONEMASK_ERROR)
};
#endif // DEBUG

//------------------------------------------------------------------------------
// Types
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Global Variables
//------------------------------------------------------------------------------

HANDLE hCanDev = INVALID_HANDLE_VALUE;
//------------------------------------------------------------------------------
// Local Variables
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Local Functions
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//
// Function: can_openDevice
//
// This function calls CANOpenHandle to acquire an handle on the CAN driver
//
// Parameters:
//      None
//
// Returns:
//      TRUE if success, FALSE if failure
//
//-----------------------------------------------------------------------------
static BOOL can_openDevice ( )
{
	// Use CAN SDK API to open CAN device
	hCanDev = CANOpenHandle(CAN_DEV_NAME);

	if (INVALID_HANDLE_VALUE == hCanDev) 
	{
		RETAILMSG(1, (TEXT("can_openDevice: error opening CAN device!!!\r\n")));
		hCanDev = NULL;
		return FALSE;
	}

	return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function: can_closeDevice
//
// This function calls CANCloseHandle to release an handle on the CAN driver
//
// Parameters:
//      None
//
// Returns:
//      TRUE if success, FALSE if failure
//
//-----------------------------------------------------------------------------
static BOOL can_closeDevice ( )
{
	if (INVALID_HANDLE_VALUE == hCanDev) 
	{
		return FALSE;
	}
	CANCloseHandle(hCanDev);
	hCanDev = INVALID_HANDLE_VALUE;
	return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function: CTE_Init
//
// The Device Manager calls this function as a result of a call to the
//      ActivateDevice() function.
//
// Parameters:
//      pContext
//          [in] Pointer to a string containing the registry path to the
//                active key for the stream interface driver.
//
// Returns:
//      Returns a handle to the device context created if successful. Returns
//      zero if not successful.
//
//-----------------------------------------------------------------------------
DWORD CTE_Init(LPCTSTR pContext)
{    
    DEBUGMSG (ZONE_INIT|ZONE_FUNCTION, (TEXT("CTE_Init +\r\n")));

    DEBUGMSG (ZONE_INIT|ZONE_FUNCTION, (TEXT("CTE_Init - \r\n")));
	
    // Otherwise return the created instance
    return (DWORD)pContext;
}


//-----------------------------------------------------------------------------
//
// Function: CTE_Deinit
//
// This function uninitializes a device.
//
// Parameters:
//      hDeviceContext
//          [in] Handle to the device context.
//
// Returns:
//      TRUE indicates success. FALSE indicates failure.
//
//-----------------------------------------------------------------------------
BOOL CTE_Deinit(DWORD hDeviceContext)
{

    DEBUGMSG (ZONE_DEINIT|ZONE_FUNCTION, (TEXT("CTE_Deinit +DeviceContext=0x%x\r\n"),hDeviceContext));

    DEBUGMSG (ZONE_DEINIT|ZONE_FUNCTION, (TEXT("CTE_Deinit -\r\n")));

    return TRUE;
}


//-----------------------------------------------------------------------------
//
// Function: CTE_Open
//
// This function opens a device for reading, writing, or both.
//
// Parameters:
//      hDeviceContext
//          [in] Handle to the device context. The XXX_Init function creates
//                and returns this handle.
//      AccessCode
//          [in] Access code for the device. The access is a combination of
//                read and write access from CreateFile.
//      ShareMode
//          [in] File share mode of the device. The share mode is a
//                combination of read and write access sharing from CreateFile.
//
// Returns:
//      This function returns a handle that identifies the open context of
//      the device to the calling application.
//
//-----------------------------------------------------------------------------
DWORD CTE_Open(DWORD hDeviceContext, DWORD AccessCode, DWORD ShareMode)
{
    DEBUGMSG (ZONE_OPEN|ZONE_FUNCTION, (TEXT("CTE_Open +hDeviceContext=0x%x\r\n"),hDeviceContext));

    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(AccessCode);
    UNREFERENCED_PARAMETER(ShareMode);
    
    DEBUGMSG (ZONE_OPEN|ZONE_FUNCTION, (TEXT("CTE_Open -\r\n")));

	if (!can_openDevice()) {
		RETAILMSG(1, (L"CTE_Init: Failed opening device\r\n"));
		return 0;
	}
    
    return hDeviceContext;
}

//-----------------------------------------------------------------------------
//
// Function: CTE_Close
//
// This function opens a device for reading, writing, or both.
//
// Parameters:
//      hOpenContext
//          [in] Handle returned by the XXX_Open function, used to identify
//                the open context of the device.
//
// Returns:  
//      TRUE indicates success. FALSE indicates failure.
//
//-----------------------------------------------------------------------------
BOOL CTE_Close(DWORD hOpenContext)
{
    DEBUGMSG (ZONE_CLOSE|ZONE_FUNCTION, (TEXT("CTE_Close +\r\n")));

    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(hOpenContext);

	can_closeDevice();

	DEBUGMSG (ZONE_CLOSE|ZONE_FUNCTION, (TEXT("CTE_Close -\r\n")));

    return TRUE;
}

//-----------------------------------------------------------------------------
//
// Function: CTE_IOControl
//
// This function sends a command to a device.
//
// Parameters:
//      hOpenContext 
//          [in] Handle to the open context of the device. The XXX_Open 
//                function creates and returns this identifier.
//      dwCode 
//          [in] I/O control operation to perform. These codes are 
//                device-specific and are usually exposed to developers through 
//                a header file.
//      pBufIn 
//          [in] Pointer to the buffer containing data to transfer to the 
//                device. 
//      dwLenIn 
//         [in] Number of bytes of data in the buffer specified for pBufIn.
//
//      pBufOut 
//          [out] Pointer to the buffer used to transfer the output data 
//                  from the device.
//      dwLenOut 
//          [in] Maximum number of bytes in the buffer specified by pBufOut.
//
//      pdwActualOut 
//          [out] Pointer to the DWORD buffer that this function uses to 
//                  return the actual number of bytes received from the device.
//
// Returns:  
//      The new data pointer for the device indicates success. A value of -1 
//      indicates failure.
//
//-----------------------------------------------------------------------------
BOOL CTE_IOControl(DWORD hOpenContext, DWORD dwCode, PBYTE pBufIn, 
                   DWORD dwLenIn, PBYTE pBufOut, DWORD dwLenOut,
                   PDWORD pdwActualOut)
{
    BOOL bRet = FALSE;
	PCANTEST_TRANSFER pTransfer;
	PUINT8 pData;

    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(hOpenContext);
    UNREFERENCED_PARAMETER(pdwActualOut);

    switch (dwCode)
    {
		case 0 :
		{
			break;
		}

		case CANTEST_IOCTL_READ :
		{
			pData = NULL;
			pTransfer = (PCANTEST_TRANSFER)pBufIn;
			if (NULL == pBufIn)
				break;

			if (pTransfer->DataLen != 0) {
				if (CeOpenCallerBuffer((PVOID *)&pData, 
									   pTransfer->Data,
									   pTransfer->DataLen,
									   ARG_O_PTR,
									   FALSE) != S_OK) {
					RETAILMSG(1, (L"Failed in CeOpenCallerBuffer\r\n"));
					break;
				}
			}

			pTransfer->iResult = -1;

			// Setup CAN_PACKET structure
			CAN_PACKET packet;
			packet.dwTimeout = pTransfer->dwTimeout;
			packet.bAbortTxIfTimeout = pTransfer->bAbortTxIfTimeout;
			packet.byRW = CAN_RW_READ;
			packet.pbyBuf = pData;
			packet.PRIO = 0; // Don't care in reception
			packet.wLen = (WORD)pTransfer->DataLen;
			packet.lpiResult = &pTransfer->iResult;

			// Setup CAN_TRANSFER_BLOCK structure
			CAN_TRANSFER_BLOCK xfer;
			xfer.iNumPackets = 1;
			xfer.pCANPackets = &packet;

			// Request transfer
			CANTransfer(hCanDev, &xfer);

			// Copy back data, datalen, format, ID, ID len and time stamp to PCANTEST_TRANSFER structure
			pTransfer->ID = packet.ID;
			pTransfer->IDLen = (packet.format == CAN_STANDARD ? 11 : 29);
			pTransfer->bRemote = (packet.frame == CAN_REMOTE);
			memcpy(pTransfer->Data, packet.pbyBuf, packet.wLen);
			pTransfer->TimeStamp = packet.timestamp;
			pTransfer->DataLen = (WORD)packet.wLen;

			if (pData != NULL) {
				CeCloseCallerBuffer(pData,
									pTransfer->Data,
									pTransfer->DataLen,
									ARG_O_PTR);

			}

			bRet = TRUE;

			break;
		}
		
		case CANTEST_IOCTL_WRITE :
		{
			pData = NULL;
			if (NULL == pBufIn)
				break;
			pTransfer = (PCANTEST_TRANSFER)pBufIn;

			// Don't open buffer for remote and zero size messages
			if (pTransfer->bRemote != CAN_REMOTE && pTransfer->DataLen != 0) {
				if (pTransfer->Data != NULL) {
					if (CeOpenCallerBuffer((PVOID *)&pData, 
										   pTransfer->Data,
										   pTransfer->DataLen,
										   ARG_I_PTR,
										   FALSE) != S_OK) {
						RETAILMSG(1, (L"Failed in CeOpenCallerBuffer\r\n"));
						break;
					}
				}
			}

			CAN_PACKET packet;
			int iResult;
			packet.dwTimeout = pTransfer->dwTimeout;
			packet.bAbortTxIfTimeout = pTransfer->bAbortTxIfTimeout;
			packet.byRW = CAN_RW_WRITE;
			
			if (!pTransfer->bRemote) {
				packet.frame = CAN_DATA;
			}
			else {
				packet.frame = CAN_REMOTE;
			}

			if (pTransfer->IDLen == 11) {
				packet.format = CAN_STANDARD;
			}
			else if (pTransfer->IDLen == 29) {
				packet.format = CAN_EXTENDED;
			}
			else {
				RETAILMSG(1, (L"Invalid ID len %d\r\n", pTransfer->IDLen));
				break;
			}
			packet.pbyBuf = pData;
			packet.PRIO = pTransfer->byLocalPrio;
			packet.wLen = (WORD)pTransfer->DataLen;
			packet.ID = pTransfer->ID;
			packet.lpiResult = &iResult;

			CAN_TRANSFER_BLOCK xfer;

			xfer.iNumPackets = 1;
			xfer.pCANPackets = &packet;

			CANTransfer(hCanDev, &xfer);

			pTransfer->iResult = iResult;

			if (pData != NULL) {
				CeCloseCallerBuffer(pData,
									pTransfer->Data,
									pTransfer->DataLen,
									ARG_I_PTR);
			}

			bRet = TRUE;
			break;
			
		}

		case CANTEST_IOCTL_ENABLE_LOOPBACK:
		{
			bRet = CANEnableLoopback(hCanDev, TRUE);
			break;
		}

		case CANTEST_IOCTL_DISABLE_LOOPBACK:
		{
			bRet = CANEnableLoopback(hCanDev, FALSE);
			break;
		}

		case CANTEST_IOCTL_SET_TIMING:
		{
			PCAN_TIMING p_timing = (PCAN_TIMING)pBufIn;
			bRet = CANSetTiming(hCanDev, 
								p_timing->Prescaler,
								p_timing->PropSeg,
								p_timing->Phase1Seg,
								p_timing->Phase2Seg,
								p_timing->RJW);
			break;
		}

		case CANTEST_IOCTL_GET_TIMING:
		{
			PCAN_TIMING p_timing = (PCAN_TIMING)pBufOut;
			bRet = CANGetTiming(hCanDev, 
								&p_timing->BaseClock,
								&p_timing->Prescaler,
								&p_timing->PropSeg,
								&p_timing->Phase1Seg,
								&p_timing->Phase2Seg,
								&p_timing->RJW);
			break;
		}

		case CANTEST_IOCTL_GET_BITRATE :
		{
			PUINT32 bitrate = (PUINT32)pBufOut;
			*bitrate = CANGetBitRate(hCanDev);
			bRet = TRUE;
			break;
		}

		case CANTEST_IOCTL_SET_BITRATE :
		{
			PUINT32 reqBitrate = (PUINT32)pBufIn;
			PUINT32 actBitrate = (PUINT32)pBufOut;
			*actBitrate = CANSetBitRate(hCanDev, *reqBitrate);
			bRet = TRUE;
			break;
		}

		case CANTEST_IOCTL_ADD_FILTER:
		{
			HANDLE hFilter;
			PCAN_FILTER pFilter;

			pFilter = (PCAN_FILTER)pBufIn;
			hFilter = CANAddFilter(hCanDev, pFilter);

			bRet = (hFilter != NULL);
			*((HANDLE *)(pBufOut)) = hFilter;
			break;
		}

		case CANTEST_IOCTL_REMOVE_FILTER:
		{
			HANDLE hFilter;
			hFilter = *((HANDLE *)(pBufIn));
			bRet = CANRemoveFilter(hCanDev, hFilter);
			break;
		}

		case CANTEST_IOCTL_ENABLE_LOCAL_PRIO :
		{
			bRet = CANEnableTLPRIO(hCanDev);
			break;
		}

		case CANTEST_IOCTL_DISABLE_LOCAL_PRIO :
		{
			bRet = CANDisableTLPRIO(hCanDev);
			break;
		}

		default:
        {
            bRet = FALSE;
            break;
        }
    }

    return bRet;
}

//-----------------------------------------------------------------------------
//
// Function: CTE_DllEntry
//
// This function is called when the driver is initialized
//
// Returns:  
//      TRUE indicates success. FALSE indicates failure.
//
//-----------------------------------------------------------------------------
BOOL WINAPI CTE_DllEntry(HANDLE hInstDll, DWORD dwReason, LPVOID lpvReserved)
{
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(lpvReserved);

    switch (dwReason) {
        case DLL_PROCESS_ATTACH:
            DEBUGREGISTER((HINSTANCE) hInstDll);
            DEBUGMSG(ZONE_FUNCTION, (TEXT("CTE_DllEntry: DLL_PROCESS_ATTACH lpvReserved(0x%x)\r\n"),lpvReserved));
            DisableThreadLibraryCalls((HMODULE) hInstDll);
            break;
         
        case DLL_PROCESS_DETACH:
            DEBUGMSG(ZONE_FUNCTION, (TEXT("CTE_DllEntry: DLL_PROCESS_DETACH lpvReserved(0x%x)\r\n"),lpvReserved));
            break;
    }
    // return TRUE for success
    return TRUE;
}
