//------------------------------------------------------------------------------
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//------------------------------------------------------------------------------
//
//  Copyright (C) 2007-2010, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
#ifndef __KITL_CFG_H
#define __KITL_CFG_H


// Define a data structure that will be used to pass data between the OAL
// and KITL components.
typedef struct
{
    PCSP_CCM_REGS   g_pCCM;   // These pointers are required for serial port
    PCSP_IOMUX_REGS g_pIOMUX; // OAL may need to access the serial ports
} _OALKITLSharedDataStruct;

//------------------------------------------------------------------------------

#define OAL_KITL_ETH_INDEX          0

extern BOOL LAN911xInit(UINT8 *pAddress, UINT32 offset, UINT16 mac[3]);
extern UINT16 LAN911xSendFrame(UINT8 *pBuffer, UINT32 length);
extern UINT16 LAN911xGetFrame(UINT8 *pBuffer, UINT16 *pLength);
extern VOID LAN911xEnableInts();
extern VOID LAN911xDisableInts();
extern VOID LAN911xCurrentPacketFilter(UINT32 filter);
extern BOOL LAN911xMulticastList(UINT8 *pAddresses, UINT32 count);

extern BOOL FECInit(UINT8 *pAddress, UINT32 offset, UINT16 mac[3]);
extern BOOL FECInitDMABuffer(UINT32 address, UINT32 size);
extern UINT16 FECSendFrame(UINT8 *pData, UINT32 length);
extern UINT16 FECGetFrame(UINT8 *pData, UINT16 *pLength);
extern VOID FECEnableInts();
extern VOID FECDisableInts();
extern VOID FECCurrentPacketFilter(UINT32 filter);
extern BOOL FECMulticastList(UINT8 *pAddresses, UINT32 count);


BOOL SerialInit(KITL_SERIAL_INFO *pInfo);
VOID SerialDeinit();
UINT16 SerialRecv(UINT8 *pData, UINT16 size);
UINT16 SerialSend(UINT8 *pData, UINT16 size);
VOID SerialSendComplete(UINT16 size);
VOID SerialEnableInts();
VOID SerialDisableInts();
VOID SerialFlowControl (BOOL fOn);

extern BOOL ETHInitDMABuffer(UINT32 address, UINT32 size);
extern BOOL ENETInit(UINT8 *pAddress, UINT32 offset, UINT16 mac[3]);
extern BOOL ENETInitDMABuffer(UINT32 address, UINT32 size);
extern UINT16 ENETSendFrame(UINT8 *pData, UINT32 length);
extern UINT16 ENETGetFrame(UINT8 *pData, UINT16 *pLength);
extern VOID ENETEnableInts();
extern VOID ENETDisableInts();
extern VOID ENETPowerOff();
extern VOID ENETPowerOn();
extern VOID ENETCurrentPacketFilter(UINT32 filter);
extern BOOL ENETMulticastList(UINT8 *pAddresses, UINT32 count);

//------------------------------------------------------------------------------

#endif
