//------------------------------------------------------------------------------
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//------------------------------------------------------------------------------
//
//  Copyright (C) 2007-2010, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  ioctl_cfg.h
//
//  Configuration file for the IOCTL component.
//
//------------------------------------------------------------------------------

#ifndef __IOCTL_CFG_H
#define __IOCTL_CFG_H

#if __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------
//  RESTRICTION
//
//  This file is a configuration file for the IOCTL component.
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//
//  Define:  IOCTL_PLATFORM_TYPE/OEM
//
//  Defines the platform type and OEM string.
//
#define IOCTL_PLATFORM_TYPE                 (L"i.MX6")
#define IOCTL_PLATFORM_OEM                  (L"Freescale i.MX6 SABRE SD")
#define IOCTL_PLATFORM_MANUFACTURER         (L"Freescale")
#define IOCTL_PLATFORM_NAME                 (L"i.MX6 SABRE SD")

//------------------------------------------------------------------------------
//  Define:  IOCTL_PROCESSOR_VENDOR/NAME/CORE
//
//  Defines the processor information
//
#define IOCTL_PLATFORM_INFO		CTL_CODE(FILE_DEVICE_HAL,3078,METHOD_BUFFERED,FILE_ANY_ACCESS)

#define IOCTL_PROCESSOR_VENDOR (L"Freescale")
#define IOCTL_PROCESSOR_NAME   (L"MX6")
#define IOCTL_PROCESSOR_CORE   (L"ArmCortexA9")


//------------------------------------------------------------------------------
//
//  Define:  IOCTL_PROCESSOR_INSTRUCTION_SET
//
//  Defines the processor instruction set information
//
#define IOCTL_PROCESSOR_INSTRUCTION_SET (0)
#define IOCTL_PROCESSOR_CLOCK_SPEED     BSP_MAX_CORE_CLK_FREQ



//------------------------------------------------------------------------------
// Board-specific IOCTLS
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------

#if __cplusplus
}
#endif

#endif
