/*---------------------------------------------------------------------------
* Copyright (C) 2007-2010, Freescale Semiconductor, Inc. All Rights Reserved.
* THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
* AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
*--------------------------------------------------------------------------*/

//------------------------------------------------------------------------------
//
//  File:  ioctl_tab.h
//
//  Configuration file for the OAL IOCTL component.
//
//  This file is included by the platform's ioctl.c file and defines the
//  global IOCTL table, g_oalIoCtlTable[]. Therefore, this file may ONLY
//  define OAL_IOCTL_HANDLER entries.
//
// IOCTL CODE,                          Flags   Handler Function
//------------------------------------------------------------------------------

#include <oal_ioctl_tab.h>

{ IOCTL_HAL_ILTIMING,                    0,  OALIoCtlHalILTiming         },
{ IOCTL_HAL_POSTINIT,                    0,  OALIoCtlHalPostInit         },
{ IOCTL_HAL_PRESUSPEND,                  0,  OALIoCtlHalPresuspend       },
{ IOCTL_HAL_QUERY_DISPLAYSETTINGS,       0,  OALIoCtlQueryDispSettings   },
{ IOCTL_HAL_GET_HWENTROPY,               0,  OALIoCtlHalGetHWEntropy     },
{ IOCTL_HAL_IRQ2SYSINTR,                 0,  OALIoCtlHalIrq2Sysintr      },
{ IOCTL_HAL_FORCE_IRQ,                   0,  OALIoCtlHalForceIrq         },
{ IOCTL_HAL_UNFORCE_IRQ,                 0,  OALIoCtlHalUnforceIrq       },
{ IOCTL_HAL_QUERY_SI_VERSION,            0,  OALIoCtlQuerySiVersion      },
{ IOCTL_HAL_QUERY_BOARD_ID,              0,  OALIoCtlQueryBoardId        },
{ IOCTL_KITL_GET_INFO,                   0,  OALIoCtlKitlGetInfo         },
{ IOCTL_KITL_POWER_CALL,                 0,  KITLIoctl                   },
{ IOCTL_HAL_GET_POWER_DISPOSITION,       0,  OALIoCtlHalGetPowerDisposition },
{ IOCTL_PLATFORM_INFO,					 0,	 OALIoCtlChipType			 },
{ IOCTL_HAL_GET_EBOOT_VERSION,			 0,	 OALIoCtlEbootVersion		 },
{ IOCTL_HAL_GET_KERNEL_VERSION,			 0,	 OALIoCtlKernelVersion		 },
{ IOCTL_HAL_GET_KERNEL_BOOTLINE,		 0,	 OALIoCtlBootLine			 },
// Required Termination
{ 0,                                     0,  NULL                        }
