//------------------------------------------------------------------------------
//
//  Copyright (C) 2007-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  bsp_clocks.h
//
//  This file contains macros for the different clocks available on the SoC.
//
//------------------------------------------------------------------------------
#ifndef __BSP_CLOCKS_H
#define __BSP_CLOCKS_H


//------------------------------------------------------------------------------
// Board clocks
//------------------------------------------------------------------------------

//
// Define the input clocks
//
#define BSP_CLK_OSC_FREQ            (24000000U)     // Internal OSC
#define BSP_CLK_CKIH_FREQ           (24576000U)     // CKIH
#define BSP_CLK_CKIH2_FREQ          (0)             // CKIH2 (NC)
#define BSP_CLK_CKIL_FREQ           (32768U)        // CKIL
#define BSP_CLK_MLBCLK_IN_FREQ      (0)             // mlb_mlbclk_in
#define BSP_CLK_SPDIF_SRCLK_FREQ    (0)             // spdif_srclk
#define BSP_CLK_ESAI_HCKR_FREQ      (0)             // ipp_ind_esai_hckr
#define BSP_CLK_ESAI_HCKT_FREQ      (0)             // ipp_ind_esai_hckt
#define BSP_CLK_DI0_FREQ            (0)             // ipp_di0_clk
#define BSP_CLK_DI1_FREQ            (0)             // ipp_di1_clk
#define BSP_CLK_RTC_OSC             (0)             // ipp_io_ieee_rtc_osc
#define BSP_CLK_FEC_PHY             (0)             // fec_phy_clock  
#define BSP_CLK_ASRC_EXT            (0)             // ipp_asrc_ext


#define BSP_CLK_PLL2_FREQ_EVKA       600000000      // EVK RevA PLL2 = 600 MHz
#define BSP_CLK_PLL2_FREQ           400000000       // Else PLL2 = 400 MHz 

// USB clock divider macros
//  
//      USBOH3_CLK_ROOT = PLL2 / (PREDIV * POSTDIV)
//                      = 400 MHz / (6 * 1) = 66.67 MHz (EVK RevB/ARM2)
//                      = 600 MHz / (3 * 3) = 66.67 MHz (EVK RevA)
//
#define BSP_CLK_USBOH3_PREDIV(srcFreq)  ((srcFreq == BSP_CLK_PLL2_FREQ_EVKA) ? (3-1) : (6-1))
#define BSP_CLK_USBOH3_POSTDIV(srcFreq)  ((srcFreq == BSP_CLK_PLL2_FREQ_EVKA) ? (3-1) : (1-1))

// DPLL = 4 * (OSC) * (MFI + MFN/(MFD+1)) / (PDF+1)
//      = 4 * (24 MHz) * (12 + 1/(1+1)) / (0+1)
//      = 1200 MHz
#define DP_OP_1200MHz                       0x000000C0  // dp_op
#define DP_MFN_1200MHz                      0x00000001  // dp_mfn
#define DP_MFD_1200MHz                      0x00000001  // dp_mfd
#define DP_CTL_1200MHz                      0x00001232  // dp_ctl

// DPLL = 4 * (OSC) * (MFI + MFN/(MFD+1)) / (PDF+1)
//      = 4 * (24 MHz) * (10 + 5/(11+1)) / (0+1)
//      = 1000 MHz
#define DP_OP_1000MHz                       0x000000A0  // dp_op
#define DP_MFN_1000MHz                      0x00000005  // dp_mfn
#define DP_MFD_1000MHz                      0x0000000B  // dp_mfd
#define DP_CTL_1000MHz                      0x00001232  // dp_ctl

// DPLL = 4 * (OSC) * (MFI + MFN/(MFD+1)) / (PDF+1)
//      = 4 * (24 MHz) * (8 + 1/(2+1)) / (0+1)
//      = 800 MHz
#define DP_OP_800MHz                        0x00000080  // dp_op
#define DP_MFN_800MHz                       0x00000001  // dp_mfn
#define DP_MFD_800MHz                       0x00000002  // dp_mfd
#define DP_CTL_800MHz                       0x00001232  // dp_ctl

// DPLL = 2 * (OSC) * (MFI + MFN/(MFD+1)) / (PDF+1)
//      = 2 * (24 MHz) * (8 + 1/(2+1)) / (0+1)
//      = 400 MHz
#define DP_OP_400MHz                        0x00000080  // dp_op
#define DP_MFN_400MHz                       0x00000001  // dp_mfn
#define DP_MFD_400MHz                       0x00000002  // dp_mfd
#define DP_CTL_400MHz                       0x00000232  // dp_ctl

// DPLL = 2 * (OSC) * (MFI + MFN/(MFD+1)) / (PDF+1)
//      = 2 * (24 MHz) * (6 + 45/(47+1)) / (0+1)
//      = 333 MHz
#define DP_OP_333MHz                        0x00000060  // dp_op
#define DP_MFN_333MHz                       0x0000002D  // dp_mfn
#define DP_MFD_333MHz                       0x0000002F  // dp_mfd
#define DP_CTL_333MHz                       0x00000232  // dp_ctl

// DPLL = 2 * (OSC) * (MFI + MFN/(MFD+1)) / (PDF+1)
//      = 2 * (24 MHz) * (6 + 23/(23+1)) / (1+1)
//      = 167 MHz
#define DP_OP_167MHz                        0x00000061  // dp_op
#define DP_MFN_167MHz                       0x00000017  // dp_mfn
#define DP_MFD_167MHz                       0x00000017  // dp_mfd
#define DP_CTL_167MHz                       0x00000232  // dp_ctl

#endif  // __BSP_CLOCKS_H
