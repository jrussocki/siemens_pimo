//------------------------------------------------------------------------------
//
//  Copyright (C) 2008-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  bspdisplay.h
//
//  Provides BSP-specific display driver information
//
//------------------------------------------------------------------------------

#ifndef __BSPDISPLAY_H__
#define __BSPDISPLAY_H__

#include "ipu_common.h"

//------------------------------------------------------------------------------
// Defines

#define DEFAULT_VIDEO_MEM_SIZE                (5*1024*1024) // 5M bytes for default video RAM size

#define MAX_LCD_NUM_MODES                     1 // 1 if only LCD mode supported
                                                // 2+ if TV mode(s) also supported

//------------------------------------------------------------------------------
// DirectDraw Display Driver Support for Screen Rotation
// Support for rotation may be disabled through the 
// BSP_DIRECTDRAW_SUPPORT_ROTATION define.  When this is set to FALSE,
// screen rotation will be prevented.  The purpose of this is to prevent
// GDI CETK test errors that are related to screen rotation.
//------------------------------------------------------------------------------
// Video Memory memory attributes
// The video memory region may be configured as cacheable, write-through (WT)
// or as non-cacheable, bufferable (NCB).  WT mode provides a small performance 
// benefit. So we use WT mode by default.
//------------------------------------------------------------------------------
// Frame Dropping
// Video frames can be dropped discreetly (i.e., without returning a notice to
// the calling application) by setting BSP_DROP_FRAMES_QUIETLY to TRUE.
// Windows Media Player does not respond well to Flip() calls that result in
// a dropped frame.  Setting this variable to TRUE will allow WMP to play
// smoother video.
//------------------------------------------------------------------------------
// IC Path
// IC sub-module should be disabled while the overlay processing flow doesn't require
// resizing and rotation. But after IC sub-module is disabled, the input bufferr will used
// for SDC updating directly which may lengthen flip time. When BSP_DISABLE_NO_IC_PATH
// is enabled, IC sub-module will be always enabled event no resizing and rotation, it can
// make flip time more smoothy, but it consumes addtional memory and bus bandwidth.
//------------------------------------------------------------------------------
// Secondary display device
// When LVDS2 is enabled, LCD will become secondary display device. Customer can get 
// the secondary primary surface by using flag DDSCAPS_PRIMARYSURFACE2. But the window 
// manager was not involved in creating secondary primary surface, application must draw 
// windows, menus, dialog boxes itself.
//------------------------------------------------------------------------------

#define BSP_DIRECTDRAW_SUPPORT_ROTATION TRUE
#define BSP_VID_MEM_CACHE_WRITETHROUGH  FALSE
#define BSP_DROP_FRAMES_QUIETLY         TRUE
#define BSP_DISABLE_NO_IC_PATH          TRUE

#if  (defined BSP_DISPLAY_LVDS1 && defined BSP_DISPLAY_LVDS2) || (defined BSP_DISPLAY_HDMI && defined BSP_DISPLAY_LVDS2)
#define BSP_ENABLE_SECONDARY_PRIMARY_SURFACE TRUE
#else
#define BSP_ENABLE_SECONDARY_PRIMARY_SURFACE FALSE
#endif

#define HDMI_PANEL_CONF_NUM				6
#define LVDS1_PANEL_CONF_NUM			6
#define LVDS2_PANEL_CONF_NUM			6

#define CONFIG_REG_PATH_HDMI			TEXT("Drivers\\Display\\Configuration\\HDMI")
#define CONFIG_REG_PATH_LVDS1			TEXT("Drivers\\Display\\Configuration\\LVDS1")
#define CONFIG_REG_PATH_LVDS2			TEXT("Drivers\\Display\\Configuration\\LVDS2")

#define CONFIG_RESOLUTION				TEXT("Resolution")
#define CONFIG_HEIGHT					TEXT("Height")
#define CONFIG_WIDTH					TEXT("Width")
#define CONFIG_TRANSFER_CYCLE			TEXT("TransferCycle")
#define CONFIG_V_SYNC_WIDTH				TEXT("VSyncWidth")
#define CONFIG_V_START_WIDTH			TEXT("VStartWidth")
#define CONFIG_V_END_WIDTH				TEXT("VEndWidth")
#define CONFIG_H_SYNC_WIDTH				TEXT("HSyncWidth")
#define CONFIG_H_START_WIDTH			TEXT("HStartWidth")
#define CONFIG_H_END_WIDTH				TEXT("HEndWidth")
#define CONFIG_PIXEL_CLK				TEXT("PixelClkCycleFreq")
#define CONFIG_PIXEL_DATA_OFFSET		TEXT("PixelDataOffsetPosition")
#define CONFIG_PIXEL_CLK_UP				TEXT("PixelClkUp")
#define CONFIG_PIXEL_CLK_DOWN			TEXT("PixelClkDown")
#define CONFIG_DATA_MASK_EN				TEXT("DataMaskEnable")
#define CONFIG_CLK_IDLE_EN				TEXT("ClkIdleEnable")
#define CONFIG_CLK_SEL_EN				TEXT("ClkSelectEnable")
#define CONFIG_V_SYNC_POLARITY			TEXT("VSyncPolarity")
#define CONFIG_OUTPUT_ENABLE_POLARITY	TEXT("OuputEnablePolarity")
#define CONFIG_DATA_POL					TEXT("DataPol")
#define CONFIG_CLK_POL					TEXT("ClkPol")
#define CONFIG_H_SYNC_POLARITY			TEXT("HSyncPolarity")
#define CONFIGURE_BUS_WIDTH				TEXT("DataBusWidth")
#define CONFIGURE_PIXEL_FORMAT			TEXT("PixelFormat")

//------------------------------------------------------------------------------
// Types
typedef struct {
	LPCTSTR key;
	LPBYTE value;
} DISPLAY_REG_CONFIG;

typedef enum {
	DISPLAY_VGA = 1	,	// 640*480
	DISPLAY_XGA		,	// 1024*768
	DISPLAY_SXGA	,	// 1280*1024
	DISPLAY_UXGA	,	// 1600*1200
	DISPLAY_HD		,	// 1280*720
	DISPLAY_FHD		,	// 1920*1080
	DISPLAY_WUXGA	,	// 1920*1200
	DISPLAY_CUSTOM		// Custom resolution (you have to set 'Height' and 'Width' keys in registry)
} DISPLAY_RESOLUTION;

//   Enumeration of display modes supported by the platform.
//   add name of a new panel in the following format
//   DISPLAY_PANEL_<Name>
typedef enum {

	DISPLAY_PANEL_HDMI = 0							, // HDMI PANEL
	DISPLAY_PANEL_LVDS1								, // LVDS1 PANEL
	NUMBER_PANEL_DI0_PANELS							,
	DISPLAY_PANEL_LVDS2 = NUMBER_PANEL_DI0_PANELS	, // LVDS2 PANEL
	NUM_SUPPORTED_PANELS

	/** COMMENTED BY TC
#ifdef BSP_DISPLAY_HDMI
 
    DISPLAY_PANEL_HDMI_480P60=0,  
    DISPLAY_PANEL_HDMI_XGA,
    DISPLAY_PANEL_HDMI_SXGA,
    DISPLAY_PANEL_HDMI_UXGA,
    DISPLAY_PANEL_HDMI_720P60,    
    DISPLAY_PANEL_HDMI_1080P60,  
    DISPLAY_PANEL_HDMI_WUXGA,
    NUMBER_PANEL_DI0_PANELS,
#endif 

#ifdef BSP_DISPLAY_DVI
    DISPLAY_PANEL_DVI_SVGA = 0,
    DISPLAY_PANEL_DVI_XGA,
    DISPLAY_PANEL_DVI_SXGA,
    DISPLAY_PANEL_DVI_UXGA,
    DISPLAY_PANEL_DVI_1080P,
    NUMBER_PANEL_DI0_PANELS,
#endif

#ifdef BSP_DISPLAY_LVDS1
    DISPLAY_PANEL_LVDS1 = 0,
    NUMBER_PANEL_DI0_PANELS,
#endif 

#ifdef BSP_DISPLAY_WVGA
DISPLAY_PANEL_CHUNGHWA_WVGA = 0,  // Registry value is X
    NUMBER_PANEL_DI0_PANELS,
#endif

#ifdef BSP_DISPLAY_NO_DI0
    NUMBER_PANEL_DI0_PANELS = 0,  
#endif



//The following panel type should be for DI1.

#ifdef BSP_DISPLAY_LVDS2
    DISPLAY_PANEL_LVDS2 = NUMBER_PANEL_DI0_PANELS,
    NUM_SUPPORTED_PANELS,       
#endif
#ifdef BSP_DISPLAY_NO_DI1
    NUM_SUPPORTED_PANELS = NUMBER_PANEL_DI0_PANELS,   
#endif

	*/
} DISPLAY_PANEL_ID;

//   Enumeration of Display connection types.
//   add name of a new port type in the following format
//   DISPLAY_PORT_<Name>
typedef enum {
    DISPLAY_PORT_INVALID,
    DISPLAY_PORT_VGA,
    DISPLAY_PORT_LVDS1,
    DISPLAY_PORT_LVDS2,
    DISPLAY_PORT_DLVDS,
    DISPLAY_PORT_TV,
    DISPLAY_PORT_CHUNGHWA,
    DISPLAY_PORT_DVI,
    DISPLAY_PORT_HDMI,
    // New port type goes here ,
    // New port type goes here ,
    numPort,
} DISPLAY_PORT_TYPE;

//------------------------------------------------------------------------------
// Functions

#endif   // __BSPDISPLAY_H__
