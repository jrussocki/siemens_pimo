//------------------------------------------------------------------------------
//
//  Copyright (C) 2007-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  dvfs.h
//
//  This file contains macros for the supporting DVFS (Dynamic Voltage and
//  Frequency scaling).
//
//------------------------------------------------------------------------------
#ifndef __DVFS_H
#define __DVFS_H

//------------------------------------------------------------------------------
// LOAD TRACKING CONFIGURATION
//------------------------------------------------------------------------------
#define BSP_DVFS_LOADTRACK_MSEC             50  // 50 msec tracking window
#define BSP_DVFS_LOADTRACK_UP_PCT           25  // <25% idle -> raise setpoint
#define BSP_DVFS_LOADTRACK_UP_MSEC          2*BSP_DVFS_LOADTRACK_MSEC
#define BSP_DVFS_LOADTRACK_DN_PCT           85  // >85% idle -> lower setpoint
#define BSP_DVFS_LOADTRACK_DN_MSEC          5*BSP_DVFS_LOADTRACK_MSEC


//------------------------------------------------------------------------------
// DVFS SETPOINT CONFIGURATION
//------------------------------------------------------------------------------

// BSP_DVFS_PLL2_DIV_PODF defines the divider for PLL2 that will be the 
// step_clk used to source the ARM under the following conditions:
// - temporary ARM clock during PLL1 relock   IMPORTANT:  resulting clock rate
//   must not exceed maximum frequency for low-voltage setpoint, 167 MHz)
// - ARM clock after PLL1 migration documented below
#define BSP_DVFS_PLL2_DIV_PODF              2           // div by 3 (400/3 = 133 MHz)

// Define BSP_DVFS_PER_USE_RPM_VCC to select Reduce Performance Mode (RPM) 
// voltage for the peripheral domain.  Note:  RPM voltage setting cannot be 
// used on current silicon per latest data sheet.
// #define BSP_DVFS_PER_USE_RPM_VCC


// ----- CPU DOMAIN ----- //

// HIGH3 SETPOINT
#define BSP_DVFS_CPU_HIGH3_DP_OP            DP_OP_1200MHz   // dp_op
#define BSP_DVFS_CPU_HIGH3_DP_MFN           DP_MFN_1200MHz  // dp_mfn
#define BSP_DVFS_CPU_HIGH3_DP_MFD           DP_MFD_1200MHz  // dp_mfd
#define BSP_DVFS_CPU_HIGH3_DP_CTL           DP_CTL_1200MHz  // dp_ctl
#define BSP_DVFS_CPU_HIGH3_PLL_FREQ         1200000000      // freq
#define BSP_DVFS_CPU_HIGH3_ARM_PODF         0               // div by 1
#define BSP_DVFS_CPU_HIGH3_mV               1300
#define BSP_DVFS_CPU_HIGH3_ARM_FREQ  \
        (BSP_DVFS_CPU_HIGH3_PLL_FREQ / (BSP_DVFS_CPU_HIGH3_ARM_PODF+1))

// HIGH2 SETPOINT
#define BSP_DVFS_CPU_HIGH2_DP_OP            DP_OP_1000MHz   // dp_op
#define BSP_DVFS_CPU_HIGH2_DP_MFN           DP_MFN_1000MHz  // dp_mfn
#define BSP_DVFS_CPU_HIGH2_DP_MFD           DP_MFD_1000MHz  // dp_mfd
#define BSP_DVFS_CPU_HIGH2_DP_CTL           DP_CTL_1000MHz  // dp_ctl
#define BSP_DVFS_CPU_HIGH2_PLL_FREQ         1000000000      // freq
#define BSP_DVFS_CPU_HIGH2_ARM_PODF         0               // div by 1
#define BSP_DVFS_CPU_HIGH2_mV               1250
#define BSP_DVFS_CPU_HIGH2_ARM_FREQ  \
        (BSP_DVFS_CPU_HIGH2_PLL_FREQ / (BSP_DVFS_CPU_HIGH2_ARM_PODF+1))

// HIGH SETPOINT
#define BSP_DVFS_CPU_HIGH_DP_OP             DP_OP_800MHz    // dp_op
#define BSP_DVFS_CPU_HIGH_DP_MFN            DP_MFN_800MHz   // dp_mfn
#define BSP_DVFS_CPU_HIGH_DP_MFD            DP_MFD_800MHz   // dp_mfd
#define BSP_DVFS_CPU_HIGH_DP_CTL            DP_CTL_800MHz   // dp_ctl
#define BSP_DVFS_CPU_HIGH_PLL_FREQ          800000000       // freq
#define BSP_DVFS_CPU_HIGH_ARM_PODF          0               // div by 1
#define BSP_DVFS_CPU_HIGH_mV                1100
#define BSP_DVFS_CPU_HIGH_ARM_FREQ  \
        (BSP_DVFS_CPU_HIGH_PLL_FREQ / (BSP_DVFS_CPU_HIGH_ARM_PODF+1))

// MEDIUM SETPOINT
#define BSP_DVFS_CPU_MED_DP_OP              DP_OP_400MHz    // dp_op
#define BSP_DVFS_CPU_MED_DP_MFN             DP_MFN_400MHz   // dp_mfn
#define BSP_DVFS_CPU_MED_DP_MFD             DP_MFD_400MHz   // dp_mfd
#define BSP_DVFS_CPU_MED_DP_CTL             DP_CTL_400MHz   // dp_ctl
#define BSP_DVFS_CPU_MED_PLL_FREQ           400000000       // freq
#define BSP_DVFS_CPU_MED_ARM_PODF           0               // div by 1
#define BSP_DVFS_CPU_MED_mV                 950
#define BSP_DVFS_CPU_MED_ARM_FREQ  \
        (BSP_DVFS_CPU_MED_PLL_FREQ / (BSP_DVFS_CPU_MED_ARM_PODF+1))

// LOW SETPOINT
#define BSP_DVFS_CPU_LOW_DP_OP              DP_OP_333MHz    // dp_op
#define BSP_DVFS_CPU_LOW_DP_MFN             DP_MFN_333MHz   // dp_mfn
#define BSP_DVFS_CPU_LOW_DP_MFD             DP_MFD_333MHz   // dp_mfd
#define BSP_DVFS_CPU_LOW_DP_CTL             DP_CTL_333MHz   // dp_ctl
#define BSP_DVFS_CPU_LOW_PLL_FREQ           333000000       // freq
#define BSP_DVFS_CPU_LOW_ARM_PODF           1               // div by 2
#define BSP_DVFS_CPU_LOW_mV                 950
#define BSP_DVFS_CPU_LOW_ARM_FREQ  \
        (BSP_DVFS_CPU_LOW_PLL_FREQ / (BSP_DVFS_CPU_LOW_ARM_PODF+1))


// ----- PERIPHRAL DOMAIN ----- //
// HIGH2 SETPOINT
#define BSP_DVFS_PER_HIGH2_PER_CLK_SEL      CCM_CBCDR_PERIPH_CLK_SEL_PLL2
#define BSP_DVFS_PER_HIGH2_PLL_FREQ         BSP_CLK_PLL2_FREQ
#define BSP_DVFS_PER_HIGH2_mV               BSP_PMIC_VCC_NORMAL_VOLT
#define BSP_DVFS_PER_HIGH2_DVFS_PODF        0           // div by 1 (400 MHz)
#define BSP_DVFS_PER_HIGH2_AHB_PODF         2           // div by 3 (133 MHz)
#define BSP_DVFS_PER_HIGH2_NFC_PODF         3           // div by 4 (33 MHz)
#define BSP_DVFS_PER_HIGH2_AXI_A_PODF       0           // div by 1 (400 MHz)
#define BSP_DVFS_PER_HIGH2_AXI_B_PODF       1           // div by 2 (200 MHz)
#define BSP_DVFS_PER_HIGH2_EMI_SLOW_PODF    0           // div by 1 (133 MHz)
#define BSP_DVFS_PER_HIGH2_BUS_DIV  \
        (CSP_BITFVAL(CCM_CBCDR_AHB_PODF, BSP_DVFS_PER_HIGH2_AHB_PODF) |  \
         CSP_BITFVAL(CCM_CBCDR_NFC_PODF, BSP_DVFS_PER_HIGH2_NFC_PODF) |  \
         CSP_BITFVAL(CCM_CBCDR_AXI_B_PODF, BSP_DVFS_PER_HIGH2_AXI_B_PODF) | \
         CSP_BITFVAL(CCM_CBCDR_EMI_SLOW_PODF, BSP_DVFS_PER_HIGH2_EMI_SLOW_PODF))
#define BSP_DVFS_PER_HIGH2_BUS_MASK  \
        (CSP_BITFMASK(CCM_CBCDR_AHB_PODF) |  \
         CSP_BITFMASK(CCM_CBCDR_NFC_PODF) |  \
         CSP_BITFMASK(CCM_CBCDR_AXI_B_PODF) |  \
         CSP_BITFMASK(CCM_CBCDR_EMI_SLOW_PODF))
#define BSP_DVFS_PER_HIGH2_DVFS_FREQ \
        (BSP_DVFS_PER_HIGH2_PLL_FREQ / (BSP_DVFS_PER_HIGH2_DVFS_PODF+1))
#define BSP_DVFS_PER_HIGH2_AHB_FREQ  \
        (BSP_DVFS_PER_HIGH2_DVFS_FREQ / (BSP_DVFS_PER_HIGH2_AHB_PODF+1))
#define BSP_DVFS_PER_HIGH2_AXI_A_FREQ  \
        (BSP_DVFS_PER_HIGH2_DVFS_FREQ / (BSP_DVFS_PER_HIGH2_AXI_A_PODF+1))
#define BSP_DVFS_PER_HIGH2_AXI_B_FREQ  \
            (BSP_DVFS_PER_HIGH2_DVFS_FREQ / (BSP_DVFS_PER_HIGH2_AXI_B_PODF+1))
#define BSP_DVFS_PER_HIGH2_EMI_SLOW_FREQ  \
        (BSP_DVFS_PER_HIGH2_AHB_FREQ / (BSP_DVFS_PER_HIGH2_EMI_SLOW_PODF+1))
#define BSP_DVFS_PER_HIGH2_NFC_FREQ  \
        (BSP_DVFS_PER_HIGH2_EMI_SLOW_FREQ / (BSP_DVFS_PER_HIGH2_NFC_PODF+1))
#define BSP_DVFS_PER_HIGH2_PERBUS_FREQ      BSP_DVFS_PER_HIGH2_AHB_FREQ
#define BSP_DVFS_PER_HIGH2_MEMBUS_FREQ      BSP_DVFS_PER_HIGH2_AXI_A_FREQ

// HIGH SETPOINT
#define BSP_DVFS_PER_HIGH_PER_CLK_SEL       CCM_CBCDR_PERIPH_CLK_SEL_PLL2
#define BSP_DVFS_PER_HIGH_PLL_FREQ          BSP_CLK_PLL2_FREQ
#define BSP_DVFS_PER_HIGH_mV                BSP_PMIC_VCC_NORMAL_VOLT
#define BSP_DVFS_PER_HIGH_DVFS_PODF         0           // div by 1 (400 MHz)
#define BSP_DVFS_PER_HIGH_AHB_PODF          5           // div by 6 (66 MHz)
#define BSP_DVFS_PER_HIGH_NFC_PODF          1           // div by 2 (33 MHz)
#define BSP_DVFS_PER_HIGH_AXI_A_PODF        0           // div by 1 (400 MHz)
#define BSP_DVFS_PER_HIGH_AXI_B_PODF        1           // div by 2 (200 MHz)
#define BSP_DVFS_PER_HIGH_EMI_SLOW_PODF     0           // div by 1 (66 MHz)
#define BSP_DVFS_PER_HIGH_BUS_DIV  \
        (CSP_BITFVAL(CCM_CBCDR_AHB_PODF, BSP_DVFS_PER_HIGH_AHB_PODF) |  \
         CSP_BITFVAL(CCM_CBCDR_NFC_PODF, BSP_DVFS_PER_HIGH_NFC_PODF) |  \
         CSP_BITFVAL(CCM_CBCDR_AXI_B_PODF, BSP_DVFS_PER_HIGH_AXI_B_PODF) | \
         CSP_BITFVAL(CCM_CBCDR_EMI_SLOW_PODF, BSP_DVFS_PER_HIGH_EMI_SLOW_PODF))
#define BSP_DVFS_PER_HIGH_BUS_MASK  \
        (CSP_BITFMASK(CCM_CBCDR_AHB_PODF) |  \
         CSP_BITFMASK(CCM_CBCDR_NFC_PODF) |  \
         CSP_BITFMASK(CCM_CBCDR_AXI_B_PODF) |  \
         CSP_BITFMASK(CCM_CBCDR_EMI_SLOW_PODF))
#define BSP_DVFS_PER_HIGH_DVFS_FREQ \
        (BSP_DVFS_PER_HIGH_PLL_FREQ / (BSP_DVFS_PER_HIGH_DVFS_PODF+1))
#define BSP_DVFS_PER_HIGH_AHB_FREQ  \
        (BSP_DVFS_PER_HIGH_DVFS_FREQ / (BSP_DVFS_PER_HIGH_AHB_PODF+1))
#define BSP_DVFS_PER_HIGH_AXI_A_FREQ  \
        (BSP_DVFS_PER_HIGH_DVFS_FREQ / (BSP_DVFS_PER_HIGH_AXI_A_PODF+1))
#define BSP_DVFS_PER_HIGH_AXI_B_FREQ  \
            (BSP_DVFS_PER_HIGH_DVFS_FREQ / (BSP_DVFS_PER_HIGH_AXI_B_PODF+1))
#define BSP_DVFS_PER_HIGH_EMI_SLOW_FREQ  \
        (BSP_DVFS_PER_HIGH_AHB_FREQ / (BSP_DVFS_PER_HIGH_EMI_SLOW_PODF+1))
#define BSP_DVFS_PER_HIGH_NFC_FREQ  \
        (BSP_DVFS_PER_HIGH_EMI_SLOW_FREQ / (BSP_DVFS_PER_HIGH_NFC_PODF+1))
#define BSP_DVFS_PER_HIGH_PERBUS_FREQ       BSP_DVFS_PER_HIGH_AHB_FREQ
#define BSP_DVFS_PER_HIGH_MEMBUS_FREQ       BSP_DVFS_PER_HIGH_AXI_A_FREQ
        
// MEDIUM SETPOINT
#define BSP_DVFS_PER_MED_PER_CLK_SEL        CCM_CBCDR_PERIPH_CLK_SEL_PERIPH_APM
#define BSP_DVFS_PER_MED_PLL_FREQ           BSP_DVFS_CPU_LOW_PLL_FREQ // 333 MHz
#ifdef BSP_DVFS_PER_USE_RPM_VCC
#define BSP_DVFS_PER_MED_mV                 1225
#else
#define BSP_DVFS_PER_MED_mV                 BSP_PMIC_VCC_NORMAL_VOLT
#endif
#define BSP_DVFS_PER_MED_DVFS_PODF          0           // div by 1 (333 MHz)
#define BSP_DVFS_PER_MED_AHB_PODF           4           // div by 5 (66 MHz)
#define BSP_DVFS_PER_MED_NFC_PODF           1           // div by 2 (33 MHz)
#define BSP_DVFS_PER_MED_AXI_A_PODF         0           // div by 1 (333 MHz)
#define BSP_DVFS_PER_MED_AXI_B_PODF         1           // div by 2 (166 MHz)
#define BSP_DVFS_PER_MED_EMI_SLOW_PODF      0           // div by 1 (66 MHz)
#define BSP_DVFS_PER_MED_BUS_DIV  \
        (CSP_BITFVAL(CCM_CBCDR_AHB_PODF, BSP_DVFS_PER_MED_AHB_PODF) |  \
         CSP_BITFVAL(CCM_CBCDR_NFC_PODF, BSP_DVFS_PER_MED_NFC_PODF) |  \
         CSP_BITFVAL(CCM_CBCDR_AXI_B_PODF, BSP_DVFS_PER_MED_AXI_B_PODF) | \
         CSP_BITFVAL(CCM_CBCDR_EMI_SLOW_PODF, BSP_DVFS_PER_MED_EMI_SLOW_PODF))
#define BSP_DVFS_PER_MED_BUS_MASK  \
        (CSP_BITFMASK(CCM_CBCDR_AHB_PODF) |  \
         CSP_BITFMASK(CCM_CBCDR_NFC_PODF) |  \
         CSP_BITFMASK(CCM_CBCDR_AXI_B_PODF) |  \
         CSP_BITFMASK(CCM_CBCDR_EMI_SLOW_PODF))
#define BSP_DVFS_PER_MED_DVFS_FREQ \
        (BSP_DVFS_PER_MED_PLL_FREQ / (BSP_DVFS_PER_MED_DVFS_PODF+1))
#define BSP_DVFS_PER_MED_AHB_FREQ  \
        (BSP_DVFS_PER_MED_DVFS_FREQ / (BSP_DVFS_PER_MED_AHB_PODF+1))
#define BSP_DVFS_PER_MED_AXI_A_FREQ  \
        (BSP_DVFS_PER_MED_DVFS_FREQ / (BSP_DVFS_PER_MED_AXI_A_PODF+1))
#define BSP_DVFS_PER_MED_AXI_B_FREQ  \
            (BSP_DVFS_PER_MED_DVFS_FREQ / (BSP_DVFS_PER_MED_AXI_B_PODF+1))
#define BSP_DVFS_PER_MED_EMI_SLOW_FREQ  \
        (BSP_DVFS_PER_MED_AHB_FREQ / (BSP_DVFS_PER_MED_EMI_SLOW_PODF+1))
#define BSP_DVFS_PER_MED_NFC_FREQ  \
        (BSP_DVFS_PER_MED_EMI_SLOW_FREQ / (BSP_DVFS_PER_MED_NFC_PODF+1))
#define BSP_DVFS_PER_MED_PERBUS_FREQ        BSP_DVFS_PER_MED_AHB_FREQ
#define BSP_DVFS_PER_MED_MEMBUS_FREQ        BSP_DVFS_PER_MED_AXI_A_FREQ

        
// LOW SETPOINT
#define BSP_DVFS_PER_LOW_PER_CLK_SEL        CCM_CBCDR_PERIPH_CLK_SEL_PERIPH_APM
#define BSP_DVFS_PER_LOW_PLL_FREQ           BSP_DVFS_CPU_LOW_PLL_FREQ  // 333 MHz
#ifdef BSP_DVFS_PER_USE_RPM_VCC
#define BSP_DVFS_PER_LOW_mV                 1225
#else
#define BSP_DVFS_PER_LOW_mV                 BSP_PMIC_VCC_NORMAL_VOLT
#endif
#define BSP_DVFS_PER_LOW_DVFS_PODF          0           // div by 1 (333 MHz)
#define BSP_DVFS_PER_LOW_AHB_PODF           7           // div by 8 (42 MHz)
#define BSP_DVFS_PER_LOW_NFC_PODF           1           // div by 2 (21 MHz)
#define BSP_DVFS_PER_LOW_AXI_A_PODF         0           // div by 1 (333 MHz)
#define BSP_DVFS_PER_LOW_AXI_B_PODF         7           // div by 8 (42 MHz)
#define BSP_DVFS_PER_LOW_EMI_SLOW_PODF      0           // div by 1 (42 MHz)
#define BSP_DVFS_PER_LOW_BUS_DIV  \
        (CSP_BITFVAL(CCM_CBCDR_AHB_PODF, BSP_DVFS_PER_LOW_AHB_PODF) |  \
         CSP_BITFVAL(CCM_CBCDR_NFC_PODF, BSP_DVFS_PER_LOW_NFC_PODF) |  \
         CSP_BITFVAL(CCM_CBCDR_AXI_B_PODF, BSP_DVFS_PER_LOW_AXI_B_PODF) | \
         CSP_BITFVAL(CCM_CBCDR_EMI_SLOW_PODF, BSP_DVFS_PER_LOW_EMI_SLOW_PODF))
#define BSP_DVFS_PER_LOW_BUS_MASK  \
        (CSP_BITFMASK(CCM_CBCDR_AHB_PODF) |  \
         CSP_BITFMASK(CCM_CBCDR_NFC_PODF) |  \
         CSP_BITFMASK(CCM_CBCDR_AXI_B_PODF) |  \
         CSP_BITFMASK(CCM_CBCDR_EMI_SLOW_PODF))
#define BSP_DVFS_PER_LOW_DVFS_FREQ \
        (BSP_DVFS_PER_LOW_PLL_FREQ / (BSP_DVFS_PER_LOW_DVFS_PODF+1))
#define BSP_DVFS_PER_LOW_AHB_FREQ  \
        (BSP_DVFS_PER_LOW_DVFS_FREQ / (BSP_DVFS_PER_LOW_AHB_PODF+1))
#define BSP_DVFS_PER_LOW_AXI_A_FREQ  \
        (BSP_DVFS_PER_LOW_DVFS_FREQ / (BSP_DVFS_PER_LOW_AXI_A_PODF+1))
#define BSP_DVFS_PER_LOW_AXI_B_FREQ  \
            (BSP_DVFS_PER_LOW_DVFS_FREQ / (BSP_DVFS_PER_LOW_AXI_B_PODF+1))
#define BSP_DVFS_PER_LOW_EMI_SLOW_FREQ  \
        (BSP_DVFS_PER_LOW_AHB_FREQ / (BSP_DVFS_PER_LOW_EMI_SLOW_PODF+1))
#define BSP_DVFS_PER_LOW_NFC_FREQ  \
        (BSP_DVFS_PER_LOW_EMI_SLOW_FREQ / (BSP_DVFS_PER_LOW_NFC_PODF+1))
#define BSP_DVFS_PER_LOW_PERBUS_FREQ        BSP_DVFS_PER_LOW_AHB_FREQ
#define BSP_DVFS_PER_LOW_MEMBUS_FREQ        BSP_DVFS_PER_LOW_AXI_A_FREQ


// Define setpoints to determine when RPM can be used.
// BSP_DVFS_CPU_HIGHEST_RPM_SETPOINT defines the highest CPU setpoint
// that allows the system to enter RPM.  PLL1 cannot exceed 333 MHz.
// BSP_DVFS_PER_LOWEST_HPM_SETPOINT defines the lowest HPM setpoint
// that must be forced when the CPU setpoint cannot support RPM.
#define BSP_DVFS_CPU_HIGHEST_RPM_SETPOINT   DDK_DVFC_SETPOINT_LOW
#define BSP_DVFS_PER_LOWEST_HPM_SETPOINT    DDK_DVFC_SETPOINT_HIGH

// ----- PERIPHERAL-SPECIFIC FREQUENCY REQUIREMENTS ----- //
#define BSP_DVFS_IPU_MIN_AXI_FREQ           200000000       // 200 MHz
#define BSP_DVFS_VPU_MIN_AXI_FREQ           200000000       // 200 MHz
#define BSP_DVFS_GPU_MIN_AXI_FREQ           200000000       // 200 MHz
#define BSP_DVFS_GPU2D_MIN_AXI_FREQ         200000000       // 200 MHz
#define BSP_DVFS_TVE_MIN_AXI_FREQ           200000000       // 200 MHz

#define BSP_DVFS_SATA_MIN_AHB_FREQ          133000000       // 133 MHz
#define BSP_DVFS_USB_MIN_AHB_FREQ           133000000       // 133 MHz
// PATA needs 50 MHz IPG to support UDMA mode 4.  AHB is always 2x IPG, so set AHB
// requirement for 100 MHz.
#define BSP_DVFS_PATA_MIN_AHB_FREQ          100000000       // 100 MHz
// FEC needs 50 MHz IPG to support full-duplex operation.  AHB is always 2x IPG, so set AHB
// requirement for 100 MHz.
#define BSP_DVFS_FEC_MIN_AHB_FREQ           100000000       // 100 MHz

// Define a reserved hardware IRQ that will be used to implement
// signaling between the kernel load tracking and the DVFC driver.
#define BSP_DVFS_IRQ    IRQ_RESERVED107

#endif  // __DVFS_H
