//------------------------------------------------------------------------------
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//-----------------------------------------------------------------------------
//
//  Copyright (C) 2007-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//-----------------------------------------------------------------------------
//
//  File:  bsp_cfg.h
//
//  This file contains system constants specific to the MX6Q EVK board.
//
//------------------------------------------------------------------------------
#ifndef __BSP_CFG_H
#define __BSP_CFG_H

//------------------------------------------------------------------------------
//
//  Define:  BSP_DEVICE_PREFIX
//
//  Prefix used to generate device name for bootload/KITL
//
#define BSP_DEVICE_PREFIX       "MX6"                  // Device name prefix

//------------------------------------------------------------------------------
// BOARD ID definition
//------------------------------------------------------------------------------

// Define different board ID. Driver can use these values to check which 
// board the driver is running on to get different behavior. 
// For reading CPU board ID, BSP_CPU_BID() should be used to 
// get the correct CPU board ID.
//
#define BSP_MX6Q_SABREAuto_REV1_BID     0xffff

#define BSP_CPU_BID(value)              (value&0xffff)

//------------------------------------------------------------------------------
// CPU Configuration Settings
//------------------------------------------------------------------------------

// Define BSP_OAL_NEON if you want to enable save/restore of VFP/NEON.
// By default, the kernel supports on-demand save/restore of VFPv2 register set,
// but the native support and context save area is not sufficient for 
// VFPv3/NEON.  Setting BSP_OAL_NEON will cause the VFPv3/NEON registers to be 
// saved on each thread context switch using the kernel coprocessor support.  
// Comment out this definition if you don't intend on supporting VFP/NEON 
// application code and want to save the overhead and power associated with 
// supporting the NEON hardware block.  NOTE: if BSP_OAL_NEON is not
// defined, CP10/CP11 access will be disabled and will result in an undefined
// instruction exception for all VFPv2/VFPv3/NEON instructions.
//
#define BSP_OAL_NEON

// Define BSP_OAL_HIZ_DDR_IO_DURING_IDLE to place the DDR I/O pads in
// HiZ during idle.  Note that the DDR I/O pads will not be updated unless the
// system has detected the DDR can be placed into DDR self-refresh during
// idle (i.e. the ARM core is the only active master using DDR).  Lower DDR
// I/O power can be achieved by placing the pads in HiZ.  The penalty will be a
// few extra cycles to save, update, and restore the DDR I/O pad settings.
// #define BSP_OAL_HIZ_DDR_IO_DURING_IDLE

//------------------------------------------------------------------------------
// Clock Configuration Settings
//------------------------------------------------------------------------------

// Set BSP_MAX_CORE_CLK_FREQ to the maximum supported CPU clock frequency.
// The frequency specified must match the speed grade of the SoC.
// Supported settings are:
//       800000000  = 800 MHz   (Automotive SoC limited to 800 MHz)
#define BSP_MAX_CORE_CLK_FREQ   800000000           // Max CPU clock in Hz
#define RESCHED_PERIOD          1                   // Reschedule ms
#ifdef BSP_OAL_TIMER32K
#define BSP_SYSTIMER_PRESCALAR  (1-1)               // System timer prescalar
#define BSP_SYSTIMER_CLKSRC     EPIT_CR_CLKSRC_CKIL // CKIL system timer source
#else
#define BSP_SYSTIMER_PRESCALAR  (1-1)               // System timer prescalar
#define BSP_SYSTIMER_CLKSRC     EPIT_CR_CLKSRC_HIGHFREQ // PERCLK
#endif

//------------------------------------------------------------------------------
// SDHC configurations
//------------------------------------------------------------------------------
#define BSP_CLK_GATING_BETWEEN_CMDS_SDHC1   TRUE
#define BSP_CLK_GATING_BETWEEN_CMDS_SDHC2   TRUE
#define BSP_CLK_GATING_BETWEEN_CMDS_SDHC3   TRUE
#define BSP_CLK_GATING_BETWEEN_CMDS_SDHC4   TRUE
#define BSP_SDHC_SDMA_MIN_TRANSFER          256
#define BSP_SDHC_MAX_SDBUS_CLK_SUPPORTED    25000000U
#define BSP_SDHC_POLL_DATA_LENGTH           0x7FFFFFFF // should be always > 64

//------------------------------------------------------------------------------
// SDMA Configuration
//------------------------------------------------------------------------------
#define BSP_SDMA_MC0PTR         IMAGE_WINCE_DDKSDMA_IRAM_PA_START

#define BSP_SDMA_CHNPRI_AUDIO   (SDMA_CHNPRI_CHNPRI_HIGHEST)
#define BSP_SDMA_CHNPRI_ATA     (SDMA_CHNPRI_CHNPRI_HIGHEST-5)
#define BSP_SDMA_CHNPRI_FIRI    (SDMA_CHNPRI_CHNPRI_HIGHEST-1)
#define BSP_SDMA_CHNPRI_CSPI    (SDMA_CHNPRI_CHNPRI_HIGHEST-2)
#define BSP_SDMA_CHNPRI_SDHC1   (SDMA_CHNPRI_CHNPRI_HIGHEST-3)
#define BSP_SDMA_CHNPRI_SDHC2   (SDMA_CHNPRI_CHNPRI_HIGHEST-3)
#define BSP_SDMA_CHNPRI_SERIAL  (SDMA_CHNPRI_CHNPRI_HIGHEST-4)

#define BSP_SDMA_SUPPORT_ATA    TRUE
#define BSP_SDMA_SUPPORT_SSI1   TRUE
#define BSP_SDMA_SUPPORT_SSI2   TRUE
#define BSP_SDMA_SUPPORT_SSI3   TRUE
#define BSP_SDMA_SUPPORT_CSPI1  TRUE
#define BSP_SDMA_SUPPORT_CSPI2  TRUE
#define BSP_SDMA_SUPPORT_CSPI3  TRUE
#define BSP_SDMA_SUPPORT_UART1  TRUE
#define BSP_SDMA_SUPPORT_UART2  TRUE
#define BSP_SDMA_SUPPORT_UART3  TRUE
#define BSP_SDMA_SUPPORT_UART4  TRUE
#define BSP_SDMA_SUPPORT_UART5  TRUE
#define BSP_SDMA_SUPPORT_FIRI   TRUE
#define BSP_SDMA_SUPPORT_NANDFC FALSE

//------------------------------------------------------------------------------
// Audio Configuration
//------------------------------------------------------------------------------

// Set BSP_AUDIO_DMA_BUF_ADDR to static DMA buffer physical address.
#define BSP_AUDIO_DMA_BUF_ADDR  IMAGE_WINCE_AUDIO_IRAM_PA_START
#define BSP_AUDIO_DMA_BUF_SIZE  IMAGE_WINCE_AUDIO_IRAM_SIZE

// Power supply settings
#define BSP_AUDIO_POWER_GPIO_PORT	DDK_GPIO_PORT4
#define BSP_AUDIO_POWER_GPIO_PIN	10

// Headphone detect settings
#define BSP_AUDIO_HPD_GPIO_PORT DDK_GPIO_PORT7
#define BSP_AUDIO_HPD_GPIO_PIN  8
#define BSP_AUDIO_HPD_IRQ       IRQ_GPIO7_PIN8


//------------------------------------------------------------------------------
// Ethernet Board Configuration
//------------------------------------------------------------------------------
// Ethernet reset is connected to GPIO7_6 muxed on PATA_DA_0 at ATL1
#define BSP_ETH_RST_IOMUX_PIN   DDK_IOMUX_PAD_PATA_DA_0
#define BSP_ETH_RST_IOMUX_ALT   DDK_IOMUX_PIN_MUXMODE_ALT1
#define BSP_ETH_RST_GPIO_BASE   CSP_BASE_REG_PA_GPIO7
#define BSP_ETH_RST_GPIO_PORT   DDK_GPIO_PORT7
#define BSP_ETH_RST_GPIO_PIN    6


//------------------------------------------------------------------------------
// Nled Configuration
//------------------------------------------------------------------------------
// Nled reset is connected to GPIO2_6
#define BSP_NLED_GPIO_BASE   CSP_BASE_REG_PA_GPIO4
#define BSP_NLED_GPIO_PORT   DDK_GPIO_PORT4
#define BSP_NLED_GPIO_PIN    4

//------------------------------------------------------------------------------
// PMIC Board Configuration
//------------------------------------------------------------------------------
#define BSP_PMIC_IRQ            IRQ_GPIO5_PIN7

#define BSP_PMIC_CSPI           FALSE
#define BSP_PMIC_I2C            TRUE

#define BSP_PMIC_CSPI_SS        0           // SS0
#define BSP_PMIC_CSPI_FREQ      20000000    // 20 MHz 
#define BSP_PMIC_CSPI_PORT      2
#define BSP_PMIC_CSPI_BASE      CSP_BASE_REG_PA_ECSPI1
#define BSP_PMIC_IOMUX_PIN      DDK_IOMUX_PIN_DISP0_DAT13
#define BSP_PMIC_IOMUX_PAD      DDK_IOMUX_PAD_DISP0_DAT13
#define BSP_PMIC_GPIO_PORT      DDK_GPIO_PORT5
#define BSP_PMIC_GPIO_PIN       7

#define BSP_PMIC_I2C_FREQ       100000
#define BSP_PMIC_I2C_PORT       2
#define BSP_PMIC_I2C_DEVICE     L"I2C2:"
#define BSP_PMIC_I2C_ADDR       0x8

#define BSP_BACKLIGHT_DISPLAY   BACKLIGHT_ISINK_A
#define BSP_BACKLIGHT_DISPLAY_REGULATOR BACKLIGHT_REGL_DCDC5

#define BSP_PMIC_USE_PMIC_WATCHDOG  FALSE

// Macro to convert voltage specified in mV to PMIC voltage code
#define BSP_PMIC_VOLT2CODE_VDDGP(mV)    ((((((mV)*9)/25)-203)/7)+1)
#define BSP_PMIC_VOLT2CODE_VCC(mV)      ((((((mV)*382)/(371*25))-29))+1)
#define BSP_PMIC_VOLT2CODE(mV)          (((mV) - 600) / 25)

// Standby (stop mode) voltages 
#define BSP_PMIC_VDDGP_STANDBY_VOLT     850    // 1050mV
#define BSP_PMIC_VDDGP_STANDBY_CODE     BSP_PMIC_VOLT2CODE_VDDGP(BSP_PMIC_VDDGP_STANDBY_VOLT)
#define BSP_PMIC_VCC_STANDBY_VOLT       950     // 950mV
#define BSP_PMIC_VCC_STANDBY_CODE       BSP_PMIC_VOLT2CODE_VCC(BSP_PMIC_VCC_STANDBY_VOLT)
#define BSP_PMIC_VDDA_STANDBY_VOLT      950     // 950mV
#define BSP_PMIC_VDDA_STANDBY_CODE      BSP_PMIC_VOLT2CODE(BSP_PMIC_VDDA_STANDBY_VOLT)

// Normal (high-speed run mode) voltages
#define BSP_PMIC_VDDGP_NORMAL_VOLT      1100    // 1050mV
#define BSP_PMIC_VDDGP_NORMAL_CODE      BSP_PMIC_VOLT2CODE_VDDGP(BSP_PMIC_VDDGP_NORMAL_VOLT)
#define BSP_PMIC_VCC_NORMAL_VOLT        1300    // 1300mV
#define BSP_PMIC_VCC_NORMAL_CODE        BSP_PMIC_VOLT2CODE_VCC(BSP_PMIC_VCC_NORMAL_VOLT)
#define BSP_PMIC_VDDA_NORMAL_VOLT       1300    // 1300mV
#define BSP_PMIC_VDDA_NORMAL_CODE       BSP_PMIC_VOLT2CODE(BSP_PMIC_VDDA_NORMAL_VOLT)


//------------------------------------------------------------------------------
// LCD Panel Configuration to support IOCTL_HAL_QUERY_DISPLAYSETTINGS
//
// The values set here will dictate the initial display mode for
// the platform, overriding the initial mode set in the registry.  
// Set these values to 0 to ensure that the initial configuration
// from platform.reg is used.
// 
//------------------------------------------------------------------------------
#define BSP_PREF_DISPLAY_WIDTH          0 //240
#define BSP_PREF_DISPLAY_HEIGHT         0 //320
#define BSP_PREF_DISPLAY_BPP            0 //16


#define BSP_DISP_PWM_IOMUX_PIN          DDK_IOMUX_PIN_SD1_CMD   //PIN
#define BSP_DISP_PWM_IOMUX_PAD          DDK_IOMUX_PAD_SD1_CMD   //PAD

#define BSP_DISP_PWM_GPIO_PIN           18    //Pin 0 
#define BSP_DISP_PWM_GPIO_PIN_MASK      GPIO_PIN_MASK(BSP_DISP_PWM_GPIO_PIN)
#define BSP_DISP_PWM_GPIO               DDK_GPIO_PORT1    //GPIO 1

//------------------------------------------------------------------------------
// Power management of the debug serial port is controlled as follows:
//
// BSP_DEBUG_SERIAL_PM = TRUE  => Power management enabled. UART will be
//                                clock gated when TX FIFO is empty.
//
// BSP_DEBUG_SERIAL_PM = FALSE => Power management disabled. UART clocks
//                                will remain enabled upon first call to
//                                OEMWriteDebugByte/OEMReadDebugByte.  This
//                                setting may be required when using the
//                                debug serial port for console input.
//
// NOTE:  These settings only apply for the OS image.  The UART clocks are
//        enabled and not managed for bootloaders.
//
#define BSP_DEBUG_SERIAL_PM     TRUE


//------------------------------------------------------------------------------
// Serial KITL Configurations
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//
//  Define:  BSP_BASE_REG_PA_SERIALKITL
//
//  Specifies physical address of serial port used for serial KITL transport.
//
#define BSP_BASE_REG_PA_SERIALKITL      CSP_BASE_REG_PA_UART1
#define BSP_UART_KITL_SERIAL_BAUD       115200

//------------------------------------------------------------------------------
// I2C Port
// The I2C_PORT specifies the which I2C device will be used.  It must be
// one of the following :
//     CAM_I2C_PORT
//     USB_I2C_PORT
//------------------------------------------------------------------------------
#define CAM_I2C_PORT           L"I2C1:"
#define USB_I2C_PORT           L"I2C3:"

// I2C pinmuxing
#define I2C1_SCL_DDK_IOMUX_PAD			DDK_IOMUX_PAD_CSI0_DAT9
#define I2C1_SCL_DDK_IOMUX_PIN			DDK_IOMUX_PIN_CSI0_DAT9
#define I2C1_SCL_DDK_IOMUX_PIN_MUXMODE	DDK_IOMUX_PIN_MUXMODE_ALT4
#define I2C1_SCL_DDK_IOMUX_SELECT_INPUT	DDK_IOMUX_SELECT_INPUT_I2C1_IPP_SCL_IN

#define I2C1_SDA_DDK_IOMUX_PAD			DDK_IOMUX_PAD_CSI0_DAT8
#define I2C1_SDA_DDK_IOMUX_PIN			DDK_IOMUX_PIN_CSI0_DAT8
#define I2C1_SDA_DDK_IOMUX_PIN_MUXMODE	DDK_IOMUX_PIN_MUXMODE_ALT4
#define I2C1_SDA_DDK_IOMUX_SELECT_INPUT	DDK_IOMUX_SELECT_INPUT_I2C1_IPP_SDA_IN

//------------------------------------------------------------------------------
#define SYSINTR_USBOTG          (SYSINTR_FIRMWARE+2)

//------------------------------------------------------------------------------
// UART Port
// Defines for UART Rx and Tx SDMA buffer size per SDMA buffer descriptor.  
//------------------------------------------------------------------------------
#define SERIAL_SDMA_RX_BUFFER_SIZE 0x200
#define SERIAL_SDMA_TX_BUFFER_SIZE 0x400

//------------------------------------------------------------------------------
// Defines the Maximum Baudrate and reference frequency for UART
//------------------------------------------------------------------------------
#define UART_MAX_BAUDRATE    4000000
#define UART_REF_FREQ        (16 * UART_MAX_BAUDRATE)


//------------------------------------------------------------------------------
// SATA Configuration
//------------------------------------------------------------------------------
// SATA PHY use usb clock or external clock
// eboot and SATA/TPS driver should be same
#define USEUSBCLOCK 1
#define USEEXTCLOCK 0

//------------------------------------------------------------------------------
// Expand IO Configuration
//------------------------------------------------------------------------------
//For expand IO 1
#define EIO_BACKLIGHT_ON    0
#define EIO_PORT3_P114      1
#define EIO_CPU_PER_RST     2
#define EIO_MAIN_PER_RST_B  3
#define EIO_IPOD_RST_B      4
#define EIO_MLB_RST_B       5
#define EIO_SSI_STERRING    6
#define EIO_GPS_RST_B       7
//For expand IO 2
#define EIO_CTRL_O      8
#define EIO_CTRL_1      9
#define EIO_CTRL_2      10
#define EIO_CTRL_3      11
#define EIO_CTRL_4      12
#define EIO_PORT3_P116  13
#define EIO_PORT2_P81   14
#define EIO_PORT1_P101  15
#define EIO_MAX         15
//
// IO Control Codes
//
// Developers are encourage to use the macros listed below to access the driver
// capabilities.
// All the IOCTL codes provided here has a macro equivalent. The details for each
// IOCTL will be explained in the corresponding macros.
//
#define EIO_IOCTL_READ_EIO        CTL_CODE(FILE_DEVICE_BUS_EXTENDER, 3000, METHOD_BUFFERED, FILE_ANY_ACCESS)  
#define EIO_IOCTL_CONFIGURE_EIO   CTL_CODE(FILE_DEVICE_BUS_EXTENDER, 3001, METHOD_BUFFERED, FILE_ANY_ACCESS)  


#endif
