;------------------------------------------------------------------------------
;
;   Copyright (C) 2009-2011, Freescale Semiconductor, Inc. All Rights Reserved.
;   THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
;   AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
;
;------------------------------------------------------------------------------

    ; DDR2 IOMUX configuration

    ; Configure DDR clock input to use AXI_B (default is AXI_A)
    ldr     r1, =CSP_BASE_REG_PA_CCM
    ldr     r0, [r1, #0x18]
    orr     r0, r0, #(1 << 10)
    str     r0, [r1, #0x18]


    ; IOMUXC_SW_PAD_CTL_PAD_DRAM_DQM3
    ldr     r1, =0x53fa8554
    ldr     r0, =0x00380000
    str     r0, [r1]
    
    ; IOMUXC_SW_PAD_CTL_PAD_DRAM_SDQS3
    ldr     r1, =0x53fa8558
    ldr     r0, =0x00380040
    str     r0, [r1]
    
    ; IOMUXC_SW_PAD_CTL_PAD_DRAM_DQM2
    ldr     r1, =0x53fa8560
    ldr     r0, =0x00380000
    str     r0, [r1]
    
    ; IOMUXC_SW_PAD_CTL_PAD_DRAM_SDODT1
    ldr     r1, =0x53fa8564
    ldr     r0, =0x00380040
    str     r0, [r1]
    
    ; IOMUXC_SW_PAD_CTL_PAD_DRAM_SDQS2
    ldr     r1, =0x53fa8568
    ldr     r0, =0x00380040
    str     r0, [r1]
    
    ; IOMUXC_SW_PAD_CTL_PAD_DRAM_SDCLK_1
    ldr     r1, =0x53fa8570
    ldr     r0, =0x00380000
    str     r0, [r1]
    
    ; IOMUXC_SW_PAD_CTL_PAD_DRAM_CAS
    ldr     r1, =0x53fa8574
    ldr     r0, =0x00380000
    str     r0, [r1]
    
    ; IOMUXC_SW_PAD_CTL_PAD_DRAM_SDCLK_0
    ldr     r1, =0x53fa8578
    ldr     r0, =0x00380000
    str     r0, [r1]
    
    ; IOMUXC_SW_PAD_CTL_PAD_DRAM_SDQS0
    ldr     r1, =0x53fa857c
    ldr     r0, =0x00380040
    str     r0, [r1]
    
    ; IOMUXC_SW_PAD_CTL_PAD_DRAM_SDODT0
    ldr     r1, =0x53fa8580
    ldr     r0, =0x00380040
    str     r0, [r1]
    
    ; IOMUXC_SW_PAD_CTL_PAD_DRAM_DQM0
    ldr     r1, =0x53fa8584
    ldr     r0, =0x00380000
    str     r0, [r1]
    
    ; IOMUXC_SW_PAD_CTL_PAD_DRAM_RAS
    ldr     r1, =0x53fa8588
    ldr     r0, =0x00380000
    str     r0, [r1]
    
    ; IOMUXC_SW_PAD_CTL_PAD_DRAM_SDQS1
    ldr     r1, =0x53fa8590
    ldr     r0, =0x00380040
    str     r0, [r1]
    
    ; IOMUXC_SW_PAD_CTL_PAD_DRAM_DQM1
    ldr     r1, =0x53fa8594
    ldr     r0, =0x00380000
    str     r0, [r1]
    
    ; IOMUXC_SW_PAD_CTL_GRP_ADDDS
    ldr     r1, =0x53fa86f0
    ldr     r0, =0x00380000
    str     r0, [r1]
    
    ; IOMUXC_SW_PAD_CTL_GRP_DDRMODE_CTL
    ldr     r1, =0x53fa86f4
    ldr     r0, =0x00000200
    str     r0, [r1]
    
    ; IOMUXC_SW_PAD_CTL_GRP_DDRPKE
    ldr     r1, =0x53fa86fc
    ldr     r0, =0x00000000
    str     r0, [r1]
    
    ; IOMUXC_SW_PAD_CTL_GRP_DDRMODE - CMOS mode
    ldr     r1, =0x53fa8714
    ldr     r0, =0x00000000
    str     r0, [r1]
    
    ; IOMUXC_SW_PAD_CTL_GRP_B0DS
    ldr     r1, =0x53fa8718
    ldr     r0, =0x00380000
    str     r0, [r1]
    
    ; IOMUXC_SW_PAD_CTL_GRP_B1DS
    ldr     r1, =0x53fa871c
    ldr     r0, =0x00380000
    str     r0, [r1]

    ; IOMUXC_SW_PAD_CTL_GRP_CTLDS    
    ldr     r1, =0x53fa8720
    ldr     r0, =0x00380000
    str     r0, [r1]
    
    ; IOMUXC_SW_PAD_CTL_GRP_DDR_TYPE - DDR_SEL=0 
    ldr     r1, =0x53fa8724
    ldr     r0, =0x06000000
    str     r0, [r1]
    
    ; IOMUXC_SW_PAD_CTL_GRP_B2DS
    ldr     r1, =0x53fa8728
    ldr     r0, =0x00380000
    str     r0, [r1]
    
    ; IOMUXC_SW_PAD_CTL_GRP_B3DS
    ldr     r1, =0x53fa872c
    ldr     r0, =0x00380000
    str     r0, [r1]

    ; Initialize DDR2 memory - Hynix H5PS2G83AFR
    ldr     r1, =0x63fd9088
    ldr     r0, =0x2b2f3031
    str     r0, [r1]

    ldr     r1, =0x63fd9090
    ldr     r0, =0x40363333
    str     r0, [r1]

    ldr     r1, =0x63fd90F8
    ldr     r0, =0x00000800
    str     r0, [r1]

    ldr     r1, =0x63fd907c
    ldr     r0, =0x01310132
    str     r0, [r1]

    ldr     r1, =0x63fd9080
    ldr     r0, =0x0133014b
    str     r0, [r1]
    
    ; Enable bank interleaving, RALAT = 0x3, DDR2_EN = 1
    ldr     r1, =0x63fd9018
    ldr     r0, =0x000016d0
    str     r0, [r1]

    ; Enable CSD0 and CSD1, row width = 15, column width = 10, burst length = 4, data width = 32bit
    ldr     r1, =0x63fd9000
    ldr     r0, =0xc4110000
    str     r0, [r1]

    ; tRFC = 78 ck, tXS = 82 ck, tXP = 2 ck, tXPDLL(tXARD) = 2 ck, tFAW = 14 ck, CAS latency = 5 ck
    ldr     r1, =0x63fd900C
    ldr     r0, =0x4d5122d2
    str     r0, [r1]

    ; tRCD = 5 ck, tRP = 5 ck, tRC = 23 ck, tRAS = 18 ck, tRPA = 1, tWR = 6 ck, tMRD = 2 ck, tCWL = 4 ck
    ldr     r1, =0x63fd9010
    ldr     r0, =0x92d18a22
    str     r0, [r1]

    ; tDLLK(tXSRD) = 200 cycles, tRTP = 3 ck, tWTR = 3ck, tRRD = 3ck
    ldr     r1, =0x63fd9014
    ldr     r0, =0x00c70092
    str     r0, [r1]

    ldr     r1, =0x63fd902c
    ldr     r0, =0x000026d2
    str     r0, [r1]

    ldr     r1, =0x63fd9030
    ldr     r0, =0x009f000e
    str     r0, [r1]

    ldr     r1, =0x63fd9008
    ldr     r0, =0x12272000
    str     r0, [r1]

    ldr     r1, =0x63fd9004
    ldr     r0, =0x00030012
    str     r0, [r1]

    ldr     r1, =0x63fd901c
    ldr     r0, =0x04008010
    str     r0, [r1]

    ldr     r1, =0x63fd901c
    ldr     r0, =0x00008032
    str     r0, [r1]

    ldr     r1, =0x63fd901c
    ldr     r0, =0x00008033
    str     r0, [r1]

    ldr     r1, =0x63fd901c
    ldr     r0, =0x00008031
    str     r0, [r1]

    ldr     r1, =0x63fd901c
    ldr     r0, =0x0b5280b0
    str     r0, [r1]

    ldr     r1, =0x63fd901c
    ldr     r0, =0x04008010
    str     r0, [r1]

    ldr     r1, =0x63fd901c
    ldr     r0, =0x00008020
    str     r0, [r1]

    ldr     r1, =0x63fd901c
    ldr     r0, =0x00008020
    str     r0, [r1]

    ; BL = 4, CAS latency = 5, write recovery = 6
    ldr     r1, =0x63fd901c
    ldr     r0, =0x0a528030
    str     r0, [r1]

    ldr     r1, =0x63fd901c
    ldr     r0, =0x03c68031  
    str     r0, [r1]

    ; reduced drive strength, enable 50ohm ODT
    ldr     r1, =0x63fd901c
    ldr     r0, =0x00468031
    str     r0, [r1]

    ldr     r1, =0x63fd901c
    ldr     r0, =0x04008018
    str     r0, [r1]

    ldr     r1, =0x63fd901c
    ldr     r0, =0x0000803a
    str     r0, [r1]

    ldr     r1, =0x63fd901c
    ldr     r0, =0x0000803b
    str     r0, [r1]

    ldr     r1, =0x63fd901c
    ldr     r0, =0x00008039
    str     r0, [r1]

    ldr     r1, =0x63fd901c
    ldr     r0, =0x0b528138
    str     r0, [r1]

    ldr     r1, =0x63fd901c
    ldr     r0, =0x04008018
    str     r0, [r1]

    ldr     r1, =0x63fd901c
    ldr     r0, =0x00008028
    str     r0, [r1]

    ldr     r1, =0x63fd901c
    ldr     r0, =0x00008028
    str     r0, [r1]

    ; BL = 4, CAS latency = 5, write recovery = 6
    ldr     r1, =0x63fd901c
    ldr     r0, =0x0a528038
    str     r0, [r1]

    ldr     r1, =0x63fd901c
    ldr     r0, =0x03c68039
    str     r0, [r1]

    ; reduced drive strength, enable 50ohm ODT
    ldr     r1, =0x63fd901c
    ldr     r0, =0x00468039 
    str     r0, [r1]

    ldr     r1, =0x63fd9020
    ldr     r0, =0x00005800
    str     r0, [r1]

    ; Enable 50ohm ODT
    ldr     r1, =0x63fd9058
    ldr     r0, =0x00033337
    str     r0, [r1]

    ldr     r1, =0x63fd901c
    ldr     r0, =0x00000000    
    str     r0, [r1]

    END
