;
; Copyright (c) Microsoft Corporation.  All rights reserved.
;
;
; Use of this source code is subject to the terms of the Microsoft end-user
; license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
; If you did not accept the terms of the EULA, you are not authorized to use
; this source code. For a copy of the EULA, please see the LICENSE.RTF on your
; install media.
;
;------------------------------------------------------------------------------
;
; Copyright (C) 2007-2011, Freescale Semiconductor, Inc. All Rights Reserved.
; THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
; AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
;
;------------------------------------------------------------------------------
;
; File: oemaddrtab_cfg.inc
;
; This file is used to define g_oalAddressTable. This table is passed to
; KernelStart to estabilish physical to virtual memory mapping. This table
; is used also in memory OAL module to map between physical and virtual
; memory addresses via OALPAtoVA/OALVAtoPA functions.
;
; The config.bib file defines image memory layout ant it contains virtual
; cached memory addresses which must be synchronized with g_oalAddressTable.
; With each table change make sure that those constant are still valid.
;
;------------------------------------------------------------------------------

    INCLUDE image_cfg.inc
    
; Export Definition

    EXPORT  g_oalAddressTable[DATA]

;------------------------------------------------------------------------------
; Old Table format: cached address, physical address, size

g_oalAddressTable

    ; RAM 0x1000_0000 0x27FF_FFFF
    ;     0x8000_0000 0x97FF_FFFF
    ;DCD 0x80000000, 0x10000000, 0x00000180
    DCD 0x80000000, CSP_BASE_MEM_PA_DRAM_LOW, STATIC_MAPPING_RAM_SIZE

    ;
    ; RESERVED 48MB 0x2800_0000 0x2AFF_FFFF or ?
    ;               0x9800_0000 0x9AFF_FFFF

    ; EIM 0x0C00_0000 0x0E0F_FFFF
    ;     0x9B00_0000 0x9D0F_FFFF
    ;DCD 0x9B000000, 0x0C000000, 0x00000021
    DCD 0x9B000000, CSP_BASE_MEM_PA_EIM_CS0+0x04000000, 33

    ;
    ; RESERVED 3MB  0x2D10_0000 0x2D3F_FFFF or ? or ?
    ;               0x9D10_0000 0x9D3F_FFFF

    ; IO 0x0000_0000 0x02BF_FFFF
    ;    0x9D40_0000 0x9FFF_FFFF
    ;DCD 0x9D400000, 0x00000000, 0x0000002C
    DCD 0x9D400000, CSP_BASE_MEM_PA_ROM, 44

    DCD 0x00000000, 0x00000000, 0x00000000

;------------------------------------------------------------------------------
; CE7 Device Table format: VA, PA >> 8, Size in bytes (not MB), Section Attribs
; Attributes set for kernel and user RW access for debugging with shell
; set attribs = 0 to restrict device table register access to kernel drivers only
; Don't overlap with SDRAM address mapping below

g_oalCE7DeviceTable

    ;
    ; RESERVED 384MB 0x1000_0000 0x27FF_FFFF
    ;                0xA000_0000 0xB7FF_FFFF

    ;
    ; RESERVED 48MB 0x2800_0000 0x2AFF_FFFF or ?
    ;               0xB800_0000 0xBAFF_FFFF

    ; EIM 0x0800_0000 0x0A0F_FFFF
    ;     0xBB00_0000 0xBD0F_FFFF
    ;DCD 0xBB000000, 0x000C0000, 0x02100000, 0x00000C00
    DCD 0xBB000000, (CSP_BASE_MEM_PA_EIM_CS0+0x04000000)>>8, 33<<20, 0x00000C00

    ;
    ; RESERVED 3MB  0x2D10_0000 0x2D3F_FFFF or ? or ?
    ;               0xBD10_0000 0xBD3F_FFFF

    ; IO 0x0000_0000 0x02BF_FFFF
    ;    0xBD40_0000 0xBFFF_FFFF
    ;DCD 0xBD400000, 0x00000000, 0x02C00000, 0x00000C00
    DCD 0xBD400000, CSP_BASE_MEM_PA_ROM>>8, 44<<20, 0x00000C00

    DCD 0x00000000, 0x00000000, 0x00000000, 0x00000000

;------------------------------------------------------------------------------
; CE7 Table format: cached address, physical address, size

g_oalCE7AddressTable

    ; #define CE_NEW_MAPPING_TABLE 0x87654321
    ;DCD 0x87654321, 0x10201458, 0x00000000
    DCD CE_NEW_MAPPING_TABLE, g_oalCE7DeviceTable, 0x00000000

    ; RAM1 0x1000_0000 0x27FF_FFFF
    ;      0x8000_0000 0x97FF_FFFF
    ;DCD 0x80000000, 0x10000000, 0x00000180
    DCD 0x80000000, CSP_BASE_MEM_PA_DRAM_LOW, STATIC_MAPPING_RAM_SIZE

    ;
    ; RESERVED 48MB 0x2800_0000 0x2AFF_FFFF or ?
    ;               0x9800_0000 0x9AFF_FFFF

    ; RAM2 0x2B00_0000 0x2D0F_FFFF
    ;      0x9B00_0000 0x9D0F_FFFF
    ;DCD 0x9B000000, 0x2B000000, 0x00000021
    DCD 0x9B000000, 0x2B000000, 0x00000021

    ;
    ; RESERVED 3MB  0x2D10_0000 0x2D3F_FFFF or ? or ?
    ;               0x9D10_0000 0x9D3F_FFFF

    ; RAM3 0x2D40_0000 0x2FFF_FFFF
    ;      0x9D40_0000 0x9FFF_FFFF
    ;DCD 0x9D400000, 0x2D400000, 0x0000002C
    DCD 0x9D400000, 0x2D400000, 0x0000002C

    DCD 0x00000000, 0x00000000, 0x00000000

;------------------------------------------------------------------------------

    END
