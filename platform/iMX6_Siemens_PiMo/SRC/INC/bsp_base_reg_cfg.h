//------------------------------------------------------------------------------
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//------------------------------------------------------------------------------
//
//  Copyright (C) 2012, Adeneo Embedded All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//  Copyright (C) 2007-2010, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  bsp_base_reg_cfg.h
//
//  This header file defines location for BSP on-board devices. It usually
//  should contain only physical addresses. Virtual addresses should be obtain
//  via OALPAtoVA function call. Base addresses for SoC are defined in similar
//  file s3c2410x_base_reg_cfg.h.
//
//------------------------------------------------------------------------------
#ifndef __BSP_BASE_REG_CFG_H
#define __BSP_BASE_REG_CFG_H

#if __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------
//
//  NAMING CONVENTIONS
//
//  BSP_BASE_REG_ is the standard prefix for BSP device base registers.
//
//  Memory ranges are accessed using physical, uncached, or cached addresses,
//  depending on the system state. The following abbreviations are used for
//  each addressing type:
//
//      PA - physical address
//      CA - cached virtual address
//      UA - uncached virtual address
//
//  The naming convention for base registers is:
//
//      xxx_BASE_REG_<ADDRTYPE>_<SUBSYSTEM>
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//
//  Define:  BSP_BASE_REG_PA_DEBUG_SERIAL
//
//  Specifies physical address of the UART used for serial debug support.
//  Undefine to remove serial debug support from image.
//
#define BSP_BASE_REG_PA_DEBUG_SERIAL        CSP_BASE_REG_PA_UART1

///------------------------------------------------------------------------------
//
//  Define:  BSP_BASE_REG_PA_LAN911x
//
#define BSP_BASE_REG_PA_LAN911x_IOBASE      (CSP_BASE_MEM_PA_EIM_CS0+0x04000000)

//------------------------------------------------------------------------------
//
//  Define:  BSP_BASE_REG_PA_FRAMEBUFFER
//
//  Specifies physical address of display frame buffer.  We use
//  a reserved block of external SDRAM for the frame buffer. Note that there
//  must exist memory mapping in oemaddrtab_cfg.h for this memory area.
//
#define BSP_BASE_REG_PA_FRAMEBUFFER             (0x80100000)

//------------------------------------------------------------------------------

#if __cplusplus
}
#endif

#endif
