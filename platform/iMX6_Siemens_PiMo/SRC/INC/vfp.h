//------------------------------------------------------------------------------
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//------------------------------------------------------------------------------
//
//  Copyright (C) 2008-2010, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File: vfp.h
//
//  This file defines the VFP arguments and functions
//
//------------------------------------------------------------------------------
#ifndef _VFP_H_
#define _VFP_H_

#if    __cplusplus
extern "C" {
#endif

typedef struct
{
    UINT32  FPSCR;
    UINT64  D[32];
} NEON_CONTEXT, *PNEON_CONTEXT;

#define VFP_EXTRA_REG_SIZE      sizeof(NEON_CONTEXT)

//------------------------------------------------------------------------------


void VFP_InitExtraRegs(LPBYTE pExtraRegs);
void VFP_SaveExtraRegs(LPBYTE pExtraRegs);
void VFP_RestoreExtraRegs(LPBYTE pExtraRegs);
void VFP_Init(void);

#ifdef __cplusplus
}
#endif

#endif

