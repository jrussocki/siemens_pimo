//------------------------------------------------------------------------------
//
//  Copyright (C) 2004-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------

/*
*********************************************************************
* THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
* ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
* THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
* PARTICULAR PURPOSE.
* Copyright(c) Dialog Semiconductor ltd. 2009.,  All rights reserved.
*
* File Name : sdk_wdt.h
*
* Abstract : Declares the streams driver interface functions for
*			 IMX6 watchdog timer.
*********************************************************************
*/

#ifndef _SDK_WDT_H_
#define _SDK_WDT_H_

// Put the Sytem Include Header Files First.
#include <windows.h>

#ifdef __cplusplus
extern "C" {
#endif // cplusplus





// IOCTLS for WDTs control

#define WDT_IOCTL_INDEX        0x0900   

    // Enable a watch dog timer to monitor an input output pin
#define IOCTL_ENABLE_WATCHDOG_TIMER    CTL_CODE( FILE_DEVICE_UNKNOWN,  \
                                             WDT_IOCTL_INDEX + 0, \
                                               METHOD_BUFFERED,     \
                                               FILE_ANY_ACCESS)

#define IOCTL_DISABLE_WATCHDOG_TIMER    CTL_CODE( FILE_DEVICE_UNKNOWN,  \
                                             WDT_IOCTL_INDEX + 1, \
                                               METHOD_BUFFERED,     \
                                               FILE_ANY_ACCESS)






typedef struct {
    LPCWSTR WatchdogName; // Watchdog timer name
    DWORD TimerPeriod;  // Period of watchdog timer in millseconds
    DWORD TimerWait;    // Delay to wait within TimerPeriod before enabling the default action
    DWORD Action; // Action to perform at the end of the timer period or within
    BOOL Enable; // Define if the timer is enabled or disabled
}T_IOCTL_ENABLE_WATCHDOG_TIMER_PARAM;

typedef struct{
HANDLE watchdog_timer_to_stop;
} T_IOCTL_DISABLE_WATCHDOG_TIMER_PARAM;

/*********************************************************************
* Function Name : WDT_Init()
*
* Arguments : 
* dwContext[in]: Pointer to a string containing the registry 
*                path to the active key for 
*                the stream interface driver.
* lpvBusContext[in]: Potentially process-mapped pointer passed as 
*                    the fourth parameter to ActivateDeviceEx().
*
* Returns : Returns a handle to the device context 
*           created if successful. 
*           Returns zero if not successful. 
*********************************************************************/
DWORD WDT_Init(DWORD dwContext, LPCVOID lpvBusContext);

/*********************************************************************
* Function Name : WDT_Deinit()
* Arguments :  
* dwContext[in]: Handle to the device context, returned
*                by WDT_Init  
*
* Returns : TRUE indicates success. FALSE indicates failure.
*********************************************************************/
BOOL WDT_Deinit(DWORD dwContext);

/*********************************************************************
* Function Name : WDT_Open()
*
* Arguments : 
* dwContext[in]: Context returned by WDT_Init.
* dwAccess[in]: parm access code
* dwShare[in]: parm share mode
*
* Returns : If SUCCESS, this function returns a handle that identifies 
*           the open context of the device to the calling application. 
*           If the device can be opened multiple times, this handle 
*           will be used to identify each open context. 
*           This function returns zero if the device cannot be opened.
*
*********************************************************************/
DWORD WDT_Open(DWORD dwContext, DWORD dwAccess, DWORD dwShare);

/*********************************************************************
* Function Name : WDT_Close() 
*
* Arguments :
* dwOpen[in]: Handle returned by the WDT_Open (Device Manager) 
*             function, which is used to identify the open context
*             of the device.
* Returns : TRUE indicates success. FALSE indicates failure.
*
*********************************************************************/
BOOL WDT_Close(DWORD dwOpen);

BOOL WDT_Read(DWORD dwOpen);
BOOL WDT_Write(DWORD dwOpen);
BOOL WDT_Seek(DWORD dwOpen);

/*********************************************************************
* Function Name : WDT_IOControl()
*
* Arguments : 
* dwOpen[in]: Handle to the open context of the device. 
*           The XXX_Open (Device Manager) function creates and returns
*           this identifier.
* dwCode[in]: I/O control operation to perfom
* pIn[in]: Pointer to the buffer containing data to
*           transfer to the device.
* dwIn[in]: Number of bytes of data in the buffer 
*           specified for pIn.
* pOut[out]: Pointer to the buffer used to transfer the output data 
*         from the device.
* dwOut[in]: Maximum number of bytes in the buffer specified 
*            by pOut.
* pdwBytesWritten[out]: Pointer to the DWORD buffer that this function 
*                       uses to return the actual number of bytes
*                       received from the device.  
*
* Returns : TRUE indicates success. FALSE indicates failure.
*
*********************************************************************/
BOOL WDT_IOControl(DWORD dwOpen, DWORD dwCode, PBYTE pIn,
				   DWORD dwIn, PBYTE pOut, DWORD dwOut,
                   DWORD *pdwBytesWritten);

#ifdef __cplusplus
} // extern "C"
#endif

#endif /* _WDT_HEADER_H */

