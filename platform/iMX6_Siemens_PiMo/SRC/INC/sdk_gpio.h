//------------------------------------------------------------------------------
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//------------------------------------------------------------------------------
//
//  Copyright (C) 2008-2010, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File: sdk_gpio.h
//
//
//------------------------------------------------------------------------------
#ifndef _SDK_GPIO_H_
#define _SDK_GPIO_H_

#include <winioctl.h>

#if    __cplusplus
extern "C" {
#endif

// IOCTLS for GPIOs control.
#define GPIO_IOCTL_INDEX        0x1000

// Configure GPIO direction and interrupt of a pin
#define IOCTL_GPIO_CONFIG       CTL_CODE( FILE_DEVICE_UNKNOWN,  \
                                           GPIO_IOCTL_INDEX + 0,  \
                                           METHOD_BUFFERED,             \
                                           FILE_ANY_ACCESS)
// Set data on GPIO output pin
#define IOCTL_GPIO_SET          CTL_CODE( FILE_DEVICE_UNKNOWN,  \
                                           GPIO_IOCTL_INDEX + 1,  \
                                           METHOD_BUFFERED,             \
                                           FILE_ANY_ACCESS)
// Get data on GPIO input pin
#define IOCTL_GPIO_GET          CTL_CODE( FILE_DEVICE_UNKNOWN,  \
                                           GPIO_IOCTL_INDEX + 2,  \
                                           METHOD_BUFFERED,             \
                                           FILE_ANY_ACCESS)
// Set interrupt on GPIO pin
#define IOCTL_GPIO_IRQ_INIT     CTL_CODE( FILE_DEVICE_UNKNOWN,  \
                                           GPIO_IOCTL_INDEX + 3,  \
                                           METHOD_BUFFERED,             \
                                           FILE_ANY_ACCESS)
// Remove interrupt on GPIO pin
#define IOCTL_GPIO_IRQ_DEINIT   CTL_CODE( FILE_DEVICE_UNKNOWN,  \
                                           GPIO_IOCTL_INDEX + 4,  \
                                           METHOD_BUFFERED,             \
                                           FILE_ANY_ACCESS)
 // Get an event from an interrupt                                         
#define IOCTL_GPIO_IRQ_GET_EVENT CTL_CODE( FILE_DEVICE_UNKNOWN,  \
                                           GPIO_IOCTL_INDEX + 5,  \
                                           METHOD_BUFFERED,             \
                                           FILE_ANY_ACCESS)
// Set iomux configuration for a pin
// TC : IO MUX IOCTL removed, because the configuration is made in the bootloader.

// Configure physical properties of the signal on the pin (slew rate, etc...)
// TC : PAD CONF IOCTL removed, because the configuration is made in the bootloader.


// Get the whole ccnfiguration for an pin of a GPIO port
#define IOCTL_GPIO_GET_CONFIG   CTL_CODE( FILE_DEVICE_UNKNOWN,  \
                                           GPIO_IOCTL_INDEX + 14,  \
                                           METHOD_BUFFERED,             \
                                           FILE_ANY_ACCESS)

typedef enum 
{
    GPIO_PORT1      = 0,
    GPIO_PORT2      = 1,
    GPIO_PORT3      = 2,
    GPIO_PORT4      = 3,
    GPIO_PORT5      = 4,
    GPIO_PORT6      = 5,
    GPIO_PORT7      = 6
} GPIO_PORT;

typedef enum 
{
    GPIO_DIR_IN     = 0,
    GPIO_DIR_OUT    = 1
} GPIO_DIR;

typedef enum 
{
    GPIO_INTR_LOW_LEV   = 0,
    GPIO_INTR_HIGH_LEV  = 1,
    GPIO_INTR_RISE_EDGE = 2,
    GPIO_INTR_FALL_EDGE = 3,
    GPIO_INTR_BOTH_EDGE = 4,    // Unavailable on MX31/MX32
    GPIO_INTR_NONE      = 5
} GPIO_INTR;

typedef struct {
    GPIO_PORT port;
    UINT32 pin;
    GPIO_DIR dir;
    GPIO_INTR intr;
} T_IOCTL_GPIO_CONFIG_PARAM;

typedef struct {
    GPIO_PORT port;
    UINT32 pin;
    UINT32 data;
} T_IOCTL_GPIO_SET_PARAM;

typedef struct {
    GPIO_PORT port;
    UINT32 pin;
    UINT32 data;
} T_IOCTL_GPIO_GET_PARAM;

typedef struct {
    GPIO_PORT port;
    DWORD dwPin;
    HANDLE hEvent;
} T_IOCTL_GPIO_IRQ_INIT_PARAM;

typedef struct {
    GPIO_PORT port;
    DWORD dwPin;
} T_IOCTL_GPIO_IRQ_DEINIT_PARAM;

typedef struct {
    DDK_IOMUX_PIN pin;
    DDK_IOMUX_PIN_MUXMODE muxmode;
    DDK_IOMUX_PIN_SION sion;
} T_IOCTL_IOMUX_SET_PARAM;

typedef T_IOCTL_IOMUX_SET_PARAM T_IOCTL_IOMUX_GET_PARAM;

typedef struct {
    DDK_IOMUX_PAD pad;
    DDK_IOMUX_PAD_SLEW slew;
    DDK_IOMUX_PAD_DRIVE drive;
    DDK_IOMUX_PAD_SPEED speed;
    DDK_IOMUX_PAD_OPENDRAIN openDrain;
    DDK_IOMUX_PAD_PULL pull;
    DDK_IOMUX_PAD_HYSTERESIS hysteresis;
} T_IOCTL_PADCONF_SET_PARAM;

typedef T_IOCTL_PADCONF_SET_PARAM T_IOCTL_PADCONF_GET_PARAM;

typedef struct
{
    GPIO_PORT port;
    UINT32 pin;
    CSP_GPIO_REGS reg;
}IOCTL_GPIO_GET_CONFIG_PARAM;

#ifdef __cplusplus
}
#endif

#endif

