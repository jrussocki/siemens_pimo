//------------------------------------------------------------------------------
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//------------------------------------------------------------------------------
//
//  Copyright (C) 2008-2010, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File: sdk_pwm.h
//
//
//------------------------------------------------------------------------------
#ifndef _SDK_PWM_H_
#define _SDK_PWM_H_

#include <winioctl.h>

#if    __cplusplus
extern "C" {
#endif

// IOCTLS for PWM control.
#define PWM_IOCTL_INDEX         0x2000

#define IOCTL_PWM_SET_PERIOD    CTL_CODE( FILE_DEVICE_UNKNOWN,  \
                                           PWM_IOCTL_INDEX + 0, \
                                           METHOD_BUFFERED,     \
                                           FILE_ANY_ACCESS)

#define IOCTL_PWM_SET_SAMPLE    CTL_CODE( FILE_DEVICE_UNKNOWN,  \
                                           PWM_IOCTL_INDEX + 1, \
                                           METHOD_BUFFERED,     \
                                           FILE_ANY_ACCESS)

#define IOCTL_PWM_START         CTL_CODE( FILE_DEVICE_UNKNOWN,  \
                                           PWM_IOCTL_INDEX + 2, \
                                           METHOD_BUFFERED,     \
                                           FILE_ANY_ACCESS)

#define IOCTL_PWM_STOP          CTL_CODE( FILE_DEVICE_UNKNOWN,  \
                                           PWM_IOCTL_INDEX + 3, \
                                           METHOD_BUFFERED,     \
                                           FILE_ANY_ACCESS)

#define IOCTL_PWM_CLOCK_GATING  CTL_CODE( FILE_DEVICE_UNKNOWN,  \
                                           PWM_IOCTL_INDEX + 4, \
                                           METHOD_BUFFERED,     \
                                           FILE_ANY_ACCESS)

// TC : IO MUX IOCTL removed, because the configuration is made in the bootloader.

#define IOCTL_PWM_SET_DIVIDER   CTL_CODE( FILE_DEVICE_UNKNOWN,  \
                                           PWM_IOCTL_INDEX + 6, \
                                           METHOD_BUFFERED,     \
                                           FILE_ANY_ACCESS)

#define IOCTL_PWM_READREGISTER   CTL_CODE( FILE_DEVICE_UNKNOWN, \
                                           PWM_IOCTL_INDEX + 7, \
                                           METHOD_BUFFERED,     \
                                           FILE_ANY_ACCESS)

typedef struct {
    UINT32 u32Period;
} T_IOCTL_PWM_SET_PERIOD_PARAM;

typedef struct {
    UINT32 u32Sample;
} T_IOCTL_PWM_SET_SAMPLE_PARAM;

typedef struct {
    BOOL bEnabled;
} T_IOCTL_PWM_CLOCK_GATING_PARAM;

typedef struct {
    DDK_IOMUX_PIN pin;
} T_IOCTL_PWM_IOMUX_PARAM;

typedef struct {
    UINT32 u32Divider;
} T_IOCTL_PWM_SET_DIVIDER_PARAM;

#ifdef __cplusplus
}
#endif

#endif