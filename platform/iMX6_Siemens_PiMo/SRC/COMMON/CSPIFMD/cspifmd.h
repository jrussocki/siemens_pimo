//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//-----------------------------------------------------------------------------
//
// Copyright (C) 2007-2010, Freescale Semiconductor, Inc. All Rights Reserved.
// THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
// AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//-----------------------------------------------------------------------------
//
// FILE:    cspifmd.h
//
//  This file contains structure definitions required by CSPI FMD
//
//------------------------------------------------------------------------------

#ifndef __SPIFMD_H__
#define __SPIFMD_H__

#include "bsp.h"

#pragma warning(push)
#pragma warning(disable: 4115 4127 4201 4214)
#include "fmd.h"
#pragma warning(pop)

// DEFINES

#define SPIFMD                                    3         // Flash Type
#define SPIFMD_SECTOR_SIZE                        512       // Sector(page) Size
#define SPIFMD_BLOCK_SIZE                         4096       // Sector(page) Size
#define SPIFMD_BLOCK_CNT                          1024      // Max Number of Blocks
#define SPIFMD_SECTOR_CNT                         8         // Number of sectors(pages) for a block

#define CAREAD_CMD          0x03
#define WREN_CMD            0x06
#define WRDI_CMD            0x04
#define RDID_CMD            0x9F
#define RDSR_CMD            0xD7
#define PSP_CMD             0x3D2A7FFC                  // Program Sector Protection CMD
#define ErSP_CMD            0x3D2A7FCF                  // Erase Sector Protection CMD
#define DSP_CMD             0x3D2A7F9A                  // Disable Sector Protection CMD
#define EnSP_CMD            0x3D2A7FA9                  // Enable Sector Protection CMD
#define BE_CMD              0x50                        // Block Erase CMD
#define BW_CMD              0x84                        // Buffer Write CMD
#define BMMPP_CMD           0x83                        // Buffer to Main Memory Page Program CMD
#define READ_CMD            0xD2                        // Main Memory Page Read CMD
#define PCR_CMD             0x3D2A80A6                  // Programming the Configuration Register CMD

#define WRSR_CYCLE_TIME             100*1000            //Write Status Register Cycle Time (in microseconds)
#define PP_CYCLE_TIME               5*1000              //Page Program Cycle Time
#define BE_CYCLE_TIME               2*1000*1000         //Block Erase Cycle Time

//Manufacturer and Device ID
#define MANUFACTURER_ID             0x1F

//Status Register
#define SR_PAGESIZE                 (1 << 24)           //Page Size Bit
#define SR_PROTECT                  (1 << 25)           //Write Protect Bit
#define SR_RDY                      (1 << 31)           //RDY Bit

#define WIP_TIMEOUT                 1000                //number of attempts to read WIP bit
#define ONE_MS                      1000                //1 MS in units of microseconds

// PROTOTYPES

BOOL SPIFMD_Init(void);
BOOL SPIFMD_Deinit (void);

BOOL SPIFMD_GetInfo(PFlashInfo pFlashInfo);
BOOL SPIFMD_EraseBlock(BLOCK_ID blockID);
DWORD SPIFMD_GetBlockStatus(BLOCK_ID blockID);
BOOL SPIFMD_SetBlockStatus(BLOCK_ID blockID, DWORD dwStatus);

BOOL SPIFMD_ReadSector(SECTOR_ADDR startSectorAddr, LPBYTE pSectorBuff,
                      PSectorInfo pSectorInfoBuff, DWORD dwNumSectors);
BOOL SPIFMD_WriteSector(SECTOR_ADDR startSectorAddr, LPBYTE pSectorBuff,
                       PSectorInfo pSectorInfoBuff, DWORD dwNumSectors);

#endif      // __SPIFMD_H__

