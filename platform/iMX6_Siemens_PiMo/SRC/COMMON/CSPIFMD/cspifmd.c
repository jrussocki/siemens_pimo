//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//-----------------------------------------------------------------------------
//
// Copyright (C) 2007-2011, Freescale Semiconductor, Inc. All Rights Reserved.
// THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
// AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//-----------------------------------------------------------------------------
//
#include "cspifmd.h"

//-----------------------------------------------------------------------------
// External Functions
extern VOID OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX index, DDK_CLOCK_GATE_MODE mode);
extern VOID OALStall(UINT32 uSecs);

//-----------------------------------------------------------------------------
// External Variables
extern BSP_ARGS *g_pBSPArgs;

//-----------------------------------------------------------------------------
// Defines

//port that SPI Flash is connected to
#define SPIFMD_PORT     CSP_BASE_REG_PA_ECSPI1
//SS signal that SPI Flash is connected to
#define SPIFMD_SS       CSPI_CONTROLREG_CHANNELSELECT_1
//from Flash datasheet, Flash supports modes 0 and 3
#define SPIFMD_PHA      CSPI_CONFIGREG_SCLKPHA_PHASE0
#define SPIFMD_CPOL     CSPI_CONFIGREG_SCLKPOL_ACTIVEHIGH
//burst length of 8 bits
#define SPIFMD_BURST_LENGTH 8-1

#define SPIFMD_FREQ     20000000 // 20MHz

#define SPIFMD_FIFOENTRY 64

//-----------------------------------------------------------------------------
// Types


//-----------------------------------------------------------------------------
// Global Variables

//-----------------------------------------------------------------------------
// Local Variables
static PCSP_ECSPI_REG g_pCSPI;
static DWORD g_dwCSPICfg;

//-----------------------------------------------------------------------------
// Local Functions
//static DWORD CalcClockDiv(DWORD dwDesiredFreq, DWORD dwControllerFreq);
static VOID CSPI_ClockEnable(BOOL bEnable);
static VOID CSPI_GpioMux();
static BOOL TransactMessage(LPDWORD pbyTxBuf, LPDWORD pbyRxBuf, DWORD dwMsgLen);
static BOOL WriteInProgress();
static BOOL FlashIsWriteable();
static BOOL WriteEnable();
static BOOL ReadSR(LPDWORD pdwSR);
static BOOL PageSetting();

//-----------------------------------------------------------------------------
//
//  Function: CalcClockDiv
//
//  This function calculates the divisor used to divide down the controller clock to yield the data bit clock
//
//  Parameters:
//      dwDesiredFreq 
//          [in] Desired Frequency for bit clock of SPI bus (in Hz).
//      dwControllerFreq 
//          [in] Current clock frequency gated into controller (in Hz).
//
//  Returns:  
//      Returns divisor to be used for SPI Data Rate Control bits.
//
//-----------------------------------------------------------------------------
#if 0
static DWORD CalcClockDiv(DWORD dwDesiredFreq, DWORD dwControllerFreq)
{
    DWORD dwDesiredDivisor = dwControllerFreq / dwDesiredFreq;
    DWORD dwCurDivisor = 4;
    DWORD dwDivisorBitCode = CSPI_CONREG_DIV4;

    //divisor has to be power of 2 starting at 4, assuming actual frequency has to be less than dwDesiredFreq
    for(dwDivisorBitCode = CSPI_CONREG_DIV4; dwDivisorBitCode <= CSPI_CONREG_DIV512; dwDivisorBitCode++)
    {
        //located smallest valid divisor that will generate the bit clock closest to dwDesiredFreq
        if(dwCurDivisor >= dwDesiredDivisor)
        {
            return dwDivisorBitCode;
        }
        dwCurDivisor *= 2;
    }

    //max out at 512
    return CSPI_CONREG_DIV512;
}
#endif

//-----------------------------------------------------------------------------
//
//  Function: CSPI_ClockEnable
//
//  This function eanbles/disables the clock into a CSPI controller
//
//  Parameters:
//      bEnable 
//          [in] If TRUE then enable clock, else disable clock.
//
//  Returns:  
//      none.
//
//-----------------------------------------------------------------------------
static VOID CSPI_ClockEnable(BOOL bEnable)
{
#if 1   // MX6_BRING_UP
    // CCM related, to rewrite
    // NO IPG CLK in MX6
#else
#if SPIFMD_PORT == CSP_BASE_REG_PA_ECSPI1
    OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_ECSPI1_IPG, (bEnable ? DDK_CLOCK_GATE_MODE_ENABLED_RUN : DDK_CLOCK_GATE_MODE_DISABLED));
    OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_ECSPI1_PERCLK, (bEnable ? DDK_CLOCK_GATE_MODE_ENABLED_RUN : DDK_CLOCK_GATE_MODE_DISABLED));
#endif
#endif
}


//-----------------------------------------------------------------------------
//
//  Function: CSPI_GpioMux
//
//  This function muxes the appropriate GPIO for the CSPI controller being used (Note, for each board there may exist a different GPIO muxing scheme,
//  this function may need to be altered for a different board).
//
//  Parameters:
//      bEnable 
//          [in] If TRUE then enable clock, else disable clock.
//
//  Returns:  
//      none.
//
//-----------------------------------------------------------------------------
static VOID CSPI_GpioMux()
{
//    PCSP_IOMUX_REGS pIOMUX = (PCSP_IOMUX_REGS) OALPAtoUA(CSP_BASE_REG_PA_IOMUXC);

#if SPIFMD_PORT == CSP_BASE_REG_PA_ECSPI1
    // TODO:  Pin conflict on ARD with LAN9220
#if 0
    //eCSPI1_SCLK
    OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_D16, DDK_IOMUX_PIN_MUXMODE_ALT4, DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_D16, DDK_IOMUX_PAD_SLEW_SLOW, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_NONE, DDK_IOMUX_PAD_HYSTERESIS_ENABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);
    OAL_IOMUX_SELECT_INPUT(pIOMUX,DDK_IOMUX_SELECT_INPUT_ECSPI1_IPP_CSPI_CLK_IN,3);

    // SS0 and SS1 need to be reconfigured to workaround ENGcm08720
    // Now SS0 is set to GPIO and SS1 is set to eCSPI1 SS1
    OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_EB2, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_EB2, DDK_IOMUX_PAD_SLEW_FAST, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_KEEPER, DDK_IOMUX_PAD_HYSTERESIS_ENABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);
    //eCSPI1_SS1
    OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_D19, DDK_IOMUX_PIN_MUXMODE_ALT4, DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_D19, DDK_IOMUX_PAD_SLEW_SLOW, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_NONE, DDK_IOMUX_PAD_HYSTERESIS_ENABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);
    OAL_IOMUX_SELECT_INPUT(pIOMUX,DDK_IOMUX_SELECT_INPUT_ECSPI1_IPP_IND_SS_B_1,2);

    //eCSPI1_MISO
    OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_D17, DDK_IOMUX_PIN_MUXMODE_ALT4, DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_D17, DDK_IOMUX_PAD_SLEW_SLOW, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_NONE, DDK_IOMUX_PAD_HYSTERESIS_ENABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);
    OAL_IOMUX_SELECT_INPUT(pIOMUX,DDK_IOMUX_SELECT_INPUT_ECSPI1_IPP_IND_MISO,3);

    //eCSPI1_MOSI
    OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_EIM_D18, DDK_IOMUX_PIN_MUXMODE_ALT4, DDK_IOMUX_PIN_SION_REGULAR);
    OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_EIM_D18, DDK_IOMUX_PAD_SLEW_SLOW, DDK_IOMUX_PAD_DRIVE_HIGH, DDK_IOMUX_PAD_OPENDRAIN_DISABLE, DDK_IOMUX_PAD_PULL_NONE, DDK_IOMUX_PAD_HYSTERESIS_ENABLE, DDK_IOMUX_PAD_VDOEN_NULL, DDK_IOMUX_PAD_OUTVOLT_NULL);
    OAL_IOMUX_SELECT_INPUT(pIOMUX,DDK_IOMUX_SELECT_INPUT_ECSPI1_IPP_IND_MOSI,3);
#endif
#elif SPIFMD_PORT == CSP_BASE_REG_PA_CSPI2
    //board currently does not use CSPI 2 (pins are muxed for different peripherals)
#elif SPIFMD_PORT == CSP_BASE_REG_PA_CSPI3
    //board currently does not use CSPI 3 (pins are muxed for different peripherals)
#endif
}

//-----------------------------------------------------------------------------
//
//  Function: PrepareBuffer
//
//  This function prepares a buffer so that the least significant byte will get shifted out first. This function is 
//  neccessary to use instead of a memcpy because the hardware fifos can only be loaded DWORD at a time, and will 
//  shift it out MSB first. This is an issue when you have to transfer multiple bytes in one exchange
//  without deasserting the chip select, as in the case when dealing with the eeprom.
//
//  Parameters:
//        pOutBuffer
//          [in] Pointer to output buffer
//        pbyRxBpInBufferuf
//          [in] Pointer to input buffer
//        dwSize
//          [in] Length of data to copy
//
//  Returns:
//        TRUE if transaction was  successful, else return FALSE
//
//-----------------------------------------------------------------------------
void PrepareBuffer(PVOID pOutBuffer, PVOID pInBuffer, DWORD dwSize)
{
    DWORD i = 0;
    PBYTE pIn = (PBYTE) pInBuffer;
    PBYTE pOut = (PBYTE) pOutBuffer;

    for(i = 0; i < dwSize; i = i+4)
    {
        if((dwSize - i) < 4)
        {
            switch (dwSize - i)
            {
            case 1:
                pOut[i] = pIn[i];
                break;

            case 2:
                pOut[i] = pIn[i+1];
                pOut[i+1] = pIn[i];
                break;

            case 3:
                pOut[i] = pIn[i+2];
                pOut[i+1] = pIn[i+1];
                pOut[i+2] = pIn[i];
                break;

            default:
                break;
            }
        }
        else
        {
            pOut[i] = pIn[i+3];
            pOut[i+1] = pIn[i+2];
            pOut[i+2] = pIn[i+1];
            pOut[i+3] = pIn[i];
        }
    }
}



//-----------------------------------------------------------------------------
//
//  Function: TransactMessage
//
//  This function performs the SPI transactions. Inspired by COMMON_FSL_V2 CSPI driver code.
//
//  Parameters:
//        pbyTxBuf
//          [in] Pointer to transmit buffer
//        pbyRxBuf
//          [in] Pointer to receive buffer
//        dwMsgLen
//          [in] Length of TX/RX buffer in units of message words (in this case bytes)
//
//  Returns:
//        TRUE if transaction was  successful, else return FALSE
//
//-----------------------------------------------------------------------------
static BOOL TransactMessage(LPDWORD pdwTxBuf, LPDWORD pdwRxBuf, DWORD dwMsgLen)
{
    enum {
        LOAD_TXFIFO, 
        CONTINUE_XCHG, 
        FETCH_RXFIFO
    } xchState;

    DWORD dwTxCnt = 0;
    DWORD dwRxCnt = 0;
    DWORD dwTmpData = 0;
    LPDWORD pdwTxPtr = pdwTxBuf;
    LPDWORD pdwRxPtr = pdwRxBuf;
    DWORD dwTemp = 0;
    BOOL bXchDone = FALSE;
    //DWORD i;

    if(pdwTxBuf == NULL)
    {
        return FALSE;
    }

    if(dwMsgLen > SPIFMD_FIFOENTRY*4)
    {
            OALMSG(TRUE, (_T("Trans: Trans length is larger than FIFO length! \r\n")));
        return FALSE;
    }

    //dwMsgLen will be the number of bytes, we need the number of bits. multiply by 8.
    INSREG32(&g_pCSPI->CONTROLREG, CSP_BITFMASK(CSPI_CONTROLREG_BURSTLENGTH),
        CSP_BITFVAL(CSPI_CONTROLREG_BURSTLENGTH, dwMsgLen*8-1));
    
    INSREG32(&g_pCSPI->CONTROLREG, CSP_BITFMASK(CSPI_CONTROLREG_EN),
        CSP_BITFVAL(CSPI_CONTROLREG_EN, CSPI_CONTROLREG_EN_ENABLE));
    

    xchState = LOAD_TXFIFO;

    while(!bXchDone)
    {
xch_loop:
        switch (xchState)
        {
            //initial load of TX FIFO, also starts transaction
            case LOAD_TXFIFO: 
                while ((!(INREG32(&g_pCSPI->STATREG) & CSP_BITFMASK(CSPI_STATREG_TF)))
                    && (dwTxCnt < dwMsgLen))
                //for (i=0;i<dwTemp;i++)
                {
                        // put next Tx data into CSPI FIFO
                        OUTREG32(&g_pCSPI->TXDATA, *pdwTxPtr);

                        // increment Tx Buffer to next data point
                        pdwTxPtr++;

                        // increment Tx exchange counter
                        dwTxCnt = dwTxCnt + sizeof(DWORD);
                }
                
                // start exchange
                INSREG32(&g_pCSPI->CONTROLREG, CSP_BITFMASK(CSPI_CONTROLREG_XCH), 
                    CSP_BITFVAL(CSPI_CONTROLREG_XCH, CSPI_CONTROLREG_XCH_EN));

                //xchState = (dwTxCnt < dwMsgLen) ? CONTINUE_XCHG : FETCH_RXFIFO;
                xchState = FETCH_RXFIFO;
                //break;
            //reads out RX FIFO after all TX data has been sent
            case FETCH_RXFIFO:
                

                // wait until transaction is complete 
                while (!(INREG32(&g_pCSPI->STATREG) & CSP_BITFMASK(CSPI_STATREG_TC)));

                OUTREG32(&g_pCSPI->STATREG,CSP_BITFMASK(CSPI_STATREG_TC));

                // Fetch all remaining rxdata in RXFIFO
                while ((INREG32(&g_pCSPI->STATREG) & CSP_BITFMASK(CSPI_STATREG_RR))
                         && dwRxCnt < dwMsgLen)
                {
                    dwTmpData = INREG32(&g_pCSPI->RXDATA);

                    // if receive data is not to be discarded
                    if (pdwRxBuf != NULL)
                    {
                        // get next Rx data from CSPI FIFO
                        *pdwRxPtr = dwTmpData;
                        // increment Rx Buffer to next data point
                        pdwRxPtr++;
                        
                    }

                    // increment Rx exchange counter
                    dwRxCnt = dwRxCnt + sizeof(DWORD);
                }

                if(dwTxCnt >= dwMsgLen)
                {
                    bXchDone = TRUE;
                }
                else
                {
                    xchState = LOAD_TXFIFO;
                }

                break;
            //enters this state if TX Buf has more data to send than what the initial load TX FIFO code could handle,
            //also reads from RX FIFO.
            case CONTINUE_XCHG:
                if (dwTxCnt >= dwMsgLen)
                {
                    xchState = FETCH_RXFIFO;
                    break;
                }
            
                // wait until RX FIFO Ready
                dwTemp = INREG32(&g_pCSPI->STATREG);
                while (!(dwTemp & CSP_BITFMASK(CSPI_STATREG_RR)))
                {
                    ////if the transfer completed and TxFifo is full then set transmit again
                    //if((dwTemp & CSP_BITFMASK(CSPI_STATREG_TC)) && (dwTemp & CSP_BITFMASK(CSPI_STATREG_TF)))
                    //{
                    //    // start exchange
                    //    INSREG32(&g_pCSPI->CONFIGREG, CSP_BITFMASK(CSPI_CONFIGREG_XCH), 
                    //    CSP_BITFVAL(CSPI_CONFIGREG_XCH, CSPI_CONFIGREG_XCH_EN));
                    //}
            
                    //if the transfer completed that means CSPI hardware run faster than CPU test it
                    //we need take all RXFIFO and go back to check if all data are actually sent.
                    if(dwTemp & CSP_BITFMASK(CSPI_STATREG_TC))
                    {
                        xchState = FETCH_RXFIFO;
                        goto xch_loop;
                    }
                    dwTemp = INREG32(&g_pCSPI->STATREG);
            
                }
                dwTmpData = INREG32(&g_pCSPI->RXDATA);
            
                // if receive data is not to be discarded
                if (pdwRxBuf != NULL)
                {
                    // get next Rx data from CSPI FIFO
                    *pdwRxPtr = dwTmpData;
            
                    // increment Rx Buffer to next data point
                    pdwRxPtr++;
                }
            
                // increment Rx exchange counter
                dwRxCnt = dwRxCnt + sizeof(DWORD);
            
                OUTREG32(&g_pCSPI->TXDATA, *pdwTxPtr);
            
                // increment Tx Buffer to next data point
                pdwTxPtr++;
            
                // increment Tx exchange counter
                dwTxCnt = dwTxCnt + sizeof(DWORD);
            
                //CSPI Driver here checks if Transaction Complete flag is set, this situation show not occur in this code here
                //since there should always be data in the TX FIFO so Transaction COmplete flag should not be detected here.
                //COMMENT b06505: the condition does occur when CPU loading TX FIFO is slower than CSPI hardware exchanging data. 
                if (INREG32(&g_pCSPI->STATREG) & CSP_BITFMASK(CSPI_STATREG_TC))
                {
                    xchState = FETCH_RXFIFO;
                }
                break;
        }
    }

    INSREG32(&g_pCSPI->CONTROLREG, CSP_BITFMASK(CSPI_CONTROLREG_EN),
        CSP_BITFVAL(CSPI_CONTROLREG_EN, CSPI_CONTROLREG_EN_DISABLE));
  
    return TRUE;
}


//-----------------------------------------------------------------------------
//
//  Function: ReadSR
//
//  This function reads the status register from the SPI Flash chip.
//
//  Parameters:
//        pdwSR
//          [in] Pointer to receive buffer
//
//  Returns:
//        TRUE if read is successful, else return FALSE
//
//-----------------------------------------------------------------------------
static BOOL ReadSR(LPDWORD pdwSR)
{
    BOOL bRet = FALSE;
    DWORD dwTxBuf = (DWORD)(RDSR_CMD << 8*3);
    DWORD dwRxBuf = 0;

    TransactMessage(&dwTxBuf, &dwRxBuf, sizeof(dwTxBuf));

    dwRxBuf <<= 8;

    OALMSG(FALSE, (_T("ReadSR: Status Register = 0x%x \r\n"), dwRxBuf));
    //if SR is valid then return true
    if((dwRxBuf))
    {
        *pdwSR = dwRxBuf;
        bRet = TRUE;
    }

    return bRet;
}

//-----------------------------------------------------------------------------
//
//  Function: WriteDone
//
//  This function checks if there is a write in progress on the SPI Flash chip.
//
//  Parameters:
//        None.
//
//  Returns:
//        TRUE if Write is done, else return FALSE
//
//-----------------------------------------------------------------------------
static BOOL WriteDone()
{
    DWORD dwCnt = 0;
    DWORD dwRxBuf = 0;

    while(dwCnt < WIP_TIMEOUT)
    {

        //if write done then return true
        if(ReadSR(&dwRxBuf))
        {
            if (dwRxBuf & SR_RDY) return TRUE;
        }
        OALStall(ONE_MS);
        dwCnt++;
    }
    
    return FALSE;
}

//-----------------------------------------------------------------------------
//
//  Function: FlashIsWriteable
//
//  This function waits until Flash is in Writeable state (i.e WIP bit is not set) or timeout is reached.
//
//  Parameters:
//        None.
//
//  Returns:
//        TRUE if Flash goes to a writeable state before the timeout is reached, else return FALSE
//
//-----------------------------------------------------------------------------
static BOOL FlashIsWriteable()
{
    DWORD dwCnt = 0;
    DWORD dwRxBuf = 0;

    while(dwCnt < WIP_TIMEOUT)
    {
        if(ReadSR(&dwRxBuf))
        {
            if (dwRxBuf & SR_PROTECT)
                return FALSE;
            else
                return TRUE;
        }
        OALStall(ONE_MS);
        dwCnt++;
    }

    return FALSE;
}

//-----------------------------------------------------------------------------
//
//  Function: WriteEnable
//
//  This function sets the Flash Chip to be in the Write Enable condition. Note
//  this has to be set at Power-up, after a Program/Erase instruction, Write Status
//  Register 
//
//  Parameters:
//        None.
//
//  Returns:
//        TRUE if Write Enable command could be sent out and if Flash is returned to a writeable state before a timeout,
//        else return FALSE
//
//-----------------------------------------------------------------------------
static BOOL WriteEnable()
{
    DWORD dwPSPBuf[17] = {PSP_CMD,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    DWORD dwTxBuf;

    if(FlashIsWriteable())
    {
        return TRUE;
    }

    dwTxBuf = ErSP_CMD;

    // Issue Erase Sector Protection Command first
    TransactMessage(&dwTxBuf, NULL, sizeof(dwTxBuf));

    // Make sure previous operation is done
    if (!WriteDone())
    {
        OALMSG(TRUE, (_T("WriteEnable: CMD ErSP is still in progress! \r\n")));
        return FALSE;
    }

    //Write Program Sector Protection command
    TransactMessage(dwPSPBuf, NULL, sizeof(dwPSPBuf));

    // Make sure previous operation is done
    if (!WriteDone())
    {
        OALMSG(TRUE, (_T("WriteEnable: CMD PSP is still in progress! \r\n")));
        return FALSE;
    }

    dwTxBuf = DSP_CMD;

    // Write Disable Sector Protection command
    TransactMessage(&dwTxBuf,NULL,sizeof(dwTxBuf));

    // Make sure previous operation is done
    if (!WriteDone())
    {
        OALMSG(TRUE, (_T("WriteEnable: CMD DSP is still in progress! \r\n")));
        return FALSE;
    }

    if(!FlashIsWriteable())
    {
        return FALSE;
    }

    return TRUE;
}

//-----------------------------------------------------------------------------
//
//  Function: PageSetting
//
//  This function set the page size from 528 to 512 if needed.
//
//  Parameters:
//        None
//
//  Returns:
//        TRUE if page size is 512 Bytes, else return FALSE
//
//-----------------------------------------------------------------------------
static BOOL PageSetting()
{
    BOOL bRet = FALSE;
    DWORD dwTxBuf = PCR_CMD;
    DWORD dwRxBuf = 0;

    if(ReadSR(&dwRxBuf))
    {
        if (dwRxBuf & SR_PAGESIZE) 
        {
            OALMSG(TRUE, (_T("Page Size is 512 Bytes! \r\n")));
            return TRUE;
        }
        else
        {
            OALMSG(TRUE, (_T("Warning: Unsupported Page Size of 528 Bytes! \r\n")));
            OALMSG(TRUE, (_T("Reprogramming the Page Size is in progress. \r\n")));
            OALMSG(TRUE, (_T("Do NOT Power Down the board! \r\n")));
            OALMSG(TRUE, (_T("...... \r\n")));
            TransactMessage(&dwTxBuf, NULL, sizeof(dwTxBuf));
            
            // Make sure previous operation is done
            if (!WriteDone())
            {
                OALMSG(TRUE, (_T("PageSetting: CMD PCR is still in progress! \r\n")));
                return FALSE;
            }
            
            OALMSG(TRUE, (_T("Reprogrammed the Page Size to be 512 Bytes! \r\n")));
            OALMSG(TRUE, (_T("Please Power Cycle the Board ! \r\n")));
            OALMSG(TRUE, (_T("SpinForever... \r\n")));
            for(;;);
        }
    }
        

    return bRet;
}

//-----------------------------------------------------------------------------
//
//  Function: SPIFMD_Init
//
//  This function initializes the SPI Flash interface.
//
//  Parameters:
//        None.
//
//  Returns:
//        None.
//
//-----------------------------------------------------------------------------
BOOL SPIFMD_Init(void)
{  
    BOOL bInit = TRUE;
    DWORD dwTxBuf = 0;
    DWORD dwRxBuf = 0;
    //BSP_ARGS *pArgs = (BSP_ARGS *)IMAGE_SHARE_ARGS_UA_START; 
    static BYTE pRxBuff[SPIFMD_SECTOR_SIZE];
    static BYTE pTxBuff[SPIFMD_SECTOR_SIZE];
    BOOL TEST = FALSE;


    g_pCSPI= (PCSP_ECSPI_REG)OALPAtoUA(SPIFMD_PORT);

    //gate clock into controller
    CSPI_ClockEnable(TRUE);

    //mux GPIO pins
    CSPI_GpioMux();

    INSREG32(&g_pCSPI->CONTROLREG, CSP_BITFMASK(CSPI_CONTROLREG_EN),
        CSP_BITFVAL(CSPI_CONTROLREG_EN, CSPI_CONTROLREG_EN_DISABLE));
        
    // Configure the CSPI interface
    OUTREG32(&g_pCSPI->CONFIGREG,
        CSP_BITFVAL(CSPI_CONFIGREG_SCLKPHA, CSPI_CONFIGREG_SCLKPHA_PHASE0<<SPIFMD_SS)      |
        CSP_BITFVAL(CSPI_CONFIGREG_SCLKPOL, CSPI_CONFIGREG_SCLKPOL_ACTIVEHIGH<<SPIFMD_SS)  |
        CSP_BITFVAL(CSPI_CONFIGREG_SSBCTRL, CSPI_CONFIGREG_SSBCTRL_SINGLEBURST) |
        CSP_BITFVAL(CSPI_CONFIGREG_SSBPOL, CSPI_CONFIGREG_SSBPOL_ACTIVELOW<<SPIFMD_SS)    |
        CSP_BITFVAL(CSPI_CONFIGREG_DATACTL, CSPI_CONFIGREG_DATACTL_STAYHIGH)    |
        CSP_BITFVAL(CSPI_CONFIGREG_SCLKCTL, CSPI_CONFIGREG_SCLKCTL_STAYHIGH)    |
        CSP_BITFVAL(CSPI_CONFIGREG_HTLENGTH, 0x0));

    OUTREG32(&g_pCSPI->CONTROLREG, 
        CSP_BITFVAL(CSPI_CONTROLREG_EN, CSPI_CONTROLREG_EN_ENABLE)              |
        CSP_BITFVAL(CSPI_CONTROLREG_HW, CSPI_CONTROLREG_HW_HTMODE_DISABLE)      |
        CSP_BITFVAL(CSPI_CONTROLREG_XCH, CSPI_CONTROLREG_XCH_IDLE)              |
        CSP_BITFVAL(CSPI_CONTROLREG_SMC, CSPI_CONTROLREG_SMC_NORMAL_MODE)       |
        CSP_BITFVAL(CSPI_CONTROLREG_CHANNELMODE, CSPI_CONTROLREG_CHANNELMODE_MASTER<<SPIFMD_SS)|
        CSP_BITFVAL(CSPI_CONTROLREG_PREDIVIDER, g_pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_ECSPI] / SPIFMD_FREQ) |
        CSP_BITFVAL(CSPI_CONTROLREG_POSTDIVIDER, 0)                             |
        CSP_BITFVAL(CSPI_CONTROLREG_DRCTL, CSPI_CONTROLREG_DRCTL_DONTCARE)      |
        CSP_BITFVAL(CSPI_CONTROLREG_CHANNELSELECT, SPIFMD_SS)            |
        CSP_BITFVAL(CSPI_CONTROLREG_BURSTLENGTH, 32-1));

    OALMSG(FALSE, (_T("CONTROLREG = 0x%x\r\n"), INREG32(&g_pCSPI->CONTROLREG)));
    OALMSG(FALSE, (_T("CONFIGREG = 0x%x\r\n"), INREG32(&g_pCSPI->CONFIGREG)));
    
    
    //Read in RDID (4 Byte Transaction)
    dwTxBuf = (DWORD)(RDID_CMD << 8*3);
    TransactMessage(&dwTxBuf, &dwRxBuf, sizeof(dwTxBuf));

    dwRxBuf <<= 8;

    OALMSG(TRUE, (_T("SPIFMD_Init: Manufacturer ID = 0x%x \r\n"), dwRxBuf));
    //check Manufacturer ID, 1st RX buffer byte is dummy
    if( (dwRxBuf >> 8*3) != MANUFACTURER_ID)
    {
        OALMSG(TRUE, (_T("SPIFMD_Init: Manufacturer ID = 0x%x, Should be 0x%x \r\n"), dwRxBuf, MANUFACTURER_ID));
        bInit = FALSE;
        goto cleanup;
    }

    if(!PageSetting())
    {
        OALMSG(TRUE, (_T("SPIFMD_Init: Page Setting failed \r\n")));
        bInit = FALSE;
        goto cleanup;
    }

    if(!WriteEnable())
    {
        OALMSG(TRUE, (_T("SPIFMD_Init: WriteEnable failed \r\n")));
        bInit = FALSE;
        goto cleanup;
    }

    if(TEST)
    {
        DWORD i =0;

        //write
        for(i = 0; i < SPIFMD_SECTOR_SIZE; i++)
        {
            *(pTxBuff+i) = (BYTE)i;
        }
        OALMSG(TRUE, (_T("SPIFMD_Init: WRITING\r\n")));
        SPIFMD_WriteSector(0, pTxBuff,
                      NULL, 1);

        for(i = 0; i < SPIFMD_SECTOR_SIZE/4; i+=4)
        {
            OALMSG(FALSE, (_T("SPIFMD_Init: Reading Data 0x%x   0x%x   0x%x   0x%x\r\n"), \
                *((LPDWORD)pTxBuff + i), *((LPDWORD)pTxBuff + (i+1)), *((LPDWORD)pTxBuff + (i+2)), *((LPDWORD)pTxBuff + (i+3))));
        }

        //read again
        OALMSG(TRUE, (_T("SPIFMD_Init: Reset RxBuff\r\n")));
        memset(pRxBuff, 0x00, SPIFMD_SECTOR_SIZE);
        OALMSG(TRUE, (_T("SPIFMD_Init: READING\r\n")));
        SPIFMD_ReadSector(0, pRxBuff,
                      NULL, 1);
        

        for(i = 0; i < SPIFMD_SECTOR_SIZE/4; i+=4)
        {
            OALMSG(FALSE, (_T("SPIFMD_Init: Reading Data 0x%x   0x%x   0x%x   0x%x\r\n"), \
                *((LPDWORD)pRxBuff + i), *((LPDWORD)pRxBuff + (i+1)), *((LPDWORD)pRxBuff + (i+2)), *((LPDWORD)pRxBuff + (i+3))));
        }
        if(memcmp(pTxBuff, pRxBuff, SPIFMD_SECTOR_SIZE) != 0)
        {
            OALMSG(TRUE, (_T("\r\nBuffers are not the same\r\n")));
        }
        else
        {
            OALMSG(TRUE, (_T("\r\nBuffers are the same\r\n")));
        }
    }

    OALMSG(OAL_INFO, (_T("INFO: Initialized SPI Flash!\r\n")));

cleanup:
    return bInit;
}

//-----------------------------------------------------------------------------
//
//  Function: SPIFMD_Deinit
//
//  This function de-initializes the flash write enable (can still read).
//
//  Parameters:
//        None.
//
//  Returns:  
//        None.
//
//-----------------------------------------------------------------------------
BOOL SPIFMD_Deinit (void)
{
    //BYTE byTxBuf;
    BOOL bInit = TRUE;
    
    if(!FlashIsWriteable())
    {
        bInit = FALSE;
    }

    if(g_pCSPI)
    {
        g_pCSPI = NULL;
    }

    return bInit;
}


//-----------------------------------------------------------------------------
//
//  Function: SPIFMD_GetInfo
//
//  This function determines the size characteristics for the SPI Flash
//
//  Parameters:
//      pFlashInfo 
//          [out] A pointer to a structure that contains the size 
//          characteristics for the flash memory device.
//
//  Returns:  
//      Returns TRUE on success. Returns FALSE on failure.
//
//-----------------------------------------------------------------------------
BOOL SPIFMD_GetInfo(PFlashInfo pFlashInfo)
{
    if (!pFlashInfo)
        return(FALSE);

    pFlashInfo->flashType           = (FLASH_TYPE)SPIFMD;
    pFlashInfo->dwNumBlocks         = SPIFMD_BLOCK_CNT;
    pFlashInfo->wSectorsPerBlock    = SPIFMD_SECTOR_CNT;
    pFlashInfo->wDataBytesPerSector = SPIFMD_SECTOR_SIZE;
    pFlashInfo->dwBytesPerBlock     = SPIFMD_BLOCK_SIZE;

    return(TRUE);
}


//-----------------------------------------------------------------------------
//
//  Function: SPIFMD_GetBlockStatus
//
//  This function returns the status of a block.
//
//  Parameters:
//      blockID 
//          [in] The block number used to check status.
//
//  Returns:  
//      Flags to describe the status of the block.
//
//-----------------------------------------------------------------------------
DWORD SPIFMD_GetBlockStatus(BLOCK_ID blockID)
{
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(blockID);

    return(BLOCK_STATUS_UNKNOWN);
}


//-----------------------------------------------------------------------------
//
//  Function: SPIFMD_SetBlockStatus
//
//  This function sets the status of a block.
//
//  Parameters:
//      blockID 
//          [in] The block number used to set status. 
//
//      dwStatus
//          [in] The status value to set.
//
//  Returns:  
//      Returns TRUE on success. Returns FALSE on failure.
//
//-----------------------------------------------------------------------------
BOOL SPIFMD_SetBlockStatus(BLOCK_ID blockID, DWORD dwStatus)
{
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(blockID);
    UNREFERENCED_PARAMETER(dwStatus);

    return(TRUE);
}

//-----------------------------------------------------------------------------
//
// Function: SPIFMD_EraseBlock
//
// Function Erases the specified flash block. 
//
// Parameters:
//      blockID 
//          [in] block number to erase.
//
// Returns:  
//      TRUE/FALSE for success
//
//-----------------------------------------------------------------------------
BOOL SPIFMD_EraseBlock(BLOCK_ID blockID)
{
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(blockID);

    //WriteSector has build-in erase function
    return(TRUE);
}

//-----------------------------------------------------------------------------
//
// Function: SPIFMD_ReadSector
//
// Function Reads the requested sector data and metadata from the flash media. 
//
// Parameters:
//      startSectorAddr 
//          [in] starting sector address.
//
//      pSectorBuff 
//          [in] Ptr to  buffer that contains sector data read from flash.
//                 Set to NULL if  data is not needed.
//
//      pSectorInfoBuff 
//          [in] Buffer for an array of sector information structures.
//                 One sector    information entry for every sector to be read. 
//                 Set to NULL if not needed.
//
//      dwNumSectors 
//          [in] Number of sectors to read.
//
// Returns:  
//      TRUE/FALSE for success
//
//-----------------------------------------------------------------------------
BOOL SPIFMD_ReadSector(SECTOR_ADDR startSectorAddr, LPBYTE pSectorBuff,
                      PSectorInfo pSectorInfoBuff, DWORD dwNumSectors)
{
    BOOL read_status = TRUE;
    DWORD dwCurPage = 0;
    LPBYTE pCurPageBuf = pSectorBuff;
    DWORD dwCurAddr = startSectorAddr * SPIFMD_SECTOR_SIZE; //changed to 24-bit flat address (from sector address)
    DWORD dwNumOfPages = dwNumSectors * (SPIFMD_SECTOR_SIZE / SPIFMD_SECTOR_SIZE);
    //to make the buffer smaller we make reads only 128 +4 bytes length
    BYTE byTxBuf[SPIFMD_SECTOR_SIZE/4 + 4];
    BYTE byRxBuf[SPIFMD_SECTOR_SIZE/4 + 4];
    DWORD i;

    UNREFERENCED_PARAMETER(pSectorInfoBuff);

    byTxBuf[3] = CAREAD_CMD;
    for(dwCurPage = 0; dwCurPage  < dwNumOfPages; dwCurPage++)
    {
        for(i=0;i<4;i++)
        {
            byTxBuf[2] = (BYTE)((dwCurAddr+i*SPIFMD_SECTOR_SIZE/4) >> 16);
            byTxBuf[1] = (BYTE)((dwCurAddr+i*SPIFMD_SECTOR_SIZE/4) >> 8);
            byTxBuf[0] = (BYTE)((dwCurAddr+i*SPIFMD_SECTOR_SIZE/4));
            
            memset(&byTxBuf[4], 0x0, sizeof(byTxBuf)-4);

            TransactMessage((LPDWORD) byTxBuf, (LPDWORD) byRxBuf, sizeof(byTxBuf));

            //copy into sector buffer (first 4 read bytes are dummy)
            PrepareBuffer((pCurPageBuf+i*SPIFMD_SECTOR_SIZE/4), (byRxBuf + 4), SPIFMD_SECTOR_SIZE/4);
        }

        dwCurAddr += SPIFMD_SECTOR_SIZE;
        pCurPageBuf += SPIFMD_SECTOR_SIZE;
    }

//cleanup:
    return read_status;
}

//-----------------------------------------------------------------------------
//
// Function: SPIFMD_WriteSector
//
// Function Writes the requested sector data and metadata to the flash media. 
//
// Parameters:
//      startSectorAddr 
//          [in] starting physical sector address.
//
//      pSectorBuff 
//          [in] Ptr to  buffer that contains sector data to write.
//                 Set to NULL if  data is not needed.
//
//      pSectorInfoBuff 
//          [in] Buffer for an array of sector information structures.
//                 One sector    information entry for every sector to be written.
//                 Set to NULL if not needed. (Not currently used)
//
//      dwNumSectors 
//          [in] Number of sectors to write.
//
// Returns:  
//      TRUE/FALSE for success
//
//-----------------------------------------------------------------------------
BOOL SPIFMD_WriteSector(SECTOR_ADDR startSectorAddr, LPBYTE pSectorBuff,
                       PSectorInfo pSectorInfoBuff, DWORD dwNumSectors)
{
    BOOL write_status = TRUE;
    DWORD dwCurPage = 0;
    DWORD dwCurAddr = startSectorAddr * SPIFMD_SECTOR_SIZE; //changed to 24-bit flat address (from sector address)
    DWORD dwNumOfPages = dwNumSectors * (SPIFMD_SECTOR_SIZE / SPIFMD_SECTOR_SIZE);
    LPBYTE pCurPageBuf = pSectorBuff;
    BYTE byTxBuf[SPIFMD_SECTOR_SIZE/4 + 4]; //4 extra bytes for command + address
    DWORD dwTxBuf = (BMMPP_CMD << 8*3) | (startSectorAddr << 9); 
    DWORD i;

    UNREFERENCED_PARAMETER(pSectorInfoBuff);

    if(pSectorBuff == NULL)
    {
        write_status = FALSE;
        goto cleanup;
    }
    
    byTxBuf[3] = BW_CMD;

    //page programs for sector
    for(dwCurPage = 0; dwCurPage  < dwNumOfPages; dwCurPage++)
    {
        for(i=0;i<4;i++)
        {
            byTxBuf[2] = (BYTE)((dwCurAddr+i*SPIFMD_SECTOR_SIZE/4) >> 16);
            byTxBuf[1] = (BYTE)((dwCurAddr+i*SPIFMD_SECTOR_SIZE/4) >> 8);
            byTxBuf[0] = (BYTE)((dwCurAddr+i*SPIFMD_SECTOR_SIZE/4));

            PrepareBuffer((byTxBuf + 4), (pCurPageBuf+i*SPIFMD_SECTOR_SIZE/4), SPIFMD_SECTOR_SIZE/4);

            TransactMessage((LPDWORD)byTxBuf, NULL, sizeof(byTxBuf));

            // Make sure previous operation is done
            if (!WriteDone())
            {
                OALMSG(TRUE, (_T("SPIFMD_WriteSector: CMD BW is still in progress! \r\n")));
                write_status = FALSE;
                goto cleanup;
            }
        }
        
        dwTxBuf = (BMMPP_CMD << 8*3) | ((startSectorAddr+dwCurPage) << 9);
        TransactMessage(&dwTxBuf, NULL, sizeof(dwTxBuf));

        // Make sure previous operation is done
        if (!WriteDone())
        {
            OALMSG(TRUE, (_T("SPIFMD_WriteSector: CMD BMMPP is still in progress! \r\n")));
            write_status = FALSE;
            goto cleanup;
        }
        
        dwCurAddr += SPIFMD_SECTOR_SIZE;
        pCurPageBuf += SPIFMD_SECTOR_SIZE;
    }

cleanup:
    return write_status;
}
