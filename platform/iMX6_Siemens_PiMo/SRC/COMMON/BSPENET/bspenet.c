//-----------------------------------------------------------------------------
//
//  Copyright (C) 2012, Adeneo Embedded All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
// Copyright (C) 2008-2009 Freescale Semiconductor, Inc. All Rights Reserved.
// THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
// AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//-----------------------------------------------------------------------------
//
// File: bspdnet.c
//
// BSP-specific support for Ethernet KITL.
//
//-----------------------------------------------------------------------------
#include "bsp.h"

extern PVOID pv_HWregCLKCTRL;
//extern PCSP_PLL_BASIC_REGS g_pPLL_ENET;
extern VOID OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX index, DDK_CLOCK_GATE_MODE mode);

//------------------------------------------------------------------------------
// External Fuctions
VOID OALStall(UINT32 uSecs);

// GPIO defines for ethernet workaround
#define RGMII_nRST_GPIO_PORT			OALPAtoUA(CSP_BASE_REG_PA_GPIO1)
#define RGMII_nRST_GPIO_PIN_MASK		GPIO_PIN_MASK(25)
#define RGMII_nRST_PIN				DDK_IOMUX_PIN_ENET_CRS_DV
#define RGMII_nRST_PIN_MODE			DDK_IOMUX_PIN_MUXMODE_ALT5
#define RGMII_nRST_PAD				DDK_IOMUX_PAD_ENET_CRS_DV
#define RGMII_RXC_GPIO_PORT			OALPAtoUA(CSP_BASE_REG_PA_GPIO6)
#define RGMII_RXC_GPIO_PIN_MASK			GPIO_PIN_MASK(30)
#define RGMII_RD0_GPIO_PORT			OALPAtoUA(CSP_BASE_REG_PA_GPIO6)
#define RGMII_RD0_GPIO_PIN_MASK			GPIO_PIN_MASK(25)
#define RGMII_RD1_GPIO_PORT			OALPAtoUA(CSP_BASE_REG_PA_GPIO6)
#define RGMII_RD1_GPIO_PIN_MASK			GPIO_PIN_MASK(27)
#define RGMII_RD2_GPIO_PORT			OALPAtoUA(CSP_BASE_REG_PA_GPIO6)
#define RGMII_RD2_GPIO_PIN_MASK			GPIO_PIN_MASK(28)
#define RGMII_RD3_GPIO_PORT			OALPAtoUA(CSP_BASE_REG_PA_GPIO6)
#define RGMII_RD3_GPIO_PIN_MASK			GPIO_PIN_MASK(29)
#define RGMII_RX_CTL_GPIO_PORT			OALPAtoUA(CSP_BASE_REG_PA_GPIO6)
#define RGMII_RX_CTL_GPIO_PIN_MASK		GPIO_PIN_MASK(24)
#define RGMII_ENET_REF_CLK_GPIO_PORT		OALPAtoUA(CSP_BASE_REG_PA_GPIO1)
#define RGMII_ENET_REF_CLK_GPIO_PIN_MASK	GPIO_PIN_MASK(23)

//------------------------------------------------------------------------------
//
//  Function:  OALKitlGetENETDmaBuffer(void)
//
//  This function is called by ENET debug functions to get
//  the physical memory for ENET DMA.
//
//  Parameters:
//      None.
//
//  Returns:
//      Physical address of the buffer used for ENET DMA
//
//------------------------------------------------------------------------------
DWORD OALKitlGetENETDmaBuffer(void)
{
    return  IMAGE_SHARE_ENET_RAM_PA_START;
}

//------------------------------------------------------------------------------
//
//  Function:  OALKitlENETBSPInit(void)
//
//  This function is called by ENET debug functions to initialize ENET 
//  board-specific I/O signals and clocking.
//
//  Parameters:
//      None.
//
//  Returns:
//      None
//
//------------------------------------------------------------------------------
void OALKitlENETBSPInit(void)
{
    //PCSP_IOMUX_REGS pIOMUX;
    PCSP_GPIO_REGS pGPIO;

    
	//pIOMUX = (PCSP_IOMUX_REGS) OALPAtoUA(CSP_BASE_REG_PA_IOMUXC);
    //if (pIOMUX == NULL)
    //{
    //    OALMSG(TRUE, (L"OALKitlFECBSPInit: IOMUXC mapping failed!\r\n"));
    //    return;
    //}

    // Config PinMux for ENET
    //MDC
    //OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_ENET_MDC, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
    //OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_ENET_MDC,
    //                     DDK_IOMUX_PAD_SLEW_SLOW,
    //                     DDK_IOMUX_PAD_DRIVE_40_OHM,
    //                     DDK_IOMUX_PAD_SPEED_NULL,
    //                     DDK_IOMUX_PAD_ODT_NULL,
    //                     DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
    //                     DDK_IOMUX_PAD_PULL_DOWN_100K,
    //                     DDK_IOMUX_PAD_HYSTERESIS_NULL,
    //                     DDK_IOMUX_PAD_DDRINPUT_NULL,
    //                     DDK_IOMUX_PAD_DDRSEL_NULL);

    //MDIO
    //OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_ENET_MDIO, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
    //OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_ENET_MDIO,
    //                     DDK_IOMUX_PAD_SLEW_FAST,
    //                     DDK_IOMUX_PAD_DRIVE_40_OHM,
    //                     DDK_IOMUX_PAD_SPEED_MED_100MHZ,
    //                     DDK_IOMUX_PAD_ODT_NULL,
    //                     DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
    //                     DDK_IOMUX_PAD_PULL_DOWN_100K,
    //                     DDK_IOMUX_PAD_HYSTERESIS_NULL,
    //                     DDK_IOMUX_PAD_DDRINPUT_NULL,
    //                     DDK_IOMUX_PAD_DDRSEL_NULL);
    //OAL_IOMUX_SELECT_INPUT(pIOMUX, DDK_IOMUX_SELECT_INPUT_ENET_IPP_IND_MAC0_MDIO, 0);

    //RGMII TX
    //OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_RGMII_TXC, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
    //OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_RGMII_TXC,
    //                     DDK_IOMUX_PAD_SLEW_SLOW,
    //                     DDK_IOMUX_PAD_DRIVE_40_OHM,
    //                     DDK_IOMUX_PAD_SPEED_NULL,
    //                     DDK_IOMUX_PAD_ODT_NULL,
    //                     DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
    //                     DDK_IOMUX_PAD_PULL_DOWN_100K,
    //                     DDK_IOMUX_PAD_HYSTERESIS_NULL,
    //                     DDK_IOMUX_PAD_DDRINPUT_NULL,
    //                     DDK_IOMUX_PAD_DDRSEL_NULL);
	
    //OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_RGMII_TD0, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
    //OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_RGMII_TD0,
    //                     DDK_IOMUX_PAD_SLEW_SLOW,
    //                     DDK_IOMUX_PAD_DRIVE_40_OHM,
    //                     DDK_IOMUX_PAD_SPEED_NULL,
    //                     DDK_IOMUX_PAD_ODT_NULL,
    //                     DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
    //                     DDK_IOMUX_PAD_PULL_DOWN_100K,
    //                     DDK_IOMUX_PAD_HYSTERESIS_NULL,
    //                     DDK_IOMUX_PAD_DDRINPUT_NULL,
    //                     DDK_IOMUX_PAD_DDRSEL_NULL);

    //OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_RGMII_TD1, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
    //OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_RGMII_TD1,
    //                     DDK_IOMUX_PAD_SLEW_SLOW,
    //                     DDK_IOMUX_PAD_DRIVE_40_OHM,
    //                     DDK_IOMUX_PAD_SPEED_NULL,
    //                     DDK_IOMUX_PAD_ODT_NULL,
    //                     DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
    //                     DDK_IOMUX_PAD_PULL_DOWN_100K,
    //                     DDK_IOMUX_PAD_HYSTERESIS_NULL,
    //                     DDK_IOMUX_PAD_DDRINPUT_NULL,
    //                     DDK_IOMUX_PAD_DDRSEL_NULL);

    //OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_RGMII_TD2, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
    //OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_RGMII_TD2,
    //                     DDK_IOMUX_PAD_SLEW_SLOW,
    //                     DDK_IOMUX_PAD_DRIVE_40_OHM,
    //                     DDK_IOMUX_PAD_SPEED_NULL,
    //                     DDK_IOMUX_PAD_ODT_NULL,
    //                     DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
    //                     DDK_IOMUX_PAD_PULL_DOWN_100K,
    //                     DDK_IOMUX_PAD_HYSTERESIS_NULL,
    //                     DDK_IOMUX_PAD_DDRINPUT_NULL,
    //                     DDK_IOMUX_PAD_DDRSEL_NULL);

    //OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_RGMII_TD3, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
    //OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_RGMII_TD3,
    //                     DDK_IOMUX_PAD_SLEW_SLOW,
    //                     DDK_IOMUX_PAD_DRIVE_40_OHM,
    //                     DDK_IOMUX_PAD_SPEED_NULL,
    //                     DDK_IOMUX_PAD_ODT_NULL,
    //                     DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
    //                     DDK_IOMUX_PAD_PULL_DOWN_100K,
    //                     DDK_IOMUX_PAD_HYSTERESIS_NULL,
    //                     DDK_IOMUX_PAD_DDRINPUT_NULL,
    //                     DDK_IOMUX_PAD_DDRSEL_NULL);

    //OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_RGMII_TX_CTL, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
    //OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_RGMII_TX_CTL,
    //                    DDK_IOMUX_PAD_SLEW_SLOW,
    //                    DDK_IOMUX_PAD_DRIVE_40_OHM,
    //                    DDK_IOMUX_PAD_SPEED_NULL,
    //                    DDK_IOMUX_PAD_ODT_NULL,
    //                    DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
    //                    DDK_IOMUX_PAD_PULL_DOWN_100K,
    //                    DDK_IOMUX_PAD_HYSTERESIS_NULL,
    //                    DDK_IOMUX_PAD_DDRINPUT_NULL,
    //                    DDK_IOMUX_PAD_DDRSEL_NULL);

    //  RSTn
	//OAL_IOMUX_SET_MUX(pIOMUX, RGMII_nRST_PIN, RGMII_nRST_PIN_MODE, DDK_IOMUX_PIN_SION_REGULAR);
	//OAL_IOMUX_SET_PAD(pIOMUX, RGMII_nRST_PAD,
    //                 DDK_IOMUX_PAD_SLEW_SLOW,
    //                 DDK_IOMUX_PAD_DRIVE_40_OHM,
    //                 DDK_IOMUX_PAD_SPEED_NULL,
    //                 DDK_IOMUX_PAD_ODT_NULL,
    //                 DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
    //                 DDK_IOMUX_PAD_PULL_KEEPER,
    //                 DDK_IOMUX_PAD_HYSTERESIS_NULL,
    //                 DDK_IOMUX_PAD_DDRINPUT_NULL,
    //                 DDK_IOMUX_PAD_DDRSEL_NULL); MHX */
	pGPIO = (PCSP_GPIO_REGS)RGMII_nRST_GPIO_PORT;
	SETREG32(&pGPIO->GDIR, RGMII_nRST_GPIO_PIN_MASK);
	SETREG32(&pGPIO->DR, RGMII_nRST_GPIO_PIN_MASK);

	//  CLK_25M
	//OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_ENET_REF_CLK, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
	//OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_ENET_REF_CLK,
    //                 DDK_IOMUX_PAD_SLEW_NULL,
    //                 DDK_IOMUX_PAD_DRIVE_NULL,
    //                 DDK_IOMUX_PAD_SPEED_NULL,
    //                 DDK_IOMUX_PAD_ODT_NULL,
    //                 DDK_IOMUX_PAD_OPENDRAIN_NULL,
    //                 DDK_IOMUX_PAD_PULL_NULL,
    //                 DDK_IOMUX_PAD_HYSTERESIS_NULL,
    //                 DDK_IOMUX_PAD_DDRINPUT_NULL,
    //                 DDK_IOMUX_PAD_DDRSEL_NULL);

    /*
        RXD0 is PHYADDRESS0 (hardwired to 1)
        RXD1 is PHYADDRESS1 (hardwired to 0)
        LED_ACT is PHYADDRESS2 (hardwired to 0)
        PHYADDRESS[4:3] = 00b
	
        So, our PHYADDRESS is 0x1 (00001b)
    */

    //  RGMII RX
	//OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_RGMII_RXC, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
	//OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_RGMII_RXC,
    //                     DDK_IOMUX_PAD_SLEW_SLOW,
    //                     DDK_IOMUX_PAD_DRIVE_40_OHM,
    //                     DDK_IOMUX_PAD_SPEED_NULL,
    //                     DDK_IOMUX_PAD_ODT_NULL,
    //                     DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
    //                     DDK_IOMUX_PAD_PULL_DOWN_100K,
    //                     DDK_IOMUX_PAD_HYSTERESIS_NULL,
    //                     DDK_IOMUX_PAD_DDRINPUT_NULL,
    //                     DDK_IOMUX_PAD_DDRSEL_NULL);
	//OAL_IOMUX_SELECT_INPUT(pIOMUX, DDK_IOMUX_SELECT_INPUT_ENET_IPP_IND_MAC0_RXCLK,0);

	//OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_RGMII_RD0, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
    //OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_RGMII_RD0,
    //                    DDK_IOMUX_PAD_SLEW_SLOW,
    //                    DDK_IOMUX_PAD_DRIVE_40_OHM,
    //                    DDK_IOMUX_PAD_SPEED_NULL,
    //                    DDK_IOMUX_PAD_ODT_NULL,
    //                    DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
    //                    DDK_IOMUX_PAD_PULL_DOWN_100K,
    //                    DDK_IOMUX_PAD_HYSTERESIS_NULL,
    //                    DDK_IOMUX_PAD_DDRINPUT_NULL,
    //                    DDK_IOMUX_PAD_DDRSEL_NULL);
    //OAL_IOMUX_SELECT_INPUT(pIOMUX, DDK_IOMUX_SELECT_INPUT_ENET_IPP_IND_MAC0_RXDATA_0,0);

	//OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_RGMII_RD1, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
	//OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_RGMII_RD1,
    //                    DDK_IOMUX_PAD_SLEW_SLOW,
    //                    DDK_IOMUX_PAD_DRIVE_40_OHM,
    //                    DDK_IOMUX_PAD_SPEED_NULL,
    //                    DDK_IOMUX_PAD_ODT_NULL,
    //                    DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
    //                    DDK_IOMUX_PAD_PULL_DOWN_100K,
    //                    DDK_IOMUX_PAD_HYSTERESIS_NULL,
    //                    DDK_IOMUX_PAD_DDRINPUT_NULL,
    //                    DDK_IOMUX_PAD_DDRSEL_NULL);
	//OAL_IOMUX_SELECT_INPUT(pIOMUX, DDK_IOMUX_SELECT_INPUT_ENET_IPP_IND_MAC0_RXDATA_1,0);

	//OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_RGMII_RD2, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
	//OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_RGMII_RD2,
    //                    DDK_IOMUX_PAD_SLEW_SLOW,
    //                    DDK_IOMUX_PAD_DRIVE_40_OHM,
    //                    DDK_IOMUX_PAD_SPEED_NULL,
    //                    DDK_IOMUX_PAD_ODT_NULL,
    //                    DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
    //                    DDK_IOMUX_PAD_PULL_DOWN_100K,
    //                    DDK_IOMUX_PAD_HYSTERESIS_NULL,
    //                    DDK_IOMUX_PAD_DDRINPUT_NULL,
    //                    DDK_IOMUX_PAD_DDRSEL_NULL);
	//OAL_IOMUX_SELECT_INPUT(pIOMUX, DDK_IOMUX_SELECT_INPUT_ENET_IPP_IND_MAC0_RXDATA_2,0);

	//OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_RGMII_RD3, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
	//OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_RGMII_RD3,
    //                    DDK_IOMUX_PAD_SLEW_SLOW,
    //                    DDK_IOMUX_PAD_DRIVE_40_OHM,
    //                    DDK_IOMUX_PAD_SPEED_NULL,
    //                    DDK_IOMUX_PAD_ODT_NULL,
    //                    DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
    //                    DDK_IOMUX_PAD_PULL_DOWN_100K,
    //                    DDK_IOMUX_PAD_HYSTERESIS_NULL,
    //                    DDK_IOMUX_PAD_DDRINPUT_NULL,
    //                    DDK_IOMUX_PAD_DDRSEL_NULL);
	//OAL_IOMUX_SELECT_INPUT(pIOMUX, DDK_IOMUX_SELECT_INPUT_ENET_IPP_IND_MAC0_RXDATA_3,0);

	//OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_RGMII_RX_CTL, DDK_IOMUX_PIN_MUXMODE_ALT1, DDK_IOMUX_PIN_SION_REGULAR);
    //OAL_IOMUX_SET_PAD(pIOMUX, DDK_IOMUX_PAD_RGMII_RX_CTL,
    //                     DDK_IOMUX_PAD_SLEW_SLOW,
    //                     DDK_IOMUX_PAD_DRIVE_40_OHM,
    //                     DDK_IOMUX_PAD_SPEED_NULL,
    //                     DDK_IOMUX_PAD_ODT_NULL,
    //                     DDK_IOMUX_PAD_OPENDRAIN_DISABLE,
    //                     DDK_IOMUX_PAD_PULL_DOWN_100K,
    //                     DDK_IOMUX_PAD_HYSTERESIS_NULL,
    //                     DDK_IOMUX_PAD_DDRINPUT_NULL,
    //                     DDK_IOMUX_PAD_DDRSEL_NULL);
    //OAL_IOMUX_SELECT_INPUT(pIOMUX, DDK_IOMUX_SELECT_INPUT_ENET_IPP_IND_MAC0_RXEN,0);

}

//------------------------------------------------------------------------------
//
// Function:    BSPENETClockInit()
//
//------------------------------------------------------------------------------
VOID BSPENETClockInit()
{
    // Gate on ENET clocks in CLKCTRL
    PCSP_PLL_BASIC_REGS pPLL_ENET;
    PCSP_CCM_REGS pCCM;
    pPLL_ENET = (PCSP_PLL_BASIC_REGS) OALPAtoUA(CSP_BASE_REG_PA_PLL_ENET);
    if (pPLL_ENET== NULL)
    {
        OALMSG(TRUE, (L"BSPENETClockInit: pPLL_ENET OALPAtoUA mapping failed!\r\n"));
        return;
    } 
    
    pCCM = (PCSP_CCM_REGS) OALPAtoUA(CSP_BASE_REG_PA_CCM);
    if (pCCM == NULL)
    {
        OALMSG(TRUE, (L"BSPENETClockInit: pCCM OALPAtoUA mapping failed!\r\n"));
        return;
    } 

    INSREG32BF(&pPLL_ENET->CTRL, PLL_ENET_CTRL_ENABLE, 0x1);
    INSREG32BF(&pPLL_ENET->CTRL, PLL_ENET_CTRL_DIV_SELECT, 0x3);

    OALStall(1000);
    OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_ENET, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
}

//------------------------------------------------------------------------------
//
// Function:    EthernetIntf BSPENETInterface()
//
//
// Return Value:
//        This fuction returns ethernet interface type
//
//------------------------------------------------------------------------------
EthernetIntf BSPENETInterface()
{
    return RGMII_INTERFACE;
}

//------------------------------------------------------------------------------
//
// Function:    BOOL BSPENETEnableDBSWP()
//
//
// Return Value:
//        This fuction returns TRUE if need to enable descriptor byte swapping
//
//------------------------------------------------------------------------------
BOOL BSPENETEnableDBSWP()
{
    return TRUE;
}

//------------------------------------------------------------------------------
//
// Function:    BOOL BSPENETPhyConfig()
//
//
// Return Value:
//        This fuction returns TRUE if need extra phy config
//
//------------------------------------------------------------------------------
BOOL BSPENETPhyConfig()
{
    return TRUE;
}
