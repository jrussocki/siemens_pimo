//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//------------------------------------------------------------------------------
//
//  Copyright (C) 2007-2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------

#include <bsp.h>
#include "dvfs.h"

#define CLOCK_HOTFIX 0

PCSP_CCM_REGS g_pCCM = NULL;
PCSP_PLL_BASIC_REGS g_pPLL_CPU = NULL;
PCSP_PLL_BUS_REGS g_pPLL_BUS = NULL;
PCSP_PLL_BASIC_REGS g_pPLL_USBOTG = NULL;
PCSP_PLL_BASIC_REGS g_pPLL_USBHOST = NULL;
PCSP_PLL_MEDIA_REGS g_pPLL_AUD = NULL;
PCSP_PLL_MEDIA_REGS g_pPLL_VIDEO = NULL;
PCSP_PLL_BASIC_REGS g_pPLL_MLB = NULL;
PCSP_PLL_BASIC_REGS g_pPLL_ENET = NULL;
PCSP_PLL_BASIC_REGS g_pPLL_PFD_480MHZ = NULL;
PCSP_PLL_BASIC_REGS g_pPLL_PFD_528MHZ = NULL;

PDDK_CLK_CONFIG g_pDdkClkConfig = NULL;

//------------------------------------------------------------------------------
// Local Variables
//------------------------------------------------------------------------------
// Global Variables


PCSP_ANAMISC_REGS   g_AnaMiscRegs;


//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Functions

//------------------------------------------------------------------------------
//
//  Function:   OALBspGetClockFreq
//
//  This function returns clock settings from the global BSP args structure.
//
//  Parameters:
//      nClkId
//          [out] Clock ID.
//
//  Returns:
//      requested value.
//
//------------------------------------------------------------------------------

UINT32 OALBspGetClockFreq(UINT32 nClkId)
{
    UINT32 nRetVal = (UINT32)-1;

    if (g_pPLL_CPU == NULL)
    {
        g_pPLL_CPU = (PCSP_PLL_BASIC_REGS) OALPAtoUA(CSP_BASE_REG_PA_PLL_CPU);
    }

    if (g_pPLL_CPU != NULL)
    {
        BSP_ARGS *pBspArgs = (BSP_ARGS *)IMAGE_SHARE_ARGS_UA_START;

        switch( nClkId )
        {
        case DDK_CLOCK_SIGNAL_PLL1_CPU:
            nRetVal = pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL1_CPU];
            break;

        case DDK_CLOCK_SIGNAL_IPG:
            nRetVal = pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_IPG];
            break;

            // this returns the CPU/IPG divider!!
        case DDK_CLOCK_SIGNAL_PLL1_REF:
            nRetVal = EXTREG32BF(&g_pPLL_CPU->CTRL, PLL_CPU_CTRL_DIV_SELECT);
            break;
        }
    }

    return 0;
}

//------------------------------------------------------------------------------
//
//  Function:   OALBspArgsInit
//
//  This function initializes the parameters of the global BSP args structure.
//
//  Parameters:
//      pBSPArgs
//          [out] Points to BSP arguments structure to be updated.
//
//  Returns:
//      TRUE if boot successfully, otherwise returns FALSE.
//
//------------------------------------------------------------------------------
BOOL OALBspArgsInit(BSP_ARGS *pBSPArgs)
{
    BOOL rc = FALSE;
    UINT32 osc_clk, ckih_camp1_clk, ckih_camp2_clk;
    UINT32 pll_ref_clk, step_clk, periph_apm_clk, periph_main_clk, main_bus_clk;
    UINT32 clk, div;
    DDK_CLOCK_GATE_INDEX index;
    DDK_CLOCK_BAUD_SOURCE baudSrc;
    DDK_DVFC_DOMAIN domain;
    DDK_DVFC_SETPOINT setpoint;
    
    UNREFERENCED_PARAMETER(main_bus_clk);
    UNREFERENCED_PARAMETER(periph_apm_clk);
    UNREFERENCED_PARAMETER(periph_main_clk);
    UNREFERENCED_PARAMETER(ckih_camp2_clk);
    UNREFERENCED_PARAMETER(ckih_camp1_clk);
    UNREFERENCED_PARAMETER(step_clk);

    if (pBSPArgs == NULL)
    {
        goto cleanUp;
    }

    g_pCCM = (PCSP_CCM_REGS) OALPAtoUA(CSP_BASE_REG_PA_CCM);
    if (g_pCCM == NULL)
    {
        goto cleanUp;
    }

    g_pPLL_CPU = (PCSP_PLL_BASIC_REGS) OALPAtoUA(CSP_BASE_REG_PA_PLL_CPU);
    if (g_pPLL_CPU == NULL)
    {
        goto cleanUp;
    }

    g_pPLL_BUS = (PCSP_PLL_BUS_REGS) OALPAtoUA(CSP_BASE_REG_PA_PLL_BUS);
    if (g_pPLL_BUS == NULL)
    {
        goto cleanUp;
    }

    g_pPLL_USBOTG = (PCSP_PLL_BASIC_REGS) OALPAtoUA(CSP_BASE_REG_PA_PLL_USBOTG);
    if (g_pPLL_USBOTG == NULL)
    {
        goto cleanUp;
    }

    g_pPLL_USBHOST = (PCSP_PLL_BASIC_REGS) OALPAtoUA(CSP_BASE_REG_PA_PLL_USBHOST);
    if (g_pPLL_USBHOST == NULL)
    {
        goto cleanUp;
    }

    g_pPLL_AUD = (PCSP_PLL_MEDIA_REGS) OALPAtoUA(CSP_BASE_REG_PA_PLL_AUD);
    if (g_pPLL_AUD == NULL)
    {
        goto cleanUp;
    }

    g_pPLL_VIDEO = (PCSP_PLL_MEDIA_REGS) OALPAtoUA(CSP_BASE_REG_PA_PLL_VIDEO);
    if (g_pPLL_VIDEO == NULL)
    {
        goto cleanUp;
    }    

    g_pPLL_MLB = (PCSP_PLL_BASIC_REGS) OALPAtoUA(CSP_BASE_REG_PA_PLL_MLB);
    if (g_pPLL_MLB == NULL)
    {
        goto cleanUp;
    }    

    g_pPLL_ENET = (PCSP_PLL_BASIC_REGS) OALPAtoUA(CSP_BASE_REG_PA_PLL_ENET);
    if (g_pPLL_ENET == NULL)
    {
        goto cleanUp;
    } 

    g_pPLL_PFD_480MHZ= (PCSP_PLL_BASIC_REGS) OALPAtoUA(CSP_BASE_REG_PA_PLL_PFD_480MHZ);
    if (g_pPLL_PFD_480MHZ == NULL)
    {
        goto cleanUp;
    } 

    g_pPLL_PFD_528MHZ = (PCSP_PLL_BASIC_REGS) OALPAtoUA(CSP_BASE_REG_PA_PLL_PFD_528MHZ);
    if (g_pPLL_PFD_528MHZ == NULL)
    {
        goto cleanUp;
    }     

    g_pDdkClkConfig = (PDDK_CLK_CONFIG) IMAGE_WINCE_DDKCLK_RAM_UA_START;
    if (g_pDdkClkConfig == NULL)
    {
        goto cleanUp;
    }

    for (baudSrc = 0; baudSrc < DDK_CLOCK_BAUD_SOURCE_ENUM_END; baudSrc++)
    {
        // Initialize root clock references
        g_pDdkClkConfig->rootRefCount[baudSrc] = 0;
    }
    
    for (index = 0; index < DDK_CLOCK_GATE_INDEX_ENUM_END; index++)
    {
        // Initialize clock tree root/leaf references
        g_pDdkClkConfig->clkTreeNode[index].root = DDK_CLOCK_BAUD_SOURCE_GND;
        g_pDdkClkConfig->clkTreeNode[index].numLeaf = 0;

        // Initialize clock references
        g_pDdkClkConfig->clkRefInfo[index].disabledMode = DDK_CLOCK_GATE_MODE_DISABLED;
        g_pDdkClkConfig->clkRefInfo[index].enabledMode = DDK_CLOCK_GATE_MODE_ENABLED_ALL;
        g_pDdkClkConfig->clkRefInfo[index].refCount = 0;
        g_pDdkClkConfig->clkRefInfo[index].bHandshakeMask = 0;

        // Initialize peripheral setpoint requirements
        g_pDdkClkConfig->periphSetpointReq[index].mV = 0;
        g_pDdkClkConfig->periphSetpointReq[index].freq[DDK_DVFC_FREQ_AHB] = 0;
        g_pDdkClkConfig->periphSetpointReq[index].freq[DDK_DVFC_FREQ_AXI] = 0;
    }

    // Configure handshake masks to be applied for specific clock nodes when they are disabled
#if 1   // MX6_BRING_UP
    // CCM related, to rewrite
    g_pDdkClkConfig->clkRefInfo[DDK_CLOCK_GATE_INDEX_MMDC_CORE_IPG_P0].bHandshakeMask = CSP_BITFMASK(CCM_CCDR_MMDC_CH0_MASK);
    g_pDdkClkConfig->clkRefInfo[DDK_CLOCK_GATE_INDEX_MMDC_CORE_IPG_P1].bHandshakeMask = CSP_BITFMASK(CCM_CCDR_MMDC_CH1_MASK);

#else
    g_pDdkClkConfig->clkRefInfo[DDK_CLOCK_GATE_INDEX_EMI_FAST].bHandshakeMask = CSP_BITFMASK(CCM_CCDR_EMI_HS_FAST_MASK);
    g_pDdkClkConfig->clkRefInfo[DDK_CLOCK_GATE_INDEX_EMI_SLOW].bHandshakeMask = CSP_BITFMASK(CCM_CCDR_EMI_HS_SLOW_MASK);
    g_pDdkClkConfig->clkRefInfo[DDK_CLOCK_GATE_INDEX_EMI_INT1].bHandshakeMask = CSP_BITFMASK(CCM_CCDR_EMI_HS_INT1_MASK);
    g_pDdkClkConfig->clkRefInfo[DDK_CLOCK_GATE_INDEX_EMI_INT2].bHandshakeMask = CSP_BITFMASK(CCM_CCDR_EMI_HS_INT2_MASK);
    g_pDdkClkConfig->clkRefInfo[DDK_CLOCK_GATE_INDEX_IPU].bHandshakeMask = CSP_BITFMASK(CCM_CCDR_IPU_HS_MASK);
#endif

    for (domain = 0; domain < DDK_DVFC_DOMAIN_ENUM_END; domain++)
    {
        for (setpoint = 0; setpoint < DDK_DVFC_SETPOINT_ENUM_END; setpoint++)
        {
            // Initialize setpoint requests
            g_pDdkClkConfig->setpointReqCount[domain][setpoint] = 0;
        }

        // Initialize load tracking requests
        g_pDdkClkConfig->setpointLoad[domain] = DDK_DVFC_SETPOINT_ENUM_END-1;
    }
    // Initialize DVFC state variables
    g_pDdkClkConfig->bDvfcActive = FALSE;
    g_pDdkClkConfig->bSetpointPending = FALSE;
    g_pDdkClkConfig->setpointCur[DDK_DVFC_DOMAIN_CPU] = DDK_DVFC_SETPOINT_HIGH;
    g_pDdkClkConfig->setpointMax[DDK_DVFC_DOMAIN_CPU] = DDK_DVFC_SETPOINT_HIGH;
    g_pDdkClkConfig->setpointMin[DDK_DVFC_DOMAIN_CPU] = DDK_DVFC_SETPOINT_LOW;

    g_pDdkClkConfig->setpointCur[DDK_DVFC_DOMAIN_PERIPH] = DDK_DVFC_SETPOINT_HIGH2;
    g_pDdkClkConfig->setpointMax[DDK_DVFC_DOMAIN_PERIPH] = DDK_DVFC_SETPOINT_HIGH2;
    g_pDdkClkConfig->setpointMin[DDK_DVFC_DOMAIN_PERIPH] = DDK_DVFC_SETPOINT_LOW;

	 // ENET PLL
    {
        OUTREG32(&g_pPLL_ENET->CTRL_CLR, 0x1000);      // PWRDOWN -> 0
        OUTREG32(&g_pPLL_ENET->CTRL_CLR, 0x10000);   // BYPASS -> 0
        OUTREG32(&g_pPLL_ENET->CTRL_SET, 0x3);     // DIV -> 1

        OUTREG32(&g_pPLL_ENET->CTRL_SET, 0x2000);  // enable
        while (!(INREG32(&g_pPLL_ENET->CTRL) & 0x80000000)); // wait lock
    }


#if CLOCK_HOTFIX
    // ENET PLL
    {
        OUTREG32(&g_pPLL_ENET->CTRL_CLR, 0x1000);      // PWRDOWN -> 0
        OUTREG32(&g_pPLL_ENET->CTRL_CLR, 0x10000);   // BYPASS -> 0
        OUTREG32(&g_pPLL_ENET->CTRL_SET, 0x1);     // DIV -> 1

        OUTREG32(&g_pPLL_ENET->CTRL_SET, 0x2000);  // enable
        while (!(INREG32(&g_pPLL_ENET->CTRL) & 0x80000000)); // wait lock
    }

    // AUDIO PLL
    {
        OUTREG32(&g_pPLL_AUD->CTRL_CLR, 0x1000);      // PWRDOWN -> 0
        OUTREG32(&g_pPLL_AUD->CTRL_CLR, 0x10000);   // BYPASS -> 0
        OUTREG32(&g_pPLL_AUD->CTRL_SET, 0x1);     // DIV -> 1

        OUTREG32(&g_pPLL_AUD->CTRL_SET, 0x2000);  // enable
        while (!(INREG32(&g_pPLL_AUD->CTRL) & 0x80000000)); // wait lock
    }

    // VIDEO PLL
    {
        OUTREG32(&g_pPLL_VIDEO->CTRL_CLR, 0x1000);      // PWRDOWN -> 0
        OUTREG32(&g_pPLL_VIDEO->CTRL_CLR, 0x10000);   // BYPASS -> 0
        OUTREG32(&g_pPLL_VIDEO->CTRL_SET, 0x1);     // DIV -> 1

        OUTREG32(&g_pPLL_VIDEO->CTRL_SET, 0x2000);  // enable
        while (!(INREG32(&g_pPLL_VIDEO->CTRL) & 0x80000000)); // wait lock
    }


#endif

    if (INREG32(&g_pCCM->CCR) & CSP_BITFMASK(CCM_CCR_COSC_EN))
    {
        osc_clk = BSP_CLK_OSC_FREQ;
    }
    else
    {
        osc_clk = 0;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_OSC] = osc_clk;


#if 1   // MX6_BRING_UP
    // CCM related, to rewrite
    // What does CKIH and CKIH2 mean in MX6? to be examined
#else
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_CKIH] = BSP_CLK_CKIH_FREQ;
    if (INREG32(&g_pCCM->CCR) & CSP_BITFMASK(CCM_CCR_CAMP1_EN))
    {
        ckih_camp1_clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_CKIH];
    }
    else
    {
        ckih_camp1_clk = 0;
    }

    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_CKIH2] = BSP_CLK_CKIH2_FREQ;
    if (INREG32(&g_pCCM->CCR) & CSP_BITFMASK(CCM_CCR_CAMP2_EN))
    {
        ckih_camp2_clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_CKIH2];
    }
    else
    {
        ckih_camp2_clk = 0;
    }

    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_CKIH] = ckih_camp1_clk;
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_CKIH2] = ckih_camp2_clk;
#endif

    // Calculate PLL1 (CPU) frequency
    if (EXTREG32BF(&g_pPLL_CPU->CTRL, PLL_CPU_CTRL_ENABLE))
    {
        pll_ref_clk = BSP_CLK_OSC_FREQ;
        div = EXTREG32BF(&g_pPLL_CPU->CTRL, PLL_CPU_CTRL_DIV_SELECT);
        clk = (UINT32) ((pll_ref_clk * div) / (2));
    }
    else
    {
        clk = 0;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL1_CPU] = clk;

    // Calculate PLL2 (Bus) frequency
    if (EXTREG32BF(&g_pPLL_BUS->CTRL, PLL_BUS_CTRL_ENABLE))
    {
        pll_ref_clk = BSP_CLK_OSC_FREQ;
        if (EXTREG32BF(&g_pPLL_BUS->CTRL, PLL_BUS_CTRL_DIV_SELECT) == 1)
        {
            div = 22;
        }
        else
        {
            div = 20;
        }
        clk = (UINT32) (pll_ref_clk * div);    
    }
    else
    {
        clk = 0;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL2_BUS] = clk;

    // Calculate PLL3 (USBOTG) frequency
    if (EXTREG32BF(&g_pPLL_USBOTG->CTRL, PLL_USBOTG_CTRL_ENABLE))
    {
        pll_ref_clk = BSP_CLK_OSC_FREQ;
        if (EXTREG32BF(&g_pPLL_USBOTG->CTRL, PLL_USBOTG_CTRL_DIV_SELECT) == 1)
        {
            div = 22;
        }
        else
        {
            div = 20;
        }
        clk = (UINT32) (pll_ref_clk * div);    
    }
    else
    {
        clk = 0;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG] = clk;

    // Calculate USBHost frequency
    if (EXTREG32BF(&g_pPLL_USBHOST->CTRL, PLL_USBHOST_CTRL_ENABLE))
    {
        pll_ref_clk = BSP_CLK_OSC_FREQ;
        if (EXTREG32BF(&g_pPLL_USBHOST->CTRL, PLL_USBHOST_CTRL_DIV_SELECT) == 1)
        {
            div = 22;
        }
        else
        {
            div = 20;
        }
        clk = (UINT32) (pll_ref_clk * div);    
    }
    else
    {
        clk = 0;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL7_USBHOST] = clk;

    // Calculate PLL4 (Audio) frequency
    if (EXTREG32BF(&g_pPLL_AUD->CTRL, PLL_MEDIA_CTRL_ENABLE))
    {
        pll_ref_clk = BSP_CLK_OSC_FREQ;
        div = EXTREG32BF(&g_pPLL_AUD->CTRL, PLL_MEDIA_CTRL_DIV_SELECT);
        clk = (UINT32) ((pll_ref_clk * div) / (2));
    }
    else
    {
        clk = 0;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL4_AUDIO] = clk;

#if Fix_HDMI
    // Calculate PLL5 (Video) frequency
    if (EXTREG32BF(&g_pPLL_VIDEO->CTRL, PLL_MEDIA_CTRL_ENABLE))
    {
        UINT32 post_div = 1;
        UINT32 test_div_sel = 0;
        UINT32 control3 = 0;
        UINT32 mfn = 1;
        UINT32 mfd = 1;

        pll_ref_clk = BSP_CLK_OSC_FREQ;

#if 0
        UINT32 rev = mx6q_revision();
        if ( (rev >= IMX_CHIP_REVISION_1_1) || cpu_is_mx6dl() )
#endif
        {
            test_div_sel = EXTREG32BF(&g_pPLL_VIDEO->CTRL, PLL_MEDIA_CTRL_TEST_DIV_SELECT);
            if (test_div_sel == 0)
                post_div = 4;
            else if (test_div_sel == 1)
                post_div = 2;
            else
                post_div = 1;
            
            control3 = EXTREG32BF(&g_AnaMiscRegs->ANA_MISC2, ANALOG_MISC2_CONTROL3);
            if (control3 == 1)
                post_div *= 2;
            else if (control3 == 3)
                post_div *= 4;
        }

        div = EXTREG32BF(&g_pPLL_VIDEO->CTRL, PLL_MEDIA_CTRL_DIV_SELECT);
        mfn = EXTREG32BF(&g_pPLL_VIDEO->NUM, PLL_MEDIA_NUM_A);
        mfd = EXTREG32BF(&g_pPLL_VIDEO->DENOM, PLL_MEDIA_DENOM_B);

        clk = (pll_ref_clk * div) + ((pll_ref_clk / mfd) * mfn);

        clk = clk / post_div;
    }
    else
    {
        clk = 0;
    }
#else
    // Calculate PLL5 (Video) frequency
    if (EXTREG32BF(&g_pPLL_VIDEO->CTRL, PLL_MEDIA_CTRL_ENABLE))
    {
        pll_ref_clk = BSP_CLK_OSC_FREQ;
        div = EXTREG32BF(&g_pPLL_VIDEO->CTRL, PLL_MEDIA_CTRL_DIV_SELECT);
        clk = (UINT32) ((pll_ref_clk * div) / (2));
    }
    else
    {
        clk = 0;
    }
#endif
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL5_VIDEO] = clk;

    // Calculate MLB frequency
    // MX6_BRING_UP, how to calculate?
    clk = 0;
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL6_MLB] = clk;

    // Calculate PLL6 (ENET) frequency
    if (EXTREG32BF(&g_pPLL_ENET->CTRL, PLL_ENET_CTRL_ENABLE))
    {
        div = EXTREG32BF(&g_pPLL_ENET->CTRL, PLL_ENET_CTRL_DIV_SELECT);
        switch (div)
        {
            case 0:
                clk = 25000000U;        // 25MHz
                break;
            case 1:
                clk = 50000000U;        // 50MHz
                break;
            case 2:
                clk = 100000000U;       // 100MHz
                break;
            case 3:
                clk = 125000000U;       // 125MHz
                break;
            default:
                clk = 0;
                break;
        }
    }
    else
    {
        clk = 0;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL8_ENET] = clk;

    OALMSG(0, (L"Before Calculating 480PFD, PFD reg is %x\r\n", INREG32(&g_pPLL_PFD_480MHZ)));

    // Calculate PFD_480 frequency
    pll_ref_clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG];   // USB 480MHz

    if (!EXTREG32BF(&g_pPLL_PFD_480MHZ->CTRL, PLL_PFD_CTRL_PFD0_CLKGATE))
    {
        //if (EXTREG32BF(&g_pPLL_PFD_480MHZ->CTRL, PLL_PFD_CTRL_PFD0_STABLE))
        //{
            div = EXTREG32BF(&g_pPLL_PFD_480MHZ->CTRL, PLL_PFD_CTRL_PFD0_FRAC);
            clk = (LONGLONG)pll_ref_clk * 18 / div;
        //}
        //else
        //{
        //    // not stable
        //    clk = 0;
        //}
    }
    else
    {

        clk = 0;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD0_720M] = clk;

    if (!EXTREG32BF(&g_pPLL_PFD_480MHZ->CTRL, PLL_PFD_CTRL_PFD1_CLKGATE))
    {
        //if (EXTREG32BF(&g_pPLL_PFD_480MHZ->CTRL, PLL_PFD_CTRL_PFD1_STABLE))
        //{
            div = EXTREG32BF(&g_pPLL_PFD_480MHZ->CTRL, PLL_PFD_CTRL_PFD1_FRAC);
            clk = (LONGLONG)pll_ref_clk * 18 / div;
        //}
        //else
        //{
        //    // not stable
        //    clk = 0;
        //}
    }
    else
    {
        clk = 0;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD1_540M] = clk;

    if (!EXTREG32BF(&g_pPLL_PFD_480MHZ->CTRL, PLL_PFD_CTRL_PFD2_CLKGATE))
    {
        //if (EXTREG32BF(&g_pPLL_PFD_480MHZ->CTRL, PLL_PFD_CTRL_PFD2_STABLE))
        //{
            pll_ref_clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG];   // USB 480MHz
            div = EXTREG32BF(&g_pPLL_PFD_480MHZ->CTRL, PLL_PFD_CTRL_PFD2_FRAC);
            clk = (LONGLONG)pll_ref_clk * 18 / div;
        //}
        //else
        //{
        //    // not stable
        //    clk = 0;
        //}
    }
    else
    {
        clk = 0;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD2_508M] = clk;

    if (!EXTREG32BF(&g_pPLL_PFD_480MHZ->CTRL, PLL_PFD_CTRL_PFD3_CLKGATE))
    {
        //if (EXTREG32BF(&g_pPLL_PFD_480MHZ->CTRL, PLL_PFD_CTRL_PFD3_STABLE))
        //{
            pll_ref_clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG];   // USB 480MHz
            div = EXTREG32BF(&g_pPLL_PFD_480MHZ->CTRL, PLL_PFD_CTRL_PFD3_FRAC);
            clk = (LONGLONG)pll_ref_clk * 18 / div;
        //}
        //else
        //{
        //    // not stable
        //    clk = 0;
        //}
    }
    else
    {
        clk = 0;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD3_454M] = clk;

    OALMSG(0, (L"Before Calculating 528PFD, PFD reg is %x\r\n", INREG32(&g_pPLL_PFD_528MHZ->CTRL)));
    
	// Calculate PFD_528 frequency
    pll_ref_clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL2_BUS];   // BUS 528MHz

    if (!EXTREG32BF(&g_pPLL_PFD_528MHZ->CTRL, PLL_PFD_CTRL_PFD0_CLKGATE))
    {
        //if (EXTREG32BF(&g_pPLL_PFD_528MHZ->CTRL, PLL_PFD_CTRL_PFD0_STABLE))
        //{
            div = EXTREG32BF(&g_pPLL_PFD_528MHZ->CTRL, PLL_PFD_CTRL_PFD0_FRAC);
            clk = (LONGLONG)pll_ref_clk * 18 / div;
        //}
        //else
        //{
        //    // not stable
        //    clk = 0;
        //}
    }
    else
    {
        clk = 0;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD0_352M] = clk;

    if (!EXTREG32BF(&g_pPLL_PFD_528MHZ->CTRL, PLL_PFD_CTRL_PFD1_CLKGATE))
    {
        //if (EXTREG32BF(&g_pPLL_PFD_528MHZ->CTRL, PLL_PFD_CTRL_PFD1_STABLE))
        //{
            div = EXTREG32BF(&g_pPLL_PFD_528MHZ->CTRL, PLL_PFD_CTRL_PFD1_FRAC);
            clk = (LONGLONG)pll_ref_clk * 18 / div;
        //}
        //else
        //{
        //    // not stable
        //    clk = 0;
        //}
    }
    else
    {
        clk = 0;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD1_594M] = clk;

    if (!EXTREG32BF(&g_pPLL_PFD_528MHZ->CTRL, PLL_PFD_CTRL_PFD2_CLKGATE))
    {
        //if (EXTREG32BF(&g_pPLL_PFD_528MHZ->CTRL, PLL_PFD_CTRL_PFD2_STABLE))
        //{
            div = EXTREG32BF(&g_pPLL_PFD_528MHZ->CTRL, PLL_PFD_CTRL_PFD2_FRAC);
            clk = (LONGLONG)pll_ref_clk * 18 / div;
        //}
        //else
        //{
        //    // not stable
        //    clk = 0;
        //}
    }
    else
    {
        clk = 0;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD2_400M] = clk;

    if (!EXTREG32BF(&g_pPLL_PFD_528MHZ->CTRL, PLL_PFD_CTRL_PFD3_CLKGATE))
    {
        //if (EXTREG32BF(&g_pPLL_PFD_528MHZ->CTRL, PLL_PFD_CTRL_PFD3_STABLE))
        //{
            div = EXTREG32BF(&g_pPLL_PFD_528MHZ->CTRL, PLL_PFD_CTRL_PFD3_FRAC);
            clk = (LONGLONG)pll_ref_clk * 18 / div;
        //}
        //else
        //{
        //    // not stable
        //    clk = 0;
        //}
    }
    else
    {
        clk = 0;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD3_200M] = clk;

    // PLL1_REF
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL1_REF] = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_OSC];

    // PLL2_BURNIN
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL2_BURNIN] = 0;

    // 120M
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_120M] = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG] / 4;

    // 60M
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_80M] = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG] / 6;

    // 80M
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_60M] = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG] / 8;

    // ARM
    div = EXTREG32BF(&g_pCCM->CACRR, CCM_CACRR_ARM_PODF) + 1;
    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL1_CPU];
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_ARM] = clk / div;

    // PERIPH
    clk = 0;
    div = 1;
    switch (EXTREG32BF(&g_pCCM->CBCDR, CCM_CBCDR_PERIPH_CLK_SEL))
    {
        case 0:
            switch (EXTREG32BF(&g_pCCM->CBCMR, CCM_CBCMR_PRE_PERIPH_CLK_SEL))
            {
                case 0:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL2_BUS];
                    break;
                case 1:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD2_400M];
                    break;
                case 2:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD0_352M];
                    break;
                case 3:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD3_200M];
                    break;
                default:
                    break;
            }
            break;
        case 1:
            switch (EXTREG32BF(&g_pCCM->CBCMR, CCM_CBCMR_PERIPH_CLK2_SEL))
            {
                case 0:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG];
                    break;
                case 1:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL1_REF];
                    break;
                case 2:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL2_BURNIN];
                    break;
                default:
                    break;
            }
            div *= (EXTREG32BF(&g_pCCM->CBCDR, CCM_CBCDR_PERIPH_CLK2_PODF) + 1);
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PERIPH] = clk / div;

    // AXI
    clk = 0;
    div = EXTREG32BF(&g_pCCM->CBCDR, CCM_CBCDR_AXI_PODF) + 1;

    switch (EXTREG32BF(&g_pCCM->CBCDR, CCM_CBCDR_AXI_SEL))
    {
        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PERIPH];
            break;
        case 1:
            switch (EXTREG32BF(&g_pCCM->CBCDR, CCM_CBCDR_AXI_ALT_SEL))
            {
                case 0:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD2_400M];
                    break;
                case 1:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD1_540M];
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_AXI] = clk / div;

    // MMDC_CH0_AXI
    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PERIPH];
    div = EXTREG32BF(&g_pCCM->CBCDR, CCM_CBCDR_MMDC_CH0_AXI_PODF) + 1;
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_MMDC_CH0_AXI] = clk / div;

    // MMDC_CH1_AXI
    clk = 0;
    div = EXTREG32BF(&g_pCCM->CBCDR, CCM_CBCDR_MMDC_CH1_PODF) + 1;

    switch (EXTREG32BF(&g_pCCM->CBCDR, CCM_CBCDR_PERIPH2_CLK_SEL))
    {
        case 0:
            switch (EXTREG32BF(&g_pCCM->CBCMR, CCM_CBCMR_PRE_PERIPH2_CLK_SEL))
            {
                case 0:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL2_BUS];
                    break;
                case 1:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD2_400M];
                    break;
                case 2:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD0_352M];
                    break;
                case 3:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD3_200M];
                    break;
                default:
                    break;
            }
            break;
        case 1:
            switch(EXTREG32BF(&g_pCCM->CBCMR, CCM_CBCMR_PERIPH2_CLK2_SEL))
            {
                case 0:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG];
                    break;
                case 1:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL1_REF];
                    break;
                default:
                    break;
            }
            div *= EXTREG32BF(&g_pCCM->CBCDR, CCM_CBCDR_PERIPH2_CLK2_PODF);
            break;
        default:
            break;
    }

    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_MMDC_CH1_AXI] = clk / div;

    // AHB_132M
    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PERIPH];
    div = EXTREG32BF(&g_pCCM->CBCDR, CCM_CBCDR_AHB_PODF) + 1;
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_AHB_132M] = clk / div;

    // IPG
    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_AHB_132M];
    div = EXTREG32BF(&g_pCCM->CBCDR, CCM_CBCDR_IPG_PODF) + 1;
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPG] = clk / div;

    // PERCLK
    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPG];
    div = EXTREG32BF(&g_pCCM->CSCMR1, CCM_CSCMR1_PERCLK_PODF) + 1;
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PERCLK] = clk / div;

    // CKIL_SYNC
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_CKIL_SYNC] = 0;

    // CKIH
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_CKIH] = 0;

    // IPU2_HSP
    clk = 0;
    div = EXTREG32BF(&g_pCCM->CSCDR3, CCM_CSCDR3_IPU2_HSP_PODF) + 1;

    switch (EXTREG32BF(&g_pCCM->CSCDR3, CCM_CSCDR3_IPU2_HSP_CLK_SEL))
    {
        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_MMDC_CH0_AXI];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD2_400M];
            break;
        case 2:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_120M];
            break;
        case 3:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD1_540M];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPU2_HSP] = clk / div;

    // IPU1_HSP
    clk = 0;
    div = EXTREG32BF(&g_pCCM->CSCDR3, CCM_CSCDR3_IPU1_HSP_PODF) + 1;

    switch (EXTREG32BF(&g_pCCM->CSCDR3, CCM_CSCDR3_IPU1_HSP_CLK_SEL))
    {
        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_MMDC_CH0_AXI];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD2_400M];
            break;
        case 2:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_120M];
            break;
        case 3:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD1_540M];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPU1_HSP] = clk / div;

    // PCIE_AXI
    clk = 0;
    div = 1;

    switch (EXTREG32BF(&g_pCCM->CBCMR, CCM_CBCMR_PCIE_AXI_CLK_SEL))
    {
        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_AXI];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_AHB_132M];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PCIE_AXI] = clk / div;

    // VDO_AXI
    clk = 0;
    div = 1;

    switch (EXTREG32BF(&g_pCCM->CBCMR, CCM_CBCMR_VDOAXI_CLK_SEL))
    {
        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_AXI];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_AHB_132M];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_VDO_AXI] = clk / div;

    // VPU_AXI
    clk = 0;
    div = EXTREG32BF(&g_pCCM->CSCDR1, CCM_CSCDR1_VPU_AXI_PODF) + 1;

    switch (EXTREG32BF(&g_pCCM->CBCMR, CCM_CBCMR_VPU_AXI_CLK_SEL))
    {
        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_AXI];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD2_400M];
            break;
        case 2:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD0_352M];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_VPU_AXI] = clk / div;

    // USDHC1
    clk = 0;
    div = EXTREG32BF(&g_pCCM->CSCDR1, CCM_CSCDR1_USDHC1_PODF) + 1;

    switch(EXTREG32BF(&g_pCCM->CSCMR1, CCM_CSCMR1_USDHC1_CLK_SEL))
    {
        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD2_400M];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD0_352M];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_USDHC1] = clk / div;

    // USDHC2
    clk = 0;
    div = EXTREG32BF(&g_pCCM->CSCDR1, CCM_CSCDR1_USDHC2_PODF) + 1;

    switch(EXTREG32BF(&g_pCCM->CSCMR1, CCM_CSCMR1_USDHC2_CLK_SEL))
    {
        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD2_400M];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD0_352M];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_USDHC2] = clk / div;

    // USDHC3
    clk = 0;
    div = EXTREG32BF(&g_pCCM->CSCDR1, CCM_CSCDR1_USDHC3_PODF) + 1;

    switch(EXTREG32BF(&g_pCCM->CSCMR1, CCM_CSCMR1_USDHC3_CLK_SEL))
    {
        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD2_400M];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD0_352M];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_USDHC3] = clk / div;

    // USDHC4
    clk = 0;
    div = EXTREG32BF(&g_pCCM->CSCDR1, CCM_CSCDR1_USDHC4_PODF) + 1;

    switch(EXTREG32BF(&g_pCCM->CSCMR1, CCM_CSCMR1_USDHC4_CLK_SEL))
    {
        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD2_400M];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD0_352M];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_USDHC4] = clk / div;

    // SSI1
    clk = 0;
    div = (EXTREG32BF(&g_pCCM->CS1CDR, CCM_CS1CDR_SSI1_CLK_PODF) + 1) * (EXTREG32BF(&g_pCCM->CS1CDR, CCM_CS1CDR_SSI1_CLK_PRED) + 1);

    switch (EXTREG32BF(&g_pCCM->CSCMR1, CCM_CSCMR1_SSI1_CLK_SEL))
    {
        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD2_508M];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD3_454M];
            break;
        case 2:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL4_AUDIO];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_SSI1] = clk / div;

    // SSI2
    clk = 0;
    div = (EXTREG32BF(&g_pCCM->CS2CDR, CCM_CS2CDR_SSI2_CLK_PODF) + 1) * (EXTREG32BF(&g_pCCM->CS2CDR, CCM_CS2CDR_SSI2_CLK_PRED) + 1);

    switch (EXTREG32BF(&g_pCCM->CSCMR1, CCM_CSCMR1_SSI2_CLK_SEL))
    {
        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD2_508M];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD3_454M];
            break;
        case 2:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL4_AUDIO];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_SSI2] = clk / div;

    // SSI3
    clk = 0;
    div = (EXTREG32BF(&g_pCCM->CS1CDR, CCM_CS1CDR_SSI3_CLK_PODF) + 1) * (EXTREG32BF(&g_pCCM->CS1CDR, CCM_CS1CDR_SSI3_CLK_PRED) + 1);

    switch (EXTREG32BF(&g_pCCM->CSCMR1, CCM_CSCMR1_SSI3_CLK_SEL))
    {
        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD2_508M];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD3_454M];
            break;
        case 2:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL4_AUDIO];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_SSI3] = clk / div;

    // CAN
    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_60M];
    div = EXTREG32BF(&g_pCCM->CSCMR2, CCM_CSCMR2_CAN_CLK_PODF) + 1;
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_CAN] = clk / div;

    // UART
    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_80M];
    div = EXTREG32BF(&g_pCCM->CSCDR1, CCM_CSCDR1_UART_CLK_PODF) + 1;
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_UART] = clk / div;

    // SPDIF0
    clk = 0;
    div = (EXTREG32BF(&g_pCCM->CDCDR, CCM_CDCDR_SPDIF0_CLK_PODF) + 1) * (EXTREG32BF(&g_pCCM->CDCDR, CCM_CDCDR_SPDIF0_CLK_PRED) + 1);
    switch (EXTREG32BF(&g_pCCM->CDCDR, CCM_CDCDR_SPDIF0_CLK_SEL))
    {
        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL4_AUDIO];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD2_508M];
            break;
        case 2:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD3_454M];
            break;
        case 3:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_SPDIF0] = clk / div;

    // SPDIF1
    clk = 0;
    div = (EXTREG32BF(&g_pCCM->CDCDR, CCM_CDCDR_SPDIF1_CLK_PODF) + 1) * (EXTREG32BF(&g_pCCM->CDCDR, CCM_CDCDR_SPDIF1_CLK_PRED) + 1);
    switch (EXTREG32BF(&g_pCCM->CDCDR, CCM_CDCDR_SPDIF1_CLK_SEL))
    {
        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL4_AUDIO];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD2_508M];
            break;
        case 2:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD3_454M];
            break;
        case 3:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_SPDIF1] = clk / div;

    // ESAI
    clk = 0;
    div = (EXTREG32BF(&g_pCCM->CS1CDR, CCM_CS1CDR_ESAI_CLK_PODF) + 1) * (EXTREG32BF(&g_pCCM->CS1CDR, CCM_CS1CDR_ESAI_CLK_PRED) + 1);
    switch (EXTREG32BF(&g_pCCM->CSCMR2, CCM_CSCMR2_ESAI_CLK_SEL))
    {
        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL4_AUDIO];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD2_508M];
            break;
        case 2:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD3_454M];
            break;
        case 3:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_ESAI] = clk / div;

    // ECSPI
    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_60M];
    div = EXTREG32BF(&g_pCCM->CSCDR2, CCM_CSCDR2_ECSPI_CLK_PODF) + 1;
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_ECSPI] = clk / div;

    // ACLK_EMI_SLOW
    clk = 0;
    div = EXTREG32BF(&g_pCCM->CSCMR1, CCM_CSCMR1_ACLK_EMI_SLOW_PODF) + 1;
    switch (EXTREG32BF(&g_pCCM->CSCMR1, CCM_CSCMR1_ACLK_EMI_SLOW_SEL))
    {
        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_AXI];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG];
            break;
        case 2:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD2_400M];
            break;
        case 3:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD0_352M];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_ACLK_EMI_SLOW] = clk / div;

    // ACLK_EMI
    clk = 0;
    div = EXTREG32BF(&g_pCCM->CSCMR1, CCM_CSCMR1_ACLK_EMI_PODF) + 1;
    switch (EXTREG32BF(&g_pCCM->CSCMR1, CCM_CSCMR1_ACLK_EMI_SEL))
    {
        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_AXI];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG];
            break;
        case 2:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD2_400M];
            break;
        case 3:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD0_352M];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_ACLK_EMI] = clk / div;

    // ENFC
    clk = 0;
    div = (EXTREG32BF(&g_pCCM->CS2CDR, CCM_CS2CDR_ENFC_CLK_PODF) + 1) * (EXTREG32BF(&g_pCCM->CS2CDR, CCM_CS2CDR_ENFC_CLK_PRED) + 1);
    switch (EXTREG32BF(&g_pCCM->CS2CDR, CCM_CS2CDR_ENFC_CLK_SEL))
    {
        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD0_352M];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL2_BUS];
            break;
        case 2:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG];
            break;
        case 3:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD2_400M];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_ENFC] = clk / div;

    // HSI_TX
    clk = 0;
    div = EXTREG32BF(&g_pCCM->CDCDR, CCM_CDCDR_HSI_TX_PODF) + 1;
    switch (EXTREG32BF(&g_pCCM->CDCDR, CCM_CDCDR_HSI_TX_CLK_SEL))
    {
        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_120M];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD2_400M];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_HSI_TX] = clk / div;

    // VIDEO_27M
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_VIDEO_27M] = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD1_540M] / 20;

    // LDB_DI0_SERIAL
    clk = 0;
    div = 1;
    switch (EXTREG32BF(&g_pCCM->CS2CDR, CCM_CS2CDR_LDB_DI0_CLK_SEL))
    {
        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL5_VIDEO];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD0_352M];
            break;
        case 2:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD2_400M];
            break;
        case 3:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD1_540M];
            break;
        case 4:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_LDB_DI0_SERIAL] = clk / div;

    // LDB_DI1_SERIAL
    clk = 0;
    div = 1;
    switch (EXTREG32BF(&g_pCCM->CS2CDR, CCM_CS2CDR_LDB_DI1_CLK_SEL))
    {
        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL5_VIDEO];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD0_352M];
            break;
        case 2:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD2_400M];
            break;
        case 3:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD1_540M];
            break;
        case 4:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_LDB_DI1_SERIAL] = clk / div;

    // LDB_DI0_IPU
    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_LDB_DI0_SERIAL] * 2;
    switch (EXTREG32BF(&g_pCCM->CSCMR2, CCM_CSCMR2_LDB_DI0_IPU_DIV))
    {
        case 0:
            div = 7;        // 2/7 = 1/3.5
            break;
        case 1:
            div = 14;       // 2/14 = 1/7
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_LDB_DI0_IPU] = clk / div;

    // LDB_DI1_IPU
    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_LDB_DI1_SERIAL] * 2;
    switch (EXTREG32BF(&g_pCCM->CSCMR2, CCM_CSCMR2_LDB_DI1_IPU_DIV))
    {
        case 0:
            div = 7;        // 2/7 = 1/3.5
            break;
        case 1:
            div = 14;       // 2/14 = 1/7
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_LDB_DI1_IPU] = clk / div;

    // IPU1_DI0
    clk = 0;
    div = 1;

    switch (EXTREG32BF(&g_pCCM->CHSCCDR, CCM_CHSCCDR_IPU1_DI0_CLK_SEL))
    {
        case 0:
            div *= (EXTREG32BF(&g_pCCM->CHSCCDR, CCM_CHSCCDR_IPU1_DI0_PODF) + 1);
            switch (EXTREG32BF(&g_pCCM->CHSCCDR, CCM_CHSCCDR_IPU1_DI0_PRE_CLK_SEL))
            {
                case 0:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_MMDC_CH0_AXI];
                    break;
                case 1:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG];
                    break;
                case 2:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL5_VIDEO];
                    break;
                case 3:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD0_352M];
                    break;
                case 4:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD2_400M];
                    break;
                case 5:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD1_540M];
                    break;
                default:
                    break;
            }
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPP_DI0];
            break;
        case 2:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPP_DI1];
            break;
        case 3:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_LDB_DI0_IPU];
            break;
        case 4:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_LDB_DI1_IPU];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPU1_DI0] = clk / div;

    // IPU1_DI1
    clk = 0;
    div = 1;

    switch (EXTREG32BF(&g_pCCM->CHSCCDR, CCM_CHSCCDR_IPU1_DI1_CLK_SEL))
    {
        case 0:
            div *= (EXTREG32BF(&g_pCCM->CHSCCDR, CCM_CHSCCDR_IPU1_DI1_PODF) + 1);
            switch (EXTREG32BF(&g_pCCM->CHSCCDR, CCM_CHSCCDR_IPU1_DI1_PRE_CLK_SEL))
            {
                case 0:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_MMDC_CH0_AXI];
                    break;
                case 1:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG];
                    break;
                case 2:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL5_VIDEO];
                    break;
                case 3:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD0_352M];
                    break;
                case 4:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD2_400M];
                    break;
                case 5:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD1_540M];
                    break;
                default:
                    break;
            }
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPP_DI0];
            break;
        case 2:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPP_DI1];
            break;
        case 3:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_LDB_DI0_IPU];
            break;
        case 4:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_LDB_DI1_IPU];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPU1_DI1] = clk / div;

    // IPU2_DI0
    clk = 0;
    div = 1;

    switch (EXTREG32BF(&g_pCCM->CSCDR2, CCM_CSCDR2_IPU2_DI0_CLK_SEL))
    {
        case 0:
            div *= (EXTREG32BF(&g_pCCM->CHSCCDR, CCM_CSCDR2_IPU2_DI0_PODF) + 1);
            switch (EXTREG32BF(&g_pCCM->CHSCCDR, CCM_CSCDR2_IPU2_DI0_PRE_CLK_SEL))
            {
                case 0:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_MMDC_CH0_AXI];
                    break;
                case 1:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG];
                    break;
                case 2:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL5_VIDEO];
                    break;
                case 3:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD0_352M];
                    break;
                case 4:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD2_400M];
                    break;
                case 5:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD1_540M];
                    break;
                default:
                    break;
            }
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPP_DI0];
            break;
        case 2:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPP_DI1];
            break;
        case 3:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_LDB_DI0_IPU];
            break;
        case 4:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_LDB_DI1_IPU];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPU2_DI0] = clk / div;

    // IPU2_DI1
    clk = 0;
    div = 1;

    switch (EXTREG32BF(&g_pCCM->CSCDR2, CCM_CSCDR2_IPU2_DI1_CLK_SEL))
    {
        case 0:
            div *= (EXTREG32BF(&g_pCCM->CHSCCDR, CCM_CSCDR2_IPU2_DI1_PODF) + 1);
            switch (EXTREG32BF(&g_pCCM->CHSCCDR, CCM_CSCDR2_IPU2_DI1_PRE_CLK_SEL))
            {
                case 0:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_MMDC_CH0_AXI];
                    break;
                case 1:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG];
                    break;
                case 2:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL5_VIDEO];
                    break;
                case 3:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD0_352M];
                    break;
                case 4:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD2_400M];
                    break;
                case 5:
                    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD1_540M];
                    break;
                default:
                    break;
            }
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPP_DI0];
            break;
        case 2:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPP_DI1];
            break;
        case 3:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_LDB_DI0_IPU];
            break;
        case 4:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_LDB_DI1_IPU];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPU2_DI1] = clk / div;

    // ASRC
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_ASRC] = 0;
    // WRCK
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_WRCK] = 0;

    // GPU2D_CORE
    clk = 0;
    div = EXTREG32BF(&g_pCCM->CBCMR, CCM_CBCMR_GPU2D_CORE_CLK_PODF) + 1;
    switch (EXTREG32BF(&g_pCCM->CBCMR, CCM_CBCMR_GPU2D_CLK_SEL))
    {

        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_AXI];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG];
            break;
        case 2:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD0_352M];
            break;
        case 3:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD2_400M];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_GPU2D_CORE] = clk / div;

    // GPU2D_AXI
    clk = 0;
    div = 1;
    switch (EXTREG32BF(&g_pCCM->CBCMR, CCM_CBCMR_GPU2D_AXI_CLK_SEL))
    {
        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_AXI];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_AHB_132M];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_GPU2D_AXI] = clk / div;

    // GPU3D_CORE
    clk = 0;
    div = EXTREG32BF(&g_pCCM->CBCMR, CCM_CBCMR_GPU3D_CORE_PODF) + 1;
    switch (EXTREG32BF(&g_pCCM->CBCMR, CCM_CBCMR_GPU3D_CORE_CLK_SEL))
    {

        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_MMDC_CH0_AXI];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG];
            break;
        case 2:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD1_594M];
            break;
        case 3:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD2_400M];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_GPU3D_CORE] = clk / div;


    // GPU3D_AXI
    clk = 0;
    div = 1;
    switch (EXTREG32BF(&g_pCCM->CBCMR, CCM_CBCMR_GPU3D_AXI_CLK_SEL))
    {
        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_AXI];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_AHB_132M];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_GPU3D_AXI] = clk / div;

    // GPU3D_SHADER
    clk = 0;
    div = EXTREG32BF(&g_pCCM->CBCMR, CCM_CBCMR_GPU3D_SHADER_PODF) + 1;
    switch (EXTREG32BF(&g_pCCM->CBCMR, CCM_CBCMR_GPU3D_SHADER_CLK_SEL))
    {

        case 0:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_MMDC_CH0_AXI];
            break;
        case 1:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG];
            break;
        case 2:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD1_594M];
            break;
        case 3:
            clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD0_720M];
            break;
        default:
            break;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_GPU3D_SHADER] = clk / div;

    pBSPArgs->updateMode = FALSE;
    
    rc = TRUE;


cleanUp:

    return rc;
}

//------------------------------------------------------------------------------
//
//  Function:   OALPrepareUARTFreq
//
//  This function initializes UART Freq
//
//  Parameters:
//      pBSPArgs
//          [out] Points to BSP arguments structure to be updated.
//
//  Returns:
//      TRUE if boot successfully, otherwise returns FALSE.
//
//  Remarks:
//      The original code sturctre initialize serial hardware after
//      "OALBspArgsInit". So we can not output debug strings in it
//      In fact, only dependency is UART freq, so we calculate it
//      first in a specific function
//
//------------------------------------------------------------------------------
BOOL OALPrepareUARTFreq(BSP_ARGS *pBSPArgs)
{
    BOOL rc = TRUE;
    UINT32 pll_ref_clk;
    UINT32 clk, div;

    g_pPLL_USBOTG = (PCSP_PLL_BASIC_REGS) OALPAtoUA(CSP_BASE_REG_PA_PLL_USBOTG);
    g_pCCM = (PCSP_CCM_REGS) OALPAtoUA(CSP_BASE_REG_PA_CCM);

    // Calculate PLL3 (USBOTG) frequency
    if (EXTREG32BF(&g_pPLL_USBOTG->CTRL, PLL_USBOTG_CTRL_ENABLE))
    {
        pll_ref_clk = BSP_CLK_OSC_FREQ;
        if (EXTREG32BF(&g_pPLL_USBOTG->CTRL, PLL_USBOTG_CTRL_DIV_SELECT) == 1)
        {
            div = 22;
        }
        else
        {
            div = 20;
        }
        clk = (UINT32) (pll_ref_clk * div);    
    }
    else
    {
        clk = 0;
    }
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG] = clk;

    // 60M
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_80M] = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG] / 6;

    // UART
    clk = pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_80M];
    div = EXTREG32BF(&g_pCCM->CSCDR1, CCM_CSCDR1_UART_CLK_PODF) + 1;
    pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_UART] = clk / div;

    return rc;
}

//------------------------------------------------------------------------------
//
//  Function:   OALBspArgsPrint
//
//  This function prints the parameters of the global BSP args structure.
//
//  Parameters:
//      pBSPArgs
//          [in] Points to BSP arguments structure to be printed.
//
//  Returns:
//      None
//
//------------------------------------------------------------------------------
VOID OALBspArgsPrint(BSP_ARGS *pBSPArgs)
{
    if (pBSPArgs == NULL)
    {
        goto cleanUp;
    }

    OALMSG(OAL_INFO, (TEXT("BSP Clock Configuration:\r\n")));
    OALMSG(OAL_INFO, (TEXT("    OSC             = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_OSC]));
    OALMSG(OAL_INFO, (TEXT("    PLL1 (CPU)      = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL1_CPU]));
    OALMSG(OAL_INFO, (TEXT("    PLL2 (BUS)      = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL2_BUS]));
    OALMSG(OAL_INFO, (TEXT("    PLL3 (USBOTG)   = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_USBOTG]));
    OALMSG(OAL_INFO, (TEXT("    PLL4 (AUDIO)    = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL4_AUDIO]));
    OALMSG(OAL_INFO, (TEXT("    PLL5 (VIDEO)    = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL5_VIDEO]));
    OALMSG(OAL_INFO, (TEXT("    PLL6 (MLB)      = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL6_MLB]));
    OALMSG(OAL_INFO, (TEXT("    PLL7 (USBHOST)  = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL7_USBHOST]));
    OALMSG(OAL_INFO, (TEXT("    PLL8 (ENET)     = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL8_ENET]));
    OALMSG(OAL_INFO, (TEXT("    PLL1_REF        = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL1_REF]));
    OALMSG(OAL_INFO, (TEXT("    PLL2_BURNIN     = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL2_BURNIN]));
    OALMSG(OAL_INFO, (TEXT("    PLL3_60M        = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_60M]));
    OALMSG(OAL_INFO, (TEXT("    PLL3_80M        = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_80M]));
    OALMSG(OAL_INFO, (TEXT("    PLL3_120M       = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PLL3_120M]));
    OALMSG(OAL_INFO, (TEXT("    480M_PFD0_720M  = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD0_720M]));
    OALMSG(OAL_INFO, (TEXT("    480M_PFD1_540M  = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD1_540M]));
    OALMSG(OAL_INFO, (TEXT("    480M_PFD2_508M  = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD2_508M]));
    OALMSG(OAL_INFO, (TEXT("    480M_PFD3_454M  = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_480M_PFD3_454M]));
    OALMSG(OAL_INFO, (TEXT("    528M_PFD0_352M  = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD0_352M]));
    OALMSG(OAL_INFO, (TEXT("    528M_PFD1_594M  = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD1_594M]));
    OALMSG(OAL_INFO, (TEXT("    528M_PFD2_400M  = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD2_400M]));
    OALMSG(OAL_INFO, (TEXT("    528M_PFD3_200M  = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_528M_PFD3_200M]));
    OALMSG(OAL_INFO, (TEXT("    ARM             = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_ARM]));
    OALMSG(OAL_INFO, (TEXT("    AXI             = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_AXI]));
    OALMSG(OAL_INFO, (TEXT("    PERIPH          = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PERIPH]));
    OALMSG(OAL_INFO, (TEXT("    MMDC_CH0_AXI    = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_MMDC_CH0_AXI]));
    OALMSG(OAL_INFO, (TEXT("    MMDC_CH1_AXI    = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_MMDC_CH1_AXI]));
    OALMSG(OAL_INFO, (TEXT("    AHB_132M        = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_AHB_132M]));
    OALMSG(OAL_INFO, (TEXT("    IPG             = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPG]));
    OALMSG(OAL_INFO, (TEXT("    PERCLK          = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PERCLK]));
    OALMSG(OAL_INFO, (TEXT("    CKIL_SYNC       = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_CKIL_SYNC]));
    OALMSG(OAL_INFO, (TEXT("    CKIH            = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_CKIH]));
    OALMSG(OAL_INFO, (TEXT("    IPU2_HSP        = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPU2_HSP]));
    OALMSG(OAL_INFO, (TEXT("    IPU1_HSP        = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPU1_HSP]));
    OALMSG(OAL_INFO, (TEXT("    PCIE_AXI        = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_PCIE_AXI]));
    OALMSG(OAL_INFO, (TEXT("    VDO_AXI         = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_VDO_AXI]));
    OALMSG(OAL_INFO, (TEXT("    VPU_AXI         = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_VPU_AXI]));
    OALMSG(OAL_INFO, (TEXT("    USDHC1          = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_USDHC1]));
    OALMSG(OAL_INFO, (TEXT("    USDHC2          = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_USDHC2]));
    OALMSG(OAL_INFO, (TEXT("    USDHC3          = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_USDHC3]));
    OALMSG(OAL_INFO, (TEXT("    USDHC4          = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_USDHC4]));
    OALMSG(OAL_INFO, (TEXT("    SSI1            = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_SSI1]));
    OALMSG(OAL_INFO, (TEXT("    SSI2            = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_SSI2]));
    OALMSG(OAL_INFO, (TEXT("    SSI3            = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_SSI3]));
    OALMSG(OAL_INFO, (TEXT("    CAN             = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_CAN]));
    OALMSG(OAL_INFO, (TEXT("    UART            = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_UART]));
    OALMSG(OAL_INFO, (TEXT("    SPDIF0          = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_SPDIF0]));
    OALMSG(OAL_INFO, (TEXT("    SPDIF1          = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_SPDIF1]));
    OALMSG(OAL_INFO, (TEXT("    ESAI            = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_ESAI]));
    OALMSG(OAL_INFO, (TEXT("    ECSPI           = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_ECSPI]));
    OALMSG(OAL_INFO, (TEXT("    ACLK_EMI_SLOW   = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_ACLK_EMI_SLOW]));
    OALMSG(OAL_INFO, (TEXT("    ACLK_EMI        = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_ACLK_EMI]));
    OALMSG(OAL_INFO, (TEXT("    ENFC            = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_ENFC]));
    OALMSG(OAL_INFO, (TEXT("    HSI_TX          = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_HSI_TX]));
    OALMSG(OAL_INFO, (TEXT("    VIDEO_27M       = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_VIDEO_27M]));
    OALMSG(OAL_INFO, (TEXT("    LDB_DI0_SERIAL  = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_LDB_DI0_SERIAL]));
    OALMSG(OAL_INFO, (TEXT("    LDB_DI1_SERIAL  = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_LDB_DI1_SERIAL]));
    OALMSG(OAL_INFO, (TEXT("    LDB_DI0_IPU     = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_LDB_DI0_IPU]));
    OALMSG(OAL_INFO, (TEXT("    LDB_DI1_IPU     = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_LDB_DI1_IPU]));
    OALMSG(OAL_INFO, (TEXT("    IPU1_DI0        = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPU1_DI0]));
    OALMSG(OAL_INFO, (TEXT("    IPU1_DI1        = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPU1_DI1]));
    OALMSG(OAL_INFO, (TEXT("    IPU2_DI0        = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPU2_DI0]));
    OALMSG(OAL_INFO, (TEXT("    IPU2_DI1        = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPU2_DI1]));
    OALMSG(OAL_INFO, (TEXT("    ASRC            = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_ASRC]));
    OALMSG(OAL_INFO, (TEXT("    WRCK            = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_WRCK]));
    OALMSG(OAL_INFO, (TEXT("    GPU2D_CORE      = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_GPU2D_CORE]));
    OALMSG(OAL_INFO, (TEXT("    GPU2D_AXI       = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_GPU2D_AXI]));
    OALMSG(OAL_INFO, (TEXT("    GPU3D_CORE      = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_GPU3D_CORE]));
    OALMSG(OAL_INFO, (TEXT("    GPU3D_AXI       = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_GPU3D_AXI]));
    OALMSG(OAL_INFO, (TEXT("    GPU3D_SHADER    = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_GPU3D_SHADER]));
    OALMSG(OAL_INFO, (TEXT("    IPP_DI0         = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPP_DI0]));
    OALMSG(OAL_INFO, (TEXT("    IPP_DI1         = %10d Hz\r\n"),
           pBSPArgs->clockFreq[DDK_CLOCK_SIGNAL_IPP_DI1]));
cleanUp:
    return;
}

// Returns offset between start of memory and start of OAL_PLAT_INFO
UINT32 BSP_GetPlatInfoOffset()
{
	BSP_ARGS *pArgs = NULL;

	// Get PlatInfo offset in boot args
	UINT32 offset = (UINT32)&(pArgs->platInfo) - (UINT32)pArgs;

	return IMAGE_SHARE_ARGS_RAM_OFFSET + offset;
}

// Returns OAL_PLAT_INFO structure's physical address
UINT32 BSP_GetPlatInfoAddress()
{
	BSP_ARGS *pArgs = NULL;
	UINT32 o;

	// Get PlatInfo offset in boot args
	o = (UINT32)&(pArgs->platInfo) - (UINT32)pArgs;

	// Return physical address of PlatInfo structure
	return IMAGE_SHARE_ARGS_RAM_PA_START + o;
}