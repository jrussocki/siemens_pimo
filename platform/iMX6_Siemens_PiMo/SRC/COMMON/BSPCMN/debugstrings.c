//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//------------------------------------------------------------------------------
//
//  Copyright (C) 2011, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
#include "bsp.h"

const TCHAR ClkSigName[DDK_CLOCK_SIGNAL_ENUM_END + 1][20] = {
    {L"OSC"},                       //   = 0,
    {L"PLL1_CPU"},                  //   = 1,            // CPU
    {L"PLL2_BUS"},                  //   = 2,            // BUS (528MHz)
    {L"PLL3_USBOTG"},               //   = 3,            // USB (480MHz)
    {L"PLL1_REF"},                  //   = 4,
    {L"PLL4_AUDIO"},                //   = 5,            // AUDIO
    {L"ARM"},                       //   = 6,
    {L"AXI"},                       //   = 7,
    {L"PERIPH"},                    //   = 8,            // This is a intermidiate signal
    {L"MMDC_CH0_AXI"},              //   = 9,
    {L"MMDC_CH1_AXI"},              //   = 10,
    {L"AHB_132M"},                  //   = 11,
    {L"IPG"},                       //   = 12,
    {L"PERCLK"},                    //   = 13,           // PERCLK and PERIPH_CLK, is there any difference, to be examined
    {L"CKIL_SYNC"},                 //   = 14,
    {L"CKIH"},                      //   = 15,
    {L"IPU2_HSP"},                  //   = 16,
    {L"IPU1_HSP"},                  //   = 17,
    {L"PCIE_AXI"},                  //   = 18,
    {L"VDO_AXI"},                   //   = 19,
    {L"VPU_AXI"},                   //   = 20,
    {L"528M_PFD2_400M"},            //   = 21,
    {L"USDHC1"},                    //   = 22,
    {L"USDHC2"},                    //   = 23,
    {L"USDHC3"},                    //   = 24,
    {L"USDHC4"},                    //   = 25,
    {L"SSI1"},                      //   = 26,
    {L"SSI2"},                      //   = 27,
    {L"SSI3"},                      //   = 28,
    {L"CAN"},                       //   = 29,
    {L"UART"},                      //   = 30,
    {L"SPDIF0"},                    //   = 31,
    {L"SPDIF1"},                    //   = 32,
    {L"ESAI"},                      //   = 33,
    {L"ECSPI"},                     //   = 34,
    {L"ACLK_EMI_SLOW"},             //   = 35,
    {L"ACLK_EMI"},                  //   = 36,
    {L"ENFC"},                      //   = 37,
    {L"HSI_TX"},                    //   = 38,
    {L"VIDEO_27M"},                 //   = 39,
    {L"LDB_DI0_SERIAL"},            //   = 40,
    {L"LDB_DI1_SERIAL"},            //   = 41,
    {L"LDB_DI0_IPU"},               //   = 42,
    {L"LDB_DI1_IPU"},               //   = 43,
    {L"IPU1_DI0"},                  //   = 44,
    {L"IPU1_DI1"},                  //   = 45,
    {L"IPU2_DI0"},                  //   = 46,
    {L"IPU2_DI1"},                  //   = 47,
    {L"ASRC"},                      //   = 48,
    {L"SIG49"},
    {L"WRCK"},                      //   = 50,
    {L"GPU2D_CORE"},                //   = 51,
    {L"GPU2D_AXI"},                 //   = 52,
    {L"GPU3D_CORE"},                //   = 53,
    {L"GPU3D_AXI"},                 //   = 54,
    {L"GPU3D_SHADER"},              //   = 55,
    {L"SIG56"},
    {L"SIG57"},
    {L"PLL5_VIDEO"},                //   = 58,           // VIDEO
    {L"PLL3_120M"},                 //   = 59,
    {L"PLL3_80M"},                  //   = 60,
    {L"PLL3_60M"},                  //   = 61,
    {L"PLL6_MLB"},                  //   = 62,           // MLB
    {L"PLL2_BURNIN"},               //   = 63,
    {L"IPP_DI0"},                   //   = 64,
    {L"IPP_DI1"},                   //   = 65,
    {L"PLL7_USBHOST"},              //   = 66,           // USBHOST
    {L"PLL8_ENET"},                 //   = 67,           // ENET
    {L"SIG68"},
    {L"528M_PFD3_200M"},            //   = 69,
    {L"SIG70"},
    {L"SIG71"},
    {L"528M_PFD0_352M"},            //   = 72,
    {L"480M_PFD1_540M"},            //   = 73,
    {L"480M_PFD0_720M"},            //   = 74,
    {L"528M_PFD1_594M"},            //   = 75,
    {L"480M_PFD2_508M"},            //   = 76, // 508.2MHz
    {L"480M_PFD3_454M"},            //   = 77, // 454.7Mhz
    {L"SIG78"},
    {L"SIG79"},
    {L"SIG_END"}                   //   = 80
};

const TCHAR ClkGateName[DDK_CLOCK_GATE_INDEX_ENUM_END + 1][40] = {
    {L"AIPS_TZ1"},                          // = 0,    // CGR0[1:0]
    {L"AIPS_TZ2"},                          // = 1,    // CGR0[3:2]
    {L"APBHDMA_HCLK"},                      // = 2,    // CGR0[5:4]
    {L"ASRC_CLK"},                          // = 3,    // CGR0[7:6]
    {L"CAAM_SECURE_MEM"},                   // = 4,    // CGR0[9:8]
    {L"CAAM_WRAPPER_ACLK"},                 // = 5,    // CGR0[11:10]
    {L"CAAM_WRAPPER_IPG"},                  // = 6,    // CGR0[13:12]
    {L"CAN1"},                              // = 7,    // CGR0[15:14]
    {L"CAN1_SERIAL"},                       // = 8,    // CGR0[17:16]
    {L"CAN2"},                              // = 9,    // CGR0[19:18]
    {L"CAN2_SERIAL"},                       // = 10,   // CGR0[21:20]
    {L"CHEETAH_DBG"},                       // = 11,   // CGR0[23:22]
    {L"DCIC1"},                             // = 12,   // CGR0[25:24]
    {L"DCIC2"},                             // = 13,   // CGR0[27:26]
    {L"DTCP_DTCP"},                         // = 14,   // CGR0[29:28]
    {L"R1"},                                // = 15,   // CGR0[31:30]
    {L"ECSPI1"},                            // = 16,   // CGR1[1:0]
    {L"ECSPI2"},                            // = 17,   // CGR1[3:2]
    {L"ECSPI3"},                            // = 18,   // CGR1[5:4]
    {L"ECSPI4"},                            // = 19,   // CGR1[7:6]
    {L"ECSPI5"},                            // = 20,   // CGR1[9:8]
    {L"ENET"},                              // = 21,   // CGR1[11:10]
    {L"EPIT1"},                             // = 22,   // CGR1[13:12]
    {L"EPIT2"},                             // = 23,   // CGR1[15:14]
    {L"ESAI"},                              // = 24,   // CGR1[17:16]
    {L"R2"},                                // = 25,   // CGR1[19:18]
    {L"GPT"},                               // = 26,   // CGR1[21:20]
    {L"GPT_SERIAL"},                        // = 27,   // CGR1[23:22]
    {L"GPU2D"},                             // = 28,   // CGR1[25:24]
    {L"GPU3D"},                             // = 29,   // CGR1[27:26]
    {L"R3"},                                // = 30,   // CGR1[29:28]
    {L"R4"},                                // = 31,   // CGR1[31:30]
    {L"HDMI_TX_IAHBCLK"},                   // = 32,   // CGR2[1:0]
    {L"R5"},                                // = 33,   // CGR2[3:2]
    {L"HDMI_TX_ISFR"},                      // = 34,   // CGR2[5:4]
    {L"I2C1_SERIAL"},                       // = 35,   // CGR2[7:6]
    {L"I2C2_SERIAL"},                       // = 36,   // CGR2[9:8]
    {L"I2C3_SERIAL"},                       // = 37,   // CGR2[11:10]
    {L"IIM"},                               // = 38,   // CGR2[13:12]
    {L"IOMUX_IPT"},                         // = 39,   // CGR2[15:14]
    {L"IPMUX1"},                            // = 40,   // CGR2[17:16]
    {L"IPMUX2"},                            // = 41,   // CGR2[19:18]
    {L"IPMUX3"},                            // = 42,   // CGR2[21:20]
    {L"IPSYNC_IP2APB_TZASC2_IPG_MASTER"},   // = 43,   // CGR2[23:22]
    {L"IPSYNC_IP2APB_TZASC2_IPG"},          // = 44,   // CGR2[25:24]
    {L"IPSYNC_VDOA_IPG_MASTER"},            // = 45,   // CGR2[27:26]
    {L"R6"},                                // = 46,   // CGR2[29:28]
    {L"R7"},                                // = 47,   // CGR2[31:30]
    {L"IPU1_IPU"},                          // = 48,   // CGR3[1:0]
    {L"IPU1_IPU_DI0"},                      // = 49,   // CGR3[3:2]
    {L"IPU1_IPU_DI1"},                      // = 50,   // CGR3[5:4]
    {L"IPU2_IPU"},                          // = 51,   // CGR3[7:6]
    {L"IPU2_IPU_DI0"},                      // = 52,   // CGR3[9:8]
    {L"IPU2_IPU_DI1"},                      // = 53,   // CGR3[11:10]
    {L"LDB_DI0"},                           // = 54,   // CGR3[13:12]
    {L"LDB_DI1"},                           // = 55,   // CGR3[15:14]
    {L"MIPI_CORE_CFG"},                     // = 56,   // CGR3[17:16]
    {L"MLB"},                               // = 57,   // CGR3[19:18]
    {L"MMDC_CORE_ACLK_FAST_CORE_P0"},       // = 58,   // CGR3[21:20]
    {L"MMDC_CORE_ACLK_FAST_CORE_P1"},       // = 59,   // CGR3[23:22]
    {L"MMDC_CORE_IPG_P0"},                  // = 60,   // CGR3[25:24]
    {L"MMDC_CORE_IPG_P1"},                  // = 61,   // CGR3[27:26]
    {L"OCRAM"},                             // = 62,   // CGR3[29:28]
    {L"OPENVGAXI_ROOT"},                    // = 63,   // CGR3[31:30]
    {L"PCIE_ROOT"},                         // = 64,   // CGR4[1:0]
    {L"PERFMON1_APB"},                      // = 65,   // CGR4[3:2]
    {L"PERFMON2_APB"},                      // = 66,   // CGR4[4:3]
    {L"PERFMON3_APB"},                      // = 67,   // CGR4[7:6]
    {L"PL301_FAST1_S133"},                  // = 68,   // CGR4[9:8]
    {L"R8"},                                // = 69,   // CGR4[11:10]
    {L"PL301_PER1_BCH"},                    // = 70,   // CGR4[13:12]
    {L"PL301_PER2_MAIN"},                   // = 71,   // CGR4[15:14]
    {L"PWM1"},                              // = 72,   // CGR4[17:16]
    {L"PWM2"},                              // = 73,   // CGR4[19:18]
    {L"PWM3"},                              // = 74,   // CGR4[21:20]
    {L"PWM4"},                              // = 75,   // CGR4[23:22]
    {L"RAWNAND_U_BCH_INPUT_APB"},           // = 76,   // CGR4[25:24]
    {L"RAWNAND_U_GPMI_BCH_INPUT_BCH"},      // = 77,   // CGR4[27:26]
    {L"RAWNAND_U_GPMI_BCH_INPUT_GPMI_IO"},  // = 78,   // CGR4[29:28]
    {L"RAWNAND_U_GPMI_INPUT_APB"},          // = 79,   // CGR4[31:30]
    {L"ROM"},                               // = 80,   // CGR5[1:0]
    {L"R9"},                                // = 81,   // CGR5[3:2]
    {L"SATA"},                              // = 82,   // CGR5[5:4]
    {L"SDMA"},                              // = 83,   // CGR5[7:6]
    {L"R10"},                               // = 84,   // CGR5[9:8]
    {L"R11"},                               // = 85,   // CGR5[11:10]
    {L"SPBA"},                              // = 86,   // CGR5[13:12]
    {L"SPDIF"},                             // = 87,   // CGR5[15:14]
    {L"R12"},                               // = 88,   // CGR5[17:16]
    {L"SSI1"},                              // = 89,   // CGR5[19:18]
    {L"SSI2"},                              // = 90,   // CGR5[21:20]
    {L"SSI3"},                              // = 91,   // CGR5[23:22]
    {L"UART"},                              // = 92,   // CGR5[25:24]
    {L"UART_SERIAL"},                       // = 93,   // CGR5[27:26]
    {L"R13"},                               // = 94,   // CGR5[29:28]
    {L"R14"},                               // = 95,   // CGR5[31:30]
    {L"USBOH3"},                            // = 96,   // CGR6[1:0]
    {L"USDHC1"},                            // = 97,   // CGR6[3:2]
    {L"USDHC2"},                            // = 98,   // CGR6[5:4]
    {L"USDHC3"},                            // = 99,   // CGR6[7:6]
    {L"USDHC4"},                            // = 100,  // CGR6[9:8]
    {L"EMI_SLOW"},                          // = 101,  // CGR6[11:10]
    {L"VDOAXI"},                            // = 102,  // CGR6[13:12]
    {L"VPU"},                               // = 103,  // CGR6[15:14]
    {L"R15"},                               // = 104,  // CGR6[17:16]
    {L"R16"},                               // = 105,  // CGR6[19:18]
    {L"R17"},                               // = 106,  // CGR6[21:20]
    {L"R18"},                               // = 107,  // CGR6[23:22]
    {L"R19"},                               // = 108,  // CGR6[25:24]
    {L"R20"},                               // = 109,  // CGR6[27:26]
    {L"R21"},                               // = 110,  // CGR6[29:28]
    {L"R22"},                               // = 111,  // CGR6[31:30]
    {L"R23"},                               // = 112,  // CGR7[1:0]
    {L"R24"},                               // = 113,  // CGR7[3:2]
    {L"R25"},                               // = 114,  // CGR7[5:4]
    {L"R26"},                               // = 115,  // CGR7[7:6]
    {L"R27"},                               // = 116,  // CGR7[9:8]
    {L"R28"},                               // = 117,  // CGR7[11:10]
    {L"R29"},                               // = 118,  // CGR7[13:12]
    {L"R30"},                               // = 119,  // CGR7[15:14]
    {L"R31"},                               // = 120,  // CGR7[17:16]
    {L"R32"},                               // = 121,  // CGR7[19:18]
    {L"R33"},                               // = 122,  // CGR7[21:20]
    {L"R34"},                               // = 123,  // CGR7[23:22]
    {L"R35"},                               // = 124,  // CGR7[25:24]
    {L"R36"},                               // = 125,  // CGR7[27:26]
    {L"R37"},                               // = 126,  // CGR7[29:28]
    {L"R38"},                               // = 127,  // CGR7[31:30]
    {L"CG_END"}                             // = 128
};

const TCHAR BaudSrcName[DDK_CLOCK_BAUD_SOURCE_ENUM_END + 1][20] = {
    {L"PLL1_CPU"},          // = 0,
    {L"PLL1_REF"},          // = 1,
    {L"PLL2_BURNIN"},       // = 2,
    {L"PLL2_BUS"},          // = 3,
    {L"PLL3_USBOTG"},       // = 4,
    {L"PLL4_AUDIO"},        // = 5,
    {L"PLL5_VIDEO"},        // = 6,
    {L"120M"},              // = 7,
    {L"528M_PFD3_200M"},    // = 8,
    {L"528M_PFD0_352M"},    // = 9,
    {L"528M_PFD2_400M"},    // = 10,
    {L"480M_PFD3_454M"},    // = 11,
    {L"480M_PFD2_508M"},    // = 12,
    {L"480M_PFD1_540M"},    // = 13,
    {L"528M_PFD1_594M"},    // = 14,
    {L"480M_PFD0_720M"},    // = 15,
    {L"PLL3_60M"},          // = 16,
    {L"PLL3_80M"},          // = 17,
    {L"AHB_132M"},          // = 18,
    {L"PERIPH"},            // = 19,
    {L"AXI"},               // = 20,
    {L"MMDC_CH0"},          // = 21,
    {L"IPG"},               // = 22,
    {L"IPP_DI0"},           // = 23,
    {L"IPP_DI1"},           // = 24,
    {L"LDB_DI0_SERIAL"},    // = 25,
    {L"LDB_DI1_SERIAL"},    // = 26,
    {L"LDB_DI0_IPU"},       // = 27,
    {L"LDB_DI1_IPU"},       // = 28,
    {L"PLL6_MLB"},          // = 29,
    {L"PLL7_USBHOST"},      // = 30,
    {L"PLL8_ENET"},         // = 31,
    {L"GND"},               // = 32,
	{L"MMDC_CH1"},          // = 33,
    {L"BAUDSRC_END"},       // = 34
};
