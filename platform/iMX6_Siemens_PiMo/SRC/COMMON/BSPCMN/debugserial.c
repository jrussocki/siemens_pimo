//-----------------------------------------------------------------------------
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//------------------------------------------------------------------------------
//
//  Copyright (C) 2007-2010, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
//  File:  debug.c
//
//  This module is provides the interface to the serial port.
//
//-----------------------------------------------------------------------------

#pragma warning(push)
#pragma warning(disable: 4115)
#include <windows.h>
#include <nkintr.h>
#pragma warning(pop)

#include "bsp.h"
#include "debugserial.h"
#include "serialutils.h"

//------------------------------------------------------------------------------
// External Functions
extern BOOL OALBspArgsInit(BSP_ARGS *pBSPArgs);
extern BOOL OALPrepareUARTFreq(BSP_ARGS *pBSPArgs);
extern VOID OALBspArgsPrint(BSP_ARGS * pBSPArgs);
extern VOID OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX index, DDK_CLOCK_GATE_MODE mode);


//------------------------------------------------------------------------------
// Defines


//------------------------------------------------------------------------------
// Externs

extern DWORD initialOALLogZones;

//------------------------------------------------------------------------------
// Global Variables

// Create global variables for the pointers to the CCM, IOMUX, and PBC control
// registers so that all of the existing OAL, PQOAL, and COMMON code will
// still work as before.
//
// These global variables are just copies of what is in the g_OALKITLSharedData
// structure which will be used to exchange data between KITL and the OAL.

PCSP_IOMUX_REGS g_pIOMUX;
PCSP_UART_REG g_pUART;  // Also used by the OAL timer and IOCTL functions.


//------------------------------------------------------------------------------
// Local Variables


//------------------------------------------------------------------------------
// Local Functions


//-----------------------------------------------------------------------------
//
// FUNCTION:    OALSerialInit
//
// DESCRIPTION:
//      Initializes the interal UART with the specified communication settings.
//
//      Note that KITL has it's own version of this function since it cannot
//      directly access the global variables that are provided by the OAL.
//
// PARAMETERS:
//      pSerInfo
//          [in]   Serial port configuration settings.
//
// RETURNS:
//      If this function succeeds, it returns TRUE, otherwise
//      it returns FALSE.
//
//-----------------------------------------------------------------------------
BOOL OALSerialInit(PSERIAL_INFO pSerInfo)
{
    return (OALConfigSerialUART(pSerInfo) &&
            OALConfigSerialIOMUX (pSerInfo->uartBaseAddr, g_pIOMUX));
}


//------------------------------------------------------------------------------
//
//  Function: OEMInitDebugSerial
//
//  Initializes the debug serial port.
//
//------------------------------------------------------------------------------
VOID OEMInitDebugSerial()
{
    SERIAL_INFO serInfo;

    BSP_ARGS *pBspArgs = (BSP_ARGS *)IMAGE_SHARE_ARGS_UA_START;

    // Fill all as zero
    memset(pBspArgs->clockFreq, 0, sizeof(pBspArgs->clockFreq));

    // Prepare UART freq first so that we can dump info in the 
    // function call to OALBspArgsInit
    OALPrepareUARTFreq(pBspArgs);

    g_pIOMUX = (PCSP_IOMUX_REGS) OALPAtoUA(CSP_BASE_REG_PA_IOMUXC);
    if (g_pIOMUX == NULL)
    {
        return;
    }

    serInfo.baudRate      = 115200;
    serInfo.dataBits      = UART_UCR2_WS_8BIT;
    serInfo.parity        = UART_UCR2_PROE_EVEN;
    serInfo.stopBits      = UART_UCR2_STPB_1STOP;
    serInfo.bParityEnable = FALSE;
    serInfo.flowControl   = FALSE;

    // Disable all OAL log zones until serial debug properly configured
    OALLogSetZones(0);

#ifdef BSP_BASE_REG_PA_DEBUG_SERIAL
    g_pUART = (PCSP_UART_REG) OALPAtoUA(BSP_BASE_REG_PA_DEBUG_SERIAL);
    if (g_pUART == NULL)
    {
        return;
    }
    serInfo.uartBaseAddr = BSP_BASE_REG_PA_DEBUG_SERIAL;

#if 0   // MX6_BRING_UP
    // CCM related, to rewrite
#else
    switch(BSP_BASE_REG_PA_DEBUG_SERIAL)
    {
    case CSP_BASE_REG_PA_UART1:
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_UART, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_UART_SERIAL, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        break;

    case CSP_BASE_REG_PA_UART2:
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_UART, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_UART_SERIAL, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        break;

    case CSP_BASE_REG_PA_UART3:
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_UART, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_UART_SERIAL, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        break;

    case CSP_BASE_REG_PA_UART4:
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_UART, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_UART_SERIAL, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        break;

    case CSP_BASE_REG_PA_UART5:
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_UART, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_UART_SERIAL, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        break;
    }
#endif

    OALSerialInit(&serInfo);


    // Configure the default log settings.
    OALLogSetZones(initialOALLogZones);

    // Initialize BSP_ARGS to get early clocking info
    OALBspArgsInit(pBspArgs);

    // Serial debug support is now active.  Print BSP_ARGS info.
    OALBspArgsPrint(pBspArgs);
#else
    g_pUART = NULL;
#endif
}


//------------------------------------------------------------------------------
//
//  Function: OEMWriteDebugByte
//
//  Transmits a character out the debug serial port.
//
VOID OEMWriteDebugByte(UINT8 ch)
{
    if (g_pUART != NULL)
    {
        // Wait until there is room in the FIFO
        while(INREG32(&g_pUART->UTS) & CSP_BITFMASK(UART_UTS_TXFULL))
            ; // Intentional polling loop.

        // Send the character
        OUTREG32(&g_pUART->UTXD, ch);
    }
}


//------------------------------------------------------------------------------
//
//  Function: OEMReadDebugByte
//
//  Reads a byte from the debug serial port. Does not wait for a character.
//  If a character is not available function returns "OEM_DEBUG_READ_NODATA"
//
int OEMReadDebugByte()
{
    int retVal = OEM_DEBUG_READ_NODATA;

    if (g_pUART != NULL)
    {
        if (INREG32(&g_pUART->USR2) & CSP_BITFMASK(UART_USR2_RDR))
        {
            retVal = INREG32(&g_pUART->URXD) & 0xFF;
        }
    }

    return retVal;
}


/*
    @func   void | OEMWriteDebugLED | Writes specified pattern to debug LEDs 1-4.
    @rdesc  None.
    @comm
    @xref
*/
void OEMWriteDebugLED(UINT16 Index, DWORD Pattern)
{
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(Index);
    UNREFERENCED_PARAMETER(Pattern);

    // There is currently no support for any debug-specific LEDs so this
    // function is just a no-op.
}
