//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//-----------------------------------------------------------------------------
//
//  Copyright (C) 2007-2010, Freescale Semiconductor, Inc. All Rights Reserved.
//  THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
//  AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//-----------------------------------------------------------------------------
//
//  File:  ethdrv.c
//
//  Common Ethernet debug driver support that is shared between EBOOT and
//  KITL.
//
//-----------------------------------------------------------------------------
#include "bsp.h"
#include <halether.h>

//-----------------------------------------------------------------------------
// External Functions


//-----------------------------------------------------------------------------
// External Variables


//-----------------------------------------------------------------------------
// Defines


//-----------------------------------------------------------------------------
// Types
//-----------------------------------------------------------------------------
// Prototypes for Ethernet controller I/O functions
typedef void (*PFN_EDBG_SETREGDW)(const UINT32 dwBase, const UINT32 dwOffset, 
                                    const UINT32 dwVal);
typedef UINT32 (*PFN_EDBG_GETREGDW)(const UINT32 dwBase, const UINT32 dwOffset);
typedef void (*PFN_EDBG_READFIFO)(const UINT32 dwBase, const UINT32 dwOffset, 
                                    UINT32 *pdwBuf, UINT32 dwDwordCount);
typedef void (*PFN_EDBG_WRITEFIFO)(const UINT32 dwBase, const UINT32 dwOffset, 
                                      const UINT32 *pdwBuf, UINT32 dwDwordCount);


//-----------------------------------------------------------------------------
// Global Variables


//-----------------------------------------------------------------------------
// Local Variables


//------------------------------------------------------------------------------
// Local Functions
//


//-----------------------------------------------------------------------------
//
//  Function: OEMEthGetSecs
//
//  This function returns the number of seconds that have passed since a 
//  certain fixed time.  This function handles time-outs while in polling 
//  mode. The origin of the count is unimportant as long as the count is 
//  incremental.
//
//  Parameters:
//      None.
//
// Returns:
//      Count of seconds that have passed since a certain fixed time.
//
//-----------------------------------------------------------------------------
DWORD OEMEthGetSecs(void)
{
    PCSP_SNVS_REGS pSNVS = (PCSP_SNVS_REGS) OALPAtoUA(CSP_BASE_REG_PA_SNVS_HP);
    
    return (INREG32(&pSNVS->LPSRTCLR) >> 15);
}


//-----------------------------------------------------------------------------
//
//  Function:  OALGetTickCount
//
//  This function is called by some KITL libraries to obtain relative time
//  since device boot. It is mostly used to implement timeout in network
//  protocol.
//
//  Parameters:
//      None.
//
// Returns:
//      Returns the number of 1-millisecond ticks that have elapsed since 
//      the last system boot or reset.
//
//-----------------------------------------------------------------------------
UINT32 OALGetTickCount()
{
    return OEMEthGetSecs () * 1000;
}


//------------------------------------------------------------------------------
//
//  Function:  OALStall
//
VOID OALStall(UINT32 uSecs)
{
    LARGE_INTEGER liStart, liEnd, liStallCount;
    static LARGE_INTEGER liFreq = {0, 0};
    static PCSP_EPIT_REG pEPIT = NULL;
    BSP_ARGS *pBspArgs;

    if (pEPIT == NULL)
    {
        // Map EPIT registers
        pEPIT = (PCSP_EPIT_REG) OALPAtoUA(CSP_BASE_REG_PA_EPIT1);
        if (pEPIT == NULL)
        {
            EdbgOutputDebugString("OALStall: EPIT mapping failed!\r\n");
            return;
        }

    }

    if (liFreq.QuadPart == 0)
    {
        pBspArgs = (BSP_ARGS *)IMAGE_SHARE_ARGS_UA_START;
        
        switch(EXTREG32BF(&pEPIT->CR, EPIT_CR_CLKSRC))
        {
        case EPIT_CR_CLKSRC_CKIL:
            liFreq.QuadPart = pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_CKIL_SYNC];
            break;

        case EPIT_CR_CLKSRC_IPGCLK:
            liFreq.QuadPart = pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_IPG];
            break;

        default:
            liFreq.QuadPart = pBspArgs->clockFreq[DDK_CLOCK_SIGNAL_PERCLK];
            break;
        }

        liFreq.QuadPart = liFreq.QuadPart / (EXTREG32BF(&pEPIT->CR, EPIT_CR_PRESCALAR) + 1);
    }


    liStallCount.QuadPart = ((liFreq.QuadPart * uSecs - 1) / 1000000) + 1;   // always round up
    
    liStart.QuadPart = INREG32(&pEPIT->CNT);
    do {
        liEnd.QuadPart = INREG32(&pEPIT->CNT);
    } while ( (liStart.QuadPart - liEnd.QuadPart) <= liStallCount.QuadPart);

}
