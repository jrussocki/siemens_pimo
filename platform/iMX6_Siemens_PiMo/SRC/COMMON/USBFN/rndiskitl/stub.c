//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES.
//
//------------------------------------------------------------------------------
//
//  File:  stub.c
//
#pragma warning(push)
#pragma warning(disable: 4115 4201 4214 4100)
#include <windows.h>
#include <oal.h>
#include <blcommon.h>
#include <windows.h>
#include <devload.h>
#include <ceddk.h>
#include <oal.h>

#include <bsp.h>


DWORD FslUfnGetKitlDMABuffer()
{
    return IMAGE_BOOT_IPLIMAGE_RAM_START;
}
