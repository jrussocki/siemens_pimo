//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES.
//
//------------------------------------------------------------------------------
//
//  File:  hwinit.c
//
//------------------------------------------------------------------------------
//
// Copyright (C) 2005-2010, Freescale Semiconductor, Inc. All Rights Reserved.
// THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
// AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------ 
#pragma warning(push)
#pragma warning(disable: 4115 4201 4214)
#include <windows.h>
#include <oal.h>
#include "bsp.h"
#include <ethdbg.h>
#include "common_usbcommon.h"
#include <mx6_ddk.h>
#include <halether.h>
#include "regsusbphy.h"

static PCSP_CCM_REGS g_pCCM;

static PCSP_IOMUX_REGS pUSBIOMUX = NULL;
extern BSP_ARGS *g_pBSPArgs;



extern VOID OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX index, DDK_CLOCK_GATE_MODE mode);


void GetRNDISMACAddress(UINT16 mac[3])
{
    //memcpy( mac, g_EbootCFG.mac, 6);
    memcpy( mac, g_pBSPArgs->kitl.mac, 6);
    mac[0] |= 2;
}

//-----------------------------------------------------------------------------
//
//  Function: InitializeMux
//
//  This function is to configure the IOMUX
//  can safely share access.
//
//  Parameters:
//     speed - the speed of the device to configure as OTG can be configured as 
//     HS or FS .  Currently only HS is used and as such the parameter is not used.
//
//  Returns:
//      TRUE - success, FALSE - failure.
//
//-----------------------------------------------------------------------------
static BOOL InitializeMux()
{
    PCSP_IOMUX_REGS pIOMUX;
    
    pIOMUX = (PCSP_IOMUX_REGS) OALPAtoUA(CSP_BASE_REG_PA_IOMUXC);
    if (pIOMUX == NULL)
    {
        OALMSG(TRUE, (L"USB: IOMUXC mapping failed!\r\n"));
        return FALSE;
    }

    OAL_IOMUX_SET_MUX(pIOMUX, DDK_IOMUX_PIN_GPIO_1, DDK_IOMUX_PIN_MUXMODE_ALT3, DDK_IOMUX_PIN_SION_REGULAR);
    
     




    return TRUE;
}

DWORD GetUSBBaseAddress()
{
    return CSP_BASE_REG_PA_USB;
}

BOOL controllerSetup(CSP_USB_REGS* regs)
{
    USB_USBMODE_T mode;
    DWORD * temp;

    USB_USBCMD_T cmd;
    USB_USBSTS_T status;
    USB_PORTSC_T portsc;

    //Stop the controller first    
    temp = (DWORD *)&cmd;
    *temp = INREG32(&regs->OTG.USBCMD);
    cmd.RS = 0;
    OUTREG32(&regs->OTG.USBCMD, *temp);
    while ((INREG32(&regs->OTG.USBCMD) & 0x1) == 0x1);  
  
    // reset the usb otg   
    temp = (DWORD *)&cmd;
    *temp = INREG32(&regs->OTG.USBCMD);
    cmd.RST = 1;
    OUTREG32(&regs->OTG.USBCMD, *temp);
    while ((INREG32(&regs->OTG.USBCMD) & 0x1<<1) == (0x1<<1));
        
    // Set USB Mode 
    temp=(DWORD *)&mode;
    *temp=2;
    OUTREG32(&regs->OTG.USBMODE, *temp);    
    while ((INREG32(&regs->OTG.USBMODE) != 2));
    //mode.SLOM=1;        // 2.3 hardware and later
    //OUTREG32(&regs->OTG.USBMODE, *temp);  

     // Reset all interrupts           
    temp  =(DWORD*)&status;
    *temp=0;            
    OUTREG32(&regs->OTG.USBSTS, *temp);
      
    //set UTMI
    temp=(DWORD *)&portsc;
    *temp=INREG32(&regs->OTG.PORTSC);

    // Set PORTSC 
    if( portsc.PTS != 0)
    {
        portsc.PTS=0;       //Set Transeiver to UTMI               
        INSREG32(&regs->OTG.PORTSC,(3<<30),*temp);
    }  

    OUTREG32(&regs->OTG.USBINTR,  0x157);

    return TRUE;    
}


//-----------------------------------------------------------------------------
//
//  Function: InitializeTransceiver
//
//  This function is to configure the USB core
//
//  Parameters:
//     regs - pointer to the registers of the OTG USB core
//     speed - the speed of the OTG USB Core to be configured.
//     
//  Returns:
//     TRUE - success, FALSE - failure
//
//-----------------------------------------------------------------------------
static BOOL InitializeTransceiver(CSP_USB_REGS* regs)
{
    DWORD r=TRUE;
    DWORD * temp;
    PVOID pv_HWregUSBPhy0;
    PVOID pv_HWregUSBPhy1;
    PCSP_PLL_BASIC_REGS pPLL_USBOTG;
    OTG_CTRL_T ctrl;

    pv_HWregUSBPhy0 = (PVOID) OALPAtoUA(CSP_BASE_REG_PA_OTGPHY);
    pv_HWregUSBPhy1 = (PVOID) OALPAtoUA(CSP_BASE_REG_PA_UH1PHY);
    pPLL_USBOTG=(PCSP_PLL_BASIC_REGS) OALPAtoUA(CSP_BASE_REG_PA_PLL_USBOTG);
 
    {
    DWORD data=0;
    pPLL_USBOTG->CTRL_CLR=(0x1<<16);
    pPLL_USBOTG->CTRL_SET=(0x1<<6);
    
    data = INREG32(pPLL_USBOTG->CTRL);  
    data |=  0x1020;
    OUTREG32(pPLL_USBOTG->CTRL, data);

   
    // Soft Reset the PHY
    HW_USBPHY_CTRL_SET(0,BM_USBPHY_CTRL_SFTRST);
    OALStall(10000);

    //clear the soft reset
    HW_USBPHY_CTRL_CLR(0,BM_USBPHY_CTRL_SFTRST);
    OALStall(10000);

    HW_USBPHY_CTRL_CLR(0,BM_USBPHY_CTRL_CLKGATE);
    OALStall(10000);
    
   // Clear the power down bits
    HW_USBPHY_PWD(0).U = 0;

    }
    {
        USB_PORTSC_T portsc;
        temp = (DWORD *)&portsc;
        *temp = INREG32(&regs->OTG.PORTSC);
        if(portsc.PHCD ==1)
        {
          portsc.PHCD = 0;
          OUTREG32(&regs->OTG.PORTSC, *temp);
        }
    }
  
    temp=(DWORD *)&ctrl;
    *temp = INREG32(&regs->OTG_CTRL);
    ctrl.PM = 1;       // OTG Power Mask
    ctrl.WIE = 1;
    SETREG32(&regs->OTG_CTRL, *temp);

    
#if 0
    {
        USB_PORTSC_T portsc;
        temp=(DWORD *)&portsc;
        *temp=INREG32(&regs->OTG.PORTSC);
        // Set PORTSC 
        portsc.PTS=0;       //Set Transeiver to UTMI        
        // first mask out the PTS field, then set all the above bits
        INSREG32(&regs->OTG.PORTSC,(3<<30),*temp);
    }   
#endif    

    // reset the usb otg   
    {
        USB_USBCMD_T cmd;
        temp = (DWORD *)&cmd;
        *temp = INREG32(&regs->OTG.USBCMD);
        cmd.RST = 1;
        OUTREG32(&regs->OTG.USBCMD, *temp);
        while ((INREG32(&regs->OTG.USBCMD) & 0x1<<1) == (0x1<<1)){
       
        }
    }
    {
        DWORD dwCurSec,timeOut;
        timeOut = 1; //delay 1 s.
        dwCurSec = OEMEthGetSecs();
        while((OEMEthGetSecs() - dwCurSec) <= timeOut);
    }

  
    controllerSetup( regs);
    return r;
}


//PCSP_DPLL_REGS g_pDPLL1;
//PCSP_DPLL_REGS g_pDPLL2;
//PCSP_DPLL_REGS g_pDPLL3;






//-------------------------------------------------------------
//
// Function: USBClockInit
//
// Init and start the USB Core Clock
//
// Parameter: NULL
//
// Return: 
//   TRUE - success to start the clock 
//   FALSE - fail to start the clock
//
//-------------------------------------------------------------
BOOL USBClockInit(void)
{  

    //PCSP_CCM_REGS pCCM;
    //DWORD bit, cgr_num, i; //DDK_CLOCK_GATE_INDEX_USBOTG / 16
  //  DWORD temp;

    g_pCCM = (PCSP_CCM_REGS) OALPAtoUA(CSP_BASE_REG_PA_CCM);  

#if 0
    // Enable USBOH2_IPG
    bit = DDK_CLOCK_GATE_MODE_ENABLED_ALL << (( DDK_CLOCK_GATE_INDEX_USBOH3_IPG % 16) << 1);
    i = DDK_CLOCK_GATE_INDEX_USBOH3_IPG;
    cgr_num = 0;
    while( i >= 16)
    {
        cgr_num++;
        i -= 16;
    }
    OUTREG32( &pCCM->CCGR[cgr_num], ((INREG32(&pCCM->CCGR[cgr_num]) | bit)) );  

    // Enable USBOH2_60M
    bit = DDK_CLOCK_GATE_MODE_ENABLED_ALL << (( DDK_CLOCK_GATE_INDEX_USBOH3_60M % 16) << 1);
    i = DDK_CLOCK_GATE_INDEX_USBOH3_60M;
    cgr_num = 0;
    while( i >= 16)
    {
        cgr_num++;
        i -= 16;
    }
    OUTREG32( &pCCM->CCGR[cgr_num], ((INREG32(&pCCM->CCGR[cgr_num]) | bit)) );  

    // Enable USB_PHY
    bit = DDK_CLOCK_GATE_MODE_ENABLED_ALL << (( DDK_CLOCK_GATE_INDEX_USB_PHY1 % 16) << 1);
    i = DDK_CLOCK_GATE_INDEX_USB_PHY1;
    cgr_num = 0;
    while( i >= 16)
    {
        cgr_num++;
        i -= 16;
    }
    OUTREG32( &pCCM->CCGR[cgr_num], ((INREG32(&pCCM->CCGR[cgr_num]) | bit)) );  
#endif

#if 0   // MX6_BRING_UP
    // CCM related, to rewrite
    // Different Clock Configuration in MX6, to be examined
#else
   // DDKClockConfigBaud

    OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_USBOH3, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
//    temp=INREG32(g_pPLL_USBOTG->CTRL);
//     RETAILMSG(1,(TEXT("22temp:%x\r\n"),temp));
//    temp|=(1<<12);
//    temp|=(1<<6);
//    RETAILMSG(1,(TEXT("11temp:%x\r\n"),temp));
 //   OUTREG32(g_pPLL_USBOTG->CTRL,temp);
    
  //  OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_USBOH3_60M, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
  //  OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_USB_PHY1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
 //   OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_USB_PHY2, DDK_CLOCK_GATE_MODE_ENABLED_ALL);

    //g_pCCM=(PCSP_CCM_REGS)pCCM;
#if 0
    if (!DDKClockConfigBaud(DDK_CLOCK_SIGNAL_USBOH3, DDK_CLOCK_BAUD_SOURCE_PLL3, 5-1, 2-1))
    {
        DEBUGMSG (ZONE_ERROR, (L"USBClockSet: DDKClockConfigBaud for USBOH3 failed\r\n"));
        return FALSE;
    }
 
    // for USB_PHY
    if (!DDKClockConfigBaud(DDK_CLOCK_SIGNAL_USB_PHY, DDK_CLOCK_BAUD_SOURCE_OSC, 1-1, 1-1))
    {
        DEBUGMSG (ZONE_ERROR, (L"USBClockSet: DDKClockConfigBaud for USB_PHY failed\r\n"));
        return FALSE;
    }

    #endif
#endif

#if 0
    {
        BOOL rc = TRUE;

        UINT32 oldReg, newReg, *pSelReg = NULL, *pDivReg = NULL;
        UINT32 selShift = 0;
        UINT32 selMask = 0;
        UINT32 preDivShift = 0;
        UINT32 preDivMask = 0;
        UINT32 postDivShift = 0;
        UINT32 postDivMask = 0;
        UINT32 selVal = src;
        UINT32 selVal2 = 0

        selShift = CCM_CSCMR1_USBOH3_CLK_SEL_LSH;
        selMask = CSP_BITFMASK(CCM_CSCMR1_USBOH3_CLK_SEL);
        pSelReg = &g_pCCM->CSCMR1;
        
        preDivShift = CCM_CSCDR1_USBOH3_CLK_PRED_LSH;
        preDivMask = CSP_BITFMASK(CCM_CSCDR1_USBOH3_CLK_PRED);
        postDivShift = CCM_CSCDR1_USBOH3_CLK_PODF_LSH;
        postDivMask = CSP_BITFMASK(CCM_CSCDR1_USBOH3_CLK_PODF);
        pDivReg = &g_pCCM->CSCDR1;


        if(pSelReg)
        {
            selVal <<= selShift;
            
            do
            {
                oldReg = INREG32(pSelReg);
                newReg = (oldReg & (~selMask)) | selVal;
            } while ((UINT32) InterlockedTestExchange((LPLONG) pSelReg, 
                               oldReg, newReg) != oldReg);
        }

       // Update the dividers
        if (pDivReg)
        {
            // Update the global clock signal table
            //BSPClockUpdateFreq(sig, src, preDiv, postDiv);
            
            preDiv <<= preDivShift;
            preDiv &= preDivMask;
            postDiv <<= postDivShift;
            postDiv &= postDivMask;
            do
            {
                oldReg = INREG32(pDivReg);
                newReg = (oldReg & (~(preDivMask | postDivMask))) 
                    | (preDiv) | (postDiv);
            } while ((UINT32)InterlockedTestExchange((LPLONG)pDivReg, 
                               oldReg, newReg) != oldReg);        
        }

    }
#endif

    return TRUE;
}

BOOL USBClockEnable(BOOL bFlag)
{  
#if 1   // MX6_BRING_UP
    // CCM related, to rewrite
    // Different Clock Configuration in MX6, need to rewrite
#else
    g_pCCM = (PCSP_CCM_REGS) OALPAtoUA(CSP_BASE_REG_PA_CCM);  
    if(bFlag)
    {
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_USBOH3_IPG, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_USBOH3_60M, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_USB_PHY1, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_USB_PHY2, DDK_CLOCK_GATE_MODE_ENABLED_ALL);
    }
    else
    {
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_USBOH3_IPG, DDK_CLOCK_GATE_MODE_DISABLED);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_USBOH3_60M, DDK_CLOCK_GATE_MODE_DISABLED);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_USB_PHY1, DDK_CLOCK_GATE_MODE_DISABLED);
        OALClockSetGatingMode(DDK_CLOCK_GATE_INDEX_USB_PHY2, DDK_CLOCK_GATE_MODE_DISABLED);
    }


    if (!DDKClockConfigBaud(DDK_CLOCK_SIGNAL_USBOH3, DDK_CLOCK_BAUD_SOURCE_PLL2_BUS, 6-1, 1-1))
    {
        DEBUGMSG (ZONE_ERROR, (L"USBClockSet: DDKClockConfigBaud for USBOH3 failed\r\n"));
        return FALSE;
    }
 
    // for USB_PHY
    if (!DDKClockConfigBaud(DDK_CLOCK_SIGNAL_USB_PHY, DDK_CLOCK_BAUD_SOURCE_OSC, 1-1, 1-1))
    {
        DEBUGMSG (ZONE_ERROR, (L"USBClockSet: DDKClockConfigBaud for USB_PHY failed\r\n"));
        return FALSE;
    }
#endif

    return TRUE;
}


//------------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
//  Function: HardwareInitialize
//
//  This function is being called by CSP code to initialize the transceiver, USB core
//  and CPLD
//
//  Parameters:
//     regs - Pointer to the 3 USB Core registers.
//     
//  Returns:
//     TRUE - success, FALSE - failure
//
//-----------------------------------------------------------------------------

BOOL HardwareInitialize(CSP_USB_REGS * regs)
{
    pUSBIOMUX = (PCSP_IOMUX_REGS) OALPAtoUA(CSP_BASE_REG_PA_IOMUXC);
    
    if (USBClockInit() && 
         InitializeMux()  && InitializeTransceiver(regs))
        return TRUE;

    return FALSE;
}

//------------------------------------------------------------------------------
// Function: PowerDownSchemeExist
// 
// Description: This function is called to check if we can stop the system clock
//              and PHY clock, it is determined by IC capability, if VBUS and ID
//              interrupt without clock can wake up the system, we can enter 
//              low power mode, else we can't
//
//              while in USB KITL, we always return FALSE
//------------------------------------------------------------------------------
BOOL PowerDownSchemeExist(void)
{
    return FALSE;
}

//------------------------------------------------------------------------------
// Function: BSPUsbfnDetachNeedRSSwitch
// 
// Description: This function is called to check if we need to close and reopen 
//              USBCMD.RS to put port back to original mode
//              needed in MX28
//
//------------------------------------------------------------------------------
BOOL BSPUsbfnDetachNeedRSSwitch(void)
{
    return FALSE;
}

//------------------------------------------------------------------------------

// Function: BSPUsbPhyEnterLowPowerMode
// 
// Description: This function is called to enable/disable VBUS interrupt
//     blEnable
//         [IN] TRUE - enable, FALSE - disable
//
// Return: 
//      NULL
//------------------------------------------------------------------------------
void BSPUsbPhyEnterLowPowerMode(PUCHAR baseMem, BOOL blEnable)
{
    UNREFERENCED_PARAMETER(baseMem);     
    UNREFERENCED_PARAMETER(blEnable); 
}

//-----------------------------------------------------------------------------
//
//  Function: BSPUsbResetPHY
//
//  This is called by the public common CSP library to reset USB phy.
//
//  Parameters:
//      regs - Pointer to the 3 USB Core registers.
//
//  Returns:
//      
//
//-----------------------------------------------------------------------------
void BSPUsbResetPHY(CSP_USB_REGS * regs)
{
    UNREFERENCED_PARAMETER(regs);     
}

//------------------------------------------------------------------------------
// Function: BSPUsbSetBusConfig
// 
// Description: This function is called to set proper AHB BURST Mode
//
// Return: 
//      NULL
//------------------------------------------------------------------------------
void BSPUsbSetBusConfig(PUCHAR baseMem)
{
    // MX6 did nothing
    UNREFERENCED_PARAMETER(baseMem);
}

//------------------------------------------------------------------------------
// Function: BSPUSBInterruptControl
//
// This function is used to register USB interrupt as system wakeup source or NOT
//
// Parameters:
//          dwIOCTL  -- IOCTL_HAL_ENABLE_WAKE or IOCTL_HAL_DISABLE_WAKE
//          pSysIntr -- pointer to sys interrupt
//          dwLength -- size of pSysIntr
//     
// Returns:
//      N/A
//     
//
//------------------------------------------------------------------------------
void BSPUSBInterruptControl(DWORD dwIOCTL, PVOID pSysIntr, DWORD dwLength)
{
    UNREFERENCED_PARAMETER(dwIOCTL);
    UNREFERENCED_PARAMETER(pSysIntr);
    UNREFERENCED_PARAMETER(dwLength);
    return;
}

//-----------------------------------------------------------------------------
//
//  Function: BSPPhyShowDevDiscon
//  
//  Description : This function checks USBPHY if disconnect happens in peripheral mode
//
//  Retrun : TRUE means disconnect
//           FALSE means not disconnect
//
//------------------------------------------------------------------------------
BOOL BSPPhyShowDevDiscon(void)
{
    return FALSE;
}

//------------------------------------------------------------------------------
// Function: BSPUsbSetCurrentLimitation
// 
// Description: This function is called to set proper current limitaion 
//
// Parameters 
//      bLimitOn : BOOL, true to turn on current limitation, false to turn off limitation
// Return: 
//      NULL
//------------------------------------------------------------------------------
void BSPUsbSetCurrentLimitation(BOOL bLimitOn)
{
    UNREFERENCED_PARAMETER(bLimitOn);
}
